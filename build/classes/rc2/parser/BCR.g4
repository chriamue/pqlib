// Define a grammar called BCR
grammar BCR;

@header {
package rc2.parser;
}

complete : (generic)* ( initializing '{' (command)* '}' )?;
initializing : accessModes functionHead ;
generic: '@generic' '{' NAME '}' '{' CHARARRAY (',' CHARARRAY )* '}' ;
accessModes : '@accessModes' '{' (ACCESSMODE (',' ACCESSMODE )* )? '}' ;
functionHead : NAME '(' (NAME (',' NAME )*)? ')' ;
command : ifinstruction | simpleCommand;
simpleCommand : labelCommand | returnCommand | breakCommand | continueCommand | whileCommand | doWhileCommand | assignmentCommand | methodCallCommand; // simpleSet | newSet | constantSet | methodSet | calculationSet;
labelCommand : NAME ':' ;
returnCommand : 'abort' ';' | 'proceed' ';' | 'proceed' 'on' NAME '?=' expr ';' ;
whileCommand : 'while' '(' ifcontent ')' ifElseBody ;
doWhileCommand : 'do' ifElseBody 'while' '(' ifcontent ')' ';' ;
breakCommand : 'break' ';' ;
continueCommand : 'continue' ';' ;
assignmentCommand : (typeInformation)? (TYPE)? NAME '=' expr ';' ;
methodCallCommand : methodCall ';' ;

expr : orExpr;
orExpr : xorExpr | orExpr '|' xorExpr;
xorExpr : andExpr | xorExpr '^' andExpr;
andExpr : shiftExpr | andExpr '&' shiftExpr;
shiftExpr : addExpr | shiftExpr ('<<'|'>>'|'>>>') addExpr;
addExpr : mulExpr | addExpr ('+'|'-') mulExpr;
mulExpr : unaryExpr | mulExpr ('*'|'/'|'%') unaryExpr;
unaryExpr : literalExpr | constantExpr | methodCall | newExpr | ('!'|'~'|'-') unaryExpr ;
constantExpr : INT | DOUBLE | CHARARRAY ;
literalExpr : NAME | '(' expr ')' ;
newExpr: 'new' NAME '(' (expr (',' expr )* )? ')' ;
methodCall : (typeInformation)? (NAME '.')? NAME '(' (expr (',' expr )* )? ')' ;
//newExpr: 'new' NAME '(' (NAME (',' NAME )* )? ')' ;
//methodCall : (typeInformation)? (NAME '.')? NAME '(' (NAME (',' NAME )* )? ')' ;

// simpleSet : (typeInformation)? NAME '=' NAME ';' ;
// newSet : NAME '=' 'new' NAME '(' (NAME (',' NAME )* )? ')' ';' ;
// constantSet : NAME '=' (DOUBLE | INT) ';' ;
// methodSet : (typeInformation)? (NAME '=')? (NAME '.')? NAME '(' (NAME (',' NAME )* )? ')' ';' ;
// calculationSet : (typeInformation)? NAME '=' (NAME OPERATORTWOAR (NAME | DOUBLE | INT) | OPERATORONEAR NAME ) ';' ;
typeInformation : '@type' '{' TYPE '}' ;

ifinstruction : 'if' '(' ifcontent ')' ifElseBody ( 'else' ifElseBody )? ;
ifcontent : ifconnection ;
ifconnection : ifsubcondition ('&&' ifsubcondition)* | ifsubcondition ('||' ifsubcondition)* ;
ifsubcondition : ifliteral | ('!')? '(' ifconnection ')';
ifliteral: (typeInformation)? ('!')?  NAME IFOPERATOR (NAME | (('-')? (DOUBLE | INT))) | ('!')? NAME 'instanceof' NAME | ('!')? NAME IFOPERATOR NULL | ('!')? 'isConst' '(' NAME ')'  | ('!')? 'isMode' '(' '(' NAME (',' NAME)* ')' ',' '(' ACCESSMODE ('||' ACCESSMODE )* ')' ')' | ('!')? 'isParallelMode' '(' PARALLELMODE (',' PARALLELMODE )* ')' ;
ifElseBody : command | '{' (command)* '}' ;


CHARARRAY :   '"' ( '\\"' | '\\\\' | ~('\n'|'\r') )*? '"';

// CHARARRAY : '"' ( [^"A]* ('A'|'A"')* )* '"' ;
NULL : 'null' ;
TYPE : 'int' | 'long' | 'double' | 'char' | 'short' | 'byte' | 'boolean' | 'float' | 'string' | 'object' ;
ACCESSMODE : [rw_?]+ ;
PARALLELMODE : 'serial' | 'parallel' ;
NAME : [A-Za-z_]+[0-9A-Za-z_]* ;
DOUBLE : [0-9]+ '.' [0-9]+ ;
INT : [0-9]+ ;
IFOPERATOR : '==' | '!=' | '<=' | '>=' | '<' | '>' ;
// OPERATORTWOAR : '+' | '-' | '*' | '/' | '%' | '|' | '&' | '^' | '<<' | '>>' | '>>>' ;
// OPERATORONEAR : '!' | '~' ;
WS : [ \r\t]+ -> skip ;
NEWLINE : '\n' -> skip ;
COMMENTLINE : '//' ~('\r' | '\n')* -> skip ;
COMMENTBLOCK : '/*' (.)*? '*/' -> skip ;
