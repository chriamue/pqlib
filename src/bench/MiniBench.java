/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package bench;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import edu.umass.pql.*;
import java.util.ArrayList;

public class MiniBench implements TableConstants, VarConstants
{
	static final int i0r = Env.encodeReadVar(TYPE_INT, 0);
	static final int i0w = Env.encodeWriteVar(TYPE_INT, 0);
	static final int i1r = Env.encodeReadVar(TYPE_INT, 1);
	static final int i1w = Env.encodeWriteVar(TYPE_INT, 1);
	static final int i2r = Env.encodeReadVar(TYPE_INT, 2);
	static final int i2w = Env.encodeWriteVar(TYPE_INT, 2);
	static final int i3r = Env.encodeReadVar(TYPE_INT, 3);
	static final int i3w = Env.encodeWriteVar(TYPE_INT, 3);
	static final int i4r = Env.encodeReadVar(TYPE_INT, 4);
	static final int i4w = Env.encodeWriteVar(TYPE_INT, 4);
	static final int i5r = Env.encodeReadVar(TYPE_INT, 5);
	static final int i5w = Env.encodeWriteVar(TYPE_INT, 5);

	static final int o0r = Env.encodeReadVar(TYPE_OBJECT, 0);
	static final int o0w = Env.encodeWriteVar(TYPE_OBJECT, 0);
	static final int o1r = Env.encodeReadVar(TYPE_OBJECT, 1);
	static final int o1w = Env.encodeWriteVar(TYPE_OBJECT, 1);
	static final int o2r = Env.encodeReadVar(TYPE_OBJECT, 2);
	static final int o2w = Env.encodeWriteVar(TYPE_OBJECT, 2);
	static final int o3r = Env.encodeReadVar(TYPE_OBJECT, 3);
	static final int o3w = Env.encodeWriteVar(TYPE_OBJECT, 3);

	static final Env env = new Env(INT_TABLE | OBJECT_TABLE, new Object[] { new int[9], new Object[9] });

	public static void
	setInts(int i0, int i1, int i2, int i3)
	{
		env.setInt(i0r, i0);
		env.setInt(i1r, i1);
		env.setInt(i2r, i2);
		env.setInt(i3r, i3);
	}

	void
	setObjects(Object v0, Object v1, Object v2)
	{
		env.setObject(o0r, v0);
		env.setObject(o1r, v1);
		env.setObject(o2r, v2);
	}

	// ================================================================================
	// compute reflection overhead

	public static final void
	put16(String s)
	{
		System.out.print(s);
		if (s.length() < 8)
			System.out.print("\t");
		if (s.length() < 16)
			System.out.print("\t");
	}


	// ================================================================================
	public static String[] benchmarks = new String[] { "NoMatch", "DocFreq", "BASE_DocFreq", "DocSet", "BASE_DocSet" };

	public static int
	benchNoMatch()
	{
		setInts(0, 1, 1000000, 777777);
		Reduction red = PQLFactory.Forall(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.NEQ_Int(i0r, i3r),
					       i4r, i1r);
		long start = System.currentTimeMillis();
		red.reset(env);
		red.next(env);
		return (int) (System.currentTimeMillis() - start);
	}

	static String[][] documents = ParallelReductionTest.generateDocuments(32, 2048);

	public static int
	benchDocFreq()
	{
		env.setInt(i4r, 1);
		env.setObject(o0r, documents);
		Join join = ParallelReductionTest.pql_comp_computeWordToDocumentToFrequency();
		long start = System.currentTimeMillis();
		join.reset(env);
		join.next(env);
		return (int) (System.currentTimeMillis() - start);
	}

	public static int
	benchBASE_DocFreq()
	{
		long start = System.currentTimeMillis();
		ParallelReductionTest.computeWordToDocumentToFrequency(documents);
		return (int) (System.currentTimeMillis() - start);
	}

	public static int
	benchDocSet()
	{
		env.setInt(i4r, 1);
		env.setObject(o0r, documents);
		Join join = ParallelReductionTest.pql_comp_computeWordToContainingDocumentIDs();
		long start = System.currentTimeMillis();
		join.reset(env);
		join.next(env);
		return (int) (System.currentTimeMillis() - start);
	}

	public static int
	benchBASE_DocSet()
	{
		long start = System.currentTimeMillis();
		ParallelReductionTest.computeWordToContainingDocumentIDs(documents);
		return (int) (System.currentTimeMillis() - start);
	}


	public static int
	runBenchmark(String mname)
	{
		Method my_method = null;

		if (my_method == null)
			for (Method m : MiniBench.class.getDeclaredMethods()) {
				if (m.getName().equals(mname)) {
					my_method = m;
					break;
				}
			}
		if (my_method == null)
			throw new RuntimeException("Undefined benchmark method `" + mname + "'");

		try {
			Integer timings = (Integer) my_method.invoke(null);
			return timings.intValue();
		} catch (InvocationTargetException t) {
			if (t.getTargetException() instanceof RuntimeException)
				throw ((RuntimeException) t.getTargetException());
			else
				throw new RuntimeException(t.getTargetException());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void
	printHeader()
	{
		put16("#cores");
		for (int i = 0 ; i < benchmarks.length; i++)
			put16(benchmarks[i]);
		System.out.println();
	}

	public static void
	printTimings(int cpu_count, ArrayList<Integer> timings)
	{
		put16("" + cpu_count);
		for (int i = 0 ; i < benchmarks.length; i++)
			put16(timings.get(i) + " ms");
		System.out.println();
	}

	public static void
	printRelativeTimings(int cpu_count, ArrayList<Integer> baselines, ArrayList<Integer> timings)
	{
		java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
		put16("" + cpu_count);
		for (int i = 0 ; i < benchmarks.length; i++)
			put16(df.format((baselines.get(i) * 1.0) / (timings.get(i) * 1.0)));
		System.out.println();
	}

	static int BENCHMARK_COUNTS = 32;

	public static ArrayList<Integer>
	benchmark()
	{
		ArrayList<Integer> retval = new ArrayList<Integer>();

		for (int i = 0 ; i < benchmarks.length; i++) {
			int number = 0;
			for (int k = 0; k < BENCHMARK_COUNTS; k++)
				runBenchmark("bench" + benchmarks[i]); // discard

			for (int k = 0; k < BENCHMARK_COUNTS; k++)
				number += runBenchmark("bench" + benchmarks[i]); // count

			retval.add(number);
		}

		return retval;
	}

	public static void
	benchAllThreadCounts(ArrayList<Integer> baselines)
	{
		for (int i = 1; i < Env.DEFAULT_THREADS_NR * 2; i++) {
		 	Env.setThreadsNr(i);
		 	printRelativeTimings(i, baselines, benchmark());
		}
	}

	public static void
	main(String[] __)
	{
		PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
		ArrayList<Integer> baselines = benchmark();
		printHeader();
		printTimings(1, baselines);

		System.out.println("\n\tSEGMENTED:\n");
		PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.SEGMENTED);
		benchAllThreadCounts(baselines);

		System.out.println("\n\tSTRIPED:\n");
		PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.STRIPED);
		benchAllThreadCounts(baselines);
	}
}