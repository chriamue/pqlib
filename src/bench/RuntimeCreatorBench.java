/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bench;

import benchmarks.BenchmarkOptions;
import benchmarks.bonus.Bonus;
import benchmarks.bonus.Department;
import benchmarks.bonus.Employee;
import benchmarks.threegrep.Generator;
import benchmarks.webgraph.Link;
import benchmarks.webgraph.Webdoc;
import edu.umass.pql.ArithmeticTest;
import edu.umass.pql.DefaultValueDouble;
import edu.umass.pql.DefaultValueInt;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TestBase;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.junit.Test;

/**
 *
 * @author Hilmar
 */
public class RuntimeCreatorBench extends TestBase {

    @DefaultValueInt(0)
    public static int sumInt (int l, int r) {
        return l + 1;
    }

    @DefaultValueInt(0)
    public static int sum2Int (int l, int r) {
        return l + r;
    }

    @DefaultValueInt(0)
    public static int myFunction (int l, int r) {
        System.out.println("myFunction " + l + ", " + r);
        return 0;
    }

    @DefaultValueDouble(0.0D)
    public static double sumDouble (double l, double r) {
        return l + r;
    }

    public static long measureSteps [] = new long[4];

    @Test
    public Join idf (Env env) {

        /* idf:
   Reduce[DEFAULT_MAP(?i0; ?i2, METHOD<sumInt(int,int),null>(?i4): __):
      !o3]: { CONTAINS(?o1; !o0);  Field(?o0; !o2);  EQ_Int(?i1; !i4);
      ARRAY_LOOKUP_Int(?o2; __, !i2); }
 Ausgabe: o3*/
        this.env = env;

        Method method = null;
        for (int i=0; i<RuntimeCreatorBench.class.getMethods().length; i++) {
            if (RuntimeCreatorBench.class.getMethods()[i].getName().contains("sumInt"))
                method = RuntimeCreatorBench.class.getMethods()[i];
        }

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.N_DEFAULT_MAP( (i0r | VAR_CONST_FLAG), i5r, PQLFactory.Reductors.METHOD_ADAPTER(method, i4r, __), o3w) },
                PQLFactory.CONTAINS(o1r, o0w),
                PQLFactory.FIELD(Webdoc.class, "words", o0r, o2w),
                PQLFactory.EQ_Int( (i0r | VAR_CONST_FLAG), i4w),
                PQLFactory.ARRAY_LOOKUP_Int(o2r, __ ,i5w)
                );

        return p;
    }

    @Test
    public Join bonus (Env env) {
        /*bonus:
   Reduce[MAP(?o0, ?d0): !o5]: { CONTAINS(?o1; !o0);  Field(?o0; !o2);
   Field(?o2; !d1);  Reduce[METHOD<sumDouble(double,double),null>(?d2):
   !d4]: { Field(?o0; !o4);  CONTAINS(?o4; !o3);  Field(?o3; !d2); };
   MUL_Double(?d1, ?d4; !d0); }
 Ausgabe: o5*/
        this.env = env;

        Method method = null;
        for (int i=0; i<RuntimeCreatorBench.class.getMethods().length; i++) {
            if (RuntimeCreatorBench.class.getMethods()[i].getName().contains("sumDouble"))
                method = RuntimeCreatorBench.class.getMethods()[i];
        }

        final int d4r = Env.encodeReadVar(TYPE_DOUBLE, 4);
        final int d4w = Env.encodeWriteVar(TYPE_DOUBLE, 4);
        final int d5r = Env.encodeReadVar(TYPE_DOUBLE, 5);

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.MAP(o2r, d0r, o5w) },
                PQLFactory.CONTAINS( (o0r | VAR_CONST_FLAG), o2w),
                PQLFactory.FIELD(Employee.class, "dept", o2r, o1w),
                PQLFactory.FIELD(Department.class, "bonus_factor", o1r, d1w),
                PQLFactory.ADD_Double( (d5r | VAR_CONST_FLAG), (d5r | VAR_CONST_FLAG), d4w),
                PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, d2r, d4w) },
                    PQLFactory.FIELD(Employee.class, "bonusSet", o2r, o4w),
                    PQLFactory.CONTAINS(o4r, o3w),
                    PQLFactory.FIELD(Bonus.class, "bonus_base", o3r, d2w)
                    ),
                PQLFactory.MUL_Double(d1r, d4r, d0w)
                );

        return p;
    }

    @Test
    public Join threegrep (Env env) {
        /*threegrep:
   Reduce[SET(?o1): !o4]: { ARRAY_LOOKUP_Object(?o2; __, !o1);
   ARRAY_LOOKUP_Byte(?o1; !i6, ?i0);  INT_RANGE_CONTAINS(?i12, ?i13;
   ?i6);  ADD_Int(?i6, ?i1; !i8);  ARRAY_LOOKUP_Byte(?o1; ?i8, ?i2);
   ADD_Int(?i6, ?i3; !i10);  ARRAY_LOOKUP_Byte(?o1; ?i10, ?i4); }
 Ausgabe: o4*/

        this.env = env;

        final int i12r = Env.encodeReadVar(TYPE_INT, 12);
        final int i13r = Env.encodeReadVar(TYPE_INT, 13);
        final int i8r = Env.encodeReadVar(TYPE_INT, 8);
        final int i8w = Env.encodeWriteVar(TYPE_INT, 8);
        final int i10r = Env.encodeReadVar(TYPE_INT, 10);
        final int i10w = Env.encodeWriteVar(TYPE_INT, 10);

        env.v_object[1] = new byte [1];
        env.v_object[2] = Generator.data_array;
        env.v_object[4] = new PSet ();
        env.v_int[0] = 48;
        env.v_int[2] = 49;
        env.v_int[4] = 50;
        env.v_int[1] = 1;
        env.v_int[3] = 2;
        env.v_int[12] = 0;
        env.v_int[13] = Generator.RECORD_SIZE - 3;

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(o1r, o4w)},
                PQLFactory.ARRAY_LOOKUP_Object( o2r, __, o1w),
                PQLFactory.ARRAY_LOOKUP_Byte(o1r, i6w, (i0r | VAR_CONST_FLAG) ),
                PQLFactory.INT_RANGE_CONTAINS( (i12r | VAR_CONST_FLAG), (i13r | VAR_CONST_FLAG), i6r),
                PQLFactory.ADD_Int(i6r, (i1r | VAR_CONST_FLAG), i8w),
                PQLFactory.ARRAY_LOOKUP_Byte(o1r, i8r, (i2r | VAR_CONST_FLAG)),
                PQLFactory.ADD_Int(i6r, (i3r | VAR_CONST_FLAG), i10w),
                PQLFactory.ARRAY_LOOKUP_Byte(o1r, i10r, (i4r | VAR_CONST_FLAG))
                );

        /*Method method = null;
        for (int i=0; i<RuntimeCreatorBench.class.getMethods().length; i++) {
            if (RuntimeCreatorBench.class.getMethods()[i].getName().contains("myFunction"))
                method = RuntimeCreatorBench.class.getMethods()[i];
        }

        env.v_int[1] = 0;

        env.v_int[2] = 0;
        env.v_int[3] = 99;
        env.v_int[4] = 5;
        env.v_int[5] = 42;

        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, i0r, i1w)},
                PQLFactory.ConjunctiveBlock(
                    PQLFactory.INT_RANGE_CONTAINS( i2r, i3r, i0w),
                    PQLFactory.ADD_Int(i0r, i4r, i0w),
                    PQLFactory.EQ_Int(i0r, i5r) )
                );*/

        return p;
    }

    @Test
    public Join webgraph (Env env) {
        /*webgraph:
   Reduce[SET(?o0): !o8]: { CONTAINS(?o1; !o0);  Field(?o0; !o3);
   CONTAINS(?o3; !o2);  Field(?o2; !o5);  Field(?o5; !o6);
   CONTAINS(?o6; !o4);  Field(?o4; ?o0); }
 Ausgabe: o8*/

        this.env = env;
        final int o6r = Env.encodeReadVar(TYPE_OBJECT, 6);
        final int o6w = Env.encodeWriteVar(TYPE_OBJECT, 6);
        final int o8w = Env.encodeWriteVar(TYPE_OBJECT, 8);

        env.v_object[0] = new Webdoc (0);
        env.v_object[1] = benchmarks.webgraph.Generator.documents;
        env.v_object[2] = new Link (new Webdoc (0), new Webdoc (0));
        env.v_object[3] = new HashSet();
        env.v_object[4] = new Link (new Webdoc (0), new Webdoc (0));
        env.v_object[5] = new Webdoc (0);
        env.v_object[6] = new HashSet ();
        env.v_object[8] = new PSet();

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(o0r, o8w)},
                PQLFactory.ConjunctiveBlock( PQLFactory.CONTAINS( (o1r | VAR_CONST_FLAG), o0w),
                PQLFactory.FIELD(Webdoc.class, "outlinks", o0r, o3w),
                PQLFactory.CONTAINS(o3r, o2w),
                PQLFactory.FIELD(Link.class, "destination", o2r, o5w),
                PQLFactory.FIELD(Webdoc.class, "outlinks", o5r, o6w),
                PQLFactory.CONTAINS(o6r, o4w),
                PQLFactory.FIELD(Link.class, "destination", o4r, o0r))
                );

        return p;
    }

    public Join setnested (Env env) {
        /*Reduce[SET(?i1): !o2]: { CONTAINS(?o0; !i1);  LT_Int(?i1, ?i0); CONTAINS(?o1; ?i1); }*/

        this.env = env;

        env.v_object[0] = generatedSet[0];
        env.v_object[1] = generatedSet[1];
        env.v_int[0] = 10;

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o2w)},
                PQLFactory.ConjunctiveBlock( PQLFactory.CONTAINS( (o0r | VAR_CONST_FLAG), i1w),
                PQLFactory.LT_Int(i1r, (i0r | VAR_CONST_FLAG) ),
                PQLFactory.CONTAINS( (o1r | VAR_CONST_FLAG), i1r))
                );

        return p;

    }

    public Join mapnested (Env env) {

        this.env = env;

        env.v_object[0] = generatedMap[0];
        env.v_object[1] = generatedMap[1];
        env.v_int[0] = 10;
        env.v_int[2] = 0;
        env.v_int[3] = 0;
        env.v_int[4] = 9;
        env.v_int[5] = 999;
        env.v_int[6] = 0;

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o2w)},
                PQLFactory.ConjunctiveBlock(PQLFactory.INT_RANGE_CONTAINS((i6r | VAR_CONST_FLAG), (i5r | VAR_CONST_FLAG), i2w),
                PQLFactory.LOOKUP( o0r, i2r, i1w),
                PQLFactory.LT_Int(i1r, (i0r | VAR_CONST_FLAG) ),
                PQLFactory.INT_RANGE_CONTAINS((i6r | VAR_CONST_FLAG), (i4r | VAR_CONST_FLAG), i3w),
                PQLFactory.LOOKUP(o1r, i3r, i1r))
                );

        return p;

    }

    public Join arraynested (Env env) {

        this.env = env;

        env.v_object[0] = generatedArray.get(0);
        env.v_object[1] = generatedArray.get(1);
        env.v_int[0] = 10;

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o2w)},
                PQLFactory.ConjunctiveBlock( PQLFactory.ARRAY_LOOKUP_Int( o0r, __, i1w),
                PQLFactory.LT_Int(i1r, (i0r | VAR_CONST_FLAG) ),
                PQLFactory.ARRAY_LOOKUP_Int( o1r, __, i1r))
                );

        return p;

    }

    public void compute(Join j) {
        checkTrue(j);
    }

    public Env getEnv() {
        return env;
    }

    // deferred initialisation; see *_INIT below
    public static PSet <Integer> [] generatedSet = null;
    public static PMap <Integer, Integer> [] generatedMap = null;
    public static List <int[]> generatedArray = null;

    public static PSet <Integer> [] generateSet (int size) {

        int factor = BenchmarkOptions.factor;
        generatedSet = (PSet<Integer>[]) new PSet [2];
        Random randomSet = new Random();
        randomSet.setSeed(12345);
        for (int i=0; i<2; i++) {
	    PSet <Integer> newSet = new PSet <Integer> ();

	    final int total_size = ( ((i==0 && !BenchmarkOptions.dataSwap) || (i == 1 && BenchmarkOptions.dataSwap)) ? 1000 * size : 1)*factor;
	    ArrayList<Integer> ali = new ArrayList<Integer>(total_size);

	    final int increment_range = 1000000000 / total_size;
	    int pos = 0;

	    //zuerst ein grosses, dann ein kleines Set
	    for (int j=0; j < total_size; j++ ) {
		ali.add(pos += 1 + randomSet.nextInt(increment_range));
	    }

	    // order of insertion affects internal set representation
	    Collections.shuffle(ali);
	    for (Integer k : ali) {
		newSet.add(k);
	    }

	    generatedSet[i] = newSet;
        }
        return generatedSet;
    }

    public static PMap <Integer, Integer> [] generateMap (int size) {

        int factor = BenchmarkOptions.factor;
        generatedMap = (PMap<Integer,Integer>[]) new PMap [2];
        Random randomMap = new Random();
        long seed = 1380055649401L;
        randomMap.setSeed(seed);
        for (int i=0; i<2; i++) {
             PMap <Integer, Integer> newMap = new PMap <Integer, Integer> ();

            //zuerst eine grosse, dann eine kleine Map
            for (int j=0; j< (((i==0 && !BenchmarkOptions.dataSwap) || (i == 1 && BenchmarkOptions.dataSwap)) ? size : 1)*factor; j++ )
                newMap.put(j, randomMap.nextInt(100000000));

            generatedMap[i] = newMap;
        }
        return generatedMap;
    }

    public static List <int[]> generateArray (int size) {

        int factor = BenchmarkOptions.factor;
        generatedArray = new ArrayList <int[]> ();
        Random randomArray = new Random();
        randomArray.setSeed(12345);
        for (int i=0; i<2; i++) {
            //zuerst ein grosses, dann ein kleines Array
            int arrayLength = (((i==0 && !BenchmarkOptions.dataSwap) || (i == 1 && BenchmarkOptions.dataSwap)) ? 1000 * size : 1)*factor;

            int [] newArray = new int [arrayLength];

            for (int j=0; j<arrayLength; j++ )
                newArray[j] = randomArray.nextInt(1000000);

            generatedArray.add(newArray);
        }
        return generatedArray;
    }

    public static final benchmarks.BenchmarkInitInterface SET_INIT =
	new benchmarks.BenchmarkInitInterface() {
	    @Override
	    public void
	    init(int size) {
		generatedSet = generateSet(size);
	    }
    };


    public static final benchmarks.BenchmarkInitInterface MAP_INIT =
	new benchmarks.BenchmarkInitInterface() {
	    @Override
	    public void
	    init(int size) {
		generatedMap = generateMap(size);
	    }
    };


    public static final benchmarks.BenchmarkInitInterface ARRAY_INIT =
	new benchmarks.BenchmarkInitInterface() {
	    @Override
	    public void
	    init(int size) {
		generatedArray = generateArray(size);
	    }
    };

}
