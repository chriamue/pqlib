/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package replacetool;

import benchmarks.bonus.Bonus;
import benchmarks.bonus.Department;
import benchmarks.bonus.Employee;
import benchmarks.threegrep.Generator;
import edu.umass.pql.AbstractBlock;
import edu.umass.pql.ControlStructure;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reduction;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import static edu.umass.pql.VarConstants.__;
import edu.umass.pql.VarInfo;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.ReductionImpl;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import rc2.ExtensionInterface;
import rc2.RCInterface;
import rc2.bcr.BCRAccessMode;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;
import replacetool.AbstractRule.*;
import replacetool.AbstractRule.Command;
import replacetool.AbstractRule.EnvContentType;
import replacetool.AbstractRule.RuleType;
import replacetool.SimpleRules.RangeContainsRule;

/**
 * binary replacing tool for given input join
 * rules can be set through inheriting from AbstractRule
 *
 * @author Hilmar
 */
public class ReplaceTool {

    private int randomSize = 16;

    /**
     * constructor
     *
     * @param extensions all extensions used in input-join
     * @throws RuntimeCreatorException exception while registering extension
     */
    public ReplaceTool (ExtensionInterface ... extensions) throws RuntimeCreatorException {
        ExtensionInterface.registerExtensions(extensions);
    }

    /**
     * start the binary replacing here
     *
     * @param input input-join
     * @param env the environment (used for constant values)
     * @param rules replacing-and-merging rules
     * @return the ReplaceResult contains the new join and the new environment
     */
    public ReplaceResult replace(Join input, Env env, Iterable <AbstractRule> rules) {
        return replace(input, calculateConstantVarInfo(input), env, rules, null);
    }

    private ReplaceResult replace(Join input, VarInfo varInfo, Env env, Iterable <AbstractRule> rules, Random determinedSelection) {
        CommandSplit cmdSplit = new CommandSplit();
        cmdSplit.doSplit(input);

        List <PossibleMerge> possibleMerges = new ArrayList ();

        for (AbstractRule rule : rules) {
            if (rule.getRuleType() == RuleType.JoinJoin) {
                for (List <Join> joinBlock : cmdSplit.joinsPerBlock) {
                    for (Join firstJoin : joinBlock)
                        if (rule.isLeftCommand(new Command(firstJoin, varInfo, env)))
                            for (Join secondJoin : joinBlock)
                                if (secondJoin != firstJoin && rule.canReplace(new Command(firstJoin, varInfo, env), new Command(secondJoin, varInfo, env)))
                                    possibleMerges.add(new PossibleMerge(rule, new Command(firstJoin, varInfo, env), new Command(secondJoin, varInfo, env), rule.replaceCommands(new Command(firstJoin, varInfo, env), new Command(secondJoin, varInfo, env))));
                }
            } else if (rule.getRuleType() == RuleType.ReductorJoin) {
                for (Reductor reductor : cmdSplit.joinsOfReductor.keySet())
                    if (rule.isLeftCommand(new Command(reductor, varInfo, env)))
                        for (Join join : cmdSplit.joinsOfReductor.get(reductor))
                            if (rule.canReplace(new Command(reductor, varInfo, env), new Command(join, varInfo, env)))
                                possibleMerges.add(new PossibleMerge(rule, new Command(reductor, varInfo, env), new Command(join, varInfo, env), rule.replaceCommands(new Command(reductor, varInfo, env), new Command(join, varInfo, env))));
            } else if (rule.getRuleType() == RuleType.ReductorReductor) {
                for (List <Reductor> reductorList : cmdSplit.reductorSets)
                    if (rule.isLeftCommand(new Command(reductorList.get(0), varInfo, env)))
                        for (int i=1; i<reductorList.size(); i++)
                            if (rule.canReplace(new Command(reductorList.get(0), varInfo, env), new Command(reductorList.get(i), varInfo, env)))
                                possibleMerges.add(new PossibleMerge(rule, new Command(reductorList.get(0), varInfo, env), new Command(reductorList.get(i), varInfo, env), rule.replaceCommands(new Command(reductorList.get(0), varInfo, env), new Command(reductorList.get(i), varInfo, env))));
            }
        }

        if (determinedSelection != null) {
            int selection = determinedSelection.nextInt(possibleMerges.size()+1);
            if (selection == 0)
                return new ReplaceResult(input.copyRecursively(), env.copy());
            else
                return new ReplaceResult(merge(input,possibleMerges.get(selection-1)), mergeEnv(possibleMerges.get(selection-1)));
        } else {
            Random globalRandom = new Random();
            Join [] merging = new Join [possibleMerges.size()+1];
            Env [] mergedEnvs = new Env [possibleMerges.size()+1];
            VarInfo [] mergedVarInfos = new VarInfo [possibleMerges.size()+1];
            merging[0] = input.copyRecursively();
            mergedEnvs[0] = env.copy();
            mergedVarInfos[0] = varInfo;
            for (int i=1; i<merging.length; i++) {
                merging[i] = merge(input,possibleMerges.get(i-1));
                mergedEnvs[i] = mergeEnv(possibleMerges.get(i-1));
                mergedVarInfos[i] = calculateConstantVarInfo(merging[i]);
            }

            double lowestCost = Double.MAX_VALUE;
            int lowestCostWhere = -1;
            for (int i=0; i<merging.length; i++) {
                double averageCost = 0;
                for (int j=0; j<randomSize; j++) {
                    Random localRandom = new Random(globalRandom.nextLong());
                    Join tryMerge = replace(merging[i], mergedVarInfos[i], mergedEnvs[i], rules, localRandom).getJoin();
                    averageCost += tryMerge.getFullCost(mergedVarInfos[i], mergedEnvs[i]);
                }
                averageCost /= randomSize;
                if (averageCost < lowestCost) {
                    lowestCost = averageCost;
                    lowestCostWhere = i;
                }
            }

            if (lowestCostWhere == 0)
                return new ReplaceResult(merging[0], mergedEnvs[0]);
            else
                return new ReplaceResult(replace(merging[lowestCostWhere], mergedVarInfos[lowestCostWhere], mergedEnvs[lowestCostWhere], rules, null).getJoin(), mergedEnvs[lowestCostWhere]);
        }
    }

    private Env mergeEnv (PossibleMerge possibleMerge) {
        Map <EnvContentType, List <Object> > envExtension = possibleMerge.fromRule.getEnvExtension(possibleMerge.fromLeft, possibleMerge.fromRight);
        Env copiedEnv = possibleMerge.fromLeft.getEnv().copy();
        if (envExtension.containsKey(EnvContentType.Int)) {
            int [] oldInts = copiedEnv.v_int;
            copiedEnv.v_int = new int [oldInts.length + envExtension.get(EnvContentType.Int).size()];
            System.arraycopy(oldInts, 0, copiedEnv.v_int, 0, oldInts.length);
            for (int i=0; i<envExtension.get(EnvContentType.Int).size(); i++)
                copiedEnv.v_int[i + oldInts.length] = (Integer)envExtension.get(EnvContentType.Int).get(i);
        }
        if (envExtension.containsKey(EnvContentType.Long)) {
            long [] oldLongs = copiedEnv.v_long;
            copiedEnv.v_long = new long [oldLongs.length + envExtension.get(EnvContentType.Long).size()];
            System.arraycopy(oldLongs, 0, copiedEnv.v_long, 0, oldLongs.length);
            for (int i=0; i<envExtension.get(EnvContentType.Long).size(); i++)
                copiedEnv.v_long[i + oldLongs.length] = (Long)envExtension.get(EnvContentType.Long).get(i);
        }
        if (envExtension.containsKey(EnvContentType.Double)) {
            double [] oldDoubles = copiedEnv.v_double;
            copiedEnv.v_double = new double [oldDoubles.length + envExtension.get(EnvContentType.Double).size()];
            System.arraycopy(oldDoubles, 0, copiedEnv.v_double, 0, oldDoubles.length);
            for (int i=0; i<envExtension.get(EnvContentType.Double).size(); i++)
                copiedEnv.v_double[i + oldDoubles.length] = (Double)envExtension.get(EnvContentType.Double).get(i);
        }
        if (envExtension.containsKey(EnvContentType.Object)) {
            Object [] oldObjects = copiedEnv.v_object;
            copiedEnv.v_object = new Object [oldObjects.length + envExtension.get(EnvContentType.Object).size()];
            System.arraycopy(oldObjects, 0, copiedEnv.v_object, 0, oldObjects.length);
            for (int i=0; i<envExtension.get(EnvContentType.Object).size(); i++)
                copiedEnv.v_object[i + oldObjects.length] = envExtension.get(EnvContentType.Object).get(i);
        }
        return copiedEnv;
    }

    private static class IndexTypeKey {
        private final int index;
        private final int type;

        private IndexTypeKey (int _index, int _type) {index = _index; type=_type;}
        @Override public boolean equals (Object other) { return (((IndexTypeKey)other).index == index && ((IndexTypeKey)other).type == type); }
        @Override public int hashCode () { return index; }
    }

    /**
     * use this to calculate a varInfo containing a constant var-set automatically
     *
     * @param input the join
     * @return a VarInfo, containing all constant variables
     */
    public static VarInfo calculateConstantVarInfo (Join input) {
        return new VarInfo(calculateConstantSet(input));
    }

    /**
     * use this to calculate a constant var-set automatically
     *
     * @param input the join
     * @return a VarSet, containing all constant variables
     */
    public static VarSet calculateConstantSet (Join input) {

        VarSet constantVars = new VarSet();
        Map <IndexTypeKey, Boolean> variables = new HashMap();
        Join current = input;
        Stack <Join> next = new Stack ();
        while (true) {
            if (current instanceof AbstractBlock) {
                Join [] childs = ((AbstractBlock)current).getComponents();
                for (int j=childs.length-1; j>=0; j--)
                    next.push(childs[j]);
            } else if (current instanceof ReductionImpl) {
                Join endJoin = ((ReductionImpl)current).getComponentInternal(0);
                next.push(endJoin);
                for (Reductor reductor : ((ReductionImpl)current).getReductors())
                    for (int i=0; i<reductor.getArgsNr(); i++) {
                        if (Env.isWildcard(reductor.getArg(i)))
                            continue;
                        IndexTypeKey key = new IndexTypeKey(Env.varIndex(reductor.getArg(i)), Env.varType(reductor.getArg(i)));
                        if (!variables.containsKey(key))
                            variables.put(key, Boolean.FALSE);
                        if (Env.isWriteVar(reductor.getArg(i)))
                            variables.put(key, Boolean.TRUE);
                    }
            } else {
                for (int i=0; i<current.getArgsNr(); i++) {
                    if (Env.isWildcard(current.getArg(i)))
                        continue;
                    IndexTypeKey key = new IndexTypeKey(Env.varIndex(current.getArg(i)), Env.varType(current.getArg(i)));
                    if (!variables.containsKey(key))
                        variables.put(key, Boolean.FALSE);
                    if (Env.isWriteVar(current.getArg(i)))
                        variables.put(key, Boolean.TRUE);
                }
            }

            if (next.size() == 0)
                break;
            current = next.pop();
        }

        for (IndexTypeKey key : variables.keySet())
            if (!variables.get(key))
                constantVars.insert(Env.encodeReadVar(key.type, key.index));

        return constantVars;
    }

    private Join merge (Join input, PossibleMerge mergeInfo) {
        if (mergeInfo.fromLeft.isReductor() && !mergeInfo.to.isReductor()) {
            Join newJoin = changeSubJoin(input, mergeInfo.fromRight, mergeInfo.to);
            return changeSubJoin(newJoin, new Command(translateOldInNew(input, newJoin, mergeInfo.fromLeft.getJoin()), mergeInfo.fromLeft.getVarInfo(), mergeInfo.fromLeft.getEnv()), null);
        } else {
            Join newJoin = changeSubJoin(input, mergeInfo.fromLeft, mergeInfo.to);
            return changeSubJoin(newJoin, new Command(translateOldInNew(input, newJoin, mergeInfo.fromRight.getJoin()), mergeInfo.fromRight.getVarInfo(), mergeInfo.fromRight.getEnv()), null);
        }
    }

    private Join translateOldInNew (Join oldJoin, Join newJoin, Join subJoin) {
        Join currentNew = newJoin, currentOld = oldJoin;
        Stack <Join> nextNew = new Stack (), nextOld = new Stack();
        while (true) {

            if (currentOld == subJoin)
                return currentNew;

            if (currentOld instanceof AbstractBlock) {
                Join [] childs = ((AbstractBlock)currentOld).getComponents();
                for (int j=childs.length-1; j>=0; j--)
                    nextOld.push(childs[j]);
                childs = ((AbstractBlock)currentNew).getComponents();
                for (int j=childs.length-1; j>=0; j--)
                    nextNew.push(childs[j]);
            } else if (currentOld instanceof ReductionImpl) {
                Join endJoin = ((ReductionImpl)currentOld).getComponentInternal(0);
                nextOld.push(endJoin);
                endJoin = ((ReductionImpl)currentNew).getComponentInternal(0);
                nextNew.push(endJoin);
            }

            if (nextOld.size() == 0)
                break;
            currentOld = nextOld.pop();
            currentNew = nextNew.pop();
        }

        return null;
    }

    private Join changeSubJoin (Join input, Command from, Command to) {

        Join newJoin = input.copyRecursively();
        Join currentNew = newJoin, currentOld = input;
        Stack <Join> nextNew = new Stack (), nextOld = new Stack();
        while (true) {

            if (currentOld == from.getJoin() && to == null && !from.isReductor()) {
                removeSubJoin(newJoin, currentNew);
                return newJoin;
            }

            if (currentOld instanceof AbstractBlock) {
                Join [] childs = ((AbstractBlock)currentOld).getComponents();
                for (int j=childs.length-1; j>=0; j--) {
                    if (!from.isReductor() && to != null && childs[j] == from.getJoin()) {
                        ((AbstractBlock)currentNew).setComponentInternal(j, to.getJoin());
                        return newJoin;
                    }
                    nextOld.push(childs[j]);
                }
                childs = ((AbstractBlock)currentNew).getComponents();
                for (int j=childs.length-1; j>=0; j--)
                    nextNew.push(childs[j]);
            } else if (currentOld instanceof ReductionImpl) {
                Join endJoin = ((ReductionImpl)currentOld).getComponentInternal(0);
                nextOld.push(endJoin);
                endJoin = ((ReductionImpl)currentNew).getComponentInternal(0);
                nextNew.push(endJoin);
                Reductor [] reductors = ((ReductionImpl)currentOld).getReductors();
                if (from.isReductor()) {
                    for (int i=0; i<reductors.length; i++)
                        if (reductors[i] == from.getReductor())
                            ((ReductionImpl)currentNew).removeReductor(((ReductionImpl)currentNew).getReductors()[i]);
                    if (to != null)
                        ((ReductionImpl)currentNew).addReductor(to.getReductor());
                    if (((ReductionImpl)currentNew).getReductors().length == 0)
                        removeSubJoin(newJoin, currentNew);
                    return newJoin;
                }
            }

            if (nextOld.size() == 0)
                break;
            currentOld = nextOld.pop();
            currentNew = nextNew.pop();
        }

        return newJoin;

    }

    private void removeSubJoin (Join input, Join toRemove) {

        Join current = input;
        Stack <Join> next = new Stack ();
        while (true) {
            if (current instanceof AbstractBlock) {
                Join [] childs = ((AbstractBlock)current).getComponents();
                for (int j=childs.length-1; j>=0; j--) {
                    if (childs[j] == toRemove) {
                        if (toRemove instanceof ReductionImpl)
                            ((AbstractBlock)current).setComponentInternal(j, ((ReductionImpl)toRemove).getComponentInternal(0));
                        else
                            ((AbstractBlock)current).removeComponentInternal(j);
                        if (((AbstractBlock)current).getComponents().length == 0)
                            removeSubJoin(input, current);
                        return;
                    }
                    next.push(childs[j]);
                }
            } else if (current instanceof ReductionImpl) {
                Join endJoin = ((ReductionImpl)current).getComponentInternal(0);
                next.push(endJoin);
            }

            if (next.size() == 0)
                break;
            current = next.pop();
        }

    }

    public static void main (String [] args) {

        (new benchmarks.threegrep.Generator()).init(100);

        final int i12r = Env.encodeReadVar(TYPE_INT, 12);
        final int i13r = Env.encodeReadVar(TYPE_INT, 13);
        final int i8r = Env.encodeReadVar(TYPE_INT, 8);
        final int i8w = Env.encodeWriteVar(TYPE_INT, 8);
        final int i10r = Env.encodeReadVar(TYPE_INT, 10);
        final int i10w = Env.encodeWriteVar(TYPE_INT, 10);
        Join p = null;
        try {
            ReplaceTool replaceTool = new ReplaceTool();
            p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(TestBase.o1r, TestBase.o4w)},
                    PQLFactory.ARRAY_LOOKUP_Object( TestBase.o2r, __, TestBase.o1w),
                    PQLFactory.ARRAY_LOOKUP_Byte(TestBase.o1r, TestBase.i6w, (TestBase.i0r ) ),
                    //new PQLExtension("prime_range", (i12r ), (i13r ), TestBase.i6w),
                    PQLFactory.INT_RANGE_CONTAINS( (i12r ), (i13r ), TestBase.i6w),
                    PQLFactory.INT_RANGE_CONTAINS( (TestBase.i3r ), (TestBase.i1r), TestBase.i6r),
                    PQLFactory.ADD_Int(TestBase.i6r, (TestBase.i1r ), i8w),
                    PQLFactory.ARRAY_LOOKUP_Byte(TestBase.o1r, i8r, (TestBase.i2r)),
                    PQLFactory.ADD_Int(TestBase.i6r, (TestBase.i3r ), i10w),
                    PQLFactory.ARRAY_LOOKUP_Byte(TestBase.o1r, i10r, (TestBase.i4r ))
                    );

            Env env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                    new Object[] {
                            new int[14],
                            new long[5],
                            new double[3],
                            new Object[6]
                    });

            env.v_object[1] = new byte [1];
            env.v_object[2] = Generator.data_array;
            env.v_object[4] = new PSet ();
            env.v_int[0] = 48;
            env.v_int[2] = 49;
            env.v_int[4] = 50;
            env.v_int[1] = 1;
            env.v_int[3] = 2;
            env.v_int[12] = 0;
            env.v_int[13] = Generator.RECORD_SIZE - 3;

            Set <AbstractRule> rules = new HashSet ();
            rules.add(new RangeContainsRule());
            ReplaceResult res = replaceTool.replace(p, env, rules);
            System.out.println(p + "\n\n\n\n==================\n\n" + env + "\n\n\n\n********************\n\n");
            System.out.println(res.getJoin() + "\n\n\n\n==================\n\n" + res.getEnv() + "\n\n\n\n********************\n\n");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class CommandSplit {
        //joins, which are in same conjunctive-block
        private List <List <Join> > joinsPerBlock;
        //joins, which are inside one reductor
        private Map <Reductor, List <Join> > joinsOfReductor;
        //nested reductors
        private List <List <Reductor> > reductorSets;

        private void doSplit(Join input) {

            Map <Reductor, Join> reductorEndsWith = new HashMap ();

            joinsPerBlock = new ArrayList ();
            joinsOfReductor = new HashMap ();
            reductorSets = new ArrayList ();
            Join current = input;
            Stack <Join> next = new Stack ();
            while (true) {
                if (current instanceof AbstractBlock) {
                    Join [] childs = ((AbstractBlock)current).getComponents();
                    for (int j=childs.length-1; j>=0; j--)
                        next.push(childs[j]);

                    joinsPerBlock.add(new ArrayList());
                    for (int j=0; j<childs.length; j++)
                        if (!(childs[j] instanceof ControlStructure))
                            joinsPerBlock.get(joinsPerBlock.size()-1).add(childs[j]);
                    if (joinsPerBlock.get(joinsPerBlock.size()-1).size() == 0)
                        joinsPerBlock.remove(joinsPerBlock.size()-1);
                } else if (current instanceof ReductionImpl) {
                    Join endJoin = ((ReductionImpl)current).getComponentInternal(0);
                    next.push(endJoin);
                    while (endJoin instanceof ControlStructure) {
                        if (endJoin instanceof AbstractBlock) {
                            Join [] childsEndJoin = ((AbstractBlock)endJoin).getComponents();
                            if (childsEndJoin.length == 0)
                                break;
                            else
                                endJoin = childsEndJoin[childsEndJoin.length-1];
                        } else if (endJoin instanceof ReductionImpl)
                            endJoin = ((ReductionImpl)endJoin).getComponentInternal(0);
                    }
                    Reductor [] reductors = ((ReductionImpl)current).getReductors();
                    List <List <Reductor> > sortedRedctors = new ArrayList ();

                    for (Reductor reductor : reductors) {
                        joinsOfReductor.put(reductor, new ArrayList ());
                        List <Reductor> nextSortedReductors = new ArrayList ();
                        nextSortedReductors.add(reductor);
                        for (Reductor reductor2 : reductors)
                            if (reductor2 != reductor)
                                nextSortedReductors.add(reductor2);
                        sortedRedctors.add(nextSortedReductors);
                        reductorSets.add(nextSortedReductors);
                    }

                    for (Reductor stillOpen : reductorEndsWith.keySet())
                        for (List <Reductor> reductorList : reductorSets)
                            if (reductorList.get(0) == stillOpen)
                                reductorList.addAll(sortedRedctors.get(0));

                    for (Reductor reductor : reductors)
                        reductorEndsWith.put(reductor, endJoin);
                } else {
                    for (Reductor stillOpen : reductorEndsWith.keySet())
                        joinsOfReductor.get(stillOpen).add(current);
                }

                Set <Reductor> toRemoveSet = new HashSet();
                for (Reductor stillOpen : reductorEndsWith.keySet())
                    if (reductorEndsWith.get(stillOpen) == current)
                        toRemoveSet.add(stillOpen);
                for (Reductor toRemove : toRemoveSet)
                    reductorEndsWith.remove(toRemove);
                
                if (next.size() == 0)
                    break;
                current = next.pop();
            }
        }

        @Override
        public String toString () {
            return "[CommandSplit]\n" + joinsPerBlock.toString() + "\n" + joinsOfReductor.toString() + "\n" + reductorSets.toString();
        }
    }

    private static class PossibleMerge {

        private final AbstractRule fromRule;
        private final Command fromLeft;
        private final Command fromRight;
        private final Command to;

        private PossibleMerge (AbstractRule _fromRule, Command _fromLeft, Command _fromRight, Command _to) {
            fromRule = _fromRule;
            fromLeft = _fromLeft;
            fromRight = _fromRight;
            to = _to;
        }
    }

    public static class ReplaceResult {

        private final Env env;
        private final Join join;

        private ReplaceResult (Join _join, Env _env) {
            env = _env;
            join = _join;
        }

        public Env getEnv () {
            return env;
        }

        public Join getJoin () {
            return join;
        }

    }

    public static class ReplaceToolException extends Exception {
        public ReplaceToolException(String error) {
            super(error);
        }
    }

    public static class ReplaceToolRuntimeException extends RuntimeException {
        public ReplaceToolRuntimeException(String error) {
            super(error);
        }
    }

}
