/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package replacetool;

import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.Reductor;
import edu.umass.pql.VarInfo;
import java.util.List;
import java.util.Map;
import replacetool.ReplaceTool.ReplaceToolRuntimeException;

/**
 *
 * @author Hilmar
 */
public abstract class AbstractRule {

    public enum RuleType {JoinJoin, ReductorJoin, ReductorReductor};
    public enum EnvContentType {Int, Long, Double, Object};

    abstract public RuleType getRuleType ();
    abstract public boolean isLeftCommand (Command command);
    abstract public boolean canReplace (Command left, Command right);
    abstract public Command replaceCommands (Command left, Command right);
    abstract public Map <EnvContentType, List <Object> > getEnvExtension (Command left, Command right);

    public static class Command {

        private final Join join;
        private final Reductor reductor;
        private final VarInfo varInfo;
        private final Env env;

        public Command (Join _join, VarInfo _varInfo, Env _env) {
            join = _join;
            reductor = null;
            varInfo = _varInfo;
            env = _env;
        }

        public Command (Reductor _reductor, VarInfo _varInfo, Env _env) {
            join = null;
            reductor = _reductor;
            varInfo = _varInfo;
            env = _env;
        }

        public boolean isReductor() {
            return (reductor != null);
        }

        public Join getJoin ()  {
            if (join == null)
                throw new ReplaceToolRuntimeException("the command is no join.");
            return join;
        }

        public Reductor getReductor ()  {
            if (reductor == null)
                throw new ReplaceToolRuntimeException("the command is no reductor.");
            return reductor;
        }

        public VarInfo getVarInfo() {
            return varInfo;
        }

        public Env getEnv() {
            return env;
        }

    }

}
