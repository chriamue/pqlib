/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package replacetool;

import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.VarConstants;
import edu.umass.pql.il.Container.INT_RANGE_CONTAINS;
import edu.umass.pql.il.Container.LONG_RANGE_CONTAINS;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Hilmar
 */
public final class SimpleRules {

    public static class RangeContainsRule extends AbstractRule implements VarConstants {

        @Override
        public RuleType getRuleType() {
            return RuleType.JoinJoin;
        }

        @Override
        public boolean isLeftCommand(Command command) {
            return ( (command.getJoin() instanceof INT_RANGE_CONTAINS || command.getJoin() instanceof LONG_RANGE_CONTAINS)
                    && !Env.isWildcard(((Join.BI3)command.getJoin()).getArg(0))&& !Env.isWildcard(((Join.BI3)command.getJoin()).getArg(1))
                    && command.getVarInfo().isConstant(((Join.BI3)command.getJoin()).getArg(0)) && command.getVarInfo().isConstant(((Join.BI3)command.getJoin()).getArg(1)) );
        }

        @Override
        public boolean canReplace(Command left, Command right) {
            return ( (right.getJoin() instanceof INT_RANGE_CONTAINS || right.getJoin() instanceof LONG_RANGE_CONTAINS)
                    && !Env.isWildcard(((Join.BI3)right.getJoin()).getArg(0))&& !Env.isWildcard(((Join.BI3)right.getJoin()).getArg(1))
                    && right.getVarInfo().isConstant(((Join.BI3)right.getJoin()).getArg(0)) && right.getVarInfo().isConstant(((Join.BI3)right.getJoin()).getArg(1))
                    && Env.varIndex( ((Join.BI3)left.getJoin()).getArg(2) ) == Env.varIndex( ((Join.BI3)right.getJoin()).getArg(2) )
                    && Env.varType(((Join.BI3)left.getJoin()).getArg(2) ) == Env.varType( ((Join.BI3)right.getJoin()).getArg(2) ) );
        }

        @Override
        public Command replaceCommands(Command left, Command right) {
            Join join = null;
            int resultVar = ((Join.BI3)left.getJoin()).getArg(2);
            if (!Env.isWriteVar(resultVar))
                resultVar = ((Join.BI3)right.getJoin()).getArg(2);
            if (right.getJoin() instanceof INT_RANGE_CONTAINS && left.getJoin() instanceof INT_RANGE_CONTAINS)
                join = new INT_RANGE_CONTAINS( Env.encodeReadVar(TYPE_INT, left.getEnv().v_int.length), Env.encodeReadVar(TYPE_INT, left.getEnv().v_int.length+1), resultVar);
            else
                join = new LONG_RANGE_CONTAINS( Env.encodeReadVar(TYPE_LONG, left.getEnv().v_long.length), Env.encodeReadVar(TYPE_LONG, left.getEnv().v_long.length+1), resultVar);
            return new Command(join, left.getVarInfo(), left.getEnv());
        }

        @Override
        public Map<EnvContentType, List<Object>> getEnvExtension(Command left, Command right) {
            Map<EnvContentType, List<Object>> returnMap = new HashMap ();
            List <Object> value = new ArrayList ();
            if (right.getJoin() instanceof INT_RANGE_CONTAINS && left.getJoin() instanceof INT_RANGE_CONTAINS) {
                value.add(new Integer( Math.max( left.getEnv().getInt( ((Join.BI3)left.getJoin()).getArg(0) ), left.getEnv().getInt( ((Join.BI3)right.getJoin()).getArg(0) )) ));
                value.add(new Integer( Math.min( left.getEnv().getInt( ((Join.BI3)left.getJoin()).getArg(1) ), left.getEnv().getInt( ((Join.BI3)right.getJoin()).getArg(1) )) ));
                returnMap.put( EnvContentType.Int, value);
            } else {
                value.add(new Long( Math.max( left.getEnv().getLong( ((Join.BI3)left.getJoin()).getArg(0) ), left.getEnv().getLong( ((Join.BI3)right.getJoin()).getArg(0) )) ));
                value.add(new Long( Math.min( left.getEnv().getLong( ((Join.BI3)left.getJoin()).getArg(1) ), left.getEnv().getLong( ((Join.BI3)right.getJoin()).getArg(1) )) ));
                returnMap.put( EnvContentType.Long, value);
            }
            return returnMap;
        }


    }

}
