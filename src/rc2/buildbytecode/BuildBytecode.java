/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.buildbytecode;

import static edu.umass.bc.BcFlags.TYPE_LOAD;
import static edu.umass.bc.BcFlags.TYPE_STORE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.CHECKCAST;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.IRETURN;
import static org.objectweb.asm.Opcodes.RETURN;
import static org.objectweb.asm.Opcodes.V1_6;
import org.objectweb.asm.util.TraceClassVisitor;
import rc2.DebugMethodVisitor;
import rc2.FlagUtils;
import rc2.RCInterface;
import rc2.SubCommands;
import rc2.optimizer.OptimizeVisitor;
import rc2.representation.PQLArgument;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BuildBytecode {

    private static Map <String, Label> labelMap;

    public static byte [] build (String path, boolean parallel) throws Exception {
        ClassWriter cv;
        ClassVisitor cw = null;
        cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
        cw = cv;
        FieldVisitor fv;
        MethodVisitor mv;
        AnnotationVisitor av0;

        cw.visit(V1_6, ACC_PUBLIC, "Evaluator", null, "java/lang/Object", null);

        cw.visitSource("Evaluator.java", null);

        {
            mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(1, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            mv.visitInsn(RETURN);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLocalVariable("this", "LEvaluator;", null, l0, l1, 0);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        {
            if (!parallel)
                mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "evaluate", "(Ledu/umass/pql/Env;)Z", null, null);
            else
                mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "evaluate", "(Ledu/umass/pql/Env;II)Z", null, null);
            mv.visitCode();

            labelMap = new HashMap <String, Label>();
            BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream(path), "UTF-8"));
            String nextLine;
            while ( (nextLine = in.readLine()) != null)
                writeLine(nextLine, mv);
            in.close();

            mv.visitMaxs(0, 0);
            mv.visitEnd();
        }
        cw.visitEnd();

        byte [] ret = cv.toByteArray();
        return ret;
    }

    private static int fromFlag (String flag) throws Exception {
        boolean ignore = true;
        for (Field field : Opcodes.class.getFields()) {
            if (ignore && field.getName().equals("NOP"))
                ignore = false;
            if (!ignore && field.getName().equals(flag))
                return (Integer)field.get(null);
        }
        return -1;
    }

    private static Label getLabel (String label) {
        if (!labelMap.containsKey(label))
            labelMap.put(label, new Label());
        return labelMap.get(label);
    }

    public static void writeLine(String line, MethodVisitor mv) throws Exception {
        String [] tokens = line.replace(":", "").replace(",", "").split(" ");
        if (tokens[0].equals("visitInsn"))
            mv.visitInsn(fromFlag(tokens[1]));
        else if (tokens[0].equals("visitIntInsn"))
            mv.visitIntInsn(fromFlag(tokens[1]), Integer.parseInt(tokens[2]));
        else if (tokens[0].equals("visitVarInsn"))
            mv.visitVarInsn(fromFlag(tokens[1]), Integer.parseInt(tokens[2]));
        else if (tokens[0].equals("visitTypeInsn"))
            mv.visitTypeInsn(fromFlag(tokens[1]), tokens[2]);
        else if (tokens[0].equals("visitFieldInsn"))
            mv.visitFieldInsn(fromFlag(tokens[1]), tokens[2], tokens[3], tokens[4]);
        else if (tokens[0].equals("visitMethodInsn"))
            mv.visitMethodInsn(fromFlag(tokens[1]), tokens[2], tokens[3], tokens[4], false);
        else if (tokens[0].equals("visitJumpInsn"))
            mv.visitJumpInsn(fromFlag(tokens[1]), getLabel(tokens[2]));
        else if (tokens[0].equals("visitLabel"))
            mv.visitLabel(getLabel(tokens[1]));
        else if (tokens[0].equals("visitLdcInsn"))
            mv.visitLdcInsn(new Integer(Integer.parseInt(tokens[1])));
        else if (tokens[0].equals("visitIincInsn"))
            mv.visitIincInsn( Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
    }
}
