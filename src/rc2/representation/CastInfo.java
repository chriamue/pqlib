/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.representation;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilmar
 */
public class CastInfo {

    private List <String> classNames;
    private List <PQLArgument> arguments;

    public CastInfo () {
        classNames = new ArrayList <String> ();
        arguments = new ArrayList <PQLArgument> ();
    }

    public void add (String className, PQLArgument arg) {
        classNames.add(className);
        arguments.add(arg);
    }

    public boolean containsSubclass (PQLArgument arg, Class checkClass) throws ClassNotFoundException {
        for (int i=0; i<classNames.size(); i++) {
            if (arguments.get(i).equals(arg) && checkClass.isAssignableFrom(Class.forName(classNames.get(i))))
                return true;
        }
        return false;
    }

    public boolean containsWrongSubclass (PQLArgument arg, Class checkClass) throws ClassNotFoundException {
        for (int i=0; i<classNames.size(); i++) {
            if (arguments.get(i).equals(arg) && !checkClass.isAssignableFrom(Class.forName(classNames.get(i))))
                return true;
        }
        return false;
    }
}
