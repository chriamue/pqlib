/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.representation;

import edu.umass.bc.BcFlags;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.*;
import edu.umass.pql.il.ReductionImpl;
import edu.umass.pql.il.Type;
import edu.umass.pql.il.reductor.MethodAdapterReductor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import rc2.FlagUtils;
import rc2.SkeletonBuilder;
import rc2.bcr.BCRExtension;
import rc2.parallel.ParallelExecution;
import rc2.parallel.ParallelMerge;

/**
 *
 * @author Hilmar
 */
public class PQLToken implements BcFlags, VarConstants {

    public int instruction;
    public int type;
    public int additionalOptions;
    public List <Integer> arguments;
    public List <PQLToken> childs;
    public PQLToken innerReductor;
    public Field field;
    public Method method;
    public PQLToken parent;
    public Join fromJoin;
    public int parallelArgIndex;
    public ParallelMerge parallelMerge;
    public boolean isReductor;
    public Class javaType;

    public boolean startsIteration;
    public int parallelModeIndex;
    public boolean codeEntryPoint;
    public PQLReturnBehavior trueBehavior;
    public PQLReturnBehavior falseBehavior;
    public PQLToken jumpBackTo;
    public Object methodDefaultValue;
    public String extensionName;
    public BCRExtension bcrExtension;

    public PQLToken (Join join, PQLToken _parent, SkeletonBuilder skeletonBuilder, Env env) throws RuntimeCreatorException {
        fromJoin = join;
        arguments = new ArrayList <Integer> ();
        childs = new ArrayList <PQLToken> ();
        int flags = join.accept(new JoinFlagsVisitor());
        instruction = (flags & 0xFF00);
        type = flags & 0xFF;
        additionalOptions = (flags & 0xFF0000);

        if (join instanceof PQLExtension) {
            parallelArgIndex = ((PQLExtension)join).getBCRExtension().getParallelArgIndex();
            extensionName = ((PQLExtension)join).getExtensionName();
            bcrExtension = ((PQLExtension)join).getBCRExtension();
        } else
            parallelArgIndex = ParallelExecution.getStandardParallelArgIndex(this);

        if (instruction == TYPE_FIELD) {
            Object tmpField = ((edu.umass.pql.il.Field)join).getField();
            if (!(tmpField instanceof Field))
                throw new RuntimeCreatorException("Field member has to be instance of java.lang.reflect.Field");
            field = ((Field)tmpField);
        }

        //reduction has multiple arguments of the containing reductors => ignore these
        if (instruction != TYPE_REDUCTION) {
            for (int i=0; i<join.getArgsNr(); i++) {
                arguments.add(join.getArg(i));
                skeletonBuilder.argumentInUse(join.getArg(i), env);
            }
        }

        if (instruction == TYPE_CHECK_JAVA_TYPE) {
            Object _javaType = ((Type.JAVA_TYPE)join).getType();
            if (_javaType instanceof Class)
                javaType = (Class)_javaType;
            else
                throw new RuntimeCreatorException("in JAVA_TYPE object of type 'Class' expected");
        }

        if (instruction == TYPE_CONJUCTIVE || instruction == TYPE_DISJUNCTIVE) {

            Join [] elements = (instruction == TYPE_CONJUCTIVE ? ((AbstractBlock.PreConjunctive)join).getComponents() : ((AbstractBlock.PreDisjunctive)join).getComponents());
            if (elements.length == 0)
                throw new RuntimeCreatorException("empty control-structure not allowed.");
            for (Join child : elements)
                childs.add(new PQLToken(child, this, skeletonBuilder, env));
        } else if (instruction == TYPE_REDUCTION) {
            Join body = ((ReductionImpl)join).getComponentInternal(0);
            Reductor[] reductors = ((ReductionImpl)join).getReductors();
            if (reductors.length == 0)
                throw new RuntimeCreatorException("empty reductor not allowed.");
            fromReductor(reductors[0], skeletonBuilder, env);
            PQLToken lastPQLToken = this;
            for (int i=1; i<reductors.length; i++) {
                lastPQLToken.childs.add(new PQLToken(reductors[i], lastPQLToken, skeletonBuilder, env));
                lastPQLToken = lastPQLToken.childs.get(0);
            }
            lastPQLToken.childs.add(new PQLToken(body, lastPQLToken, skeletonBuilder, env));
        }

        parent = _parent;
    }

    private PQLToken (Reductor reductor, PQLToken _parent, SkeletonBuilder skeletonBuilder, Env env) throws RuntimeCreatorException {
        arguments = new ArrayList <Integer> ();
        childs = new ArrayList <PQLToken> ();
        fromReductor(reductor, skeletonBuilder, env);
        parent = _parent;
    }

    private void fromReductor (Reductor reductor, SkeletonBuilder skeletonBuilder, Env env) throws RuntimeCreatorException {
        int flags = reductor.accept(new ReductorFlagsVisitor());
        instruction = (flags & 0xFF00);
        type = flags & 0xFF;
        additionalOptions = (flags & 0xFF0000);
        isReductor = true;

        if (reductor instanceof PQLReductorExtension) {
            parallelMerge = ((PQLReductorExtension)reductor).getBCRExtension().getParallelMerge();
            extensionName = ((PQLReductorExtension)reductor).getExtensionName();
            bcrExtension = ((PQLReductorExtension)reductor).getBCRExtension();
        } else
            parallelMerge = ParallelExecution.getStandardParallelMerge(this);

        if (instruction == REDUCTOR_METHOD_ADAPTER || (reductor instanceof PQLReductorExtension && ((PQLReductorExtension)reductor).getMethod() != null) ) {
            if (instruction == REDUCTOR_METHOD_ADAPTER) {
                method = (Method)(((MethodAdapterReductor)reductor).getReductorObject());
                methodDefaultValue = ((MethodAdapterReductor)reductor).getDefault();
             } else {
                method = ((PQLReductorExtension)reductor).getMethod();
                methodDefaultValue = new Integer(((DefaultValueInt)method.getAnnotation(DefaultValueInt.class)).value());
            }
             if (method.getParameterTypes().length != 2)
                 throw new RuntimeCreatorException("specified method has to have 2 parameters.");
             if (method.getReturnType() != method.getParameterTypes()[0] || method.getReturnType() != method.getParameterTypes()[1])
                 throw new RuntimeCreatorException("return-parameter and arguments of specified method has to be of same type.");
             String methodType = FlagUtils.fieldTypeToName(method.getReturnType().getName());
             if (methodType.startsWith("[") || (methodType.endsWith(";") && !methodType.equals("Ljava/lang/Object;")))
                 throw new RuntimeCreatorException("the argument-type of the specified method is not supported (just primitive types and java.lang.Object suppported)");
        }

        for (int i=0; i<reductor.getArgsNr(); i++) {
            if (additionalOptions == WITH_INNER_REDUCTOR &&
                    ( (instruction == REDUCTOR_MAP && i == 1) || (instruction == REDUCTOR_DEFAULTMAP && i == 2)))
                continue;
            arguments.add(reductor.getArg(i));
            skeletonBuilder.argumentInUse(reductor.getArg(i), env);
        }

        if (additionalOptions == WITH_INNER_REDUCTOR) {

            if (instruction == REDUCTOR_MAP)
                instruction = REDUCTOR_N_MAP;
            else if (instruction == REDUCTOR_DEFAULTMAP)
                instruction = REDUCTOR_N_DEFAULTMAP;

            innerReductor = new PQLToken(reductor.getInnerReductor(), null, skeletonBuilder, env);
            int argIndex = -1;
            if (innerReductor.instruction == REDUCTOR_METHOD_ADAPTER || innerReductor.instruction == REDUCTOR_ARRAY || innerReductor.instruction == REDUCTOR_SET)
                argIndex = 1;
            else if (innerReductor.instruction == REDUCTOR_MAP)
                argIndex = 2;
            else if (innerReductor.instruction == REDUCTOR_DEFAULTMAP)
                argIndex = 3;
            else
                argIndex = 0;
            innerReductor.arguments.set(argIndex, Env.encodeWriteVar(TYPE_OBJECT, env.v_object.length));
            arguments.add(Env.encodeWriteVar(TYPE_OBJECT, env.v_object.length));
            skeletonBuilder.argumentInUse(Env.encodeWriteVar(TYPE_OBJECT, env.v_object.length), env);
        }
    }

    public static PQLToken create (Join join, SkeletonBuilder skeletonBuilder, Env env, Map <Integer, Class> typeInformations) throws RuntimeCreatorException {
        PQLToken startToken = new PQLToken(PQLFactory.ConjunctiveBlock(PQLFactory.TRUE, join), null, skeletonBuilder, env);
        startToken.childs.get(1).codeEntryPoint = true;

        for (PQLToken child : startToken.getAllChilds()) {
            if (!child.isControlStructure()) {
                skeletonBuilder.currentBuildingToken = child;
                child.startsIteration = AdditionalInfo.getWithIteration(child, skeletonBuilder, typeInformations, env);
                skeletonBuilder.currentBuildingToken = null;
            }
        }

        for (PQLToken child : startToken.getAllChilds()) {
            if (!child.isControlStructure() && !child.isReductor()) {
                child.trueBehavior = new PQLReturnBehavior();
                child.falseBehavior = new PQLReturnBehavior();
                child.calculateReturnBehavior(child, child.trueBehavior, true);
                child.calculateReturnBehavior(child, child.falseBehavior, false);
            }
        }

        return startToken;
    }

    public Object getMethodDefaultValue () {
        return methodDefaultValue;
    }

    public Set <PQLToken> getAllChilds () {
        Set <PQLToken> tokens = new HashSet <PQLToken> ();
        getAllChilds(tokens);
        return tokens;
    }

    private void getAllChilds (Set <PQLToken> tokens) {
        tokens.add(this);
        for (PQLToken child : childs)
            child.getAllChilds(tokens);
    }

    public List <PQLToken> getAllChildsInOrder () {
        List <PQLToken> tokens = new ArrayList <PQLToken> ();
        getAllChildsInOrder(tokens);
        return tokens;
    }

    private void getAllChildsInOrder (List <PQLToken> tokens) {
        tokens.add(this);
        for (PQLToken child : childs)
            child.getAllChildsInOrder(tokens);
    }

    private void calculateReturnBehavior(PQLToken from, PQLReturnBehavior returnBehavior, boolean result) throws RuntimeCreatorException {
        if (isReductor() && result) {
            returnBehavior.insertReductors.add(this);
            if (parent != null && parent.isReductor()) {
                parent.calculateReturnBehavior(from, returnBehavior, true);
                return;
            } else {
                PQLToken jumpBackToken = from.getJumpBackToken(this, true);
                if (jumpBackToken != null) {
                    returnBehavior.jumpTo = jumpBackToken;
                    returnBehavior.jumpToIterationLabel = true;
                    return;
                }
            }
        }

        if (parent == null || codeEntryPoint)
            returnBehavior.finalReturn = ( (result || ((isControlStructure() || isReductor()) && from.startsIteration) ) ? 1 : 0);
        else {
            if (parent.isControlStructure()) {
                if ( (parent.instruction == TYPE_CONJUCTIVE ? result : !result) || ((isReductor()) && from.startsIteration) ) {
                    PQLToken nextToken = getNeighbourToken(1);
                    if (nextToken == null) {
                        parent.calculateReturnBehavior(from, returnBehavior, (parent.instruction == TYPE_CONJUCTIVE) );
                        return;
                    }
                    returnBehavior.jumpTo = nextToken;
                } else {
                    PQLToken jumpBackToken = getJumpBackToken(parent, (parent.instruction == TYPE_DISJUNCTIVE) );
                    if (jumpBackToken == null) {
                        parent.calculateReturnBehavior(from, returnBehavior, (parent.instruction == TYPE_DISJUNCTIVE) );
                        return;
                    }
                    returnBehavior.jumpTo = jumpBackToken;
                    returnBehavior.jumpToIterationLabel = true;
                    return;
                }
            } else if (parent.isReductor()) {
                parent.calculateReturnBehavior(from, returnBehavior, result);
                return;
            } else
                throw new RuntimeCreatorException("unknown parent-type.");

            while (true) {
                if (returnBehavior.jumpTo.isControlStructure())
                    returnBehavior.jumpTo = returnBehavior.jumpTo.childs.get(0);    //empty control-structures / reductors are not allowed
                else if (returnBehavior.jumpTo.isReductor()) {
                    returnBehavior.insertInitReductors.add(returnBehavior.jumpTo);
                    PQLToken innerReductor = returnBehavior.jumpTo.innerReductor;
                    while (innerReductor != null) {
                        returnBehavior.insertInitReductors.add(innerReductor);
                        innerReductor = innerReductor.innerReductor;
                    }
                    returnBehavior.jumpTo = returnBehavior.jumpTo.childs.get(0);
                } else
                    break;
            }
        }
    }

    private PQLToken getJumpBackToken (PQLToken context, boolean result) {
        if (!isInside(context))
            return null;

        if (result && startsIteration)
            return this;

        PQLToken preToken = getNeighbourToken(-1);
        if (preToken != null) {
            if (preToken.startsIteration) {
                if (!preToken.isInside(context))
                    return null;
                return preToken;
            }
            return preToken.getJumpBackToken(context, false);
        } else {
            if (parent != null)
                return parent.getJumpBackToken(context, false);
            return null;
        }
    }

    private PQLToken getNeighbourToken (int relativePosition) {
        if (parent != null && parent.isControlStructure()) {
            int positionInControlStructure = -1;
            for (int i=0; i<parent.childs.size(); i++)
                if (parent.childs.get(i) == this) {
                    positionInControlStructure = i;
                    break;
                }
            if (positionInControlStructure < 0)
                return null;
            positionInControlStructure += relativePosition;
            if (!(positionInControlStructure < 0 || positionInControlStructure >= parent.childs.size()))
                return parent.childs.get(positionInControlStructure);
        }
        return null;
    }

    private boolean isInside (PQLToken context) {
        if (context == this)
            return true;
        for (int i=0; i<context.childs.size(); i++)
            if (isInside(context.childs.get(i)))
                return true;
        return false;
    }

    /*public void calculateReturnBehaviors () throws RuntimeCreatorException {
        calculatePreReturnBehaviors(null);
        calculatePosition();
    }

    private void calculatePreReturnBehaviors (PQLToken _jumpBackTo) throws RuntimeCreatorException {
        //code, which is not inserted linear (controlstructures have no code, reductors are inserted if needed)
        createdNonLinear = (isControlStructure() || isReductor());
        //detect, whether token has iteration or not
        startsIteration = false;
        if (!this.isControlStructure())
            startsIteration = gen_AdditionalInfos.withIteration(this);
        jumpBackTo = _jumpBackTo;
        if (startsIteration)
            _jumpBackTo = this;
        if (this.isReductor())
            _jumpBackTo = null;

        for (PQLToken child : childs)
            child.calculatePreReturnBehaviors(_jumpBackTo);
    } */

    /*private void calculatePosition () throws RuntimeCreatorException {
        trueBehavior = new PQLReturnBehavior();
        getReturnBehavior(true, false, trueBehavior, this);
        falseBehavior = new PQLReturnBehavior();
        getReturnBehavior(false, false, falseBehavior, this);

        for (PQLToken child : childs)
            child.calculatePosition();*/
    //}

    /*private void getReturnBehavior (boolean result, boolean strictFalseReturn, PQLReturnBehavior returnBehavior, PQLToken origRequest) {

        if (this.isReductor()) {
            //a reductor with result true? => insert all reductors, which are direct parents
            if (result) {
                PQLToken reductor = this;
                returnBehavior.insertReductors.add(this);
                if (reductor.parent != null && reductor.parent.isReductor()) {
                    reductor.parent.getReturnBehavior(true, strictFalseReturn, returnBehavior, origRequest);
                    return;
                }
            //go back to the starting reductor of the nested reductors ..
            } else if (parent != null && parent.isReductor()) {
                parent.getReturnBehavior(false, strictFalseReturn, returnBehavior, origRequest);
                return;
            //.. and then behave, as if the result was true (because when the body of the
            //   reductor finished (with false), the reductor itself finishes too, but succeeds with true)
            } else if (!strictFalseReturn)
                result = true;
        }

        //direct child of a control-structure => the control-structure defines the behavior
        if (parent != null && parent.isControlStructure()) {
            if (parent.instruction == TYPE_CONJUCTIVE) {
                //first find the token in the childs..
                int jumpToPosition = -1;
                for (int i=0; i<parent.childs.size(); i++) {
                    if (parent.childs.get(i) == this) {
                        //true => jump to next child
                        if (result)
                            jumpToPosition = i+1;
                        //false => go back to the last iteration-child
                        else {
                            for (int j=i-(strictFalseReturn ? 0 : 1); j>=0; j--) {
                                if (parent.childs.get(j).startsIteration) {
                                    jumpToPosition = j;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                //we don't find a valid position?
                if (jumpToPosition < 0 || jumpToPosition >= parent.childs.size()) {
                    parent.getReturnBehavior(result, false, returnBehavior, origRequest);
                    if (result)
                        getReturnBehavior(false, true, returnBehavior, origRequest);
                } else {
                    returnBehavior.jumpTo = parent.childs.get(jumpToPosition).getNextCreateLinearToken();
                    returnBehavior.insertInitReductors = parent.childs.get(jumpToPosition).getInitiatingReductors();
                    //backward jump to iterationLabel?
                    if (!result)
                        returnBehavior.jumpToIterationLabel = true;
                }
            }
        //inherit parent behavior (parent is a reductor in this case..)
        } else if (parent != null)
            parent.getReturnBehavior(result, false, returnBehavior, origRequest);
        //the 'default-case': we finished and jump out of the method
        else
            returnBehavior.finalReturn = (result ? 1 : 0);
    }

    public PQLToken getNextCreateLinearToken () {
        if (!createdNonLinear)
            return this;
        //reductors can have inner reductors => last child is next 'real' child
        else if (isReductor())
            return childs.get(childs.size()-1).getNextCreateLinearToken();
        //but control-structures first 'real' child is the first one
        else
            return childs.get(0).getNextCreateLinearToken();
    }*/

    public boolean isControlStructure () {
        return (instruction == TYPE_CONJUCTIVE || instruction == TYPE_DISJUNCTIVE);
    }

    public boolean isReductor () {
        return isReductor;
        /*return (instruction == REDUCTOR_SET || instruction == REDUCTOR_ARRAY || instruction == REDUCTOR_MAP ||
                instruction == REDUCTOR_DEFAULTMAP || instruction == REDUCTOR_EXISTS || instruction == REDUCTOR_METHOD_ADAPTER || instruction == TYPE_EXTENSION_N_DEFAULT_MAP_METHOD_ADAPTER);*/
    }

    /*public Set <PQLToken> getInitiatingReductors () {
        return getInitiatingReductors(new HashSet <PQLToken> ());
    }

    private Set <PQLToken> getInitiatingReductors (Set <PQLToken> tokens) {
        if (!createdNonLinear)
            return tokens;
        //reductors can have inner reductors => last child is next 'real' child
        else if (isReductor()) {
            tokens.add(this);
            return childs.get(childs.size()-1).getInitiatingReductors(tokens);
        }
        //but control-structures first 'real' child is the first one
        else
            return childs.get(0).getInitiatingReductors(tokens);
    }*/

    public int getInternalId () throws RuntimeCreatorException {
        if (parent == null)
            return 0;
        else {
            for (int i=0; i<parent.childs.size(); i++)
                if (parent.childs.get(i) == this)
                    return parent.getMaxInternalId() + i + 1;
            throw new RuntimeCreatorException("child of PQLToken not found (internal error).");
        }
    }

    public int getMaxInternalId () throws RuntimeCreatorException {
        if (parent == null)
            return 0;
        else
            return parent.getMaxInternalId() + parent.childs.size();
    }

    public int getParallelModeIndex() {
        return parallelModeIndex;
    }

    public String getName () {
        if (extensionName != null)
            return extensionName;

        try {
            for (Field field : BcFlags.class.getDeclaredFields())
                if ((field.getName().startsWith("TYPE_") || field.getName().startsWith("REDUCTOR_")) && field.get(null).equals(instruction))
                    return field.getName();
        } catch (Exception e) {}

        return "UNKNOWN-NAME";
    }

    @Override
    public String toString () {
        try {
            return toString(0);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String toString (int level) throws Exception {
        String returnStr = "";
        for (int i=0; i<level; i++)
            returnStr += "   ";
        returnStr += "[" + getInternalId() + "]: ";
        returnStr += getName();
        returnStr += " (" + FlagUtils.getTypeName(type) + ( startsIteration ? ", starts iteration" : "") + ")";
        if (trueBehavior != null && falseBehavior != null)
            returnStr += "    true: " + trueBehavior.toString() + ", false: " + falseBehavior.toString();
        returnStr += "\n";
        for (int i=0; i<childs.size(); i++)
            returnStr += childs.get(i).toString(level+1);
        return returnStr;
    }

    @Override
    public boolean equals (Object other) {
        return other == this;
    }

    @Override
    public int hashCode () {
        return 0;
    }

}
