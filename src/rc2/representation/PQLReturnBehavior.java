/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.representation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Hilmar
 */
public class PQLReturnBehavior {

    public PQLToken jumpTo;
    public Set <PQLToken> insertReductors;
    public Set <PQLToken> insertInitReductors;
    public int finalReturn;
    public boolean jumpToIterationLabel;

    public PQLReturnBehavior () {
        insertReductors = new HashSet <PQLToken> ();
        insertInitReductors = new HashSet <PQLToken> ();
        jumpTo = null;
        finalReturn = -1;
        jumpToIterationLabel = false;
    }

    public boolean isFinalReturn () {
        return (finalReturn >= 0 && jumpTo == null);
    }

    @Override
    public String toString () {
        try {
            String insertStr = "";
            Object [] insertReductorsArray = insertReductors.toArray();
            for (int i=0; i< insertReductorsArray.length; i++)
                insertStr += (i > 0 ? ", " : "") + ((PQLToken)insertReductorsArray[i]).getInternalId();
            if (!insertStr.equals(""))
                insertStr = " {insert: " + insertStr + "}";

            String insertInitStr = "";
            Object [] insertInitReductorsArray = insertInitReductors.toArray();
            for (int i=0; i< insertInitReductorsArray.length; i++)
                insertInitStr += (i > 0 ? ", " : "") + ((PQLToken)insertInitReductorsArray[i]).getInternalId();
            if (!insertInitStr.equals(""))
                insertInitStr = " {insert init: " + insertInitStr + "}";

            if (isFinalReturn() && finalReturn == 0)
                return "final false" + insertStr + insertInitStr;
            else if (isFinalReturn() && finalReturn == 1)
                return "final true" + insertStr + insertInitStr;
            else
                return "jumpTo: " + jumpTo.getInternalId() + (jumpToIterationLabel ? "[iteration]" : "") + insertStr + insertInitStr;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}
