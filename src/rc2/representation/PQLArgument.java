/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.representation;

import benchmarks.BenchmarkOptions;
import static edu.umass.bc.BcFlags.TYPE_SHORT;
import edu.umass.pql.Env;
import edu.umass.pql.VarConstants;
import java.util.ArrayList;
import java.util.List;
import rc2.SkeletonBuilder;

/**
 *
 * @author Hilmar
 */
public class PQLArgument implements VarConstants {

    public int index;
    public int type;
    public boolean isConst;
    public int localIndex;
    public Object constValue;

    public PQLArgument (int _index, int _type, boolean _isConst, Env env) {
        index = _index;
        type = _type;
        isConst = _isConst;
        if (BenchmarkOptions.antiOptimizedConst)
            isConst = false;
        //don't set to -1: if we have a constant PQLArgument it won't change and it can come to confusion with the 'compareValues' method of 'SubCommands'
        localIndex = 0;
        constValue = null;
        if (isConst) {
            if (type == TYPE_INT)
                constValue = new Integer (env.v_int[index]);
            else if (type == TYPE_DOUBLE)
                constValue = new Double (env.v_double[index]);
            else if (type == TYPE_LONG)
                constValue = new Long (env.v_long[index]);
            //there are temporary object variables with index v_object.length
            else if (index < env.v_object.length)
                constValue = env.v_object[index];
        }
    }

    public PQLArgument (int flags) {
        index = flags >> VAR_INDEX_SHIFT;
        type = ((flags >> VAR_TABLE_SHIFT) & 0x7);
        isConst = false;
        constValue = null;
    }

    public static void argumentInUse (List <PQLArgument> pqlArguments, int id, SkeletonBuilder skeletonBuilder, Env env) {
        int _index = id >> VAR_INDEX_SHIFT;
        int _type = ((id >> VAR_TABLE_SHIFT) & 0x7);
        if (_type == TYPE_WILDCARD)
            return;

        for (PQLArgument arg : pqlArguments)
            if (arg.index == _index && arg.type == _type) {
                if (arg.isConst && (id & VAR_READ_FLAG) == 0) {
                    arg.isConst = false;
                    arg.constValue = null;
                }
                return;
            }

        pqlArguments.add(new PQLArgument(_index, _type, ((id & VAR_READ_FLAG) != 0), env));
    }

    @Override
    public boolean equals (Object other) {
        return (other instanceof PQLArgument && ((PQLArgument)other).type == type && ((PQLArgument)other).index == index );
    }

}
