/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.representation;

import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.JoinVisitor;
import edu.umass.pql.VarInfo;
import java.lang.reflect.Method;
import java.util.Map;
import rc2.ExtensionInterface;
import rc2.SkeletonBuilder;
import rc2.bcr.BCRExtension;

/**
 *
 * @author Hilmar
 */
public class PQLExtension extends Join {

    private Method method;
    private int [] args;
    private BCRExtension fromBCRExtension;
    private String bcrExtensionName;
    private AdditionalInfo additionalInfo;

    private PQLToken token;
    private SkeletonBuilder builder;
    private Map <Integer, Class> typeInformations;

    /*public PQLExtension (ExtensionName _extension, int ... _args) {
        extension = _extension;
        args = new int [getArgsNr()];
        for (int i=0; i<_args.length; i++)
            args[i] = _args[i];
    }*/

    public PQLExtension (String _bcrExtensionName, int ... _args) throws RuntimeCreatorException {
        bcrExtensionName = _bcrExtensionName;
        for (ExtensionInterface extension : ExtensionInterface.getAllExtensions())
            for (BCRExtension bcrExtension : extension.getBCRExtensions())
                if (bcrExtension.getName().equals(bcrExtensionName) && bcrExtension.getExtensionType() == BCRExtension.ExtensionType.Join) {
                    fromBCRExtension = bcrExtension;
                    break;
                }

        if (fromBCRExtension == null)
            throw new RuntimeCreatorException("extension '" + bcrExtensionName + "' not found. (is it maybe a reductor?)");

        args = new int [getArgsNr()];
        for (int i=0; i<_args.length; i++)
            args[i] = _args[i];
    }

    @Override
    public int getArgsNr() {
        if (fromBCRExtension != null)
            return fromBCRExtension.getArgumentAmount();

        /*switch (extension) {
            default:
                return 0;
        }*/
        return 0;
    }

    @Override
    public int getReadArgsNr() {
        int readCounter = 0;
        for (int arg : args)
            if (Env.isReadVar(arg))
                readCounter++;
        return readCounter;
    }

    @Override
    public void setArg(int i, int v) {
        args[i] = v;
    }

    @Override
    public int getArg(int i) {
        return args[i];
    }

    @Override
    public void reset(Env env) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean next(Env env) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void accept(JoinVisitor visitor) {
        visitor.visitDefault(this);
    }

    @Override
    public Join copyRecursively() {
        return this.copy();
    }

    @Override
    public String getConstructorName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getSize(VarInfo var_info, Env env) {
        if (token == null)
            return 1.0;

        if (additionalInfo == null)
            additionalInfo = new AdditionalInfo(token, builder, typeInformations, env);

        return additionalInfo.constSize;
    }

    @Override
    public double getSelectivity(VarInfo var_info, Env env) {
        if (token == null)
            return 1.0;

        if (additionalInfo == null)
            additionalInfo = new AdditionalInfo(token, builder, typeInformations, env);

        return additionalInfo.selectivity;
    }

    @Override
    public double getAccessCost(VarInfo var_info, Env env) {
        if (token == null)
            return 1.0;

        if (additionalInfo == null)
            additionalInfo = new AdditionalInfo(token, builder, typeInformations, env);

        return additionalInfo.accessCost;
    }

    @Override
    public int accept(JoinFlagsVisitor visitor) {
        return visitor.visit(this);
    }

    public void bindToPQLToken (PQLToken _token, SkeletonBuilder _builder, Map <Integer, Class> _typeInformations) {
        token = _token;
        builder = _builder;
        typeInformations = _typeInformations;
    }

    public String getExtensionName () {
        return bcrExtensionName;
    }

    public BCRExtension getBCRExtension () {
        return fromBCRExtension;
    }

    @Override
    public String
    getName() {
        return bcrExtensionName;
    }

}
