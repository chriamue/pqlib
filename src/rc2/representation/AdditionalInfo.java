/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.representation;

import benchmarks.BenchmarkOptions;
import edu.umass.bc.BcFlags;
import edu.umass.bc.RuntimeClassLoader;
import edu.umass.pql.Env;
import edu.umass.pql.VarConstants;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.TraceClassVisitor;
import rc2.DebugMethodVisitor;
import rc2.ExecGenSourcecode;
import rc2.FlagUtils;
import rc2.RCInterface;
import rc2.RCInterface.ParallelMode;
import rc2.SkeletonBuilder;
import rc2.SubCommands;
import rc2.bcr.BytecodeCreatorRoutine;
import rc2.optimizer.OptimizeVisitor;

/**
 *
 * @author Hilmar
 */
public class AdditionalInfo implements VarConstants, BcFlags, Opcodes {

    public boolean withIteration;
    public double constSize;
    public double accessCost;
    public double selectivity;

    public AdditionalInfo (PQLToken token, SkeletonBuilder builder, Map <Integer, Class> typeInformations, Env env) {
        build(token, typeInformations, builder.buildParallel, builder, env, 2);
        build(token, typeInformations, builder.buildParallel, builder, env, 3);
        build(token, typeInformations, builder.buildParallel, builder, env, 4);
    }

    public AdditionalInfo(boolean _withIteration, Void _unused_) {
        withIteration = _withIteration;
    }

    public static boolean getWithIteration (PQLToken token, SkeletonBuilder builder, Map <Integer, Class> typeInformations, Env env) throws RuntimeCreatorException {
        try {
            AdditionalInfo allInfos = ExecGenSourcecode.additional_buildToken(null, builder, token, 2, typeInformations, env);
            return allInfos.withIteration;
        } catch (Throwable t) {
            throw new RuntimeCreatorException(t.toString());
        }
    }

    private void build (PQLToken token, Map <Integer, Class> typeInformations, ParallelMode parallelMode, SkeletonBuilder builder, Env env, int translateStep) {
        try {
            SkeletonBuilder skeletonBuilder = new SkeletonBuilder(parallelMode, true);
            skeletonBuilder.pqlArguments = builder.pqlArguments;

            for (PQLArgument arg : skeletonBuilder.pqlArguments) {
                if (!arg.isConst || arg.type == TYPE_OBJECT) {
                    arg.localIndex = skeletonBuilder.localIndexCount;
                    skeletonBuilder.localIndexCount += ((arg.type == TYPE_LONG || arg.type == TYPE_DOUBLE) ? 2 : 1);
                }
            }

            ClassWriter cv;
            ClassVisitor cw = null;
            cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
            cw = cv;
            MethodVisitor mv;

            cw.visit(V1_6, ACC_PUBLIC, "Evaluator", null, "java/lang/Object", null);

            cw.visitSource("Evaluator.java", null);

            {
                mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
                mv.visitCode();
                Label l0 = new Label();
                mv.visitLabel(l0);
                mv.visitLineNumber(1, l0);
                mv.visitVarInsn(ALOAD, 0);
                mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
                mv.visitInsn(RETURN);
                Label l1 = new Label();
                mv.visitLabel(l1);
                mv.visitLocalVariable("this", "LEvaluator;", null, l0, l1, 0);
                mv.visitMaxs(1, 1);
                mv.visitEnd();
            }
            {
                mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "evaluate", "(Ledu/umass/pql/Env;D)D", null, null);
                //mv = new DebugMethodVisitor(mv);
                mv.visitCode();

                for (PQLArgument arg : skeletonBuilder.pqlArguments) {
                    if (!arg.isConst || arg.type == TYPE_OBJECT) {
                        mv.visitVarInsn(ALOAD, 0);
                        mv.visitFieldInsn(GETFIELD, "edu/umass/pql/Env", FlagUtils.getEnvArrayName(arg.type), "[" + FlagUtils.getTypeSpecifier(arg.type));
                        SubCommands.constLoadRuntime(mv, TYPE_INT, new Integer(arg.index));
                        mv.visitInsn( FlagUtils.getArrayInstruction(TYPE_LOAD, arg.type) );
                        if (arg.type == TYPE_OBJECT) {
                            if (typeInformations.containsKey(arg.index))
                                mv.visitTypeInsn(CHECKCAST, typeInformations.get(arg.index).getName().replace(".", "/"));
                            else if (arg.isConst)
                                throw new RuntimeCreatorException("constant objects has to have an specified instance-type.");
                        }
                        mv.visitVarInsn(FlagUtils.getPushStoreCommand(TYPE_STORE, arg.type), arg.localIndex);
                    }
                }

                for (PQLToken child : token.getAllChilds())
                    if (!child.isControlStructure() && !child.isReductor())
                        ExecGenSourcecode.mainCommands_buildToken(mv, skeletonBuilder, child, 0, typeInformations, env);

                AdditionalInfo allInfos = ExecGenSourcecode.additional_buildToken(mv, skeletonBuilder, token, translateStep, typeInformations, env);
                withIteration = allInfos.withIteration;

                mv.visitVarInsn(DLOAD, 1);
                mv.visitInsn(DRETURN);

                mv.visitMaxs(0, 0);
                mv.visitEnd();
            }
            cw.visitEnd();

            byte [] code = cv.toByteArray();

            Method startMethod = null;
            RuntimeClassLoader cl = new RuntimeClassLoader ();
            Class dynamicClass = cl.defineClass("Evaluator", code );
            for (int i=0; i<dynamicClass.getMethods().length; i++) {
                if (dynamicClass.getMethods()[i].getName().equals("evaluate")) {
                    startMethod = dynamicClass.getMethods()[i];
                    break;
                }
            }

            if (translateStep == 2)
                constSize = (Double)startMethod.invoke(null, new Object[] { env, new Double(1) });
            else if (translateStep == 3)
                accessCost = (Double)startMethod.invoke(null, new Object[] { env, new Double(1) });
            else if (translateStep == 4)
                selectivity = (Double)startMethod.invoke(null, new Object[] { env, new Double(1) });
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "[AdditionalInfo]  withIteration: " + withIteration + ", constSize: " + constSize;
    }

}
