/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.representation;

import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.Env;
import edu.umass.pql.Reductor;
import edu.umass.pql.il.reductor.ReductorVisitor;
import java.lang.reflect.Method;
import rc2.ExtensionInterface;
import rc2.bcr.BCRExtension;
import rc2.bcr.BCRExtension.ExtensionType;

/**
 *
 * @author Hilmar
 */
public class PQLReductorExtension extends Reductor {

    public enum ReductorExtensionName {N_DEFAULT_MAP_METHOD_ADAPTER};

    private ReductorExtensionName extension;
    private Method method;
    private int [] args;
    private BCRExtension fromBCRExtension;
    private String bcrExtensionName;

    /*public PQLReductorExtension (ReductorExtensionName _extension, int ... _args) throws RuntimeCreatorException {
        super(getOutputArg(_args));
        extension = _extension;
        args = new int [_args.length];
        for (int i=0; i<_args.length; i++)
            args[i] = _args[i];
    }

    public PQLReductorExtension (ReductorExtensionName _extension, Method _method, int ... _args) throws RuntimeCreatorException {
        super(getOutputArg(_args));
        extension = _extension;
        method = _method;
        args = new int [_args.length];
        for (int i=0; i<_args.length; i++)
            args[i] = _args[i];
    }*/

    public PQLReductorExtension (String _bcrExtensionName, int ... _args) throws RuntimeCreatorException {
        super(getOutputArg(_args));
        bcrExtensionName = _bcrExtensionName;
        for (ExtensionInterface extension : ExtensionInterface.getAllExtensions())
            for (BCRExtension bcrExtension : extension.getBCRExtensions())
                if (bcrExtension.getName().equals(bcrExtensionName) && bcrExtension.getExtensionType() == ExtensionType.Reductor) {
                    fromBCRExtension = bcrExtension;
                    break;
                }

        if (fromBCRExtension == null)
            throw new RuntimeCreatorException("extension '" + bcrExtensionName + "' not found. (is it maybe a join?)");

        args = new int [_args.length];
        for (int i=0; i<_args.length; i++)
            args[i] = _args[i];
    }

    public PQLReductorExtension (String _bcrExtensionName, Method _method, int ... _args) throws RuntimeCreatorException {
        this(_bcrExtensionName, _args);
        method = _method;
    }

    private static int getOutputArg(int [] _args) throws RuntimeCreatorException {
        for (int arg : _args)
            if (Env.isWriteVar(arg))
                return arg;
        throw new RuntimeCreatorException("reductor need an output-variable.");
    }

    @Override
    public int
    getArgsNr()
    {
            return args.length;
    }

    @Override
    public int
    getArg(int index)
    {
            return args[index];
    }

    @Override
    public void accept(ReductorVisitor visitor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getAggregate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setAggregateInternal(Object aggregate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean succeed(Env env) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected int getArgInternal(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setArgInternal(int i, int v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void absorbAggregate(Object other_aggregate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        if (fromBCRExtension != null)
            return fromBCRExtension.getName();
        else
            return extension.name();
    }

    public int accept(ReductorFlagsVisitor visitor) {
        return visitor.visit(this);
    }

    public ReductorExtensionName getExtension () {
        return extension;
    }

    public Method getMethod() {
        return method;
    }

    public BCRExtension getBCRExtension () {
        return fromBCRExtension;
    }

    public String getExtensionName () {
        return bcrExtensionName;
    }
}
