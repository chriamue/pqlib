/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.bcr;

import java.util.HashSet;
import java.util.Set;
import rc2.GenericInterface;
import rc2.parallel.ParallelMerge;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BCRExtension {

    public enum ExtensionType {Reductor, Join};

    private String name;
    private String path;
    private int argumentAmount;
    private ExtensionType extensionType;
    private int id;
    private int parallelArgIndex;
    private ParallelMerge parallelMerge;
    private Class <? extends GenericInterface> generic;
    private Object genericInformation;

    private static int idCounter = 128;

    public BCRExtension (String _name, String _path, int _argumentAmount, ExtensionType _extensionType) throws RuntimeCreatorException {
        name = _name;
        path = _path;
        argumentAmount = _argumentAmount;
        extensionType = _extensionType;
        id = idCounter++;
        if (idCounter >= 255)
            throw new RuntimeCreatorException("too much extensions. (128 max. possible)");
        parallelArgIndex = -1;
    }

    public BCRExtension (String _name, String _path, int _argumentAmount, ExtensionType _extensionType, int _parallelArgIndex) throws RuntimeCreatorException {
        this(_name, _path, _argumentAmount, _extensionType);
        parallelArgIndex = _parallelArgIndex;
    }

    public BCRExtension (String _name, String _path, int _argumentAmount, ExtensionType _extensionType, ParallelMerge _parallelMerge) throws RuntimeCreatorException {
        this(_name, _path, _argumentAmount, _extensionType);
        parallelMerge = _parallelMerge;
    }

    public BCRExtension (String _name, String _path, int _argumentAmount, ExtensionType _extensionType, Class <? extends GenericInterface> _generic, Object _genericInformation) throws RuntimeCreatorException {
        this(_name, _path, _argumentAmount, _extensionType);
        generic = _generic;
        genericInformation = _genericInformation;
    }

    public BCRExtension (String _name, String _path, int _argumentAmount, ExtensionType _extensionType, int _parallelArgIndex, Class <? extends GenericInterface> _generic, Object _genericInformation) throws RuntimeCreatorException {
        this(_name, _path, _argumentAmount, _extensionType, _parallelArgIndex);
        generic = _generic;
        genericInformation = _genericInformation;
    }

    public BCRExtension (String _name, String _path, int _argumentAmount, ExtensionType _extensionType, ParallelMerge _parallelMerge, Class <? extends GenericInterface> _generic, Object _genericInformation) throws RuntimeCreatorException {
        this(_name, _path, _argumentAmount, _extensionType, _parallelMerge);
        generic = _generic;
        genericInformation = _genericInformation;
    }

    public String getName () {
        return name;
    }

    public String getPath () {
        return path;
    }

    public int getArgumentAmount () {
        return argumentAmount;
    }

    public ExtensionType getExtensionType () {
        return extensionType;
    }

    public int getId () {
        return id;
    }

    public int getParallelArgIndex () {
        return parallelArgIndex;
    }

    public ParallelMerge getParallelMerge () {
        return parallelMerge;
    }

    public boolean isGeneric () {
        return (generic != null);
    }

    public Class <? extends GenericInterface> getGeneric () {
        return generic;
    }

    public Object getGenericInformation () {
        return genericInformation;
    }

    @Override
    public int hashCode () {
        return 0;
    }

}
