/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.bcr;

import java.util.List;
import java.util.Set;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public abstract class SubCondition {

    public boolean isConstant;
    public boolean constantResult;
    public Set <Integer> labelIds;

    abstract public void calcConstant (int accessMode) throws RuntimeCreatorException;
    abstract public void createCode (BCRCommand command) throws RuntimeCreatorException;
    abstract public List <SubConditionFinal> getAllConditions ();

}
