/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.BcFlags;
import java.util.ArrayList;
import java.util.List;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BCRCommand implements BcFlags {

    public static int MAIN_ENTRY_POINT = 1;
    public static int SET_VARIABLE = 2;
    public static int IF_CONDITION = 4;
    public static int FUNCTION_START = 8;
    public static int ACCESS_DEFINITION = 16;
    public static int RETURN = 32;
    public static int GOTO = 64;
    public static int LABEL = 128;
    public static int GENERIC = 256;

    public static int COND_IF = (1 << 12);
    public static int COND_ELSE = (2 << 12);

    public static int COND_SIMPLE = (1 << 24);
    public static int COND_CONST = (2 << 24);
    public static int COND_MODE = (4 << 24);
    public static int COND_NULL = (8 << 24);
    public static int COND_NONNULL = (16 << 24);
    public static int COND_INSTANCE = (32 << 24);
    public static int COND_PARALLELMODE = (64 << 24);

    public static int SET_SIMPLE = (1 << 12);
    public static int SET_METHOD = (2 << 12);
    public static int SET_CALCULATION = (4 << 12);
    public static int SET_CONSTANT_INIT = (8 << 12);
    public static int SET_NEW = (16 << 12);

    public String origTerm;
    public int id;
    public int line;
    public String srcName;

    public int level;
    public int flags;
    public SubCondition condition;
    public String mainEntryPointName;
    public List <String> setArguments;
    public List <String> genericArguments;
    public BCRVariable variable;
    public boolean returnValue;
    public String labelName;
    public int convertToType;

    public BCRCommand (String _origTerm, int _line) {
        origTerm = _origTerm;
        line = _line;
        id = line;
        level = 0;
        flags = 0;
        mainEntryPointName = "";
        setArguments = new ArrayList <String> ();
        genericArguments = new ArrayList <String> ();
        variable = null;
        returnValue = false;
        labelName = "";
        convertToType = TYPE_UNKNOWN;
    }

    public RuntimeCreatorException error (String err) throws RuntimeCreatorException {
        throw new RuntimeCreatorException(err + " (line: " + line + (srcName != null ? ", in file: '" + srcName + "'" : "" ) + ")");
    }

    @Override
    public boolean equals (Object other) {
        if (!(other instanceof BCRCommand))
            return false;
        return (((BCRCommand)other).id == id);
    }

    @Override
    public String toString () {
        return "[BCRCommand]  " + origTerm + ", " + id + ", " + line + ", " + srcName + ", " + level + ", " + flags + ", " + condition + ", " + mainEntryPointName + ", " + setArguments + ", " + genericArguments + ", " + (variable == null ? "null" : variable.getName()) + ", " + returnValue + ", " + labelName + ", " + convertToType;
    }
}
