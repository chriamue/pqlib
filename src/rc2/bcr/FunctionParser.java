/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import rc2.FlagUtils;
import rc2.bcr.SubConditionNested.ConnectionType;
import rc2.parser.BCRLexer;
import rc2.parser.BCRParser;
import rc2.parser.BCRParser.FunctionHeadContext;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class FunctionParser {

    private final static String codePath = "data/bcr_code/%FILE%.bcr";

    private String srcName;
    private String originSrc;
    private String currentSrc;
    private int line;
    private int shifting;
    private boolean firstParse;
    private boolean genericsParse;
    //private BCRCommand iterationSecondLabel;
    //private boolean iterationSecondLabelUsed;
    private int idCounter;
    private String currentBreakLabel;
    private String currentContinueLabel;

    private BCRCommand currentConditionCommand;
    private SubCondition currentSubCondition;
    private boolean negateNextSubCondition;

    public FunctionParser (String path) throws IOException {

        StringBuilder builder = new StringBuilder();
        BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream((path.contains(".") ? path : codePath.replace("%FILE%", path))), "UTF-8"));
        String nextLine;
        while ( (nextLine = in.readLine()) != null)
            builder.append(nextLine + "\n");
        in.close();

        originSrc = builder.toString();
        srcName = (path.contains(".") ? path : path + ".bcr");

    }

    private void initialize(List <BCRCommand> commands, List <ParseTree> allTokens, ParseTree tree, TokenStream tokenStream, int newCommands) {
        for (int i=0; i<newCommands; i++) {
            BytecodeCreatorRoutine.commands.add(new BCRCommand(tree.getText(), tokenStream.get(tree.getSourceInterval().a).getLine()));
            commands.add(BytecodeCreatorRoutine.commands.get(BytecodeCreatorRoutine.commands.size()-1));
            commands.get(commands.size()-1).level = shifting;
            commands.get(commands.size()-1).srcName = srcName;
            commands.get(commands.size()-1).id = idCounter++;
        }
        for (int i=0; i<tree.getChildCount(); i++)
            parseNext(tree.getChild(i), allTokens);
    }

    private String getTypeInformation (List <ParseTree> allTokens, int startIndex) {
        if (startIndex+1 < allTokens.size() && allTokens.get(startIndex+1).getText().equals("@type")) {
            String returnType = allTokens.get(startIndex+3).getText();
            for (int i=0; i<5; i++)
                allTokens.remove(startIndex);
            return returnType;
        }
        return null;
    }

    private String [] allPossibleAccessModes (String [] accessModes) {


        List <String> returnList = new ArrayList <String> ();
        for (String accessMode : accessModes) {
            int firstIndex = accessMode.indexOf("?");
            if (firstIndex == -1) {
                returnList.add(accessMode);
                continue;
            } else {
                returnList.add(accessMode.replaceFirst("?", "_"));
                returnList.add(accessMode.replaceFirst("?", "r"));
                returnList.add(accessMode.replaceFirst("?", "w"));
            }
        }
        return returnList.toArray(new String [returnList.size()]);
    }

    static private int private_label_counter = 0;

    private String
    genFreshLabel()
    {
	return "__l_" + (private_label_counter++);
    }

    private String
    genFreshVarname()
    {
	return "__v_" + (private_label_counter++);
    }


    private void
    emitLabel(ParseTree tree, TokenStream tokenStream, String label) throws RuntimeCreatorException {
	List<BCRCommand> commands = new ArrayList <BCRCommand> ();
	List<ParseTree> allTokens = new ArrayList <ParseTree> ();
	initialize(commands, allTokens, tree, tokenStream, 1);
	commands.get(0).flags = BCRCommand.LABEL;
	commands.get(0).labelName = label;
    }

    private void
    emitGoto(ParseTree tree, TokenStream tokenStream, String label) throws RuntimeCreatorException {
	List<BCRCommand> commands = new ArrayList <BCRCommand> ();
	List<ParseTree> allTokens = new ArrayList <ParseTree> ();
	initialize(commands, allTokens, tree, tokenStream, 1);
	commands.get(0).flags = BCRCommand.GOTO;
	commands.get(0).labelName = label;
    }

    private static void setConditionArgumentsIfMode (SubConditionFinal subConditionFinal, List <String> variables, List <String> accessModes) {
        for (String accessMode : accessModes) {
            String newAccessMode = "";
            for (int i=0; i<BytecodeCreatorRoutine.variables.size(); i++) {
                if (!BytecodeCreatorRoutine.variables.get(i).isArgument())
                    continue;
                String newVal = "?";
                for (int j=0; j<variables.size(); j++)
                    if (variables.get(j).equals(BytecodeCreatorRoutine.variables.get(i).getName()))
                        newVal = "" + accessMode.charAt(j);
                newAccessMode += newVal;
            }
            subConditionFinal.conditionArguments.add(newAccessMode);
        }

        for (int i=0; i<subConditionFinal.conditionArguments.size(); i++) {
            if (subConditionFinal.conditionArguments.get(i).contains("?")) {
                for (BCRAccessMode accessMode : BytecodeCreatorRoutine.accessModes)
                    if (accessMode.isCompatibleWithTemplateAccessMode(subConditionFinal.conditionArguments.get(i)))
                        subConditionFinal.conditionArguments.add(accessMode.toString());
                subConditionFinal.conditionArguments.remove(i);
                i--;
            }
        }
    }


    /**
     * Processes an AST fragment
     *
     * @param tree The AST fragment to process
     * @param tokenStream A token stream, though I have no idea what we use that for
     * @return An expression AST (for expressoins only)
     */
    private ExprAST parseNext (ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException {

        List <BCRCommand> commands = new ArrayList <BCRCommand> ();
        List <ParseTree> allTokens = new ArrayList <ParseTree> ();

        if (tree instanceof BCRParser.InitializingContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 2);

            commands.get(1).flags = BCRCommand.ACCESS_DEFINITION;
            int i;
            for (i=3; true; i+=2) {
                if (allTokens.get(i) instanceof BCRParser.FunctionHeadContext)
                    break;
                if (firstParse) {
                    String [] accessModes = new String[] {allTokens.get(i).getText()};
                    while (true) {
                        int lengthBefore = accessModes.length;
                        accessModes = allPossibleAccessModes(accessModes);
                        if (accessModes.length == lengthBefore)
                            break;
                    }
                    for (String accessMode : accessModes)
                        BytecodeCreatorRoutine.accessModes.add(new BCRAccessMode(accessMode));
                }
            }
            if (BytecodeCreatorRoutine.accessModes.size() == 0)
                BytecodeCreatorRoutine.accessModes.add(new BCRAccessMode(""));

            commands.get(0).flags = BCRCommand.FUNCTION_START;
            BytecodeCreatorRoutine.functionName = allTokens.get(i+1).getText();
            i+=3;
            int variableIndex = 0;
            if (firstParse)
                for (; i<allTokens.size(); i+=2)
                    BytecodeCreatorRoutine.variables.add(new BCRVariable(allTokens.get(i).getText(), variableIndex++));
        } else if (tree instanceof BCRParser.IfinstructionContext && !genericsParse) {
            parseNext(tree.getChild(2), tokenStream);
            shifting+=4;
            parseNext(tree.getChild(4), tokenStream);
            shifting-=4;
            if (tree.getChildCount() > 5) {
                BCRCommand command = new BCRCommand("else", tokenStream.get(tree.getChild(5).getSourceInterval().a).getLine());
                command.id = idCounter++;
                command.level = shifting;
                command.srcName = srcName;
                command.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_ELSE;
                BytecodeCreatorRoutine.commands.add(command);
                shifting+=4;
                parseNext(tree.getChild(6), tokenStream);
                shifting-=4;
            }
	} else if (tree instanceof BCRParser.WhileCommandContext && !genericsParse) {
	    final String start_label = genFreshLabel();
	    final String loop_label = genFreshLabel();
	    final String end_label = genFreshLabel();
	    final ParseTree body = tree.getChild(4);
	    final ParseTree cond = tree.getChild(2);

	    emitGoto(tree, tokenStream, start_label);
	    emitLabel(tree, tokenStream, loop_label);
            String oldContinueLabel = currentContinueLabel;
            String oldBreakLabel = currentBreakLabel;
            currentBreakLabel = end_label;
            currentContinueLabel = start_label;
	    parseNext(body, tokenStream);
            currentContinueLabel = oldContinueLabel;
            currentBreakLabel = oldBreakLabel;
	    emitLabel(tree, tokenStream, start_label);
	    parseNext(cond, tokenStream);
	    shifting += 4;
	    emitGoto(tree, tokenStream, loop_label);
	    shifting -= 4;
            emitLabel(tree, tokenStream, end_label);
	} else if (tree instanceof BCRParser.DoWhileCommandContext && !genericsParse) {
	    final String start_label = genFreshLabel();
	    final String loop_label = genFreshLabel();
	    final String end_label = genFreshLabel();
	    emitLabel(tree, tokenStream, loop_label);

	    final ParseTree body = tree.getChild(1);
	    final ParseTree cond = tree.getChild(4);

            String oldContinueLabel = currentContinueLabel;
            String oldBreakLabel = currentBreakLabel;
            currentBreakLabel = end_label;
            currentContinueLabel = start_label;
	    parseNext(body, tokenStream);
            currentBreakLabel = oldBreakLabel;
            currentContinueLabel = oldContinueLabel;
            emitLabel(tree, tokenStream, start_label);
	    parseNext(cond, tokenStream);

	    shifting += 4;
	    emitGoto(tree, tokenStream, loop_label);
            emitLabel(tree, tokenStream, end_label);
	    shifting -= 4;
        } else if (tree instanceof BCRParser.ContinueCommandContext && !genericsParse) {
            if (currentContinueLabel == null) {
                initialize(commands, allTokens, tree, tokenStream, 1);
                commands.get(0).error("continue is just allowed inside a loop.");
            }
            emitGoto(tree, tokenStream, currentContinueLabel);
        } else if (tree instanceof BCRParser.BreakCommandContext && !genericsParse) {
            if (currentBreakLabel == null) {
                initialize(commands, allTokens, tree, tokenStream, 1);
                commands.get(0).error("break is just allowed inside a loop.");
            }
            emitGoto(tree, tokenStream, currentBreakLabel);
        } else if (tree instanceof BCRParser.IfcontentContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 1);
            commands.get(0).flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF;
            currentConditionCommand = commands.get(0);
            currentSubCondition = null;
            negateNextSubCondition = false;
            parseNext(tree.getChild(0), tokenStream);
        } else if (tree instanceof BCRParser.IfconnectionContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 0);
            if (tree.getChildCount() == 1)
                parseNext(tree.getChild(0), tokenStream);
            else {
                boolean negateThis = false;
                if (negateNextSubCondition) {
                    negateThis = true;
                    negateNextSubCondition = false;
                }
                SubCondition oldSubCondition = currentSubCondition;
                SubCondition newSubCondition = null;
                newSubCondition = new SubConditionNested();
                ((SubConditionNested)newSubCondition).connection = (tree.getChild(1).getText().equals("&&") ? ConnectionType.And : ConnectionType.Or);
                for (int i=0; i<tree.getChildCount(); i+=2) {
                    currentSubCondition = newSubCondition;
                    parseNext(tree.getChild(i), tokenStream);
                }
                if (negateThis)
                    ((SubConditionNested)newSubCondition).negate();
                if (oldSubCondition == null)
                    BytecodeCreatorRoutine.commands.get(BytecodeCreatorRoutine.commands.size()-1).condition = newSubCondition;
                else
                    ((SubConditionNested)oldSubCondition).subConditions.add(newSubCondition);
            }

        } else if (tree instanceof BCRParser.IfsubconditionContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 0);
            negateNextSubCondition = (tree.getChildCount() > 1 && allTokens.get(0).getText().equals("!"));
            for (int i=0; i<tree.getChildCount(); i++)
                parseNext(tree.getChild(i), tokenStream);
        } else if (tree instanceof BCRParser.IfliteralContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 0);
            SubCondition oldSubCondition = currentSubCondition;
            SubConditionFinal subConditionFinal = new SubConditionFinal();
            boolean negateLiteral = false;
            if (allTokens.get(0).getText().equals("!")) {
                negateLiteral = true;
                allTokens.remove(0);
            }
            if (allTokens.get(0).getText().equals("isConst"))
                subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_CONST;
            else if (allTokens.get(0).getText().equals("isMode"))
                subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_MODE;
            else if (allTokens.get(0).getText().equals("isParallelMode"))
                subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_PARALLELMODE;
            if (subConditionFinal.flags != 0) {
                if (allTokens.get(0).getText().equals("isMode")) {
                    boolean fillAccessModes = false;
                    List <String> variables = new ArrayList <String> ();
                    List <String> accessModes = new ArrayList <String> ();
                    for (int i=3; i<allTokens.size()-2; i+=2) {
                        String value = allTokens.get(i).getText();
                        if (value.equals(",")) {
                            fillAccessModes = true;
                            continue;
                        }
                        if (!fillAccessModes)
                            variables.add(value);
                        else
                            accessModes.add(value);
                    }

                    setConditionArgumentsIfMode(subConditionFinal, variables, accessModes);
                } else {
                    for (int i=2; i<allTokens.size(); i+=2)
                        subConditionFinal.conditionArguments.add(allTokens.get(i).getText());
                }
            } else {
                subConditionFinal.conditionArguments.add(allTokens.get(0).getText());
                if (allTokens.get(2).getText().equals("null")) {
                    if (allTokens.get(1).getText().equals("=="))
                        subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_NULL;
                    else if (allTokens.get(1).getText().equals("!="))
                        subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_NONNULL;
                    else
                        currentConditionCommand.error("null is just compatible with == or !=");
                } else {
                    if (allTokens.get(1).getText().equals("instanceof")) {
                        subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_INSTANCE;
                        String instanceName = allTokens.get(2).getText();
                        if (instanceName.endsWith("Array") && instanceName.startsWith("__"))
                            instanceName = instanceName.replace("Array", "[]").replace("__", "");
                        subConditionFinal.conditionArguments.add(instanceName);
                    } else {
                        String type = getTypeInformation(allTokens, 0);
                        if (allTokens.get(0).getText().equals("!")) {
                            negateLiteral = true;
                            allTokens.remove(0);
                        }
                        subConditionFinal.conditionArguments.remove(subConditionFinal.conditionArguments.size()-1);
                        subConditionFinal.conditionArguments.add(allTokens.get(0).getText());
                        subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_SIMPLE;
                        subConditionFinal.conditionArguments.add(allTokens.get(1).getText() + (type != null ? "{" + type + "}" : ""));
                        subConditionFinal.conditionArguments.add(allTokens.get(2).getText());
                        if (subConditionFinal.conditionArguments.get(subConditionFinal.conditionArguments.size()-1).equals("-"))
                            subConditionFinal.conditionArguments.set(subConditionFinal.conditionArguments.size()-1, subConditionFinal.conditionArguments.get(subConditionFinal.conditionArguments.size()-1) + allTokens.get(3).getText());
                    }
                }
            }
            subConditionFinal.negate = negateLiteral;

            if (oldSubCondition == null)
                BytecodeCreatorRoutine.commands.get(BytecodeCreatorRoutine.commands.size()-1).condition = subConditionFinal;
            else
                ((SubConditionNested)oldSubCondition).subConditions.add(subConditionFinal);


        } else if (tree instanceof BCRParser.LabelCommandContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 1);
            String labelName = allTokens.get(0).getText();
            if (labelName.equals("local")) {
                commands.get(0).flags = BCRCommand.MAIN_ENTRY_POINT;
                commands.get(0).mainEntryPointName = "local init";
            } else if (labelName.equals("global")) {
                commands.get(0).flags = BCRCommand.MAIN_ENTRY_POINT;
                commands.get(0).mainEntryPointName = "global init";
            } else if (labelName.equals("size_calc")) {
                commands.get(0).flags = BCRCommand.MAIN_ENTRY_POINT;
                commands.get(0).mainEntryPointName = "size_calc";
            } else if (labelName.equals("access_cost_calc")) {
                commands.get(0).flags = BCRCommand.MAIN_ENTRY_POINT;
                commands.get(0).mainEntryPointName = "access_cost_calc";
            } else if (labelName.equals("selectivity_calc")) {
                commands.get(0).flags = BCRCommand.MAIN_ENTRY_POINT;
                commands.get(0).mainEntryPointName = "selectivity_calc";
            } else if (labelName.equals("iteration")) {
                commands.get(0).flags = BCRCommand.MAIN_ENTRY_POINT;
                commands.get(0).mainEntryPointName = "iteration";
                /*if (iterationSecondLabel != null)
                    commands.get(0).error("multiple iteration labels not allowed");
                iterationSecondLabel = new BCRCommand("iteration:", commands.get(0).line);
                iterationSecondLabel.id = idCounter++;
                iterationSecondLabel.srcName = srcName;
                iterationSecondLabel.level = shifting;
                iterationSecondLabel.flags = BCRCommand.LABEL;
                iterationSecondLabel.labelName = "#iteration_intern#";
                BytecodeCreatorRoutine.commands.add(iterationSecondLabel);*/
            } else
                commands.get(0).error("invalid label-name");
                /*commands.get(0).flags = BCRCommand.LABEL;
                commands.get(0).labelName = labelName;*/
        }/* else if (tree instanceof BCRParser.GotoCommandContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 1);
            String labelName = allTokens.get(1).getText();
            commands.get(0).flags = BCRCommand.GOTO;
            if (labelName.equals("iteration"))
                //commands.get(0).labelName = "#iteration_intern#";
                //iterationSecondLabelUsed = true;
                commands.get(0).error("goto to iteration is not allowed.");
            else
                commands.get(0).labelName = labelName;
        } */else if (tree instanceof BCRParser.ReturnCommandContext && !genericsParse) {
	    if (tree.getChildCount() > 2) {
		// complex return construction
                int startId = idCounter;
		initialize(commands, allTokens, tree, tokenStream, 8);

                int sizeBefore = BytecodeCreatorRoutine.commands.size();

		String var = tree.getChild(2).toString();
		ExprAST ast = parseNext(tree.getChild(4), tokenStream);
                String expressionVar = "";
                if (ast instanceof ExprASTLiteral)
                    expressionVar = ((ExprASTLiteral)ast).literal;
                else {
                    expressionVar = genFreshVarname();
                    ast.emit(expressionVar, null, tree, tokenStream); // evaluates the specified expression

                    int sizeNewCmds = BytecodeCreatorRoutine.commands.size()-sizeBefore;
                    for (int i=0; i<sizeNewCmds; i++) {
                        BCRCommand lastCommand = BytecodeCreatorRoutine.commands.remove(BytecodeCreatorRoutine.commands.size()-1);
                        BytecodeCreatorRoutine.commands.add(BytecodeCreatorRoutine.commands.size()-7-sizeNewCmds, lastCommand);
                    }
                    for (int i=BytecodeCreatorRoutine.commands.size()-8-sizeNewCmds; i<BytecodeCreatorRoutine.commands.size(); i++)
                        BytecodeCreatorRoutine.commands.get(i).id = startId+i-(BytecodeCreatorRoutine.commands.size()-8-sizeNewCmds);
                }

                BCRCommand ifIsMode1 = commands.get(0);
                BCRCommand ifExpr = commands.get(1);
                ifExpr.level += 4;
                BCRCommand proceed1 = commands.get(2);
                proceed1.level += 8;
                BCRCommand elseExpr = commands.get(3);
                elseExpr.level += 4;
                BCRCommand abort1 = commands.get(4);
                abort1.level += 8;
                BCRCommand ifIsMode2 = commands.get(5);
                BCRCommand assign = commands.get(6);
                assign.level += 4;
                BCRCommand proceed2 = commands.get(7);
                proceed2.level += 4;

                SubConditionFinal subConditionFinal1 = new SubConditionFinal();
                SubConditionFinal subConditionFinal2 = new SubConditionFinal();
                ifIsMode1.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF;
                ifIsMode2.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF;
                ifIsMode1.condition = subConditionFinal1;
                ifIsMode2.condition = subConditionFinal2;
                subConditionFinal1.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_MODE;
                subConditionFinal2.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_MODE;
                elseExpr.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_ELSE;
                List <String> variables = new ArrayList<String> ();
                variables.add(var);
                List <String> accessModes1 = new ArrayList<String> ();
                accessModes1.add("r");
                setConditionArgumentsIfMode(subConditionFinal1, variables, accessModes1);
                List <String> accessModes2 = new ArrayList<String> ();
                accessModes2.add("w");
                setConditionArgumentsIfMode(subConditionFinal2, variables, accessModes2);

                SubConditionFinal subConditionFinal = new SubConditionFinal();
                ifExpr.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF;
                ifExpr.condition = subConditionFinal;
                subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_SIMPLE;
                subConditionFinal.conditionArguments.add(var);
                subConditionFinal.conditionArguments.add("==");
                subConditionFinal.conditionArguments.add(expressionVar);

                proceed1.flags = BCRCommand.RETURN;
                proceed1.returnValue = true;
                proceed2.flags = BCRCommand.RETURN;
                proceed2.returnValue = true;
                abort1.flags = BCRCommand.RETURN;
                abort1.returnValue = false;

                assign.flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_SIMPLE;
                assign.variable = BytecodeCreatorRoutine.addVariable(var);
                assign.setArguments.add(expressionVar);

	    } else {
		initialize(commands, allTokens, tree, tokenStream, 1);
		commands.get(0).flags = BCRCommand.RETURN;
		commands.get(0).returnValue = (allTokens.get(0).getText().equals("proceed"));
	    }
        }  else if (tree instanceof BCRParser.MethodCallCommandContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 0);

	    ExprAST ast = parseNext(tree.getChild(0), tokenStream);
	    ast.emit(null, null, tree, tokenStream);
        // } else if (tree instanceof BCRParser.SimpleSetContext && !genericsParse) {
        //     initialize(commands, allTokens, tree, tokenStream, 1);
        //     String type = getTypeInformation(allTokens, 0);
        //     commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_SIMPLE;
        //     if (type != null)
        //         commands.get(0).convertToType = FlagUtils.getTypeFromName("{" + type + "}", commands.get(0));
        //     commands.get(0).variable = BytecodeCreatorRoutine.addVariable(allTokens.get(0).getText());
        //     commands.get(0).setArguments.add(allTokens.get(2).getText());
        //     if (commands.get(0).setArguments.get(0).equals("methodStandardValue"))
        //         NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.MethodType);
        //     else
        //         for (String parallelReservedExpression : BytecodeCreatorRoutine.parallelReservedExpressions)
        //             if (commands.get(0).setArguments.get(0).equals(parallelReservedExpression))
        //                 NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.ParallelMode);

        // } else if (tree instanceof BCRParser.NewSetContext && !genericsParse) {
        //     initialize(commands, allTokens, tree, tokenStream, 1);
        //     commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_NEW;
        //     commands.get(0).variable = BytecodeCreatorRoutine.addVariable(allTokens.get(0).getText());
        //     int lastToken = allTokens.size()-3;
        //     for (int i=3; i<=lastToken; i+=2) {
        //         String newToken = allTokens.get(i).getText();
        //         if (i == 3 && newToken.endsWith("Array"))
        //             newToken = newToken.replace("Array", "[]");
        //         commands.get(0).setArguments.add(newToken);
        //     }
        // } else if (tree instanceof BCRParser.ConstantSetContext && !genericsParse) {
        //     initialize(commands, allTokens, tree, tokenStream, 1);
        //     commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_CONSTANT_INIT;
        //     commands.get(0).variable = BytecodeCreatorRoutine.addVariable(allTokens.get(0).getText());
        //     commands.get(0).setArguments.add(allTokens.get(2).getText());
        // } else if (tree instanceof BCRParser.MethodSetContext && !genericsParse) {
        //     initialize(commands, allTokens, tree, tokenStream, 1);
        //     String type = getTypeInformation(allTokens, 0);
        //     commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_METHOD;
        //     if (allTokens.get(1).getText().equals("=")) {
        //         commands.get(0).variable = BytecodeCreatorRoutine.addVariable(allTokens.get(0).getText());
        //         for (int i=0; i<2; i++)
        //             allTokens.remove(0);
        //     }
        //     if (allTokens.get(1).getText().equals(".")) {
        //         commands.get(0).setArguments.add(allTokens.get(0).getText());
        //         for (int i=0; i<2; i++)
        //             allTokens.remove(0);
        //     } else
        //         commands.get(0).setArguments.add("_");
        //     for (int i=0; i<allTokens.size()-2; i+=2) {
        //         String methodArg = allTokens.get(i).getText();
        //         if (methodArg.equals("at"))
        //             methodArg = "[]";
        //         commands.get(0).setArguments.add(methodArg + (i == 0 && type != null ? "{" + type + "}" : ""));
        //     }

        //     for (MethodTranslateAlias alias : MethodTranslateAlias.aliasMethods)
        //         if (commands.get(0).setArguments.get(1).equals(alias.key)) {
        //             BytecodeCreatorRoutine.commands.remove(BytecodeCreatorRoutine.commands.size()-1);
        //             int curShifting = commands.get(0).level;
        //             for (int i=0; i<alias.convertToKey.length; i++) {
        //                 BCRCommand ifCommand = new BCRCommand(commands.get(0).origTerm, commands.get(0).line);
        //                 BCRCommand elseCommand = new BCRCommand(commands.get(0).origTerm, commands.get(0).line);
        //                 BCRCommand mainCommand = new BCRCommand(commands.get(0).origTerm, commands.get(0).line);
        //                 ifCommand.id = idCounter++;
        //                 elseCommand.id = idCounter++;
        //                 mainCommand.id = idCounter++;
        //                 ifCommand.srcName = commands.get(0).srcName;
        //                 elseCommand.srcName = commands.get(0).srcName;
        //                 mainCommand.srcName = commands.get(0).srcName;
        //                 ifCommand.level = curShifting;
        //                 elseCommand.level = curShifting;
        //                 curShifting += 4;
        //                 mainCommand.level = curShifting;
        //                 ifCommand.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_INSTANCE;
        //                 ifCommand.conditionArguments.add(commands.get(0).setArguments.get(0));
        //                 ifCommand.conditionArguments.add(alias.instanceChecks[i]);
        //                 elseCommand.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_ELSE;
        //                 mainCommand.flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_METHOD;
        //                 mainCommand.variable = commands.get(0).variable;
        //                 for (String arg : commands.get(0).setArguments)
        //                     mainCommand.setArguments.add(arg);
        //                 mainCommand.setArguments.set(1, alias.convertToKey[i]);
        //                 if (i == alias.convertToKey.length-1) {
        //                     mainCommand.level -= 4;
        //                     BytecodeCreatorRoutine.commands.add(mainCommand);
        //                 } else {
        //                     BytecodeCreatorRoutine.commands.add(ifCommand);
        //                     BytecodeCreatorRoutine.commands.add(mainCommand);
        //                     BytecodeCreatorRoutine.commands.add(elseCommand);
        //                 }
        //            }
        //            break;
        //         }
        // } else if (tree instanceof BCRParser.CalculationSetContext && !genericsParse) {
        //     initialize(commands, allTokens, tree, tokenStream, 1);
        //     String type = getTypeInformation(allTokens, 0);
        //     commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_CALCULATION;
        //     commands.get(0).variable = BytecodeCreatorRoutine.addVariable(allTokens.get(0).getText());
        //     if (allTokens.size() == 5) {
        //         commands.get(0).setArguments.add(allTokens.get(3).getText());
        //         commands.get(0).setArguments.add(allTokens.get(2).getText() + (type != null ? "{" + type + "}" : ""));
        //     } else {
        //         commands.get(0).setArguments.add(allTokens.get(2).getText());
        //         commands.get(0).setArguments.add(allTokens.get(3).getText() + (type != null ? "{" + type + "}" : ""));
        //         commands.get(0).setArguments.add(allTokens.get(4).getText());
        //     }
        } else if (tree instanceof BCRParser.AssignmentCommandContext && !genericsParse) {

            initialize(commands, allTokens, tree, tokenStream, 0);
	    String type = getTypeInformation(allTokens, 0);
	    int lhs_index = type == null ? 0 : 1;
            String castTo = null, lhs = "";
            if (tree.getChildCount() - lhs_index == 5) {
                castTo = allTokens.get(0).getText();
                lhs = allTokens.get(1).getText();
                lhs_index++;
            } else
                lhs = allTokens.get(0).getText();
	    final ParseTree rhs = tree.getChild(lhs_index + 2);

	    ExprAST ast = parseNext(rhs, tokenStream);
	    ast.emit(lhs, type, castTo, tree, tokenStream);

        } else if (tree instanceof BCRParser.ExprContext && !genericsParse) {
	    ExprAST ast = parseNext(tree.getChild(0), tokenStream);
	    return ast;
        } else if (!genericsParse
		   && (tree instanceof BCRParser.OrExprContext
		       || tree instanceof BCRParser.XorExprContext
		       || tree instanceof BCRParser.AndExprContext
		       || tree instanceof BCRParser.ShiftExprContext
		       || tree instanceof BCRParser.AddExprContext
		       || tree instanceof BCRParser.MulExprContext)) {

	    if (tree.getChildCount() == 1) {
		ExprAST ast = parseNext(tree.getChild(0), tokenStream);
		return ast;
	    }

            initialize(commands, allTokens, tree, tokenStream, 0);
	    ExprAST lhs = parseNext(tree.getChild(0), tokenStream);
            initialize(commands, allTokens, tree, tokenStream, 0);
	    String op = tree.getChild(1).toString();
	    ExprAST rhs = parseNext(tree.getChild(2), tokenStream);
	    return new ExprASTBinOp(lhs, op, rhs);

        } else if (tree instanceof BCRParser.UnaryExprContext && !genericsParse) {
	    if (tree.getChildCount() == 1) {
		return parseNext(tree.getChild(0), tokenStream);
	    }

            initialize(commands, allTokens, tree, tokenStream, 0);
	    String op = tree.getChild(0).toString();
            if (op.equals("-")) {
                ExprASTLiteral ast = new ExprASTLiteral("-1");
                String varName = genFreshVarname();
                ast.emit(varName, null, null, tree, tokenStream);
                return new ExprASTBinOp(new ExprASTLiteral(varName), "*", parseNext(tree.getChild(1), tokenStream));
            }

	    return new ExprASTUnOp(op, parseNext(tree.getChild(1), tokenStream));

        } else if (tree instanceof BCRParser.ConstantExprContext && !genericsParse) {
            initialize(commands, allTokens, tree, tokenStream, 0);
            String varName = genFreshVarname();
	    ExprASTLiteral ast = new ExprASTLiteral(allTokens.get(0).getText());
	    ast.emit(varName, null, null, tree, tokenStream);
            return new ExprASTLiteral(varName);
        } else if (tree instanceof BCRParser.LiteralExprContext && !genericsParse) {
	    if (tree.getChildCount() == 3) {
		return parseNext(tree.getChild(1), tokenStream);
	    }

            initialize(commands, allTokens, tree, tokenStream, 0);
	    return new ExprASTLiteral(allTokens.get(0).getText());

        } else if (tree instanceof BCRParser.MethodCallContext && !genericsParse) {

            int argIndex = 2;

            initialize(commands, allTokens, tree, tokenStream, 0);
            final String type = getTypeInformation(allTokens, 0);
            if (type != null)
                argIndex++;
	    String target = null;
            if (allTokens.get(1).getText().equals(".")) {
                argIndex+=2;
                target = allTokens.get(0).getText();
                for (int i=0; i<2; i++)
                    allTokens.remove(0);
            }

            /*
            initialize(commands, allTokens, tree, tokenStream, 0);
	    String type = getTypeInformation(allTokens, 0);
	    int lhs_index = type == null ? 0 : 1;
            String castTo = null, lhs = "";
            if (tree.getChildCount() - lhs_index == 5) {
                castTo = allTokens.get(0).getText();
                lhs = allTokens.get(1).getText();
                lhs_index++;
            } else
                lhs = allTokens.get(0).getText();
	    final ParseTree rhs = tree.getChild(lhs_index + 2);

	    ExprAST ast = parseNext(rhs, tokenStream);
	    ast.emit(lhs, type, castTo, tree, tokenStream);
                    */

	    final ArrayList<ExprAST> args = new ArrayList<ExprAST>();
            int startArgIndex = argIndex;
            for (int i=0; i<=tree.getChildCount()-startArgIndex; i+=2) {
		String methodArg = allTokens.get(i).getText();
                if (methodArg.equals("__at"))
                    methodArg = "[]";

                if (i > 0) {
                    ExprAST ast = parseNext(tree.getChild(argIndex+i-2), tokenStream);
                    if (ast instanceof ExprASTLiteral && !((ExprASTLiteral)ast).literal.startsWith("\"") && !Character.isDigit(((ExprASTLiteral)ast).literal.charAt(0)) && !(((ExprASTLiteral)ast).literal.charAt(0) == '-') )
                        methodArg = ((ExprASTLiteral)ast).literal;
                    else {

                        String argVar = genFreshVarname();
                        ast.emit(argVar, null, null, tree, tokenStream);
                        methodArg = argVar;
                    }
                }

		args.add(new ExprASTLiteral(methodArg));
            }
	    return new ExprASTMethodCall(type, target, args);
	    // System.err.println(tree.getClass() + ":");
	    // for (int i = 0 ; i < tree.getChildCount(); i++) {
	    // 	System.err.println(" #" + i + ": " + tree.getChild(i));
	    // }
        } else if (tree instanceof BCRParser.NewExprContext && !genericsParse) {
	    initialize(commands, allTokens, tree, tokenStream, 0);
	    ArrayList<ExprAST> args = new ArrayList<ExprAST>();
	    int lastToken = tree.getChildCount()-2;// allTokens.size()-2;
	    for (int i=1; i<=lastToken; i+=2) {
		String newToken = allTokens.get(i).getText();
		if (i == 1 && newToken.endsWith("Array") && newToken.startsWith("__")) {
		    newToken = newToken.replace("Array", "[]").replace("__", "");
		} else if (i > 1) {
                    ExprAST ast = parseNext(tree.getChild(i), tokenStream);
                    String argVar = genFreshVarname();
                    ast.emit(argVar, null, null, tree, tokenStream);
                    newToken = argVar;
                }
		args.add(new ExprASTLiteral(newToken));
	    }

	    return new ExprASTNew(allTokens.get(0).getText(), args);

        } else if (tree instanceof BCRParser.GenericContext) {
            if (genericsParse) {
                initialize(commands, allTokens, tree, tokenStream, 0);
                commands.add(new BCRCommand("generic", 0));
                commands.get(0).id = idCounter++;
                commands.get(0).flags = BCRCommand.GENERIC;
                commands.get(0).genericArguments.add(allTokens.get(2).getText());
                for (int i=5; i<allTokens.size(); i+=2)
                    commands.get(0).genericArguments.add(allTokens.get(i).getText().substring(1, allTokens.get(i).getText().length()-1));

                BCRGeneric newGeneric = new BCRGeneric(commands.get(0).genericArguments.get(0));
                BytecodeCreatorRoutine.generics.add(newGeneric);
                BytecodeCreatorRoutine.pwCommands.write("                Map <String, Integer> " + newGeneric.getName() + "___MAP___ = new HashMap<String, Integer>();\n");
                BytecodeCreatorRoutine.pwAdditional.write("                Map <String, Integer> " + newGeneric.getName() + "___MAP___ = new HashMap<String, Integer>();\n");
                for (int j=1; j<commands.get(0).genericArguments.size(); j++) {
                    newGeneric.add(commands.get(0).genericArguments.get(j));
                    BytecodeCreatorRoutine.pwCommands.write("                " + newGeneric.getName() + "___MAP___.put(\"" + commands.get(0).genericArguments.get(j) + "\", " + (j-1) + ");\n");
                    BytecodeCreatorRoutine.pwAdditional.write("                " + newGeneric.getName() + "___MAP___.put(\"" + commands.get(0).genericArguments.get(j) + "\", " + (j-1) + ");\n");
                }
            }
        } else
            for (int i=0; i<tree.getChildCount(); i++)
                parseNext(tree.getChild(i), tokenStream);

	return null;
    }

    private void parseNext (ParseTree tree, List <ParseTree> allTokens) {
        allTokens.add(tree);
        for (int i=0; i<tree.getChildCount(); i++)
            parseNext(tree.getChild(i), allTokens);
    }

    public void buildCommands (boolean _firstParse) throws RuntimeCreatorException {

        String justGenerics = originSrc.substring(0, originSrc.indexOf("@accessModes"));
        ANTLRInputStream inputStream = new ANTLRInputStream(justGenerics);
        BCRLexer lexer  = new BCRLexer(inputStream);
        TokenStream tokenStream = new BufferedTokenStream (lexer);
        BCRParser parser = new BCRParser(tokenStream);

        genericsParse = (BytecodeCreatorRoutine.generics.size() == 0);
        firstParse = false;
        idCounter = 0;
        List <ParseTree> parseTrees = parser.complete().children;
        if (parseTrees != null)
            for (ParseTree tree : parseTrees )
                parseNext(tree, tokenStream);
        Map <String, String> replacements = new HashMap <String, String> ();
        for (BCRGeneric generic : BytecodeCreatorRoutine.generics)
            replacements.put("#" + generic.getName() + "#", generic.getValue());

        currentSrc = originSrc;
        for (String key : replacements.keySet())
            currentSrc = currentSrc.replace(key, replacements.get(key));

        try
        {
            inputStream = new ANTLRInputStream(currentSrc);
            lexer  = new BCRLexer(inputStream);
            tokenStream = new BufferedTokenStream (lexer);
            parser = new BCRParser(tokenStream);

            //iterationSecondLabelUsed = false;
            genericsParse = false;
            firstParse = _firstParse;
            shifting = 0;
            line = 1;
            idCounter = 0;
            currentBreakLabel = null;
            currentContinueLabel = null;
            for (ParseTree tree : parser.complete().children )
                parseNext(tree, tokenStream);

            /*for (BCRCommand cmd : BytecodeCreatorRoutine.commands)
                System.out.println(cmd);
            System.out.println("++++++++++++++++++++++++");*/

            /*if (iterationSecondLabelUsed && iterationSecondLabel == null)
                throw new RuntimeCreatorException("jump to iteration label, but it is not used.");
            if (!iterationSecondLabelUsed && iterationSecondLabel != null)
                BytecodeCreatorRoutine.commands.remove(iterationSecondLabel);*/

        } catch (RuntimeCreatorException e) {
            throw e;
        } catch (Exception e) {
            String result = e.toString() + "\n";
            for (StackTraceElement element : e.getStackTrace())
                result += element.toString() + "\n";
            throw new RuntimeCreatorException(result);
        }
    }


    private abstract class ExprAST
    {
	ExprAST[] elements;
        List <BCRCommand> commands = new ArrayList <BCRCommand> ();
        List <ParseTree> allTokens = new ArrayList <ParseTree> ();

	public ExprAST(ExprAST ... args)
	{
	    elements = args;
	}

        public void emit(String lhs, String type, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
        {
            emit(lhs, type, null, tree, tokenStream);
        }

	abstract void emit(String lhs, String type, String castTo, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException;

	// Stores element[arg_no] in a temporary variable, if needed
	protected String
	precomputeArg(int arg_no, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
	{
	    if (this.elements[arg_no] instanceof ExprASTLiteral) {
		return ((ExprASTLiteral) this.elements[arg_no]).literal;
	    }
	    String varname = genFreshVarname();
	    this.elements[arg_no].emit(varname, null, tree, tokenStream);
	    this.elements[arg_no] = new ExprASTLiteral(varname);
	    return varname;
	}
    }

    private class ExprASTBinOp extends ExprAST
    {
	String op;

	public ExprASTBinOp(ExprAST lhs, String op, ExprAST rhs)
	{
	    super(lhs, rhs);
	    this.op = op;
	}

	@Override
	void
	emit(String lhs, String type, String castTo, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
	{
	    String rhs_l = precomputeArg(0, tree, tokenStream);
	    String rhs_r = precomputeArg(1, tree, tokenStream);

	    initialize(commands, allTokens, tree, tokenStream, 1);
	    commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_CALCULATION;
            commands.get(0).variable = BytecodeCreatorRoutine.addVariable(lhs);
            if (castTo != null)
                commands.get(0).variable.castTo = FlagUtils.getBasicType(FlagUtils.indexToExtendedType(FlagUtils.extendedTypeNameToIndex(castTo)));
	    commands.get(0).setArguments.add(rhs_l);
	    commands.get(0).setArguments.add(this.op + (type != null ? "{" + type + "}" : ""));
	    commands.get(0).setArguments.add(rhs_r);
	}

	@Override
	public String
	toString()
	{
	    return "BINOP("+ this.op+","+this.elements[0]+"," + this.elements[1]+")";
	}
    }

    private class ExprASTUnOp extends ExprAST
    {
	String op;

	public ExprASTUnOp(String op, ExprAST rhs)
	{
	    super(rhs);
	    this.op = op;
	}

	@Override
	void
	emit(String lhs, String type, String castTo, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
	{
	    String rhs = precomputeArg(0, tree, tokenStream);

	    initialize(commands, allTokens, tree, tokenStream, 1);
	    commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_CALCULATION;
	    commands.get(0).variable = BytecodeCreatorRoutine.addVariable(lhs);
            if (castTo != null)
                commands.get(0).variable.castTo = FlagUtils.getBasicType(FlagUtils.indexToExtendedType(FlagUtils.extendedTypeNameToIndex(castTo)));
	    commands.get(0).setArguments.add(rhs);
	    commands.get(0).setArguments.add(this.op + (type != null ? "{" + type + "}" : ""));
	}

	@Override
	public String
	toString()
	{
	    return "UNOP("+ this.op+","+this.elements[0]+")";
	}
    }

    private class ExprASTNew extends ExprAST
    {
	String classname;

	public ExprASTNew(String classname, ArrayList<ExprAST> args)
	{
	    super(new ExprAST[args.size()]);
	    for (int i = 0; i < args.size(); i++) {
		this.elements[i] = args.get(i);
	    }
	    this.classname = classname;
	}

	@Override
	void
	emit(String lhs, String type, String castTo, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
	{
            initialize(commands, allTokens, tree, tokenStream, 1);
            commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_NEW;
            commands.get(0).variable = BytecodeCreatorRoutine.addVariable(lhs);
            if (castTo != null)
                commands.get(0).variable.castTo = FlagUtils.getBasicType(FlagUtils.indexToExtendedType(FlagUtils.extendedTypeNameToIndex(castTo)));
            for (ExprAST arg : this.elements) {
                commands.get(0).setArguments.add(((ExprASTLiteral)arg).literal);
            }
	}

	@Override
	public String
	toString()
	{
	    return "NEW("+ this.classname+","+java.util.Arrays.toString(this.elements)+")";
	}
    }

    private class ExprASTMethodCall extends ExprAST
    {
	String ty, target;

	public ExprASTMethodCall(String ty, String target, ArrayList<ExprAST> args)
	{
	    super(new ExprAST[args.size()]);
	    for (int i = 0; i < args.size(); i++) {
		this.elements[i] = args.get(i);
	    }
	    this.ty = ty;
	    this.target = target;
	}

	@Override
	void
	emit(String lhs, String type, String castTo, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
	{
	    String selfty = this.ty;
	    if (selfty == null) {
		selfty = type;
	    }
	    initialize(commands, allTokens, tree, tokenStream, 1);
            commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_METHOD;
            if (lhs != null) {
                commands.get(0).variable = BytecodeCreatorRoutine.addVariable(lhs);
                if (castTo != null)
                    commands.get(0).variable.castTo = FlagUtils.getBasicType(FlagUtils.indexToExtendedType(FlagUtils.extendedTypeNameToIndex(castTo)));
		if (type != null)
		    commands.get(0).convertToType = FlagUtils.getTypeFromName("{" + type + "}", commands.get(0));
            } else if (castTo != null)
                commands.get(0).error("variable-type specified without variable.");
            if (this.target != null) {
                commands.get(0).setArguments.add(this.target);
            } else
                commands.get(0).setArguments.add("_");
	    boolean first = true;
            for (ExprAST arg : this.elements) {
                String methodArg = ((ExprASTLiteral)arg).literal;
                commands.get(0).setArguments.add(methodArg + (first && selfty != null ? "{" + selfty + "}" : ""));
		first = false;
            }

            for (MethodTranslateAlias alias : MethodTranslateAlias.aliasMethods)
                if (commands.get(0).setArguments.get(1).equals(alias.key)) {
                    BytecodeCreatorRoutine.commands.remove(BytecodeCreatorRoutine.commands.size()-1);
                    int curShifting = commands.get(0).level;
                    for (int i=0; i<alias.convertToKey.length; i++) {
                        BCRCommand ifCommand = new BCRCommand(commands.get(0).origTerm, commands.get(0).line);
                        BCRCommand elseCommand = new BCRCommand(commands.get(0).origTerm, commands.get(0).line);
                        BCRCommand mainCommand = new BCRCommand(commands.get(0).origTerm, commands.get(0).line);
                        ifCommand.id = idCounter++;
                        elseCommand.id = idCounter++;
                        mainCommand.id = idCounter++;
                        ifCommand.srcName = commands.get(0).srcName;
                        elseCommand.srcName = commands.get(0).srcName;
                        mainCommand.srcName = commands.get(0).srcName;
                        ifCommand.level = curShifting;
                        elseCommand.level = curShifting;
                        curShifting += 4;
                        mainCommand.level = curShifting;
                        SubConditionFinal subConditionFinal = new SubConditionFinal();
                        ifCommand.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF;
                        ifCommand.condition = subConditionFinal;
                        subConditionFinal.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_IF|BCRCommand.COND_INSTANCE;
                        subConditionFinal.conditionArguments.add(commands.get(0).setArguments.get(0));
                        subConditionFinal.conditionArguments.add(alias.instanceChecks[i]);
                        elseCommand.flags = BCRCommand.IF_CONDITION|BCRCommand.COND_ELSE;
                        mainCommand.flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_METHOD;
                        mainCommand.variable = commands.get(0).variable;
                        for (String arg : commands.get(0).setArguments)
                            mainCommand.setArguments.add(arg);
                        mainCommand.setArguments.set(1, alias.convertToKey[i]);
                        if (i == alias.convertToKey.length-1) {
                            mainCommand.level -= 4;
                            BytecodeCreatorRoutine.commands.add(mainCommand);
                        } else {
                            BytecodeCreatorRoutine.commands.add(ifCommand);
                            BytecodeCreatorRoutine.commands.add(mainCommand);
                            BytecodeCreatorRoutine.commands.add(elseCommand);
                        }
                   }
		    break;
		}
	}

	@Override
	public String
	toString()
	{
	    return "INVOKE("+ this.ty+","+this.target+","+java.util.Arrays.toString(this.elements)+")";
	}
    }

    private class ExprASTLiteral extends ExprAST
    {
	String literal;

	public ExprASTLiteral(String literal)
	{
	    super();
	    this.literal = literal;
	}

	boolean
	isConstant()
	{
	    return this.literal.length() > 0
		&& (Character.isDigit(this.literal.charAt(0))
		    || this.literal.charAt(0) == '"'
                    || this.literal.charAt(0) == '-');
	}

	@Override
	void
	emit(String lhs, String type, String castTo, ParseTree tree, TokenStream tokenStream) throws RuntimeCreatorException
	{
	    initialize(commands, allTokens, tree, tokenStream, 1);
	    if (this.isConstant()) {
		commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_CONSTANT_INIT;
	    } else {
		commands.get(0).flags = BCRCommand.SET_VARIABLE|BCRCommand.SET_SIMPLE;
	    }
	    if (type != null)
                commands.get(0).convertToType = FlagUtils.getTypeFromName("{" + type + "}", commands.get(0));
            commands.get(0).variable = BytecodeCreatorRoutine.addVariable(lhs);
            if (castTo != null)
                commands.get(0).variable.castTo = FlagUtils.getBasicType(FlagUtils.indexToExtendedType(FlagUtils.extendedTypeNameToIndex(castTo)));
            commands.get(0).setArguments.add(this.literal);
            if (commands.get(0).setArguments.get(0).equals("__methodStandardValue"))
                NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.MethodType);
            else
                for (String parallelReservedExpression : BytecodeCreatorRoutine.parallelReservedExpressions)
                    if (commands.get(0).setArguments.get(0).equals(parallelReservedExpression))
                        NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.ParallelMode);
	}

	@Override
	public String
	toString()
	{
	    return "LITERAL("+ this.literal + ")";
	}

    }

}
