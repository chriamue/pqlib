/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.util.function.Predicate;

/**
 *
 * @author Hilmar
 */
public class EqualPredicate implements Predicate {

    private Object checkWith;

    public EqualPredicate (Object _checkWith) {
        checkWith = _checkWith;
    }

    @Override
    public boolean test(Object t) {
        return (t != null && t.equals(checkWith));
    }

    @Override
    public Predicate and(Predicate other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Predicate negate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Predicate or(Predicate other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
