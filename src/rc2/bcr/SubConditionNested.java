/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.bcr;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class SubConditionNested extends SubCondition {

    public enum ConnectionType {And, Or};

    public List <SubCondition> subConditions;
    public ConnectionType connection;

    public SubConditionNested() {
        subConditions = new ArrayList <SubCondition> ();
    }

    public void negate() {
        for (SubCondition subCondition : subConditions) {
            if (subCondition instanceof SubConditionFinal)
                ((SubConditionFinal)subCondition).negate = !((SubConditionFinal)subCondition).negate;
            else
                ((SubConditionNested)subCondition).negate();
        }
        if (connection == ConnectionType.And)
            connection = ConnectionType.Or;
        else
            connection = ConnectionType.And;
    }

    @Override
    public void calcConstant (int accessMode) throws RuntimeCreatorException {
        for (SubCondition subCondition : subConditions)
            subCondition.calcConstant(accessMode);

        boolean notConstantSubConditionFound = false;
        for (int i=0; i<subConditions.size(); i++) {
            SubCondition subCondition = subConditions.get(i);
            if (!subCondition.isConstant)
                notConstantSubConditionFound = true;
            else if (connection == ConnectionType.And && !subCondition.constantResult) {
                isConstant = true;
                constantResult = false;
                return;
            } else if (connection == ConnectionType.Or && subCondition.constantResult) {
                isConstant = true;
                constantResult = true;
                return;
            }
        }
        if (!notConstantSubConditionFound) {
            isConstant = true;
            constantResult = (connection == ConnectionType.And ? true : false);
        }
    }

    @Override
    public void createCode (BCRCommand command) throws RuntimeCreatorException {
        Set <Integer> suceedLabelIds = new HashSet <Integer> ();
        Set <Integer> failLabelIds = new HashSet <Integer> ();
        createCode(command, true, suceedLabelIds, failLabelIds);
        labelIds = new HashSet <Integer> ();
        for (Integer labelId : failLabelIds)
            labelIds.add(labelId);
    }

    public void createCode (BCRCommand command, boolean proceedIfSuceed, Set <Integer> suceedLabelIds, Set <Integer> failLabelIds) throws RuntimeCreatorException {
        int indexLastCondition = -1;
        for (int i=0; i<subConditions.size(); i++) {
            SubCondition subCondition = subConditions.get(i);
            if (!subCondition.isConstant)
                indexLastCondition = i;
        }

        Set <Integer> suceedLabelIdsNext = new HashSet <Integer> ();
        Set <Integer> failLabelIdsNext = new HashSet <Integer> ();

        for (int i=0; i<subConditions.size(); i++) {
            SubCondition subCondition = subConditions.get(i);
            if (!subCondition.isConstant) {
                boolean proceedIfSuceedNext = (indexLastCondition == i ? proceedIfSuceed : (connection == ConnectionType.And));
                if (subCondition instanceof SubConditionNested)
                    ((SubConditionNested)subCondition).createCode(command, proceedIfSuceedNext, suceedLabelIdsNext, failLabelIdsNext );
                else {
                    ((SubConditionFinal)subCondition).createCode(command, proceedIfSuceedNext);
                    if (proceedIfSuceedNext)
                        failLabelIdsNext.add(((SubConditionFinal)subCondition).labelIds.iterator().next());
                    else
                        suceedLabelIdsNext.add(((SubConditionFinal)subCondition).labelIds.iterator().next());
                }
            }
        }

        if (proceedIfSuceed) {
            for (Integer labelId : suceedLabelIdsNext)
                BytecodeCreatorRoutine.snippets.labelJumpsHere(labelId);
            for (Integer labelId : failLabelIdsNext)
                failLabelIds.add(labelId);
        } else {
            for (Integer labelId : failLabelIdsNext)
                BytecodeCreatorRoutine.snippets.labelJumpsHere(labelId);
            for (Integer labelId : suceedLabelIdsNext)
                suceedLabelIds.add(labelId);
        }
    }

    @Override
    public List <SubConditionFinal> getAllConditions () {
        List <SubConditionFinal> returnList = new ArrayList <SubConditionFinal> ();
        for (SubCondition subCondition : subConditions)
            returnList.addAll(subCondition.getAllConditions());
        return returnList;
    }

    @Override
    public String toString () {
        String returnStr = "[SubConditionNested]  ";
        for (int i=0; i<subConditions.size(); i++)
            returnStr += (i > 0 ? (connection == ConnectionType.And ? " && " : " || ") : "") + "( " + subConditions.get(i).toString() + " )";
        return returnStr;
    }

}
