/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import rc2.RCInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class NewMethodAttribute {

    public static List < NewMethodAttribute > newMethodAttributes;

    enum NewMethodReason {Instance, ConstantNull, Constant, FieldType, MethodType, ParallelMode};
    enum ParallelMode {};

    public int argumentIndex;
    public NewMethodReason reason;
    public String instanceName;

    public int maxPossibilities;
    public int currentPossibility;

    public NewMethodAttribute (int _argumentIndex, NewMethodReason _reason, String _instanceName) throws RuntimeCreatorException {
        argumentIndex = _argumentIndex;
        setReason(_reason);
        if (_instanceName == null)
            instanceName = null;
        else
            instanceName = _instanceName;

        currentPossibility = 0;
    }

    public void setReason (NewMethodReason _reason) {
        reason = _reason;
        if (reason == NewMethodReason.Instance)
            maxPossibilities = 3;
        else if (reason == NewMethodReason.ConstantNull)
            maxPossibilities = 3;
        else if (reason == NewMethodReason.Constant)
            maxPossibilities = 2;
        else if (reason == NewMethodReason.FieldType)
            maxPossibilities = 5;
        else if (reason == NewMethodReason.MethodType)
            maxPossibilities = 9;
        else if (reason == NewMethodReason.ParallelMode)
            maxPossibilities = RCInterface.ParallelMode.values().length;
    }

    public static void addAttribute (int _argumentIndex, NewMethodReason _reason) throws RuntimeCreatorException {
        addAttribute(_argumentIndex, _reason, null);
    }

    public static void addAttribute (int _argumentIndex, NewMethodReason _reason, String _instanceName) throws RuntimeCreatorException {

        for (NewMethodAttribute newMethodAttribute : newMethodAttributes) {
            if (newMethodAttribute.argumentIndex == _argumentIndex) {
                if (_reason == newMethodAttribute.reason && _reason == NewMethodReason.Instance && newMethodAttribute.instanceName.equals(_instanceName))
                    return;
                else if (_reason != NewMethodReason.Instance && newMethodAttribute.reason != NewMethodReason.Instance) {
                    if (_reason == NewMethodReason.ConstantNull)
                        newMethodAttribute.setReason(NewMethodReason.ConstantNull);
                    return;
                }
            }
        }

        newMethodAttributes.add(new NewMethodAttribute(_argumentIndex, _reason, _instanceName));
    }

    public boolean isConstPossibility() {
        if (reason == NewMethodReason.Constant || reason == NewMethodReason.ParallelMode)
            return true;
        else if (reason == NewMethodReason.FieldType || reason == NewMethodReason.MethodType)
            return false;
        else
            return (currentPossibility > 0);
    }

    public boolean isConstPossibilityTrue() {
        if (reason == NewMethodReason.Constant)
            return (currentPossibility == 1);
        else if (reason == NewMethodReason.FieldType || reason == NewMethodReason.MethodType)
            return false;
        else
            return (currentPossibility == 2);
    }

    public boolean isCondMatching(int flags) {
        return ( (flags == BCRCommand.COND_INSTANCE && reason == NewMethodReason.Instance) ||
                (flags == BCRCommand.COND_CONST && reason != NewMethodReason.Instance && reason != NewMethodReason.FieldType && reason != NewMethodReason.MethodType && reason != NewMethodReason.ParallelMode) ||
                (flags == BCRCommand.COND_NULL && reason != NewMethodReason.Instance && reason != NewMethodReason.FieldType && reason != NewMethodReason.MethodType && reason != NewMethodReason.ParallelMode) ||
                (flags == BCRCommand.COND_NONNULL && reason != NewMethodReason.Instance && reason != NewMethodReason.FieldType && reason != NewMethodReason.MethodType && reason != NewMethodReason.ParallelMode) );
    }

    public String getSwitchCommand () {
        if (reason == NewMethodReason.Instance)
            return "SubCommands.decideInstance (typeInformations, token.arguments.get(" + argumentIndex + "), \"" + instanceName.replace("/", ".") + "\")";
            //return "typeInformations.containsKey( (token.arguments.get(" + argumentIndex + ")  >> VAR_INDEX_SHIFT) ) ? 1 + (Class.forName(\"" + instanceName.replace("/", ".") + "\").isAssignableFrom(typeInformations.get( (token.arguments.get(" + argumentIndex + ")  >> VAR_INDEX_SHIFT) )) ? 1 : 0) : 0";
        else if (reason == NewMethodReason.Constant)
            return "skeletonBuilder.findArgument(token.arguments.get(" + argumentIndex + ")).isConst ? 1 : 0";
        else if (reason == NewMethodReason.ConstantNull)
            return "skeletonBuilder.findArgument(token.arguments.get(" + argumentIndex + ")).isConst ? 1 + (env.v_object[skeletonBuilder.findArgument(token.arguments.get(" + argumentIndex + ")).index] == null ? 1 : 0) : 0";
        else if (reason == NewMethodReason.FieldType)
            return "FlagUtils.typeNameToIndex(FlagUtils.fieldTypeToName(token.field.getType().getName()))";
        else if (reason == NewMethodReason.MethodType)
            return "FlagUtils.extendedTypeNameToIndex(token.method.getReturnType().getName())";
        else
            return "token.getParallelModeIndex()";
    }

    public static boolean isValidMethodAttributes () throws ClassNotFoundException {

        Set <Integer> argumentIndices = new HashSet <Integer> ();

        for (NewMethodAttribute attr : newMethodAttributes)
            argumentIndices.add(attr.argumentIndex);


        for (Integer currentArgumentIndex : argumentIndices) {
            List <NewMethodAttribute> instanceAttributes = new ArrayList <NewMethodAttribute> ();
            for (NewMethodAttribute attr : newMethodAttributes)
                if (attr.argumentIndex == currentArgumentIndex && attr.reason == NewMethodReason.Instance && attr.currentPossibility >= 0)
                    instanceAttributes.add(attr);
            boolean allConst = true, allNotConst = true;
            for (NewMethodAttribute attr : instanceAttributes) {
                if (attr.isConstPossibility())
                    allNotConst = false;
                else
                    allConst = false;
            }
            if (!allNotConst && !allConst)
                return false;
            //check, if their is a pair of instances, where NONE of the two classes is a superclass of the other (this is not possible, because we talk about the same variable)
            if (allConst)
                for (int i=0; i<instanceAttributes.size(); i++)
                    for (int j=i+1; j<instanceAttributes.size(); j++)
                        if (instanceAttributes.get(i).currentPossibility == 2 && instanceAttributes.get(j).currentPossibility == 2 && !(Class.forName(instanceAttributes.get(i).instanceName.replace("/", ".")).isAssignableFrom(Class.forName(instanceAttributes.get(j).instanceName.replace("/", "."))) || Class.forName(instanceAttributes.get(j).instanceName.replace("/", ".")).isAssignableFrom(Class.forName(instanceAttributes.get(i).instanceName.replace("/", ".")))) )
                            return false;
        }
        return true;
    }

}
