/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import java.util.HashMap;
import java.util.Map;
import rc2.FlagUtils;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class OperatorInfo implements VarConstants, BcFlags {

    private static Map <String, OperatorInfo> operators = generateOperators();
    private enum OperatorType {Add, Sub, Mul, Div, Mod, Neg, Inv, Or, And, Xor, Shl, Shr, SShr};

    private OperatorType operatorType;
    private String command;
    private int type;
    private int matchedInstruction;

    private OperatorInfo (String _command, OperatorType _operatorType, int _matchedInstruction) {
        command = _command;
        operatorType = _operatorType;
        matchedInstruction = _matchedInstruction;
        type = TYPE_UNKNOWN;
    }

    private OperatorInfo (OperatorInfo copyFrom) {
        command = copyFrom.command;
        operatorType = copyFrom.operatorType;
        matchedInstruction = copyFrom.matchedInstruction;
        type = copyFrom.type;
    }

    private static Map <String, OperatorInfo> generateOperators() {
        Map <String, OperatorInfo> _operators = new HashMap <String, OperatorInfo> ();
        _operators.put("+", new OperatorInfo("ADD", OperatorType.Add, TYPE_ADD) );
        _operators.put("-", new OperatorInfo("SUB", OperatorType.Sub, TYPE_SUB) );
        _operators.put("*", new OperatorInfo("MUL", OperatorType.Mul, TYPE_MUL) );
        _operators.put("/", new OperatorInfo("DIV", OperatorType.Div, TYPE_DIV) );
        _operators.put("%", new OperatorInfo("REM", OperatorType.Mod, TYPE_MOD) );
        _operators.put("!", new OperatorInfo("NEG", OperatorType.Neg, TYPE_NEG) );
        _operators.put("~", new OperatorInfo("XOR", OperatorType.Inv, TYPE_BITINV) );
        _operators.put("|", new OperatorInfo("OR", OperatorType.Or, TYPE_BITOR) );
        _operators.put("&", new OperatorInfo("AND", OperatorType.And, TYPE_BITAND) );
        _operators.put("^", new OperatorInfo("XOR", OperatorType.Xor, TYPE_BITXOR) );
        _operators.put("<<", new OperatorInfo("SHL", OperatorType.Shl, TYPE_BITSHL) );
        _operators.put(">>", new OperatorInfo("SHR", OperatorType.Shr, TYPE_BITSHR) );
        _operators.put(">>>", new OperatorInfo("USHR", OperatorType.SShr, TYPE_BITSSHR) );
        return _operators;
    }

    public static OperatorInfo findFromKey(String operator) throws RuntimeCreatorException {
        for (String key : operators.keySet())
            if (operator.equals(operators.get(key).getRawCommand()))
                return new OperatorInfo(operators.get(key));

        throw new RuntimeCreatorException("unknown operator: '" + operator + "'");
    }

    public static OperatorInfo find(String operator, BCRCommand command) throws RuntimeCreatorException {
        for (String key : operators.keySet()) {
            if (operator.startsWith(key)) {
                String cutOperator = operator.substring(key.length());
                if (!(cutOperator.equals("") || cutOperator.startsWith("{")))
                    continue;
                OperatorInfo returnOperator = new OperatorInfo(operators.get(key));
                if (!cutOperator.equals(""))
                    returnOperator.type = FlagUtils.getTypeFromName(cutOperator, command);
                return returnOperator;
            }
        }
        command.error("unknown operator: '" + operator + "'");
        return null;
    }

    public static OperatorInfo find(int instruction) throws RuntimeCreatorException {
        for (String key : operators.keySet()) {
            if (operators.get(key).matchedInstruction == instruction) {
                OperatorInfo returnOperator = new OperatorInfo(operators.get(key));
                return returnOperator;
            }
        }
        throw new RuntimeCreatorException("unknown instruction: '" + instruction + "'");
    }

    public boolean canBeDouble () {
        return (operatorType == OperatorType.Add || operatorType == OperatorType.Sub || operatorType == OperatorType.Mul || operatorType == OperatorType.Div || operatorType == OperatorType.Mod || operatorType == OperatorType.Neg);
    }

    public String getRawCommand () {
        return command;
    }

    public String getCommand () {
        return (type == TYPE_INT ? "I" : (type == TYPE_LONG ? "L" : "D")) + command;
    }

    public void setType (int _type) {
        type = _type;
    }

    public int getType () {
        return type;
    }

    public int getSecondArgumentType () {
        if (operatorType == OperatorType.Shl ||operatorType == OperatorType.Shr ||operatorType == OperatorType.SShr)
            return TYPE_INT;
        return getType();
    }

    public int getArgumentAmount () {
        if (operatorType == OperatorType.Neg || operatorType == OperatorType.Inv)
            return 1;
        return 2;
    }

    public String getOperatorSymbol() {
        for (String key : operators.keySet()) {

            if (operators.get(key).command.equals(this.command))
                return key;
        }
        return null;
    }

    public boolean isPlusOrMinus () {
        return (operatorType == OperatorType.Add || operatorType == OperatorType.Sub);
    }

    public boolean isMinus () {
        return (operatorType == OperatorType.Sub);
    }

    public boolean isInv () {
        return (operatorType == OperatorType.Inv);
    }

    public void setFinalType (boolean secondArgumentConstant, BCRCommand command, BCRVariable left, BCRVariable right) throws RuntimeCreatorException {
        if (secondArgumentConstant && type == TYPE_UNKNOWN)
            type = TYPE_INT;
        if (secondArgumentConstant && type != TYPE_INT && type != TYPE_LONG)
            command.error("constant calculations are just allowed with integer-type.");

        if (type == TYPE_UNKNOWN) {
            if (right == null)
                type = left.getType(command);
            else if (left.getType(command) == right.getType(command) || right.getType(command) == TYPE_UNKNOWN)
                type = left.getType(command);
            else if (left.getType(command) == TYPE_UNKNOWN)
                type = right.getType(command);
        }

        if (type == TYPE_INT || type == TYPE_LONG)
            return;
        if (type == TYPE_DOUBLE && (operatorType == OperatorType.Add || operatorType == OperatorType.Sub || operatorType == OperatorType.Mul || operatorType == OperatorType.Div || operatorType == OperatorType.Neg) )
            return;
        command.error("the combination of given type and specified operator are not supported. (" + FlagUtils.getTypeName(type) + ", " + operatorType.name() + ")");
    }
}
