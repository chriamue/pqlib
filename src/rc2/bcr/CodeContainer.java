/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Hilmar
 */
public class CodeContainer {

    private List <String> initLines;
    private List <String> lines;
    private List <Map <Integer, List <Integer> > > labels;
    private List <Integer> activeMethods;

    public CodeContainer () {
        lines = new ArrayList <String> ();
        initLines = new ArrayList <String> ();
        labels = new ArrayList <Map <Integer, List <Integer> > > ();
        activeMethods = new ArrayList <Integer> ();
    }

    public int add (String line) {
        lines.add(line);
        return lines.size()-1;
    }

    public void addInitLine (String line) {
        initLines.add(line);
    }

    public void insertBefore (int id, String line) {
        lines.set(id, line + lines.get(id));
    }

    public void insertAfter (int id, String line) {
        if (id == -1)
            insertBefore(id+1, line);
        else
            lines.set(id, lines.get(id) + line);
    }

    public String removeLastLine () {
        String removeLine = lines.get(lines.size()-1);
        lines.remove(lines.size()-1);
        return removeLine;
    }

    public int getCurrentPosition () {
        return lines.size()-1;
    }

    public void labelJumpsHere (int id) {
        labelJumpsToPosition(lines.size()-1, id);
    }

    public void labelJumpsToPosition (int jumpTo, int id) {
        if (!labels.get(activeMethods.size()-1).containsKey(jumpTo))
            labels.get(activeMethods.size()-1).put(jumpTo, new ArrayList <Integer> ());
        labels.get(activeMethods.size()-1).get(jumpTo).add(id);
    }

    public void newActiveMethodStarts () {
        activeMethods.add(getCurrentPosition());
        labels.add(new HashMap <Integer, List <Integer> > ());
    }

    public void finalWrite(StringWriter pw) {

        for (String initLine : initLines)
            pw.write(initLine);

        int labelCounter = 0;
        for (int i=0; i<activeMethods.size(); i++) {
            for (int j=0; j<lines.size(); j++) {
                if (labels.get(i).containsKey(j)) {
                    for (Integer k : labels.get(i).get(j) )
                        lines.set(k, lines.get(k).replace("[LABEL]", "label" + labelCounter));
                    //pw.write("        Label label" + labelCounter++ + " = new Label();\n");
                    insertAfter(activeMethods.get(i), "        Label label" + labelCounter++ + " = new Label();\n");
                }
            }
        }

        labelCounter = 0;
        for (int i=0; i<lines.size(); i++) {
            pw.write(lines.get(i));
            for (Map <Integer, List <Integer> > labelList : labels) {
                if (labelList.containsKey(i)) {
                    pw.write("        mv.visitLabel(label" + labelCounter++ + ");\n");
                    break;
                }
            }
        }
    }

}
