/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.util.ArrayList;
import java.util.List;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class LabelInfo {

    public boolean labelExists;
    public boolean gotoExists;
    public List <Integer> ids;
    public int jumpTo;

    public LabelInfo (boolean isGoto, int _id, int _jumpTo) {
        labelExists = !isGoto;
        gotoExists = isGoto;
        ids = new ArrayList <Integer> ();
        if (isGoto)
            ids.add(_id);
        jumpTo = _jumpTo;
    }

    public void addGoto (int id) {
        ids.add(id);
        gotoExists = true;
    }

    public void addLabel (int _jumpTo, BCRCommand command) throws RuntimeCreatorException {
        if (labelExists)
            command.error("duplicate label.");
        jumpTo = _jumpTo;
        labelExists = true;
    }

    public void finish () throws RuntimeCreatorException {
        if (!gotoExists)
            return;
        if (!labelExists)
            throw new RuntimeCreatorException("goto without label");
        for (Integer id : ids)
            BytecodeCreatorRoutine.snippets.labelJumpsToPosition(jumpTo, id);
    }

}
