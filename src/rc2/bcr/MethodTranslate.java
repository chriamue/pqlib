/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.objectweb.asm.Label;
import rc2.ExtensionInterface;
import rc2.FlagUtils;
import rc2.SubCommands;
import static rc2.bcr.BytecodeCreatorRoutine.commands;
import rc2.bcr.NewMethodAttribute.NewMethodReason;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class MethodTranslate implements VarConstants, BcFlags {

    private static List <MethodTranslate> methods;

    protected String key;
    protected String methodName;
    protected String fromClass;
    protected String argumentsAndReturn;
    protected String invokeType;
    protected int [] castArguments;

    public MethodTranslate (String _key, String _methodName, Class _fromClass, Class [] _arguments, Class _returnValue) throws RuntimeCreatorException {
        key = _key;
        methodName = _methodName;
        fromClass = _fromClass.getName().replace(".", "/");
        castArguments = new int [_arguments.length + 1];

        boolean methodFound = false;
        boolean isStatic = false;
        for (Method method : _fromClass.getMethods()) {
            if (!method.getName().equals(_methodName) || !method.getReturnType().equals(_returnValue) || method.getParameterTypes().length != _arguments.length)
                continue;
            boolean argsEqual = true;
            for (int i=0; i<method.getParameterTypes().length; i++)
                if (!method.getParameterTypes()[i].equals(_arguments[i])) {
                    argsEqual = false;
                    break;
                }

            if (argsEqual) {
                 if (Modifier.isStatic(method.getModifiers()))
                     isStatic = true;
                 methodFound = true;
                 break;
            }
        }
        if (!methodFound)
            throw new RuntimeCreatorException("method '" + _methodName + "' with given parameters not found.");

        if (_fromClass.isInterface())
            invokeType = "INVOKEINTERFACE";
        else {
            if (isStatic)
                invokeType = "INVOKESTATIC";
            else
                invokeType = "INVOKEVIRTUAL";
        }
        argumentsAndReturn = "(";
        for (int i=0; i<_arguments.length + 1; i++) {
            if ( i < _arguments.length)
                argumentsAndReturn += getInternalClassName(_arguments[i]);
            Class arg = ( i < _arguments.length ? _arguments[i] : _returnValue);
            castArguments[i] = getTypeFromClass(arg);
        }
        argumentsAndReturn += ")" + getInternalClassName(_returnValue);
    }

    protected static int getTypeFromClass (Class _fromClass) {
        if (int.class.isAssignableFrom(_fromClass))
            return TYPE_INT;
        else if (boolean.class.isAssignableFrom(_fromClass))
            return TYPE_BOOLEAN;
        else if (char.class.isAssignableFrom(_fromClass))
            return TYPE_CHAR;
        else if (short.class.isAssignableFrom(_fromClass))
            return TYPE_SHORT;
        else if (byte.class.isAssignableFrom(_fromClass))
            return TYPE_BYTE;
        else if (long.class.isAssignableFrom(_fromClass))
            return TYPE_LONG;
        else if (double.class.isAssignableFrom(_fromClass))
            return TYPE_DOUBLE;
        else if (float.class.isAssignableFrom(_fromClass))
            return TYPE_FLOAT;
        else if (void.class.isAssignableFrom(_fromClass))
            return -1;
        else
            return TYPE_OBJECT;
    }

    protected static String getInternalClassName (Class _fromClass) {
        int arrayDimension = 0;
        while (_fromClass.isArray()) {
            _fromClass = _fromClass.getComponentType();
            arrayDimension++;
        }

        String className = "";
        if (void.class.isAssignableFrom(_fromClass))
            className = "V";
        else if (int.class.isAssignableFrom(_fromClass))
            className = "I";
        else if (boolean.class.isAssignableFrom(_fromClass))
            className = "Z";
        else if (long.class.isAssignableFrom(_fromClass))
            className = "J";
        else if (double.class.isAssignableFrom(_fromClass))
            className = "D";
        else if (char.class.isAssignableFrom(_fromClass))
            className = "C";
        else if (short.class.isAssignableFrom(_fromClass))
            className = "S";
        else if (byte.class.isAssignableFrom(_fromClass))
            className = "B";
        else if (float.class.isAssignableFrom(_fromClass))
            className = "F";
        else
            className = "L" + _fromClass.getName().replace(".", "/") + ";";

        for (; arrayDimension > 0; arrayDimension--)
            className = "[" + className;

        return className;
    }

    public static void generate () throws RuntimeCreatorException {
        methods = new ArrayList <MethodTranslate> ();
        for (ExtensionInterface extension : ExtensionInterface.getAllExtensions())
            for (MethodTranslate method : extension.getMethods()) {
                for (MethodTranslate other : methods) {
                    if (other.key.equals(method.key))
                        throw new RuntimeCreatorException("duplicate method-keys: '" + other.key + "'");
                    if (method.key.startsWith("__") || method.key.equals("length"))
                        throw new RuntimeCreatorException("method keys 'length' and '__*' are reserved.");
                }
                methods.add(method);
            }
    }

    public static void translate (BCRCommand command) throws Exception {

        if (command.setArguments.size() < 2)
            command.error("invalid amount of method-arguments");

        BCRVariable invokeObj = null;
        if (!command.setArguments.get(0).equals("_")) {
            invokeObj = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(0), false, command));
            invokeObj.setType(TYPE_OBJECT, command);
            invokeObj.pushValue(command, TYPE_OBJECT);
        }

        if (command.setArguments.get(1).startsWith("length{") || (command.setArguments.get(1).equals("length"))) {
            if (command.setArguments.size() != 2)
                command.error("invalid amount of method-arguments");

            int arrayType;
            if (command.setArguments.get(1).startsWith("length{"))
                arrayType = FlagUtils.getTypeFromName(command.setArguments.get(1).substring(6), command);
            else
                arrayType = BCRObjectCast.findArrayType(BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(0), false)), command);
            BCRObjectCast.objectCasts.add(new BCRObjectCast(BCRObjectCast.STATIC_NEED_CLASS, "[" + FlagUtils.getTypeSpecifier(arrayType), invokeObj, null, null, command));
            BytecodeCreatorRoutine.snippets.add("        mv.visitInsn(ARRAYLENGTH);\n");
            command.variable.setType(TYPE_INT, command);
            command.variable.storeValue(command, TYPE_INT);
        } else if (command.setArguments.get(1).equals("__getField")) {
            if (command.setArguments.size() != 2)
                command.error("invalid amount of method-arguments");

            int fromType = TYPE_UNKNOWN;
            for (NewMethodAttribute newMethodAttribute : NewMethodAttribute.newMethodAttributes)
                if (newMethodAttribute.reason == NewMethodAttribute.NewMethodReason.FieldType)
                    fromType = FlagUtils.indexToType(newMethodAttribute.currentPossibility);
            if (fromType == TYPE_UNKNOWN)
                command.error("type cannot be unknown (internal error).");

            BCRObjectCast.objectCasts.add(new BCRObjectCast(BCRObjectCast.DYNAMIC_NEED_CLASS|(fromType == TYPE_OBJECT ? BCRObjectCast.DYNAMIC_GET_CLASS : 0), "token.field.getDeclaringClass().getName().replace(\".\", \"/\")" , invokeObj, (fromType == TYPE_OBJECT ? "token.field.getType().getName().replace(\".\", \"/\")" : null), (fromType == TYPE_OBJECT ? command.variable : null), command));
            //BytecodeCreatorRoutine.snippets.add("        mv.visitTypeInsn(CHECKCAST, token.field.getDeclaringClass().getName().replace(\".\", \"/\"));\n");
            BytecodeCreatorRoutine.snippets.add("        mv.visitFieldInsn(GETFIELD, token.field.getDeclaringClass().getName().replace(\".\", \"/\"), token.field.getName(), FlagUtils.fieldTypeToName(token.field.getType().getName()));\n");

            if (fromType == TYPE_FLOAT) {
                BytecodeCreatorRoutine.snippets.add("        mv.visitInsn(F2D);\n");
                fromType = TYPE_DOUBLE;
            }
            command.variable.setType(fromType, command);
            command.variable.storeValue(command, fromType);
        } else if (command.setArguments.get(1).equals("__callInnerReductor")) {
            BytecodeCreatorRoutine.snippets.add("        skeletonBuilder.insertInnerReductor(mv, token);\n");
        } else if (command.setArguments.get(1).equals("__getJavaTypeClass")) {
            BytecodeCreatorRoutine.snippets.add("        mv.visitLdcInsn(token.javaType.getName());\n");
            BytecodeCreatorRoutine.snippets.add("        mv.visitMethodInsn(INVOKESTATIC, \"java/lang/Class\", \"forName\", \"(Ljava/lang/String;)Ljava/lang/Class;\", false);\n");
            if (command.variable.getType(command) == TYPE_UNKNOWN && !command.variable.inherits())
                command.variable.setType(TYPE_OBJECT, command);
            BCRObjectCast.objectCasts.add(new BCRObjectCast( BCRObjectCast.STATIC_GET_CLASS, null, null, "java/lang/Class", command.variable, command));
            command.variable.storeValue(command, TYPE_OBJECT);
        } else if (command.setArguments.get(1).equals("__callMethod")) {
            if (command.setArguments.size() != 4)
                command.error("invalid amount of method-arguments");

            int argumentType = TYPE_UNKNOWN;
            for (NewMethodAttribute newMethodAttribute : NewMethodAttribute.newMethodAttributes)
                if (newMethodAttribute.reason == NewMethodAttribute.NewMethodReason.MethodType)
                    argumentType = FlagUtils.indexToExtendedType(newMethodAttribute.currentPossibility);
            String argumentTypeStr = FlagUtils.getTypeSpecifier(argumentType);
            BCRVariable var1 = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(2), false, command));
            BCRVariable var2 = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(3), false, command));

            if (var1.getType(command) == TYPE_UNKNOWN && !var1.inherits())
                var1.setType(FlagUtils.getBasicType(argumentType), command);
            if (var2.getType(command) == TYPE_UNKNOWN && !var2.inherits())
                var2.setType(FlagUtils.getBasicType(argumentType), command);

            //var1.setType(FlagUtils.getBasicType(argumentType), command);
            //var2.setType(FlagUtils.getBasicType(argumentType), command);
            var1.pushValue(command, argumentType);
            var2.pushValue(command, argumentType);
            BytecodeCreatorRoutine.snippets.add("        mv.visitMethodInsn(INVOKESTATIC, token.method.getDeclaringClass().getName().replace(\".\", \"/\"), token.method.getName(), \"(" + argumentTypeStr + argumentTypeStr + ")" + argumentTypeStr + "\", false);\n");
            if (command.variable.getType(command) == TYPE_UNKNOWN && !command.variable.inherits())
                command.variable.setType(FlagUtils.getBasicType(argumentType), command);
            command.variable.storeValue(command, argumentType);
        } else if (command.setArguments.get(1).startsWith("[]{") || command.setArguments.get(1).equals("[]")) {
            if ( ( command.variable != null && command.setArguments.size() != 3) || ( command.variable == null && command.setArguments.size() != 4))
                command.error("invalid amount of method-arguments");

            int arrayType;
            if ( command.setArguments.get(1).equals("[]"))
                arrayType = BCRObjectCast.findArrayType(BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(0), false)), command);
            else
                arrayType = FlagUtils.getTypeFromName(command.setArguments.get(1).substring(2), command);
            BCRObjectCast.objectCasts.add(new BCRObjectCast(BCRObjectCast.STATIC_NEED_CLASS|( (arrayType == TYPE_OBJECT && command.variable != null) ? BCRObjectCast.STATIC_GET_CLASS : 0), "[" + FlagUtils.getTypeSpecifier(arrayType), invokeObj, ((arrayType == TYPE_OBJECT && command.variable != null) ? "java/lang/Object" : null), ((arrayType == TYPE_OBJECT && command.variable != null) ? command.variable : null ), command));

            BCRVariable position = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(2), false, command));
            position.setType(TYPE_INT, command);
            position.pushValue(command, TYPE_INT);

            if (command.variable == null) {
                BCRVariable entry = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(3), false, command));
                entry.setType(FlagUtils.getBasicType(arrayType), command);
                entry.pushValue(command, arrayType);
            }

            BytecodeCreatorRoutine.snippets.add("        mv.visitInsn(" + FlagUtils.getStrArrayInstruction( (command.variable == null ? TYPE_STORE : TYPE_LOAD) , arrayType) + ");\n");
            if (command.variable != null) {
                command.variable.setType(FlagUtils.getBasicType(arrayType), command);
                command.variable.storeValue(command, arrayType);
            }
        } else {
            for (MethodTranslate methodTranslate : methods) {
                if (methodTranslate.key.equals(command.setArguments.get(1))) {
                    if (command.setArguments.size() != methodTranslate.castArguments.length - 1 + 2)
                        command.error("invalid amount of method-arguments");
                    String returnTypeStr = methodTranslate.argumentsAndReturn.substring(methodTranslate.argumentsAndReturn.indexOf(")")+1);
                    boolean noObjectReturn = true;
                    if ((returnTypeStr.contains("[") || returnTypeStr.contains(";")) && command.variable != null) {
                        noObjectReturn = false;
                        /*int arrayDimension = 0;
                        while (returnTypeStr.startsWith("[")) {
                            returnTypeStr = returnTypeStr.substring(1);
                            arrayDimension ++;
                        }*/
                        if (!returnTypeStr.contains("["))
                            returnTypeStr = returnTypeStr.replace(";", "").substring(returnTypeStr.indexOf("L")+1);
                        //for (int i=0; i<arrayDimension; i++)
                        //    returnTypeStr = "[" + returnTypeStr;
                    }
                    if (!(noObjectReturn && invokeObj == null))
                        BCRObjectCast.objectCasts.add(new BCRObjectCast( (invokeObj != null ? BCRObjectCast.STATIC_NEED_CLASS : 0)|(noObjectReturn ? 0 : BCRObjectCast.STATIC_GET_CLASS), (invokeObj != null ? methodTranslate.fromClass : null), invokeObj, (noObjectReturn ? null : returnTypeStr), (noObjectReturn ? null : command.variable), command));

                    /*if (invokeObj != null && invokeObj.isArgument()) {
                        boolean foundNewMethodAttribute = false;
                        for (NewMethodAttribute newMethodAttribute : NewMethodAttribute.newMethodAttributes)
                            if (newMethodAttribute.reason == NewMethodReason.Instance && newMethodAttribute.argumentIndex == BytecodeCreatorRoutine.getVariableIndex(invokeObj.getName(), true) ) {
                                foundNewMethodAttribute = true;
                                if (!newMethodAttribute.isConstPossibility())
                                    BytecodeCreatorRoutine.snippets.add("        mv.visitTypeInsn(CHECKCAST, \"" + methodTranslate.fromClass + "\");\n");
                                break;
                            }
                        if (!foundNewMethodAttribute)
                            throw new RuntimeCreatorException("no newMethodAttribute found with name: '" + invokeObj.getName() + "'");
                    }*/

                    for (int i=0; i<methodTranslate.castArguments.length-1; i++) {
                        BCRVariable argument = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(command.setArguments.get(2 + i), false, command));
                        if (argument.getType(command) == TYPE_UNKNOWN && !argument.inherits())
                            argument.setType(FlagUtils.getBasicType(methodTranslate.castArguments[i]), command);
                        argument.pushValue(command, methodTranslate.castArguments[i]);
                    }

                    BytecodeCreatorRoutine.snippets.add("        mv.visitMethodInsn(" + methodTranslate.invokeType + ", \"" + methodTranslate.fromClass + "\", \"" + methodTranslate.methodName + "\", \"" + methodTranslate.argumentsAndReturn + "\", " + (methodTranslate.invokeType.equals("INVOKEINTERFACE") ? "true" : "false") + ");\n");

                    if (command.variable != null) {
                        int toType = methodTranslate.castArguments[methodTranslate.castArguments.length-1];
                        if (command.variable.getType(command) == TYPE_UNKNOWN && !command.variable.inherits())
                            command.variable.setType(FlagUtils.getBasicType(toType), command);
                        command.variable.storeValue(command, toType);
                    } else {
                        int returnType = methodTranslate.castArguments[methodTranslate.castArguments.length-1];
                        if (returnType != -1)
                            BytecodeCreatorRoutine.snippets.add("        mv.visitInsn(" + ((returnType == TYPE_DOUBLE || returnType == TYPE_LONG) ? "POP2" : "POP") + ");\n");
                    }
                    return;
                }
            }
            command.error("unknown method: " + command.setArguments.get(1));
        }
    }

    public static String getClassName (String _key) throws RuntimeCreatorException {
        for (MethodTranslate methodTranslate : methods)
            if (methodTranslate.key.equals(_key))
                return methodTranslate.fromClass;
        return null;
    }

}
