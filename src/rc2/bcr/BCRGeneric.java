/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilmar
 */
public class BCRGeneric {

    private String name;
    private List <String> values;
    public int currentPossibility;

    public BCRGeneric (String _name) {
        name = _name;
        values = new ArrayList <String>();
        currentPossibility = 0;
    }

    public void add (String value) {
        values.add(value);
    }

    public int getMaxPossibilities () {
        return values.size();
    }

    public String getSwitchCommand () {
        return "(int)" + name + "___MAP___.get(" + name + ")";
    }

    public String getName() {
        return name;
    }

    public String getValue () {
        return values.get(currentPossibility);
    }

}
