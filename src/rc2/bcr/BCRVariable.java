/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.BcFlags;
import java.util.ArrayList;
import java.util.List;
import rc2.FlagUtils;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BCRVariable implements BcFlags {

    public static int ARGUMENT = (1 << 8);

    protected static int localIndexCounter;

    private int flags;
    private String name;
    private int argumentIndex;
    private int localIndex;
    private List <BCRVariable> inheritTypeFrom;
    private Class objectClass;
    public int castTo;

    public BCRVariable(String _name, int _argumentIndex) {
        flags = (TYPE_UNKNOWN | ARGUMENT );
        name = _name;
        argumentIndex = _argumentIndex;

        castTo = -1;
        localIndex = -1;
        inheritTypeFrom = new ArrayList <BCRVariable> ();
        objectClass = null;
    }

    public BCRVariable(String _name) {
        flags = TYPE_UNKNOWN;
        name = _name;

        castTo = -1;
        localIndex = -1;
        inheritTypeFrom = new ArrayList <BCRVariable> ();
        objectClass = null;
    }

    public void reset () {
        localIndex = -1;
        localIndexCounter = 0;
        flags = ((isArgument() ? ARGUMENT : 0) | TYPE_UNKNOWN);
        inheritTypeFrom = new ArrayList <BCRVariable> ();
    }

    public int getType (BCRCommand command) throws RuntimeCreatorException {
        setTypeFromCast();
        List <Integer> types = new ArrayList <Integer> ();
        int type = (flags & 0xFF);
        if (inheritTypeFrom.size() == 0)
            return type;
        else {
            for (BCRVariable inheritVar : inheritTypeFrom)
                types.add(inheritVar.getType(command));
            for (Integer newType : types) {
                if (newType == TYPE_UNKNOWN)
                    continue;
                else if (type == TYPE_UNKNOWN)
                    type = newType;
                else if (type != newType)
                    command.error("miss-matching types for variable: '" + name + "' (types found: " + FlagUtils.getTypeName(type) + " and " + FlagUtils.getTypeName(newType) + ")");
            }
            return type;
        }
    }

    public List <BCRVariable> getArgumentInheritedFrom () {
        List <BCRVariable> returnList = new ArrayList <BCRVariable> ();
        if (inheritTypeFrom.size() == 0) {
            if ((flags & ARGUMENT) != 0)
                returnList.add(this);
        } else {
            for (BCRVariable inheritVar : inheritTypeFrom)
                returnList.addAll(inheritVar.getArgumentInheritedFrom());
        }
        return returnList;
    }

    public void setType (int _type, BCRCommand command) throws RuntimeCreatorException {
        //argument type is set in runtime
        if ((flags & ARGUMENT) != 0)
            return;

        setTypeFromCast();
        int type = (flags & 0xFF);
        if (type != TYPE_UNKNOWN && _type != TYPE_UNKNOWN && type != _type)
            command.error("miss-matching types ('" + name + "') (was: " + FlagUtils.getTypeName(type) + ", is now: " + FlagUtils.getTypeName(_type) + ")");
        if (_type != TYPE_UNKNOWN)
            flags = _type | (flags & 0xFFFFFF00);
    }

    private void setTypeFromCast () {
        int type = (flags & 0xFF);
        if (type == TYPE_UNKNOWN && castTo != -1) {
            type = castTo;
            flags = type | (flags & 0xFFFFFF00);
        }
    }

    public void setTypeFromInherit (BCRVariable _inheritTypeFrom, BCRCommand command) throws RuntimeCreatorException {
        /*if (_inheritTypeFrom.getType(command) != TYPE_UNKNOWN)
            setType(_inheritTypeFrom.getType(command), command);
        else {*/
            inheritTypeFrom.add(_inheritTypeFrom);
            //just a check, the inherit is not miss-matching
            if (! this.isArgument())
                getType(command);
        //}
    }

    public boolean inherits () {
        return this.inheritTypeFrom.size() > 0;
    }

    public BCRVariable inheritFromArg (BCRCommand command) throws RuntimeCreatorException {
        if (inheritTypeFrom.size() == 0)
            return (isArgument() ? this : null);
        else if (inheritTypeFrom.size() == 1)
            return inheritTypeFrom.get(0).inheritFromArg(command);
        else {
            BCRVariable returnVar = null;
            for (int i=0; i<inheritTypeFrom.size(); i++) {
                BCRVariable foundVar = inheritTypeFrom.get(i).inheritFromArg(command);
                if (foundVar != null) {
                    if (returnVar != null && !foundVar.equals(returnVar))
                        command.error("a object-variable can not inherit from more than one argument.");
                    returnVar = foundVar;
                }
            }
            return returnVar;
        }
    }

    public BCRVariable argInheritFromThis (BCRCommand command) throws RuntimeCreatorException {
        BCRVariable returnVar = null;
        for (BCRVariable var : BytecodeCreatorRoutine.variables) {
            if (var.isArgument()) {
                if (var.inheritFrom(this)) {
                    if (returnVar != null)
                        command.error("more than one object-argument inherits from same variable.");
                    returnVar = var;
                }
            }
        }
        return returnVar;
    }

    private boolean inheritFrom (BCRVariable var) {
        if (inheritTypeFrom.size() == 0)
            return (this.equals(var));
        else if (inheritTypeFrom.size() == 1)
            return inheritTypeFrom.get(0).inheritFrom(var);
        else {
            for (int i=0; i<inheritTypeFrom.size(); i++) {
                if (inheritTypeFrom.get(i).inheritFrom(var))
                    return true;
            }
            return false;
        }
    }

    public void pushValue (BCRCommand command) throws RuntimeCreatorException {
        createPushOrStoreCode(true, command, getTypeCode(command));
    }

    public void pushValue (BCRCommand command, int convertToType) throws RuntimeCreatorException {
        createPushOrStoreCode(true, command, "" + convertToType);
    }

    public void pushValue (BCRCommand command, BCRVariable convertTo) throws RuntimeCreatorException {
        createPushOrStoreCode(true, command, "" + convertTo.getTypeCode(command));
    }

    public void storeValue (BCRCommand command) throws RuntimeCreatorException {
        createPushOrStoreCode(false, command, getTypeCode(command));
    }

    public void storeValue (BCRCommand command, int convertFromType) throws RuntimeCreatorException {
        createPushOrStoreCode(false, command, "" + convertFromType);
    }

    public void storeValue (BCRCommand command, BCRVariable convertFrom) throws RuntimeCreatorException {
        createPushOrStoreCode(false, command, "" + convertFrom.getTypeCode(command));
    }

    private void createPushOrStoreCode (boolean pushValue, BCRCommand command, String convert) throws RuntimeCreatorException {
        /*if ((flags & ARGUMENT) != 0) {
            BytecodeCreatorRoutine.snippets.add("        PQLArgument pqlArg" + globalArgCounter + " = skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(name, true) + "));\n");
            BytecodeCreatorRoutine.snippets.add("        SubCommands." + (pushValue ? "push" : "store") + "Value(mv, pqlArg" + globalArgCounter + ".localIndex, pqlArg" + globalArgCounter++ + ".type, " + (convertToType == -1 ? "pqlArg WRONG HERE .type" : convertToType) +  (pushValue ? ", pqlArg" + (globalArgCounter-1) : "") + ");\n" );
        } else {
            if (localIndex == -1)
                localIndex = localIndexCounter++;
            int type = (flags & 0xFF);
            if (type == TYPE_UNKNOWN && (inheritTypeFrom == null || (inheritTypeFrom.flags & ARGUMENT) == 0))
                command.error("type cannot be unknown, but cannot be inherited (multiple inherits not allowed too)");
            String fromType = "" + (type == TYPE_UNKNOWN ? "skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(inheritTypeFrom.name, true) + ").type" : type);
            BytecodeCreatorRoutine.snippets.add("        SubCommands." + (pushValue ? "push" : "store") + "Value(mv, skeletonBuilder.getReservedVariable(" + localIndex + "), " + fromType + ", " + (convertToType == -1 ? fromType : convertToType) + (pushValue ? ", null" : "") + ");\n");
        }*/
        BytecodeCreatorRoutine.snippets.add("        SubCommands." + (pushValue ? "push" : "store") + "Value(mv, " + getLocalIndexCode(command) + ", " + getTypeCode(command) + ", " + convert +  (pushValue ? (", " + getPQLArgumentCode(command)) : "") + ");\n" );
    }

    public String getLocalIndexCode (BCRCommand command) throws RuntimeCreatorException {
        if ((flags & ARGUMENT) != 0)
            return "skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(name, true, command) + ")).localIndex";
        else {
            if (localIndex == -1)
                localIndex = localIndexCounter++;
            return "skeletonBuilder.getReservedVariable(token, " + localIndex + ", " + getTypeCode(command) + ")";
        }
    }

    public String getTypeCode (BCRCommand command) throws RuntimeCreatorException {
        if ((flags & ARGUMENT) != 0)
            return "skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(name, true, command) + ")).type";
        else {
            int type = getType(command);
            List <BCRVariable> inheritedArguments = getArgumentInheritedFrom();

            String typesFromStr = "";
            for (int i=0; i<inheritedArguments.size(); i++)
                typesFromStr += (i != 0 ? ", " : "") + "skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(inheritedArguments.get(i).name, true, command) + ")).type";
            if (type != TYPE_UNKNOWN)
                typesFromStr += (!typesFromStr.equals("") ? ", " : "") + type;
            if (typesFromStr.equals(""))
                command.error("type of variable '" + name + "' is unkown.");
            return "skeletonBuilder.matchArgumentTypes(" + typesFromStr + ")";
            /*if (type == TYPE_UNKNOWN && inheritTypeFrom.size() > 0)
            if (type == TYPE_UNKNOWN && (inheritTypeFrom == null || (inheritTypeFrom.flags & ARGUMENT) == 0))
                command.error("type cannot be unknown, but cannot be inherited (multiple inherits not allowed too)");

            skeletonBuilder.matchArgumentTypes()

            return "" + (type == TYPE_UNKNOWN ? "skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(inheritTypeFrom.name, true) + ").type" : type);*/
        }
    }

    public String getPQLArgumentCode (BCRCommand command) throws RuntimeCreatorException {
        if ((flags & ARGUMENT) != 0)
            return "skeletonBuilder.findArgument(token.arguments.get(" + BytecodeCreatorRoutine.getVariableIndex(name, true, command) + "))";
        else
            return "null";
    }

    public boolean isArgument () {
        return ((flags & ARGUMENT) != 0);
    }

    public String getName () {
        return name;
    }

    public int getArgumentIndex () {
        return argumentIndex;
    }

    public void setObjectClass (Class _objectClass, BCRCommand command) throws RuntimeCreatorException {
        if (objectClass != null && !objectClass.equals(_objectClass))
            command.error("multiple object classes not allowed. (was: '" + objectClass.getName() + "', now: '" + objectClass.getName() + "')");
        objectClass = _objectClass;
    }

    public Class getObjectClass () {
        return objectClass;
    }

    @Override
    public boolean equals (Object other) {
        if (!(other instanceof BCRVariable))
            return false;
        return ((BCRVariable)other).name.equals(name);
    }

    @Override
    public String toString () {
        try {
            return "[BCRVariable]  " + name + ", type: " + getType(null);
        } catch (RuntimeCreatorException e) {
            e.printStackTrace();
            return "";
        }
    }
}
