/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.pql.VarConstants;
import java.util.ArrayList;
import java.util.List;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BCREnvVar implements VarConstants {    
    
    public static List <BCREnvVar> variables;       
    
    public int index;
    public int type;
    public List <BCRAccessMode.AccessMode> accessTypes;
    public boolean isConstant;
    
    public BCREnvVar (int id) {
        index = id >> VAR_INDEX_SHIFT;
        type = (id >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;        
        accessTypes = new ArrayList <BCRAccessMode.AccessMode> ();
        
        if (type == __)
            accessTypes.add(BCRAccessMode.AccessMode.Wildcard);
        else if ((id & VAR_READ_FLAG) != 0)
            accessTypes.add(BCRAccessMode.AccessMode.Read);
        else
            accessTypes.add(BCRAccessMode.AccessMode.Write);
        
        isConstant = ((id & VAR_CONST_FLAG) != 0);            
    }
    
    public static void newAccess (int id) throws Exception {
        BCREnvVar tmpEnvVar = new BCREnvVar(id);
        boolean foundMatch = false;
        for (BCREnvVar it : variables) {
            if (it.index == tmpEnvVar.index && it.type == tmpEnvVar.type) {
                foundMatch = true;
                if (it.isConstant != tmpEnvVar.isConstant)
                    throw new RuntimeCreatorException("Same Env-Var has to have consistent constant/non-constant access.");                
                if (!it.accessTypes.contains(tmpEnvVar.accessTypes.get(0)))
                    it.accessTypes.add(tmpEnvVar.accessTypes.get(0));
            }
        }
        
        if (!foundMatch)
            variables.add(tmpEnvVar);
    }
    
}
