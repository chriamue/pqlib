/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.bcr;

import rc2.ExtensionInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class ConstructorInfo {

    private String key;
    private Class constructorClass;
    private Class[] arguments;

    private String argumentsAndReturn;

    public ConstructorInfo (String _key, Class _constructorClass, Class [] _arguments) {
        key = _key;
        constructorClass = _constructorClass;
        arguments = _arguments;

        if (arguments != null) {
            argumentsAndReturn = "(";
            for (Class arg : arguments)
                argumentsAndReturn += MethodTranslate.getInternalClassName(arg);
            argumentsAndReturn += ")V";
        }
    }

    public ConstructorInfo (String _key, Class _constructorClass) {
        this(_key, _constructorClass, null);
    }

    public int getArgumentAmount () {
        return arguments.length;
    }

    public int[] getArgumentTypes () {
        int [] types = new int [arguments.length];
        for (int i=0; i<types.length; i++)
            types[i] = MethodTranslate.getTypeFromClass(arguments[i]);
        return types;
    }

    public String getArgumentsAndReturn () {
        return argumentsAndReturn;
    }

    public String getKey() {
        return key;
    }

    public Class getConstructorClass () {
        return constructorClass;
    }

    public boolean isAbstract () {
        return argumentsAndReturn == null;
    }

    public static ConstructorInfo findByInstanceName (String name) throws RuntimeCreatorException {
        for (ExtensionInterface extension : ExtensionInterface.getAllExtensions())
            for (ConstructorInfo info : extension.getConstructors())
                if (info.constructorClass.getName().replace(".", "/").equals(name))
                    return info;
        throw new RuntimeCreatorException("could not find a constructor for class: '" + name + "'");
    }

}
