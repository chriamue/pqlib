/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import java.util.ArrayList;
import java.util.List;
import rc2.FlagUtils;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BCRObjectCast {

    public static int STATIC_NEED_CLASS = 1;
    public static int DYNAMIC_NEED_CLASS = 2;
    public static int STATIC_GET_CLASS = 4;
    public static int DYNAMIC_GET_CLASS = 8;
    public static List <BCRObjectCast> objectCasts = new ArrayList <BCRObjectCast> ();

    private int codePosition;
    private int flags;
    private String needClass;
    private BCRVariable needClassVar;
    private String getClass;
    private BCRVariable getClassVar;
    private BCRCommand command;

    public BCRObjectCast (int _flags, String _needClass, BCRVariable _needClassVar, String _getClass, BCRVariable _getClassVar, BCRCommand _command) throws Exception {
        flags = _flags;
        needClass = _needClass;
        needClassVar = _needClassVar;
        getClass = _getClass;
        getClassVar = _getClassVar;
        command = _command;

        codePosition = BytecodeCreatorRoutine.snippets.getCurrentPosition();
        if ((flags & STATIC_GET_CLASS) != 0)
            getClassVar.setObjectClass(Class.forName(getClass.replace("/", ".")), command);
    }

    public static int findArrayType (BCRVariable variable, BCRCommand command) throws RuntimeCreatorException {
        for (BCRObjectCast objCast : objectCasts) {
            if (objCast.getClassVar != null && objCast.getClassVar.equals(variable)) {
                if (!objCast.getClass.startsWith("["))
                    continue;
                return FlagUtils.typeNameToExactType(objCast.getClass.substring(1));
            }
        }
        command.error("could not find the type of the array.");
        return -1;
    }

    public static void apply () throws Exception {
        for (BCRObjectCast objectCast : objectCasts) {
            if ((objectCast.flags & (STATIC_NEED_CLASS|DYNAMIC_NEED_CLASS)) != 0) {
                BCRVariable fromArg = objectCast.needClassVar.inheritFromArg(objectCast.command);
                if (fromArg != null)
                    BytecodeCreatorRoutine.snippets.insertAfter(objectCast.codePosition, "        skeletonBuilder.castObjectIfNecessary (mv, token, " + fromArg.getArgumentIndex() + ", " + ((objectCast.flags & (DYNAMIC_NEED_CLASS)) != 0 ? objectCast.needClass : "\"" + objectCast.needClass + "\"") + ");\n");
                else {
                    if ((objectCast.flags & (DYNAMIC_NEED_CLASS)) != 0)
                        objectCast.command.error("dynamic class detection just allowed for arguments");
                    //if (objectCast.needClassVar.getObjectClass() != null && !Class.forName(objectCast.needClass).isAssignableFrom(objectCast.needClassVar.getObjectClass()))

                    Class needClassObj = Object.class;
                    if (objectCast.needClassVar.getObjectClass() != null)
                        needClassObj = objectCast.needClassVar.getObjectClass();
                    if (!Class.forName(objectCast.needClass.replace("/", ".")).isAssignableFrom(needClassObj))
                        BytecodeCreatorRoutine.snippets.insertAfter(objectCast.codePosition, "        mv.visitTypeInsn(CHECKCAST, \"" + objectCast.needClass + "\");\n");
                    /*else if (objectCast.needClassVar.getObjectClass() == null)
                        objectCast.command.error("we need class: '" + objectCast.needClass + "' but we have no one specified (or it is just dynamic specified).");*/
                }
            }
            if ((objectCast.flags & (STATIC_GET_CLASS|DYNAMIC_GET_CLASS)) != 0) {
                BCRVariable toArg = objectCast.getClassVar.argInheritFromThis(objectCast.command);
                if (toArg != null)
                    BytecodeCreatorRoutine.snippets.addInitLine("        skeletonBuilder.registerObjectCast (token, " + toArg.getArgumentIndex() + ", " + ((objectCast.flags & (DYNAMIC_GET_CLASS)) != 0 ? objectCast.getClass : "\"" + objectCast.getClass + "\"") + ");\n");
            }
        }
        objectCasts = new ArrayList <BCRObjectCast> ();
    }

}
