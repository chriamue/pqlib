/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.pql.VarConstants;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BCRAccessMode extends Object implements VarConstants {

    public enum AccessMode {Read, Write, Wildcard};
    public enum IterationState {Unknown, With, Without};

    public AccessMode [] access;
    //public IterationState iterationState;

    public BCRAccessMode (String _access) throws RuntimeCreatorException {
        access = new AccessMode [_access.length()];
        for (int i=0; i<_access.length(); i++) {
            if (_access.charAt(i) == 'r')
                access[i] = AccessMode.Read;
            else if (_access.charAt(i) == 'w')
                access[i] = AccessMode.Write;
            else if (_access.charAt(i) == '_')
                access[i] = AccessMode.Wildcard;
            else
                throw new RuntimeCreatorException("unknown access mode. ( " + _access + " )");
        }
    }

    public BCRAccessMode (PQLToken token) throws RuntimeCreatorException {
        access = new AccessMode [token.arguments.size()];
        for (int i=0; i<access.length; i++) {
            if ( ( (token.arguments.get(i) >> VAR_TABLE_SHIFT) & 0x7) == TYPE_WILDCARD)
                access[i] = AccessMode.Wildcard;
            else if ( (token.arguments.get(i) & VAR_READ_FLAG) != 0)
                access[i] = AccessMode.Read;
            else
                access[i] = AccessMode.Write;
        }
    }

    @Override
    public boolean equals (Object other) {
        if (!(other instanceof BCRAccessMode))
            return false;
        if (access.length != ((BCRAccessMode)other).access.length)
            return false;
        for (int i=0; i<access.length; i++)
            if (access[i] != ((BCRAccessMode)other).access[i])
                return false;
        return true;
    }

    public int toId () {
        int offset = 0;
        for (int i=0; i<access.length; i++)
            offset += Math.pow(3, i);
        offset -= 1;

        int position = 0;
        for (int i=0; i<access.length; i++) {
            position *= 3;
            position += (access[i] == AccessMode.Read ? 0 : (access[i] == AccessMode.Write ? 1 : 2) );
        }

        return offset + position;
    }
    
    public static BCRAccessMode fromId(int id) {
        int offset = 0;
        int accessLength = 0;
        int lastOffset = -1;
        for (int i=0; true; i++) {
            if (offset > id)
                break;
            lastOffset = offset;
            offset += Math.pow(3, i+1);
            accessLength = i+1;
        }
        id -= (lastOffset);
        
        String _access = "";
        for (int i=0; i<accessLength; i++) {
            _access = (id%3 == 0 ? "r" : (id%3 == 1 ? "w" : "_")) + _access;
            id /= 3;
        }
        
        try {
            return new BCRAccessMode(_access);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*public void setIterationState (boolean withIteration) throws RuntimeCreatorException {
        if ( (iterationState == IterationState.With && !withIteration) || (iterationState == IterationState.Without && withIteration))
            throw new RuntimeCreatorException("miss-matching iteration-states. ONE access-mode has to have ALYWAYS AN iteration or NO iteration.");
        iterationState = (withIteration ? IterationState.With : IterationState.Without);
    }*/

    public boolean isCompatibleWithTemplateAccessMode(String template) {
        AccessMode [] templateAccess = new AccessMode[template.length()];
        for (int i=0; i<template.length(); i++) {
            if (template.charAt(i) == 'r')
                templateAccess[i] = AccessMode.Read;
            else if (template.charAt(i) == 'w')
                templateAccess[i] = AccessMode.Write;
            else if (template.charAt(i) == '_')
                templateAccess[i] = AccessMode.Wildcard;
            else
                templateAccess[i] = access[i];
        }
        BCRAccessMode templateMode = null;
        try {
            templateMode = new BCRAccessMode("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        templateMode.access = templateAccess;
        return templateMode.equals(this);
    }

    @Override
    public String toString () {
        String returnStr = "";
        for (int i=0; i<access.length; i++) {
            if (access[i] == AccessMode.Read)
                returnStr += "r";
            else if (access[i] == AccessMode.Write)
                returnStr += "w";
            else
                returnStr += "_";
        }
        return returnStr;
    }

    @Override
    public int hashCode () {
        return 0;
    }

}
