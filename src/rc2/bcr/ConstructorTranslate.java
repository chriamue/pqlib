/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.ASMFlagTranslations;
import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.PDefaultMap;
import java.util.List;
import rc2.FlagUtils;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class ConstructorTranslate implements VarConstants, BcFlags {

    public enum SpecialConstructor {IntArray, BooleanArray, ShortArray, FloatArray, LongArray, CharArray, ByteArray, DoubleArray, ObjectArray, None};

    private SpecialConstructor specialConstructor;
    private String instanceName;
    private ConstructorInfo constructorInfo;

    public ConstructorTranslate (String _instanceName) throws RuntimeCreatorException {
        if (_instanceName.equals("[I"))
            specialConstructor = SpecialConstructor.IntArray;
        else if (_instanceName.equals("[Z"))
            specialConstructor = SpecialConstructor.BooleanArray;
        else if (_instanceName.equals("[S"))
            specialConstructor = SpecialConstructor.ShortArray;
        else if (_instanceName.equals("[F"))
            specialConstructor = SpecialConstructor.FloatArray;
        else if (_instanceName.equals("[J"))
            specialConstructor = SpecialConstructor.LongArray;
        else if (_instanceName.equals("[C"))
            specialConstructor = SpecialConstructor.CharArray;
        else if (_instanceName.equals("[B"))
            specialConstructor = SpecialConstructor.ByteArray;
        else if (_instanceName.equals("[D"))
            specialConstructor = SpecialConstructor.DoubleArray;
        else if (_instanceName.equals("[Ljava/lang/Object;"))
            specialConstructor = SpecialConstructor.ObjectArray;
        else {
            specialConstructor = SpecialConstructor.None;
            instanceName = _instanceName;
            constructorInfo = ConstructorInfo.findByInstanceName(instanceName);
            if (constructorInfo.isAbstract())
                throw new RuntimeCreatorException("you cannot instantiate from an abstract class.");
        }
    }

    public int getExpectedArgumentAmount() {
        switch (specialConstructor) {
            case IntArray:
            case BooleanArray:
            case ShortArray:
            case FloatArray:
            case LongArray:
            case CharArray:
            case ByteArray:
            case DoubleArray:
            case ObjectArray:
                return 1;
            case None:
                return constructorInfo.getArgumentAmount();
        }
        return 0;
    }

    private int[] getExpectedType() {
        switch (specialConstructor) {
            case IntArray:
            case BooleanArray:
            case ShortArray:
            case FloatArray:
            case LongArray:
            case CharArray:
            case ByteArray:
            case DoubleArray:
            case ObjectArray:
                return new int [] {TYPE_INT};

            case None:
                return constructorInfo.getArgumentTypes();
        }
        return new int [0];
    }

    public void apply (List <String> argList, BCRCommand command) throws RuntimeCreatorException {
        if (specialConstructor == SpecialConstructor.None) {
            BytecodeCreatorRoutine.snippets.add("        mv.visitTypeInsn(NEW, \"" + instanceName + "\");\n");
            BytecodeCreatorRoutine.snippets.add("        mv.visitInsn(DUP);\n");

            for (int i=0; i<getExpectedArgumentAmount(); i++) {
                BCRVariable argument = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(argList.get(1 + i), false, command));
                if (argument.getType(command) == TYPE_UNKNOWN && !argument.inherits())
                    argument.setType(FlagUtils.getBasicType(getExpectedType()[i]), command);
                argument.pushValue(command, getExpectedType()[i]);
            }

            BytecodeCreatorRoutine.snippets.add("        mv.visitMethodInsn(INVOKESPECIAL, \"" + instanceName + "\", \"<init>\", \"" + constructorInfo.getArgumentsAndReturn() + "\", false);\n");
        } else {

            for (int i=0; i<getExpectedArgumentAmount(); i++) {
                BCRVariable argument = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(argList.get(1 + i), false, command));
                if (argument.getType(command) == TYPE_UNKNOWN && !argument.inherits())
                    argument.setType(FlagUtils.getBasicType(getExpectedType()[i]), command);
                argument.pushValue(command, getExpectedType()[i]);
            }

            if (specialConstructor == SpecialConstructor.IntArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_INT) + ");\n");
            else if (specialConstructor == SpecialConstructor.BooleanArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_BOOLEAN) + ");\n");
            else if (specialConstructor == SpecialConstructor.ShortArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_INT) + ");\n");
            else if (specialConstructor == SpecialConstructor.FloatArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_BOOLEAN) + ");\n");
            else if (specialConstructor == SpecialConstructor.LongArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_BOOLEAN) + ");\n");
            else if (specialConstructor == SpecialConstructor.CharArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_INT) + ");\n");
            else if (specialConstructor == SpecialConstructor.ByteArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_BOOLEAN) + ");\n");
            else if (specialConstructor == SpecialConstructor.DoubleArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitIntInsn(NEWARRAY, " + FlagUtils.getArrayType(TYPE_BOOLEAN) + ");\n");
            else if (specialConstructor == SpecialConstructor.ObjectArray)
                BytecodeCreatorRoutine.snippets.add("        mv.visitTypeInsn(ANEWARRAY, \"java/lang/Object\");\n");
        }
    }

}
