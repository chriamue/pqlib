/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import rc2.ExecGenSourcecode;
import rc2.ExtensionInterface;
import rc2.FlagUtils;
import rc2.RCInterface;
import rc2.StandardExtension;
import rc2.SubCommands;
import rc2.representation.CastInfo;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class BytecodeCreatorRoutine implements VarConstants, BcFlags {

    private static final int MAIN_ENTRY_POINT_AMOUNT = 5;

    public static StringWriter pwCommands;
    //protected static PrintWriter pwAdditional;
    protected static StringWriter pwAdditional;
    protected static CodeContainer snippets;

    public static List <BCRCommand> commands;
    public static List <BCRGeneric> generics;
    protected static String functionName;
    protected static List <String> currentInstructionFlags;
    public static List <BCRVariable> variables;
    public static List <BCRAccessMode> accessModes;
    protected static List <IfElse> conditionPairs;
    protected static int startFunction;
    private static Map <Integer, Boolean> snippetFoundIteration;
    private static int labelIterationCounter;

    protected static final String [] parallelReservedExpressions = {"__thread_index", "__thread_amount"};

    public static BCRVariable addVariable (String name) {
        for (BCRVariable variable : variables)
            if (variable.getName().equals(name))
                return variable;
        variables.add(new BCRVariable(name));
        return variables.get(variables.size()-1);
    }

    public static int getVariableIndex (String name, boolean haveToBeArgument) {
        for (int i=0; i<variables.size(); i++)
            if (variables.get(i).getName().equals(name) && (!haveToBeArgument || variables.get(i).isArgument()) )
                return i;
        return -1;
    }

    public static int getVariableIndex (String name, boolean haveToBeArgument, BCRCommand command) throws RuntimeCreatorException {
        int returnVal = getVariableIndex(name, haveToBeArgument);
        if (returnVal == -1)
            command.error("variable not found: '" + name + "'.");
        return returnVal;
    }

    public static void constLoadCode (int type, Object value, BCRCommand command) throws RuntimeCreatorException {
        if (type == TYPE_INT && ( ((Integer)value) >= 0 && ((Integer)value) <= 5) )
            snippets.add("        mv.visitInsn(ICONST_" + ((Integer)value) + ");\n");
        else if (type == TYPE_INT && ((Integer)value) == -1 )
            snippets.add("        mv.visitInsn(ICONST_M1);\n");
        else if (type == TYPE_DOUBLE && ( ((Double)value) == 0 && ((Double)value) == 1) )
            snippets.add("        mv.visitInsn(DCONST_" + (((Double)value) == 0 ? "0" : "1") + ");\n");
        else if (type == TYPE_LONG && ( ((Long)value) == 0 && ((Long)value) == 1) )
            snippets.add("        mv.visitInsn(LCONST_" + (((Long)value) == 0 ? "0" : "1") + ");\n");
        else if (type == TYPE_INT)
            snippets.add("        mv.visitLdcInsn(new Integer(" + ((Integer)value) + "));\n");
        else if (type == TYPE_DOUBLE)
            snippets.add("        mv.visitLdcInsn(new Double(" + ((Double)value) + "));\n");
        else if (type == TYPE_LONG)
            snippets.add("        mv.visitLdcInsn(new Long(" + ((Long)value) + "l));\n");
        else if (type == TYPE_STRING)
            snippets.add("        mv.visitLdcInsn(new String(\"" + ((String)value) + "\"));\n");
        else
            command.error("unknown constant type");
   }

    public static String getFullInstanceName (String instance) throws RuntimeCreatorException {
        if (instance.equals("short[]"))
            return short[].class.getName().replace(".", "/");
        else if (instance.equals("float[]"))
            return float[].class.getName().replace(".", "/");
        else if (instance.equals("int[]"))
            return int[].class.getName().replace(".", "/");
        else if (instance.equals("long[]"))
            return long[].class.getName().replace(".", "/");
        else if (instance.equals("char[]"))
            return char[].class.getName().replace(".", "/");
        else if (instance.equals("boolean[]"))
            return boolean[].class.getName().replace(".", "/");
        else if (instance.equals("byte[]"))
            return byte[].class.getName().replace(".", "/");
        else if (instance.equals("double[]"))
            return double[].class.getName().replace(".", "/");
        else if (instance.equals("object[]"))
            return Object[].class.getName().replace(".", "/");
        else {
            for (ExtensionInterface extension : ExtensionInterface.getAllExtensions())
                for (ConstructorInfo constructor : extension.getConstructors())
                    if (constructor.getKey().equals(instance))
                        return constructor.getConstructorClass().getName().replace(".", "/");
            throw new RuntimeCreatorException("unknown instance: '" + instance + "'");
        }
    }

    private static void parseFunction (List <String> readLines, int lineOffset, boolean firstParse) throws Exception {
        commands = new ArrayList <BCRCommand> ();
        if (readLines.size() == 1 && readLines.get(0).trim().startsWith("import")) {
            String functionName = readLines.get(0).trim().substring(7);
            FunctionParser parser = new FunctionParser (functionName);
            parser.buildCommands(firstParse);
        } else {
            for (int i=0; i<readLines.size(); i++) {
                try {
                    parseLine(readLines.get(i), i + lineOffset + 1, firstParse);
                } catch (RuntimeCreatorException e) {
                    throw new Exception("An error occured while parsing (line: " + (i + lineOffset + 1) + "): " + e.getMessage());
                }
            }
        }
    }

    private static void parseLine (String line, int lineNumber, boolean firstParse) throws Exception {

        if (line.trim().equals(""))
            return;

        while (line.contains("{gen_")) {
            int indexStart = line.indexOf("{gen_");
            int indexEnd = line.indexOf("}", indexStart);
            String genericName = line.substring(indexStart+1, indexEnd);
            boolean foundGeneric = false;
            for (int i=0; i<generics.size(); i++)
                if (generics.get(i).getName().equals(genericName)) {
                    line = line.substring(0, indexStart) + generics.get(i).getValue() + line.substring(indexEnd+1);
                    foundGeneric = true;
                }
            if (!foundGeneric)
                throw new RuntimeCreatorException("generic not found: '" + genericName + "'");

        }
        BCRCommand newCommand = new BCRCommand(line, lineNumber);
        while (line.startsWith(" ")) {
            line = line.substring(1);
            newCommand.level ++;
        }
        line = line.trim();
        String [] rawTokens = line.split("[\\s:,\\(\\)]+");
        List <String> tokens = new ArrayList <String> ();
        for (int i=0; i<rawTokens.length; i++)
            if (!rawTokens[i].trim().equals(""))
                tokens.add(rawTokens[i]);

        if (line.matches("[^ (]+\\(((\\w)+[, ]*)*\\)")) {
            newCommand.flags = BCRCommand.FUNCTION_START;
            functionName = tokens.get(0);
            if (firstParse) {
                for (int i=1; i<tokens.size(); i++)
                    variables.add(new BCRVariable(tokens.get(i), i-1));
            }
        }
        else if (line.matches(":([, ]*\\w)*")) {
            newCommand.flags = BCRCommand.ACCESS_DEFINITION;
            if (firstParse)
                for (int i=0; i<tokens.size(); i++)
                    accessModes.add(new BCRAccessMode(tokens.get(i)));
            if (accessModes.size() == 0)
                accessModes.add(new BCRAccessMode(""));
        }
        else if (line.matches("((if|ifconst|ifmode|ifnull|ifnonnull|ifinstance|ifparallelmode) ([\\w\\]\\[]*\\s*)+(\\s*(==|!=|>|<|>=|<=)[^\\s]*\\s*\\w+)?|else)")) {
            SubConditionFinal subConditionFinal = new SubConditionFinal();
            newCommand.flags = BCRCommand.IF_CONDITION;
            subConditionFinal.flags = BCRCommand.IF_CONDITION;
            if (tokens.get(0).startsWith("if")) {
                newCommand.flags |= BCRCommand.COND_IF;
                subConditionFinal.flags |= BCRCommand.COND_IF;
                newCommand.condition = subConditionFinal;
            } else if (tokens.get(0).equals("else") && tokens.size() == 1)
                newCommand.flags |= BCRCommand.COND_ELSE;
            for (int i=1; i<tokens.size(); i++)
                subConditionFinal.conditionArguments.add(tokens.get(i));

            if ((subConditionFinal.flags & BCRCommand.COND_ELSE) == 0) {
                if (tokens.get(0).equals("if"))
                    subConditionFinal.flags |= BCRCommand.COND_SIMPLE;
                else if (tokens.get(0).equals("ifconst"))
                    subConditionFinal.flags |= BCRCommand.COND_CONST;
                else if (tokens.get(0).equals("ifmode"))
                    subConditionFinal.flags |= BCRCommand.COND_MODE;
                else if (tokens.get(0).equals("ifnull"))
                    subConditionFinal.flags |= BCRCommand.COND_NULL;
                else if (tokens.get(0).equals("ifnonnull"))
                    subConditionFinal.flags |= BCRCommand.COND_NONNULL;
                else if (tokens.get(0).equals("ifinstance"))
                    subConditionFinal.flags |= BCRCommand.COND_INSTANCE;
                else if (tokens.get(0).equals("ifparallelmode"))
                    subConditionFinal.flags |= BCRCommand.COND_PARALLELMODE;
            }
        }
        else if (line.matches("=> [\\w ]+")) {
            newCommand.flags = BCRCommand.MAIN_ENTRY_POINT;
            for (int i=1; i<tokens.size(); i++)
                newCommand.mainEntryPointName += tokens.get(i) + (i < tokens.size()-1 ? " " : "");
        }
        else if (line.matches("\\w+\\s*=[^\\s]*\\s*([\\w\\.\\{\\}\\[\\]]+\\s*)+")) {
            newCommand.flags = BCRCommand.SET_VARIABLE;
            if (tokens.get(2).equals("new"))
                newCommand.flags |= BCRCommand.SET_NEW;
            else {
                try {
                    Integer.parseInt(tokens.get(2));
                    newCommand.flags |= BCRCommand.SET_CONSTANT_INIT;
                } catch (NumberFormatException e) {
                    try {
                        Double.parseDouble(tokens.get(2));
                        newCommand.flags |= BCRCommand.SET_CONSTANT_INIT;
                    } catch (NumberFormatException e2) {
                        newCommand.flags |= BCRCommand.SET_SIMPLE;
                    }
                }
            }
            if ((newCommand.flags & BCRCommand.SET_SIMPLE) != 0 && tokens.get(2).contains(".")) {
                newCommand.flags ^= BCRCommand.SET_SIMPLE;
                newCommand.flags |= BCRCommand.SET_METHOD;
            }
            if (tokens.get(1).length() != 1) {
                if ((newCommand.flags & BCRCommand.SET_SIMPLE) == 0)
                    throw new RuntimeCreatorException("types can just specified in simple assignments (a = b)");
                newCommand.convertToType = FlagUtils.getTypeFromName(tokens.get(1).substring(1), newCommand);
            }
            if (tokens.get(0).equals("_"))
                newCommand.variable = null;
            else
                newCommand.variable = addVariable(tokens.get(0));

            for (int i=2 + ((newCommand.flags & BCRCommand.SET_NEW) != 0 ? 1 : 0); i<tokens.size(); i++) {
                String [] splitTokens = tokens.get(i).split("\\.");
                for (String splitToken : splitTokens)
                    newCommand.setArguments.add(splitToken);
            }

            if ((newCommand.flags & BCRCommand.SET_SIMPLE) != 0) {
                if (newCommand.setArguments.get(0).equals("__methodStandardValue"))
                    NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.MethodType);
                else {
                    for (String parallelReservedExpression : parallelReservedExpressions)
                        if (newCommand.setArguments.get(0).equals(parallelReservedExpression))
                            NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.ParallelMode);
                }
            }
        }
        else if (line.matches("\\w+\\s*=\\s*[\\w]+\\s*[^\\s]+\\s*\\w*")) {
            newCommand.flags = BCRCommand.SET_VARIABLE | BCRCommand.SET_CALCULATION;
            newCommand.variable = addVariable(tokens.get(0));

            for (int i=2; i<tokens.size(); i++)
                newCommand.setArguments.add(tokens.get(i));
        } else if (line.matches("return\\s*(true|false)")) {
            newCommand.flags = BCRCommand.RETURN;
            newCommand.returnValue = tokens.get(1).equals("true");
        }
        else if (line.matches("goto\\s*\\w+")) {
            newCommand.flags = BCRCommand.GOTO;
            newCommand.labelName = tokens.get(1);
        }
        else if (line.matches("\\w+:")) {
            newCommand.flags = BCRCommand.LABEL;
            newCommand.labelName = tokens.get(0);
        }
        else if (line.matches("gen_.*")) {
            newCommand.flags = BCRCommand.GENERIC;
            for (int i=0; i<tokens.size(); i++)
                newCommand.genericArguments.add(tokens.get(i));

            if (firstParse) {
                BCRGeneric newGeneric = new BCRGeneric(newCommand.genericArguments.get(0));
                generics.add(newGeneric);
                pwCommands.write("                Map <String, Integer> " + newGeneric.getName() + "___MAP___ = new HashMap<String, Integer>();\n");
                pwAdditional.write("                Map <String, Integer> " + newGeneric.getName() + "___MAP___ = new HashMap<String, Integer>();\n");
                for (int j=1; j<newCommand.genericArguments.size(); j++) {
                    newGeneric.add(newCommand.genericArguments.get(j));
                    pwCommands.write("                " + newGeneric.getName() + "___MAP___.put(\"" + newCommand.genericArguments.get(j) + "\", " + (j-1) + ");\n");
                    pwAdditional.write("                " + newGeneric.getName() + "___MAP___.put(\"" + newCommand.genericArguments.get(j) + "\", " + (j-1) + ");\n");
                }
            }

        }
        else
            throw new RuntimeCreatorException("Cannot parse '" + line + "'");

        commands.add(newCommand);
    }

    private enum InitPosition {FunctionHead, AccessModes, Body};
    private static void preTranslate () throws RuntimeCreatorException
    {
        conditionPairs = new ArrayList <IfElse> ();
        InitPosition initPosition = InitPosition.FunctionHead;

        List <String> argumentNames = new ArrayList <String> ();
        for (BCRVariable var : variables)
            if (var.isArgument())
                argumentNames.add(var.getName());

        for (int i=0; i<commands.size(); i++) {
            if (initPosition == InitPosition.FunctionHead && (commands.get(i).flags & BCRCommand.GENERIC) != 0) {}
            else if (initPosition == InitPosition.FunctionHead) {
                initPosition = InitPosition.AccessModes;
                if ((commands.get(i).flags & BCRCommand.FUNCTION_START) == 0)
                    commands.get(0).error("missing function definition");
            } else if (initPosition == InitPosition.AccessModes) {
                initPosition = InitPosition.Body;
                startFunction = i+1;
                 if ((commands.get(i).flags & BCRCommand.ACCESS_DEFINITION) == 0)
                    commands.get(1).error("missing access definition");
            }

            for (int j=conditionPairs.size()-1; j>=0; j--) {
                if (conditionPairs.get(j).ifTerm.level < commands.get(i).level)
                    break;
                if (conditionPairs.get(j).endTerm == null)
                    conditionPairs.get(j).endTerm = commands.get(i);
            }

            if ((commands.get(i).flags & BCRCommand.IF_CONDITION) != 0) {
                if ((commands.get(i).flags & BCRCommand.COND_IF) != 0)
                    conditionPairs.add(new IfElse(commands.get(i)));
                else
                    for (int j=conditionPairs.size()-1; j>=0; j--)
                        if (conditionPairs.get(j).ifTerm.level == commands.get(i).level && conditionPairs.get(j).elseTerm == null) {
                            conditionPairs.get(j).elseTerm = commands.get(i);
                            conditionPairs.get(j).endTerm = null;
                            break;
                        }
            }

            if ((commands.get(i).flags & (BCRCommand.IF_CONDITION|BCRCommand.COND_IF)) == (BCRCommand.IF_CONDITION|BCRCommand.COND_IF)) {
                for (SubConditionFinal subConditionFinal : commands.get(i).condition.getAllConditions()) {
                    if ((subConditionFinal.flags & (BCRCommand.IF_CONDITION|BCRCommand.COND_CONST)) == (BCRCommand.IF_CONDITION|BCRCommand.COND_CONST) && subConditionFinal.conditionArguments.size() == 1) {
                        for (int j=0; j<argumentNames.size(); j++) {
                            if (argumentNames.get(j).equals(subConditionFinal.conditionArguments.get(0)))
                                NewMethodAttribute.addAttribute(j, NewMethodAttribute.NewMethodReason.Constant);
                        }
                    } else if (((subConditionFinal.flags & (BCRCommand.IF_CONDITION|BCRCommand.COND_NULL)) == (BCRCommand.IF_CONDITION|BCRCommand.COND_NULL) || (subConditionFinal.flags & (BCRCommand.IF_CONDITION|BCRCommand.COND_NONNULL)) == (BCRCommand.IF_CONDITION|BCRCommand.COND_NONNULL) ) && subConditionFinal.conditionArguments.size() == 1) {
                        for (int j=0; j<argumentNames.size(); j++) {
                            if (argumentNames.get(j).equals(subConditionFinal.conditionArguments.get(0)))
                                NewMethodAttribute.addAttribute(j, NewMethodAttribute.NewMethodReason.ConstantNull);
                        }
                    } else if (((subConditionFinal.flags & (BCRCommand.IF_CONDITION|BCRCommand.COND_INSTANCE)) == (BCRCommand.IF_CONDITION|BCRCommand.COND_INSTANCE) ) && subConditionFinal.conditionArguments.size() == 2) {
                        for (int j=0; j<argumentNames.size(); j++) {
                            if (argumentNames.get(j).equals(subConditionFinal.conditionArguments.get(0)))
                                NewMethodAttribute.addAttribute(j, NewMethodAttribute.NewMethodReason.Instance, getFullInstanceName(subConditionFinal.conditionArguments.get(1)));
                        }
                    } else if (((subConditionFinal.flags & (BCRCommand.IF_CONDITION|BCRCommand.COND_PARALLELMODE)) == (BCRCommand.IF_CONDITION|BCRCommand.COND_PARALLELMODE) ))
                        NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.ParallelMode);
                }
            }
            if ((commands.get(i).flags & (BCRCommand.SET_METHOD|BCRCommand.SET_VARIABLE)) == (BCRCommand.SET_METHOD|BCRCommand.SET_VARIABLE) ) {
                if (commands.get(i).setArguments.get(1).equals("__getField"))
                    NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.FieldType);
                if (commands.get(i).setArguments.get(1).equals("__callMethod"))
                    NewMethodAttribute.addAttribute(-1, NewMethodAttribute.NewMethodReason.MethodType);
                /*for (int j=0; j<argumentNames.size(); j++) {
                    if (argumentNames.get(j).equals(commands.get(i).setArguments.get(0)) && MethodTranslate.getClassName(commands.get(i).setArguments.get(1)) != null)
                        NewMethodAttribute.addAttribute(j, NewMethodAttribute.NewMethodReason.Instance, MethodTranslate.getClassName(commands.get(i).setArguments.get(1)));
                }*/
            }
        }
    }

    private static void translate (int step) throws Exception
    {
        //get the indices of the current translate-step (as example: global init / local init    or    const-variable xy / not-const    etc.. )
        int originStep = step;
        int mainEntryPoint = step % MAIN_ENTRY_POINT_AMOUNT;
        int accessMode = step / MAIN_ENTRY_POINT_AMOUNT % accessModes.size();
        int mainEntryStatus = -1;
        step /= accessModes.size()*MAIN_ENTRY_POINT_AMOUNT;

        for (BCRGeneric generic : generics) {
            generic.currentPossibility = step % generic.getMaxPossibilities();
            step /= generic.getMaxPossibilities();
        }

        for (NewMethodAttribute attr : NewMethodAttribute.newMethodAttributes) {
            attr.currentPossibility = step % attr.maxPossibilities;
            step /= attr.maxPossibilities;
        }

        //initialize variables (just on every second step, global+local init shares the variables) + if-else-terms
        if (mainEntryPoint == 0) {
            for (BCRVariable var : variables)
                var.reset();
            labelIterationCounter = 0;
        }
        for (IfElse ifElse : conditionPairs)
            ifElse.calcConstant(accessMode);

        //a map of all labels
        Map <String, LabelInfo> labels = new HashMap <String, LabelInfo> ();

        //translate first function command till end (first commands are generics, functionhead and accessmode-definition)
        for (int i=startFunction; i<commands.size(); i++) {
            boolean ignore = false;
            //if-else is constant and have to be ignored? => skip this command
            for (IfElse ifElse : conditionPairs)
                if (ifElse.ignoreCommand(commands.get(i), (mainEntryStatus != 1 || ignore) ))
                    ignore = true;

            if (!ignore) {
                if ((commands.get(i).flags & BCRCommand.MAIN_ENTRY_POINT) != 0) {
                    snippets.add("        //" + commands.get(i).origTerm.trim() + "\n");
                    snippets.add("__LINE_REFERENCE_" + commands.get(i).line + "\n");
                    snippets.add("__SRC_REFERENCE_" + commands.get(i).srcName + "\n");
                    if (commands.get(i).mainEntryPointName.equals("global init"))
                        mainEntryStatus = (mainEntryPoint == 0 ? 1 : 0);
                    else if (commands.get(i).mainEntryPointName.equals("local init"))
                        mainEntryStatus = (mainEntryPoint == 1 ? 1 : 0);
                    else if (commands.get(i).mainEntryPointName.equals("size_calc"))
                        mainEntryStatus = (mainEntryPoint == 2 ? 1 : 0);
                    else if (commands.get(i).mainEntryPointName.equals("access_cost_calc"))
                        mainEntryStatus = (mainEntryPoint == 3 ? 1 : 0);
                    else if (commands.get(i).mainEntryPointName.equals("selectivity_calc"))
                        mainEntryStatus = (mainEntryPoint == 4 ? 1 : 0);
                    else if (commands.get(i).mainEntryPointName.equals("iteration")) {
                        if (mainEntryPoint == 1) {
                            /*if (foundIteration)
                                commands.get(i).error("Multiple => iteration starts are not allowed.");*/
                            //accessModes.get(accessMode).setIterationState(true);
                            //foundIteration = true;
                            snippets.addInitLine("        skeletonBuilder.registerIterationLabel(token);\n");
                            String labelName = "__label_iteration_" + (labelIterationCounter++);
                            int jumpTo = snippets.getCurrentPosition();
                            labels.put(labelName, new LabelInfo(false, -1, jumpTo));
                            //snippets.add("        skeletonBuilder.startIterationLabel(mv, token);\n");
                        }
                        mainEntryStatus = (mainEntryPoint == 1 ? 1 : 0);
                    } else
                        commands.get(i).error("unknown main-entry-point. (" + commands.get(i).mainEntryPointName + ")");
                } else if (mainEntryStatus != 0) {
                    if (mainEntryStatus == -1)
                        commands.get(i).error("main-entry-point not set yet.");

                    snippets.add("        //" + commands.get(i).origTerm.trim() + "\n");
                    snippets.add("__LINE_REFERENCE_" + commands.get(i).line + "\n");
                    snippets.add("__SRC_REFERENCE_" + commands.get(i).srcName + "\n");
                    if ((commands.get(i).flags & BCRCommand.SET_VARIABLE) != 0) {
                        if ((commands.get(i).flags & BCRCommand.SET_SIMPLE) != 0) {
                            if (commands.get(i).setArguments.size() != 1)
                                commands.get(i).error("invalid amount of arguments (" + commands.get(i).setArguments.size() + ", 1 expected)");
                            if (commands.get(i).setArguments.get(0).equals("__methodStandardValue")) {
                                int argumentType = TYPE_UNKNOWN;
                                for (NewMethodAttribute newMethodAttribute : NewMethodAttribute.newMethodAttributes)
                                    if (newMethodAttribute.reason == NewMethodAttribute.NewMethodReason.MethodType)
                                        argumentType = FlagUtils.indexToExtendedType(newMethodAttribute.currentPossibility);
                                snippets.add("        SubCommands.constLoadRuntime(mv, " + argumentType + ", token.getMethodDefaultValue());\n");
                                commands.get(i).variable.setType(argumentType, commands.get(i));
                                commands.get(i).variable.storeValue(commands.get(i), argumentType);
                            } else if (commands.get(i).setArguments.get(0).equals(parallelReservedExpressions[0])) {
                                snippets.add("        mv.visitVarInsn(ILOAD, 1);\n");
                                commands.get(i).variable.setType(TYPE_INT, commands.get(i));
                                commands.get(i).variable.storeValue(commands.get(i), TYPE_INT);
                            } else if (commands.get(i).setArguments.get(0).equals(parallelReservedExpressions[1])) {
                                snippets.add("        mv.visitVarInsn(ILOAD, 2);\n");
                                commands.get(i).variable.setType(TYPE_INT, commands.get(i));
                                commands.get(i).variable.storeValue(commands.get(i), TYPE_INT);
                            }  else {
                                int secondVar = getVariableIndex(commands.get(i).setArguments.get(0), false);
                                if (secondVar == -1)
                                    commands.get(i).error("invalid variable on the right side. (" + commands.get(i).setArguments.get(0) + ")");

                                if (commands.get(i).variable.getName().equals("__calculated_result")) {
                                    variables.get(secondVar).pushValue(commands.get(i), TYPE_DOUBLE);
                                    snippets.add("        mv.visitVarInsn(DSTORE, 1);\n");
                                } else {
                                    if (commands.get(i).convertToType == TYPE_UNKNOWN) {
                                        int typeAlreadySet = commands.get(i).variable.getType(commands.get(i));
                                        if (typeAlreadySet == TYPE_UNKNOWN && !commands.get(i).variable.inherits()) {
                                            commands.get(i).variable.setTypeFromInherit(variables.get(secondVar), commands.get(i));
                                            variables.get(secondVar).pushValue(commands.get(i));
                                            commands.get(i).variable.storeValue(commands.get(i), variables.get(secondVar));
                                        } else {
                                            if (typeAlreadySet == TYPE_UNKNOWN) {
                                                variables.get(secondVar).pushValue(commands.get(i));
                                                commands.get(i).variable.storeValue(commands.get(i));
                                            } else {
                                                variables.get(secondVar).pushValue(commands.get(i), typeAlreadySet);
                                                commands.get(i).variable.storeValue(commands.get(i), typeAlreadySet);
                                            }
                                        }
                                    } else {
                                        variables.get(secondVar).pushValue(commands.get(i), commands.get(i).convertToType);
                                        commands.get(i).variable.storeValue(commands.get(i), commands.get(i).convertToType);
                                    }
                                }
                            }
                        } else if ((commands.get(i).flags & BCRCommand.SET_METHOD) != 0) {
                            MethodTranslate.translate(commands.get(i));
                        } else if ((commands.get(i).flags & BCRCommand.SET_CONSTANT_INIT) != 0) {
                            if (commands.get(i).setArguments.size() != 1)
                                commands.get(i).error("invalid amount of arguments");
                            if (commands.get(i).setArguments.get(0).charAt(0) == '"') {
                                commands.get(i).variable.setType(TYPE_OBJECT, commands.get(i));
                                constLoadCode(TYPE_STRING, commands.get(i).setArguments.get(0).substring(1, commands.get(i).setArguments.get(0).length()-1), commands.get(i));
                                commands.get(i).variable.storeValue(commands.get(i), TYPE_OBJECT);
                            } else {
                                try {
                                    Integer.parseInt(commands.get(i).setArguments.get(0));
                                    commands.get(i).variable.setType(TYPE_INT, commands.get(i));
                                    constLoadCode(TYPE_INT, new Integer(Integer.parseInt(commands.get(i).setArguments.get(0))), commands.get(i));
                                    commands.get(i).variable.storeValue(commands.get(i), TYPE_INT);
                                } catch (NumberFormatException e) {
                                    try {
                                        Long.parseLong(commands.get(i).setArguments.get(0));
                                        commands.get(i).variable.setType(TYPE_LONG, commands.get(i));
                                        constLoadCode(TYPE_LONG, new Long(Long.parseLong(commands.get(i).setArguments.get(0))), commands.get(i));
                                        commands.get(i).variable.storeValue(commands.get(i), TYPE_LONG);
                                    } catch (NumberFormatException e2) {
                                        commands.get(i).variable.setType(TYPE_DOUBLE, commands.get(i));
                                        constLoadCode(TYPE_DOUBLE, new Double(Double.parseDouble(commands.get(i).setArguments.get(0))), commands.get(i));
                                        commands.get(i).variable.storeValue(commands.get(i), TYPE_DOUBLE);
                                    }
                                }
                            }
                        } else if ((commands.get(i).flags & BCRCommand.SET_NEW) != 0) {
                            ConstructorTranslate constructorTranslate = new ConstructorTranslate(getFullInstanceName(commands.get(i).setArguments.get(0)));
                            if (commands.get(i).setArguments.size() != constructorTranslate.getExpectedArgumentAmount()+1)
                                commands.get(i).error("invalid amount of arguments");
                            constructorTranslate.apply(commands.get(i).setArguments, commands.get(i));
                            commands.get(i).variable.setType(TYPE_OBJECT, commands.get(i));
                            String returnTypeStr = getFullInstanceName(commands.get(i).setArguments.get(0));
                            BCRObjectCast.objectCasts.add(new BCRObjectCast( BCRObjectCast.STATIC_GET_CLASS, null, null, returnTypeStr, commands.get(i).variable, commands.get(i)));
                            commands.get(i).variable.storeValue(commands.get(i), TYPE_OBJECT);
                        } else {

                            OperatorInfo operator = OperatorInfo.find(commands.get(i).setArguments.get(1), commands.get(i));
                            if (commands.get(i).setArguments.size() != operator.getArgumentAmount() + 1)
                                commands.get(i).error("invalid amount of arguments");

                            BCRVariable [] variablesOperator = new BCRVariable[3];
                            variablesOperator[0] = commands.get(i).variable;
                            variablesOperator[1] = variables.get(getVariableIndex(commands.get(i).setArguments.get(0), false, commands.get(i)));
                            variablesOperator[2] = null;
                            if (operator.getArgumentAmount() == 2 && !Character.isDigit(commands.get(i).setArguments.get(2).charAt(0)))
                                variablesOperator[2] = variables.get(getVariableIndex(commands.get(i).setArguments.get(2), false, commands.get(i)));

                            boolean isConstant = (variablesOperator[2] == null && operator.getArgumentAmount() == 2);
                            Object constantValue = null;
                            int constantType = TYPE_UNKNOWN;

                            if (variablesOperator[0].getType(commands.get(i)) == TYPE_UNKNOWN &&
                                    variablesOperator[0].getArgumentInheritedFrom().size() == 0) {
                                if (variablesOperator[2] != null && variablesOperator[1].getType(commands.get(i)) != TYPE_UNKNOWN && variablesOperator[2].getType(commands.get(i)) != TYPE_UNKNOWN) {
                                    if (variablesOperator[1].getType(commands.get(i)) == TYPE_DOUBLE || variablesOperator[2].getType(commands.get(i)) == TYPE_DOUBLE)
                                        variablesOperator[0].setType(TYPE_DOUBLE, commands.get(i));
                                    else if (variablesOperator[1].getType(commands.get(i)) == TYPE_LONG || variablesOperator[2].getType(commands.get(i)) == TYPE_LONG)
                                        variablesOperator[0].setType(TYPE_LONG, commands.get(i));
                                    else
                                        variablesOperator[0].setType(TYPE_INT, commands.get(i));
                                } else
                                    variablesOperator[0].setTypeFromInherit(variablesOperator[1], commands.get(i));
                            }

                            String types = "";
                            for (int j=0; j<3; j++) {
                                if (j == 2 && variablesOperator[2] == null) {
                                    if (isConstant) {
                                        try {
                                            constantValue = new Integer (Integer.parseInt(commands.get(i).setArguments.get(2)));
                                            constantType = TYPE_INT;
                                        } catch (NumberFormatException e) {
                                            try {
                                                constantValue = new Long (Long.parseLong(commands.get(i).setArguments.get(2)));
                                                constantType = TYPE_LONG;
                                            } catch (NumberFormatException e2) {
                                                constantValue = new Double (Double.parseDouble(commands.get(i).setArguments.get(2)));
                                                constantType = TYPE_DOUBLE;
                                            }
                                        }
                                        types += ", " + constantType;
                                    } else
                                        types += ", " + TYPE_UNKNOWN;
                                }

                                else if (variablesOperator[j].getType(commands.get(i)) == TYPE_UNKNOWN) {
                                    List <BCRVariable> inheritedVars = variablesOperator[j].getArgumentInheritedFrom();
                                    if (inheritedVars.size() == 0) {
                                        variablesOperator[j].setType(TYPE_DOUBLE, commands.get(i));
                                        types += (j != 0 ? ", " : "") + TYPE_DOUBLE;
                                    } else
                                        types += (j != 0 ? ", " : "") + "new PQLArgument(token.arguments.get(" + inheritedVars.get(0).getArgumentIndex() + ")).type";
                                } else
                                    types += (j != 0 ? ", " : "") + variablesOperator[j].getType(commands.get(i));
                            }

                            if (variablesOperator[1].equals(variablesOperator[0]) && isConstant && operator.isPlusOrMinus() && constantType == TYPE_INT && variablesOperator[0].getType(commands.get(i)) == TYPE_INT && !variablesOperator[0].isArgument())
                                snippets.add("        mv.visitIincInsn(" + variablesOperator[0].getLocalIndexCode(commands.get(i)) + ", " + (Integer)constantValue * (operator.isMinus() ? -1 : 1) + ");\n");
                            else {
                                types = operator.getType() + ", " + types;
                                variablesOperator[1].pushValue(commands.get(i));
                                snippets.add("        SubCommands.insertOperator(mv, 0, \"" + operator.getRawCommand() + "\", " + types + ");\n");
                                if (variablesOperator[2] != null)
                                    variablesOperator[2].pushValue(commands.get(i));
                                else if (isConstant)
                                    constLoadCode(constantType, constantValue, commands.get(i));
                                snippets.add("        SubCommands.insertOperator(mv, 1, \"" + operator.getRawCommand() + "\", " + types + ");\n");

                                variablesOperator[0].storeValue(commands.get(i));
                            }

                            /*OperatorInfo operator = OperatorInfo.find(commands.get(i).setArguments.get(1), commands.get(i));
                            if (commands.get(i).setArguments.size() != operator.getArgumentAmount() + 1)
                                commands.get(i).error("invalid amount of arguments");
                            //if (!(commands.get(i).setArguments.get(1).equals("+")))
                            //    commands.get(i).error("unknown operator");
                            BCRVariable secondVar = variablesOperator.get(getVariableIndex(commands.get(i).setArguments.get(0), false, commands.get(i)));
                            BCRVariable thirdVar = null;
                            if (operator.getArgumentAmount() == 2 && !commands.get(i).setArguments.get(2).matches("\\d*"))
                                thirdVar = variablesOperator.get(getVariableIndex(commands.get(i).setArguments.get(2), false, commands.get(i)));
                            boolean isConstant = (thirdVar == null && operator.getArgumentAmount() == 2);
                            operator.setFinalType(isConstant, commands.get(i), secondVar, thirdVar);
                            if (secondVar.equals(commands.get(i).variable) && thirdVar == null && ( operator.isPlusOrMinus() ) && operator.getType() != TYPE_LONG && operator.getType() == commands.get(i).variable.getType(commands.get(i)))
                                snippets.add("        mv.visitIincInsn(" + secondVar.getLocalIndexCode(commands.get(i)) + ", " + Integer.parseInt(commands.get(i).setArguments.get(2))* (operator.isMinus() ? -1 : 1) + ");\n");
                            else {
                                if (isConstant) {
                                    secondVar.setType(operator.getType(), commands.get(i));
                                    commands.get(i).variable.setType(operator.getType(), commands.get(i));
                                } else if (!commands.get(i).variable.isArgument())
                                    commands.get(i).variable.setType(operator.getType(), commands.get(i));
                                secondVar.pushValue(commands.get(i), operator.getType());
                                if (isConstant) {
                                    Object constantValue = null;
                                    if (operator.getType() == TYPE_LONG)
                                        constantValue = new Long(Long.parseLong(commands.get(i).setArguments.get(2)));
                                    else
                                        constantValue = new Integer(Integer.parseInt(commands.get(i).setArguments.get(2)));
                                    constLoadCode(operator.getType(), constantValue, commands.get(i));
                                } else if (thirdVar != null)
                                    thirdVar.pushValue(commands.get(i), operator.getSecondArgumentType() );
                                else if (operator.isInv()) {
                                    if (operator.getType() == TYPE_INT)
                                        constLoadCode(operator.getType(), new Integer(-1), commands.get(i));
                                    else
                                        constLoadCode(operator.getType(), new Long(-1), commands.get(i));
                                }
                                snippets.add("        mv.visitInsn(" + operator.getCommand() + ");\n");
                                commands.get(i).variable.storeValue(commands.get(i), operator.getType());
                            }*/
                        }
                    } else if ((commands.get(i).flags & BCRCommand.GOTO) != 0) {
                        String labelName = commands.get(i).labelName;
                        int jumpId = snippets.add("        mv.visitJumpInsn(GOTO, [LABEL]);\n");
                        if (!labels.containsKey(labelName))
                            labels.put(labelName, new LabelInfo(true, jumpId, -1));
                        else
                            labels.get(labelName).addGoto(jumpId);
                    } else if ((commands.get(i).flags & BCRCommand.LABEL) != 0) {
                        String labelName = commands.get(i).labelName;
                        int jumpTo = snippets.getCurrentPosition();
                        if (!labels.containsKey(labelName))
                            labels.put(labelName, new LabelInfo(false, -1, jumpTo));
                        else
                            labels.get(labelName).addLabel(jumpTo, commands.get(i));
                    } else if ((commands.get(i).flags & BCRCommand.RETURN) != 0) {
                        snippets.add("        skeletonBuilder.insertReturnBehavior(mv, token, " + commands.get(i).returnValue + ");\n");
                    } else
                        commands.get(i).error("unknown command (internal error): " + commands.get(i));
                }
            }
        }

        if (mainEntryPoint == 1) {
            snippetFoundIteration.put(originStep, (labelIterationCounter > 0));

            //if we have 2 or more iterations, we have to make a switch-case at the end
            if (labelIterationCounter > 1) {
                BCRVariable iterationSwitch = BytecodeCreatorRoutine.addVariable("__iteration_switch");
                iterationSwitch.setType(TYPE_INT, BytecodeCreatorRoutine.commands.get(0));
                for (int j=0; j<labelIterationCounter; j++) {
                    LabelInfo labelInfo = labels.get("__label_iteration_" + j);
                    constLoadCode(TYPE_INT, new Integer(j), BytecodeCreatorRoutine.commands.get(0));
                    iterationSwitch.storeValue(BytecodeCreatorRoutine.commands.get(0), TYPE_INT);
                    BytecodeCreatorRoutine.snippets.insertBefore(labelInfo.jumpTo, BytecodeCreatorRoutine.snippets.removeLastLine());
                    BytecodeCreatorRoutine.snippets.insertBefore(labelInfo.jumpTo, BytecodeCreatorRoutine.snippets.removeLastLine());
                }
                snippets.add("        Label __switch_Iteration_label = skeletonBuilder.startIterationLabel(mv, token);\n");
                String iterationValues = "", newLabelValues = "";
                for (int j=0; j<labelIterationCounter; j++) {
                    iterationValues += (j==0 ? "" : ", ") + j;
                    newLabelValues += (j==0 ? "" : ", ") + "new Label()";
                }
                snippets.add("        Label [] __case_Iteration_labels = new Label [] {" + newLabelValues + "};\n");
                iterationSwitch.pushValue(BytecodeCreatorRoutine.commands.get(0), TYPE_INT);
                snippets.add("        mv.visitLookupSwitchInsn(__switch_Iteration_label, new int[]{" + iterationValues + "}, __case_Iteration_labels);\n");
                for (int j=0; j<labelIterationCounter; j++) {
                    snippets.add("        mv.visitLabel(__case_Iteration_labels[" + j + "]);\n");
                    int jumpId = snippets.add("        mv.visitJumpInsn(GOTO, [LABEL]);\n");
                    LabelInfo labelInfo = labels.get("__label_iteration_" + j);
                    labelInfo.addGoto(jumpId);
                }
            }

            //if we have 1 iteration, replace this label through the iteration-entry point
            if (labelIterationCounter == 1) {
                LabelInfo labelInfo = labels.get("__label_iteration_0");
                labels.remove("__label_iteration_0");
                snippets.insertAfter(labelInfo.jumpTo, "        skeletonBuilder.startIterationLabel(mv, token);\n");
            }

        }

        //in ignore command there is also created the code of non-constant if-else-term, so eventually some code is needed at the end
        for (IfElse ifElse : conditionPairs)
            ifElse.ignoreCommand(null, (mainEntryStatus != 1));

        //finalize GOTO/LABEL-commands
        for (LabelInfo label : labels.values())
            label.finish();

        //local variables have to reserved at runtime
        if (mainEntryPoint == MAIN_ENTRY_POINT_AMOUNT-1 && BCRVariable.localIndexCounter > 0)
            snippets.addInitLine("        skeletonBuilder.reserveNewVariables(token, " + BCRVariable.localIndexCounter + ");\n");

    }

    private static void buildSwitchCase (StringWriter writer, String switchExpression, List <String> caseExpressions, String defaultExpression, String shiftingStr, int currentProgress, int [] progress, int snippetOffset) throws ClassNotFoundException {
        int caseExprAmount = 0;
        for (String caseExpr : caseExpressions)
            if (caseExpr != null)
                caseExprAmount++;

        if (caseExprAmount > 1 || defaultExpression != null)
            writer.write(shiftingStr + "switch (" + switchExpression + ") {\n");
        for (int i=0; i<caseExpressions.size(); i++) {
            if (caseExpressions.get(i) != null) {
                if (caseExprAmount > 1 || defaultExpression != null)
                    writer.write(shiftingStr + "    case " + caseExpressions.get(i) + ":\n");
                buildSwitchCase(writer, currentProgress+1, progress, snippetOffset);
                //if (caseExprAmount > 1 || defaultExpression != null)
                //    pwCommands.write(shiftingStr + "        break;\n");
            }
            progress[currentProgress] ++;
        }
        if (defaultExpression != null) {
            writer.write(shiftingStr + "    default:\n");
            writer.write(shiftingStr + "        " + defaultExpression + "\n");
        }
        if (caseExprAmount > 1 || defaultExpression != null)
            writer.write(shiftingStr + "}\n");

        progress[currentProgress] = 0;
    }

    private static void buildSwitchCase (StringWriter writer, int currentProgress, int [] progress, int snippetOffset) throws ClassNotFoundException {
        int shifting = currentProgress*2+4;
        String shiftingStr = "";
        for (int i=0; i<shifting; i++)
            shiftingStr += "    ";

        for (int i=2+generics.size(); i<progress.length; i++)
            NewMethodAttribute.newMethodAttributes.get(i-2-generics.size()).currentPossibility = ( i >= currentProgress ? -1 : progress[i]);

        if (currentProgress == progress.length) {
            int snippetId = 0;
            for (int i=NewMethodAttribute.newMethodAttributes.size()-1; i>=0; i--) {
                snippetId *= NewMethodAttribute.newMethodAttributes.get(i).maxPossibilities;
                snippetId += progress[i+2+generics.size()];
            }
            for (int i=generics.size()-1; i>=0; i--) {
                snippetId *= generics.get(i).getMaxPossibilities();
                snippetId += progress[i+2];
            }
            snippetId *= accessModes.size();
            snippetId += progress[0];
            snippetId *= MAIN_ENTRY_POINT_AMOUNT;
            snippetId += progress[1];
            snippetId += snippetOffset;
            //pwCommands.write(shiftingStr + "System.out.println(\"visiting snippet: " + snippetId + "\");\n");
            if (writer == pwCommands)
                writer.write(shiftingStr + "return ExecGenSourcecode.codeSnippets_visitSnippet(" + snippetId + ", mv, skeletonBuilder, token);\n");
            else {
                Boolean res1 = snippetFoundIteration.get(snippetId-snippetOffset+1);
                Boolean res2 = snippetFoundIteration.get(snippetId-snippetOffset);
                Boolean res3 = snippetFoundIteration.get(snippetId-snippetOffset-1);
                writer.write(shiftingStr + "return new AdditionalInfo(" + (res1 != null ? res1 : (res2 != null ? res2 : res3)) + ", ExecGenSourcecode.codeSnippets_visitSnippet(" + (snippetId+2) + ", mv, skeletonBuilder, token));\n");
            }
        } else {
            String switchExpression = null;
            String defaultExpression = (currentProgress == 0 ? "throw new RuntimeCreatorException(\"invalid access mode. ('\" + BCRAccessMode.fromId(SubCommands.getAccessMode(token)) + \"')\");" : null);
            List <String> caseExpressions = new ArrayList <String> ();

            int maxCurrentProgress;
            if (currentProgress == 0) {
                switchExpression = "SubCommands.getAccessMode(token)";
                maxCurrentProgress = accessModes.size();
            } else if (currentProgress == 1) {
                if (writer == pwCommands) {
                    switchExpression = "translateStep";
                    maxCurrentProgress = 2;
                } else {
                    switchExpression = "translateStep";
                    maxCurrentProgress = 3;
                    //buildSwitchCase(writer, currentProgress+1, progress, snippetOffset);
                    //return;
                }
            } else if (currentProgress < generics.size()+2) {
                switchExpression = generics.get(currentProgress-2).getSwitchCommand();
                maxCurrentProgress = generics.get(currentProgress-2).getMaxPossibilities();
            } else {
                switchExpression = NewMethodAttribute.newMethodAttributes.get(currentProgress-2-generics.size()).getSwitchCommand();
                maxCurrentProgress = NewMethodAttribute.newMethodAttributes.get(currentProgress-2-generics.size()).maxPossibilities;
            }

            for (int i=0; i<maxCurrentProgress; i++) {
                if (currentProgress >= 2+generics.size()) {
                    NewMethodAttribute.newMethodAttributes.get(currentProgress-2-generics.size()).currentPossibility = i;
                    if (!NewMethodAttribute.isValidMethodAttributes()) {
                        caseExpressions.add(null);
                        continue;
                    }
                }
                caseExpressions.add( "" + (currentProgress == 0 ? accessModes.get(i).toId() : i + (currentProgress == 1 && writer != pwCommands ? 2 : 0 ) ) );
            }

            buildSwitchCase(writer, switchExpression, caseExpressions, defaultExpression, shiftingStr, currentProgress, progress, snippetOffset);
        }
    }

    /*private static void buildSwitchCase (int [] progress, int shifting, int currentProgress, int snippetOffset) {

        if (progress == null) {
            progress = new int [NewMethodAttribute.newMethodAttributes.size() + generics.size() + 2];
            for (int i=0; i<progress.length; i++)
                progress[i] = 0;
        }

        int [] newProgresses = new int [progress.length];
        for (int i=0; i<progress.length; i++)
            newProgresses[i] = progress[i];

        if (currentProgress > -1) {
            int maxCurrentProgress;
            String switchExpression;
            if (currentProgress == 0) {
                maxCurrentProgress = accessModes.size();
                switchExpression = "SubCommands.getAccessMode(token)";
            } else if (currentProgress == 1) {
                maxCurrentProgress = 2;
                switchExpression = "translateStep";
            } else if (currentProgress < generics.size()+2) {
                maxCurrentProgress = generics.get(currentProgress-2).getMaxPossibilities();
                switchExpression = generics.get(currentProgress-2).getSwitchCommand();
            } else {
                maxCurrentProgress = NewMethodAttribute.newMethodAttributes.get(currentProgress-2-generics.size()).maxPossibilities;
                switchExpression = NewMethodAttribute.newMethodAttributes.get(currentProgress-2-generics.size()).getSwitchCommand();
            }

            String shiftingStr = "";
            for (int i=0; i<shifting; i++)
                shiftingStr += "    ";

            if (progress[currentProgress] == 0)
                pwCommands.write(shiftingStr + "switch (" + switchExpression + ") {\n");

            if (progress[currentProgress] < maxCurrentProgress) {
                if (progress[currentProgress] > 0)
                    pwCommands.write(shiftingStr + "        break;\n");
                pwCommands.write(shiftingStr + "    case " + (currentProgress == 0 ? accessModes.get(progress[currentProgress]).toId() : progress[currentProgress]) + ":\n");
                if (currentProgress == progress.length-1) {
                    int snippetId = 0;
                    for (int i=NewMethodAttribute.newMethodAttributes.size()-1; i>=0; i--) {
                        snippetId *= NewMethodAttribute.newMethodAttributes.get(i).maxPossibilities;
                        snippetId += progress[i+2+generics.size()];
                    }
                    for (int i=generics.size()-1; i>=0; i--) {
                        snippetId *= generics.get(i).getMaxPossibilities();
                        snippetId += progress[i+2];
                    }
                    snippetId *= accessModes.size();
                    snippetId += progress[0];
                    snippetId *= 2;
                    snippetId += progress[1];
                    snippetId += snippetOffset;
                    pwCommands.write(shiftingStr + "        System.out.println(\"" + snippetId + "\");\n");
                    pwCommands.write(shiftingStr + "        rc2.created.gen_CodeSnippets.visitSnippet" + snippetId + "(mv, skeletonBuilder, token);\n");
                }
            } else {
                if (currentProgress == 0) {
                    pwCommands.write(shiftingStr + "        break;\n");
                    pwCommands.write(shiftingStr + "    default:\n");
                    pwCommands.write(shiftingStr + "        throw new RuntimeCreatorException(\"invalid access mode.\");\n");
                }
                pwCommands.write(shiftingStr + "}\n");
                return;
            }
        }

        if (currentProgress == progress.length-1)
            return;
        currentProgress++;

        int newMaxCurrentProgress;
        if (currentProgress == 0)
            newMaxCurrentProgress = accessModes.size();
        else if (currentProgress == 1)
            newMaxCurrentProgress = 2;
        else if (currentProgress < generics.size()+2)
            newMaxCurrentProgress = generics.get(currentProgress-2).getMaxPossibilities();
        else
            newMaxCurrentProgress = NewMethodAttribute.newMethodAttributes.get(currentProgress-2-generics.size()).maxPossibilities;

        buildSwitchCase(newProgresses, shifting + 2, currentProgress, snippetOffset);
        while (newProgresses[currentProgress]+1 <= newMaxCurrentProgress) {
            newProgresses[currentProgress] ++;
            buildSwitchCase(newProgresses, shifting + 2, currentProgress, snippetOffset);
        }
    }*/

    public static void create(ExtensionInterface ... extensions)
    {
        try {
            ExtensionInterface.registerExtensions(extensions);
            MethodTranslate.generate();
            MethodTranslateAlias.generate();
            BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream("src/rc2/MainCommands.java"), "UTF-8"));
            pwCommands = new StringWriter();
            pwAdditional = new StringWriter();
            StringWriter pwSnippets = new StringWriter();
            //pwAdditional = new PrintWriter(new File("src/rc2/created/gen_AdditionalInfos.java"));

            BCREnvVar.variables = new ArrayList <BCREnvVar> ();
            pwSnippets.write("/*\n * To change this template, choose Tools | Templates\n * and open the template in the editor.\n */\npackage rc2.__gen__;\n\nimport rc2.FlagUtils;\nimport org.objectweb.asm.*;\nimport rc2.representation.PQLArgument;\nimport rc2.SubCommands;\nimport rc2.representation.RuntimeCreatorException;\nimport rc2.representation.PQLToken;\nimport rc2.SkeletonBuilder;\n/**\n *\n * @author Hilmar\n */\npublic class CodeSnippets implements Opcodes {");
            //pwAdditional.write("/*\n * To change this template, choose Tools | Templates\n * and open the template in the editor.\n */\npackage rc2.created;\n\nimport edu.umass.bc.BcFlags;\nimport org.objectweb.asm.*;\nimport rc2.representation.PQLArgument;\nimport rc2.SubCommands;\nimport rc2.representation.RuntimeCreatorException;\nimport rc2.representation.PQLToken;\nimport rc2.SkeletonBuilder;\n/**\n *\n * @author Hilmar\n */\npublic class gen_AdditionalInfos implements BcFlags {\n    public static boolean withIteration (PQLToken token) throws RuntimeCreatorException {\n        switch(token.instruction) {\n");

            List <BCRExtension> bcrExtensions = new ArrayList <BCRExtension> ();
            for (ExtensionInterface extensionInterface : ExtensionInterface.getAllExtensions())
                for (BCRExtension bcrExtension : extensionInterface.getBCRExtensions())
                    bcrExtensions.add(bcrExtension);

            int additionalExtensionsIndex = -1;
            int additionalExtensionCasesIndex = -1;
            int snippetIndex = 0;
            int line = 1;
            currentInstructionFlags = new ArrayList <String> ();
            String nextLine;
            int snippetOffset = 0;
            boolean startParsing = false;
            while ( true ) {
                if (additionalExtensionsIndex >= 0) {
                    int extensionIndex = additionalExtensionsIndex/5;
                    if (extensionIndex == bcrExtensions.size()) {
                        additionalExtensionsIndex = -1;
                        continue;
                    }
                    int extensionOffset = additionalExtensionsIndex%5;
                    if (extensionOffset == 0)
                        nextLine = "            case " + (bcrExtensions.get(extensionIndex).getId() << 8) + ":";
                    else
                        nextLine = "                // ### BytecodeCreatorRoutine => EntrancePoint";
                    additionalExtensionsIndex++;
                } else if (additionalExtensionCasesIndex >= 0) {
                    if (additionalExtensionCasesIndex == bcrExtensions.size()) {
                        additionalExtensionCasesIndex = -1;
                        continue;
                    }
                    nextLine = "            case " + (bcrExtensions.get(additionalExtensionCasesIndex).getId() << 8) + ":";
                    additionalExtensionCasesIndex++;
                } else {
                    nextLine = in.readLine();
                    if (nextLine == null)
                        break;
                }
                if (nextLine.trim().equals("// ### BytecodeCreatorRoutine => StartParsing"))
                    startParsing = true;
                else if (nextLine.trim().equals("// ### BytecodeCreatorRoutine => Additional Extensions")) {
                    additionalExtensionsIndex = 0;
                    continue;
                } else if (nextLine.trim().equals("// ### BytecodeCreatorRoutine => Additional Extensions Case")) {
                    additionalExtensionCasesIndex = 0;
                    continue;
                } else if (nextLine.trim().equals("// ### BytecodeCreatorRoutine => EntrancePoint")) {
                    if (additionalExtensionsIndex < 0)
                        in.readLine();
                    line++;
                    commands = new ArrayList <BCRCommand> ();
                    snippetFoundIteration = new HashMap <Integer, Boolean> ();
                    functionName = "";
                    variables = new ArrayList <BCRVariable> ();
                    accessModes = new ArrayList <BCRAccessMode> ();
                    generics = new ArrayList <BCRGeneric> ();
                    startFunction = -1;
                    NewMethodAttribute.newMethodAttributes = new ArrayList <NewMethodAttribute> ();

                    int lineOffset = line;
                    List <String> readLines = new ArrayList <String> ();
                    boolean caseInBrackets = false;
                    while (true) {
                        if (additionalExtensionsIndex >= 0) {
                            int extensionIndex = additionalExtensionsIndex/5;
                            additionalExtensionsIndex += 3;
                            if (bcrExtensions.get(extensionIndex).isGeneric()) {

                                Class generic = bcrExtensions.get(extensionIndex).getGeneric();

                                pwAdditional.write("{\n");
                                pwCommands.write("{\n");
                                caseInBrackets = true;
                                for (String name : (String[])generic.getMethod("getGenerics").invoke(generic.newInstance())) {
                                    String newLine = "                String " + name + " = (String)Class.forName(\"" + generic.getName() + "\").getMethod(\"getGenericContent\", BCRExtension.class, String.class).invoke(Class.forName(\"" + generic.getName() + "\").newInstance(), token.bcrExtension, \"" + name + "\");\n";
                                    pwCommands.write(newLine);
                                    pwAdditional.write(newLine);
                                }
                            }
                            readLines.add("                import " + bcrExtensions.get(extensionIndex).getPath());
                            break;
                        } else {
                            nextLine = in.readLine();
                            line++;
                            if (nextLine == null || nextLine.trim().equals("*/"))
                                break;
                            readLines.add(nextLine);
                        }
                    }

                    parseFunction(readLines, lineOffset, true);
                    preTranslate();
                    int snippetCounter=0;
                    for (; true; snippetCounter++) {
                        //parse again, generics can have changed, but first calculate generic-position
                        boolean lastSnippet = (snippetCounter%(MAIN_ENTRY_POINT_AMOUNT*accessModes.size()) == (MAIN_ENTRY_POINT_AMOUNT*accessModes.size()-1));
                        int step = snippetCounter/MAIN_ENTRY_POINT_AMOUNT/accessModes.size();
                        for (BCRGeneric generic : generics) {
                            generic.currentPossibility = step % generic.getMaxPossibilities();
                            step /= generic.getMaxPossibilities();
                            if (generic.currentPossibility < generic.getMaxPossibilities()-1)
                                lastSnippet = false;
                        }
                        for (NewMethodAttribute attr : NewMethodAttribute.newMethodAttributes) {
                            attr.currentPossibility = step % attr.maxPossibilities;
                            step /= attr.maxPossibilities;
                            if (attr.currentPossibility < attr.maxPossibilities-1)
                                lastSnippet = false;
                        }
                        if (!NewMethodAttribute.isValidMethodAttributes()) {
                            snippetIndex++;
                            if (lastSnippet)
                                break;
                            continue;
                        }
                        parseFunction(readLines, lineOffset, false);
                        preTranslate();

                        String methodHead = "\n\n    public static Void visitSnippet" + snippetIndex++ + " (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token) throws Throwable {\n";
                        if (snippetCounter%MAIN_ENTRY_POINT_AMOUNT == 2 || snippetCounter%MAIN_ENTRY_POINT_AMOUNT == 3 || snippetCounter%MAIN_ENTRY_POINT_AMOUNT == 4)
                            methodHead += "        if (mv == null)\n            return null;\n";
                        //we write global + local init-methods together, because all init-lines should stay in the global init method
                        if (snippetCounter%MAIN_ENTRY_POINT_AMOUNT == 0) {
                            snippets = new CodeContainer();
                            snippets.addInitLine(methodHead);
                        } else
                            snippets.add(methodHead);
                        snippets.newActiveMethodStarts();

                        translate(snippetCounter);
                        snippets.add("    return null;\n    }");
                        if (snippetCounter%MAIN_ENTRY_POINT_AMOUNT == MAIN_ENTRY_POINT_AMOUNT-1) {
                            BCRObjectCast.apply();
                            snippets.finalWrite(pwSnippets);
                        }
                        if (lastSnippet)
                            break;
                    }

                    /*for (String currentInstructionFlag : currentInstructionFlags)
                        pwAdditional.write("            case " + currentInstructionFlag + ":\n");*/
                    currentInstructionFlags = new ArrayList <String> ();
                    /*pwAdditional.write("                switch (SubCommands.getAccessMode(token)) {\n");
                    for (BCRAccessMode accessMode : accessModes)
                        pwAdditional.write("                    case " + accessMode.toId() + ":\n                        return " + (accessMode.iterationState == BCRAccessMode.IterationState.With) + ";\n" );
                    pwAdditional.write("                    default:\n                        throw new RuntimeCreatorException(\"invalid accessmode of instruction. (is: \" + SubCommands.getAccessMode(token) + \")\");\n                }\n");*/

                    //buildSwitchCase(null, 2, -1, snippetOffset);
                    buildSwitchCase(pwCommands,0, new int [2+generics.size()+NewMethodAttribute.newMethodAttributes.size()], snippetOffset);
                    buildSwitchCase(pwAdditional,0, new int [2+generics.size()+NewMethodAttribute.newMethodAttributes.size()], snippetOffset);
                    snippetOffset += snippetCounter + 1;

                    if (caseInBrackets) {
                        pwCommands.write("}\n");
                        pwAdditional.write("}\n");
                    }
                    //pwCommands.write("                break;\n");

                } else {
                    if (nextLine.trim().startsWith("case ") && startParsing) {
                        if (!nextLine.trim().endsWith(":"))
                            throw new RuntimeCreatorException("case-command has to end with a ':' (line: " + line + ")");
                        currentInstructionFlags.add(nextLine.trim().substring(5).substring(0, nextLine.trim().length()-6));
                    }

                    String nextLineAdditional = nextLine.replace("static Void ", "static AdditionalInfo ").replace("static void ", "static AdditionalInfo ");
                    if (nextLine.trim().equals("public class MainCommands implements BcFlags, VarConstants, Opcodes {")) {
                        nextLine = "public class MainCommands implements BcFlags, VarConstants, Opcodes {";
                        nextLineAdditional = "public class AdditionalInfos implements BcFlags, VarConstants, Opcodes {";
                    }
                    else if (nextLine.trim().equals("package rc2;")) {
                        nextLine = "package rc2.__gen__;";
                        nextLineAdditional = "package rc2.__gen__;";
                    }
                    pwCommands.write(nextLine + "\n");
                    pwAdditional.write(nextLineAdditional + "\n");
                }
                line++;
            }

            pwSnippets.write("\n}");
            //pwAdditional.write("            default:\n                throw new RuntimeCreatorException(\"unknown instruction: '\" + token.instruction + \"'\");\n        }\n    }\n}");
            in.close();
            pwCommands.close();
            pwAdditional.close();
            pwSnippets.close();

            /*PrintWriter a = new PrintWriter(new File("D:/test.txt"));
            a.write(pwSnippets.toString());
            a.close();*/

            ExecGenSourcecode.compile(pwSnippets, pwCommands, pwAdditional);

            //pwAdditional.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main (String [] args)
    {
        BytecodeCreatorRoutine.create();
        RCInterface.main(args);
    }

}
