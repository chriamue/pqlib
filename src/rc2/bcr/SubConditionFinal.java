/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.bcr;

import static edu.umass.bc.BcFlags.TYPE_UNKNOWN;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import rc2.FlagUtils;
import rc2.RCInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class SubConditionFinal extends SubCondition {

    public int flags;
    public List <String> conditionArguments;
    public boolean negate;

    public SubConditionFinal () {
        conditionArguments = new ArrayList <String> ();
    }

    @Override
    public void calcConstant (int accessMode) throws RuntimeCreatorException {
        isConstant = false;
        constantResult = false;

        if ((flags & BCRCommand.COND_MODE) != 0) {
            isConstant = true;
            for (int i=0; i<conditionArguments.size(); i++)
                if (BytecodeCreatorRoutine.accessModes.get(accessMode).equals(new BCRAccessMode(conditionArguments.get(i))))
                    constantResult = true;
        } else if ((flags & BCRCommand.COND_PARALLELMODE) != 0) {
            isConstant = true;
            for (int i=0; i<conditionArguments.size(); i++) {
                int parallelIndex = -1;
                for (RCInterface.ParallelMode parallelMode : RCInterface.ParallelMode.values() )
                    if (parallelMode.name().toUpperCase().equals(conditionArguments.get(i).toUpperCase()))
                        parallelIndex = parallelMode.ordinal();
                for (NewMethodAttribute attr : NewMethodAttribute.newMethodAttributes)
                    if (attr.reason == NewMethodAttribute.NewMethodReason.ParallelMode && attr.currentPossibility == parallelIndex)
                        constantResult = true;
            }
        }
        else if ((flags & (BCRCommand.COND_INSTANCE|BCRCommand.COND_CONST|BCRCommand.COND_NULL|BCRCommand.COND_NONNULL)) != 0) {
            int argIndex = BytecodeCreatorRoutine.getVariableIndex(conditionArguments.get(0), true);
            if (argIndex != -1) {
                for (NewMethodAttribute attr : NewMethodAttribute.newMethodAttributes) {
                    if (attr.argumentIndex == argIndex && attr.isCondMatching( (flags & (BCRCommand.COND_INSTANCE|BCRCommand.COND_CONST|BCRCommand.COND_NULL|BCRCommand.COND_NONNULL)) ) &&
                            (conditionArguments.size() <= 1 || attr.instanceName.equals(BytecodeCreatorRoutine.getFullInstanceName(conditionArguments.get(1)))) &&
                            attr.isConstPossibility()) {
                        isConstant = true;
                        constantResult = attr.isConstPossibilityTrue();
                        if ((flags & BCRCommand.COND_NONNULL) != 0)
                            constantResult = (constantResult ? false : true);
                    }
                }
            }
        }
        if (negate && isConstant)
            constantResult = !constantResult;
    }

    @Override
    public void createCode (BCRCommand command) throws RuntimeCreatorException {
        createCode(command, true);
    }

    public void createCode (BCRCommand command, boolean proceedIfSuceed) throws RuntimeCreatorException {
        if (negate)
            proceedIfSuceed = !proceedIfSuceed;
        labelIds = new HashSet <Integer> ();
        BytecodeCreatorRoutine.snippets.add("        //" + command.origTerm.trim() + "\n");
        if ((flags & BCRCommand.COND_NULL) != 0 || (flags & BCRCommand.COND_NONNULL) != 0 || (flags & BCRCommand.COND_INSTANCE) != 0) {
            if (conditionArguments.size() != 1 + ((flags & BCRCommand.COND_INSTANCE) != 0 ? 1 : 0) )
                command.error("invalid amount of arguments");
            BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(conditionArguments.get(0), false, command)).pushValue(command, TYPE_OBJECT);
            if (((flags & BCRCommand.COND_INSTANCE) != 0)) {
                BytecodeCreatorRoutine.snippets.add("        mv.visitTypeInsn(INSTANCEOF, \"" + BytecodeCreatorRoutine.getFullInstanceName(conditionArguments.get(1)) + "\");\n");
                labelIds.add(BytecodeCreatorRoutine.snippets.add("        mv.visitJumpInsn(" + (proceedIfSuceed ? "IFEQ" : "IFNE") + ", [LABEL]);\n"));
            } else
                labelIds.add(BytecodeCreatorRoutine.snippets.add("        mv.visitJumpInsn(" + (proceedIfSuceed ? ((flags & BCRCommand.COND_NULL) != 0 ? "IFNONNULL" : "IFNULL") : ((flags & BCRCommand.COND_NULL) != 0 ? "IFNULL" : "IFNONNULL")) + ", [LABEL]);\n"));
        } else if ((flags & BCRCommand.COND_SIMPLE) != 0) {
            if (conditionArguments.size() != 3 )
                command.error("invalid amount of arguments");
            String operator = conditionArguments.get(1);
            int type = TYPE_UNKNOWN;
            if (operator.contains("{")) {
                type = FlagUtils.getTypeFromName(operator.substring(operator.indexOf("{")), command);
                operator = operator.substring(0, operator.indexOf("{"));
            }
            BCRVariable var1 = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(conditionArguments.get(0), false, command));
            int secondVarConstInt = -1;
            double secondVarConstDouble = 0;
            boolean isIntConst = true;
            BCRVariable var2 = null;
            try {
                secondVarConstInt = Integer.parseInt(conditionArguments.get(2));
            } catch (NumberFormatException e) {
                try {
                    secondVarConstDouble = Double.parseDouble(conditionArguments.get(2));
                    isIntConst = false;
                } catch (NumberFormatException e2) {
                    var2 = BytecodeCreatorRoutine.variables.get(BytecodeCreatorRoutine.getVariableIndex(conditionArguments.get(2), false, command));
                }
            }
            String compareType = "";
            if ((proceedIfSuceed && operator.equals("==")) || (!proceedIfSuceed && operator.equals("!=")))
                compareType = "Equal";
            else if ((proceedIfSuceed && operator.equals("!=")) || (!proceedIfSuceed && operator.equals("==")))
                compareType = "NotEqual";
            else if ((proceedIfSuceed && operator.equals("<")) || (!proceedIfSuceed && operator.equals(">=")))
                compareType = "Smaller";
            else if ((proceedIfSuceed && operator.equals(">")) || (!proceedIfSuceed && operator.equals("<=")))
                compareType = "Bigger";
            else if ((proceedIfSuceed && operator.equals("<=")) || (!proceedIfSuceed && operator.equals(">")))
                compareType = "SmallerEqual";
            else if ((proceedIfSuceed && operator.equals(">=")) || (!proceedIfSuceed && operator.equals("<")))
                compareType = "BiggerEqual";
            else
                command.error("unknown compare type: " + conditionArguments.get(1));
            if (var2 == null) {
                if (isIntConst)
                    labelIds.add(BytecodeCreatorRoutine.snippets.add("        SubCommands.compareValues(mv, SubCommands.CompareType." + compareType + ", " + var1.getLocalIndexCode(command) + ", " + var1.getTypeCode(command) + ", " + var1.getPQLArgumentCode(command) + ", -1, 1, null, " + type + ", " + secondVarConstInt + ", 0, [LABEL]);\n"));
                else
                    labelIds.add(BytecodeCreatorRoutine.snippets.add("        SubCommands.compareValues(mv, SubCommands.CompareType." + compareType + ", " + var1.getLocalIndexCode(command) + ", " + var1.getTypeCode(command) + ", " + var1.getPQLArgumentCode(command) + ", -2, 3, null, " + type + ", 0, " + secondVarConstDouble + ", [LABEL]);\n"));
            } else
                labelIds.add(BytecodeCreatorRoutine.snippets.add("        SubCommands.compareValues(mv, SubCommands.CompareType." + compareType + ", " + var1.getLocalIndexCode(command) + ", " + var1.getTypeCode(command) + ", " + var1.getPQLArgumentCode(command) + ", " + var2.getLocalIndexCode(command) + ", " + var2.getTypeCode(command) + ", " + var2.getPQLArgumentCode(command) + ", " + type + ", -1, 0, [LABEL]);\n"));
        } else
            command.error("this if-term cannot be not-constant");
    }

    @Override
    public List <SubConditionFinal> getAllConditions () {
        List <SubConditionFinal> returnList = new ArrayList <SubConditionFinal> ();
        returnList.add(this);
        return returnList;
    }

    @Override
    public String toString () {
        return "[SubConditionFinal] " + flags + ": " + conditionArguments;
    }

}
