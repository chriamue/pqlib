/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.bcr;

import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import java.util.HashSet;
import java.util.Set;
import rc2.FlagUtils;
import rc2.RCInterface;
import rc2.RCInterface.ParallelMode;
import rc2.bcr.NewMethodAttribute.NewMethodReason;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class IfElse implements VarConstants, BcFlags {

    enum PositionCondition {Outside, If, Else};

    public BCRCommand ifTerm;
    public BCRCommand elseTerm;
    public BCRCommand endTerm;

    public Set <Integer> ifTermLabelIds;
    public int elseTermLabelId;

    public boolean isConstant;
    public boolean constantResult;

    public IfElse (BCRCommand _ifTerm) {
        ifTerm = _ifTerm;
        elseTerm = null;
        endTerm = null;

        ifTermLabelIds = new HashSet <Integer> ();
        elseTermLabelId = -1;
    }

    public void calcConstant (int accessMode) throws Exception {
        ifTermLabelIds = new HashSet <Integer> ();
        elseTermLabelId = -1;

        for (BCRCommand command : BytecodeCreatorRoutine.commands) {
            //search for the equivalent BCRCommand, but probably with new values (new variable-names as ex.)
            if (command.equals(ifTerm)) {
                command.condition.calcConstant(accessMode);
                isConstant = command.condition.isConstant;
                constantResult = command.condition.constantResult;
                return;
            }
        }
        throw new RuntimeCreatorException("no matching If-Term found (internal error).");
    }

    public boolean ignoreCommand (BCRCommand command, boolean createNoCode) throws RuntimeCreatorException {
        if (!isConstant) {
            if (command == null) {
                if (endTerm == null && !createNoCode && ifTermLabelIds.size() > 0) {
                    if (elseTerm == null) {
                        for (Integer labelId : ifTermLabelIds)
                            BytecodeCreatorRoutine.snippets.labelJumpsHere(labelId);
                    } else
                        BytecodeCreatorRoutine.snippets.labelJumpsHere(elseTermLabelId);
                }
                return true;
            }

            if (command.equals(ifTerm)) {
                if (!createNoCode) {
                    command.condition.createCode(command);
                    ifTermLabelIds = command.condition.labelIds;
                }
                return true;
            } else if (elseTerm != null && command.equals(elseTerm)) {
                if (!createNoCode || ifTermLabelIds.size() > 0) {
                    BytecodeCreatorRoutine.snippets.add("        //" + command.origTerm.trim() + "\n");
                    elseTermLabelId = BytecodeCreatorRoutine.snippets.add("        mv.visitJumpInsn(GOTO, [LABEL]);\n");
                    for (Integer labelId : ifTermLabelIds)
                        BytecodeCreatorRoutine.snippets.labelJumpsHere(labelId);
                }
                return true;
            }  else if (endTerm != null && command.equals(endTerm)) {
                if ( (!createNoCode || ifTermLabelIds.size() > 0) && ((elseTerm != null && elseTermLabelId != -1) || (elseTerm == null && ifTermLabelIds.size() > 0))) {
                    if (elseTerm != null)
                        BytecodeCreatorRoutine.snippets.labelJumpsHere( elseTermLabelId );
                    else
                        for (Integer labelId : ifTermLabelIds)
                            BytecodeCreatorRoutine.snippets.labelJumpsHere(labelId);
                }
                return false;
            }
        } else if (command != null) {
            if ((command.equals(ifTerm)) || (elseTerm != null && command.equals(elseTerm)))
                return true;
            if (endTerm != null && command.equals(endTerm))
                return false;

            PositionCondition position = PositionCondition.Outside;
            for (int i=0; i<BytecodeCreatorRoutine.commands.size(); i++) {
                if (BytecodeCreatorRoutine.commands.get(i).equals(command)) {
                    if ( (position == PositionCondition.If && !constantResult) ||
                         (position == PositionCondition.Else && constantResult))
                        return true;
                    break;
                }
                if (BytecodeCreatorRoutine.commands.get(i).equals(ifTerm))
                    position = PositionCondition.If;
                else if (elseTerm != null && BytecodeCreatorRoutine.commands.get(i).equals(elseTerm))
                    position = PositionCondition.Else;
                else if (endTerm != null && BytecodeCreatorRoutine.commands.get(i).equals(endTerm))
                    position = PositionCondition.Outside;
            }
        }

        return false;
    }

    @Override
    public String toString () {
        String returnStr =  "[IfElse]" + (isConstant ? (" (const, " + (constantResult ? "true) " : "false) ") ) : "") + "\n* " + ifTerm.line + ": " + ifTerm.origTerm.trim() + "\n";
        if (elseTerm != null)
            returnStr += "* " + elseTerm.line + ": " + elseTerm.origTerm.trim() + "\n";
        returnStr += "* " + (endTerm == null ? "end" : endTerm.line) + ": [EndTerm]\n";
        return returnStr;
    }

}
