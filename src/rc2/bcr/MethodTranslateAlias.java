/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.bcr;

import java.util.ArrayList;
import java.util.List;
import rc2.ExtensionInterface;

/**
 *
 * @author Hilmar
 */
public class MethodTranslateAlias {

    public static List <MethodTranslateAlias> aliasMethods;

    public String key;
    public String [] convertToKey;
    public String [] instanceChecks;

    public MethodTranslateAlias (String _key, String [] _convertToKey, String [] _instanceChecks) {
        key = _key;
        convertToKey = _convertToKey;
        instanceChecks = _instanceChecks;
    }

    public static void generate () {
        aliasMethods = new ArrayList <MethodTranslateAlias> ();
        for (ExtensionInterface extension : ExtensionInterface.getAllExtensions())
            for (MethodTranslateAlias methodAlias : extension.getMethodAliases())
                aliasMethods.add(methodAlias);
    }

}
