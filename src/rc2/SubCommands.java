/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2;

import benchmarks.BenchmarkOptions;
import edu.umass.bc.ASMFlagTranslations;
import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.PSet;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import rc2.bcr.BCRAccessMode;
import rc2.bcr.BCRCommand;
import rc2.bcr.BCRVariable;
import rc2.bcr.EqualPredicate;
import rc2.bcr.OperatorInfo;
import rc2.representation.PQLArgument;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class SubCommands implements Opcodes, BcFlags, VarConstants {

    public enum CompareType {Equal, NotEqual, Smaller, Bigger, SmallerEqual, BiggerEqual};

    public static void pushValue(MethodVisitor mv, int byteCodeIndex, int fromType, int pushAsType, PQLArgument fromArg) throws RuntimeCreatorException {
        if (fromArg != null && fromArg.isConst && fromArg.type != TYPE_OBJECT)
            constLoadRuntime(mv, fromArg.type, fromArg.constValue);
        else
            mv.visitVarInsn(FlagUtils.getPushStoreCommand(TYPE_LOAD, fromType), byteCodeIndex);
        castValue(mv, fromType, pushAsType);
    }

    public static void storeValue(MethodVisitor mv, int byteCodeIndex, int storeAsType, int fromType) throws RuntimeCreatorException {
        castValue(mv, fromType, storeAsType);
        mv.visitVarInsn(FlagUtils.getPushStoreCommand(TYPE_STORE, storeAsType), byteCodeIndex);
    }

    public static void castValue (MethodVisitor mv, int from, int to) throws RuntimeCreatorException {

        if (from == TYPE_STRING)
            from = TYPE_OBJECT;
        if (to == TYPE_STRING)
            to = TYPE_OBJECT;

        int extendedToCast = -1;
        if (FlagUtils.getBasicType(from) != from) {
            //float is the only non-basic type which needs an explicit cast to basic-type
            if (from == TYPE_FLOAT)
                mv.visitInsn(FlagUtils.getCastInstruction(from, FlagUtils.getBasicType(from)));
            from = FlagUtils.getBasicType(from);
        }
        if (FlagUtils.getBasicType(to) != to) {
            extendedToCast = to;
            to = FlagUtils.getBasicType(to);
        }

        if (from == to) {}
        else if ( to == TYPE_OBJECT ) {
            switch (from) {
                case TYPE_INT:
                    mv.visitMethodInsn(INVOKESTATIC, "edu/umass/pql/Env", "canonicalInteger", "(I)Ljava/lang/Integer;", false);
                    break;
                case TYPE_LONG:
                    mv.visitMethodInsn(INVOKESTATIC, "edu/umass/pql/Env", "canonicalLong", "(J)Ljava/lang/Long;", false);
                    break;
                case TYPE_DOUBLE:
                    mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;", false);
                    break;
                default:
                    throw new RuntimeCreatorException("the specified type ('" + from + "') is not supported.");
            }
        } else if ( from == TYPE_OBJECT ) {
            String [] checkInstances = {"java/lang/Number", "java/lang/Boolean", "java/lang/Character"};
            //jump to endCheck, if we could find the instance of the object and converted it successful
            Label endCheck = new Label();
            for (int i=0; i<checkInstances.length; i++) {
                mv.visitInsn(DUP);
                mv.visitTypeInsn(INSTANCEOF, checkInstances[i]);
                //we check all possible object-instances, if the given object is not the current one, jump to notInstance
                Label notInstance = new Label();
                mv.visitJumpInsn(IFEQ, notInstance);
                mv.visitTypeInsn(CHECKCAST, checkInstances[i]);

                if (i == 0) {
                    switch (to) {
                        case TYPE_INT:
                            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Number", "intValue", "()I", false);
                            break;
                        case TYPE_LONG:
                            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Number", "longValue", "()J", false);
                            break;
                        case TYPE_DOUBLE:
                            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Number", "doubleValue", "()D", false);
                            break;
                        default:
                            throw new RuntimeCreatorException("the specified type ('" + to + "') is not supported.");
                    }
                } else if (i == 1) {
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
                    if (to != TYPE_INT)
                        mv.visitInsn(FlagUtils.getCastInstruction(TYPE_INT, to));
                } else if (i == 2) {
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C", false);
                    if (to != TYPE_INT)
                        mv.visitInsn(FlagUtils.getCastInstruction(TYPE_INT, to));
                }

                mv.visitJumpInsn(GOTO, endCheck);
                mv.visitLabel(notInstance);
            }
            mv.visitTypeInsn(NEW, "rc2/representation/RuntimeCreatorException");
            mv.visitInsn(DUP);
            mv.visitLdcInsn(new String("the specified object cannot be converted in a numerical value."));
            mv.visitMethodInsn(INVOKESPECIAL, "rc2/representation/RuntimeCreatorException", "<init>", "(Ljava/lang/String;)V", false);
            mv.visitInsn(ATHROW);
            mv.visitLabel(endCheck);
        }
        else
            mv.visitInsn( FlagUtils.getCastInstruction(from, to) );

        if (extendedToCast != -1) {
            //there is no cast-command for booleans
            if (extendedToCast == TYPE_BOOLEAN) {
                mv.visitInsn(ICONST_1);
                mv.visitInsn(IAND);
            } else
                mv.visitInsn(FlagUtils.getCastInstruction(to, extendedToCast));
        }
    }

    public static void compareValues (MethodVisitor mv, CompareType compareType, int byteCodeIndex1, int type1, PQLArgument fromArg1, int byteCodeIndex2, int type2, PQLArgument fromArg2, int castToType, int constValueInt, int constValueDouble, Label label) throws RuntimeCreatorException {

        //special mode: string-comparison
        if (castToType == TYPE_STRING) {
            if (byteCodeIndex2 == -1)
                throw new RuntimeCreatorException("string values can not be compared with constant integer value.");
            pushValue(mv, byteCodeIndex1, type1, TYPE_OBJECT, fromArg1);
            pushValue(mv, byteCodeIndex2, type2, TYPE_OBJECT, fromArg2);
            Label comparisonTrue = new Label();
            mv.visitJumpInsn(getReverseComparisonCommand(TYPE_OBJECT, CompareType.NotEqual), (compareType == CompareType.Equal ? comparisonTrue : label) );
            pushValue(mv, byteCodeIndex1, type1, TYPE_OBJECT, fromArg1);
            mv.visitJumpInsn(IFNULL, (compareType == CompareType.Equal ? label : comparisonTrue) );
            pushValue(mv, byteCodeIndex1, type1, TYPE_OBJECT, fromArg1);
            pushValue(mv, byteCodeIndex2, type2, TYPE_OBJECT, fromArg2);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "equals", "(Ljava/lang/Object;)Z", false);
            mv.visitJumpInsn( (compareType == CompareType.Equal ? IFEQ : IFNE), label);
            mv.visitLabel(comparisonTrue);
            return;
        } else if (castToType == TYPE_UNKNOWN) {
            castToType = type1;
            if (type1 != type2) {
                //we have an argument => take this type
                if (fromArg1 == null && fromArg2 != null)
                    castToType = type2;
                else if (fromArg1 != null && fromArg2 == null)
                    castToType = type1;
                else {
                    if (type1 == TYPE_OBJECT || type2 == TYPE_OBJECT)
                        castToType = TYPE_OBJECT;
                    else if (type1 == TYPE_DOUBLE || type2 == TYPE_DOUBLE)
                        castToType = TYPE_DOUBLE;
                    else
                        castToType = TYPE_LONG;
                }
            }
        }

        pushValue(mv, byteCodeIndex1, type1, castToType, fromArg1);
        if (byteCodeIndex2 == -1)
            constLoadRuntime(mv, type2, new Integer(constValueInt));
        else if (byteCodeIndex2 == -2)
            constLoadRuntime(mv, type2, new Integer(constValueDouble));
        else
            pushValue(mv, byteCodeIndex2, type2, castToType, fromArg2);

        if (castToType == TYPE_LONG)
            mv.visitInsn(LCMP);
        else if (castToType == TYPE_DOUBLE)
            mv.visitInsn(DCMPG);
        if (castToType != TYPE_UNKNOWN)
            mv.visitJumpInsn(getReverseComparisonCommand(( (castToType == TYPE_LONG || castToType == TYPE_DOUBLE) ? TYPE_UNKNOWN : castToType), compareType), label);
    }

    public static int getAccessMode (PQLToken token) throws RuntimeCreatorException {
        BCRAccessMode accessMode = new BCRAccessMode(token);
        return accessMode.toId();
    }

    public static Object [] mapToArray (Map map) {
        Set set = map.keySet();
        Object [] returnArray = new Object [set.size()*2];
        int arrayCounter = 0;
        for (Object entry : set) {
            returnArray[arrayCounter++] = entry;
            returnArray[arrayCounter++] = map.get(entry);
        }
        return returnArray;
    }

    public static void constLoadRuntime (MethodVisitor mv, int type, Object value) throws RuntimeCreatorException {
        if (value == null)
            mv.visitInsn(ACONST_NULL);
        else if (type == TYPE_INT &&  ((Integer)value) == 0  )
            mv.visitInsn(ICONST_0);
        else if (type == TYPE_INT &&  ((Integer)value) == 1  )
            mv.visitInsn(ICONST_1);
        else if (type == TYPE_INT &&  ((Integer)value) == 2  )
            mv.visitInsn(ICONST_2);
        else if (type == TYPE_INT &&  ((Integer)value) == 3  )
            mv.visitInsn(ICONST_3);
        else if (type == TYPE_INT &&  ((Integer)value) == 4  )
            mv.visitInsn(ICONST_4);
        else if (type == TYPE_INT &&  ((Integer)value) == 5  )
            mv.visitInsn(ICONST_5);
        else if (type == TYPE_INT && ((Integer)value) == -1 )
            mv.visitInsn(ICONST_M1);
        else if (type == TYPE_DOUBLE && ( ((Double)value) == 0 && ((Double)value) == 1) )
            mv.visitInsn((((Double)value) == 0 ? DCONST_0 : DCONST_1) );
        else if (type == TYPE_LONG && ( ((Long)value) == 0 && ((Long)value) == 1) )
            mv.visitInsn((((Long)value) == 0 ? LCONST_0 : LCONST_1) );
        else if (type == TYPE_INT || type == TYPE_DOUBLE || type == TYPE_LONG)
            mv.visitLdcInsn(value);
        else
            throw new RuntimeCreatorException("unknown constant type: " + type);
   }

    public static int decideInstance (Map <Integer, Class> typeInformations, int tokenArgument, String instanceName) throws ClassNotFoundException {
        return (typeInformations.containsKey( (tokenArgument  >> VAR_INDEX_SHIFT) ) ? 1 + (Class.forName(instanceName).isAssignableFrom(typeInformations.get( (tokenArgument  >> VAR_INDEX_SHIFT) )) ? 1 : 0) : 0);
    }

    private static int getReverseComparisonCommand(int type, CompareType compareType) throws RuntimeCreatorException {
        switch (type) {
            case TYPE_INT:
                switch (compareType) {
                    case Equal:
                        return IF_ICMPNE;
                    case NotEqual:
                        return IF_ICMPEQ;
                    case Smaller:
                        return IF_ICMPGE;
                    case Bigger:
                        return IF_ICMPLE;
                    case SmallerEqual:
                        return IF_ICMPGT;
                    case BiggerEqual:
                        return IF_ICMPLT;
                    default:
                        throw new RuntimeCreatorException("invalid arguments of getReverseComparisonCommand (internal error)");
                }
            case TYPE_OBJECT:
                switch (compareType) {
                    case Equal:
                        return IF_ACMPNE;
                    case NotEqual:
                        return IF_ACMPEQ;
                    default:
                        throw new RuntimeCreatorException("invalid arguments of getReverseComparisonCommand (internal error)");
                }
            case TYPE_UNKNOWN:
                switch (compareType) {
                    case Equal:
                        return IFNE;
                    case NotEqual:
                        return IFEQ;
                    case Smaller:
                        return IFGE;
                    case Bigger:
                        return IFLE;
                    case SmallerEqual:
                        return IFGT;
                    case BiggerEqual:
                        return IFLT;
                    default:
                        throw new RuntimeCreatorException("invalid arguments of getReverseComparisonCommand (internal error)");
                }
            default:
                throw new RuntimeCreatorException("invalid arguments of getReverseComparisonCommand (internal error)");
        }
    }

    public static void insertOperator (MethodVisitor mv, int index, String operator, int operatorType, int resultType, int leftType, int rightType) throws RuntimeCreatorException {

        OperatorInfo op = OperatorInfo.findFromKey(operator);
        if (operatorType == TYPE_UNKNOWN) {
            operatorType = TYPE_INT;
            if (resultType == TYPE_LONG || leftType == TYPE_LONG || rightType == TYPE_LONG)
                operatorType = TYPE_LONG;
            if (op.canBeDouble() && (resultType == TYPE_DOUBLE || leftType == TYPE_DOUBLE || rightType == TYPE_DOUBLE))
                operatorType = TYPE_DOUBLE;
        }

        if (index == 0)
            castValue(mv, leftType, operatorType);
        else if (rightType != TYPE_UNKNOWN)
            castValue(mv, rightType, operatorType);

        if (index == 1) {

            if (op.isInv()) {
                if (operatorType == TYPE_INT)
                    constLoadRuntime(mv, operatorType, new Integer(-1));
                else
                    constLoadRuntime(mv, operatorType, new Long(-1));
            }

            op.setType(operatorType);
            mv.visitInsn(FlagUtils.getArithmeticInstruction(op.getRawCommand(), operatorType));
            castValue(mv, operatorType, resultType);
        }

    }
}
