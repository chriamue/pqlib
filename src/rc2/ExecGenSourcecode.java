
package rc2;

import edu.umass.bc.RuntimeClassLoader;
import edu.umass.pql.Env;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.URI;
import java.security.SecureClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import org.objectweb.asm.MethodVisitor;
import rc2.representation.AdditionalInfo;
import rc2.representation.PQLToken;

public class ExecGenSourcecode {

    private final static String [] filePaths = {"data/gen_code/CodeSnippets", "data/gen_code/MainCommands", "data/gen_code/AdditionalInfos", "data/gen_code/DebugCodeSnippets"};
    private final static String [] fullClassNames = {"rc2.__gen__.CodeSnippets", "rc2.__gen__.MainCommands", "rc2.__gen__.AdditionalInfos", "rc2.__gen__.DebugCodeSnippets"};
    private static Class [] classes = null;

    public static AdditionalInfo additional_buildToken (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        init();
        try {
            return (AdditionalInfo)classes[2].getMethod("buildToken", MethodVisitor.class, SkeletonBuilder.class, PQLToken.class, int.class, Map.class, Env.class).invoke(null, mv, skeletonBuilder, token, translateStep, typeInformations, env);
        } catch (Throwable t) {
            while (t.getCause() != null)
                t = t.getCause();
            throw t;
        }
    }

    public static Void mainCommands_buildToken (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        init();
        try {
            return (Void)classes[1].getMethod("buildToken", MethodVisitor.class, SkeletonBuilder.class, PQLToken.class, int.class, Map.class, Env.class).invoke(null, mv, skeletonBuilder, token, translateStep, typeInformations, env);
        } catch (Throwable t) {
            while (t.getCause() != null)
                t = t.getCause();
            throw t;
        }
    }

    public static Void codeSnippets_visitSnippet (int id, MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token) throws Throwable {
        init();
        try {
            return (Void)classes[(skeletonBuilder.debugMode ? 3 : 0)].getMethod("visitSnippet" + id, MethodVisitor.class, SkeletonBuilder.class, PQLToken.class).invoke(null, mv, skeletonBuilder, token);
        } catch (Throwable t) {
            while (t.getCause() != null)
                t = t.getCause();
            throw t;
        }
    }

    private static void init () {
        if (classes != null)
            return;

        try {
            classes = new Class[filePaths.length];
            for (int i=0; i<filePaths.length; i++) {
                classes[i] = load(filePaths[i], fullClassNames[i]);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void compile (StringWriter writerSnippets, StringWriter writerMainCommands, StringWriter writerAdditional) throws Exception {

        StringBuilder snippets = new StringBuilder(), snippetsDebug = new StringBuilder();
        String [] lines = writerSnippets.toString().split("\n");
        for (String line : lines) {
            if (line.startsWith("__LINE_REFERENCE_")) {
                snippetsDebug.append("mv.visitLdcInsn(new Integer(skeletonBuilder.debugId));\nmv.visitLdcInsn(new Integer(" + Integer.parseInt(line.substring(17)) + "));\nmv.visitMethodInsn(INVOKESTATIC, \"rc2/SkeletonBuilder\", \"feedbackLine\", \"(II)V\", false);\n");
            } else if (line.startsWith("__SRC_REFERENCE_")) {
                snippetsDebug.append("mv.visitLdcInsn(new Integer(skeletonBuilder.debugId));\nmv.visitLdcInsn(new String(\"" + line.substring(16) + "\"));\nmv.visitMethodInsn(INVOKESTATIC, \"rc2/SkeletonBuilder\", \"feedbackSource\", \"(ILjava/lang/String;)V\", false);\n");
            }  else {
                snippets.append(line + "\n");
                snippetsDebug.append(line + "\n");
            }
        }

        String [] content = new String [] {snippets.toString(), writerMainCommands.toString(), writerAdditional.toString(), snippetsDebug.toString().replaceFirst("CodeSnippets", "DebugCodeSnippets")};
        for (int i=0; i<content.length; i++) {
            save(content[i], fullClassNames[i], filePaths[i]);
        }
    }

    private static void save(String source, String fullClassName, String path) throws Exception {

        StringBuilder src = new StringBuilder();
        src.append(source);
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        BCRJavaFileManager fileManager = new BCRJavaFileManager(compiler.getStandardFileManager(null, null, null));
        List<JavaFileObject> jfiles = new ArrayList<JavaFileObject>();
        jfiles.add(new CharSequenceJavaFileObject(fullClassName, src));
        List <String> options = new ArrayList ();
        options.add("-Xlint:deprecation");
        compiler.getTask(null, fileManager, null, options, null, jfiles).call();
        File file = new File(path);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(fileManager.getBytes());
        fos.close();
     }

    private static Class load (String path, String fullClassName) throws IOException {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        long size = file.length();
        byte[] data = new byte[(int)size];
        fis.read(data, 0, data.length);
        RuntimeClassLoader rcl = new RuntimeClassLoader();
        return rcl.defineClass(fullClassName, data);
    }

    public static class CharSequenceJavaFileObject extends SimpleJavaFileObject {

        private CharSequence content;

        public CharSequenceJavaFileObject(String className, CharSequence _content) {
            super(URI.create("string:///" + className.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            content = _content;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return content;
        }
    }

    public static class BCRJavaFileObject extends SimpleJavaFileObject {

        private ByteArrayOutputStream bos = new ByteArrayOutputStream();

        public BCRJavaFileObject(String name, Kind kind) {
            super(URI.create("string:///" + name.replace('.', '/') + kind.extension), kind);
        }

        public byte[] getBytes() {
            return bos.toByteArray();
        }

        @Override
        public OutputStream openOutputStream() throws IOException {
            return bos;
        }
    }

    public static class BCRJavaFileManager extends ForwardingJavaFileManager {

        private BCRJavaFileObject bcrJavaFileObject;

        public BCRJavaFileManager(StandardJavaFileManager standardJavaFileManager) {
            super(standardJavaFileManager);
        }

        @Override
        public ClassLoader getClassLoader(Location location) {
            return new SecureClassLoader() {
                @Override
                protected Class<?> findClass(String name) throws ClassNotFoundException {
                    byte[] b = bcrJavaFileObject.getBytes();
                    return super.defineClass(name, b, 0, b.length);
                }
            };
        }

        @Override
        public JavaFileObject getJavaFileForOutput(Location location, String className, Kind kind, FileObject sibling) throws IOException {
            bcrJavaFileObject = new BCRJavaFileObject(className, kind);
            return bcrJavaFileObject;
        }

        public byte [] getBytes () {
            return bcrJavaFileObject.getBytes();
        }
    }

}