/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2;

import java.lang.reflect.Field;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Hilmar
 */
public class DebugMethodVisitor extends MethodVisitor implements Opcodes {

    private int localCounter;
    private MethodVisitor childVisitor;
    private int lineNumber;

    public DebugMethodVisitor(int i) {
        super(i);
        localCounter = 0;
        lineNumber = 2;
    }

    public DebugMethodVisitor(int i, MethodVisitor mv) {
        super(i, mv);
        localCounter = 0;
        lineNumber = 2;
    }

    public DebugMethodVisitor(MethodVisitor mv) {
        super(Opcodes.ASM4);
        localCounter = 0;
        childVisitor = mv;
        lineNumber = 2;
    }

    public AnnotationVisitor visitAnnotationDefault() {
        return childVisitor.visitAnnotationDefault();
    }

    public AnnotationVisitor visitAnnotation(String string, boolean bln) {
        return childVisitor.visitAnnotation(string, bln);
    }

    public AnnotationVisitor visitParameterAnnotation(int i, String string, boolean bln) {
        return childVisitor.visitParameterAnnotation(i, string, bln);
    }

    public void visitAttribute(Attribute atrbt) {
        childVisitor.visitAttribute(atrbt);
    }

    public void visitCode() {
        childVisitor.visitCode();
    }

    public void visitFrame(int i, int i1, Object[] os, int i2, Object[] os1) {
        childVisitor.visitFrame(i, i1, os, i2, os1);
    }

    public void visitInsn(int i) {
        writeNow("visitInsn: " + getFlagName(i));
        if (!ignoreCommand())
            childVisitor.visitInsn(i);
        insertDebugInformation();
    }

    public void visitIntInsn(int i, int i1) {
        writeNow("visitIntInsn: " + getFlagName(i) + ", " + i1);
        if (!ignoreCommand())
            childVisitor.visitIntInsn(i, i1);
        insertDebugInformation();
    }

    public void visitVarInsn(int i, int i1) {
        writeNow("visitVarInsn: " + getFlagName(i) + ", " + i1);
        if (!ignoreCommand())
            childVisitor.visitVarInsn(i, i1);
        insertDebugInformation();
    }

    public void visitTypeInsn(int i, String string) {
        writeNow("visitTypeInsn: " + getFlagName(i) + ", " + string);
        if (!ignoreCommand())
            childVisitor.visitTypeInsn(i, string);
        insertDebugInformation();
    }

    public void visitFieldInsn(int i, String string, String string1, String string2) {
        writeNow("visitFieldInsn: " + getFlagName(i) + ", " + string + ", " + string1 + ", " + string2);
        if (!ignoreCommand())
            childVisitor.visitFieldInsn(i, string, string1, string2);
        insertDebugInformation();
    }

    public void visitMethodInsn(int i, String string, String string1, String string2) {
        writeNow("visitMethodInsn: " + getFlagName(i) + ", " + string + ", " + string1 + ", " + string2);
        if (!ignoreCommand())
            childVisitor.visitMethodInsn(i, string, string1, string2);
        insertDebugInformation();
    }

    public void visitMethodInsn(int i, String string, String string1, String string2, boolean bln) {
        writeNow("visitMethodInsn: " + getFlagName(i) + ", " + string + ", " + string1 + ", " + string2 + ", " + bln);
        if (!ignoreCommand())
            childVisitor.visitMethodInsn(i, string, string1, string2, bln);
        insertDebugInformation();
    }

    public void visitInvokeDynamicInsn(String string, String string1, Handle handle, Object[] os) {
        childVisitor.visitInvokeDynamicInsn(string, string1, handle, os);
    }

    public void visitJumpInsn(int i, Label label) {
        writeNow("visitJumpInsn: " + getFlagName(i) + ", " + label.toString());
        if (!ignoreCommand())
            childVisitor.visitJumpInsn(i, label);
        insertDebugInformation();
    }

    public void visitLabel(Label label) {
        writeNow("visitLabel: " + label.toString());
        if (!ignoreCommand())
            childVisitor.visitLabel(label);
        childVisitor.visitLineNumber(lineNumber++, label);
        insertDebugInformation();
    }

    public void visitLdcInsn(Object o) {
        writeNow("visitLdcInsn: " + o);
        if (!ignoreCommand())
            childVisitor.visitLdcInsn(o);
        insertDebugInformation();
    }

    public void visitIincInsn(int i, int i1) {
        writeNow("visitIincInsn: " + i + ", " + i1);
        if (!ignoreCommand())
            childVisitor.visitIincInsn(i, i1);
        insertDebugInformation();
    }

    public void visitTableSwitchInsn(int i, int i1, Label label, Label[] labels) {
        childVisitor.visitTableSwitchInsn(i, i1, label, labels);
    }

    public void visitLookupSwitchInsn(Label label, int[] ints, Label[] labels) {
        childVisitor.visitLookupSwitchInsn(label, ints, labels);
    }

    public void visitMultiANewArrayInsn(String string, int i) {
        childVisitor.visitMultiANewArrayInsn(string, i);
    }

    public void visitTryCatchBlock(Label label, Label label1, Label label2, String string) {
        childVisitor.visitTryCatchBlock(label, label1, label2, string);
    }

    public void visitLocalVariable(String string, String string1, String string2, Label label, Label label1, int i) {
        childVisitor.visitLocalVariable(string, string1, string2, label, label1, i);
    }

    public void visitLineNumber(int i, Label label) {
        childVisitor.visitLineNumber(i, label);
    }

    public void visitMaxs(int i, int i1) {
        childVisitor.visitMaxs(i, i1);
    }

    public void visitEnd() {
        childVisitor.visitEnd();
    }

    private void insertDebugInformation() {
        /*childVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        childVisitor.visitLdcInsn(new Integer(localCounter++));
        childVisitor.visitMethodInsn(INVOKESTATIC, "java/lang/String", "valueOf", "(I)Ljava/lang/String;");
        childVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V");*/
    }

    private boolean ignoreCommand() {
        return false;
    }

    private void writeNow (String str) {
        System.out.println(str);
    }

    private String getFlagName (int flag) {
        try {
            boolean ignore = true;
            for (Field field : Opcodes.class.getFields()) {
                if (ignore && field.getName().equals("NOP"))
                    ignore = false;
                if (!ignore && field.get(null).equals(new Integer(flag)))
                    return field.getName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "[UNKNOWN] (" + flag + ")   ";
    }

}
