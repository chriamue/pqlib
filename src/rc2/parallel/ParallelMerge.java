/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.parallel;

import edu.umass.pql.container.PDefaultMap;
import java.util.Map;
import java.util.Set;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public interface ParallelMerge {

    public int getResultIndex();
    public void mergeResult(Object left, Object right, PQLToken token) throws Exception;

    public static class MapParallelMerge implements ParallelMerge {

        @Override
        public int getResultIndex() {
            return 2;
        }

        @Override
        public void mergeResult(Object left, Object right, PQLToken token) throws Exception {
            Map resultMap = (Map)left;
            resultMap.putAll((Map)right);
        }
    }

    public static class DefaultMapParallelMerge implements ParallelMerge {

        @Override
        public int getResultIndex() {
            return 3;
        }

        @Override
        public void mergeResult(Object left, Object right, PQLToken token) throws Exception {
            Map resultMap = (Map)left;
            resultMap.putAll((Map)right);
        }
    }

    public static class NDefaultMapMethodAdapterParallelMerge implements ParallelMerge {

        @Override
        public int getResultIndex() {
            return 2;
        }

        @Override
        public void mergeResult(Object left, Object right, PQLToken token) throws Exception {
            PDefaultMap defaultMap = (PDefaultMap)left;
            PDefaultMap defaultMap2 = (PDefaultMap)right;
            for (Object entry : defaultMap2.keySet()) {
                if (defaultMap.containsKey(entry))
                    defaultMap.put(entry, token.method.invoke(null, (int)((Integer)defaultMap.get(entry)), (int)((Integer) (defaultMap2.get(entry))) ));
                else
                    defaultMap.put(entry, defaultMap2.get(entry));
            }
        }
    }

    public static class SetParallelMerge implements ParallelMerge {

        @Override
        public int getResultIndex() {
            return 1;
        }

        @Override
        public void mergeResult(Object left, Object right, PQLToken token) throws Exception {
            Set resultSet = (Set)left;
            resultSet.addAll((Set)right);
        }
    }

    public static class MethodAdapterParallelMerge implements ParallelMerge {

        @Override
        public int getResultIndex() {
            throw new RuntimeException("method-adapter parallel merge currently not supported.");
        }

        @Override
        public void mergeResult(Object left, Object right, PQLToken token) throws Exception {
            throw new RuntimeException("method-adapter parallel merge currently not supported.");
        }
    }

}
