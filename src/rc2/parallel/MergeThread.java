/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.parallel;

import static edu.umass.bc.BcFlags.REDUCTOR_MAP;
import static edu.umass.bc.BcFlags.REDUCTOR_SET;
import edu.umass.pql.container.PDefaultMap;
import java.util.Map;
import java.util.Set;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class MergeThread extends Thread {

    private ParallelExecution parent;
    private PQLToken highestReductor;
    private Object result1;
    private Object result2;

    public MergeThread(ParallelExecution _parent, PQLToken _highestReductor, Object _result1, Object _result2) {
        parent = _parent;
        highestReductor = _highestReductor;
        result1 = _result1;
        result2 = _result2;
    }

    @Override
    public void run () {
        try {
            /*if (highestReductor.instruction == REDUCTOR_SET) {
                Set resultSet = (Set)result1;
                resultSet.addAll((Set)result2);
            } else if (highestReductor.instruction == REDUCTOR_MAP) {
                Map resultMap = (Map)result1;
                resultMap.putAll((Map)result2);
            } else if (highestReductor.instruction == TYPE_EXTENSION_N_DEFAULT_MAP_METHOD_ADAPTER) {
                PDefaultMap defaultMap = (PDefaultMap)result1;
                PDefaultMap defaultMap2 = (PDefaultMap)result2;
                for (Object entry : defaultMap2.keySet()) {
                    if (defaultMap.containsKey(entry))
                        defaultMap.put(entry, highestReductor.method.invoke(null, (int)((Integer)defaultMap.get(entry)), (int)((Integer) (defaultMap2.get(entry))) ));
                    else
                        defaultMap.put(entry, defaultMap2.get(entry));
                }
            } else
                throw new RuntimeCreatorException("Object-types of merging output is of unknown types.");*/
        } catch (Throwable t) {
            parent.addError(t.toString());
        }
    }

}
