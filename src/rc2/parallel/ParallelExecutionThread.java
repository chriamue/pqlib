/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.parallel;

import edu.umass.pql.Env;
import rc2.RCInterface;

/**
 *
 * @author Hilmar
 */
public class ParallelExecutionThread extends Thread {

    private int [] fromTo;
    private RCInterface parent;
    private Env env;
    private ParallelExecution parallelExecution;

    public ParallelExecutionThread (int [] _fromTo, ParallelExecution _parallelExecution, RCInterface _parent, Env _env) {
        super();
        fromTo = _fromTo;
        parent = _parent;
        env = _env;
        parallelExecution = _parallelExecution;
    }

    @Override
    public void run () {
        try {
            parallelExecution.setReturnValue(parent.execRange(env, fromTo[0], fromTo[1]));
        } catch (Throwable t) {
            parallelExecution.addError(t.toString());
        }
    }

}
