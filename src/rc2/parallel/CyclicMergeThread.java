/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2.parallel;

import static edu.umass.bc.BcFlags.REDUCTOR_MAP;
import static edu.umass.bc.BcFlags.REDUCTOR_SET;
import edu.umass.pql.Env;
import edu.umass.pql.container.PDefaultMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CyclicBarrier;
import rc2.RCInterface;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class CyclicMergeThread implements Runnable {

    private ParallelExecution parent;
    private PQLToken highestReductor;
    private int index;
    private int n;
    private int k;
    private CyclicBarrier [][] barriers;
    private Env [] envs;
    private int objectIndex;

    private int [] fromTo;
    private RCInterface rcInterface;

    public CyclicMergeThread(ParallelExecution _parent, PQLToken _highestReductor, Env [] _envs, int _objectIndex, int _index, int _n, int _k, CyclicBarrier [][] _barriers, int [] _fromTo, RCInterface _rcInterface) {
        super();
        parent = _parent;
        highestReductor = _highestReductor;
        index = _index;
        n = _n;
        k = _k;
        envs = _envs;
        objectIndex = _objectIndex;
        barriers = _barriers;
        fromTo = _fromTo;
        rcInterface = _rcInterface;
    }

    private void merge (int from, int to) throws Exception {
        Object result1 = envs[from].v_object[objectIndex];
        Object result2 = envs[to].v_object[objectIndex];

        if (highestReductor.parallelMerge != null)
            highestReductor.parallelMerge.mergeResult(result1, result2, highestReductor);

        /*if (highestReductor.instruction == REDUCTOR_SET) {
            Set resultSet = (Set)result1;
            resultSet.addAll((Set)result2);
        } else if (highestReductor.instruction == REDUCTOR_MAP) {
            Map resultMap = (Map)result1;
            resultMap.putAll((Map)result2);
        } else if (highestReductor.instruction == TYPE_EXTENSION_N_DEFAULT_MAP_METHOD_ADAPTER) {
            PDefaultMap defaultMap = (PDefaultMap)result1;
            PDefaultMap defaultMap2 = (PDefaultMap)result2;
            for (Object entry : defaultMap2.keySet()) {
                if (defaultMap.containsKey(entry))
                    defaultMap.put(entry, highestReductor.method.invoke(null, (int)((Integer)defaultMap.get(entry)), (int)((Integer) (defaultMap2.get(entry))) ));
                else
                    defaultMap.put(entry, defaultMap2.get(entry));
            }
        } */
        else
            throw new RuntimeCreatorException("Object-types of merging output is of unknown types.");
    }

    @Override
    public void run () {
        try {
            parent.setReturnValue(rcInterface.execRange(envs[index], fromTo[0], fromTo[1]));

            for (int j=0; j<=n; j++) {
                int sync_i = (index & (~((1 << (j + 1)) - 1)));
                int other_i = (sync_i | (1 << j));
                if (other_i < k) {
                    barriers[sync_i][j].await();
                    if (sync_i != index)
                        break;
                    else
                        merge(index, other_i);
                } else if (sync_i == 0 && j == n)
                    barriers[sync_i][j].await();
            }

        } catch (Throwable t) {
            parent.addError(t.toString());
        }
    }

}
