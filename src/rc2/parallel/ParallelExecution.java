/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.parallel;

import benchmarks.BenchmarkOptions;
import edu.umass.bc.BcFlags;
import edu.umass.pql.Env;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import rc2.RCInterface;
import rc2.parallel.ParallelMerge.DefaultMapParallelMerge;
import rc2.parallel.ParallelMerge.MapParallelMerge;
import rc2.parallel.ParallelMerge.MethodAdapterParallelMerge;
import rc2.parallel.ParallelMerge.SetParallelMerge;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class ParallelExecution implements BcFlags {

    private RCInterface parent;
    private Env env;
    private String errorList;
    private int [] resultObjectIndices;
    private boolean returnValue;

    public ParallelExecution (RCInterface _parent, Env _env) {
        parent = _parent;
        env = _env;
        resultObjectIndices = new int [0];
    }

    public ParallelExecution (RCInterface _parent, Env _env, int ... _resultObjectIndices) {
        parent = _parent;
        env = _env;
        resultObjectIndices = _resultObjectIndices;
    }

    private int getThreadsAmount () {
        return BenchmarkOptions.parallelCores;
    }

    private int [][] getParallelInfos (PQLToken token, int maxThreads) throws RuntimeCreatorException {
        int [][] infos = new int [maxThreads][2];
        for (int i=0; i<maxThreads; i++) {
            infos[i][0] = i;
            infos[i][1] = maxThreads;
        }
        /*int totalSize = 0;
        if (token.instruction == TYPE_CONTAINS) {
            Object obj = env.v_object[parent.skeletonBuilder.findArgument(token.arguments.get(0)).index];
            if (! (obj instanceof Set))
                throw new RuntimeCreatorException("object not of type 'Set', code is:\n" + parent.originPQLToken.toString());
            if ((obj instanceof PSet))
                totalSize = ((PSet)obj).getRepresentation().length;
            else
                totalSize = ((Set)obj).size();
        } else if (token.instruction == TYPE_ARRAY_LOOKUP) {
            Object obj = env.v_object[parent.skeletonBuilder.findArgument(token.arguments.get(0)).index];
            if (! (obj instanceof Object[]))
                throw new RuntimeCreatorException("object not of type 'Array', code is:\n" + parent.originPQLToken.toString());
            totalSize = ((Object[])obj).length;
        }
        int sizePerThread = totalSize / maxThreads;
        if (sizePerThread <= 0)
            throw new RuntimeCreatorException("parallelization input is too small to parallelize, code is:\n" + parent.originPQLToken.toString());
        for (int i=0; i<maxThreads; i++) {
            if (i > 0)
                infos[i][0] = infos[i-1][1]+1;
            infos[i][1] = infos[i][0]+sizePerThread-1;
            if (i == maxThreads-1)
                infos[i][1] = totalSize-1;
        }*/
        return infos;
    }

    public static int getStandardParallelArgIndex (PQLToken token) {
        if (token.instruction == TYPE_CONTAINS)
            return 0;
        else if (token.instruction == TYPE_ARRAY_LOOKUP)
            return 0;
        return -1;
    }

    private static int getTokenParallelArgIndex (PQLToken token) {
        return token.parallelArgIndex;
    }

    private boolean tokenParallelizable (PQLToken token) {
        int index = getTokenParallelArgIndex(token);
        if (index < 0)
            return false;
        return (parent.skeletonBuilder.findArgument(token.arguments.get(index)).isConst );
    }

    private int getResultObjectIndex (PQLToken token) throws RuntimeCreatorException {
        /*if (token.instruction == REDUCTOR_MAP)
            return parent.skeletonBuilder.findArgument(token.arguments.get(2)).index;
        if (token.instruction == REDUCTOR_DEFAULTMAP)
            return parent.skeletonBuilder.findArgument(token.arguments.get(3)).index;
        if (token.instruction == REDUCTOR_SET)
            return parent.skeletonBuilder.findArgument(token.arguments.get(1)).index;
        if (token.instruction == TYPE_EXTENSION_N_DEFAULT_MAP_METHOD_ADAPTER)
            return parent.skeletonBuilder.findArgument(token.arguments.get(2)).index;*/
        throw new RuntimeCreatorException("unknown result-reductor (internal error), code is:\n" + parent.originPQLToken.toString());
    }

    private void mergeResults (Env [] envs, int objectIndex, PQLToken highestReductor) throws Exception {

        errorList = "";

        int multiplicator = 1;
        int currentRange = getThreadsAmount();
        while (currentRange > 1) {
            if (currentRange%2 != 0)
                throw new RuntimeCreatorException("threads have to be 2^x");
            currentRange /= 2;
            multiplicator *= 2;
            MergeThread [] threadPool = new MergeThread [currentRange];
            for (int i=0; i<currentRange; i++) {
                threadPool[i] = new MergeThread(this, highestReductor, envs[i*multiplicator].v_object[objectIndex], envs[i*multiplicator+multiplicator/2].v_object[objectIndex]);
                threadPool[i].start();
            }
            for (int i=0; i<currentRange; i++)
                threadPool[i].join();
        }

        if (!errorList.equals(""))
            throw new RuntimeCreatorException("error occured while parallel merging:\n" + errorList + "\ncode is:\n" + parent.originPQLToken.toString());
    }

    private void calcAndMerge (Env [] envs, int objectIndex, PQLToken highestReductor, int [][] parallelInfos) throws Exception {
        errorList = "";

        int k = getThreadsAmount(), n=0, countN = k;
        while ((countN & 1) == 0) {
            countN >>= 1;
            n++;
        }
        CyclicBarrier [][] barriers = new CyclicBarrier[k][n+1];
        for (int i=0; i<k; i++)
            for (int j=0; j<n+1; j++)
                barriers[i][j] = new CyclicBarrier(2);

        ExecutorService threadPool = Executors.newFixedThreadPool(k);
        for (int i=0; i<k; i++) {
            //parent.execRange(envs[i], parallelInfos[i][0], parallelInfos[i][1]);
            threadPool.execute(new CyclicMergeThread(this, highestReductor, envs, objectIndex, i, n, k, barriers, parallelInfos[i], parent));
        }

        barriers[0][n].await();
        threadPool.shutdown();
        //threadPool.awaitTermination(1, TimeUnit.DAYS);
        //threadPool.shutdown();

        if (!errorList.equals(""))
            throw new RuntimeCreatorException("error occured while parallel calculating or merging:\n" + errorList + "\ncode is:\n" + parent.originPQLToken.toString());

    }

    public static ParallelMerge getStandardParallelMerge (PQLToken token) throws RuntimeCreatorException {
        if (token.instruction == REDUCTOR_MAP)
            return new MapParallelMerge();
        else if (token.instruction == REDUCTOR_SET)
            return new SetParallelMerge();
        else if (token.instruction == REDUCTOR_DEFAULTMAP)
            return new DefaultMapParallelMerge();
        else if (token.instruction == REDUCTOR_METHOD_ADAPTER)
            return new MethodAdapterParallelMerge();
        else
            throw new RuntimeCreatorException("unknown result-reductor (internal error): " + token);
    }

    public boolean run () throws Exception {
        int threads = getThreadsAmount();
        Env [] partEnvs = new Env[threads];
        for (int i=0; i<threads; i++)
            partEnvs[i] = env.copy();

        PQLToken highestReductor = null;
        if (resultObjectIndices.length == 0) {
            for (PQLToken token : parent.startPQLToken.getAllChildsInOrder())
                if (token.isReductor()) {
                    highestReductor = token;
                    resultObjectIndices = new int [1];
                    if (token.parallelMerge != null)
                        resultObjectIndices[0] = parent.skeletonBuilder.findArgument(token.arguments.get(token.parallelMerge.getResultIndex())).index;
                    else
                        resultObjectIndices[0] = getResultObjectIndex(token);
                    break;
                }
        }

        PQLToken parallelToken = null;
        for (PQLToken token : parent.startPQLToken.getAllChildsInOrder())
            if (token.parallelModeIndex != 0) {
                parallelToken = token;
                break;
            }

        /*if (!tokenParallelizable(parallelToken))
            throw new RuntimeCreatorException("specified code not parallelizable, code is:\n" + parent.originPQLToken.toString());*/

        errorList = "";
        returnValue = true;
        int [][] parallelInfos = getParallelInfos(parallelToken, threads);
        /*ParallelExecutionThread[]threadPool = new ParallelExecutionThread[threads];
        for (int i=0; i<threads; i++) {
            threadPool[i] = new ParallelExecutionThread(parallelInfos[i], this, parent, partEnvs[i]);
            threadPool[i].start();
        }
        for (int i=0; i<threads; i++)
            threadPool[i].join();

        if (!errorList.equals(""))
            throw new RuntimeCreatorException("error occured while parallel execution:\n" + errorList + "\ncode is:\n" + parent.originPQLToken.toString());*/

        for (int i=0; i<resultObjectIndices.length; i++) {
            calcAndMerge(partEnvs, resultObjectIndices[i], highestReductor, parallelInfos);
            env.v_object[resultObjectIndices[i]] = partEnvs[0].v_object[resultObjectIndices[i]];
        }
        return returnValue;
    }

    synchronized public void setReturnValue (boolean _returnValue) {
        if (!_returnValue)
            returnValue = false;
    }

    synchronized public void addError (String msg) {
        errorList += msg + "\n";
    }

}
