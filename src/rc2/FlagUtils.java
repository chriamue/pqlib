/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2;

import edu.umass.bc.BcFlags;
import edu.umass.pql.VarConstants;
import org.objectweb.asm.Opcodes;
import rc2.bcr.BCRCommand;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class FlagUtils implements Opcodes, BcFlags, VarConstants {

    public static int getPushStoreCommand (int instruction, int type) throws RuntimeCreatorException {
        switch (instruction) {
            case TYPE_STORE: {
                switch (type) {
                    case TYPE_INT: return ISTORE;
                    case TYPE_LONG: return LSTORE;
                    case TYPE_DOUBLE: return DSTORE;
                    case TYPE_OBJECT: return ASTORE;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_LOAD: {
                switch (type) {
                    case TYPE_INT: return ILOAD;
                    case TYPE_LONG: return LLOAD;
                    case TYPE_DOUBLE: return DLOAD;
                    case TYPE_OBJECT: return ALOAD;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            default:
                throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
        }
    }

    public static String getEnvArrayName (int type) throws RuntimeCreatorException {
        switch (type) {
            case TYPE_INT:
                return "v_int";
            case TYPE_LONG:
                return "v_long";
            case TYPE_DOUBLE:
                return "v_double";
            case TYPE_OBJECT:
                return "v_object";
            default:
                throw new RuntimeCreatorException("the specified type is not supported.");
        }
    }

    public static String getTypeSpecifier (int type) throws RuntimeCreatorException {
        switch (type) {
            case TYPE_INT:
                return "I";
            case TYPE_LONG:
                return "J";
            case TYPE_DOUBLE:
                return "D";
            case TYPE_OBJECT:
                return "Ljava/lang/Object;";
            case TYPE_FLOAT:
                return "F";
            case TYPE_CHAR:
                return "C";
            case TYPE_SHORT:
                return "S";
            case TYPE_BYTE:
                return "B";
            case TYPE_BOOLEAN:
                return "Z";
            default:
                throw new RuntimeCreatorException("the specified type is not supported.");
        }
    }

    public static int getBasicType (int type) throws RuntimeCreatorException {
        switch (type) {
            case TYPE_INT:
            case TYPE_CHAR:
            case TYPE_SHORT:
            case TYPE_BYTE:
            case TYPE_BOOLEAN:
                return TYPE_INT;
            case TYPE_LONG:
                return TYPE_LONG;
            case TYPE_DOUBLE:
            case TYPE_FLOAT:
                return TYPE_DOUBLE;
            default:
                return TYPE_OBJECT;
        }
    }

    public static int getArrayInstruction (int instruction, int type) throws RuntimeCreatorException {
        switch (instruction) {
            case TYPE_STORE: {
                switch (type) {
                    case TYPE_INT: return IASTORE;
                    case TYPE_LONG: return LASTORE;
                    case TYPE_DOUBLE: return DASTORE;
                    case TYPE_OBJECT: return AASTORE;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_LOAD: {
                switch (type) {
                    case TYPE_INT: return IALOAD;
                    case TYPE_LONG: return LALOAD;
                    case TYPE_DOUBLE: return DALOAD;
                    case TYPE_OBJECT: return AALOAD;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            default:
                throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
        }
    }

    public static String getStrArrayInstruction (int instruction, int type) throws RuntimeCreatorException {
        switch (instruction) {
            case TYPE_STORE: {
                switch (type) {
                    case TYPE_INT: return "IASTORE";
                    case TYPE_LONG: return "LASTORE";
                    case TYPE_DOUBLE: return "DASTORE";
                    case TYPE_OBJECT: return "AASTORE";
                    case TYPE_SHORT: return "SASTORE";
                    case TYPE_CHAR: return "CASTORE";
                    case TYPE_BYTE: return "BASTORE";
                    case TYPE_BOOLEAN: return "BASTORE";
                    case TYPE_FLOAT: return "FASTORE";
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_LOAD: {
                switch (type) {
                    case TYPE_INT: return "IALOAD";
                    case TYPE_LONG: return "LALOAD";
                    case TYPE_DOUBLE: return "DALOAD";
                    case TYPE_OBJECT: return "AALOAD";
                    case TYPE_SHORT: return "SALOAD";
                    case TYPE_CHAR: return "CALOAD";
                    case TYPE_BYTE: return "BALOAD";
                    case TYPE_BOOLEAN: return "BALOAD";
                    case TYPE_FLOAT: return "FALOAD";
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            default:
                throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
        }
    }

    public static int getCastInstruction (int from, int to) throws RuntimeCreatorException {
        switch (from) {
            case TYPE_INT: {
                switch (to) {
                    case TYPE_LONG: return I2L;
                    case TYPE_DOUBLE: return I2D;
                    case TYPE_CHAR: return I2C;
                    case TYPE_SHORT: return I2S;
                    case TYPE_BYTE: return I2B;
                    default: throw new RuntimeCreatorException ("this combination of casting types is not implemented. (from '" + getTypeName(from) + "' to '" + getTypeName(to) + "')");
                }
            }
            case TYPE_LONG: {
                switch (to) {
                    case TYPE_INT: return L2I;
                    case TYPE_DOUBLE: return L2D;
                    default: throw new RuntimeCreatorException ("this combination of casting types is not implemented. (from '" + getTypeName(from) + "' to '" + getTypeName(to) + "')");
                }
            }
            case TYPE_DOUBLE: {
                switch (to) {
                    case TYPE_INT: return D2I;
                    case TYPE_LONG: return D2L;
                    case TYPE_FLOAT: return D2F;
                    default: throw new RuntimeCreatorException ("this combination of casting types is not implemented. (from '" + getTypeName(from) + "' to '" + getTypeName(to) + "')");
                }
            }
            case TYPE_FLOAT: {
                switch (to) {
                    case TYPE_DOUBLE: return F2D;
                    default: throw new RuntimeCreatorException ("this combination of casting types is not implemented. (from '" + getTypeName(from) + "' to '" + getTypeName(to) + "')");
                }
            }
            default:
                throw new RuntimeCreatorException ("this combination of casting types is not implemented. (from '" + getTypeName(from) + "' to '" + getTypeName(to) + "')");
        }
    }

    public static String getTypeName(int type) {
        switch (type) {
            case TYPE_OBJECT:
                return "object";
            case TYPE_INT:
                return "int";
            case TYPE_LONG:
                return "long";
            case TYPE_DOUBLE:
                return "double";
            case TYPE_CHAR:
                return "char";
            case TYPE_SHORT:
                return "short";
            case TYPE_BYTE:
                return "byte";
            case TYPE_BOOLEAN:
                return "boolean";
            case TYPE_FLOAT:
                return "float";
            case TYPE_STRING:
                return "string";
            default:
                return "unknown";
        }
    }

    public static int getTypeFromName(String typeName) throws RuntimeCreatorException {
        if (typeName.equals("{object}"))
            return TYPE_OBJECT;
        else if (typeName.equals("{int}"))
            return TYPE_INT;
        else if (typeName.equals("{double}"))
            return TYPE_DOUBLE;
        else if (typeName.equals("{long}"))
            return TYPE_LONG;
        else if (typeName.equals("{char}"))
            return TYPE_CHAR;
        else if (typeName.equals("{byte}"))
            return TYPE_BYTE;
        else if (typeName.equals("{boolean}"))
            return TYPE_BOOLEAN;
        else if (typeName.equals("{float}"))
            return TYPE_FLOAT;
        else if (typeName.equals("{short}"))
            return TYPE_SHORT;
        else if (typeName.equals("{string}"))
            return TYPE_STRING;
        else {
            throw new RuntimeCreatorException("unknown type: " + typeName);
        }
    }

    public static int getTypeFromName(String typeName, BCRCommand command) throws RuntimeCreatorException {
        try {
            return getTypeFromName(typeName);
        } catch (RuntimeCreatorException e) {
            command.error(e.getMessage());
            return TYPE_UNKNOWN;
        }
    }

    public static String fieldTypeToName (String fieldName) {
        if (fieldName.startsWith("["))
            return fieldName;
        else if (fieldName.equals("int"))
            return "I";
        else if (fieldName.equals("long"))
            return "J";
        else if (fieldName.equals("double"))
            return "D";
        else if (fieldName.equals("char"))
            return "C";
        else if (fieldName.equals("short"))
            return "S";
        else if (fieldName.equals("byte"))
            return "B";
        else if (fieldName.equals("boolean"))
            return "Z";
        else if (fieldName.equals("float"))
            return "F";
        else
            return "L" + fieldName.replace(".", "/") + ";";
    }

    public static int typeNameToBasicType (String typeName) {
        if (typeName.startsWith("["))
            return TYPE_OBJECT;
        else if (typeName.equals("I"))
            return TYPE_INT;
        else if (typeName.equals("J"))
            return TYPE_LONG;
        else if (typeName.equals("D"))
            return TYPE_DOUBLE;
        else if (typeName.equals("C"))
            return TYPE_INT;
        else if (typeName.equals("S"))
            return TYPE_INT;
        else if (typeName.equals("B"))
            return TYPE_INT;
        else if (typeName.equals("Z"))
            return TYPE_INT;
        else if (typeName.equals("F"))
            return TYPE_FLOAT;
        else
            return TYPE_OBJECT;
    }

     public static int typeNameToExactType (String typeName) {
        if (typeName.startsWith("["))
            return TYPE_OBJECT;
        else if (typeName.equals("I"))
            return TYPE_INT;
        else if (typeName.equals("J"))
            return TYPE_LONG;
        else if (typeName.equals("D"))
            return TYPE_DOUBLE;
        else if (typeName.equals("C"))
            return TYPE_CHAR;
        else if (typeName.equals("S"))
            return TYPE_SHORT;
        else if (typeName.equals("B"))
            return TYPE_BYTE;
        else if (typeName.equals("Z"))
            return TYPE_BOOLEAN;
        else if (typeName.equals("F"))
            return TYPE_FLOAT;
        else
            return TYPE_OBJECT;
    }

    public static int typeNameToIndex (String typeName) {
        switch (typeNameToBasicType(typeName)) {
            case TYPE_OBJECT:
                return 0;
            case TYPE_INT:
                return 1;
            case TYPE_LONG:
                return 2;
            case TYPE_DOUBLE:
                return 3;
            case TYPE_FLOAT:
                return 4;
        }
        return -1;
    }

    public static int indexToType (int index) {
        switch (index) {
            case 0:
                return TYPE_OBJECT;
            case 1:
                return TYPE_INT;
            case 2:
                return TYPE_LONG;
            case 3:
                return TYPE_DOUBLE;
            case 4:
                return TYPE_FLOAT;
        }
        return TYPE_UNKNOWN;
    }

    public static int extendedTypeNameToIndex (String typeName)  {
        if (typeName.equals("int"))
            return 1;
        else if (typeName.equals("long"))
            return 2;
        else if (typeName.equals("double"))
            return 3;
        else if (typeName.equals("char"))
            return 7;
        else if (typeName.equals("short"))
            return 6;
        else if (typeName.equals("byte"))
            return 5;
        else if (typeName.equals("boolean"))
            return 8;
        else if (typeName.equals("float"))
            return 4;
        else
            return 0;
    }

    public static int indexToExtendedType (int index) {
        switch (index) {
            case 0:
                return TYPE_OBJECT;
            case 1:
                return TYPE_INT;
            case 2:
                return TYPE_LONG;
            case 3:
                return TYPE_DOUBLE;
            case 4:
                return TYPE_FLOAT;
            case 5:
                return TYPE_BYTE;
            case 6:
                return TYPE_SHORT;
            case 7:
                return TYPE_CHAR;
            case 8:
                return TYPE_BOOLEAN;
        }
        return TYPE_UNKNOWN;
    }

    public static int getArrayType (int flag) throws RuntimeCreatorException {
        switch (flag & 0xFF) {
            case TYPE_INT:
                return T_INT;
            case TYPE_LONG:
                return T_LONG;
            case TYPE_BOOLEAN:
                return T_BOOLEAN;
            case TYPE_SHORT:
                return T_SHORT;
            case TYPE_CHAR:
                return T_CHAR;
            case TYPE_BYTE:
                return T_BYTE;
            case TYPE_DOUBLE:
                return T_DOUBLE;
            case TYPE_FLOAT:
                return T_FLOAT;
            default:
                throw new RuntimeCreatorException("unknown array-reductor-type. (" + flag + ")");
        }
    }

    public static String getMinValue (int flag) {
        switch (flag & 0xFF) {
            case TYPE_INT:
                return "-2147483647-1";
            case TYPE_LONG:
                return "-9223372036854775807-1";
            case TYPE_BOOLEAN:
                return "0";
            case TYPE_BYTE:
                return "-128";
            case TYPE_SHORT:
                return "-32768";
            case TYPE_CHAR:
                return "0";
            default:
                return "UNKNOWN";
        }
    }

    public static String getMaxValue (int flag) {
        switch (flag & 0xFF) {
            case TYPE_INT:
                return "2147483647";
            case TYPE_LONG:
                return "9223372036854775807";
            case TYPE_BOOLEAN:
                return "1";
            case TYPE_BYTE:
                return "127";
            case TYPE_SHORT:
                return "32767";
            case TYPE_CHAR:
                return "65535";
            default:
                return "UNKNOWN";
        }
    }

    public static int getArithmeticInstruction (String instruction, int type) throws RuntimeCreatorException {
        switch (instruction) {
            case "ADD": {
                switch (type) {
                    case TYPE_INT: return IADD;
                    case TYPE_LONG: return LADD;
                    case TYPE_DOUBLE: return DADD;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "SUB": {
                switch (type) {
                    case TYPE_INT: return ISUB;
                    case TYPE_LONG: return LSUB;
                    case TYPE_DOUBLE: return DSUB;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "MUL": {
                switch (type) {
                    case TYPE_INT: return IMUL;
                    case TYPE_LONG: return LMUL;
                    case TYPE_DOUBLE: return DMUL;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "DIV": {
                switch (type) {
                    case TYPE_INT: return IDIV;
                    case TYPE_LONG: return LDIV;
                    case TYPE_DOUBLE: return DDIV;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "MOD": {
                switch (type) {
                    case TYPE_INT: return IREM;
                    case TYPE_LONG: return LREM;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "NEG": {
                switch (type) {
                    case TYPE_INT: return INEG;
                    case TYPE_LONG: return LNEG;
                    case TYPE_DOUBLE: return DNEG;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "REM": {
                switch (type) {
                    case TYPE_INT: return IREM;
                    case TYPE_LONG: return LREM;
                    case TYPE_DOUBLE: return DREM;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "XOR": {
                switch (type) {
                    case TYPE_INT: return IXOR;
                    case TYPE_LONG: return LXOR;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "OR": {
                switch (type) {
                    case TYPE_INT: return IOR;
                    case TYPE_LONG: return LOR;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "AND": {
                switch (type) {
                    case TYPE_INT: return IAND;
                    case TYPE_LONG: return LAND;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "SHL": {
                switch (type) {
                    case TYPE_INT: return ISHL;
                    case TYPE_LONG: return LSHL;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "SHR": {
                switch (type) {
                    case TYPE_INT: return ISHR;
                    case TYPE_LONG: return LSHR;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case "USHR": {
                switch (type) {
                    case TYPE_INT: return IUSHR;
                    case TYPE_LONG: return LUSHR;
                    default: throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
                }
            }
            default:
                throw new RuntimeCreatorException ("this combination of instruction and type is not implemented (yet).");
        }
    }

}
