/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import rc2.optimizer.BytecodeCommand.MainCommand;

/**
 *
 * @author Hilmar
 */
public class OptimizeVisitor extends MethodVisitor implements Opcodes {

    private MethodVisitor childVisitor;
    protected List <BytecodeCommand> commands;
    protected Map <Integer, VariableInfo > variables;
    protected ExtendedHashSet <BytecodeLabelInfo> labels;
    protected List <ExecutionBlock> executionBlocks;
    protected Map <Label, Label> labelReplace;
    protected Set <VariableRange> varRanges;

    public OptimizeVisitor(int i) {
        super(i);
        System.err.println("unsupported constructor");
    }

    public OptimizeVisitor(int i, MethodVisitor mv) {
        super(i, mv);
        System.err.println("unsupported constructor");
    }

    public OptimizeVisitor(MethodVisitor mv) {
        super(Opcodes.ASM4);
        childVisitor = mv;
        commands = new ArrayList <BytecodeCommand> ();
        variables = new HashMap <Integer, VariableInfo > ();
        labels = new ExtendedHashSet <BytecodeLabelInfo> ();
        executionBlocks = new ArrayList <ExecutionBlock> ();
        labelReplace = new HashMap <Label, Label> ();
        varRanges = new HashSet <VariableRange> ();
    }

    @Override
    public AnnotationVisitor visitAnnotationDefault() {
        System.err.println("unsupported visitor-method: 'visitAnnotationDefault'");
        return null;
    }

    @Override
    public AnnotationVisitor visitAnnotation(String string, boolean bln) {
        System.err.println("unsupported visitor-method: 'visitAnnotation'");
        return null;
    }

    @Override
    public AnnotationVisitor visitParameterAnnotation(int i, String string, boolean bln) {
        System.err.println("unsupported visitor-method: 'visitParameterAnnotation'");
        return null;
    }

    @Override
    public void visitAttribute(Attribute atrbt) {
        System.err.println("unsupported visitor-method: 'visitAttribute'");
    }

    @Override
    public void visitCode() {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.Code, -1, new int [0], new String [0], null));
    }

    @Override
    public void visitFrame(int i, int i1, Object[] os, int i2, Object[] os1) {
        System.err.println("unsupported visitor-method: 'visitFrame'");
    }

    @Override
    public void visitInsn(int i) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.Insn, i, new int [0], new String [0], null));
    }

    @Override
    public void visitIntInsn(int i, int i1) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.IntInsn, i, new int [] {i1}, new String [0], null));
    }

    @Override
    public void visitVarInsn(int i, int i1) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.VarInsn, i, new int [] {i1}, new String [0], null));
    }

    @Override
    public void visitTypeInsn(int i, String string) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.TypeInsn, i, new int [0], new String [] {string}, null));
    }

    @Override
    public void visitFieldInsn(int i, String string, String string1, String string2) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.FieldInsn, i, new int [0], new String [] {string, string1, string2}, null));
    }

    @Override
    public void visitMethodInsn(int i, String string, String string1, String string2) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.MethodInsn, i, new int [0], new String [] {string, string1, string2}, null));
    }

    @Override
    public void visitInvokeDynamicInsn(String string, String string1, Handle handle, Object[] os) {
        System.err.println("unsupported visitor-method: 'visitInvokeDynamicInsn'");
    }

    @Override
    public void visitJumpInsn(int i, Label label) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.JumpInsn, i, new int [0], new String [0], label));
    }

    @Override
    public void visitLabel(Label label) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.Label, -1, new int [0], new String [0], label));
    }

    @Override
    public void visitLdcInsn(Object o) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.LdcInsn, -1, new int [0], new String [0], o));
    }

    @Override
    public void visitIincInsn(int i, int i1) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.IincInsn, -1, new int [] {i, i1}, new String [0], null));
    }

    @Override
    public void visitTableSwitchInsn(int i, int i1, Label label, Label[] labels) {
        System.err.println("unsupported visitor-method: 'visitTableSwitchInsn'");
    }

    @Override
    public void visitLookupSwitchInsn(Label label, int[] ints, Label[] labels) {
        System.err.println("unsupported visitor-method: 'visitLookupSwitchInsn'");
    }

    @Override
    public void visitMultiANewArrayInsn(String string, int i) {
        System.err.println("unsupported visitor-method: 'visitMultiANewArrayInsn'");
    }

    @Override
    public void visitTryCatchBlock(Label label, Label label1, Label label2, String string) {
        System.err.println("unsupported visitor-method: 'visitTryCatchBlock'");
    }

    @Override
    public void visitLocalVariable(String string, String string1, String string2, Label label, Label label1, int i) {
        System.err.println("unsupported visitor-method: 'visitLocalVariable'");
    }

    @Override
    public void visitLineNumber(int i, Label label) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.LineNumber, -1, new int [] {i}, new String [0], label));
    }

    @Override
    public void visitMaxs(int i, int i1) {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.Maxs, -1, new int [] {i, i1}, new String [0], null));
    }

    @Override
    public void visitEnd() {
        commands.add(new BytecodeCommand(this, commands.size(), MainCommand.End, -1, new int [0], new String [0], null));
    }


    public void apply () {

        Object [] varInfos = variables.values().toArray();
        for (int i=0; i<varInfos.length; i++) {
            VariableInfo varInfo = (VariableInfo)varInfos[i];
            if (varInfo.loadStore.size() == 1) {
                boolean indexConflict = false;
                for (int j=0; j<varInfos.length; j++) {
                    VariableInfo varInfoOther = (VariableInfo)varInfos[j];
                    if (i != j && varInfoOther.loadStore.size() == 1) {
                        int selfIndex [] = {commands.get(varInfo.loadStore.get(0)).intParameters[0], commands.get(varInfo.loadStore.get(0)+1).intParameters[0]};
                        int otherIndex [] = {commands.get(varInfoOther.loadStore.get(0)).intParameters[0], commands.get(varInfoOther.loadStore.get(0)+1).intParameters[0]};
                        if (otherIndex[0] == selfIndex[0] || otherIndex[0] == selfIndex[1] || otherIndex[1] == selfIndex[0] || otherIndex[1] == selfIndex[1]) {
                            indexConflict = true;
                            break;
                        }
                    }
                }
                if (!indexConflict) {
                    commands.get(varInfo.loadStore.get(0)).ignore = true;
                    commands.get(varInfo.loadStore.get(0)+1).ignore = true;
                    for (BytecodeCommand cmd : varInfo.used)
                        cmd.intParameters[0] = commands.get(varInfo.loadStore.get(0) +1).intParameters[0];
                }
            }
        }

        analyzeStructure();

        for (BytecodeCommand command : commands) {
            if (command.mainCommand == MainCommand.VarInsn && BytecodeUtils.isStoreCommand(command.subCommand) && command.varRange == null)
                command.ignore = true;
            else if (command.varRange != null)
                command.intParameters[0] = command.varRange.getNewVarIndex();
            command.apply(childVisitor);
        }
    }

    private void analyzeStructure () {

        //fill the labels-set
        for (int i=0; i<commands.size(); i++) {
            BytecodeCommand cmd = commands.get(i);
            if (cmd.ignore)
                continue;
            if (cmd.mainCommand == MainCommand.Label) {
                BytecodeLabelInfo newLabel = new BytecodeLabelInfo((Label)cmd.obj, cmd);
                labels.add(newLabel);
            } else if (cmd.mainCommand == MainCommand.JumpInsn) {
                while (labelReplace.containsKey((Label)cmd.obj))
                    cmd.obj = labelReplace.get((Label)cmd.obj);
                BytecodeLabelInfo newLabel = new BytecodeLabelInfo((Label)cmd.obj);
                labels.add(newLabel);
                labels.get(newLabel).gotoCmd.add(cmd);
            } else if (cmd.mainCommand == MainCommand.LineNumber) {
                BytecodeCommand previous = commands.get(i).getPreviousCommand(1);
                if (previous != null && previous.mainCommand == MainCommand.Label)
                    labels.get(new BytecodeLabelInfo((Label)previous.obj)).neededForLineNumber = true;
            }
        }

        //fill the executionBlocks-list
        executionBlocks.add(new ExecutionBlock(this, 0));
        for (int i=0; i<commands.size(); i++) {
            BytecodeCommand cmd = commands.get(i);
            if (cmd.ignore)
                continue;
            if (cmd.mainCommand == MainCommand.Label) {
                if (!labels.get(new BytecodeLabelInfo((Label)cmd.obj)).isUsed()) {
                    labels.remove(new BytecodeLabelInfo((Label)cmd.obj));
                    commands.get(i).ignore = true;
                } else {
                    ExecutionBlock newExecutionBlock = new ExecutionBlock(this, i);
                    executionBlocks.get(executionBlocks.size()-1).finish(i-1, new ExecutionBlock [] {newExecutionBlock});
                    executionBlocks.add(newExecutionBlock);
                }
            } else if (cmd.mainCommand == MainCommand.JumpInsn) {
                ExecutionBlock newExecutionBlock = new ExecutionBlock(this, i+1);
                if (cmd.subCommand == GOTO)
                    executionBlocks.get(executionBlocks.size()-1).finish(i, new ExecutionBlock [] {null});
                else
                    executionBlocks.get(executionBlocks.size()-1).finish(i, new ExecutionBlock [] {null, newExecutionBlock});
                executionBlocks.add(newExecutionBlock);
            }
        }
        executionBlocks.get(executionBlocks.size()-1).finish(commands.size()-1, new ExecutionBlock [0]);
        //replace the null-entries (if a forward-jump was possible)
        for (ExecutionBlock executionBlock : executionBlocks)
            executionBlock.searchMissingJumps();

        //create a variable-used-diagram
        for (int i=0; i<commands.size(); i++) {
            BytecodeCommand cmd = commands.get(i);
            if (cmd.ignore)
                continue;
            if ( ((cmd.mainCommand == MainCommand.VarInsn && !BytecodeUtils.isStoreCommand(cmd.subCommand)) || (cmd.mainCommand == MainCommand.IincInsn))
                && cmd.intParameters[0] != 0 && cmd.varRange == null) {
                varRanges.add(new VariableRange(this, cmd));
            }
        }

        //merge all variable-ranges, if possible
        Object [] varRangesArray = varRanges.toArray();
        for (int i=0; i<varRangesArray.length; i++) {
            if (varRangesArray[i] != null) {
                for (int j=i+1; j<varRangesArray.length; j++) {
                    if (varRangesArray[j] != null && ((VariableRange)varRangesArray[i]).mergeIfPossible((VariableRange)varRangesArray[j]))
                        varRangesArray[j] = null;
                }
            }
        }
        varRanges = new HashSet <VariableRange> ();
        for (Object varRange : varRangesArray)
            if (varRange != null)
                varRanges.add((VariableRange)varRange);

        //optimize variableRanges
        for (VariableRange varRange : varRanges)
            varRange.optimize();
    }

    public ExecutionBlock findExecutionBlock (int index) {
        for (ExecutionBlock executionBlock : executionBlocks)
            if (index >= executionBlock.from && index <= executionBlock.to)
                return executionBlock;
        return null;
    }

}
