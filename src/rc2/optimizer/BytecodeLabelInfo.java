/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import java.util.HashSet;
import java.util.Set;
import org.objectweb.asm.Label;

/**
 *
 * @author Hilmar
 */
public class BytecodeLabelInfo {

    public Label label;
    public boolean neededForLineNumber;
    public BytecodeCommand labelCmd;
    public Set <BytecodeCommand> gotoCmd;

    public BytecodeLabelInfo (Label _label) {
        label = _label;

        neededForLineNumber = false;
        labelCmd = null;
        gotoCmd = new HashSet <BytecodeCommand> ();
    }

    public BytecodeLabelInfo (Label _label, BytecodeCommand _labelCmd) {
        label = _label;
        labelCmd = _labelCmd;

        neededForLineNumber = false;
        gotoCmd = new HashSet <BytecodeCommand> ();
    }

    public boolean isUsed () {
        return (neededForLineNumber || gotoCmd.size() > 0);
    }

    @Override
    public boolean equals (Object other) {
        return (other instanceof BytecodeLabelInfo && ((BytecodeLabelInfo)other).label.equals(label));
    }

    @Override
    public int hashCode () {
        return 0;
    }

}
