/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import java.util.HashSet;
import java.util.Set;
import org.objectweb.asm.Label;

/**
 *
 * @author Hilmar
 */
public class ExecutionBlock {

    public OptimizeVisitor parent;
    public int from;
    public int to;
    public ExecutionBlock [] jumpTo;
    public Set <ExecutionBlock> jumpFrom;

    public ExecutionBlock (OptimizeVisitor _parent, int start) {
        parent = _parent;
        from = start;
        to = -1;
        jumpTo = null;
        jumpFrom = new HashSet <ExecutionBlock> ();
    }

    public void finish (int end, ExecutionBlock [] _jumpTo) {
        to = end;
        jumpTo = _jumpTo;
        if (to < from)
            parent.executionBlocks.remove(parent.executionBlocks.size()-1);
    }

    public void searchMissingJumps () {
        if (jumpTo.length > 0 && jumpTo[0] == null) {
            for (ExecutionBlock executionBlock : parent.executionBlocks) {
                BytecodeCommand cmd = parent.commands.get(executionBlock.from);
                if (cmd.mainCommand == BytecodeCommand.MainCommand.Label && ((Label)cmd.obj).equals((Label)parent.commands.get(this.to).obj) ) {
                    jumpTo[0] = executionBlock;
                    break;
                }
            }
        }
        for (int i=0; i<jumpTo.length; i++)
            jumpTo[i].jumpFrom.add(this);
    }

    public int getInternalId () {
        for (int i=0; i<parent.executionBlocks.size(); i++)
            if (parent.executionBlocks.get(i) == this)
                return i;
        return -1;
    }

    @Override
    public String toString () {
        String jumpToStr = "", jumpFromStr = "";
        for (int i=0; i<jumpTo.length; i++)
            jumpToStr += (i > 0 ? ", " : "") + jumpTo[i].getInternalId();
        Object [] jumpFromArr = jumpFrom.toArray();
        for (int i=0; i<jumpFromArr.length; i++)
            jumpFromStr += (i > 0 ? ", " : "") + ((ExecutionBlock)jumpFromArr[i]).getInternalId();
        return getInternalId() + (jumpToStr.equals("") ? "" : " => " + jumpToStr) + (jumpFromStr.equals("") ? "" : " <= " + jumpFromStr);
    }

    @Override
    public boolean equals (Object other) {
        return (other instanceof ExecutionBlock && ((ExecutionBlock)other).getInternalId() == getInternalId());
    }



}
