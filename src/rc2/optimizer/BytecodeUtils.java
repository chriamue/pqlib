/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import org.objectweb.asm.Opcodes;

/**
 *
 * @author Hilmar
 */
public class BytecodeUtils implements Opcodes {

    public static boolean isStoreCommand (int command) {
        switch (command) {
            case ASTORE:
            case FSTORE:
            case ISTORE:
            case DSTORE:
            case LSTORE:
                return true;
            default:
                return false;
        }
    }

    public static int reverseStoreCommand (int command) {
        switch (command) {
            case ASTORE:
                return ALOAD;
            case FSTORE:
                return FLOAD;
            case ISTORE:
                return ILOAD;
            case DSTORE:
                return DLOAD;
            case LSTORE:
                return LLOAD;
            default:
                return -1;
        }
    }

    public static int reverseLoadCommand (int command) {
        switch (command) {
            case ALOAD:
                return ASTORE;
            case FLOAD:
                return FSTORE;
            case ILOAD:
                return ISTORE;
            case DLOAD:
                return DSTORE;
            case LLOAD:
                return LSTORE;
            default:
                return -1;
        }
    }

    public static int reverseIfCommand (int command) {
        switch (command) {
            case IFEQ:
                return IFNE;
            case IFNE:
                return IFEQ;
            case IFLT:
                return IFGE;
            case IFGE:
                return IFLT;
            case IFGT:
                return IFLE;
            case IFLE:
                return IFGT;
            case IF_ICMPEQ:
                return IF_ICMPNE;
            case IF_ICMPNE:
                return IF_ICMPEQ;
            case IF_ICMPLT:
                return IF_ICMPGE;
            case IF_ICMPGE:
                return IF_ICMPLT;
            case IF_ICMPGT:
                return IF_ICMPLE;
            case IF_ICMPLE:
                return IF_ICMPGT;
            case IF_ACMPEQ:
                return IF_ACMPNE;
            case IF_ACMPNE:
                return IF_ACMPEQ;
            case IFNULL:
                return IFNONNULL;
            case IFNONNULL:
                return IFNULL;
            default:
                System.err.println("cant't reverse this jump-command: " + command);
                return -1;
        }
    }

}
