/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import java.util.Set;

/**
 *
 * @author Hilmar
 */
public class RangeBlock {

    private ExecutionBlock inBlock;
    private boolean [] blockMask;

    public RangeBlock (ExecutionBlock _inBlock, int from, int to) {
        inBlock = _inBlock;
        blockMask = new boolean [inBlock.to - inBlock.from + 1];
        for (int i=0; i<blockMask.length; i++)
            blockMask[i] = (i+inBlock.from >= from && i+inBlock.from <= to);
    }

    public void mergeWithBlocks (Set <RangeBlock> blocks) {
        for (RangeBlock block : blocks) {
            if (block.inBlock.equals(inBlock)) {
                for (int i=0; i<block.blockMask.length; i++)
                    block.blockMask[i] = (blockMask[i] || block.blockMask[i]);
                return;
            }
        }
        blocks.add(this);
    }

    public boolean isMarked (int index) {
        return (index >= inBlock.from && index <= inBlock.to && blockMask[index-inBlock.from]);
    }

    public boolean isOverlap (RangeBlock other) {
        if (inBlock.equals(other.inBlock)) {
            for (int i=0; i<blockMask.length; i++)
                if (blockMask[i] && other.blockMask[i])
                    return true;
        }
        return false;
    }

    public void mergeWith (RangeBlock other) {
        for (int i=0; i<blockMask.length; i++)
            blockMask[i] = (blockMask[i] || other.blockMask[i]);
    }

    @Override
    public boolean equals (Object other) {
        return (other instanceof RangeBlock && ((RangeBlock)other).inBlock.equals(inBlock));
    }

    @Override
    public int hashCode () {
        return 0;
    }


}
