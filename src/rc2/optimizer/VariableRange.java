/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import java.util.HashSet;
import java.util.Set;
import org.objectweb.asm.Opcodes;
import rc2.optimizer.BytecodeCommand.MainCommand;

/**
 *
 * @author Hilmar
 */
public class VariableRange implements Opcodes {

    private OptimizeVisitor parent;
    private int varIndex;
    private boolean doubleRange;
    private Set <RangeBlock> blocks;
    private Set <BytecodeCommand> refs;

    private int newVarIndex;

    public VariableRange (OptimizeVisitor _parent, BytecodeCommand loadCmd) {
        parent = _parent;
        varIndex = loadCmd.intParameters[0];
        doubleRange = (loadCmd.mainCommand != MainCommand.IincInsn && (loadCmd.subCommand == LLOAD || loadCmd.subCommand == DLOAD));
        blocks = new HashSet <RangeBlock> ();
        newVarIndex = -1;
        refs = new HashSet <BytecodeCommand> ();
        loadCmd.varRange = this;
        refs.add(loadCmd);
        recursiveBackwardPath(parent.findExecutionBlock(loadCmd.index), loadCmd.index);
    }

    public boolean mergeIfPossible (VariableRange other) {
        if (other.varIndex != varIndex)
            return false;

        boolean foundOverlap = false;
        Set <RangeBlock> additionalBlocks = new HashSet <RangeBlock> ();
        for (RangeBlock block2 : other.blocks) {
            boolean blockNoOverlap = true;
            for (RangeBlock block1 : blocks) {
                if (block1.isOverlap(block2)) {
                    block1.mergeWith(block2);
                    blockNoOverlap = false;
                    foundOverlap = true;
                }
            }
            if (blockNoOverlap)
                additionalBlocks.add(block2);
        }
        if (foundOverlap) {
            blocks.addAll(additionalBlocks);
            for (BytecodeCommand refresh : other.refs) {
                refresh.varRange = this;
                refs.add(refresh);
            }
        }
        return foundOverlap;
    }

    public int getNewVarIndex () {
        if (newVarIndex == -1) {
            int highestVarIndex = 1;
            Set <Integer> locked = new HashSet <Integer> ();
            Set <Integer> free = new HashSet <Integer> ();
            for (VariableRange other : parent.varRanges) {
                if (other.newVarIndex != -1) {
                    int nextVarIndex = other.newVarIndex + (other.doubleRange ? 2 : 1);
                    if (nextVarIndex > highestVarIndex)
                        highestVarIndex = nextVarIndex;
                    if (!isOverlapping(other)) {
                        free.add(other.newVarIndex);
                        if (other.doubleRange)
                            free.add(other.newVarIndex+1);
                    } else {
                        locked.add(other.newVarIndex);
                        if (other.doubleRange)
                            locked.add(other.newVarIndex+1);
                    }
                }
            }
            for (Integer lockedKey : locked)
                if (free.contains(lockedKey))
                    free.remove(lockedKey);
            for (Integer freeKey : free)
                if (!doubleRange || free.contains(freeKey+1) || highestVarIndex == freeKey+1) {
                    newVarIndex = freeKey;
                    return newVarIndex;
            }
            newVarIndex = highestVarIndex;
        }
        return newVarIndex;
    }

    public boolean isInRange (int index) {
        for (RangeBlock rangeBlock : blocks)
            if (rangeBlock.isMarked(index))
                return true;
        return false;
    }

    public void optimize () {
        String castTo = "";
        Set <BytecodeCommand> storeCmds = new HashSet <BytecodeCommand> ();
        Set <BytecodeCommand> loadCmds = new HashSet <BytecodeCommand> ();
        for (BytecodeCommand ref : refs) {
            if (ref.mainCommand == MainCommand.VarInsn) {
                if (BytecodeUtils.isStoreCommand(ref.subCommand)) {
                    BytecodeCommand pre = ref.getPreviousCommand(1);
                    if (pre != null && pre.mainCommand == MainCommand.TypeInsn && pre.subCommand == CHECKCAST) {
                        if (castTo.equals("") || pre.strParameters[0].equals(castTo))
                            castTo = pre.strParameters[0];
                        else
                            return;
                    } else
                        storeCmds.add(ref);
                } else {
                    BytecodeCommand next = ref.getFollowingCommand(1);
                    if (next != null && next.mainCommand == MainCommand.TypeInsn && next.subCommand == CHECKCAST) {
                        if (castTo.equals("") || next.strParameters[0].equals(castTo)) {
                            castTo = next.strParameters[0];
                            loadCmds.add(next);
                        } else
                            return;
                    }
                }
            }
        }

        if (storeCmds.size() <= loadCmds.size() && !castTo.equals("")) {
            for (BytecodeCommand loadCmd : loadCmds)
                loadCmd.ignore = true;
            BytecodeCommand insert = new BytecodeCommand(parent, -1, MainCommand.TypeInsn, CHECKCAST, new int [0], new String [] {castTo}, null);
            for (BytecodeCommand storeCmd : storeCmds)
                storeCmd.insertPrevious = insert;
        }
    }

    private void recursiveBackwardPath (ExecutionBlock executionBlock, int index) {
        if (index == -1)
            index = executionBlock.to;

        for (RangeBlock block : blocks)
            if (block.isMarked(index))
                return;

        for (int i=index; i>=executionBlock.from; i--) {
            BytecodeCommand cmd = parent.commands.get(i);
            if ((cmd.mainCommand == MainCommand.VarInsn || cmd.mainCommand == MainCommand.IincInsn) && cmd.intParameters[0] == varIndex && !cmd.ignore) {
                cmd.varRange = this;
                refs.add(cmd);
                if (cmd.mainCommand == MainCommand.VarInsn && BytecodeUtils.isStoreCommand(cmd.subCommand)) {
                    (new RangeBlock(executionBlock, i, index)).mergeWithBlocks(blocks);
                    return;
                }
            }
        }

        (new RangeBlock(executionBlock, executionBlock.from, index)).mergeWithBlocks(blocks);
        for (ExecutionBlock backwardBlocks : executionBlock.jumpFrom)
            recursiveBackwardPath(backwardBlocks, -1);
    }

    private boolean isOverlapping (VariableRange other) {
        for (RangeBlock block1 : blocks)
            for (RangeBlock block2 : other.blocks)
                if (block1.isOverlap(block2))
                    return true;
        return false;
    }

}
