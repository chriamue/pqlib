/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.optimizer;

import java.lang.reflect.Field;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Hilmar
 */
public class BytecodeCommand implements Opcodes {

    enum MainCommand {Code, Insn, IntInsn, VarInsn, TypeInsn, FieldInsn, MethodInsn, JumpInsn, Label, LdcInsn, IincInsn, LineNumber, Maxs, End};

    public MainCommand mainCommand;
    public int subCommand;
    public int [] intParameters;
    public String [] strParameters;
    public Object obj;
    public OptimizeVisitor parent;
    public int index;
    public boolean ignore;
    public VariableRange varRange;
    public BytecodeCommand insertPrevious;

    public BytecodeCommand (OptimizeVisitor _parent, int _index, MainCommand _mainCommand, int _subCommand, int [] _intParameters, String [] _strParameters, Object _obj) {
        parent = _parent;
        index = _index;
        mainCommand = _mainCommand;
        subCommand = _subCommand;
        intParameters = _intParameters;
        strParameters = _strParameters;
        obj = _obj;
        ignore = false;
        varRange = null;
        insertPrevious = null;

        if (index != -1)
            firstCheck();
    }

    public void apply (MethodVisitor mv) {
        if (insertPrevious != null) {
            if (ignore)
                insertPrevious.ignore = true;
            insertPrevious.apply(mv);
        }
        //System.out.println(toString());
        if (ignore)
            return;
        switch (mainCommand) {
            case Code:
                mv.visitCode();
                break;
            case Insn:
                mv.visitInsn(subCommand);
                break;
            case IntInsn:
                mv.visitIntInsn(subCommand, intParameters[0]);
                break;
            case VarInsn:
                mv.visitVarInsn(subCommand, intParameters[0]);
                break;
            case TypeInsn:
                mv.visitTypeInsn(subCommand, strParameters[0]);
                break;
            case FieldInsn:
                mv.visitFieldInsn(subCommand, strParameters[0], strParameters[1], strParameters[2]);
                break;
            case MethodInsn:
                mv.visitMethodInsn(subCommand, strParameters[0], strParameters[1], strParameters[2]);
                break;
            case JumpInsn:
                mv.visitJumpInsn(subCommand, (Label)obj);
                break;
            case Label:
                mv.visitLabel((Label)obj);
                break;
            case LdcInsn:
                mv.visitLdcInsn(obj);
                break;
            case IincInsn:
                mv.visitIincInsn(intParameters[0], intParameters[1]);
                break;
            case LineNumber:
                mv.visitLineNumber(intParameters[0], (Label)obj);
                break;
            case Maxs:
                mv.visitMaxs(intParameters[0], intParameters[1]);
                break;
            case End:
                mv.visitEnd();
                break;
        }
    }

    @Override
    public String toString () {
        String inRange = "";
        Object [] varRangeArr = parent.varRanges.toArray();
        for (int i=0; i<varRangeArr.length; i++)
            if (((VariableRange)varRangeArr[i]).isInRange(index))
                inRange += (!inRange.equals("") ? ", " : "") + ((VariableRange)varRangeArr[i]).getNewVarIndex();
        if (!inRange.equals(""))
            inRange = " [" + inRange + "]";
        return (ignore ? "[IGNORE]   " : "") + index + ": " + mainCommand.name() + " " + getSubCommandName(subCommand) + arrayToStr(intParameters) + arrayToStr(strParameters) + (obj == null ? "" : " " + obj.toString()) + (parent.findExecutionBlock(index) == null ? "" : " (" + parent.findExecutionBlock(index).toString() + ")" + (varRange != null ? "  -> " + varRange.getNewVarIndex() : "") + inRange);
    }

    private void firstCheck () {
        if (mainCommand == MainCommand.VarInsn || mainCommand == MainCommand.IincInsn) {
            int varIndex = intParameters[0];
            if (!parent.variables.containsKey(varIndex))
                parent.variables.put(varIndex, new VariableInfo());
            parent.variables.get(varIndex).used.add(this);

            if (mainCommand == MainCommand.VarInsn) {
                BytecodeCommand previousCommand = getPreviousCommand(1);
                if (BytecodeUtils.isStoreCommand(subCommand)) {
                    if (previousCommand != null && previousCommand.mainCommand == MainCommand.VarInsn && previousCommand.subCommand == BytecodeUtils.reverseStoreCommand(subCommand))
                        parent.variables.get(parent.commands.get(index-1).intParameters[0]).loadStore.add(index-1);
                } else {
                    if (previousCommand != null && previousCommand.mainCommand == MainCommand.VarInsn && previousCommand.subCommand == BytecodeUtils.reverseLoadCommand(subCommand) && previousCommand.intParameters[0] == intParameters[0]) {
                        this.insertPrevious = new BytecodeCommand(parent, -1, MainCommand.Insn, ((subCommand == LLOAD || subCommand == DLOAD) ? DUP2 : DUP), new int [0], new String [0], null);
                        subCommand = BytecodeUtils.reverseLoadCommand(subCommand);
                        previousCommand.ignore = true;
                    }
                }
            }
        } else if (mainCommand == MainCommand.Label) {
            BytecodeCommand previousCommand = getPreviousCommand(1);
            if (previousCommand != null) {
                if (previousCommand.mainCommand == MainCommand.JumpInsn && previousCommand.subCommand == GOTO && ((Label)previousCommand.obj).equals((Label)obj) )
                    parent.commands.get(parent.commands.size()-1).ignore = true;
                else if (previousCommand.mainCommand == MainCommand.Label) {
                    parent.labelReplace.put((Label)previousCommand.obj, (Label)obj);
                    parent.commands.get(parent.commands.size()-1).ignore = true;
                }
            }
        } else if (mainCommand == MainCommand.JumpInsn && subCommand == GOTO) {
            BytecodeCommand previousCommand = getPreviousCommand(1);
            if (previousCommand != null && previousCommand.mainCommand == MainCommand.JumpInsn) {
                ignore = true;
                previousCommand.obj = obj;
                if (previousCommand.subCommand != GOTO)
                    previousCommand.subCommand = BytecodeUtils.reverseIfCommand(previousCommand.subCommand);
            }
        }
    }

    public BytecodeCommand getPreviousCommand (int preCount) {
        while (index-preCount >= 0) {
            if (!parent.commands.get(index-preCount).ignore)
                return parent.commands.get(index-preCount);
            preCount++;
        }
        return null;
    }

    public BytecodeCommand getFollowingCommand (int postCount) {
        while (index+postCount < parent.commands.size()) {
            if (!parent.commands.get(index+postCount).ignore)
                return parent.commands.get(index+postCount);
            postCount++;
        }
        return null;
    }

    private static String getSubCommandName (int flag) {
        if (flag == -1)
            return "";
        try {
            boolean ignore = true;
            for (Field field : Opcodes.class.getFields()) {
                if (ignore && field.getName().equals("NOP"))
                    ignore = false;
                if (!ignore && field.get(null).equals(new Integer(flag)))
                    return field.getName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "[UNKNOWN] (" + flag + ")";
    }

    private static String arrayToStr (String [] array) {
        if (array.length == 0)
            return "";
        String returnStr = " [";
        for (int i=0; i<array.length; i++)
            returnStr += (i > 0 ? ", " : "") + array[i];
        return returnStr + "]";
    }

    private static String arrayToStr (int [] array) {
        if (array.length == 0)
            return "";
        String returnStr = " [";
        for (int i=0; i<array.length; i++)
            returnStr += (i > 0 ? ", " : "") + array[i];
        return returnStr + "]";
    }


}
