// Generated from BCR.g4 by ANTLR 4.2.2

package rc2.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BCRParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__41=1, T__40=2, T__39=3, T__38=4, T__37=5, T__36=6, T__35=7, T__34=8, 
		T__33=9, T__32=10, T__31=11, T__30=12, T__29=13, T__28=14, T__27=15, T__26=16, 
		T__25=17, T__24=18, T__23=19, T__22=20, T__21=21, T__20=22, T__19=23, 
		T__18=24, T__17=25, T__16=26, T__15=27, T__14=28, T__13=29, T__12=30, 
		T__11=31, T__10=32, T__9=33, T__8=34, T__7=35, T__6=36, T__5=37, T__4=38, 
		T__3=39, T__2=40, T__1=41, T__0=42, CHARARRAY=43, NULL=44, TYPE=45, ACCESSMODE=46, 
		PARALLELMODE=47, NAME=48, DOUBLE=49, INT=50, IFOPERATOR=51, WS=52, NEWLINE=53, 
		COMMENTLINE=54, COMMENTBLOCK=55;
	public static final String[] tokenNames = {
		"<INVALID>", "'/'", "'continue'", "'isParallelMode'", "'isMode'", "'new'", 
		"'||'", "'while'", "';'", "'{'", "'instanceof'", "'&&'", "'>>'", "'<<'", 
		"'on'", "'='", "'}'", "'if'", "'@type'", "'^'", "'break'", "'abort'", 
		"'&'", "'do'", "'('", "'*'", "'proceed'", "','", "'.'", "'@generic'", 
		"':'", "'>>>'", "'|'", "'~'", "'?='", "'!'", "'isConst'", "'%'", "'else'", 
		"')'", "'+'", "'@accessModes'", "'-'", "CHARARRAY", "'null'", "TYPE", 
		"ACCESSMODE", "PARALLELMODE", "NAME", "DOUBLE", "INT", "IFOPERATOR", "WS", 
		"'\n'", "COMMENTLINE", "COMMENTBLOCK"
	};
	public static final int
		RULE_complete = 0, RULE_initializing = 1, RULE_generic = 2, RULE_accessModes = 3, 
		RULE_functionHead = 4, RULE_command = 5, RULE_simpleCommand = 6, RULE_labelCommand = 7, 
		RULE_returnCommand = 8, RULE_whileCommand = 9, RULE_doWhileCommand = 10, 
		RULE_breakCommand = 11, RULE_continueCommand = 12, RULE_assignmentCommand = 13, 
		RULE_methodCallCommand = 14, RULE_expr = 15, RULE_orExpr = 16, RULE_xorExpr = 17, 
		RULE_andExpr = 18, RULE_shiftExpr = 19, RULE_addExpr = 20, RULE_mulExpr = 21, 
		RULE_unaryExpr = 22, RULE_constantExpr = 23, RULE_literalExpr = 24, RULE_newExpr = 25, 
		RULE_methodCall = 26, RULE_typeInformation = 27, RULE_ifinstruction = 28, 
		RULE_ifcontent = 29, RULE_ifconnection = 30, RULE_ifsubcondition = 31, 
		RULE_ifliteral = 32, RULE_ifElseBody = 33;
	public static final String[] ruleNames = {
		"complete", "initializing", "generic", "accessModes", "functionHead", 
		"command", "simpleCommand", "labelCommand", "returnCommand", "whileCommand", 
		"doWhileCommand", "breakCommand", "continueCommand", "assignmentCommand", 
		"methodCallCommand", "expr", "orExpr", "xorExpr", "andExpr", "shiftExpr", 
		"addExpr", "mulExpr", "unaryExpr", "constantExpr", "literalExpr", "newExpr", 
		"methodCall", "typeInformation", "ifinstruction", "ifcontent", "ifconnection", 
		"ifsubcondition", "ifliteral", "ifElseBody"
	};

	@Override
	public String getGrammarFileName() { return "BCR.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BCRParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class CompleteContext extends ParserRuleContext {
		public GenericContext generic(int i) {
			return getRuleContext(GenericContext.class,i);
		}
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public List<GenericContext> generic() {
			return getRuleContexts(GenericContext.class);
		}
		public InitializingContext initializing() {
			return getRuleContext(InitializingContext.class,0);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public CompleteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_complete; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterComplete(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitComplete(this);
		}
	}

	public final CompleteContext complete() throws RecognitionException {
		CompleteContext _localctx = new CompleteContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_complete);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==29) {
				{
				{
				setState(68); generic();
				}
				}
				setState(73);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(84);
			_la = _input.LA(1);
			if (_la==41) {
				{
				setState(74); initializing();
				setState(75); match(9);
				setState(79);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 7) | (1L << 17) | (1L << 18) | (1L << 20) | (1L << 21) | (1L << 23) | (1L << 26) | (1L << TYPE) | (1L << NAME))) != 0)) {
					{
					{
					setState(76); command();
					}
					}
					setState(81);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(82); match(16);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitializingContext extends ParserRuleContext {
		public AccessModesContext accessModes() {
			return getRuleContext(AccessModesContext.class,0);
		}
		public FunctionHeadContext functionHead() {
			return getRuleContext(FunctionHeadContext.class,0);
		}
		public InitializingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initializing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterInitializing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitInitializing(this);
		}
	}

	public final InitializingContext initializing() throws RecognitionException {
		InitializingContext _localctx = new InitializingContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_initializing);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86); accessModes();
			setState(87); functionHead();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenericContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(BCRParser.NAME, 0); }
		public TerminalNode CHARARRAY(int i) {
			return getToken(BCRParser.CHARARRAY, i);
		}
		public List<TerminalNode> CHARARRAY() { return getTokens(BCRParser.CHARARRAY); }
		public GenericContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_generic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterGeneric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitGeneric(this);
		}
	}

	public final GenericContext generic() throws RecognitionException {
		GenericContext _localctx = new GenericContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_generic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89); match(29);
			setState(90); match(9);
			setState(91); match(NAME);
			setState(92); match(16);
			setState(93); match(9);
			setState(94); match(CHARARRAY);
			setState(99);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==27) {
				{
				{
				setState(95); match(27);
				setState(96); match(CHARARRAY);
				}
				}
				setState(101);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(102); match(16);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AccessModesContext extends ParserRuleContext {
		public TerminalNode ACCESSMODE(int i) {
			return getToken(BCRParser.ACCESSMODE, i);
		}
		public List<TerminalNode> ACCESSMODE() { return getTokens(BCRParser.ACCESSMODE); }
		public AccessModesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_accessModes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterAccessModes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitAccessModes(this);
		}
	}

	public final AccessModesContext accessModes() throws RecognitionException {
		AccessModesContext _localctx = new AccessModesContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_accessModes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104); match(41);
			setState(105); match(9);
			setState(114);
			_la = _input.LA(1);
			if (_la==ACCESSMODE) {
				{
				setState(106); match(ACCESSMODE);
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(107); match(27);
					setState(108); match(ACCESSMODE);
					}
					}
					setState(113);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(116); match(16);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionHeadContext extends ParserRuleContext {
		public TerminalNode NAME(int i) {
			return getToken(BCRParser.NAME, i);
		}
		public List<TerminalNode> NAME() { return getTokens(BCRParser.NAME); }
		public FunctionHeadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionHead; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterFunctionHead(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitFunctionHead(this);
		}
	}

	public final FunctionHeadContext functionHead() throws RecognitionException {
		FunctionHeadContext _localctx = new FunctionHeadContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_functionHead);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118); match(NAME);
			setState(119); match(24);
			setState(128);
			_la = _input.LA(1);
			if (_la==NAME) {
				{
				setState(120); match(NAME);
				setState(125);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(121); match(27);
					setState(122); match(NAME);
					}
					}
					setState(127);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(130); match(39);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public IfinstructionContext ifinstruction() {
			return getRuleContext(IfinstructionContext.class,0);
		}
		public SimpleCommandContext simpleCommand() {
			return getRuleContext(SimpleCommandContext.class,0);
		}
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitCommand(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_command);
		try {
			setState(134);
			switch (_input.LA(1)) {
			case 17:
				enterOuterAlt(_localctx, 1);
				{
				setState(132); ifinstruction();
				}
				break;
			case 2:
			case 7:
			case 18:
			case 20:
			case 21:
			case 23:
			case 26:
			case TYPE:
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(133); simpleCommand();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleCommandContext extends ParserRuleContext {
		public ReturnCommandContext returnCommand() {
			return getRuleContext(ReturnCommandContext.class,0);
		}
		public ContinueCommandContext continueCommand() {
			return getRuleContext(ContinueCommandContext.class,0);
		}
		public DoWhileCommandContext doWhileCommand() {
			return getRuleContext(DoWhileCommandContext.class,0);
		}
		public LabelCommandContext labelCommand() {
			return getRuleContext(LabelCommandContext.class,0);
		}
		public BreakCommandContext breakCommand() {
			return getRuleContext(BreakCommandContext.class,0);
		}
		public MethodCallCommandContext methodCallCommand() {
			return getRuleContext(MethodCallCommandContext.class,0);
		}
		public WhileCommandContext whileCommand() {
			return getRuleContext(WhileCommandContext.class,0);
		}
		public AssignmentCommandContext assignmentCommand() {
			return getRuleContext(AssignmentCommandContext.class,0);
		}
		public SimpleCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterSimpleCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitSimpleCommand(this);
		}
	}

	public final SimpleCommandContext simpleCommand() throws RecognitionException {
		SimpleCommandContext _localctx = new SimpleCommandContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_simpleCommand);
		try {
			setState(144);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(136); labelCommand();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(137); returnCommand();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(138); breakCommand();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(139); continueCommand();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(140); whileCommand();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(141); doWhileCommand();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(142); assignmentCommand();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(143); methodCallCommand();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelCommandContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(BCRParser.NAME, 0); }
		public LabelCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labelCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterLabelCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitLabelCommand(this);
		}
	}

	public final LabelCommandContext labelCommand() throws RecognitionException {
		LabelCommandContext _localctx = new LabelCommandContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_labelCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146); match(NAME);
			setState(147); match(30);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnCommandContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NAME() { return getToken(BCRParser.NAME, 0); }
		public ReturnCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterReturnCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitReturnCommand(this);
		}
	}

	public final ReturnCommandContext returnCommand() throws RecognitionException {
		ReturnCommandContext _localctx = new ReturnCommandContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_returnCommand);
		try {
			setState(160);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(149); match(21);
				setState(150); match(8);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(151); match(26);
				setState(152); match(8);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(153); match(26);
				setState(154); match(14);
				setState(155); match(NAME);
				setState(156); match(34);
				setState(157); expr();
				setState(158); match(8);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileCommandContext extends ParserRuleContext {
		public IfElseBodyContext ifElseBody() {
			return getRuleContext(IfElseBodyContext.class,0);
		}
		public IfcontentContext ifcontent() {
			return getRuleContext(IfcontentContext.class,0);
		}
		public WhileCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterWhileCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitWhileCommand(this);
		}
	}

	public final WhileCommandContext whileCommand() throws RecognitionException {
		WhileCommandContext _localctx = new WhileCommandContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_whileCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(162); match(7);
			setState(163); match(24);
			setState(164); ifcontent();
			setState(165); match(39);
			setState(166); ifElseBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoWhileCommandContext extends ParserRuleContext {
		public IfElseBodyContext ifElseBody() {
			return getRuleContext(IfElseBodyContext.class,0);
		}
		public IfcontentContext ifcontent() {
			return getRuleContext(IfcontentContext.class,0);
		}
		public DoWhileCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doWhileCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterDoWhileCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitDoWhileCommand(this);
		}
	}

	public final DoWhileCommandContext doWhileCommand() throws RecognitionException {
		DoWhileCommandContext _localctx = new DoWhileCommandContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_doWhileCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(168); match(23);
			setState(169); ifElseBody();
			setState(170); match(7);
			setState(171); match(24);
			setState(172); ifcontent();
			setState(173); match(39);
			setState(174); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakCommandContext extends ParserRuleContext {
		public BreakCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterBreakCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitBreakCommand(this);
		}
	}

	public final BreakCommandContext breakCommand() throws RecognitionException {
		BreakCommandContext _localctx = new BreakCommandContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_breakCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176); match(20);
			setState(177); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContinueCommandContext extends ParserRuleContext {
		public ContinueCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continueCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterContinueCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitContinueCommand(this);
		}
	}

	public final ContinueCommandContext continueCommand() throws RecognitionException {
		ContinueCommandContext _localctx = new ContinueCommandContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_continueCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179); match(2);
			setState(180); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentCommandContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NAME() { return getToken(BCRParser.NAME, 0); }
		public TerminalNode TYPE() { return getToken(BCRParser.TYPE, 0); }
		public TypeInformationContext typeInformation() {
			return getRuleContext(TypeInformationContext.class,0);
		}
		public AssignmentCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterAssignmentCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitAssignmentCommand(this);
		}
	}

	public final AssignmentCommandContext assignmentCommand() throws RecognitionException {
		AssignmentCommandContext _localctx = new AssignmentCommandContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_assignmentCommand);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(183);
			_la = _input.LA(1);
			if (_la==18) {
				{
				setState(182); typeInformation();
				}
			}

			setState(186);
			_la = _input.LA(1);
			if (_la==TYPE) {
				{
				setState(185); match(TYPE);
				}
			}

			setState(188); match(NAME);
			setState(189); match(15);
			setState(190); expr();
			setState(191); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodCallCommandContext extends ParserRuleContext {
		public MethodCallContext methodCall() {
			return getRuleContext(MethodCallContext.class,0);
		}
		public MethodCallCommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodCallCommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterMethodCallCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitMethodCallCommand(this);
		}
	}

	public final MethodCallCommandContext methodCallCommand() throws RecognitionException {
		MethodCallCommandContext _localctx = new MethodCallCommandContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_methodCallCommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193); methodCall();
			setState(194); match(8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public OrExprContext orExpr() {
			return getRuleContext(OrExprContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitExpr(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196); orExpr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrExprContext extends ParserRuleContext {
		public OrExprContext orExpr() {
			return getRuleContext(OrExprContext.class,0);
		}
		public XorExprContext xorExpr() {
			return getRuleContext(XorExprContext.class,0);
		}
		public OrExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterOrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitOrExpr(this);
		}
	}

	public final OrExprContext orExpr() throws RecognitionException {
		return orExpr(0);
	}

	private OrExprContext orExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		OrExprContext _localctx = new OrExprContext(_ctx, _parentState);
		OrExprContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_orExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(199); xorExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(206);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new OrExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_orExpr);
					setState(201);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(202); match(32);
					setState(203); xorExpr(0);
					}
					} 
				}
				setState(208);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,13,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class XorExprContext extends ParserRuleContext {
		public AndExprContext andExpr() {
			return getRuleContext(AndExprContext.class,0);
		}
		public XorExprContext xorExpr() {
			return getRuleContext(XorExprContext.class,0);
		}
		public XorExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_xorExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterXorExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitXorExpr(this);
		}
	}

	public final XorExprContext xorExpr() throws RecognitionException {
		return xorExpr(0);
	}

	private XorExprContext xorExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		XorExprContext _localctx = new XorExprContext(_ctx, _parentState);
		XorExprContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_xorExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(210); andExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(217);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new XorExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_xorExpr);
					setState(212);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(213); match(19);
					setState(214); andExpr(0);
					}
					} 
				}
				setState(219);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AndExprContext extends ParserRuleContext {
		public AndExprContext andExpr() {
			return getRuleContext(AndExprContext.class,0);
		}
		public ShiftExprContext shiftExpr() {
			return getRuleContext(ShiftExprContext.class,0);
		}
		public AndExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterAndExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitAndExpr(this);
		}
	}

	public final AndExprContext andExpr() throws RecognitionException {
		return andExpr(0);
	}

	private AndExprContext andExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AndExprContext _localctx = new AndExprContext(_ctx, _parentState);
		AndExprContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_andExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(221); shiftExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(228);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AndExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_andExpr);
					setState(223);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(224); match(22);
					setState(225); shiftExpr(0);
					}
					} 
				}
				setState(230);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ShiftExprContext extends ParserRuleContext {
		public AddExprContext addExpr() {
			return getRuleContext(AddExprContext.class,0);
		}
		public ShiftExprContext shiftExpr() {
			return getRuleContext(ShiftExprContext.class,0);
		}
		public ShiftExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterShiftExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitShiftExpr(this);
		}
	}

	public final ShiftExprContext shiftExpr() throws RecognitionException {
		return shiftExpr(0);
	}

	private ShiftExprContext shiftExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ShiftExprContext _localctx = new ShiftExprContext(_ctx, _parentState);
		ShiftExprContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_shiftExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(232); addExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(239);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ShiftExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_shiftExpr);
					setState(234);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(235);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 12) | (1L << 13) | (1L << 31))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(236); addExpr(0);
					}
					} 
				}
				setState(241);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AddExprContext extends ParserRuleContext {
		public AddExprContext addExpr() {
			return getRuleContext(AddExprContext.class,0);
		}
		public MulExprContext mulExpr() {
			return getRuleContext(MulExprContext.class,0);
		}
		public AddExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterAddExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitAddExpr(this);
		}
	}

	public final AddExprContext addExpr() throws RecognitionException {
		return addExpr(0);
	}

	private AddExprContext addExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AddExprContext _localctx = new AddExprContext(_ctx, _parentState);
		AddExprContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, 40, RULE_addExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(243); mulExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(250);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new AddExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_addExpr);
					setState(245);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(246);
					_la = _input.LA(1);
					if ( !(_la==40 || _la==42) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(247); mulExpr(0);
					}
					} 
				}
				setState(252);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MulExprContext extends ParserRuleContext {
		public UnaryExprContext unaryExpr() {
			return getRuleContext(UnaryExprContext.class,0);
		}
		public MulExprContext mulExpr() {
			return getRuleContext(MulExprContext.class,0);
		}
		public MulExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mulExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterMulExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitMulExpr(this);
		}
	}

	public final MulExprContext mulExpr() throws RecognitionException {
		return mulExpr(0);
	}

	private MulExprContext mulExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MulExprContext _localctx = new MulExprContext(_ctx, _parentState);
		MulExprContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_mulExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(254); unaryExpr();
			}
			_ctx.stop = _input.LT(-1);
			setState(261);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new MulExprContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_mulExpr);
					setState(256);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(257);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 25) | (1L << 37))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(258); unaryExpr();
					}
					} 
				}
				setState(263);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnaryExprContext extends ParserRuleContext {
		public UnaryExprContext unaryExpr() {
			return getRuleContext(UnaryExprContext.class,0);
		}
		public NewExprContext newExpr() {
			return getRuleContext(NewExprContext.class,0);
		}
		public LiteralExprContext literalExpr() {
			return getRuleContext(LiteralExprContext.class,0);
		}
		public MethodCallContext methodCall() {
			return getRuleContext(MethodCallContext.class,0);
		}
		public ConstantExprContext constantExpr() {
			return getRuleContext(ConstantExprContext.class,0);
		}
		public UnaryExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterUnaryExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitUnaryExpr(this);
		}
	}

	public final UnaryExprContext unaryExpr() throws RecognitionException {
		UnaryExprContext _localctx = new UnaryExprContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_unaryExpr);
		int _la;
		try {
			setState(270);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(264); literalExpr();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(265); constantExpr();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(266); methodCall();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(267); newExpr();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(268);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 33) | (1L << 35) | (1L << 42))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(269); unaryExpr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantExprContext extends ParserRuleContext {
		public TerminalNode CHARARRAY() { return getToken(BCRParser.CHARARRAY, 0); }
		public TerminalNode INT() { return getToken(BCRParser.INT, 0); }
		public TerminalNode DOUBLE() { return getToken(BCRParser.DOUBLE, 0); }
		public ConstantExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constantExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterConstantExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitConstantExpr(this);
		}
	}

	public final ConstantExprContext constantExpr() throws RecognitionException {
		ConstantExprContext _localctx = new ConstantExprContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_constantExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CHARARRAY) | (1L << DOUBLE) | (1L << INT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralExprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NAME() { return getToken(BCRParser.NAME, 0); }
		public LiteralExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterLiteralExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitLiteralExpr(this);
		}
	}

	public final LiteralExprContext literalExpr() throws RecognitionException {
		LiteralExprContext _localctx = new LiteralExprContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_literalExpr);
		try {
			setState(279);
			switch (_input.LA(1)) {
			case NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(274); match(NAME);
				}
				break;
			case 24:
				enterOuterAlt(_localctx, 2);
				{
				setState(275); match(24);
				setState(276); expr();
				setState(277); match(39);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NewExprContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode NAME() { return getToken(BCRParser.NAME, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public NewExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_newExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterNewExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitNewExpr(this);
		}
	}

	public final NewExprContext newExpr() throws RecognitionException {
		NewExprContext _localctx = new NewExprContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_newExpr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(281); match(5);
			setState(282); match(NAME);
			setState(283); match(24);
			setState(292);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 5) | (1L << 18) | (1L << 24) | (1L << 33) | (1L << 35) | (1L << 42) | (1L << CHARARRAY) | (1L << NAME) | (1L << DOUBLE) | (1L << INT))) != 0)) {
				{
				setState(284); expr();
				setState(289);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(285); match(27);
					setState(286); expr();
					}
					}
					setState(291);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(294); match(39);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodCallContext extends ParserRuleContext {
		public TerminalNode NAME(int i) {
			return getToken(BCRParser.NAME, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public List<TerminalNode> NAME() { return getTokens(BCRParser.NAME); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TypeInformationContext typeInformation() {
			return getRuleContext(TypeInformationContext.class,0);
		}
		public MethodCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterMethodCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitMethodCall(this);
		}
	}

	public final MethodCallContext methodCall() throws RecognitionException {
		MethodCallContext _localctx = new MethodCallContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_methodCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			_la = _input.LA(1);
			if (_la==18) {
				{
				setState(296); typeInformation();
				}
			}

			setState(301);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(299); match(NAME);
				setState(300); match(28);
				}
				break;
			}
			setState(303); match(NAME);
			setState(304); match(24);
			setState(313);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 5) | (1L << 18) | (1L << 24) | (1L << 33) | (1L << 35) | (1L << 42) | (1L << CHARARRAY) | (1L << NAME) | (1L << DOUBLE) | (1L << INT))) != 0)) {
				{
				setState(305); expr();
				setState(310);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(306); match(27);
					setState(307); expr();
					}
					}
					setState(312);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(315); match(39);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeInformationContext extends ParserRuleContext {
		public TerminalNode TYPE() { return getToken(BCRParser.TYPE, 0); }
		public TypeInformationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeInformation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterTypeInformation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitTypeInformation(this);
		}
	}

	public final TypeInformationContext typeInformation() throws RecognitionException {
		TypeInformationContext _localctx = new TypeInformationContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_typeInformation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(317); match(18);
			setState(318); match(9);
			setState(319); match(TYPE);
			setState(320); match(16);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfinstructionContext extends ParserRuleContext {
		public List<IfElseBodyContext> ifElseBody() {
			return getRuleContexts(IfElseBodyContext.class);
		}
		public IfElseBodyContext ifElseBody(int i) {
			return getRuleContext(IfElseBodyContext.class,i);
		}
		public IfcontentContext ifcontent() {
			return getRuleContext(IfcontentContext.class,0);
		}
		public IfinstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifinstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterIfinstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitIfinstruction(this);
		}
	}

	public final IfinstructionContext ifinstruction() throws RecognitionException {
		IfinstructionContext _localctx = new IfinstructionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_ifinstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(322); match(17);
			setState(323); match(24);
			setState(324); ifcontent();
			setState(325); match(39);
			setState(326); ifElseBody();
			setState(329);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				setState(327); match(38);
				setState(328); ifElseBody();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfcontentContext extends ParserRuleContext {
		public IfconnectionContext ifconnection() {
			return getRuleContext(IfconnectionContext.class,0);
		}
		public IfcontentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifcontent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterIfcontent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitIfcontent(this);
		}
	}

	public final IfcontentContext ifcontent() throws RecognitionException {
		IfcontentContext _localctx = new IfcontentContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_ifcontent);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(331); ifconnection();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfconnectionContext extends ParserRuleContext {
		public IfsubconditionContext ifsubcondition(int i) {
			return getRuleContext(IfsubconditionContext.class,i);
		}
		public List<IfsubconditionContext> ifsubcondition() {
			return getRuleContexts(IfsubconditionContext.class);
		}
		public IfconnectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifconnection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterIfconnection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitIfconnection(this);
		}
	}

	public final IfconnectionContext ifconnection() throws RecognitionException {
		IfconnectionContext _localctx = new IfconnectionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_ifconnection);
		int _la;
		try {
			setState(349);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(333); ifsubcondition();
				setState(338);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==11) {
					{
					{
					setState(334); match(11);
					setState(335); ifsubcondition();
					}
					}
					setState(340);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(341); ifsubcondition();
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==6) {
					{
					{
					setState(342); match(6);
					setState(343); ifsubcondition();
					}
					}
					setState(348);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfsubconditionContext extends ParserRuleContext {
		public IfliteralContext ifliteral() {
			return getRuleContext(IfliteralContext.class,0);
		}
		public IfconnectionContext ifconnection() {
			return getRuleContext(IfconnectionContext.class,0);
		}
		public IfsubconditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifsubcondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterIfsubcondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitIfsubcondition(this);
		}
	}

	public final IfsubconditionContext ifsubcondition() throws RecognitionException {
		IfsubconditionContext _localctx = new IfsubconditionContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_ifsubcondition);
		int _la;
		try {
			setState(359);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(351); ifliteral();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(353);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(352); match(35);
					}
				}

				setState(355); match(24);
				setState(356); ifconnection();
				setState(357); match(39);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfliteralContext extends ParserRuleContext {
		public TerminalNode NAME(int i) {
			return getToken(BCRParser.NAME, i);
		}
		public TerminalNode PARALLELMODE(int i) {
			return getToken(BCRParser.PARALLELMODE, i);
		}
		public TerminalNode NULL() { return getToken(BCRParser.NULL, 0); }
		public TerminalNode ACCESSMODE(int i) {
			return getToken(BCRParser.ACCESSMODE, i);
		}
		public List<TerminalNode> NAME() { return getTokens(BCRParser.NAME); }
		public List<TerminalNode> PARALLELMODE() { return getTokens(BCRParser.PARALLELMODE); }
		public TypeInformationContext typeInformation() {
			return getRuleContext(TypeInformationContext.class,0);
		}
		public TerminalNode IFOPERATOR() { return getToken(BCRParser.IFOPERATOR, 0); }
		public TerminalNode INT() { return getToken(BCRParser.INT, 0); }
		public TerminalNode DOUBLE() { return getToken(BCRParser.DOUBLE, 0); }
		public List<TerminalNode> ACCESSMODE() { return getTokens(BCRParser.ACCESSMODE); }
		public IfliteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifliteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterIfliteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitIfliteral(this);
		}
	}

	public final IfliteralContext ifliteral() throws RecognitionException {
		IfliteralContext _localctx = new IfliteralContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_ifliteral);
		int _la;
		try {
			setState(436);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(362);
				_la = _input.LA(1);
				if (_la==18) {
					{
					setState(361); typeInformation();
					}
				}

				setState(365);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(364); match(35);
					}
				}

				setState(367); match(NAME);
				setState(368); match(IFOPERATOR);
				setState(374);
				switch (_input.LA(1)) {
				case NAME:
					{
					setState(369); match(NAME);
					}
					break;
				case 42:
				case DOUBLE:
				case INT:
					{
					{
					setState(371);
					_la = _input.LA(1);
					if (_la==42) {
						{
						setState(370); match(42);
						}
					}

					setState(373);
					_la = _input.LA(1);
					if ( !(_la==DOUBLE || _la==INT) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(377);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(376); match(35);
					}
				}

				setState(379); match(NAME);
				setState(380); match(10);
				setState(381); match(NAME);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(383);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(382); match(35);
					}
				}

				setState(385); match(NAME);
				setState(386); match(IFOPERATOR);
				setState(387); match(NULL);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(389);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(388); match(35);
					}
				}

				setState(391); match(36);
				setState(392); match(24);
				setState(393); match(NAME);
				setState(394); match(39);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(396);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(395); match(35);
					}
				}

				setState(398); match(4);
				setState(399); match(24);
				setState(400); match(24);
				setState(401); match(NAME);
				setState(406);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(402); match(27);
					setState(403); match(NAME);
					}
					}
					setState(408);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(409); match(39);
				setState(410); match(27);
				setState(411); match(24);
				setState(412); match(ACCESSMODE);
				setState(417);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==6) {
					{
					{
					setState(413); match(6);
					setState(414); match(ACCESSMODE);
					}
					}
					setState(419);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(420); match(39);
				setState(421); match(39);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(423);
				_la = _input.LA(1);
				if (_la==35) {
					{
					setState(422); match(35);
					}
				}

				setState(425); match(3);
				setState(426); match(24);
				setState(427); match(PARALLELMODE);
				setState(432);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==27) {
					{
					{
					setState(428); match(27);
					setState(429); match(PARALLELMODE);
					}
					}
					setState(434);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(435); match(39);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfElseBodyContext extends ParserRuleContext {
		public List<CommandContext> command() {
			return getRuleContexts(CommandContext.class);
		}
		public CommandContext command(int i) {
			return getRuleContext(CommandContext.class,i);
		}
		public IfElseBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifElseBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).enterIfElseBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BCRListener ) ((BCRListener)listener).exitIfElseBody(this);
		}
	}

	public final IfElseBodyContext ifElseBody() throws RecognitionException {
		IfElseBodyContext _localctx = new IfElseBodyContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_ifElseBody);
		int _la;
		try {
			setState(447);
			switch (_input.LA(1)) {
			case 2:
			case 7:
			case 17:
			case 18:
			case 20:
			case 21:
			case 23:
			case 26:
			case TYPE:
			case NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(438); command();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 2);
				{
				setState(439); match(9);
				setState(443);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 7) | (1L << 17) | (1L << 18) | (1L << 20) | (1L << 21) | (1L << 23) | (1L << 26) | (1L << TYPE) | (1L << NAME))) != 0)) {
					{
					{
					setState(440); command();
					}
					}
					setState(445);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(446); match(16);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 16: return orExpr_sempred((OrExprContext)_localctx, predIndex);

		case 17: return xorExpr_sempred((XorExprContext)_localctx, predIndex);

		case 18: return andExpr_sempred((AndExprContext)_localctx, predIndex);

		case 19: return shiftExpr_sempred((ShiftExprContext)_localctx, predIndex);

		case 20: return addExpr_sempred((AddExprContext)_localctx, predIndex);

		case 21: return mulExpr_sempred((MulExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean addExpr_sempred(AddExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean mulExpr_sempred(MulExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean orExpr_sempred(OrExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean xorExpr_sempred(XorExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean andExpr_sempred(AndExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean shiftExpr_sempred(ShiftExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3: return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\39\u01c4\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\3\2\7\2H\n\2\f\2\16\2K\13\2\3\2\3\2\3\2\7\2P\n\2\f\2"+
		"\16\2S\13\2\3\2\3\2\5\2W\n\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\7\4d\n\4\f\4\16\4g\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\7\5p\n\5\f\5\16"+
		"\5s\13\5\5\5u\n\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\7\6~\n\6\f\6\16\6\u0081"+
		"\13\6\5\6\u0083\n\6\3\6\3\6\3\7\3\7\5\7\u0089\n\7\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\5\b\u0093\n\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\5\n\u00a3\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\17\5\17\u00ba\n\17\3\17"+
		"\5\17\u00bd\n\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\7\22\u00cf\n\22\f\22\16\22\u00d2\13\22\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\7\23\u00da\n\23\f\23\16\23\u00dd\13\23\3\24"+
		"\3\24\3\24\3\24\3\24\3\24\7\24\u00e5\n\24\f\24\16\24\u00e8\13\24\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\7\25\u00f0\n\25\f\25\16\25\u00f3\13\25\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\7\26\u00fb\n\26\f\26\16\26\u00fe\13\26\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\7\27\u0106\n\27\f\27\16\27\u0109\13\27\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\5\30\u0111\n\30\3\31\3\31\3\32\3\32\3\32\3\32"+
		"\3\32\5\32\u011a\n\32\3\33\3\33\3\33\3\33\3\33\3\33\7\33\u0122\n\33\f"+
		"\33\16\33\u0125\13\33\5\33\u0127\n\33\3\33\3\33\3\34\5\34\u012c\n\34\3"+
		"\34\3\34\5\34\u0130\n\34\3\34\3\34\3\34\3\34\3\34\7\34\u0137\n\34\f\34"+
		"\16\34\u013a\13\34\5\34\u013c\n\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35"+
		"\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u014c\n\36\3\37\3\37\3 \3 \3"+
		" \7 \u0153\n \f \16 \u0156\13 \3 \3 \3 \7 \u015b\n \f \16 \u015e\13 \5"+
		" \u0160\n \3!\3!\5!\u0164\n!\3!\3!\3!\3!\5!\u016a\n!\3\"\5\"\u016d\n\""+
		"\3\"\5\"\u0170\n\"\3\"\3\"\3\"\3\"\5\"\u0176\n\"\3\"\5\"\u0179\n\"\3\""+
		"\5\"\u017c\n\"\3\"\3\"\3\"\3\"\5\"\u0182\n\"\3\"\3\"\3\"\3\"\5\"\u0188"+
		"\n\"\3\"\3\"\3\"\3\"\3\"\5\"\u018f\n\"\3\"\3\"\3\"\3\"\3\"\3\"\7\"\u0197"+
		"\n\"\f\"\16\"\u019a\13\"\3\"\3\"\3\"\3\"\3\"\3\"\7\"\u01a2\n\"\f\"\16"+
		"\"\u01a5\13\"\3\"\3\"\3\"\5\"\u01aa\n\"\3\"\3\"\3\"\3\"\3\"\7\"\u01b1"+
		"\n\"\f\"\16\"\u01b4\13\"\3\"\5\"\u01b7\n\"\3#\3#\3#\7#\u01bc\n#\f#\16"+
		"#\u01bf\13#\3#\5#\u01c2\n#\3#\2\b\"$&(*,$\2\4\6\b\n\f\16\20\22\24\26\30"+
		"\32\34\36 \"$&(*,.\60\62\64\668:<>@BD\2\b\4\2\16\17!!\4\2**,,\5\2\3\3"+
		"\33\33\'\'\5\2##%%,,\4\2--\63\64\3\2\63\64\u01df\2I\3\2\2\2\4X\3\2\2\2"+
		"\6[\3\2\2\2\bj\3\2\2\2\nx\3\2\2\2\f\u0088\3\2\2\2\16\u0092\3\2\2\2\20"+
		"\u0094\3\2\2\2\22\u00a2\3\2\2\2\24\u00a4\3\2\2\2\26\u00aa\3\2\2\2\30\u00b2"+
		"\3\2\2\2\32\u00b5\3\2\2\2\34\u00b9\3\2\2\2\36\u00c3\3\2\2\2 \u00c6\3\2"+
		"\2\2\"\u00c8\3\2\2\2$\u00d3\3\2\2\2&\u00de\3\2\2\2(\u00e9\3\2\2\2*\u00f4"+
		"\3\2\2\2,\u00ff\3\2\2\2.\u0110\3\2\2\2\60\u0112\3\2\2\2\62\u0119\3\2\2"+
		"\2\64\u011b\3\2\2\2\66\u012b\3\2\2\28\u013f\3\2\2\2:\u0144\3\2\2\2<\u014d"+
		"\3\2\2\2>\u015f\3\2\2\2@\u0169\3\2\2\2B\u01b6\3\2\2\2D\u01c1\3\2\2\2F"+
		"H\5\6\4\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2JV\3\2\2\2KI\3\2\2\2"+
		"LM\5\4\3\2MQ\7\13\2\2NP\5\f\7\2ON\3\2\2\2PS\3\2\2\2QO\3\2\2\2QR\3\2\2"+
		"\2RT\3\2\2\2SQ\3\2\2\2TU\7\22\2\2UW\3\2\2\2VL\3\2\2\2VW\3\2\2\2W\3\3\2"+
		"\2\2XY\5\b\5\2YZ\5\n\6\2Z\5\3\2\2\2[\\\7\37\2\2\\]\7\13\2\2]^\7\62\2\2"+
		"^_\7\22\2\2_`\7\13\2\2`e\7-\2\2ab\7\35\2\2bd\7-\2\2ca\3\2\2\2dg\3\2\2"+
		"\2ec\3\2\2\2ef\3\2\2\2fh\3\2\2\2ge\3\2\2\2hi\7\22\2\2i\7\3\2\2\2jk\7+"+
		"\2\2kt\7\13\2\2lq\7\60\2\2mn\7\35\2\2np\7\60\2\2om\3\2\2\2ps\3\2\2\2q"+
		"o\3\2\2\2qr\3\2\2\2ru\3\2\2\2sq\3\2\2\2tl\3\2\2\2tu\3\2\2\2uv\3\2\2\2"+
		"vw\7\22\2\2w\t\3\2\2\2xy\7\62\2\2y\u0082\7\32\2\2z\177\7\62\2\2{|\7\35"+
		"\2\2|~\7\62\2\2}{\3\2\2\2~\u0081\3\2\2\2\177}\3\2\2\2\177\u0080\3\2\2"+
		"\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0082z\3\2\2\2\u0082\u0083\3"+
		"\2\2\2\u0083\u0084\3\2\2\2\u0084\u0085\7)\2\2\u0085\13\3\2\2\2\u0086\u0089"+
		"\5:\36\2\u0087\u0089\5\16\b\2\u0088\u0086\3\2\2\2\u0088\u0087\3\2\2\2"+
		"\u0089\r\3\2\2\2\u008a\u0093\5\20\t\2\u008b\u0093\5\22\n\2\u008c\u0093"+
		"\5\30\r\2\u008d\u0093\5\32\16\2\u008e\u0093\5\24\13\2\u008f\u0093\5\26"+
		"\f\2\u0090\u0093\5\34\17\2\u0091\u0093\5\36\20\2\u0092\u008a\3\2\2\2\u0092"+
		"\u008b\3\2\2\2\u0092\u008c\3\2\2\2\u0092\u008d\3\2\2\2\u0092\u008e\3\2"+
		"\2\2\u0092\u008f\3\2\2\2\u0092\u0090\3\2\2\2\u0092\u0091\3\2\2\2\u0093"+
		"\17\3\2\2\2\u0094\u0095\7\62\2\2\u0095\u0096\7 \2\2\u0096\21\3\2\2\2\u0097"+
		"\u0098\7\27\2\2\u0098\u00a3\7\n\2\2\u0099\u009a\7\34\2\2\u009a\u00a3\7"+
		"\n\2\2\u009b\u009c\7\34\2\2\u009c\u009d\7\20\2\2\u009d\u009e\7\62\2\2"+
		"\u009e\u009f\7$\2\2\u009f\u00a0\5 \21\2\u00a0\u00a1\7\n\2\2\u00a1\u00a3"+
		"\3\2\2\2\u00a2\u0097\3\2\2\2\u00a2\u0099\3\2\2\2\u00a2\u009b\3\2\2\2\u00a3"+
		"\23\3\2\2\2\u00a4\u00a5\7\t\2\2\u00a5\u00a6\7\32\2\2\u00a6\u00a7\5<\37"+
		"\2\u00a7\u00a8\7)\2\2\u00a8\u00a9\5D#\2\u00a9\25\3\2\2\2\u00aa\u00ab\7"+
		"\31\2\2\u00ab\u00ac\5D#\2\u00ac\u00ad\7\t\2\2\u00ad\u00ae\7\32\2\2\u00ae"+
		"\u00af\5<\37\2\u00af\u00b0\7)\2\2\u00b0\u00b1\7\n\2\2\u00b1\27\3\2\2\2"+
		"\u00b2\u00b3\7\26\2\2\u00b3\u00b4\7\n\2\2\u00b4\31\3\2\2\2\u00b5\u00b6"+
		"\7\4\2\2\u00b6\u00b7\7\n\2\2\u00b7\33\3\2\2\2\u00b8\u00ba\58\35\2\u00b9"+
		"\u00b8\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bc\3\2\2\2\u00bb\u00bd\7/"+
		"\2\2\u00bc\u00bb\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00be\3\2\2\2\u00be"+
		"\u00bf\7\62\2\2\u00bf\u00c0\7\21\2\2\u00c0\u00c1\5 \21\2\u00c1\u00c2\7"+
		"\n\2\2\u00c2\35\3\2\2\2\u00c3\u00c4\5\66\34\2\u00c4\u00c5\7\n\2\2\u00c5"+
		"\37\3\2\2\2\u00c6\u00c7\5\"\22\2\u00c7!\3\2\2\2\u00c8\u00c9\b\22\1\2\u00c9"+
		"\u00ca\5$\23\2\u00ca\u00d0\3\2\2\2\u00cb\u00cc\f\3\2\2\u00cc\u00cd\7\""+
		"\2\2\u00cd\u00cf\5$\23\2\u00ce\u00cb\3\2\2\2\u00cf\u00d2\3\2\2\2\u00d0"+
		"\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1#\3\2\2\2\u00d2\u00d0\3\2\2\2"+
		"\u00d3\u00d4\b\23\1\2\u00d4\u00d5\5&\24\2\u00d5\u00db\3\2\2\2\u00d6\u00d7"+
		"\f\3\2\2\u00d7\u00d8\7\25\2\2\u00d8\u00da\5&\24\2\u00d9\u00d6\3\2\2\2"+
		"\u00da\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc%\3"+
		"\2\2\2\u00dd\u00db\3\2\2\2\u00de\u00df\b\24\1\2\u00df\u00e0\5(\25\2\u00e0"+
		"\u00e6\3\2\2\2\u00e1\u00e2\f\3\2\2\u00e2\u00e3\7\30\2\2\u00e3\u00e5\5"+
		"(\25\2\u00e4\u00e1\3\2\2\2\u00e5\u00e8\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e6"+
		"\u00e7\3\2\2\2\u00e7\'\3\2\2\2\u00e8\u00e6\3\2\2\2\u00e9\u00ea\b\25\1"+
		"\2\u00ea\u00eb\5*\26\2\u00eb\u00f1\3\2\2\2\u00ec\u00ed\f\3\2\2\u00ed\u00ee"+
		"\t\2\2\2\u00ee\u00f0\5*\26\2\u00ef\u00ec\3\2\2\2\u00f0\u00f3\3\2\2\2\u00f1"+
		"\u00ef\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2)\3\2\2\2\u00f3\u00f1\3\2\2\2"+
		"\u00f4\u00f5\b\26\1\2\u00f5\u00f6\5,\27\2\u00f6\u00fc\3\2\2\2\u00f7\u00f8"+
		"\f\3\2\2\u00f8\u00f9\t\3\2\2\u00f9\u00fb\5,\27\2\u00fa\u00f7\3\2\2\2\u00fb"+
		"\u00fe\3\2\2\2\u00fc\u00fa\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd+\3\2\2\2"+
		"\u00fe\u00fc\3\2\2\2\u00ff\u0100\b\27\1\2\u0100\u0101\5.\30\2\u0101\u0107"+
		"\3\2\2\2\u0102\u0103\f\3\2\2\u0103\u0104\t\4\2\2\u0104\u0106\5.\30\2\u0105"+
		"\u0102\3\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2"+
		"\2\2\u0108-\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u0111\5\62\32\2\u010b\u0111"+
		"\5\60\31\2\u010c\u0111\5\66\34\2\u010d\u0111\5\64\33\2\u010e\u010f\t\5"+
		"\2\2\u010f\u0111\5.\30\2\u0110\u010a\3\2\2\2\u0110\u010b\3\2\2\2\u0110"+
		"\u010c\3\2\2\2\u0110\u010d\3\2\2\2\u0110\u010e\3\2\2\2\u0111/\3\2\2\2"+
		"\u0112\u0113\t\6\2\2\u0113\61\3\2\2\2\u0114\u011a\7\62\2\2\u0115\u0116"+
		"\7\32\2\2\u0116\u0117\5 \21\2\u0117\u0118\7)\2\2\u0118\u011a\3\2\2\2\u0119"+
		"\u0114\3\2\2\2\u0119\u0115\3\2\2\2\u011a\63\3\2\2\2\u011b\u011c\7\7\2"+
		"\2\u011c\u011d\7\62\2\2\u011d\u0126\7\32\2\2\u011e\u0123\5 \21\2\u011f"+
		"\u0120\7\35\2\2\u0120\u0122\5 \21\2\u0121\u011f\3\2\2\2\u0122\u0125\3"+
		"\2\2\2\u0123\u0121\3\2\2\2\u0123\u0124\3\2\2\2\u0124\u0127\3\2\2\2\u0125"+
		"\u0123\3\2\2\2\u0126\u011e\3\2\2\2\u0126\u0127\3\2\2\2\u0127\u0128\3\2"+
		"\2\2\u0128\u0129\7)\2\2\u0129\65\3\2\2\2\u012a\u012c\58\35\2\u012b\u012a"+
		"\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012f\3\2\2\2\u012d\u012e\7\62\2\2"+
		"\u012e\u0130\7\36\2\2\u012f\u012d\3\2\2\2\u012f\u0130\3\2\2\2\u0130\u0131"+
		"\3\2\2\2\u0131\u0132\7\62\2\2\u0132\u013b\7\32\2\2\u0133\u0138\5 \21\2"+
		"\u0134\u0135\7\35\2\2\u0135\u0137\5 \21\2\u0136\u0134\3\2\2\2\u0137\u013a"+
		"\3\2\2\2\u0138\u0136\3\2\2\2\u0138\u0139\3\2\2\2\u0139\u013c\3\2\2\2\u013a"+
		"\u0138\3\2\2\2\u013b\u0133\3\2\2\2\u013b\u013c\3\2\2\2\u013c\u013d\3\2"+
		"\2\2\u013d\u013e\7)\2\2\u013e\67\3\2\2\2\u013f\u0140\7\24\2\2\u0140\u0141"+
		"\7\13\2\2\u0141\u0142\7/\2\2\u0142\u0143\7\22\2\2\u01439\3\2\2\2\u0144"+
		"\u0145\7\23\2\2\u0145\u0146\7\32\2\2\u0146\u0147\5<\37\2\u0147\u0148\7"+
		")\2\2\u0148\u014b\5D#\2\u0149\u014a\7(\2\2\u014a\u014c\5D#\2\u014b\u0149"+
		"\3\2\2\2\u014b\u014c\3\2\2\2\u014c;\3\2\2\2\u014d\u014e\5> \2\u014e=\3"+
		"\2\2\2\u014f\u0154\5@!\2\u0150\u0151\7\r\2\2\u0151\u0153\5@!\2\u0152\u0150"+
		"\3\2\2\2\u0153\u0156\3\2\2\2\u0154\u0152\3\2\2\2\u0154\u0155\3\2\2\2\u0155"+
		"\u0160\3\2\2\2\u0156\u0154\3\2\2\2\u0157\u015c\5@!\2\u0158\u0159\7\b\2"+
		"\2\u0159\u015b\5@!\2\u015a\u0158\3\2\2\2\u015b\u015e\3\2\2\2\u015c\u015a"+
		"\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u0160\3\2\2\2\u015e\u015c\3\2\2\2\u015f"+
		"\u014f\3\2\2\2\u015f\u0157\3\2\2\2\u0160?\3\2\2\2\u0161\u016a\5B\"\2\u0162"+
		"\u0164\7%\2\2\u0163\u0162\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0165\3\2"+
		"\2\2\u0165\u0166\7\32\2\2\u0166\u0167\5> \2\u0167\u0168\7)\2\2\u0168\u016a"+
		"\3\2\2\2\u0169\u0161\3\2\2\2\u0169\u0163\3\2\2\2\u016aA\3\2\2\2\u016b"+
		"\u016d\58\35\2\u016c\u016b\3\2\2\2\u016c\u016d\3\2\2\2\u016d\u016f\3\2"+
		"\2\2\u016e\u0170\7%\2\2\u016f\u016e\3\2\2\2\u016f\u0170\3\2\2\2\u0170"+
		"\u0171\3\2\2\2\u0171\u0172\7\62\2\2\u0172\u0178\7\65\2\2\u0173\u0179\7"+
		"\62\2\2\u0174\u0176\7,\2\2\u0175\u0174\3\2\2\2\u0175\u0176\3\2\2\2\u0176"+
		"\u0177\3\2\2\2\u0177\u0179\t\7\2\2\u0178\u0173\3\2\2\2\u0178\u0175\3\2"+
		"\2\2\u0179\u01b7\3\2\2\2\u017a\u017c\7%\2\2\u017b\u017a\3\2\2\2\u017b"+
		"\u017c\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017e\7\62\2\2\u017e\u017f\7"+
		"\f\2\2\u017f\u01b7\7\62\2\2\u0180\u0182\7%\2\2\u0181\u0180\3\2\2\2\u0181"+
		"\u0182\3\2\2\2\u0182\u0183\3\2\2\2\u0183\u0184\7\62\2\2\u0184\u0185\7"+
		"\65\2\2\u0185\u01b7\7.\2\2\u0186\u0188\7%\2\2\u0187\u0186\3\2\2\2\u0187"+
		"\u0188\3\2\2\2\u0188\u0189\3\2\2\2\u0189\u018a\7&\2\2\u018a\u018b\7\32"+
		"\2\2\u018b\u018c\7\62\2\2\u018c\u01b7\7)\2\2\u018d\u018f\7%\2\2\u018e"+
		"\u018d\3\2\2\2\u018e\u018f\3\2\2\2\u018f\u0190\3\2\2\2\u0190\u0191\7\6"+
		"\2\2\u0191\u0192\7\32\2\2\u0192\u0193\7\32\2\2\u0193\u0198\7\62\2\2\u0194"+
		"\u0195\7\35\2\2\u0195\u0197\7\62\2\2\u0196\u0194\3\2\2\2\u0197\u019a\3"+
		"\2\2\2\u0198\u0196\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u019b\3\2\2\2\u019a"+
		"\u0198\3\2\2\2\u019b\u019c\7)\2\2\u019c\u019d\7\35\2\2\u019d\u019e\7\32"+
		"\2\2\u019e\u01a3\7\60\2\2\u019f\u01a0\7\b\2\2\u01a0\u01a2\7\60\2\2\u01a1"+
		"\u019f\3\2\2\2\u01a2\u01a5\3\2\2\2\u01a3\u01a1\3\2\2\2\u01a3\u01a4\3\2"+
		"\2\2\u01a4\u01a6\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a6\u01a7\7)\2\2\u01a7"+
		"\u01b7\7)\2\2\u01a8\u01aa\7%\2\2\u01a9\u01a8\3\2\2\2\u01a9\u01aa\3\2\2"+
		"\2\u01aa\u01ab\3\2\2\2\u01ab\u01ac\7\5\2\2\u01ac\u01ad\7\32\2\2\u01ad"+
		"\u01b2\7\61\2\2\u01ae\u01af\7\35\2\2\u01af\u01b1\7\61\2\2\u01b0\u01ae"+
		"\3\2\2\2\u01b1\u01b4\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3"+
		"\u01b5\3\2\2\2\u01b4\u01b2\3\2\2\2\u01b5\u01b7\7)\2\2\u01b6\u016c\3\2"+
		"\2\2\u01b6\u017b\3\2\2\2\u01b6\u0181\3\2\2\2\u01b6\u0187\3\2\2\2\u01b6"+
		"\u018e\3\2\2\2\u01b6\u01a9\3\2\2\2\u01b7C\3\2\2\2\u01b8\u01c2\5\f\7\2"+
		"\u01b9\u01bd\7\13\2\2\u01ba\u01bc\5\f\7\2\u01bb\u01ba\3\2\2\2\u01bc\u01bf"+
		"\3\2\2\2\u01bd\u01bb\3\2\2\2\u01bd\u01be\3\2\2\2\u01be\u01c0\3\2\2\2\u01bf"+
		"\u01bd\3\2\2\2\u01c0\u01c2\7\22\2\2\u01c1\u01b8\3\2\2\2\u01c1\u01b9\3"+
		"\2\2\2\u01c2E\3\2\2\2\62IQVeqt\177\u0082\u0088\u0092\u00a2\u00b9\u00bc"+
		"\u00d0\u00db\u00e6\u00f1\u00fc\u0107\u0110\u0119\u0123\u0126\u012b\u012f"+
		"\u0138\u013b\u014b\u0154\u015c\u015f\u0163\u0169\u016c\u016f\u0175\u0178"+
		"\u017b\u0181\u0187\u018e\u0198\u01a3\u01a9\u01b2\u01b6\u01bd\u01c1";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}