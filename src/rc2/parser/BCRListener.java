// Generated from BCR.g4 by ANTLR 4.2.2

package rc2.parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BCRParser}.
 */
public interface BCRListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BCRParser#newExpr}.
	 * @param ctx the parse tree
	 */
	void enterNewExpr(@NotNull BCRParser.NewExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#newExpr}.
	 * @param ctx the parse tree
	 */
	void exitNewExpr(@NotNull BCRParser.NewExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#returnCommand}.
	 * @param ctx the parse tree
	 */
	void enterReturnCommand(@NotNull BCRParser.ReturnCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#returnCommand}.
	 * @param ctx the parse tree
	 */
	void exitReturnCommand(@NotNull BCRParser.ReturnCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#continueCommand}.
	 * @param ctx the parse tree
	 */
	void enterContinueCommand(@NotNull BCRParser.ContinueCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#continueCommand}.
	 * @param ctx the parse tree
	 */
	void exitContinueCommand(@NotNull BCRParser.ContinueCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#xorExpr}.
	 * @param ctx the parse tree
	 */
	void enterXorExpr(@NotNull BCRParser.XorExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#xorExpr}.
	 * @param ctx the parse tree
	 */
	void exitXorExpr(@NotNull BCRParser.XorExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#unaryExpr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpr(@NotNull BCRParser.UnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#unaryExpr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpr(@NotNull BCRParser.UnaryExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#literalExpr}.
	 * @param ctx the parse tree
	 */
	void enterLiteralExpr(@NotNull BCRParser.LiteralExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#literalExpr}.
	 * @param ctx the parse tree
	 */
	void exitLiteralExpr(@NotNull BCRParser.LiteralExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#mulExpr}.
	 * @param ctx the parse tree
	 */
	void enterMulExpr(@NotNull BCRParser.MulExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#mulExpr}.
	 * @param ctx the parse tree
	 */
	void exitMulExpr(@NotNull BCRParser.MulExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#ifElseBody}.
	 * @param ctx the parse tree
	 */
	void enterIfElseBody(@NotNull BCRParser.IfElseBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#ifElseBody}.
	 * @param ctx the parse tree
	 */
	void exitIfElseBody(@NotNull BCRParser.IfElseBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(@NotNull BCRParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(@NotNull BCRParser.ExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#accessModes}.
	 * @param ctx the parse tree
	 */
	void enterAccessModes(@NotNull BCRParser.AccessModesContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#accessModes}.
	 * @param ctx the parse tree
	 */
	void exitAccessModes(@NotNull BCRParser.AccessModesContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#ifliteral}.
	 * @param ctx the parse tree
	 */
	void enterIfliteral(@NotNull BCRParser.IfliteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#ifliteral}.
	 * @param ctx the parse tree
	 */
	void exitIfliteral(@NotNull BCRParser.IfliteralContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#assignmentCommand}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentCommand(@NotNull BCRParser.AssignmentCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#assignmentCommand}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentCommand(@NotNull BCRParser.AssignmentCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#constantExpr}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpr(@NotNull BCRParser.ConstantExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#constantExpr}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpr(@NotNull BCRParser.ConstantExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#labelCommand}.
	 * @param ctx the parse tree
	 */
	void enterLabelCommand(@NotNull BCRParser.LabelCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#labelCommand}.
	 * @param ctx the parse tree
	 */
	void exitLabelCommand(@NotNull BCRParser.LabelCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#doWhileCommand}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileCommand(@NotNull BCRParser.DoWhileCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#doWhileCommand}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileCommand(@NotNull BCRParser.DoWhileCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#methodCallCommand}.
	 * @param ctx the parse tree
	 */
	void enterMethodCallCommand(@NotNull BCRParser.MethodCallCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#methodCallCommand}.
	 * @param ctx the parse tree
	 */
	void exitMethodCallCommand(@NotNull BCRParser.MethodCallCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#orExpr}.
	 * @param ctx the parse tree
	 */
	void enterOrExpr(@NotNull BCRParser.OrExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#orExpr}.
	 * @param ctx the parse tree
	 */
	void exitOrExpr(@NotNull BCRParser.OrExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#initializing}.
	 * @param ctx the parse tree
	 */
	void enterInitializing(@NotNull BCRParser.InitializingContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#initializing}.
	 * @param ctx the parse tree
	 */
	void exitInitializing(@NotNull BCRParser.InitializingContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#generic}.
	 * @param ctx the parse tree
	 */
	void enterGeneric(@NotNull BCRParser.GenericContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#generic}.
	 * @param ctx the parse tree
	 */
	void exitGeneric(@NotNull BCRParser.GenericContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCommand(@NotNull BCRParser.CommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCommand(@NotNull BCRParser.CommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#shiftExpr}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpr(@NotNull BCRParser.ShiftExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#shiftExpr}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpr(@NotNull BCRParser.ShiftExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#ifinstruction}.
	 * @param ctx the parse tree
	 */
	void enterIfinstruction(@NotNull BCRParser.IfinstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#ifinstruction}.
	 * @param ctx the parse tree
	 */
	void exitIfinstruction(@NotNull BCRParser.IfinstructionContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#breakCommand}.
	 * @param ctx the parse tree
	 */
	void enterBreakCommand(@NotNull BCRParser.BreakCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#breakCommand}.
	 * @param ctx the parse tree
	 */
	void exitBreakCommand(@NotNull BCRParser.BreakCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#whileCommand}.
	 * @param ctx the parse tree
	 */
	void enterWhileCommand(@NotNull BCRParser.WhileCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#whileCommand}.
	 * @param ctx the parse tree
	 */
	void exitWhileCommand(@NotNull BCRParser.WhileCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#addExpr}.
	 * @param ctx the parse tree
	 */
	void enterAddExpr(@NotNull BCRParser.AddExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#addExpr}.
	 * @param ctx the parse tree
	 */
	void exitAddExpr(@NotNull BCRParser.AddExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#simpleCommand}.
	 * @param ctx the parse tree
	 */
	void enterSimpleCommand(@NotNull BCRParser.SimpleCommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#simpleCommand}.
	 * @param ctx the parse tree
	 */
	void exitSimpleCommand(@NotNull BCRParser.SimpleCommandContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#typeInformation}.
	 * @param ctx the parse tree
	 */
	void enterTypeInformation(@NotNull BCRParser.TypeInformationContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#typeInformation}.
	 * @param ctx the parse tree
	 */
	void exitTypeInformation(@NotNull BCRParser.TypeInformationContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#ifconnection}.
	 * @param ctx the parse tree
	 */
	void enterIfconnection(@NotNull BCRParser.IfconnectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#ifconnection}.
	 * @param ctx the parse tree
	 */
	void exitIfconnection(@NotNull BCRParser.IfconnectionContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#complete}.
	 * @param ctx the parse tree
	 */
	void enterComplete(@NotNull BCRParser.CompleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#complete}.
	 * @param ctx the parse tree
	 */
	void exitComplete(@NotNull BCRParser.CompleteContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#functionHead}.
	 * @param ctx the parse tree
	 */
	void enterFunctionHead(@NotNull BCRParser.FunctionHeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#functionHead}.
	 * @param ctx the parse tree
	 */
	void exitFunctionHead(@NotNull BCRParser.FunctionHeadContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#ifcontent}.
	 * @param ctx the parse tree
	 */
	void enterIfcontent(@NotNull BCRParser.IfcontentContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#ifcontent}.
	 * @param ctx the parse tree
	 */
	void exitIfcontent(@NotNull BCRParser.IfcontentContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#ifsubcondition}.
	 * @param ctx the parse tree
	 */
	void enterIfsubcondition(@NotNull BCRParser.IfsubconditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#ifsubcondition}.
	 * @param ctx the parse tree
	 */
	void exitIfsubcondition(@NotNull BCRParser.IfsubconditionContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#andExpr}.
	 * @param ctx the parse tree
	 */
	void enterAndExpr(@NotNull BCRParser.AndExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#andExpr}.
	 * @param ctx the parse tree
	 */
	void exitAndExpr(@NotNull BCRParser.AndExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BCRParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void enterMethodCall(@NotNull BCRParser.MethodCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link BCRParser#methodCall}.
	 * @param ctx the parse tree
	 */
	void exitMethodCall(@NotNull BCRParser.MethodCallContext ctx);
}