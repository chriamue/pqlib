/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Hilmar
 */
public class PossibleInstance {

    public static final int possibleInstances = 18;
    private int id;

    public PossibleInstance (int _id) {
        id = _id;
    }

    public Object getInstance () {
        if (id == 0) {
            PSet set = new PSet();
            set.add(new Integer(0));
            set.add(new Integer(1));
            return set;
        } else if (id == 1) {
            int [] array = new int[] {0, 1};
            return array;
        } else if (id == 2) {
            long [] array = new long[] {0, 1};
            return array;
        } else if (id == 3) {
            float [] array = new float[] {0, 1};
            return array;
        } else if (id == 4) {
            double [] array = new double[] {0, 1};
            return array;
        } else if (id == 5) {
            short [] array = new short[] {0, 1};
            return array;
        } else if (id == 6) {
            char [] array = new char[] {0, 1};
            return array;
        } else if (id == 7) {
            boolean [] array = new boolean[] {false, true};
            return array;
        } else if (id == 8) {
            byte [] array = new byte[] {0, 1};
            return array;
        } else if (id == 9) {
            Object [] array = new Object[] {null, null};
            return array;
        } else if (id == 10) {
            Set set = new HashSet();
            set.add(new Integer(0));
            set.add(new Integer(1));
            return set;
        } else if (id == 11) {
            PMap map = new PMap();
            map.put(new Integer(0), new Integer(0));
            map.put(new Integer(1), new Integer(1));
            return map;
        } else if (id == 12) {
            Map map = new HashMap();
            map.put(new Integer(0), new Integer(0));
            map.put(new Integer(1), new Integer(1));
            return map;
        } else if (id == 13) {
            String str = new String ("");
            return str;
        } else if (id == 14) {
            AnyFieldOrMethodContainingObject obj = new AnyFieldOrMethodContainingObject();
            return obj;
        } else if (id == 15) {
            return new Integer(1);
        }  else if (id == 16) {
            return new Long(1);
        }  else if (id == 17) {
            return new Double(1);
        } else
            return null;
    }

    public Class getType () {
        if (id == 10)
            return Set.class;
        else if (id == 12)
            return Map.class;
        return getInstance().getClass();
    }

    public static int findMatchingInstance (Object obj) {
        for (int i=0; i<possibleInstances; i++)
            if ( (new PossibleInstance(i)).getType().isAssignableFrom( obj.getClass() ))
                return i;
        return possibleInstances;
    }

}
