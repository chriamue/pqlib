/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import edu.umass.pql.Env;
import edu.umass.pql.TableConstants;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rc2.SkeletonBuilder;
import rc2.bcr.BCRAccessMode;
import rc2.representation.PQLArgument;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class ArgumentList implements TableConstants, VarConstants {

    public enum OutputType {Complete, WithoutAccessMode};

    public int [] chosen;

    private static List <PossibleInstance> instanceChecks = ArgumentList.init();

    public ArgumentList (int [] _chosen) {
        chosen = _chosen;
    }

    public ArgumentList (PQLToken token, SkeletonBuilder skeletonBuilder, Env env) {
        chosen = new int[token.arguments.size()];
        try {
            BCRAccessMode accessMode = new BCRAccessMode(token);
            for (int i=0; i<chosen.length; i++) {
                if (accessMode.access[i] != BCRAccessMode.AccessMode.Wildcard) {
                    chosen[i] = manipulatePossibility(chosen[i], 1, 1);
                    chosen[i] = manipulatePossibility(chosen[i], 1, (accessMode.access[i] != BCRAccessMode.AccessMode.Read ? 1 : 0));
                    int type = 0;
                    PQLArgument pqlArgument = skeletonBuilder.findArgument(token.arguments.get(i));
                    switch (pqlArgument.type) {
                        case TYPE_INT:
                            type = 0;
                            break;
                        case TYPE_LONG:
                            type = 1;
                            break;
                        case TYPE_DOUBLE:
                            type = 2;
                            break;
                        case TYPE_OBJECT:
                            type = 3;
                            break;
                        default:
                            throw new RuntimeCreatorException("unknown type.");
                    }
                    chosen[i] = manipulatePossibility(chosen[i], 2, type);
                    if (type == 3) {
                        chosen[i] = manipulatePossibility(chosen[i], 3, (pqlArgument.isConst ? 1 : 0));
                        chosen[i] = manipulatePossibility(chosen[i], 4, PossibleInstance.findMatchingInstance(env.v_object[pqlArgument.index]));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPartOfArgumentList (ArgumentList context, boolean accessModeIndipendent, PQLToken token) {
        if (context.chosen.length != chosen.length)
            return false;
        for (int i=0; i<chosen.length; i++) {
            if (chosen[i] == 0 && context.chosen[i] == 0)
                continue;
            if (getKey(chosen[i], 1) != getKey(context.chosen[i], 1) && !accessModeIndipendent)
                return false;
            if (getKey(chosen[i], 2) != getKey(context.chosen[i], 2))
                return false;
            if (getKey(chosen[i], 3) != getKey(context.chosen[i], 3) && getKey(context.chosen[i], 3) != 2)
                return false;
            Object possibleInstance = new PossibleInstance(getKey(context.chosen[i], 4)).getInstance();
            if (getKey(chosen[i], 4) != getKey(context.chosen[i], 4) && getKey(context.chosen[i], 4) != instanceChecks.size()) {
                if (possibleInstance != null && possibleInstance instanceof AnyFieldOrMethodContainingObject && token.field != null && possibleInstance.getClass().isAssignableFrom(token.field.getClass())) {}
                else
                    return false;
            }
        }
        return true;
    }

    private String getName (int val, OutputType outputType) {
        if (val == 0)
            return "_ (Wildcard)";
        val--;
        boolean isRead = (val%2 == 0);
        val /= 2;
        if (val == 0)
            return "int" + (outputType == OutputType.Complete ? (isRead ? " (read)" : " (write)") : "");
        else if (val == 1)
            return "long" + (outputType == OutputType.Complete ? (isRead ? " (read)" : " (write)") : "");
        else if (val == 2)
            return "double" + (outputType == OutputType.Complete ? (isRead ? " (read)" : " (write)") : "");
        else {
            val -= 3;
            int constantType = (val%3);
            val /= 3;
            return "object '" + (val < instanceChecks.size() ? instanceChecks.get(val).getType().getName() : "any object") + (outputType == OutputType.Complete ? "' " + (isRead ? "(read, " : "(write, ") : "' (") + (constantType == 0 ? "constant)" : (constantType == 1 ? "not constant)" : "constant or not)"));
        }
    }

    public BCRAccessMode getAccessMode () {
        try {
            String accessModeStr = "";
            for (int i=0; i<chosen.length; i++) {
                if (chosen[i] == 0)
                    accessModeStr += "_";
                else if ((chosen[i]-1)%2 == 0)
                    accessModeStr += "r";
                else
                    accessModeStr += "w";
            }
            return new BCRAccessMode(accessModeStr);
        } catch (RuntimeCreatorException e) {
            return null;
        }
    }

    public Map <ArgumentList, List <ArgumentList> > possibleMerges () {
        Map <ArgumentList, List <ArgumentList> > returnMap = new HashMap <ArgumentList, List <ArgumentList> > ();
        for (int i=0; i<chosen.length; i++) {
            if (getKey(chosen[i], 2) == 3) {
                for (int j=0; j<2; j++) {
                    if ( (j == 0 && getKey(chosen[i], 3) == 2) || (j == 1 && getKey(chosen[i], 4) >= instanceChecks.size()))
                        continue;
                    ArgumentList [] copies = new ArgumentList [1 + (j == 0 ? 1 : instanceChecks.size()-1)];
                    for (int k=0; k<copies.length; k++) {
                        int [] chosenCopy = new int [chosen.length];
                        for (int l=0; l<chosen.length; l++)
                            chosenCopy[l] = chosen[l];
                        copies[k] = new ArgumentList(chosenCopy);
                    }

                    if (j == 0) {
                        copies[0].chosen[i] = manipulatePossibility(chosen[i], 3, 2);       //key 3 (constant or not)  => value 2 (both)
                        copies[1].chosen[i] = manipulatePossibility(chosen[i], 3, 1 - getKey(chosen[i], 3));
                    } else {
                        copies[0].chosen[i] = manipulatePossibility(chosen[i], 4, instanceChecks.size());       //key 4 (object-type)  => value ALL object-types
                        for (int k=0; k<copies.length-1; k++)
                            copies[k+1].chosen[i] = manipulatePossibility(chosen[i], 4, k + (k >= getKey(chosen[i], 4) ? 1 : 0));
                    }

                    List <ArgumentList> copiesNeeded = new ArrayList <ArgumentList> ();
                    for (int k=1; k<copies.length; k++)
                        copiesNeeded.add(copies[k]);
                    returnMap.put(copies[0], copiesNeeded);
                }
            }
        }
        return returnMap;
    }

    public boolean sameWithoutAccessMode (ArgumentList other) {
        if (other.chosen.length != chosen.length)
            return false;

        for (int i=0; i<chosen.length; i++) {
            if (chosen[i] == 0 || other.chosen[i] == 0)
                continue;
            if ( (chosen[i]-1)/2 == (other.chosen[i]-1)/2)
                continue;
            return false;
        }

        return true;
    }

    @Override
    public String toString () {
        return toString(OutputType.Complete);
    }

    public String toString (OutputType outputType) {
        String returnStr = "";
        int maxLength = -1;
        for (int i=0; i<getMaxPossibilities(); i++)
            if (getName(i, outputType).length() > maxLength)
                maxLength = getName(i, outputType).length();
        for (int i=0; i<chosen.length; i++) {
            String nextName = getName(chosen[i], outputType);
            returnStr += nextName;
            if ( i < chosen.length-1)
                for (int j=0; j<maxLength+3 - nextName.length(); j++)
                    returnStr += " ";
        }
        return returnStr;
    }
    
    public String toString (OutputType outputType, int index) {
        return getName(chosen[index], outputType);
    }



    private static List <PossibleInstance> init () {
        instanceChecks = new ArrayList <PossibleInstance> ();
        for (int i=0; i<PossibleInstance.possibleInstances; i++)
            instanceChecks.add(new PossibleInstance(i));
        return instanceChecks;
    }

    public static int getMaxPossibilities () {
        return 1 + //wildcard
                2 * (   //read and write
                   3 +  //int, long, double
                   3* (     //objects (constant / not constant / both)
                      instanceChecks.size()+1       //specific objects / ALL objects
                   )
                );
    }

    private static int getKey (int val, int index) {
        int [] keys = new int [] {(val > 0 ? 1 : 0), (val-1)%2, ((val-1)/2), 0, 0};
        if (keys[2] > 2) {
            keys[2] = 3;
            keys[3] = (((val-1)/2)-3)%3;
            keys[4] = (((val-1)/2)-3)/3;
        }
        return keys[index];
    }

    private static int manipulatePossibility (int val, int changedKey, int changedValue) {
        int [] keys = new int [] {getKey(val, 0), getKey(val, 1), getKey(val, 2), getKey(val, 3), getKey(val, 4)};
        keys[changedKey] = changedValue;
        return (keys[0] + keys[1] + 2*(keys[2] + keys[3] + 3*keys[4]));
    }

    private static int createReadOrWriteVar (boolean isRead, int type, int index) {
        if (isRead)
            return Env.encodeReadVar(type, index);
        else
            return Env.encodeWriteVar(type, index);
    }

    public static boolean isTestablePossibility (int val) {
        return !(getKey(val, 3) == 2 || getKey(val, 4) >= instanceChecks.size());
    }

    public static int createAndGetPossibility (Env env, Map <Integer, Class> instances, int val, int argumentIndex) {
        if (val == 0)
            return TestBase.__;
        val--;
        boolean isRead = (val%2 == 0);
        val /= 2;
        if (val == 0)
            return createReadOrWriteVar(isRead, TYPE_INT, 0);
        else if (val == 1)
            return createReadOrWriteVar(isRead, TYPE_LONG, 0);
        else if (val == 2)
            return createReadOrWriteVar(isRead, TYPE_DOUBLE, 0);
        else {
            val -= 3;
            boolean isConstant = (val%3 == 0);
            val /= 3;
            int objectIndex = argumentIndex*2+(isConstant ? 0 : 1);
            env.v_object[objectIndex] = instanceChecks.get(val).getInstance();
            if (isConstant)
                instances.put(objectIndex, instanceChecks.get(val).getType());
            return createReadOrWriteVar(isRead, TYPE_OBJECT, objectIndex);
        }
    }

    public String toCompressedString () {
        String returnStr = "";
        for (int i=0; i<chosen.length; i++)
            returnStr += "" + chosen[i] + (i < chosen.length-1 ? "/" : "");
        return returnStr;
    }

    public static ArgumentList fromCompressedString (String str) {
        String [] tokens = str.split("/");
        if (!str.contains("/"))
            tokens = new String [0];
        int [] values = new int [tokens.length];
        for (int i=0; i<tokens.length; i++)
            values[i] = Integer.parseInt(tokens[i]);
        return new ArgumentList(values);
    }

    @Override
    public boolean equals (Object other) {
        if (!(other instanceof ArgumentList) || ((ArgumentList)other).chosen.length != chosen.length)
            return false;
        for (int i=0; i<chosen.length; i++)
            if (((ArgumentList)other).chosen[i] != chosen[i])
                return false;
        return true;
    }

    @Override
    public int hashCode () {
        return 0;
    }

}
