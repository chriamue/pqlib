/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.il.Arithmetic;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hilmar
 */
public class SkeletonCommand {

    public enum SkeletonCommandType {Standard, Reductor, Field, MethodAdapter, SpecialTRUE, SpecialFALSE};

    public String name;
    public Class representant;
    public SkeletonCommandType type;
    public int arguments;

    public SkeletonCommand (String _name, Class _representant, SkeletonCommandType _type, int _arguments) {
        name = _name;
        representant = _representant;
        type = _type;
        arguments = _arguments;
    }

    public int getJoinAmount () {
        if (type == SkeletonCommandType.MethodAdapter)
            return 2;
        return 1;
    }

    public Object createJoin (int [] args, int index) {
        try {
            if (type == SkeletonCommandType.Standard || type == SkeletonCommandType.Reductor) {
                if (arguments == 0)
                    return representant.getConstructor().newInstance();
                else if (arguments == 1)
                    return representant.getConstructor(int.class).newInstance(args[0]);
                else if (arguments == 2)
                    return representant.getConstructor(int.class, int.class).newInstance(args[0], args[1]);
                else if (arguments == 3)
                    return representant.getConstructor(int.class, int.class, int.class).newInstance(args[0], args[1], args[2]);
            } else if (type == SkeletonCommandType.MethodAdapter) {
                if (index == 0)
                    return PQLFactory.Reductors.METHOD_ADAPTER(AnyFieldOrMethodContainingObject.class.getDeclaredMethod("methodObj", Object.class, Object.class), args[0], args[1]);
                else
                    return PQLFactory.Reductors.METHOD_ADAPTER(AnyFieldOrMethodContainingObject.class.getDeclaredMethod("methodInt", int.class, int.class), args[0], args[1]);
            } else if (type == SkeletonCommandType.Field)
                return representant.getConstructor(Object.class, int.class, int.class).newInstance(AnyFieldOrMethodContainingObject.class.getDeclaredField("field"), args[0], args[1]);
            else if (type == SkeletonCommandType.SpecialTRUE)
                return Arithmetic.TRUE;
            else if (type == SkeletonCommandType.SpecialFALSE)
                return Arithmetic.FALSE;
            throw new Exception("amount of arguments not supported.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isReductor() {
        return (type == SkeletonCommandType.Reductor || type == SkeletonCommandType.MethodAdapter);
    }

}
