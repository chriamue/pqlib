/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import benchmarks.bonus.Bonus;
import benchmarks.bonus.Department;
import benchmarks.bonus.Employee;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.Arithmetic;
import edu.umass.pql.il.Comparison;
import edu.umass.pql.il.Container;
import edu.umass.pql.il.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import rc2.RCInterface;
import rc2.RCInterface.ParallelMode;
import rc2.SubCommands;
import rc2.evaluator.SkeletonCommand.SkeletonCommandType;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class EvaluatorBuilder implements TableConstants, VarConstants {

    private List <SkeletonCommand> commands;

    public EvaluatorBuilder () {
        commands = new ArrayList <SkeletonCommand> ();
        commands.add(new SkeletonCommand("LOOKUP", Container.LOOKUP.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("CONTAINS", Container.CONTAINS.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Int", Container.ARRAY_LOOKUP_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Long", Container.ARRAY_LOOKUP_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Short", Container.ARRAY_LOOKUP_Short.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Char", Container.ARRAY_LOOKUP_Char.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Boolean", Container.ARRAY_LOOKUP_Boolean.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Byte", Container.ARRAY_LOOKUP_Byte.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Float", Container.ARRAY_LOOKUP_Float.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Double", Container.ARRAY_LOOKUP_Double.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ARRAY_LOOKUP_Object", Container.ARRAY_LOOKUP_Object.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("POLY_LOOKUP", Container.POLY_LOOKUP.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("INT_RANGE_CONTAINS", Container.INT_RANGE_CONTAINS.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("LONG_RANGE_CONTAINS", Container.LONG_RANGE_CONTAINS.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("SET_SIZE", Container.SET_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("MAP_SIZE", Container.MAP_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("INT_ARRAY_SIZE", Container.INT_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LONG_ARRAY_SIZE", Container.LONG_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("SHORT_ARRAY_SIZE", Container.SHORT_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("CHAR_ARRAY_SIZE", Container.CHAR_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("BOOLEAN_ARRAY_SIZE", Container.BOOLEAN_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("BYTE_ARRAY_SIZE", Container.BYTE_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("FLOAT_ARRAY_SIZE", Container.FLOAT_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("DOUBLE_ARRAY_SIZE", Container.DOUBLE_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("OBJECT_ARRAY_SIZE", Container.OBJECT_ARRAY_SIZE.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("POLY_SIZE", Container.POLY_SIZE.class, SkeletonCommandType.Standard, 2));

        commands.add(new SkeletonCommand("EQ_Int", Comparison.EQ_Int.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("EQ_Long", Comparison.EQ_Long.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("EQ_Double", Comparison.EQ_Double.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("EQ_Object", Comparison.EQ_Object.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("EQ_String", Comparison.EQ_String.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEQ_Int", Comparison.NEQ_Int.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEQ_Long", Comparison.NEQ_Long.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEQ_Double", Comparison.NEQ_Double.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEQ_Object", Comparison.NEQ_Object.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEQ_String", Comparison.NEQ_String.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LT_Int", Comparison.LT_Int.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LT_Long", Comparison.LT_Long.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LT_Double", Comparison.LT_Double.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LTE_Int", Comparison.LTE_Int.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LTE_Long", Comparison.LTE_Long.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("LTE_Double", Comparison.LTE_Double.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("ADD_Int", Arithmetic.ADD_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ADD_Long", Arithmetic.ADD_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("ADD_Double", Arithmetic.ADD_Double.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("SUB_Int", Arithmetic.SUB_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("SUB_Long", Arithmetic.SUB_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("SUB_Double", Arithmetic.SUB_Double.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("MUL_Int", Arithmetic.MUL_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("MUL_Long", Arithmetic.MUL_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("MUL_Double", Arithmetic.MUL_Double.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("DIV_Int", Arithmetic.DIV_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("DIV_Long", Arithmetic.DIV_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("DIV_Double", Arithmetic.DIV_Double.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("MOD_Int", Arithmetic.MOD_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("MOD_Long", Arithmetic.MOD_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("NEG_Int", Arithmetic.NEG_Int.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEG_Long", Arithmetic.NEG_Long.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("NEG_Double", Arithmetic.NEG_Double.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("BITINV_Int", Arithmetic.BITINV_Int.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("BITINV_Long", Arithmetic.BITINV_Long.class, SkeletonCommandType.Standard, 2));
        commands.add(new SkeletonCommand("BITOR_Int", Arithmetic.BITOR_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITOR_Long", Arithmetic.BITOR_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITAND_Int", Arithmetic.BITAND_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITAND_Long", Arithmetic.BITAND_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITXOR_Int", Arithmetic.BITXOR_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITXOR_Long", Arithmetic.BITXOR_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITSHL_Int", Arithmetic.BITSHL_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITSHL_Long", Arithmetic.BITSHL_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITSHR_Int", Arithmetic.BITSHR_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITSHR_Long", Arithmetic.BITSHR_Long.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITSSHR_Int", Arithmetic.BITSSHR_Int.class, SkeletonCommandType.Standard, 3));
        commands.add(new SkeletonCommand("BITSSHR_Long", Arithmetic.BITSSHR_Long.class, SkeletonCommandType.Standard, 3));

        commands.add(new SkeletonCommand("TRUE", null, SkeletonCommandType.SpecialTRUE, 0));
        commands.add(new SkeletonCommand("FALSE", null, SkeletonCommandType.SpecialFALSE, 0));
        commands.add(new SkeletonCommand("FIELD", Field.class, SkeletonCommandType.Field, 2));

        commands.add(new SkeletonCommand("REDUCTOR_SET", edu.umass.pql.il.reductor.SetReductor.class, SkeletonCommandType.Reductor, 2));
        commands.add(new SkeletonCommand("REDUCTOR_MAP", edu.umass.pql.il.reductor.MapReductor.class, SkeletonCommandType.Reductor, 3));
        commands.add(new SkeletonCommand("REDUCTOR_METHOD_ADAPTER", edu.umass.pql.il.reductor.MethodAdapterReductor.class, SkeletonCommandType.MethodAdapter, 2));
    }

    public PQLEvaluatedCommand findEvaluatedCommand (PQLToken token, Evaluator evaluator) {
        for (SkeletonCommand cmd : commands) {
            Object join = cmd.createJoin(new int [] {0,0,0,0,0,0}, 0);
            if (join instanceof Join) {
                int flags = ((Join)join).accept(new JoinFlagsVisitor());
                if (flags == (token.instruction | token.type | token.additionalOptions)) {
                    for (PQLEvaluatedCommand evCmd : evaluator.commands)
                        if (evCmd.name.equals(cmd.name))
                            return evCmd;
                }
            }
        }
        return null;
    }

    private void run(Evaluator evaluator) throws RuntimeCreatorException {
        long startTime = System.currentTimeMillis()/1000;
        for (SkeletonCommand command : commands) {
            System.out.println("\nStarted: " + command.name + " (" + (System.currentTimeMillis()/1000 - startTime) + ")");
            int perCent = -1;
            int [] progress = new int [command.arguments];
            PQLEvaluatedCommand evCommand = new PQLEvaluatedCommand(command.name);
            while (true) {

                int newPerCent = 0;
                for (int i=0; i<progress.length; i++) {
                    newPerCent *= ArgumentList.getMaxPossibilities();
                    newPerCent += progress[progress.length-1 - i];
                }
                newPerCent = (int)(newPerCent / Math.pow(ArgumentList.getMaxPossibilities(), progress.length) * 100);
                if (Math.abs(newPerCent - perCent) >= 5) {
                    perCent = newPerCent;
                    System.out.print(perCent + "%  ..  ");
                }

                if (buildTest(command, progress)) {
                    int [] progressCopy = new int [progress.length];
                    for (int i=0; i<progressCopy.length; i++)
                        progressCopy[i] = progress[i];
                    evCommand.possibilities.add(new ArgumentList(progressCopy));
                }

                boolean finished = false;
                if (progress.length > 0) {
                    progress[0]++;
                    while (!ArgumentList.isTestablePossibility(progress[0]) && progress[0] < ArgumentList.getMaxPossibilities())
                        progress[0]++;
                    for (int j=0; j<progress.length; j++) {
                        if (progress[j] >= ArgumentList.getMaxPossibilities()-1) {
                            progress[j] = 0;
                            if (j < progress.length-1) {
                                progress[j+1] ++;
                                while (!ArgumentList.isTestablePossibility(progress[j+1]) && progress[j+1] < ArgumentList.getMaxPossibilities())
                                    progress[j+1]++;
                            } else
                                finished = true;
                        }
                    }
                } else
                    finished = true;
                if (finished)
                    break;
            }
            evaluator.commands.add(evCommand);
        }
        System.out.println("\n");
    }

    private boolean buildTest (SkeletonCommand command, int [] progress) throws RuntimeCreatorException {

        for (int i=0; i<command.getJoinAmount(); i++) {

            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

            RCInterface rc = new RCInterface();
            Env env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                    new Object[] {
                            new int[1],
                            new long[1],
                            new double[1],
                            new Object[progress.length*2]
                    });

            for (int j=0; j<progress.length*2; j++)
                env.v_object[j] = null;
            env.v_int[0] = 1;
            env.v_double[0] = 1;
            env.v_long[0] = 1;

            Map <Integer, Class> instances = new HashMap <Integer, Class> ();
            int [] arguments = new int [progress.length];
            for (int j=0; j<progress.length; j++)
                arguments[j] = ArgumentList.createAndGetPossibility(env, instances, progress[j], j);
            List <Join> joins = new ArrayList <Join> ();
            for (int j=0; j<progress.length; j++)
                joins.add(PQLFactory.EQ_Object(Env.encodeReadVar(TYPE_OBJECT, j*2+1), Env.encodeWriteVar(TYPE_OBJECT, j*2+1)));

            Join p = null;
            if (!command.isReductor()) {
                joins.add(0, (Join)command.createJoin(arguments, i));
                p = PQLFactory.ConjunctiveBlock(joins);
            } else {
                Join [] joinsArray = new Join [joins.size()];
                for (int j=0; j<joins.size(); j++)
                    joinsArray[j] = joins.get(j);
                p = PQLFactory.Reduction((edu.umass.pql.Reductor)(command.createJoin(arguments, i)), joinsArray);
            }

            boolean result = false;
            try {
                rc.build(p, env, instances, ParallelMode.Serial, RCInterface.NO_ERROR_FILTERING);
                result = rc.exec(env);
                result = true;
            } catch (Throwable t) {
                result = false;
            }
            if (result)
                return true;
        }

        return false;
    }

    public static void main (String [] args) {
        Evaluator evaluator = new Evaluator();
        try {
            new EvaluatorBuilder().run(evaluator);
            evaluator.save(Evaluator.standardEvaluatorPath);
            //evaluator.load(Evaluator.standardEvaluatorPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(evaluator.toString(PQLEvaluatedCommand.OutputType.Sorted));
    }

}
