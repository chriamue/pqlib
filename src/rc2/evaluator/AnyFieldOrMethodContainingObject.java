/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import edu.umass.pql.DefaultValueDouble;
import edu.umass.pql.DefaultValueInt;

/**
 *
 * @author Hilmar
 */
public class AnyFieldOrMethodContainingObject {

    public int field;

    @DefaultValueInt(0)
    public static int methodInt (int l, int r) {
        return l;
    }

    public static Object methodObj (Object l, Object r) {
        return l;
    }

    public AnyFieldOrMethodContainingObject ()  {
        field = 0;
    }

}
