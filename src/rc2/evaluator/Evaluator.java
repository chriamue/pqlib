/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import edu.umass.pql.Env;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import rc2.SkeletonBuilder;
import rc2.bcr.BCREnvVar;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class Evaluator {

    protected final static String standardEvaluatorPath = "data/evaluator/evaluatorResults.txt";
    protected static Evaluator standardEvaluator = null;

    protected List <PQLEvaluatedCommand> commands;

    public Evaluator () {
        commands = new ArrayList <PQLEvaluatedCommand> ();
    }

    public void save (String fileName) throws FileNotFoundException {
        PrintWriter outputFile = new PrintWriter(new File(fileName));
        for (int i=0; i<commands.size(); i++) {
            PQLEvaluatedCommand cmd = commands.get(i);
            cmd.sort();
            outputFile.write(cmd.toCompressedString() + "\n");
        }
        outputFile.close();
    }

    public void load (String fileName) throws UnsupportedEncodingException, FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
        while (true) {
            String nextLine = in.readLine();
            if (nextLine == null)
                break;
            commands.add(PQLEvaluatedCommand.fromCompressedString(nextLine));
        }
        in.close();
    }

    public static Evaluator getStandardEvaluator () throws Exception {
        if (standardEvaluator == null) {
            standardEvaluator = new Evaluator ();
            standardEvaluator.load(standardEvaluatorPath);
        }
        return standardEvaluator;
    }

    public static void filterError (Throwable t, PQLToken code, SkeletonBuilder originSkeletonBuilder, SkeletonBuilder skeletonBuilder, Env env) {

        if (t instanceof InvocationTargetException)
            t = t.getCause();

        String fallBackErrorMsg = null;
        EvaluatorBuilder builder = new EvaluatorBuilder();
        if (code == null)
            fallBackErrorMsg = "could not create a PQL-Token from the given join.";
        else {
            if (standardEvaluator == null) {
                try {
                    getStandardEvaluator();
                } catch (Exception e) {
                    fallBackErrorMsg = "could not load '" + standardEvaluatorPath + "'.";
                }
            }
            if (fallBackErrorMsg == null) {
                List <PQLToken> childs = code.getAllChildsInOrder();
                PQLToken involvedToken = null;
                try {
                    for (StackTraceElement ste : t.getStackTrace()) {
                        int lineNumber = ste.getLineNumber();
                        if (ste.getClassName().equals("Evaluator") && lineNumber >= 1) {
                            for (PQLToken child : childs)
                                if (child.getInternalId() == lineNumber-1-2) //-1, because we add 1 to the id of the token    -2 because we build a structure around the pure code (which reserves token-id 0 and 1)
                                    involvedToken = child;
                        }
                    }

                    if (involvedToken == null && skeletonBuilder.currentBuildingToken != null) {
                        for (PQLToken child : childs) {
                            if (child.getInternalId() == skeletonBuilder.currentBuildingToken.getInternalId()-2) {
                                involvedToken = child;
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (involvedToken == null)
                    System.out.println("There was an error, but it is not clear which specific token is responsible for this error.");
                else {
                    System.out.println("The input was:\n================================================\n\n" + code.toString() + "\n");
                    try {
                        System.out.println("The error occured in token [" + involvedToken.getInternalId() + "]\n");
                    } catch (RuntimeCreatorException e) {
                        e.printStackTrace();
                    }

                    PQLEvaluatedCommand cmd = builder.findEvaluatedCommand(involvedToken, standardEvaluator);
                    if (cmd != null) {
                        ArgumentList argList = new ArgumentList(involvedToken, originSkeletonBuilder, env);
                        if (!cmd.matchesToken(argList, involvedToken)) {
                            System.out.println("It seems there is an error in the structure of the input of this specific token. The input was:\n===================================\n\n" + argList.toString() + "\n");
                            System.out.println("All possible input-types of this token are:\n" + cmd.toString() + "\n");
                            fallBackErrorMsg = "";
                        }
                    }
                }
            }
        }

        if (fallBackErrorMsg == null)
            fallBackErrorMsg = "Could not find an error in the structure of the input-join. (If you read from an object int the error-throwing token and this is written earlier, maybe this object is from the false type?)";
        System.out.println(fallBackErrorMsg);
        System.out.println("\nOrigin exception was:\n=========");
        System.out.println(t.toString());
        for (StackTraceElement ste : t.getStackTrace())
            System.out.println(ste);
    }

    @Override
    public String toString () {
        return toString(PQLEvaluatedCommand.OutputType.SortedAndCrossproducts);
    }

    public String toString (PQLEvaluatedCommand.OutputType outputType) {
        String returnStr = "";
        int longestName = -1;
        for (PQLEvaluatedCommand cmd : commands)
            if (cmd.name.length() > longestName)
                longestName = cmd.name.length();

        for (int i=0; i<commands.size(); i++)
            returnStr += commands.get(i).toString(longestName + 3, outputType) + (i == commands.size()-1 ? "" : "\n\n");
        return returnStr;
    }

}
