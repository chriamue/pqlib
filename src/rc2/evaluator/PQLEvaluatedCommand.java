/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2.evaluator;

import edu.umass.pql.Env;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import rc2.SkeletonBuilder;
import rc2.bcr.BCRAccessMode;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class PQLEvaluatedCommand {

    public enum OutputType {Complete, Sorted, SortedAndCrossproducts};

    public String name;
    public List <ArgumentList> possibilities;

    private List <ArgumentList> sortedPossibilities;
    private List <ArgumentList> unsortedRemain;
    private Set <BCRAccessMode> accessModes;

    public PQLEvaluatedCommand (String _name) {
        name = _name;
        possibilities = new ArrayList <ArgumentList> ();
        sortedPossibilities = null;
    }

    public void sort () {
        if (sortedPossibilities != null)
            return;

        accessModes = new HashSet <BCRAccessMode> ();
        sortedPossibilities = new ArrayList <ArgumentList> ();
        unsortedRemain = new ArrayList <ArgumentList> ();

        for (ArgumentList possibility : possibilities)
            accessModes.add(possibility.getAccessMode());

        if (accessModes.size() > 0) {
            BCRAccessMode [] accessModesList = accessModes.toArray(new BCRAccessMode[accessModes.size()]);
            for (ArgumentList possibility1 : possibilities) {
                if (possibility1.getAccessMode().equals(accessModesList[0])) {
                    boolean [] foundMatch = new boolean [accessModesList.length-1];
                    for (ArgumentList possibility2 : possibilities) {
                        if (possibility1.sameWithoutAccessMode(possibility2)) {
                            for (int i=1; i<accessModesList.length; i++) {
                                if (accessModesList[i].equals(possibility2.getAccessMode())) {
                                    foundMatch[i-1] = true;
                                    break;
                                }
                            }
                        }
                    }
                    boolean foundMatchAllAccessModes = true;
                    for (int i=0; i<foundMatch.length; i++)
                        if (!foundMatch[i])
                            foundMatchAllAccessModes = false;
                    if (foundMatchAllAccessModes)
                        sortedPossibilities.add(possibility1);
                }
            }
        }

        for (ArgumentList possibility1 : possibilities) {
            boolean foundInSortedList = false;
            for (ArgumentList possibility2 : sortedPossibilities)
                if (possibility1.sameWithoutAccessMode(possibility2)) {
                    foundInSortedList = true;
                    break;
                }

            if (!foundInSortedList)
                unsortedRemain.add(possibility1);
        }

        for (int i=0; i<2; i++) {
            for (int j=0; j<(i == 0 ? sortedPossibilities.size() : unsortedRemain.size()); j++ ) {
                ArgumentList possibility = (i == 0 ? sortedPossibilities.get(j) : unsortedRemain.get(j));
                Map <ArgumentList, List <ArgumentList> > merges = possibility.possibleMerges();
                boolean mergedSomething = false;
                for (ArgumentList key : merges.keySet()) {
                    Set <ArgumentList> mergeIndices = new HashSet <ArgumentList> ();
                    for (int k=0; k<merges.get(key).size(); k++) {
                        boolean foundMergeIndex = false;
                        for (int l=0; l<(i == 0 ? sortedPossibilities.size() : unsortedRemain.size()); l++ ) {
                            if ((i == 0 ? sortedPossibilities.get(l) : unsortedRemain.get(l)).equals(merges.get(key).get(k))) {
                                mergeIndices.add(merges.get(key).get(k));
                                foundMergeIndex = true;
                                break;
                            }
                        }
                        if (!foundMergeIndex) {
                            mergeIndices = null;
                            break;
                        }
                    }
                    if (mergeIndices != null) {
                        mergeIndices.add(possibility);
                        (i == 0 ? sortedPossibilities : unsortedRemain).removeAll(mergeIndices);
                        (i == 0 ? sortedPossibilities : unsortedRemain).add(key);
                        mergedSomething = true;
                        break;
                    }
                }
                if (mergedSomething)
                    j = -1;
            }
        }
    }

    public boolean matchesToken (ArgumentList inputArgList, PQLToken token) {
        sort();
        for (int i=0; i<2; i++) {
            for (ArgumentList argList : (i == 0 ? sortedPossibilities : unsortedRemain)) {
                if (inputArgList.isPartOfArgumentList(argList, (i == 0), token))
                    return true;
            }
        }
        return false;
    }
    
    private void addLine (String content, StringBuilder builder, int shiftingLength) {
        int lengthAlready = 0;
        if (builder.length() == 0) {
            builder.append(name);
            lengthAlready = name.length();
        }
        for (int j=lengthAlready; j<shiftingLength; j++)
            builder.append(" ");
        builder.append(content + "\n");
    }
    
    private boolean isCrossproduct (List < List <String> > tryArguments, List < List <String> > allEntries) {
        System.out.println("try: ");
        for (List <String> abc : tryArguments) {
            for (String a : abc)
                System.out.print(a + " / ");
            System.out.println("");
        }
        int [] progress = new int [tryArguments.size()];
        while (true) {   
            boolean foundMatch = false;
            for (int i=0; i<allEntries.get(0).size(); i++) {
                foundMatch = true;
                for (int j=0; j<progress.length; j++)
                    if (!allEntries.get(j).get(i).equals(tryArguments.get(j).get(progress[j]))) {
                        foundMatch = false;
                        break;
                    }
                if (foundMatch)
                    break;
            }
            if (!foundMatch)
                return false;
            
            boolean atEnd = false;
            for (int i=0; i<progress.length; i++) {
                progress[i]++;
                if (progress[i] < tryArguments.get(i).size())
                    break;
                progress[i] = 0;
                if (i == progress.length-1)
                    atEnd = true;
            }
            if (atEnd)
                break;
        }
        return true;
    }
    
    private boolean tryFindCrossproduct (List <ArgumentList> entries, ArgumentList.OutputType outputType, StringBuilder builder, int shiftingLength) {
        if (entries.size() == 0)
            return false;
        
        List < List <String> > entriesStr = new ArrayList < List <String> > ();
        List < List <String> > allArguments = new ArrayList < List <String> > ();
        List < List <String> > tryArguments = new ArrayList < List <String> > ();
        for (int i=0; i<entries.get(0).chosen.length; i++) {
            entriesStr.add(new ArrayList <String> ());
            allArguments.add(new ArrayList <String> ());
            tryArguments.add(new ArrayList <String> ());
            for (ArgumentList argList : entries) {
                String entryName = argList.toString(outputType, i);
                entriesStr.get(i).add(entryName);
                if (!allArguments.get(i).contains(entryName)) {
                    allArguments.get(i).add(entryName);
                    tryArguments.get(i).add(entryName);
                }
            }
        }
        if (isCrossproduct(tryArguments, entriesStr)) {
            List < Set <String> > crossproduct = new ArrayList < Set <String> > ();
            for (List <String> tryArgument : tryArguments) {
                crossproduct.add(new HashSet <String> ());
                for (String arg : tryArgument)
                    crossproduct.get(crossproduct.size()-1).add(arg);
            }
            int [] setReference = new int [crossproduct.size()];
            Map <Integer, String> indexToName = new HashMap <Integer, String> ();
            int indexName = 0;
            for (int i=0; i<setReference.length; i++) {
                if (setReference[i] == 0) {
                    setReference[i] = (i+1);
                    indexToName.put((i+1), ("" + (char)( (indexName++) +65)));
                    for (int j=i+1; j<setReference.length; j++)
                        if (crossproduct.get(i).equals(crossproduct.get(j)))
                            setReference[j] = (i+1);
                }
            }
            String title = "input in the form of ";
            for (int i=0; i<setReference.length; i++)
                title += "'" + indexToName.get(setReference[i]) + "'" + (i < setReference.length-1 ? " X " : ":");
            addLine(title, builder, shiftingLength);
            for (int i=0; i<indexToName.values().size(); i++) {
                String searchedName = ("" + (char)( (i) +65));
                addLine("    '" + searchedName + "' is one of the following values:", builder, shiftingLength);
                for (Integer key : indexToName.keySet()) {
                    if (indexToName.get(key).equals(searchedName)) {
                        for (String possibility : crossproduct.get(key-1))
                            addLine("        " + possibility, builder, shiftingLength);
                        break;
                    }
                }
            }            
            while (entries.size() > 0)
                entries.remove(0);
            return true;
        } else     
            return false;
    }

    @Override
    public String toString () {
        return toString(name.length() + 3, OutputType.SortedAndCrossproducts);
    }

    public String toString (int shiftingLength, OutputType outputType) {
        if (outputType == OutputType.Sorted)
            sort();

        StringBuilder strBuilder = new StringBuilder();
        
        addLine("", strBuilder, shiftingLength);
        if (outputType == OutputType.Complete) {
            for (int i=0; i<possibilities.size(); i++)
                addLine(possibilities.get(i).toString(), strBuilder, shiftingLength);
        } else {
            if (sortedPossibilities.size() > 0) {
                String title = "Available on all access-modes (";
                BCRAccessMode [] accessModesList = accessModes.toArray(new BCRAccessMode[accessModes.size()]);
                for (int i=0; i<accessModesList.length; i++)
                    title += accessModesList[i].toString() + (i < accessModesList.length-1 ? ", " : "");
                title += ")";
                addLine(title, strBuilder, shiftingLength);
                addLine("=======================================================", strBuilder, shiftingLength);
                
                List <ArgumentList> backupSortedPossibilites = new ArrayList <ArgumentList> ();
                if (outputType == OutputType.SortedAndCrossproducts) {                    
                    for (ArgumentList argList : sortedPossibilities)
                        backupSortedPossibilites.add(argList);
                    while (tryFindCrossproduct(sortedPossibilities, ArgumentList.OutputType.WithoutAccessMode, strBuilder, shiftingLength)) {}
                }                
                for (int i=0; i<sortedPossibilities.size(); i++)
                    addLine(sortedPossibilities.get(i).toString(ArgumentList.OutputType.WithoutAccessMode), strBuilder, shiftingLength);
                
                if (outputType == OutputType.SortedAndCrossproducts)
                    sortedPossibilities = backupSortedPossibilites;
            }
            if (unsortedRemain.size() > 0) {
                addLine("Other types of available access methods", strBuilder, shiftingLength);
                addLine("=======================================================", strBuilder, shiftingLength);
                for (int i=0; i<unsortedRemain.size(); i++)
                    addLine(unsortedRemain.get(i).toString(ArgumentList.OutputType.Complete), strBuilder, shiftingLength);
            }
        }
        return strBuilder.toString();
    }

    public String toCompressedString () {
        String returnStr = name + " " + possibilities.size() + " " + sortedPossibilities.size() + " " + unsortedRemain.size() + " ";
        for (ArgumentList arg : possibilities)
            returnStr += arg.toCompressedString() + " ";
        for (ArgumentList arg : sortedPossibilities)
            returnStr += arg.toCompressedString() + " ";
        for (ArgumentList arg : unsortedRemain)
            returnStr += arg.toCompressedString() + " ";
        BCRAccessMode [] accessModesList = accessModes.toArray(new BCRAccessMode[accessModes.size()]);
        for (int i=0; i<accessModesList.length; i++)
            returnStr += accessModesList[i].toString() + " ";
        return returnStr.substring(0, returnStr.length()-1);
    }

    public static PQLEvaluatedCommand fromCompressedString (String str) {
        String [] tokens = str.split(" ");
        PQLEvaluatedCommand returnCommand = new PQLEvaluatedCommand (tokens[0]);
        int possibilitiesSize = Integer.parseInt(tokens[1]);
        int sortedPossibilitiesSize = Integer.parseInt(tokens[2]);
        int unsortedRemainSize = Integer.parseInt(tokens[3]);
        returnCommand.sortedPossibilities = new ArrayList <ArgumentList> ();
        returnCommand.unsortedRemain = new ArrayList <ArgumentList> ();
        returnCommand.accessModes = new HashSet <BCRAccessMode> ();
        for (int i=0; i<possibilitiesSize; i++)
            returnCommand.possibilities.add(ArgumentList.fromCompressedString( (i+4 >= tokens.length ? "" : tokens[i+4])));
        for (int i=0; i<sortedPossibilitiesSize; i++)
            returnCommand.sortedPossibilities.add(ArgumentList.fromCompressedString( (i+4+possibilitiesSize >= tokens.length ? "" : tokens[i+4+possibilitiesSize])));
        for (int i=0; i<unsortedRemainSize; i++)
            returnCommand.unsortedRemain.add(ArgumentList.fromCompressedString( (i+4+possibilitiesSize+sortedPossibilitiesSize >= tokens.length ? "" : tokens[i+4+possibilitiesSize+sortedPossibilitiesSize])));
        try {
            for (int i=4+possibilitiesSize+sortedPossibilitiesSize+unsortedRemainSize; i<tokens.length; i++)
                returnCommand.accessModes.add(new BCRAccessMode(tokens[i]));
        } catch (RuntimeCreatorException e) {
            e.printStackTrace();
        }
        return returnCommand;
    }

}
