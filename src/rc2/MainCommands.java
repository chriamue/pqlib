/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2;

import edu.umass.bc.BcFlags;
import edu.umass.pql.Env;
import edu.umass.pql.VarConstants;
import java.util.Map;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import rc2.FlagUtils;
import rc2.representation.PQLToken;
import rc2.SkeletonBuilder;
import rc2.SubCommands;
import java.util.HashMap;
import rc2.bcr.OperatorInfo;
import rc2.representation.AdditionalInfo;
import rc2.representation.RuntimeCreatorException;
import rc2.ExecGenSourcecode;
import rc2.bcr.BCRExtension;
import rc2.bcr.BCRAccessMode;

/**
 *
 * @author Hilmar
 */
public class MainCommands implements BcFlags, VarConstants, Opcodes {

    public static Void buildToken (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
       switch(token.instruction) {
            case TYPE_LOOKUP:
            case TYPE_CONTAINS:
            case TYPE_ARRAY_LOOKUP:
                return buildTokenDataStructure(mv, skeletonBuilder, token, translateStep, typeInformations, env);

            case TYPE_POLY_LOOKUP:
                return buildTokenPolyLookup(mv, skeletonBuilder, token, translateStep, typeInformations, env);

            // ### BytecodeCreatorRoutine => Additional Extensions Case
            case -1:
                return buildTokenExtension(mv, skeletonBuilder, token, translateStep, typeInformations, env);

            default:
                return buildTokenElse(mv, skeletonBuilder, token, translateStep, typeInformations, env);
        }
    }

    // ### BytecodeCreatorRoutine => StartParsing
    private static Void buildTokenDataStructure (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        switch(token.instruction) {

            case TYPE_LOOKUP:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import lookup
                 */

            case TYPE_CONTAINS:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import contains
                */

            case TYPE_ARRAY_LOOKUP:
            {
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_type int long short char boolean byte float double object
                arraylookup(obj, key, value)
                    :rrr rwr rrw r_w rw_ rr_ r_r

                => local init
                ifconst obj
                    => global init

                ifparallelmode serial
                    size = obj.length{{gen_type}}
                else
                    threadIndex = __thread_index
                    threadAmount = __thread_amount
                    step = obj.length{object}
                    step = step /{int} threadAmount
                    parallelStart = threadIndex *{int} step
                    parallelEnd = parallelStart +{int} step
                    threadIndex = threadIndex + 1
                    if threadIndex == threadAmount
                        parallelEnd = obj.length{object}

                    size = parallelEnd

                => local init

                ifmode rrr rrw rr_
                    if key >= size
                        return false

                    tmpValue = obj.[]{{gen_type}} key
                    ifmode rr_
                        return true
                    ifmode rrr
                        if tmpValue == value
                            return true
                    ifmode rrw
                        value = tmpValue
                        return true
                    return false
                else
                    ifparallelmode serial
                        position = 0
                    else
                        position = parallelStart

                    => iteration
                    startIteration:

                    if position >= size
                        return false

                    ifmode rwr r_r
                        elem = obj.[]{{gen_type}} position
                        if elem == value
                            ifmode rwr
                                key = position
                            position = position + 1
                            return true
                        goto skipElement

                    ifmode r_w
                        value = obj.[]{{gen_type}} position
                    ifmode rw_
                        key = position

                    position = position + 1
                    return true

                    skipElement:
                        position = position + 1
                        goto startIteration
                 */
            }
        }
        return null;
    }

    private static Void buildTokenPolyLookup (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        switch(token.instruction) {

            case TYPE_POLY_LOOKUP:
            {
                // // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                polylookup(obj, key, value)
                    :rrr rwr rrw r_w rw_ rr_ r_r

                => local init
                ifconst obj
                    => global init

                ifparallelmode parallel
                    size = parallelEndValue
                    size = size + 1
                else
                    size = 0
                    ifmode rrr rrw rr_
                    else
                        ifinstance obj PMap
                            array = obj.getRepresentation_PMap
                            size = array.length{object}
                        else
                            ifinstance obj Map
                                array = _.mapToArray obj
                                size = array.length{object}

                    ifconst obj
                    else
                        arrayShort = obj
                        arrayFloat = obj
                        arrayInt = obj
                        arrayLong = obj
                        arrayChar = obj
                        arrayBoolean = obj
                        arrayByte = obj
                        arrayDouble = obj
                        arrayObject = obj

                    ifinstance obj short[]
                        arrayShort = obj
                        size = arrayShort.length{short}
                    else
                        ifinstance obj float[]
                            arrayFloat = obj
                            size = arrayFloat.length{float}
                        else
                            ifinstance obj int[]
                                arrayInt = obj
                                size = arrayInt.length{int}
                            else
                                ifinstance obj long[]
                                    arrayLong = obj
                                    size = arrayLong.length{long}
                                else
                                    ifinstance obj char[]
                                        arrayChar = obj
                                        size = arrayChar.length{char}
                                    else
                                        ifinstance obj boolean[]
                                            arrayBoolean = obj
                                            size = arrayBoolean.length{boolean}
                                        else
                                            ifinstance obj byte[]
                                                arrayByte = obj
                                                size = arrayByte.length{byte}
                                            else
                                                ifinstance obj double[]
                                                    arrayDouble = obj
                                                    size = arrayDouble.length{double}
                                                else
                                                    arrayObject = obj
                                                    size = arrayObject.length{object}

                => local init

                ifmode rrr rrw rr_
                    ifinstance obj Map
                        ifinstance obj PMap
                            tmpValue = obj.get_PMap key
                        else
                            tmpValue = obj.get_Map key

                        ifnonnull tmpValue
                            ifmode rr_
                                return true
                            ifmode rrr
                                if tmpValue == value
                                    return true
                            ifmode rrw
                                value = tmpValue
                                return true
                        return false
                    else
                        if key >= size
                            return false

                        ifinstance obj short[]
                            tmpValueShort = arrayShort.[]{short} key
                            ifmode rrr
                                if tmpValueShort == value
                                    return true
                            ifmode rrw
                                value = tmpValueShort
                                return true
                        else
                            ifinstance obj float[]
                                tmpValueFloat = arrayFloat.[]{float} key
                                ifmode rrr
                                    if tmpValueFloat == value
                                        return true
                                ifmode rrw
                                    value = tmpValueFloat
                                    return true
                            else
                                ifinstance obj int[]
                                    tmpValueInt = arrayInt.[]{int} key
                                    ifmode rrr
                                        if tmpValueInt == value
                                            return true
                                    ifmode rrw
                                        value = tmpValueInt
                                        return true
                                else
                                    ifinstance obj long[]
                                        tmpValueLong = arrayLong.[]{long} key
                                        ifmode rrr
                                            if tmpValueLong == value
                                                return true
                                        ifmode rrw
                                            value = tmpValueLong
                                            return true
                                    else
                                        ifinstance obj char[]
                                            tmpValueChar = arrayChar.[]{char} key
                                            ifmode rrr
                                                if tmpValueChar == value
                                                    return true
                                            ifmode rrw
                                                value = tmpValueChar
                                                return true
                                        else
                                            ifinstance obj boolean[]
                                                tmpValueBoolean = arrayBoolean.[]{boolean} key
                                                ifmode rrr
                                                    if tmpValueBoolean == value
                                                        return true
                                                ifmode rrw
                                                    value = tmpValueBoolean
                                                    return true
                                            else
                                                ifinstance obj byte[]
                                                    tmpValueByte = arrayByte.[]{byte} key
                                                    ifmode rrr
                                                        if tmpValueByte == value
                                                            return true
                                                    ifmode rrw
                                                        value = tmpValueByte
                                                        return true
                                                else
                                                    ifinstance obj double[]
                                                        tmpValueDouble = arrayDouble.[]{double} key
                                                        ifmode rrr
                                                            if tmpValueDouble == value
                                                                return true
                                                        ifmode rrw
                                                            value = tmpValueDouble
                                                            return true
                                                    else
                                                        tmpValueObject = arrayObject.[]{object} key
                                                        ifmode rrr
                                                            if tmpValueObject == value
                                                                return true
                                                        ifmode rrw
                                                            value = tmpValueObject
                                                            return true

                        ifmode rr_
                            return true

                        return false
                else
                    ifparallelmode serial
                        position = 0
                    else
                        position = parallelStartValue

                    => iteration
                    startIteration:

                    if position >= size
                        return false

                    ifinstance obj Map
                        ifmode rwr rw_
                            elem1 = array.[]{object} position
                            ifnull elem1
                                goto skipElementMap

                        ifmode rwr r_w r_r
                            tmpPosition = position + 1
                            elem2 = array.[]{object} tmpPosition
                            ifnull elem2
                                goto skipElementMap

                            ifmode r_r
                                if elem2 == value
                                    return true
                                else
                                    goto skipElementMap
                            ifmode rwr
                                if elem2 != value
                                    goto skipElementMap

                        ifmode rwr rw_
                            key = elem1
                        ifmode r_w
                            value = elem2

                        position = position + 2
                        return true

                        skipElementMap:
                            position = position + 2
                            goto startIteration
                    else
                        ifmode rwr r_r r_w
                            ifinstance obj short[]
                                elemShort = arrayShort.[]{short} key
                                ifmode r_w
                                    value = elemShort
                                else
                                    if elemShort == value
                                        ifmode rwr
                                            key = position
                                        return true
                            else
                                ifinstance obj float[]
                                    elemFloat = arrayFloat.[]{float} key
                                    ifmode r_w
                                        value = elemFloat
                                    else
                                        if elemFloat == value
                                            ifmode rwr
                                                key = position
                                            return true
                                else
                                    ifinstance obj int[]
                                        elemInt = arrayInt.[]{int} key
                                        ifmode r_w
                                            value = elemInt
                                        else
                                            if elemInt == value
                                                ifmode rwr
                                                    key = position
                                                return true
                                    else
                                        ifinstance obj long[]
                                            elemLong = arrayLong.[]{long} key
                                            ifmode r_w
                                                value = elemLong
                                            else
                                                if elemLong == value
                                                    ifmode rwr
                                                        key = position
                                                    return true
                                        else
                                            ifinstance obj char[]
                                                elemChar = arrayChar.[]{char} key
                                                ifmode r_w
                                                    value = elemChar
                                                else
                                                    if elemChar == value
                                                        ifmode rwr
                                                            key = position
                                                        return true
                                            else
                                                ifinstance obj boolean[]
                                                    elemBoolean = arrayBoolean.[]{boolean} key
                                                    ifmode r_w
                                                        value = elemBoolean
                                                    else
                                                        if elemBoolean == value
                                                            ifmode rwr
                                                                key = position
                                                            return true
                                                else
                                                    ifinstance obj byte[]
                                                        elemByte = arrayByte.[]{byte} key
                                                        ifmode r_w
                                                            value = elemByte
                                                        else
                                                            if elemByte == value
                                                                ifmode rwr
                                                                    key = position
                                                                return true
                                                    else
                                                        ifinstance obj double[]
                                                            elemDouble = arrayDouble.[]{double} key
                                                            ifmode r_w
                                                                value = elemDouble
                                                            else
                                                                if elemDouble == value
                                                                    ifmode rwr
                                                                        key = position
                                                                    return true
                                                        else
                                                            elemObject = arrayObject.[]{object} key
                                                            ifmode r_w
                                                                value = elemObject
                                                            else
                                                                if elemObject == value
                                                                    ifmode rwr
                                                                        key = position
                                                                    return true

                            ifmode rwr r_r
                                goto skipElementArray

                        ifmode rw_
                            key = position

                        position = position + 1
                        return true

                        skipElementArray:
                            position = position + 1
                            goto startIteration
                */
            }
        }
        return null;
    }

    private static Void buildTokenElse (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        switch(token.instruction) {


            case TYPE_RANGE_CONTAINS:
            {
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_type int long
                rangecontains(start, stop, counter)
                    :rrr rrw

                => local init
                ifmode rrr
                    if counter >={{gen_type}} start
                        if counter <={{gen_type}} stop
                            return true
                    return false

                ifmode rrw
                    ifparallelmode serial
                        counter ={{gen_type}} start
                    else

                        threadIndex = __thread_index
                        threadAmount = __thread_amount
                        step = stop -{int} start
                        step = step + 1
                        step = step / threadAmount
                        parallelStart = threadIndex * step
                        parallelEnd = parallelStart + step
                        threadIndex = threadIndex + 1
                        if threadIndex == threadAmount
                            parallelEnd = stop -{int} start
                            parallelEnd = parallelEnd + 1

                        counter = parallelStart +{int} start
                        end = parallelEnd +{int} start
                        end = end - 1
                        tmpStop = end

                    counter = counter -{{gen_type}} 1

                    => iteration
                        counter = counter +{{gen_type}} 1
                        ifparallelmode serial
                            if counter >{{gen_type}} stop
                                return false
                        else
                            if counter >{{gen_type}} tmpStop
                                return false
                        return true

                 */
             }

            case REDUCTOR_SET:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import set
                */

            case REDUCTOR_MAP:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                map(key, value, obj)
                   :rrw

                => global init
                    obj = new PMap
                => local init
                    _ = obj.put_PMap key value
                */

            case REDUCTOR_DEFAULTMAP:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                defaultMap(default, key, value, obj)
                   :rrrw

                => global init
                    obj = new PDefaultMap default
                => local init
                    _ = obj.put_PDefaultMap key value
                */

            case REDUCTOR_N_MAP:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import n_map
                */

            case REDUCTOR_N_DEFAULTMAP:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import n_default_map
                */

            case REDUCTOR_METHOD_ADAPTER:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                method_adapter(value, result)
                   :rw

                => global init
                    result = __methodStandardValue
                => local init
                    result = _.__callMethod value result
                */

            case TYPE_CHECK_JAVA_TYPE:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import check_java_type
                */

            case TYPE_ADD:
            case TYPE_SUB:
            case TYPE_MUL:
            case TYPE_DIV:
            {
                String gen_operator = OperatorInfo.find(token.instruction).getOperatorSymbol();
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_operator + - * /
                gen_type int long double
                arithmetic{gen_operator}(val1, val2, result)
                    :rrr rrw

                => local init
                ifmode rrr
                    tmpValue = val1 {gen_operator}{{gen_type}} val2
                    if result == tmpValue
                        return true
                    else
                        return false
                else
                    result = val1 {gen_operator}{{gen_type}} val2
                    return true
                */
            }

            case TYPE_MOD:
            {
                String type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import mod
                */
            }

            case TYPE_BITAND:
            case TYPE_BITOR:
            case TYPE_BITXOR:
            case TYPE_BITSHL:
            case TYPE_BITSHR:
            case TYPE_BITSSHR:
            {
                String gen_operator = OperatorInfo.find(token.instruction).getOperatorSymbol();
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_operator % & | ^ << >> >>>
                gen_type int long
                arithmetic{gen_operator}(val1, val2, result)
                    :rrr rrw

                => local init
                ifmode rrr
                    tmpValue = val1 {gen_operator}{{gen_type}} val2
                    if result == tmpValue
                        return true
                    else
                        return false
                else
                    result = val1 {gen_operator}{{gen_type}} val2
                    return true
                */
            }

            case TYPE_NEG:
            {
                String type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import neg
                */
            }

            case TYPE_BITINV:
            {
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_type int long
                arithmetic~(val1, result)
                    :rr rw

                => local init
                ifmode rrr
                    tmpValue = val1 ~{{gen_type}}
                    if result == tmpValue
                        return true
                    else
                        return false
                else
                    result = val1 ~{{gen_type}}
                    return true
                */
            }

            case TYPE_EQ:
            {
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_type int long double object string
                comparison==(val1, val2)
                  :rr rw

                => local init
                ifmode rr
                    if val1 =={{gen_type}} val2
                        return true
                    else
                        return false
                else
                    val2 ={{gen_type}} val1
                    return true
                */
            }

            case TYPE_NEQ:
            {
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_type int long double object string
                comparison!=(val1, val2)
                  :rr

                => local init
                if val1 !={{gen_type}} val2
                    return true
                else
                    return false
                */
            }

            case TYPE_LTE:
            case TYPE_LT:
            {
                String operator = (token.instruction == TYPE_LTE ? "<=" : "<");
                String type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import lt_lte
                */
            }

            case TYPE_TRUE:
            {
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                true()
                  :

                => local init
                return true
                */
            }

            case TYPE_FALSE:
            {
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                false()
                  :

                => local init
                return false
                */
            }

            case TYPE_FIELD:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import field
                */

            case TYPE_SET_SIZE:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                set_size(obj, size)
                  :rw

                => local init

                ifinstance obj PSet
                    size = obj.size_PSet
                else
                    size = obj.size_Set
                return true
                */

            case TYPE_MAP_SIZE:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                map_size(obj, size)
                  :rw

                => local init

                ifinstance obj PMap
                    size = obj.size_PMap
                else
                    size = obj.size_Map
                return true
                */

            case TYPE_ARRAY_SIZE:
                String gen_type = FlagUtils.getTypeName(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                gen_type int long short char boolean byte float double object
                array_size(obj, size)
                  :rw

                => local init
                size = obj.length{{gen_type}}
                return true
                */

            case TYPE_POLY_SIZE:
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                poly_size(obj, size)
                  :rw

                => local init
                ifinstance obj PSet
                    size = obj.size_PSet
                else
                    ifinstance obj Set
                        size = obj.size_Set
                    else
                        ifinstance obj PMap
                            size = obj.size_PMap
                        else
                            ifinstance obj Map
                                size = obj.size_Map
                            else
                                ifinstance obj int[]
                                    size = obj.length{int}
                                else
                                    ifinstance obj long[]
                                        size = obj.length{long}
                                    else
                                        ifinstance obj short[]
                                            size = obj.length{short}
                                        else
                                            ifinstance obj char[]
                                                size = obj.length{char}
                                            else
                                                ifinstance obj boolean[]
                                                    size = obj.length{boolean}
                                                else
                                                    ifinstance obj byte[]
                                                        size = obj.length{byte}
                                                    else
                                                        ifinstance obj float[]
                                                            size = obj.length{float}
                                                        else
                                                            ifinstance obj double[]
                                                                size = obj.length{double}
                                                            else
                                                                size = obj.length{object}
                return true
                */

            case TYPE_CHECK_TYPE:
            {
                String min = FlagUtils.getMinValue(token.type);
                String max = FlagUtils.getMaxValue(token.type);
                // ### BytecodeCreatorRoutine => EntrancePoint
                /*
                import check_type
                */
            }

            default:
                throw new RuntimeCreatorException("unknown command. (id: " + token.instruction + ")");
       }
    }

    private static Void buildTokenExtension (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        switch(token.instruction) {
            // ### BytecodeCreatorRoutine => Additional Extensions
        }
        return null;
    }

}
