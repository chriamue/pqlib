/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2;

import rc2.bcr.BCRExtension;

/**
 *
 * @author Hilmar
 */
public class AddSubIntLongGenericInterface implements GenericInterface {

    @Override
    public String[] getGenerics() {
        return new String [] {"operator", "type"};
    }

    @Override
    public String getGenericContent(BCRExtension extension, String name) {
        if (name.equals("operator"))
            return ((String[])extension.getGenericInformation())[0];
        else
            return ((String[])extension.getGenericInformation())[1];
    }


}
