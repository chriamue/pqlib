/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2;

import bench.RuntimeCreatorBench;
import benchmarks.bonus.Bonus;
import benchmarks.bonus.Department;
import benchmarks.bonus.Employee;
import edu.umass.bc.RuntimeClassLoader;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.DefaultValueDouble;
import edu.umass.pql.DefaultValueInt;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.PQLFactory.ParallelisationMode;
import edu.umass.pql.ParallelQuery;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.Container.CONTAINS;
import edu.umass.pql.il.Container.LOOKUP;
import edu.umass.pql.opt.Optimizer;
import java.io.File;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Spliterator;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenSource;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNState;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.tree.ParseTree;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.TraceClassVisitor;
import rc2.bcr.BCRAccessMode;
import rc2.bcr.BCRCommand;
import rc2.bcr.BCRExtension;
import rc2.bcr.BCRGeneric;
import rc2.bcr.BCRVariable;
import rc2.bcr.BytecodeCreatorRoutine;
import rc2.bcr.EqualPredicate;
import rc2.bcr.FunctionParser;
import rc2.bcr.NewMethodAttribute;
import rc2.evaluator.AnyFieldOrMethodContainingObject;
import rc2.evaluator.Evaluator;
import rc2.parallel.ParallelExecution;
import rc2.parser.BCRLexer;
import rc2.parser.BCRParser;
import rc2.representation.AdditionalInfo;
import rc2.representation.PQLArgument;
import rc2.representation.PQLExtension;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;
import replacetool.ReplaceTool;

/**
 *
 * @author Hilmar
 */
public class RCInterface extends TestBase implements VarConstants, TableConstants {

    public enum ParallelMode {Serial, Parallel};

    public static int BYTECODE_OUTPUT = 1;
    public static int CODE_COMMENTS = 2;
    public static int DETAILED_DEBUG = 4;
    public static int OPTIMIZE = 8;
    public static int NO_ERROR_FILTERING = 16;
    public static int DYNAMIC_OPTIMIZE = 32;
    public static int LINE_DEBUG = 64;

    public SkeletonBuilder skeletonBuilder;
    public PQLToken startPQLToken;
    public Method startMethod;
    public PQLToken originPQLToken;
    public SkeletonBuilder originPQLTokenSkeletonBuilder;
    public boolean errorFiltering;

    public RCInterface (ExtensionInterface ... extensions) throws RuntimeCreatorException
    {
        ExtensionInterface.registerExtensions(extensions);
        errorFiltering = true;
    }

    @DefaultValueInt(0)
    public static int sumInt (int a, int b) {
        return a+b;
    }

    @DefaultValueDouble(0.0D)
    public static double sumDouble (double a, double b) {
        return a+b;
    }

    public void build (Join startToken, Env env, Map <Integer, Class> knownInstances, ParallelMode parallelMode) throws Throwable {
        build(startToken, env, knownInstances, parallelMode, 0);
    }

    public void build (Join startToken, Env env, Map <Integer, Class> knownInstances, ParallelMode parallelMode, int creationFlags) throws Throwable {
        try {

            originPQLTokenSkeletonBuilder = new SkeletonBuilder(parallelMode);
            originPQLToken = new PQLToken(startToken, null, originPQLTokenSkeletonBuilder, env);

            if ((creationFlags & DYNAMIC_OPTIMIZE) != 0) {

                skeletonBuilder = new SkeletonBuilder(parallelMode);
                startPQLToken = PQLToken.create(startToken, skeletonBuilder, env, knownInstances);
                if (parallelMode != ParallelMode.Serial)
                    for (PQLToken token : startPQLToken.getAllChildsInOrder())
                        if (token.startsIteration) {
                            token.parallelModeIndex = parallelMode.ordinal();
                            break;
                        }
                for (PQLToken token : startPQLToken.getAllChildsInOrder())
                    if (token.fromJoin instanceof PQLExtension)
                        ((PQLExtension)token.fromJoin).bindToPQLToken(token, skeletonBuilder, knownInstances);

                VarSet constantVars = new VarSet();
                for (PQLArgument arg : skeletonBuilder.pqlArguments)
                    if (arg.isConst)
                        constantVars.insert(Env.encodeReadVar(arg.type, arg.index));
                startToken = Optimizer.selectAccessPathRecursively(env,constantVars,startToken,true);
                originPQLToken = new PQLToken(startToken, null, originPQLTokenSkeletonBuilder, env);
            }
            skeletonBuilder = new SkeletonBuilder(parallelMode);
            errorFiltering = ((creationFlags & NO_ERROR_FILTERING) == 0);
            startPQLToken = PQLToken.create(startToken, skeletonBuilder, env, knownInstances);
            if (parallelMode != ParallelMode.Serial)
                for (PQLToken token : startPQLToken.getAllChildsInOrder())
                    if (token.startsIteration) {
                        token.parallelModeIndex = parallelMode.ordinal();
                        break;
                    }

            if ((creationFlags & DETAILED_DEBUG) != 0)
                System.out.println(startPQLToken);

            //System.out.println(startPQLToken.toString());
            RuntimeClassLoader cl = new RuntimeClassLoader ();
            Class dynamicClass = cl.defineClass("Evaluator", skeletonBuilder.build(startPQLToken, env, knownInstances, creationFlags) );
            for (int i=0; i<dynamicClass.getMethods().length; i++) {
                if (dynamicClass.getMethods()[i].getName().equals("evaluate")) {
                    startMethod = dynamicClass.getMethods()[i];
                    break;
                }
            }
        } catch (Throwable t) {
            if (!errorFiltering)
                throw t;
            else
                Evaluator.filterError(t, originPQLToken, originPQLTokenSkeletonBuilder, skeletonBuilder, env);
        }
    }

    public boolean exec (Env env) throws Exception {
        if (skeletonBuilder != null && skeletonBuilder.buildParallel == ParallelMode.Parallel) {
            ParallelExecution parallelExec = new ParallelExecution(this, env);
            return parallelExec.run();
        } else {
            Boolean result = false;
            try {
                if (startMethod == null)
                    throw new Exception("PQL-Code not build yet.");
                if (skeletonBuilder.buildParallel != ParallelMode.Serial)
                    throw new Exception("wrong parallel-mode builded.");

                result = (Boolean)startMethod.invoke(null, new Object[] { env });
            } catch (Exception e) {
                if (skeletonBuilder.debugMode && skeletonBuilder.debugLine >= 0)
                    System.out.println("Line-Debug: Line " + skeletonBuilder.debugLine + " in source '" + (skeletonBuilder.debugSource == null ? "rc2.MainCommands.java" : skeletonBuilder.debugSource) + "'");
                if (!errorFiltering)
                    throw e;
                else
                    Evaluator.filterError(e, originPQLToken, originPQLTokenSkeletonBuilder, skeletonBuilder, env);
            }
            return result;
        }
    }

    public boolean execRange (Env env, int from, int to) throws Exception {
        Boolean result = false;
        try {
            if (startMethod == null)
                throw new Exception("PQL-Code not build yet.");
            if (skeletonBuilder.buildParallel != ParallelMode.Parallel)
                throw new Exception("wrong parallel-mode builded.");

            result = (Boolean)startMethod.invoke(null, new Object[] { env, from, to });
        } catch (Exception e) {
            if (!errorFiltering)
                throw e;
            else
                Evaluator.filterError(e, originPQLToken, originPQLTokenSkeletonBuilder, skeletonBuilder, env);
        }
        return result;
    }

    public static void main (String [] args) {

        /*try {
            FunctionParser parser = new FunctionParser ("lookup");
            BytecodeCreatorRoutine.commands = new ArrayList <BCRCommand> ();
            BytecodeCreatorRoutine.accessModes = new ArrayList <BCRAccessMode> ();
            BytecodeCreatorRoutine.variables = new ArrayList <BCRVariable> ();
            BytecodeCreatorRoutine.generics = new ArrayList <BCRGeneric> ();
            BytecodeCreatorRoutine.pwCommands = new PrintWriter(new File("src/rc2/created/gen_MainCommands.java"));
            NewMethodAttribute.newMethodAttributes = new ArrayList <NewMethodAttribute> ();
            parser.buildCommands(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        int ia = 42;
        if (ia == 42)
            return;*/
        /*Map <String, String> fs = new HashMap <String, String> ();
        Set <String> fs2 = new HashSet <String> ();
        List <String> fs3 = new ArrayList <String> ();
        fs3.add("abc");
        fs3.add("deffh");
        fs3.add("ghi");
        fs3.stream().filter(new Predicate<String>() {
          @Override
          public boolean test(String element) {
              return element.length() > 4;
          }}).forEach(new Consumer <String> () {
              @Override
          public void accept(String element) {
              System.out.println(element);
          }
          });*/

        /*List <Integer> list = new ArrayList <Integer> ();
        Random rand = new Random(12345);
        for (int i=0; i<10000; i++)
            list.add(new Integer(rand.nextInt()));

        Spliterator split = list.spliterator();
        int [] abc = {Spliterator.ORDERED,Spliterator.DISTINCT,Spliterator.SORTED,Spliterator.SIZED,Spliterator.NONNULL,Spliterator.IMMUTABLE,Spliterator.CONCURRENT, Spliterator.SUBSIZED};
        String [] abc2 = {"Spliterator.ORDERED","Spliterator.DISTINCT","Spliterator.SORTED","Spliterator.SIZED","Spliterator.NONNULL","Spliterator.IMMUTABLE","Spliterator.CONCURRENT","Spliterator.SUBSIZED"};
        System.out.println("A: " + split.characteristics());
        for (int os=0; os<abc.length; os++)
            System.out.println("A2: " + ((split.characteristics()&abc[os]) != 0 ? abc2[os] : ""));
        System.out.println("B: " + split.estimateSize());
        System.out.println("C: " + split.getExactSizeIfKnown());
        System.out.println("D: " + split.toString());
        Spliterator split2 = split.trySplit().trySplit().trySplit().trySplit().trySplit().trySplit().trySplit().trySplit().trySplit();

        System.out.println("A: " + split.characteristics());
        for (int os=0; os<abc.length; os++)
            System.out.println("A2: " + ((split.characteristics()&abc[os]) != 0 ? abc2[os] : ""));
        System.out.println("B: " + split.estimateSize());
        System.out.println("C: " + split.getExactSizeIfKnown());
        System.out.println("D: " + split.toString());

        System.out.println("A: " + split2.characteristics());
        for (int os=0; os<abc.length; os++)
            System.out.println("A2: " + ((split2.characteristics()&abc[os]) != 0 ? abc2[os] : ""));
        System.out.println("B: " + split2.estimateSize());
        System.out.println("C: " + split2.getExactSizeIfKnown());
        System.out.println("D: " + split2.toString());

        for (int safr=0; safr<100; safr++) {
            split2.tryAdvance(new java.util.function.Consumer <Integer> () {

                @Override
                public void accept(Integer t) {
                    System.out.print(t + ", ");
                }
            });
            System.out.println("");
        }

        int sdfi = 24;
        if (sdfi != 25)
            return;*/

        /*try {
            System.out.println("A");
            System.out.println(Evaluator.getStandardEvaluator().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /*PMap fds = new PMap ();
        fds.get(Env.canonicalInteger(2));
        try {
            ClassReader cr2 = new ClassReader("rc2.RCInterface");
            ClassVisitor cv2 = new ClassWriter(cr2, 0);
            PrintWriter pw2 = new PrintWriter(System.out);
            TraceClassVisitor cw2 = new TraceClassVisitor(cv2, pw2);
            cr2.accept(cw2, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        //BytecodeCreatorRoutine.create(new MySqlExtension());
        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.SEGMENTED);
        //PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

        //antlr zum parsen

        try {
        RCInterface rc = new RCInterface();
        Env env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[100],
                        new long[100],
                        new double[100],
                        new Object[100]
                });

        Method method = null;
        for (int i=0; i<RCInterface.class.getMethods().length; i++) {
            if (RCInterface.class.getMethods()[i].getName().contains("sumDouble"))
                method = RCInterface.class.getMethods()[i];
        }

        //Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.MAP(l1r, d1r, o1w) }, new LOOKUP(o0r, l1w, TestBase._) );
        //Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, i1r, i2w) }, new CONTAINS(o3r, i1w) );
        //Join j = PQLFactory.FIELD(RCInterface.class, "test", o4r, i0r);
        //Join j =  new CONTAINS(o3r, i1r);
        //Join j = new LOOKUP(o0r, TestBase._, i0r);
        //Join j = PQLFactory.LT_Double(d2r, d1r);
        PMap pMap = new PMap ();
        pMap.put(new Integer(23), new Integer(15));
        pMap.put(new Integer(62143), new Integer(83));
        pMap.put(new Integer(13), new Integer(18));
        PSet pset = new PSet();
        pset.add(new Integer(24));
        pset.add(new Integer(65234));
        pset.add(new Integer(12));
        env.v_object[0] = pMap;
        env.v_object[1] = new String("abc");
        env.v_object[2] = env.v_object[1];
        env.v_object[3] = pset;
        env.v_object[4] = rc;

        env.v_int[0] = 256;
        env.v_int[1] = 24;
        env.v_int[2] = 0;
        env.v_int[4] = 2;
        env.v_int[5] = 392;
        env.v_int[6] = 0;
        env.v_int[7] = 1000;
        env.v_int[8] = 22;
        env.v_double[1] = 32.23f;
        env.v_double[2] = 5234.5423f;
        env.v_long[1] = 24;

        final int d4r = Env.encodeReadVar(TYPE_DOUBLE, 4);
        final int d4w = Env.encodeWriteVar(TYPE_DOUBLE, 4);
        final int d5r = Env.encodeReadVar(TYPE_DOUBLE, 5);
        final int o6r = Env.encodeReadVar(TYPE_OBJECT, 6);
        final int o6w = Env.encodeWriteVar(TYPE_OBJECT, 6);
        final int o7r = Env.encodeReadVar(TYPE_OBJECT, 7);
        final int o7w = Env.encodeWriteVar(TYPE_OBJECT, 7);
        final int o8r = Env.encodeReadVar(TYPE_OBJECT, 8);
        final int o8w = Env.encodeWriteVar(TYPE_OBJECT, 8);
        final int o12r = Env.encodeReadVar(TYPE_OBJECT, 12);
        final int o13r = Env.encodeReadVar(TYPE_OBJECT, 13);
        final int o13w = Env.encodeWriteVar(TYPE_OBJECT, 13);
        final int o14w = Env.encodeWriteVar(TYPE_OBJECT, 14);

        final int i7r = Env.encodeReadVar(TYPE_INT, 7);
        final int i8r = Env.encodeReadVar(TYPE_INT, 8);
        final int i9r = Env.encodeReadVar(TYPE_INT, 9);
        final int i9w = Env.encodeWriteVar(TYPE_INT, 9);

        /*Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.MAP(o2r, d0r, o6w) },
                PQLFactory.CONTAINS( (o0r | VAR_CONST_FLAG), o2w),
                PQLFactory.FIELD(Employee.class, "dept", o2r, o1w),
                PQLFactory.FIELD(Department.class, "bonus_factor", o1r, d1w),
                PQLFactory.ADD_Double( (d5r | VAR_CONST_FLAG), (d5r | VAR_CONST_FLAG), d4w),
                PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, d2r, d4w) },
                    PQLFactory.FIELD(Employee.class, "bonusSet", o2r, o4w),
                    PQLFactory.CONTAINS(o4r, o3w),
                    PQLFactory.FIELD(Bonus.class, "bonus_base", o3r, d2w)
                    ),
                PQLFactory.MUL_Double(d1r, d4r, d0w)
                );*/

        env.v_int[9] = 12;

        env.v_int[1] = 16;
        env.v_int[2] = 15;
        //Join p = PQLFactory.DisjunctiveBlock(new PQLExtension("mysql", o13r, o13r, o13r, o13r, o13r, o13r, o14w) );
        //Join p = PQLFactory.DisjunctiveBlock(new PQLExtension("sqrt", i0r, i2r), new PQLExtension("sqrt", i0r, i1r), new PQLExtension("sqrt", i0r, i2r) );


        /*env.v_long[0] = 353529687245l;
        env.v_long[1] = 3;
        Join p = new PQLExtension("sub_long", l0r, l1r, l2w);*/

        Join p = new PQLExtension("prime_range", i1r, i0r, i2w);
        /*env.v_int[2] = 10000;
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i0r, o3w)}, PQLFactory.ConjunctiveBlock(PQLFactory.BYTE(l2w), PQLFactory.SET_SIZE(o3r, i1w), PQLFactory.LTE_Int(i1r, i2r)));*/
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(o8r, o3w)}, PQLFactory.INT_RANGE_CONTAINS(i1r, i0r, l2w), PQLFactory.ConjunctiveBlock(PQLFactory.CONTAINS(o6r, o8w), PQLFactory.JAVA_TYPE(Class.class, o8r)));
//Join p = new PQLExtension("sqrt", i0r, i0w);
        //Join p = PQLFactory.CONTAINS( o13r, o2w);
        env.v_object[13] = "1234";
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i9r, o5w) } , PQLFactory.ConjunctiveBlock( PQLFactory.MOD_Int(i7r, i9r, i8r)));
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i2r, o5w) } , PQLFactory.ConjunctiveBlock(new PQLExtension("prime_range", i1r, i0r, i2w), PQLFactory.TRUE));
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i2r, o5w) } , PQLFactory.ConjunctiveBlock( PQLFactory.INT_RANGE_CONTAINS(i6r, i0r, i2w), new PQLExtension("prime_check", i2r), PQLFactory.TRUE));
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i9r, o5w) } , PQLFactory.ConjunctiveBlock( PQLFactory.MOD_Int(i7r, i9w, i8r), PQLFactory.TRUE));
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i9r, o5w) } , PQLFactory.ConjunctiveBlock( new PQLExtension("stream_contains", o12r, i9w), PQLFactory.TRUE));
        //Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i9r, o5w) } , PQLFactory.ConjunctiveBlock( new PQLExtension("stream_contains", o12r, i9r), PQLFactory.TRUE));
        String abc = "";

                /*
                REDUCE[SET(?e; !r)] f
CONTAINS(?s1; !e);
FIELDhPoint; xi(?e; !t0);
GEhinti(?t0; 0);
CONTAINS(?s2; ?e); g
                */

        /*Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o6w) },
                PQLFactory.ConjunctiveBlock(PQLFactory.INT_RANGE_CONTAINS( i4r, i5r, i1w) ));*/

        /*Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o6w) },
                PQLFactory.ConjunctiveBlock(PQLFactory.POLY_LOOKUP( o7r, i4w, i1r) ,
                PQLFactory.POLY_SIZE(o7r, i1w)));*/

        /*List <Join> joins = new ArrayList <Join> ();
        joins.add(PQLFactory.FIELD(AnyFieldOrMethodContainingObject.class, "field", Env.encodeReadVar(TYPE_OBJECT, 10), Env.encodeReadVar(TYPE_OBJECT, 11)));
        for (int i=0; i<3; i++)
            joins.add(PQLFactory.EQ_Object(Env.encodeReadVar(TYPE_OBJECT, 10+i), Env.encodeWriteVar(TYPE_OBJECT, 10+i)));
        Join p = PQLFactory.ConjunctiveBlock(joins);*/

        /*Join p = null;
        try {
            List <Join> joins = new ArrayList <Join> ();
            for (int i=0; i<3; i++)
                joins.add(PQLFactory.EQ_Object(Env.encodeReadVar(TYPE_OBJECT, 10+i), Env.encodeWriteVar(TYPE_OBJECT, 10+i)));
            p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(AnyFieldOrMethodContainingObject.class.getDeclaredMethod("methodObj", double.class, double.class), Env.encodeReadVar(TYPE_DOUBLE, 10), Env.encodeReadVar(TYPE_DOUBLE, 10)) }, PQLFactory.ConjunctiveBlock(joins));
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        //Join p = PQLFactory.CONTAINS(o4r, o3w);
        /*RuntimeCreator.useRuntimeCreator = false;
        env.v_object[1] = null;
        System.out.println(env.v_object[1]);
        rc.env = env;
        rc.checkFalse(j);
        System.out.println(env.v_object[1]);*/

        Map <Integer, Class> inst = new HashMap <Integer, Class> ();
        inst.put(0, PMap.class);
        inst.put(1, String.class);
        inst.put(2, String.class);
        inst.put(3, PSet.class);
        inst.put(4, RCInterface.class);
        inst.put(6, PSet.class);
        inst.put(7, double[].class);

        (new benchmarks.bonus.Generator()).init(100);
        //(new benchmarks.threegrep.Generator()).init(100);
	//(new benchmarks.webgraph.Generator()).init(100);

        env.v_double[2] = 0;
        env.v_double[4] = 0;
        env.v_double[5] = 0;
        env.v_double[10] = 0;
        env.v_object[0] = benchmarks.bonus.Generator.employees;
        env.v_object[1] = new Department(0);
        env.v_object[2] = new Employee(null);
        env.v_object[3] = new Bonus("", 0);
        env.v_object[4] = new TreeSet();
        env.v_object[5] = new PSet();
        env.v_object[6] = new PMap();
        env.v_object[7] = new double[] {129, 234, 324.24f, 78, 324, 26};
        env.v_object[8] = new int[] {0};
        env.v_object[9] = new int[] {0};
        env.v_object[10] = new AnyFieldOrMethodContainingObject();
        env.v_object[11] = new PSet();
        List <Integer> listInt = new ArrayList <Integer> ();
        listInt.add(12);
        listInt.add(34);
        listInt.add(2342);
        env.v_object[12] = listInt.stream();
        env.v_object[13] = new String("fs");
        env.v_object[14] = new String("abc");

        inst.put(0, PSet.class);
        inst.put(1, Department.class);
        inst.put(2, Employee.class);
        inst.put(3, Bonus.class);
        inst.put(4, TreeSet.class);
        inst.put(5, PSet.class);
        inst.put(6, PMap.class);
        inst.put(12, Stream.class);
        inst.put(13, String.class);
        inst.put(14, String.class);

        PSet a = new PSet();
        a.add(new Integer (4));
        a.add(BCRExtension.class);
        a.add(new String("fs"));
        env.v_object[6] = a;
        inst.put(6, PSet.class);
        env.v_object[3] = a;
        inst.put(3, PSet.class);

        /*java.awt.Point pts [] = new java.awt.Point[] {  new java.awt.Point(0,0), new java.awt.Point(1,8), new java.awt.Point(-3,-5), new java.awt.Point(1,-7), new java.awt.Point(-3,1) };
        PSet <java.awt.Point> set1 = new PSet <java.awt.Point> ();
        PSet <java.awt.Point> set2 = new PSet <java.awt.Point> ();
        set1.add(pts[0]);
        set1.add(pts[1]);
        set1.add(pts[2]);
        set1.add(pts[3]);
        set1.add(pts[4]);
        set2.add(pts[0]);
        set2.add(pts[1]);
        set2.add(pts[2]);
        Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(o1r, o0w) },
                PQLFactory.CONTAINS(o2r, o1w),
                PQLFactory.FIELD(java.awt.Point.class, "x", o1r, i0w),
                PQLFactory.NEG_Int(i0r, i0w),
                PQLFactory.LTE_Int(i0r, i1r),
                PQLFactory.CONTAINS(o3r, o1r));

        inst.put(0, PSet.class);
        inst.put(1, java.awt.Point.class);
        inst.put(2, PSet.class);
        inst.put(3, PSet.class);
        env.v_object[0] = new PSet();
        env.v_object[1] = new java.awt.Point(0,0);
        env.v_object[2] = set1;
        env.v_object[3] = set2;
        env.v_int[1] = 0;*/




            rc.build(p, env, inst, ParallelMode.Serial, BYTECODE_OUTPUT|CODE_COMMENTS|DETAILED_DEBUG|LINE_DEBUG);

            System.out.println(rc.startPQLToken.toString());
            //System.out.println(env.v_object[5]);
            //System.out.println( ((PMap)env.v_object[6]).size());
            System.out.println( "SET_TEST: " + env.v_object[3]);
            System.out.println("OBJ: " + env.v_object[5]);
            System.out.println( env.v_int[0] );
            //System.out.println(rc.execRange(env, 0, 20));
            System.out.println(rc.exec(env));
            //System.out.println(env.v_object[0]);

            //if (PQLFactory.getParallelisationMode() == ParallelisationMode.SEGMENTED)
            //ParallelQuery.


            //erstes Join (solange reductor oder kontrollstruktur springe weiter)
            //execRange(env,from,to) (0-250 as ex.)
            //from, to => join.getFanout(), ParallelQuery => Evaluator => attribut index


            //System.out.println( ((PMap)env.v_object[5]).keySet().size() );
            //System.out.println( ((PMap)env.v_object[6]).size());
            System.out.println( ((PSet)env.v_object[5]).size());
            System.out.println( env.v_int[0] );
            List <Integer> list1 = new ArrayList <Integer>();
            for (Object ent : ((PSet)env.v_object[5]))
                list1.add((Integer)ent);
            Collections.sort(list1);
            System.out.println( "SET_TEST: " + env.v_object[3]);
            System.out.println("OBJ: " + env.v_object[5]);
            System.out.println("OBJ: " + list1);
            System.out.println("Long (2): " + env.v_long[2]);
        } catch (Throwable t) {
            if (t instanceof InvocationTargetException)
                t = t.getCause();
            System.out.println(t.toString());
            for (StackTraceElement ste : t.getStackTrace())
                System.out.println(ste);
        }
    }

}
