/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2;

import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;
import rc2.bcr.BCRExtension;
import rc2.bcr.BCRExtension.ExtensionType;
import rc2.bcr.ConstructorInfo;
import rc2.bcr.EqualPredicate;
import rc2.bcr.MethodTranslate;
import rc2.bcr.MethodTranslateAlias;
import rc2.parallel.ParallelMerge.NDefaultMapMethodAdapterParallelMerge;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class StandardExtension extends ExtensionInterface {

    @Override
    protected void defineExtensions() throws RuntimeCreatorException {
        bcrExtensions.add(new BCRExtension("sqrt", "sqrt", 2, ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("prime_check", "prime_check", 1, ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("prime_range", "prime_range", 3, ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("stream_contains", "stream_contains", 2, ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("n_default_map_method_adapter", "n_default_map_method_adapter", 4, ExtensionType.Reductor, new NDefaultMapMethodAdapterParallelMerge()));

        bcrExtensions.add(new BCRExtension("add_int", "add_sub_int_long", 3, ExtensionType.Join, AddSubIntLongGenericInterface.class, new String[]{"+", "int"}));
        bcrExtensions.add(new BCRExtension("add_long", "add_sub_int_long", 3, ExtensionType.Join, AddSubIntLongGenericInterface.class, new String[]{"+", "long"}));
        bcrExtensions.add(new BCRExtension("sub_int", "add_sub_int_long", 3, ExtensionType.Join, AddSubIntLongGenericInterface.class, new String[]{"-", "int"}));
        bcrExtensions.add(new BCRExtension("sub_long", "add_sub_int_long", 3, ExtensionType.Join, AddSubIntLongGenericInterface.class, new String[]{"-", "long"}));
    }

    @Override
    protected void defineConstructors() {
        constructors.add(new ConstructorInfo("EqualPredicate", EqualPredicate.class, new Class [] {Object.class}));
        constructors.add(new ConstructorInfo("PDefaultMap", PDefaultMap.class, new Class [] {Object.class}));
        constructors.add(new ConstructorInfo("PSet", PSet.class, new Class [] {}));
        constructors.add(new ConstructorInfo("Set", Set.class));
        constructors.add(new ConstructorInfo("PMap", PMap.class, new Class [] {}));
        constructors.add(new ConstructorInfo("Map", Map.class));
    }

    @Override
    protected void defineMethods() throws RuntimeCreatorException {
        methods.add(new MethodTranslate("get_PMap", "get", PMap.class, new Class [] {Object.class}, Object.class ) );
        methods.add(new MethodTranslate("get_Map", "get", Map.class, new Class [] {Object.class}, Object.class ) );
        methods.add(new MethodTranslate("getRepresentation_PMap", "getRepresentation", PMap.class, new Class [] {}, Object [].class ) );
        methods.add(new MethodTranslate("mapToArray", "mapToArray", SubCommands.class, new Class [] {Map.class}, Object [].class ) );
        methods.add(new MethodTranslate("add_PSet", "add", PSet.class, new Class [] {Object.class}, boolean.class ) );
        methods.add(new MethodTranslate("put_PMap", "put", PMap.class, new Class [] {Object.class, Object.class}, Object.class ) );
        methods.add(new MethodTranslate("put_PDefaultMap", "put", PDefaultMap.class, new Class [] {Object.class, Object.class}, Object.class ) );
        methods.add(new MethodTranslate("getRepresentation_PSet", "getRepresentation", PSet.class, new Class [] {}, Object [].class ) );
        methods.add(new MethodTranslate("toArray", "toArray", Set.class, new Class [] {}, Object [].class ) );
        methods.add(new MethodTranslate("contains_PSet", "contains", PSet.class, new Class [] {Object.class}, boolean.class ) );
        methods.add(new MethodTranslate("contains_Set", "contains", Set.class, new Class [] {Object.class}, boolean.class ) );
        methods.add(new MethodTranslate("size_PMap", "size", PMap.class, new Class [] {}, int.class ) );
        methods.add(new MethodTranslate("size_Map", "size", Map.class, new Class [] {}, int.class ) );
        methods.add(new MethodTranslate("size_PSet", "size", PSet.class, new Class [] {}, int.class ) );
        methods.add(new MethodTranslate("size_Set", "size", Set.class, new Class [] {}, int.class ) );
        methods.add(new MethodTranslate("sqrt", "sqrt", Math.class, new Class [] {double.class}, double.class ) );
        methods.add(new MethodTranslate("iterator_Stream", "iterator", Stream.class, new Class [] {}, Iterator.class ) );
        methods.add(new MethodTranslate("iterator_Set", "iterator", Set.class, new Class [] {}, Iterator.class ) );
        methods.add(new MethodTranslate("hasNext", "hasNext", Iterator.class, new Class [] {}, boolean.class ) );
        methods.add(new MethodTranslate("next", "next", Iterator.class, new Class [] {}, Object.class ) );
        methods.add(new MethodTranslate("anyMatch", "anyMatch", Stream.class, new Class [] {Predicate.class}, boolean.class ) );
        methods.add(new MethodTranslate("println", "println", StandardExtension.class, new Class [] {Object.class}, void.class ) );
        methods.add(new MethodTranslate("isInstance", "isInstance", Class.class, new Class [] {Object.class}, boolean.class ) );
    }

    public static void println (Object msg) {
        System.out.println(msg);
    }

    @Override
    protected void defineMethodAliases() {
        methodAliases.add(new MethodTranslateAlias("get", new String[] {"get_PMap", "get_Map"}, new String[] {"PMap", "Map"}));
        methodAliases.add(new MethodTranslateAlias("contains", new String[] {"contains_PSet", "contains_Set"}, new String[] {"PSet", "Set"}));
    }

}
