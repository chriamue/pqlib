/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2;

import rc2.bcr.BCRExtension;

/**
 *
 * @author Hilmar
 */
public interface GenericInterface {

    public String[] getGenerics();
    public String getGenericContent (BCRExtension extension, String name);

}
