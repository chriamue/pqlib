/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rc2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import rc2.bcr.BCRExtension;
import rc2.bcr.ConstructorInfo;
import rc2.bcr.MethodTranslate;
import rc2.bcr.MethodTranslateAlias;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public abstract class ExtensionInterface {

    private static List <ExtensionInterface> extensions = new ArrayList <ExtensionInterface> ();

    protected List <BCRExtension> bcrExtensions;
    protected Set <ConstructorInfo> constructors;
    protected Set <MethodTranslate> methods;
    protected Set <MethodTranslateAlias> methodAliases;

    public ExtensionInterface() {
        bcrExtensions = new ArrayList <BCRExtension> ();
        constructors = new HashSet <ConstructorInfo> ();
        methods = new HashSet <MethodTranslate> ();
        methodAliases = new HashSet <MethodTranslateAlias> ();
    }

    public void register() throws RuntimeCreatorException {
        for (ExtensionInterface extension : extensions)
            if (extension.getClass().equals(this.getClass()))
                return;

        this.defineExtensions();
        this.defineConstructors();
        this.defineMethods();
        this.defineMethodAliases();
        extensions.add(this);
    }

    public static ExtensionInterface [] getAllExtensions () {
        return extensions.toArray(new ExtensionInterface[extensions.size()]);
    }

    public List <BCRExtension> getBCRExtensions () {
        return bcrExtensions;
    }

    public Set <ConstructorInfo> getConstructors () {
        return constructors;
    }

    public Set <MethodTranslate> getMethods () {
        return methods;
    }

    public Set <MethodTranslateAlias> getMethodAliases () {
        return methodAliases;
    }

    @Override
    public int hashCode () {
        return 0;
    }

    @Override
    public boolean equals (Object other) {
        return (other != null && other.getClass().getName().equals(other.getClass().getName()));
    }

    protected abstract void defineExtensions() throws RuntimeCreatorException;
    protected abstract void defineConstructors();
    protected abstract void defineMethods() throws RuntimeCreatorException;
    protected abstract void defineMethodAliases();

    public static void registerExtensions (ExtensionInterface [] interfaces) throws RuntimeCreatorException {
        new StandardExtension().register();
        for (ExtensionInterface extension : interfaces)
            extension.register();
    }

}
