/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rc2;

import benchmarks.BenchmarkOptions;
import edu.umass.bc.BcFlags;
import edu.umass.pql.Env;
import edu.umass.pql.VarConstants;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.TraceClassVisitor;
import rc2.RCInterface.ParallelMode;
import rc2.buildbytecode.BuildBytecode;
import rc2.optimizer.OptimizeVisitor;
import rc2.representation.CastInfo;
import rc2.representation.PQLArgument;
import rc2.representation.PQLReturnBehavior;
import rc2.representation.PQLToken;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class SkeletonBuilder implements Opcodes, VarConstants, BcFlags {

    enum VisitState {Always, Sometimes};

    public List <PQLArgument> pqlArguments;
    public int localIndexCount;
    //public List <Integer> reservedVariables;
    //public Map <PQLToken, Integer> reservedVariables;
    public Map <PQLToken, List <Integer> > reservedVariables;
    public int reservedVariableCount;
    public Map <PQLToken, Label> iterationLabels;
    public Label endLabel;
    public Map <Integer, Class> knownInstances;
    public Env env;
    public Map <PQLToken, Label> initLabels;
    public List <PQLToken> nextTokens;
    public PQLToken startToken;
    public Map <PQLToken, CastInfo > castInfos;
    public PQLToken currentBuildingToken;
    public ParallelMode buildParallel;

    public boolean debugMode;
    public int debugLine;
    public String debugSource;
    public int debugId;
    public static Map <Integer, SkeletonBuilder> debugSkeletonBuilder = new HashMap ();

    public SkeletonBuilder (ParallelMode _buildParallel) {
        buildParallel = _buildParallel;
        pqlArguments = new ArrayList <PQLArgument> ();
        //first index is reserved for Env-argument of method, next two for the parallel-infos
        localIndexCount = (buildParallel == ParallelMode.Serial ? 1 : 3);
        /*reservedVariables = new ArrayList <Integer> ();
        reservedVariables.add(0);*/
        reservedVariables = new HashMap <PQLToken, List <Integer> > ();
        reservedVariableCount = 0;
        iterationLabels = new HashMap <PQLToken, Label> ();
        initLabels = new HashMap <PQLToken, Label> ();
        nextTokens = new ArrayList <PQLToken> ();
        castInfos = new HashMap <PQLToken, CastInfo> ();
        currentBuildingToken = null;
    }

    public SkeletonBuilder (ParallelMode _buildParallel, boolean fromAdditionalInfo) {
        this(_buildParallel);
        if (fromAdditionalInfo)
            localIndexCount = 3;
    }

    public byte [] build (PQLToken _startToken, Env _env, Map <Integer, Class> _knownInstances, int creationFlags) throws Throwable {

        /*if (_startToken != null)
            return BuildBytecode.build("D:/test.txt", true);*/

        if ((creationFlags & RCInterface.LINE_DEBUG) != 0) {
            debugMode = true;
            debugLine = -1;
            debugSource = "";
            int highestDebugId = -1;
            for (Integer key : debugSkeletonBuilder.keySet())
                if (key > highestDebugId)
                    highestDebugId = key;
            debugId = highestDebugId + 1;
            debugSkeletonBuilder.put(debugId, this);
            if (buildParallel == ParallelMode.Parallel)
                throw new RuntimeCreatorException("Line-debug is not allowe in parallel-mode 'parallel'.");
        }

        startToken = _startToken;
        knownInstances = _knownInstances;
        if (BenchmarkOptions.antiOptimizedInstance)
            knownInstances = new HashMap <Integer, Class> ();
        env = _env;

        for (PQLArgument arg : pqlArguments) {
            if (!arg.isConst || arg.type == TYPE_OBJECT) {
                arg.localIndex = localIndexCount;
                localIndexCount += ((arg.type == TYPE_LONG || arg.type == TYPE_DOUBLE) ? 2 : 1);
            }
        }

        ClassWriter cv;
        ClassVisitor cw = null;
        if ((creationFlags & RCInterface.BYTECODE_OUTPUT) == 0) {
            cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
            cw = cv;
        } else {
            cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
            PrintWriter pw = new PrintWriter(System.out);
            cw = new TraceClassVisitor(cv, pw);
        }
        FieldVisitor fv;
        MethodVisitor mv;
        AnnotationVisitor av0;

        cw.visit(V1_6, ACC_PUBLIC, "Evaluator", null, "java/lang/Object", null);

        cw.visitSource("Evaluator.java", null);

        {
            mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(1, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            mv.visitInsn(RETURN);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLocalVariable("this", "LEvaluator;", null, l0, l1, 0);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        {
            if (buildParallel == ParallelMode.Serial)
                mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "evaluate", "(Ledu/umass/pql/Env;)Z", null, null);
            else
                mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "evaluate", "(Ledu/umass/pql/Env;II)Z", null, null);

            if ((creationFlags & RCInterface.DETAILED_DEBUG) != 0 && (creationFlags & RCInterface.OPTIMIZE) != 0)
                throw new RuntimeCreatorException("flags 'DETAILED_DEBUG' and 'OPTIMIZE' are currently not supported, if they are both active.");

            if ((creationFlags & RCInterface.DETAILED_DEBUG) != 0)
                mv = new DebugMethodVisitor(mv);
            if ((creationFlags & RCInterface.OPTIMIZE) != 0)
                mv = new OptimizeVisitor(mv);
            mv.visitCode();

            for (PQLArgument arg : pqlArguments) {
                //there are objects with index v_object.length used temporary
                if (arg.type == TYPE_OBJECT && arg.index >= env.v_object.length)
                    continue;
                if (!arg.isConst || arg.type == TYPE_OBJECT) {
                    mv.visitVarInsn(ALOAD, 0);
                    mv.visitFieldInsn(GETFIELD, "edu/umass/pql/Env", FlagUtils.getEnvArrayName(arg.type), "[" + FlagUtils.getTypeSpecifier(arg.type));
                    SubCommands.constLoadRuntime(mv, TYPE_INT, new Integer(arg.index));
                    mv.visitInsn( FlagUtils.getArrayInstruction(TYPE_LOAD, arg.type) );
                    if (arg.type == TYPE_OBJECT) {
                        if (knownInstances.containsKey(arg.index))
                            mv.visitTypeInsn(CHECKCAST, knownInstances.get(arg.index).getName().replace(".", "/"));
                        else if (arg.isConst)
                            throw new RuntimeCreatorException("constant objects has to have an specified instance-type.");
                    }
                    mv.visitVarInsn(FlagUtils.getPushStoreCommand(TYPE_STORE, arg.type), arg.localIndex);
                }
            }

            endLabel = new Label();
            for (PQLToken token : startToken.getAllChilds())
                if (!token.isControlStructure() && !token.isReductor()) {
                    Label newLineNumberLabel = new Label();
                    mv.visitLabel(newLineNumberLabel);
                    mv.visitLineNumber(token.getInternalId()+1, newLineNumberLabel);
                    buildToken(mv, this, token, 0, knownInstances, env);
                }

            /*nextTokens.addAll(startToken.getInitiatingReductors());
            while (nextTokens.size() > 0) {
                gen_MainCommands.buildToken(mv, this, nextTokens.get(0), 0, knownInstances, env);
                nextTokens.remove(0);
            }*/

            //nextTokens.add(startToken.getNextCreateLinearToken());
            //nextTokens.add(startToken.childs.get(0));
            //the 'insert-point', the structure is: ConjuctiveBlock (TRUE(<= insert-point), [CODE])
            insertReturnBehavior (mv, startToken.childs.get(0), true);

            while (nextTokens.size() > 0) {
                if (!initLabels.containsKey(nextTokens.get(0)))
                    initLabels.put(nextTokens.get(0), new Label());
                mv.visitLabel(initLabels.get(nextTokens.get(0)));
                //if (!nextTokens.get(0).isReductor())
                //    for (PQLToken initiatedReductor : nextTokens.get(0).getInitiatingReductors())
                //        gen_MainCommands.buildToken(mv, this, initiatedReductor, 0, knownInstances, env);
                mv.visitLineNumber(nextTokens.get(0).getInternalId()+1, initLabels.get(nextTokens.get(0)));
                buildToken(mv, this, nextTokens.get(0), 1, knownInstances, env);
                nextTokens.remove(0);
            }
            mv.visitLabel(endLabel);

            for (PQLArgument arg : pqlArguments) {
                //there are objects with index v_object.length used temporary
                if (arg.type == TYPE_OBJECT && arg.index >= env.v_object.length)
                    continue;
                if (!arg.isConst || arg.type == TYPE_OBJECT) {
                    mv.visitVarInsn(ALOAD, 0);
                    mv.visitFieldInsn(GETFIELD, "edu/umass/pql/Env", FlagUtils.getEnvArrayName(arg.type), "[" + FlagUtils.getTypeSpecifier(arg.type));
                    SubCommands.constLoadRuntime(mv, TYPE_INT, new Integer(arg.index));
                    mv.visitVarInsn(FlagUtils.getPushStoreCommand(TYPE_LOAD, arg.type), arg.localIndex);
                    mv.visitInsn( FlagUtils.getArrayInstruction(TYPE_STORE, arg.type) );
                }
            }
            mv.visitInsn(IRETURN);

            mv.visitMaxs(0, 0);
            mv.visitEnd();
            if (mv instanceof OptimizeVisitor)
                ((OptimizeVisitor)mv).apply();
        }
        cw.visitEnd();

        byte [] ret = cv.toByteArray();
        return ret;

    }

    public void argumentInUse (int id, Env env) {
        PQLArgument.argumentInUse(pqlArguments, id, this, env);
    }

    public PQLArgument findArgument (int id) {
        for (PQLArgument arg : pqlArguments)
            if (arg.index == (id >> VAR_INDEX_SHIFT) && arg.type == ((id >> VAR_TABLE_SHIFT) & 0x7) )
                return arg;
        return null;
    }

    public int matchArgumentTypes (int ... types) throws RuntimeCreatorException {
        int returnType = -1;
        for (int type : types) {
            if (returnType != -1 && returnType != type)
                throw new RuntimeCreatorException("some arguments have incompatible types. ('" + FlagUtils.getTypeName(type) + "' and '" + FlagUtils.getTypeName(returnType) + "')");
            else
                returnType = type;
        }
        return returnType;
    }

    public void reserveNewVariables (PQLToken token, int amount) {
        List <Integer> newIndexList = new ArrayList <Integer> ();
        for (int i=0; i<amount; i++)
            newIndexList.add(-1);
        reservedVariables.put(token, newIndexList);
        /*int largestIndex = 0;
        for (Integer index : reservedVariables.values())
            if (index > largestIndex)
                largestIndex = index;
        reservedVariables.put(token, largestIndex+amount);*/
        //reservedVariables.add(reservedVariables.get(reservedVariables.size()-1) + amount);
    }

    public int getReservedVariable (PQLToken token, int offset, int type) {
        //return reservedVariables.get(reservedVariables.size()-2) + index;
        //return reservedVariables.get(reservedVariables.size()-2) + index + localIndexCount;
        if (reservedVariables.get(token).get(offset) == -1) {
            reservedVariables.get(token).set(offset, reservedVariableCount);
            reservedVariableCount += ((type == TYPE_LONG || type == TYPE_DOUBLE) ? 2 : 1);
        }
        return reservedVariables.get(token).get(offset) + localIndexCount;
        /*int secondLargestIndex = 0;
        for (Integer index : reservedVariables.values())
            if (index > secondLargestIndex && index < reservedVariables.get(token))
                secondLargestIndex = index;
        return secondLargestIndex + offset + localIndexCount;*/
    }

    public void registerIterationLabel (PQLToken token) {
        iterationLabels.put(token, new Label());
    }

    public Label startIterationLabel (MethodVisitor mv, PQLToken token) {
        Label visit = iterationLabels.get(token);
        mv.visitLabel(visit);
        return visit;
    }

    public void insertInnerReductor (MethodVisitor mv, PQLToken token) throws Throwable {
        buildToken(mv, this, token.innerReductor, 1, knownInstances, env);
    }

    public void insertReturnBehavior (MethodVisitor mv, PQLToken token, boolean returnResult) throws Throwable {

        PQLReturnBehavior returnBehavior = (returnResult ? token.trueBehavior : token.falseBehavior);

        if (returnBehavior.insertInitReductors.size() > 0) {
            Object [] insertInitReductorsArray = returnBehavior.insertInitReductors.toArray();
            for (int i=0; i<insertInitReductorsArray.length; i++) {
                Label newLineNumberLabel = new Label();
                mv.visitLabel(newLineNumberLabel);
                mv.visitLineNumber( ((PQLToken)insertInitReductorsArray[i]).getInternalId()+1, newLineNumberLabel);
                buildToken(mv, this, (PQLToken)insertInitReductorsArray[i], 0, knownInstances, env);
            }
        }
        if (returnBehavior.insertReductors.size() > 0) {
            Object [] insertReductorsArray = returnBehavior.insertReductors.toArray();
            for (int i=0; i<insertReductorsArray.length; i++) {
                Label newLineNumberLabel = new Label();
                mv.visitLabel(newLineNumberLabel);
                mv.visitLineNumber( ((PQLToken)insertReductorsArray[i]).getInternalId()+1, newLineNumberLabel);
                buildToken(mv, this, (PQLToken)insertReductorsArray[i], 1, knownInstances, env);
            }
        }

        if ( returnBehavior.isFinalReturn() ) {
            SubCommands.constLoadRuntime(mv, TYPE_INT, new Integer( returnBehavior.finalReturn ));
            mv.visitJumpInsn(GOTO, endLabel);
        } else {
            PQLToken jumpTo = returnBehavior.jumpTo;
            if (returnBehavior.jumpToIterationLabel)
                mv.visitJumpInsn(GOTO, iterationLabels.get(jumpTo));
            else {
                if (!initLabels.containsKey(jumpTo)) {
                    initLabels.put(jumpTo, new Label());
                    nextTokens.add(jumpTo);
                    //currently not active, because this is just valid, if return is at the end of the code-snippet
                    //position 0: current Token, 1: next Token => if we have size 2, the next token is following and we don't need a goto
                    //if (nextTokens.size() != 2)
                    //    mv.visitJumpInsn(GOTO, initLabels.get(jumpTo));
                    mv.visitJumpInsn(GOTO, initLabels.get(jumpTo));
                } else
                    mv.visitJumpInsn(GOTO, initLabels.get(jumpTo));
            }
        }
    }

    private static void buildToken (MethodVisitor mv, SkeletonBuilder skeletonBuilder, PQLToken token, int translateStep, Map <Integer, Class> typeInformations, Env env) throws Throwable {
        skeletonBuilder.currentBuildingToken = token;
        ExecGenSourcecode.mainCommands_buildToken(mv, skeletonBuilder, token, translateStep, typeInformations, env);
        skeletonBuilder.currentBuildingToken = null;
    }

    public void castObjectIfNecessary (MethodVisitor mv, PQLToken token, int argumentIndex, String toClass) throws ClassNotFoundException {
        String toClassInternal = toClass.replace("/", ".");
        Map <PQLToken, VisitState> visitMap = new HashMap <PQLToken, VisitState>();
        fillVisitMap(startToken.childs.get(0), token, new HashSet <PQLToken>(), new ArrayList <PQLToken> (), visitMap, new Boolean(true));

        boolean casted = false, wrongCasted = false;
        for (PQLToken key : visitMap.keySet())
            if (visitMap.get(key) == VisitState.Always && castInfos.containsKey(key) && castInfos.get(key).containsSubclass(new PQLArgument(token.arguments.get(argumentIndex)), Class.forName(toClassInternal)))
                casted = true;
        for (PQLToken key : castInfos.keySet())
            if (castInfos.get(key).containsWrongSubclass(new PQLArgument(token.arguments.get(argumentIndex)), Class.forName(toClassInternal)) )
                wrongCasted = true;

        if (!casted && !wrongCasted) {
            for (PQLArgument arg : pqlArguments) {
                if (arg.equals(new PQLArgument(token.arguments.get(argumentIndex))) && knownInstances.containsKey(arg.index)) {
                    if (Class.forName(toClassInternal).isAssignableFrom(knownInstances.get(arg.index)))
                        casted = true;
                    else
                        wrongCasted = true;
                    break;
                }
            }
        }

        if (!(casted && !wrongCasted))
            mv.visitTypeInsn(CHECKCAST, toClass);
    }

    public void registerObjectCast(PQLToken token, int argumentIndex, String toClass) {
        /*try {
            System.out.println(startToken.toString());
            System.out.println("Y: " + token.getInternalId() + ", " + toClass);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        if (!castInfos.containsKey(token))
            castInfos.put(token, new CastInfo());
        castInfos.get(token).add(toClass.replace("/", "."), new PQLArgument(token.arguments.get(argumentIndex)));
    }

    public void fillVisitMap (PQLToken current, PQLToken to, Set <PQLToken> visited, List <PQLToken> visitInstantQueue, Map <PQLToken, VisitState> visitMap, Boolean firstEnd) {
        if (current.equals(to)) {
            for (PQLToken token : visitMap.keySet())
                if (!visited.contains(token))
                    visitMap.put(token, VisitState.Sometimes);
            for (PQLToken visitedToken : visited) {
                if (visitedToken.equals(to))
                    continue;
                if (!visitMap.containsKey(visitedToken))
                    visitMap.put(visitedToken, (firstEnd ? VisitState.Always : VisitState.Sometimes) );
            }
            firstEnd = false;
        }
        if (visited.contains(current))
            return;
        Set <PQLToken> copySet = new HashSet <PQLToken>();
        for (PQLToken visitedToken : visited)
            copySet.add(visitedToken);
        copySet.add(current);
        List <PQLToken> visitInstantQueueCopy = new ArrayList <PQLToken> ();
        for (int i=1; i<visitInstantQueue.size(); i++)
            visitInstantQueueCopy.add(visitInstantQueue.get(i));
        if (visitInstantQueue.size() > 0)
            fillVisitMap(visitInstantQueue.get(0), to, copySet, visitInstantQueueCopy, visitMap, firstEnd);
        else if (!current.isReductor()) {
            for (int i=0; i<2; i++) {
                PQLReturnBehavior returnBehavior = (i == 0 ? current.trueBehavior : current.falseBehavior);
                if (returnBehavior.insertReductors.size() > 0) {
                    Object [] returnBehaviorReductors = returnBehavior.insertReductors.toArray();
                    for (int j=1; j<returnBehaviorReductors.length; j++)
                        visitInstantQueueCopy.add((PQLToken)returnBehaviorReductors[j]);
                    if (!returnBehavior.isFinalReturn())
                        visitInstantQueueCopy.add(returnBehavior.jumpTo);
                    fillVisitMap((PQLToken)returnBehaviorReductors[0], to, copySet, visitInstantQueueCopy, visitMap, firstEnd);
                } else if (!returnBehavior.isFinalReturn())
                    fillVisitMap(returnBehavior.jumpTo, to, copySet, visitInstantQueueCopy, visitMap, firstEnd);
            }
        }

    }

    public static void feedbackLine (int id, int line) {
        debugSkeletonBuilder.get(id).debugLine = line;
    }

    public static void feedbackSource (int id, String src) {
        debugSkeletonBuilder.get(id).debugSource = src;
    }

}