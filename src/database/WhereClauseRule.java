/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database;

import edu.umass.pql.Env;
import edu.umass.pql.VarConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import rc2.representation.PQLExtension;
import replacetool.AbstractRule;

/**
 *
 * @author chriamue
 */
public class WhereClauseRule extends AbstractRule implements VarConstants {

    @Override
    public RuleType getRuleType() {
        return RuleType.JoinJoin;
    }

    @Override
    public boolean isLeftCommand(Command command) {
        return ((command.getJoin() instanceof PQLExtension)
                && (command.getJoin().getName().equals("mysql") || command.getJoin().getName().equals("postgresql")
                || command.getJoin().getName().equals("sqlite")));

    }

    @Override
    public boolean canReplace(Command left, Command right) {
        if (!(left.getJoin() instanceof PQLExtension)
                && (left.getJoin().getName().equals("mysql") || left.getJoin().getName().equals("postgresql")
                || left.getJoin().getName().equals("sqlite"))) {
            return false;
        }

        fieldComp(left, right);
        fieldContains(left, right);

        return false;
    }

    @Override
    public Command replaceCommands(Command left, Command right) {
        return null;
    }

    @Override
    public Map<EnvContentType, List<Object>> getEnvExtension(Command left, Command right) {
        return new HashMap();
    }

    private void fieldComp(Command left, Command right) {
        if ((right.getJoin() instanceof PQLExtension) && (right.getJoin().getName().startsWith("fieldlt_")
                || right.getJoin().getName().startsWith("fieldlteq_")
                || right.getJoin().getName().startsWith("fieldgt_")
                || right.getJoin().getName().startsWith("fieldgteq_")
                || right.getJoin().getName().startsWith("fieldeq_"))
                && (Env.varIndex((left.getJoin()).getArg(left.getJoin().getName().equals("sqlite") ? 4 : 7)) == Env.varIndex((right.getJoin()).getArg(0))
                && Env.varType((left.getJoin()).getArg(left.getJoin().getName().equals("sqlite") ? 4 : 7)) == Env.varType((right.getJoin()).getArg(0)))) {

            String currentwhereclause = (String) left.getEnv().getObject(left.getJoin().getArg(left.getJoin().getName().equals("sqlite") ? 3 : 6));
            String whereclause = "";
            switch (right.getJoin().getName()) {
                case "fieldlt_int":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " < "
                            + String.valueOf(right.getEnv().getInt(right.getJoin().getArg(2)));
                    break;
                case "fieldlt_long":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " < "
                            + String.valueOf(right.getEnv().getLong(right.getJoin().getArg(2)));
                    break;
                case "fieldlt_double":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " < "
                            + String.valueOf(right.getEnv().getDouble(right.getJoin().getArg(2)));
                    break;
                case "fieldlteq_int":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " <= "
                            + String.valueOf(right.getEnv().getInt(right.getJoin().getArg(2)));
                    break;
                case "fieldlteq_long":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " <= "
                            + String.valueOf(right.getEnv().getLong(right.getJoin().getArg(2)));
                    break;
                case "fieldlteq_double":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " <= "
                            + String.valueOf(right.getEnv().getDouble(right.getJoin().getArg(2)));
                    break;
                case "fieldgt_int":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " > "
                            + String.valueOf(right.getEnv().getInt(right.getJoin().getArg(2)));
                    break;
                case "fieldgt_long":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " > "
                            + String.valueOf(right.getEnv().getLong(right.getJoin().getArg(2)));
                    break;
                case "fieldgt_double":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " > "
                            + String.valueOf(right.getEnv().getDouble(right.getJoin().getArg(2)));
                    break;
                case "fieldgteq_int":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " >= "
                            + String.valueOf(right.getEnv().getInt(right.getJoin().getArg(2)));
                    break;
                case "fieldgteq_long":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " >= "
                            + String.valueOf(right.getEnv().getLong(right.getJoin().getArg(2)));
                    break;
                case "fieldgteq_double":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " >= "
                            + String.valueOf(right.getEnv().getDouble(right.getJoin().getArg(2)));
                    break;
                case "fieldeq_int":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " = "
                            + String.valueOf(right.getEnv().getInt(right.getJoin().getArg(2)));
                    break;
                case "fieldeq_long":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " = "
                            + String.valueOf(right.getEnv().getLong(right.getJoin().getArg(2)));
                    break;
                case "fieldeq_double":
                    whereclause = (String) right.getEnv().getObject(right.getJoin().getArg(1))
                            + " = "
                            + String.valueOf(right.getEnv().getDouble(right.getJoin().getArg(2)));
                    break;
                case "fieldeq_String":
                    whereclause = (left.getJoin().getName().equals("postgresql")
                            ? "\"" + (String) right.getEnv().getObject(right.getJoin().getArg(1)) + "\""
                            : (String) right.getEnv().getObject(right.getJoin().getArg(1)))
                            + " like '"
                            + (String) right.getEnv().getObject(right.getJoin().getArg(2))
                            + "'";
                    break;
                default:
                    whereclause = "";
            }
            if (!currentwhereclause.contains(whereclause)) {
                if (currentwhereclause.isEmpty()) {
                    currentwhereclause = whereclause;
                } else {
                    currentwhereclause = currentwhereclause + " and " + whereclause;
                }
                left.getEnv().setObject(left.getJoin().getArg(left.getJoin().getName().equals("sqlite") ? 3 : 6), currentwhereclause);
            }
        }
    }

    private void fieldContains(Command left, Command right) {
        if ((right.getJoin() instanceof PQLExtension) && (right.getJoin().getName().startsWith("fieldcontains_String"))
                && (Env.varIndex((left.getJoin()).getArg(left.getJoin().getName().equals("sqlite") ? 4 : 7)) == Env.varIndex((right.getJoin()).getArg(0))
                && Env.varType((left.getJoin()).getArg(left.getJoin().getName().equals("sqlite") ? 4 : 7)) == Env.varType((right.getJoin()).getArg(0)))) {

            String currentwhereclause = (String) left.getEnv().getObject(left.getJoin().getArg(left.getJoin().getName().equals("sqlite") ? 3 : 6));
            String whereclause = "";
            whereclause = (left.getJoin().getName().equals("postgresql")
                    ? "\"" + (String) right.getEnv().getObject(right.getJoin().getArg(1)) + "\""
                    : (String) right.getEnv().getObject(right.getJoin().getArg(1)))
                    + " like '%"
                    + (String) right.getEnv().getObject(right.getJoin().getArg(2))
                    + "%'";
            if (!currentwhereclause.contains(whereclause)) {
                if (currentwhereclause.isEmpty()) {
                    currentwhereclause = whereclause;
                } else {
                    currentwhereclause = currentwhereclause + " and " + whereclause;
                }
                left.getEnv().setObject(left.getJoin().getArg(left.getJoin().getName().equals("sqlite") ? 3 : 6), currentwhereclause);
            }
        }
    }

}
