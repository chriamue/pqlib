/***************************************************************************
 Copyright (C) 2014 chriamue

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/
package database;

import rc2.GenericInterface;
import rc2.bcr.BCRExtension;

/**
 *
 * @author chriamue
 */
public class FieldCompGenericInterface implements GenericInterface {

    @Override
    public String[] getGenerics() {
        return new String[]{"operator", "type"};
    }

    @Override
    public String getGenericContent(BCRExtension extension, String name) {
        if (name.equals("operator")) {
            return ((String[]) extension.getGenericInformation())[0];
        } else {
            return ((String[]) extension.getGenericInformation())[1];
        }
    }

}
