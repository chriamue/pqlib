/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import rc2.ExtensionInterface;
import rc2.bcr.BCRExtension;
import rc2.bcr.MethodTranslate;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author chriamue
 */
public class DatabaseExtension extends ExtensionInterface {

    public static boolean eq(String a, String b) {
        return a.equals(b);
    }

    public static boolean eq(int a, int b) {
        return a == b;
    }

    public static boolean eq(long a, long b) {
        return a == b;
    }

    public static boolean eq(double a, double b) {
        return a == b;
    }

    public static boolean contains(String a, String b) {
        return a.contains(b);
    }

    @Override
    protected void defineExtensions() throws RuntimeCreatorException {
        bcrExtensions.add(new BCRExtension("database", "db_database", 3, BCRExtension.ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("mysql", "db_mysql", 8, BCRExtension.ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("postgresql", "db_postgresql", 8, BCRExtension.ExtensionType.Join));
        bcrExtensions.add(new BCRExtension("sqlite", "db_sqlite", 5, BCRExtension.ExtensionType.Join));

        bcrExtensions.add(new BCRExtension("like_String", "db_like", 2, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"String"}));
        bcrExtensions.add(new BCRExtension("like_int", "db_like", 2, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"int"}));
        bcrExtensions.add(new BCRExtension("contains_String", "db_contains", 2, BCRExtension.ExtensionType.Join));

        bcrExtensions.add(new BCRExtension("field_String", "db_field", 3, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"String"}));
        bcrExtensions.add(new BCRExtension("field_int", "db_field", 3, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"int"}));
        bcrExtensions.add(new BCRExtension("field_long", "db_field", 3, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"long"}));
        bcrExtensions.add(new BCRExtension("field_double", "db_field", 3, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"double"}));

        bcrExtensions.add(new BCRExtension("fieldlt_int", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"<", "int"}));
        bcrExtensions.add(new BCRExtension("fieldlt_long", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"<", "long"}));
        bcrExtensions.add(new BCRExtension("fieldlt_double", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"<", "double"}));

        bcrExtensions.add(new BCRExtension("fieldlteq_int", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"<=", "int"}));
        bcrExtensions.add(new BCRExtension("fieldlteq_long", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"<=", "long"}));
        bcrExtensions.add(new BCRExtension("fieldlteq_double", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"<=", "double"}));

        bcrExtensions.add(new BCRExtension("fieldgt_int", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{">", "int"}));
        bcrExtensions.add(new BCRExtension("fieldgt_long", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{">", "long"}));
        bcrExtensions.add(new BCRExtension("fieldgt_double", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{">", "double"}));

        bcrExtensions.add(new BCRExtension("fieldgteq_int", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{">=", "int"}));
        bcrExtensions.add(new BCRExtension("fieldgteq_long", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{">=", "long"}));
        bcrExtensions.add(new BCRExtension("fieldgteq_double", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{">=", "double"}));

        bcrExtensions.add(new BCRExtension("fieldeq_int", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"==", "int"}));
        bcrExtensions.add(new BCRExtension("fieldeq_long", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"==", "long"}));
        bcrExtensions.add(new BCRExtension("fieldeq_double", "db_fieldcomp", 4, BCRExtension.ExtensionType.Join, FieldCompGenericInterface.class, new String[]{"==", "double"}));
        bcrExtensions.add(new BCRExtension("fieldeq_String", "db_fieldlike", 4, BCRExtension.ExtensionType.Join, FieldGenericInterface.class, new String[]{"String"}));
        bcrExtensions.add(new BCRExtension("fieldcontains_String", "db_fieldcontains", 4, BCRExtension.ExtensionType.Join));

    }

    @Override
    protected void defineConstructors() {
    }

    @Override
    protected void defineMethods() throws RuntimeCreatorException {
        methods.add(new MethodTranslate("format", "format", String.class, new Class[]{String.class, Object[].class}, String.class));
        methods.add(new MethodTranslate("equals", "equals", String.class, new Class[]{Object.class}, boolean.class));
        methods.add(new MethodTranslate("contains_String", "contains", DatabaseExtension.class, new Class[]{String.class, String.class}, boolean.class));

        methods.add(new MethodTranslate("getConnection", "getConnection", DriverManager.class, new Class[]{String.class}, Connection.class));

        methods.add(new MethodTranslate("createStatement", "createStatement", Connection.class, new Class[]{}, Statement.class));

        methods.add(new MethodTranslate("executeQuery", "executeQuery", Statement.class, new Class[]{String.class}, ResultSet.class));

        methods.add(new MethodTranslate("close_connection", "close", Connection.class, new Class[]{}, void.class));
        methods.add(new MethodTranslate("close_statement", "close", Statement.class, new Class[]{}, void.class));

        methods.add(new MethodTranslate("next_object", "next", ResultSet.class, new Class[]{}, boolean.class));
        methods.add(new MethodTranslate("str_replace", "replaceAll", String.class, new Class[]{String.class, String.class}, String.class));

        methods.add(new MethodTranslate("get_String", "getString", ResultSet.class, new Class[]{String.class}, String.class));
        methods.add(new MethodTranslate("get_int", "getInt", ResultSet.class, new Class[]{String.class}, int.class));
        methods.add(new MethodTranslate("get_long", "getLong", ResultSet.class, new Class[]{String.class}, long.class));
        methods.add(new MethodTranslate("get_double", "getDouble", ResultSet.class, new Class[]{String.class}, double.class));

        methods.add(new MethodTranslate("equals_int", "eq", DatabaseExtension.class, new Class[]{int.class, int.class}, boolean.class));
        methods.add(new MethodTranslate("equals_long", "eq", DatabaseExtension.class, new Class[]{long.class, long.class}, boolean.class));
        methods.add(new MethodTranslate("equals_double", "eq", DatabaseExtension.class, new Class[]{double.class, double.class}, boolean.class));
        methods.add(new MethodTranslate("equals_String", "eq", DatabaseExtension.class, new Class[]{String.class, String.class}, boolean.class));
    }

    @Override
    protected void defineMethodAliases() {
    }

}
