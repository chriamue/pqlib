/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database.tests;

import benchmarks.SQL;
import database.DatabaseExtension;
import database.FieldCompRule;
import database.SelectFromRule;
import database.WhereClauseRule;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_DOUBLE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarInfo;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import rc2.RCInterface;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;
import replacetool.AbstractRule;
import replacetool.ReplaceTool;

/**
 *
 * @author chmuelle
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SQLBonusBenchTest {

    public SQLBonusBenchTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("org.postgresql.Driver");
            Class.forName("org.sqlite.JDBC");
        } catch (Exception e) {
            e.printStackTrace();
        }
        benchmarks.bonus.Generator g = new benchmarks.bonus.Generator();
        g.init(2);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void test00createDB() {
        benchmarks.bonus.Generator.install_sql(SQL.MYSQL);
        benchmarks.bonus.Generator.install_sql(SQL.POSTGRESQL);
    }

    @Test
    public void test01printEmployee() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final Object oDatabasetype = "mysql";
        final Object oHost = "localhost";
        final Object oDatabase = "test";
        final Object oUser = "test";
        final Object oPassword = "test";
        final Object oTable1 = "bonus_employees";
        final Object oTable2 = "bonus_boni";
        final Object oColumns1 = "";
        final Object oColumns2 = "";
        final Object oWhereClause1 = "";
        final Object oWhereClause2 = "";
        final Object oColumn1 = "eid";
        final Object oColumn2 = "descr";
        final Object oValue = "b7";//"b5957";

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            ex.printStackTrace();
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, oDatabasetype);
        env.setObject(iHost, oHost);
        env.setObject(iDatabase, oDatabase);
        env.setObject(iUser, oUser);
        env.setObject(iPassword, oPassword);
        env.setObject(iTable1, oTable1);
        env.setObject(iTable2, oTable2);

        env.setObject(iColumns1, oColumns1);
        env.setObject(iColumns2, oColumns2);
        env.setObject(iWhereClause1, oWhereClause1);
        env.setObject(iWhereClause2, oWhereClause2);
        env.setObject(iValue, oValue);
        env.setObject(iColumn1, oColumn1);
        env.setObject(iColumn2, oColumn2);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_INT, 12);
        final int i3 = Env.encodeVar(TYPE_INT, 13);
        final int i4 = Env.encodeVar(TYPE_OBJECT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        /*
         inst.put(11, ResultSet.class);
         inst.put(12, int.class);
         inst.put(13, int.class);
         inst.put(14, String.class);
         inst.put(15, ResultSet.class);
         */
        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i3),
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i5),
                new PQLExtension("field_int", i5, iColumn1, i2),
                //new PQLExtension("like_int", i3, i2),
                PQLFactory.EQ_Int(i3, i2),
                new PQLExtension("field_String", i5, iColumn2, i4)
        );

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);
        rc.build(p, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(p);
        assertTrue(rc.exec(env));

        System.out.println("Test 1: eid: " + env.getInt(i2) + " eid_2: " + env.getInt(i3) + " descr: " + (String) env.getObject(i4));
    }

    //@Test
    public void test02printEmployeeOptimized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final Object oDatabasetype = "mysql";
        final Object oHost = "localhost";
        final Object oDatabase = "test";
        final Object oUser = "test";
        final Object oPassword = "test";
        final Object oTable1 = "bonus_employees";
        final Object oTable2 = "bonus_boni";
        final Object oTable3 = "bonus_departments";
        final Object oColumns1 = "";
        final Object oColumns2 = "";
        final Object oColumns3 = "";
        final Object oWhereClause1 = "";
        final Object oWhereClause2 = "";
        final Object oWhereClause3 = "";
        final Object oColumn1 = "eid";
        final Object oColumn2 = "descr";
        final Object oValue = "b7";//"b5957";

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, oDatabasetype);
        env.setObject(iHost, oHost);
        env.setObject(iDatabase, oDatabase);
        env.setObject(iUser, oUser);
        env.setObject(iPassword, oPassword);
        env.setObject(iTable1, oTable1);
        env.setObject(iTable2, oTable2);
        env.setObject(iTable3, oTable3);

        env.setObject(iColumns1, oColumns1);
        env.setObject(iColumns2, oColumns2);
        env.setObject(iColumns3, oColumns3);
        env.setObject(iWhereClause1, oWhereClause1);
        env.setObject(iWhereClause2, oWhereClause2);
        env.setObject(iWhereClause3, oWhereClause3);
        env.setObject(iValue, oValue);
        env.setObject(iColumn1, oColumn1);
        env.setObject(iColumn2, oColumn2);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_INT, 12);
        final int i3 = Env.encodeVar(TYPE_INT, 13);
        final int i4 = Env.encodeVar(TYPE_OBJECT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);

        final int i6 = Env.encodeReadVar(TYPE_INT, 16);
        env.setInt(i6, 42);
        constVars.insert(Env.encodeReadVar(TYPE_INT, 16));
        inst.put(16, int.class);
        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i5),
                new PQLExtension("field_int", i5, iColumn1, i3),
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i2),
                //PQLFactory.EQ_Int(i6, i2),
                new PQLExtension("like_int", i2, i6),
                
                PQLFactory.EQ_Int(i3, i2),
                new PQLExtension("field_String", i5, iColumn2, i4)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        //ReplaceTool.ReplaceResult res = rt.replace(p, env, rules);
        //System.out.println(res.getJoin());
        rc.build(p, env, inst, RCInterface.ParallelMode.Serial);

        assertTrue(rc.exec(env));

        System.out.println("Test 1: eid: " + env.getInt(i2) + " eid_2: " + env.getInt(i3) + " descr: "
                + (String) env.getObject(i4));
    }

    @Test
    public void test03Manual() throws SQLException {
        Set<Integer> employees = new HashSet<Integer>();
        Map<Integer, Integer> employeesDepartment = new HashMap<Integer, Integer>();
        Map<Integer, Float> departmentsFactor = new HashMap<Integer, Float>();
        Map<Integer, Set<Float>> boniBase = new HashMap<Integer, Set<Float>>();
        Map<Integer, Set<String>> boniDescription = new HashMap<Integer, Set<String>>();
        Map<Integer, Float> bonus = new HashMap<Integer, Float>();

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from bonus_employees");
        while (resultSet.next()) {
            Integer eid = resultSet.getInt("eid");
            Integer dept = resultSet.getInt("dept");
            employeesDepartment.put(eid, dept);
            employees.add(eid);
        }

        resultSet = statement.executeQuery("select * from bonus_departments");
        while (resultSet.next()) {
            Integer did = resultSet.getInt("did");
            Float factor = resultSet.getFloat("factor");
            departmentsFactor.put(did, factor);
        }

        resultSet = statement.executeQuery("select * from bonus_boni");
        while (resultSet.next()) {
            Integer eid = resultSet.getInt("eid");
            String descr = resultSet.getString("descr");
            Float base = resultSet.getFloat("base");
            Set<Float> bd = boniBase.get(eid);
            if (bd == null) {
                bd = new HashSet<Float>();
            }
            bd.add(base);
            boniBase.put(eid, bd);

            Set<String> bB = boniDescription.get(eid);
            if (bB == null) {
                bB = new HashSet<String>();
            }
            bB.add(descr);
            boniDescription.put(eid, bB);
        }

        for (Integer employee : employees) {
            Float dF = departmentsFactor.get(employeesDepartment.get(employee));
            if (dF == null) {
                dF = 0f;
            }
            Set<Float> bB = boniBase.get(employee);
            Float vbB = 0f;
            if (bB != null) {
                for (Float f : bB) {
                    vbB += f;
                }
            }
            Float b = dF * vbB;
            bonus.put(employee, b);
        }

        for (Integer employee : employees) {
            System.out.println("Bonus for employee " + employee + " is " + bonus.get(employee) + " based on " + boniDescription.get(employee));
        }

    }

//    @Test
    public void test04ManualOptimized() throws SQLException {
        Set<Integer> employees = new HashSet<Integer>();
        Map<Integer, Integer> employeesDepartment = new HashMap<Integer, Integer>();
        Map<Integer, Float> departmentsFactor = new HashMap<Integer, Float>();
        Map<Integer, Float> boniBase = new HashMap<Integer, Float>();
        Map<Integer, String> boniDescription = new HashMap<Integer, String>();
        Map<Integer, Float> bonus = new HashMap<Integer, Float>();

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from bonus_employees");
        while (resultSet.next()) {
            Integer eid = resultSet.getInt("eid");
            Integer dept = resultSet.getInt("dept");
            employeesDepartment.put(eid, dept);
            employees.add(eid);
        }

        resultSet = statement.executeQuery("select * from bonus_departments");
        while (resultSet.next()) {
            Integer did = resultSet.getInt("did");
            Float factor = resultSet.getFloat("factor");
            departmentsFactor.put(did, factor);
        }

        resultSet = statement.executeQuery("select eid,GROUP_CONCAT(descr),SUM(base) from bonus_boni group by eid");
        while (resultSet.next()) {
            Integer eid = resultSet.getInt("eid");
            String descr = resultSet.getString(2);
            String base = resultSet.getString(3);
            boniBase.put(eid, Float.parseFloat(base));
            boniDescription.put(eid, descr);
        }

        for (Integer employee : employees) {
            Float dF = departmentsFactor.get(employeesDepartment.get(employee));
            if (dF == null) {
                dF = 0f;
            }
            Float bB = boniBase.get(employee);
            if (bB == null) {
                bB = 0f;
            }
            Float b = dF * bB;
            bonus.put(employee, b);
        }

        for (Integer employee : employees) {
            System.out.println("Bonus for employee " + employee + " is " + bonus.get(employee) + " based on " + boniDescription.get(employee));
        }
    }

//    @Test
    public void test05FullManualOptimized() throws SQLException {
        Set<Integer> employees = new HashSet<Integer>();
        Map<Integer, String> boniDescription = new HashMap<Integer, String>();
        Map<Integer, Float> bonus = new HashMap<Integer, Float>();

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
        Statement statement = connection.createStatement();

        // source: SQLBench
        ResultSet resultSet = statement.executeQuery("select bonus_employees.eid, SUM(bonus_boni.base * bonus_departments.factor),GROUP_CONCAT(bonus_boni.descr) from bonus_employees JOIN bonus_departments ON (bonus_departments.did = bonus_employees.dept) JOIN bonus_boni ON (bonus_boni.eid = bonus_employees.eid) GROUP BY bonus_employees.eid;");
        while (resultSet.next()) {
            Integer eid = resultSet.getInt("eid");
            String descr = resultSet.getString(3);
            String base = resultSet.getString(2);
            employees.add(eid);
            bonus.put(eid, Float.parseFloat(base));
            boniDescription.put(eid, descr);
        }

        for (Integer employee : employees) {
            System.out.println("Bonus for employee " + employee + " is " + bonus.get(employee) + " based on " + boniDescription.get(employee));
        }
    }

    @Test
    public void test06RuntimePQL2() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);

        final int employees = Env.encodeVar(TYPE_OBJECT, 51);
        final int employeesDepartment = Env.encodeVar(TYPE_OBJECT, 52);

        inst.put(51, PSet.class);
        inst.put(52, PMap.class);
        final int departmentsFactor = Env.encodeVar(TYPE_OBJECT, 53);
        final int boniBase = Env.encodeVar(TYPE_OBJECT, 54);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 55);

        Map<Integer, Set<String>> boniDescription = new HashMap<Integer, Set<String>>();

        Method sumMethod = null;
        for (int i = 0; i < RCInterface.class.getMethods().length; i++) {
            if (RCInterface.class.getMethods()[i].getName().contains("sumDouble")) {
                sumMethod = RCInterface.class.getMethods()[i];
            }
        }

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i4), employees)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("field_int", i1, iColumn2, i5)
        );

        Join j2 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i7), Env.readVar(i8), employeesDepartment),
            PQLFactory.Reductors.SET(Env.readVar(i7), employees)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i2),
                new PQLExtension("field_int", i2, iColumn1, i7),
                new PQLExtension("field_int", i2, iColumn2, i8)
        );
        /*
         Join j2 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i6), Env.readVar(i7), departmentsFactor)},
         new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i2),
         new PQLExtension("field_int", i2, iColumn3, i6),
         new PQLExtension("field_int", i2, iColumn4, i7));
         */
        Join j3 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.N_MAP(Env.readVar(i6), PQLFactory.Reductors.SET(i7, 0), boniBase)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_int", i2, iColumn1, i6),
                new PQLExtension("field_int", i2, iColumn5, i7));

        Join p = PQLFactory.DisjunctiveBlock(j2);

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();
        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);
        ReplaceTool.ReplaceResult res = rt.replace(p, env, rules);

        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Integer employee : (PSet<Integer>) env.getObject(employees)) {
            System.out.println("Bonus for employee " + employee
                    + " is " + ((PMap<Integer, Integer>) env.getObject(employeesDepartment)).get(employee));// + " based on " + boniDescription.get(employee));
        }

    }

    //@Test
    public void test06RuntimePQL2Optimized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_DOUBLE, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeWriteVar(TYPE_INT, 19);

        final int employees = Env.encodeVar(TYPE_OBJECT, 51);
        final int employeesDepartment = Env.encodeVar(TYPE_OBJECT, 52);
        final int departmentsFactor = Env.encodeVar(TYPE_OBJECT, 53);
        final int boniBase = Env.encodeVar(TYPE_OBJECT, 54);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 55);

        Map<Integer, Set<String>> boniDescription = new HashMap<Integer, Set<String>>();

        /*
         Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i4), Env.readVar(i5), employeesDepartment),
         PQLFactory.Reductors.SET(Env.readVar(i4), employees)},
         new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
         new PQLExtension("field_int", i1, iColumn1, i4),
         new PQLExtension("field_int", i1, iColumn2, i5)
         );

         Join j2 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i6), Env.readVar(i7), departmentsFactor)},
         new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i2),
         new PQLExtension("field_int", i2, iColumn3, i6),
         new PQLExtension("field_int", i2, iColumn4, i7));

         Join j3 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i8), Env.readVar(i9), boniBase)},
         new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i2),
         new PQLExtension("field_int", i2, iColumn3, i6),
         new PQLExtension("field_int", i2, iColumn4, i7));
         */
        Method sumMethod = null;
        for (int i = 0; i < RCInterface.class.getMethods().length; i++) {
            if (RCInterface.class.getMethods()[i].getName().contains("sumDouble")) {
                sumMethod = RCInterface.class.getMethods()[i];
            }
        }

        Join j4 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i4), Env.readVar(i5), bonus)
    //,PQLFactory.Reductors.SET(Env.readVar(i4), employees)
        },
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i5),
                //PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.METHOD_ADAPTER(sumMethod, Env.readVar(i6), i7)},

                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_int", i2, iColumn1, i4),
                new PQLExtension("like_int", i4, i5)
        //PQLFactory.EQ_Int(i5, i4)
        //new PQLExtension("field_double", i2, iColumn5, i6)
        //)
        );

        Join p = PQLFactory.ConjunctiveBlock(j4);//PQLFactory.DisjunctiveBlock(j4);

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();
        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);
        ReplaceTool.ReplaceResult res = rt.replace(p, env, rules);

        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Integer employee : (PSet<Integer>) env.getObject(employees)) {
            System.out.println("Bonus for employee " + employee
                    + " is " + ((PMap<Integer, Integer>) env.getObject(bonus)).get(employee));// + " based on " + boniDescription.get(employee));
        }
    }

    @Test
    public void test08MySQL() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);

        rc.build(j1, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }
    }

    @Test
    public void test08MySQLx10() throws Throwable {
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();
        test08MySQL();

    }

    @Test
    public void test08MySQLParallelized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);

        rc.build(j1, env, inst, RCInterface.ParallelMode.Parallel, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }
    }

    @Test
    public void test08MySQLParallelizedx10() throws Throwable {
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
        test08MySQLParallelized();
    }

    @Test
    public void test08MySQLOptimized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();
        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
        ReplaceTool.ReplaceResult res = rt.replace(j1, env, rules);

        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }

    }

    @Test
    public void test08MySQLOptimizedx10() throws Throwable {
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
        test08MySQLOptimized();
    }

    @Test
    public void test08MySQLOptimizedParallelized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();
        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
        ReplaceTool.ReplaceResult res = rt.replace(j1, env, rules);

        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Parallel, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }

    }

    @Test
    public void test08MySQLOptimizedParallelizedx10() throws Throwable {
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
        test08MySQLOptimizedParallelized();
    }

    @Test
    public void test09PostgreSQL() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);
        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);

        rc.build(j1, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }

    }

    @Test
    public void test09PostgreSQLx10() throws Throwable {
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();
        test09PostgreSQL();

    }

    @Test
    public void test09PostgreSQLParallelized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);
        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);

        rc.build(j1, env, inst, RCInterface.ParallelMode.Parallel, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }

    }

    @Test
    public void test09PostgreSQLParallelizedx10() throws Throwable {
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();
        test09PostgreSQLParallelized();

    }

    @Test
    public void test09PostgreSQLOptimized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();
        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
        ReplaceTool.ReplaceResult res = rt.replace(j1, env, rules);

        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }
    }

    @Test
    public void test09PostgreSQLOptimizedx10() throws Throwable {
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
        test09PostgreSQLOptimized();
    }

    @Test
    public void test09PostgreSQLOptimizedParallelized() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "bonus_boni");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "base");
        env.setObject(iValue, "b7");
        env.setObject(iValue2, 10);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, int.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i4),
                new PQLExtension("like_int", i4, iValue2),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_double", i1, iColumn5, i10)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        ReplaceTool rt = new ReplaceTool();
        j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
        ReplaceTool.ReplaceResult res = rt.replace(j1, env, rules);

        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Parallel, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (Double base : (PSet<Double>) env.getObject(bonus)) {
            System.out.println("Bonus for employee 10 " + base);
        }
    }

    @Test
    public void test09PostgreSQLOptimizedParallelizedx10() throws Throwable {
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
        test09PostgreSQLOptimizedParallelized();
    }

    @Test
    public void test10CountriesMySQL() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /*
         VarSet constVars = new VarSet();
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
         constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
         */
        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                new PQLExtension("field_String", i1, iColumn1, i6),
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_String", i2, iColumn4, i3),
                PQLFactory.EQ_String(i3, iValue2),
                new PQLExtension("field_String", i2, iColumn1, i7),
                PQLFactory.EQ_String(i7, i6)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i7));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test10CountriesMySQLOptimized() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "capital");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        env.setObject(i3, "");
        env.setInt(i4, 0);
        env.setObject(i5, "");
        //env.setObject(i6, "");
        //env.setObject(i7, "");

        inst.put(11, ResultSet.class);
        inst.put(12, ResultSet.class);
        inst.put(13, String.class);
        inst.put(14, int.class);
        inst.put(15, String.class);
        inst.put(16, String.class);
        inst.put(17, String.class);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);
        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                new PQLExtension("field_String", i1, iColumn1, i6),
                //PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_String", i2, iColumn1, i7),
                //PQLFactory.EQ_String(i7, i6),
                new PQLExtension("like_String", i7, i6),
                new PQLExtension("field_String", i2, iColumn4, i3),
                new PQLExtension("like_String", i3, iValue2)
        //new PQLExtension("field_String", i2, iColumn1, i7)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        //rules.add(new FieldLikeRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool();
        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);
        ReplaceTool.ReplaceResult res = rt.replace(j, env, rules);
        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial);//, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));
        System.out.println(res.getJoin());
        System.out.println((String) env.getObject(i3));
        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test13CountriesPostgreSQL() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        env.setObject(i3, "");
        env.setInt(i4, 0);
        env.setObject(i5, "");
        env.setObject(i6, "");
        env.setObject(i7, "");

        inst.put(13, String.class);
        inst.put(14, int.class);
        inst.put(15, String.class);
        inst.put(16, String.class);
        inst.put(17, String.class);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);
        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                //new PQLExtension("field_String", i1, iColumn1, i6),
                //PQLFactory.ConjunctiveBlock(
                //new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                //new PQLExtension("field_String", i2, iColumn1, i7),
                //PQLFactory.EQ_String(i7, i6),
                //new PQLExtension("like_String", i7, i6),

                new PQLExtension("field_String", i1, iColumn4, i3),
                //PQLFactory.EQ_String(i3, iValue2),
                new PQLExtension("like_String", i3, iValue2),
                new PQLExtension("field_String", i1, iColumn1, i7)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test16CountriesSQLite() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "sqlite");
        env.setObject(iDatabase, "data/databases/countries.sqlite");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        env.setObject(i3, "");
        env.setInt(i4, 0);
        env.setObject(i5, "");
        env.setObject(i6, "");
        env.setObject(i7, "");

        inst.put(13, String.class);
        inst.put(14, int.class);
        inst.put(15, String.class);
        inst.put(16, String.class);
        inst.put(17, String.class);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);
        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("sqlite", iDatabase, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                //new PQLExtension("field_String", i1, iColumn1, i6),
                //PQLFactory.ConjunctiveBlock(
                //new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                //new PQLExtension("field_String", i2, iColumn1, i7),
                //PQLFactory.EQ_String(i7, i6),
                //new PQLExtension("like_String", i7, i6),

                new PQLExtension("field_String", i1, iColumn4, i3),
                //PQLFactory.EQ_String(i3, iValue2),
                new PQLExtension("like_String", i3, iValue2),
                new PQLExtension("field_String", i1, iColumn1, i7)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test18Bonus() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "");
        env.setObject(iValue, "Yannis");
        env.setObject(iValue2, "Erde");

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, String.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_OBJECT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int employeesDepartment = Env.encodeVar(TYPE_OBJECT, 51);
        final int departmentsFactor = Env.encodeVar(TYPE_OBJECT, 52);

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i8), Env.readVar(i9), employeesDepartment)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i3),
                //new PQLExtension("like_String", i3, iValue),
                //new PQLExtension("field_String", i1, iColumn3, i4),
                //new PQLExtension("like_String", i4, iValue2),
                //new PQLExtension("field_int", i1, iColumn1, i5),
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i2),
                //new PQLExtension("field_int", i1, iColumn1, i7),
                //new PQLExtension("like_int", i7, i5),
                //PQLFactory.EQ_Int(i4, iValue2),
                new PQLExtension("field_int", i2, iColumn4, i9)
        );

        //j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
        System.out.println(j1);

        //rc.build(j1, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        //assertTrue(rc.exec(env));
        //for (Double base : (PSet<Double>) env.getObject(bonus)) {
        //    System.out.println("Bonus for employee 10 " + base);
        //}
    }

    @Test
    public void test21EmployeesFactor() throws SQLException {
        Map<Integer, Integer> employeesDepartment = new HashMap<Integer, Integer>();
        Map<Integer, Double> departmentsFactor = new HashMap<Integer, Double>();
        Set<Integer> employees = new HashSet<Integer>();

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from bonus_employees");
        while (resultSet.next()) {
            Integer eid = resultSet.getInt("eid");
            Integer dept = resultSet.getInt("dept");
            employeesDepartment.put(eid, dept);
        }

        resultSet = statement.executeQuery("select * from bonus_departments");
        while (resultSet.next()) {
            Integer did = resultSet.getInt("did");
            Double factor = resultSet.getDouble("factor");
            departmentsFactor.put(did, factor);
        }
        System.out.println("" + employeesDepartment.keySet().size());

        for (Integer employee : employeesDepartment.keySet()) {
            Integer department = employeesDepartment.get(employee);
            if (departmentsFactor.get(department) > 1.0) {
                employees.add(employee);
            }
        }

        System.out.println("size " + employees.size());

        for (Integer employee : employees) {
            System.out.println("Employee: " + employee);
        }

    }

    @Test
    public void test22EmployeesFactor() throws RuntimeCreatorException, Throwable {

        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
        final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        VarSet constVars;
        VarInfo varInfo;

        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "bonus_employees");
        env.setObject(iTable2, "bonus_departments");
        env.setObject(iTable3, "");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "eid");
        env.setObject(iColumn2, "dept");
        env.setObject(iColumn3, "did");
        env.setObject(iColumn4, "factor");
        env.setObject(iColumn5, "");
        env.setObject(iValue, "Yannis");
        env.setObject(iValue2, "Erde");

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, String.class);
        inst.put(41, String.class);

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_OBJECT, 14);
        final int i5 = Env.encodeVar(TYPE_INT, 15);
        final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
        final int i7 = Env.encodeVar(TYPE_INT, 17);
        final int i8 = Env.encodeVar(TYPE_INT, 18);
        final int i9 = Env.encodeVar(TYPE_INT, 19);
        final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

        final int employeesDepartment = Env.encodeVar(TYPE_OBJECT, 51);
        final int departmentsFactor = Env.encodeVar(TYPE_OBJECT, 52);
        Set<Integer> employees = new HashSet<Integer>();

        Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i7), Env.readVar(i8), employeesDepartment)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn1, i7),
                new PQLExtension("field_int", i1, iColumn2, i8)
        );
        //j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
        //rc.build(j1, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        //assertTrue(rc.exec(env));
        Join j2 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i9), Env.readVar(i10), departmentsFactor)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable2, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_int", i2, iColumn3, i9),
                new PQLExtension("field_double", i2, iColumn4, i10)
        );

        Join j = PQLFactory.ConjunctiveBlock(j1, j2);

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, true);
        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));
        System.out.println("" + (((PMap<Integer, Integer>) env.getObject(employeesDepartment)).keySet()).size());

        for (Integer employee : ((PMap<Integer, Integer>) env.getObject(employeesDepartment)).keySet()) {
            Integer department = ((PMap<Integer, Integer>) env.getObject(employeesDepartment)).get(employee);
            if (((PMap<Integer, Double>) env.getObject(departmentsFactor)).get(department) > 1.0) {
                employees.add(employee);
            }
        }

        System.out.println("size " + employees.size());

        for (Integer employee : employees) {
            System.out.println("Employee: " + employee);
        }
    }

    //@Test
    public void test99deleteDB() {
        benchmarks.bonus.Generator.uninstall_sql(SQL.MYSQL);
    }

}
