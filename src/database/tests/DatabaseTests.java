/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database.tests;

import database.DatabaseExtension;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarInfo;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.Suite;
import rc2.RCInterface;
import rc2.bcr.BytecodeCreatorRoutine;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author chriamue
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseTests {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    //@Test
    public void test01CreateBytecode() {
        BytecodeCreatorRoutine.create(new DatabaseExtension());
    }

    @Test
    public void test13CountriesPostgreSQL() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iCon = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iQuery = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 24);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iCon, "mysql://localhost/test?user=test&password=test");
        env.setObject(iQuery, "select * from countries");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "test");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);

        final int countries = Env.encodeVar(TYPE_OBJECT, 15);

        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i2), countries)},
                new PQLExtension("database", iCon, iQuery, i1),
                new PQLExtension("field_String", i1, iColumn1, i2)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

}
