/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database.tests;

import database.DatabaseExtension;
import database.FieldCompRule;
import database.SelectFromRule;
import database.WhereClauseRule;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_DOUBLE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarInfo;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import rc2.RCInterface;
import static rc2.RCInterface.BYTECODE_OUTPUT;
import static rc2.RCInterface.CODE_COMMENTS;
import static rc2.RCInterface.DETAILED_DEBUG;
import rc2.bcr.BytecodeCreatorRoutine;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;
import replacetool.AbstractRule;
import replacetool.ReplaceTool;
import replacetool.ReplaceTool.ReplaceResult;

/**
 *
 * @author chriamue
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MysqlTest {

    RCInterface rc;
    Env env;

    final Object oDatabasetype = "mysql";
    final Object oHost = "localhost";
    final Object oDatabase = "uni";
    final Object oUser = "uni";
    final Object oPassword = "uni";
    final Object oTable = "countries";
    final Object oColumns = "";
    final Object oWhereClause = "";
    final Object oColumn1 = "capital";
    final Object oColumn2 = "countryName";
    final Object oValue = "Denmark";

    final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
    final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
    final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
    final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
    final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
    final int iTable = Env.encodeReadVar(TYPE_OBJECT, 26);
    final int iColumns = Env.encodeReadVar(TYPE_OBJECT, 27);
    final int iWhereClause = Env.encodeReadVar(TYPE_OBJECT, 28);
    final int iValue = Env.encodeReadVar(TYPE_OBJECT, 29);
    final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 30);
    final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 31);

    VarSet constVars;
    VarInfo varInfo;

    Map<Integer, Class> inst = new HashMap<Integer, Class>();

    public MysqlTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, oDatabasetype);
        env.setObject(iHost, oHost);
        env.setObject(iDatabase, oDatabase);
        env.setObject(iUser, oUser);
        env.setObject(iPassword, oPassword);
        env.setObject(iTable, oTable);
        env.setObject(iColumns, oColumns);
        env.setObject(iWhereClause, oWhereClause);
        env.setObject(iValue, oValue);
        env.setObject(iColumn1, oColumn1);
        env.setObject(iColumn2, oColumn2);

        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test01CreateBytecode() {
        BytecodeCreatorRoutine.create(new DatabaseExtension());
    }

    @Test
    public void test02Build() throws RuntimeCreatorException, Throwable {
        RCInterface rc = new RCInterface(new DatabaseExtension());

        Env env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final Object o0 = "mysql";
        final Object o1 = "localhost";
        final Object o2 = "uni";
        final Object o3 = "uni";
        final Object o4 = "uni";
        final Object o5 = "countries";
        final Object o6 = "countryName";
        final Object o7 = "";

        final int i0 = Env.encodeReadVar(TYPE_OBJECT, 0);
        final int i1 = Env.encodeReadVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeReadVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeReadVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeReadVar(TYPE_OBJECT, 4);
        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        final int i6 = Env.encodeReadVar(TYPE_OBJECT, 6);
        final int i7 = Env.encodeReadVar(TYPE_OBJECT, 7);
        final int i8 = Env.encodeWriteVar(TYPE_OBJECT, 8);

        env.setObject(i0, o0);
        env.setObject(i1, o1);
        env.setObject(i2, o2);
        env.setObject(i3, o3);
        env.setObject(i4, o4);
        env.setObject(i5, o5);
        env.setObject(i6, o6);
        env.setObject(i7, o7);

        Join p = new PQLExtension("mysql", i1, i2, i3, i4, i5, i6, i7, i8);
        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        inst.put(0, String.class);
        inst.put(1, String.class);
        inst.put(2, String.class);
        inst.put(3, String.class);
        inst.put(4, String.class);
        inst.put(5, String.class);
        inst.put(6, String.class);
        inst.put(7, String.class);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial);

    }

    @Test
    public void test03Exec() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeVar(TYPE_OBJECT, 1);

        Join p = new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial);

        assertTrue(rc.exec(env));
    }

    @Test
    public void test04Combine() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1),
                new PQLExtension("field_String", i1, iColumn2, i2));

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        System.out.println("Test 4 Combine output: " + (String) env.getObject(i2));
    }

    @Test
    public void test05Map() throws RuntimeCreatorException, Throwable {
        final int i1 = Env.encodeWriteVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeWriteVar(TYPE_OBJECT, 14);
        final int i5 = Env.encodeWriteVar(TYPE_OBJECT, 15);

        env.setObject(iColumns, "countryName,capital");
        env.v_object[13] = new PSet();
        env.v_object[14] = new PMap();

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
// nmap
        Join p = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.MAP(Env.readVar(i2), Env.readVar(i5), i4)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1),
                new PQLExtension("field_String", i1, iColumn2, i2),
                new PQLExtension("field_String", i1, iColumn1, i5));

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial, BYTECODE_OUTPUT | CODE_COMMENTS);

        assertTrue(rc.exec(env));

        for (String value : (PSet<String>) env.getObject(i3)) {
            System.out.println(value);
        }
    }

    @Test
    public void test05Set() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeWriteVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 13);

        env.setObject(iColumns, "countryName,capital");
        env.v_object[13] = new PSet();

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

        Join p = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i2), i3)},
                PQLFactory.ConjunctiveBlock(
                        new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1),
                        new PQLExtension("field_String", i1, iColumn2, i2)));

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial);// ,BYTECODE_OUTPUT | CODE_COMMENTS | DETAILED_DEBUG);

        assertTrue(rc.exec(env));

        for (String value : (PSet<String>) env.getObject(i3)) {
            System.out.println(value);
        }
    }

    @Test
    public void test06Like() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 3);

        env.setObject(iColumns, "countryName,capital");

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1),
                new PQLExtension("field_String", i1, iColumn2, i2),
                new PQLExtension("like_String", i2, iValue),
                new PQLExtension("field_String", i1, iColumn1, i3));

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(env));
        System.out.println(p);
        System.out.println("Test 6 Like output: " + (String) env.getObject(i3));
        assertEquals("Copenhagen", (String) env.getObject(i3));
    }

    @Test
    public void test07EQ() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeWriteVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 3);

        env.setObject(iColumns, "countryName,capital");
        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1),
                new PQLExtension("field_String", i1, iColumn2, i2),
                new PQLExtension("like_String", i2, iValue),
                //PQLFactory.EQ_String(i2, iValue),
                new PQLExtension("field_String", i1, iColumn1, i3));

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        System.out.println("vorher" + p);

        ReplaceTool rt = new ReplaceTool();

        ReplaceResult res = rt.replace(p, env, rules);

        System.out.println("nachher" + res.getJoin());

        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 7 EQ_ output: " + (String) res.getEnv().getObject(i3));

        assertEquals("Copenhagen", (String) res.getEnv().getObject(i3));
    }

    @Test
    public void test08ReplaceTool() throws RuntimeCreatorException, Throwable {

        final int i8 = Env.encodeVar(TYPE_OBJECT, 8);
        final int i11 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i13 = Env.encodeVar(TYPE_OBJECT, 13);

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i13),
                new PQLExtension("field_String", i13, iColumn2, i8),
                new PQLExtension("like_String", i8, iValue),
                new PQLExtension("field_String", i13, iColumn1, i11));

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(p, env, rules);
        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 8 ReplaceTool output: " + (String) res.getEnv().getObject(i11));
        assertEquals("Copenhagen", (String) res.getEnv().getObject(i11));
    }

    @Test
    public void test09FieldInt() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeWriteVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeWriteVar(TYPE_INT, 4);

        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        env.setObject(i5, "population");
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 5));
        inst.put(5, String.class);

        env.setObject(iColumns, "");

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i3),
                new PQLExtension("field_String", i3, iColumn2, i1),
                new PQLExtension("like_String", i1, iValue),
                new PQLExtension("field_String", i3, iColumn1, i2),
                new PQLExtension("field_int", i3, i5, i4));

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(p, env, rules);
        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 9 Field Int output: " + (String) res.getEnv().getObject(i2) + " : " + res.getEnv().getInt(i4));
        assertEquals("Copenhagen", (String) res.getEnv().getObject(i2));
    }

    @Test
    public void test10FieldDouble() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeWriteVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeWriteVar(TYPE_DOUBLE, 4);

        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        env.setObject(i5, "areaInSqKm");
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 5));
        inst.put(5, String.class);

        env.setObject(iColumns, "");

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i3),
                new PQLExtension("field_String", i3, iColumn2, i1),
                new PQLExtension("like_String", i1, iValue),
                new PQLExtension("field_String", i3, iColumn1, i2),
                new PQLExtension("field_double", i3, i5, i4));

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(p, env, rules);
        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 10 Field Double output: " + (String) res.getEnv().getObject(i2) + " : " + res.getEnv().getDouble(i4));
        assertEquals("Copenhagen", (String) res.getEnv().getObject(i2));
    }

    @Test
    public void test11LessThen() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeWriteVar(TYPE_INT, 4);

        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        final int i6 = Env.encodeReadVar(TYPE_INT, 6);
        env.setObject(i5, "population");
        env.setInt(i6, 10 * 1000 * 1000);
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 5));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 6));
        inst.put(5, String.class);
        inst.put(6, int.class);

        env.setObject(iColumns, "capital,countryname,population");

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i3),
                //new PQLExtension("fieldlt_int", i3, i5, i6, i4),
                new PQLExtension("field_int", i3, i5, i4),
                PQLFactory.LT_Int(i4, i6),
                new PQLExtension("field_String", i3, iColumn1, i2)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(p, env, rules);

        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 11 Less than output: " + (String) res.getEnv().getObject(i2) + " : " + res.getEnv().getInt(i4));
    }

    @Test
    public void test12LessThenEQ() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeWriteVar(TYPE_INT, 4);

        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        final int i6 = Env.encodeReadVar(TYPE_INT, 6);
        env.setObject(i5, "population");
        env.setInt(i6, 10 * 1000 * 1000);
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 5));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 6));
        inst.put(5, String.class);
        inst.put(6, int.class);

        env.setObject(iColumns, "capital,countryname,population");

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i3),
                //new PQLExtension("fieldlteq_int", i3, i5, i6, i4),
                new PQLExtension("field_int", i3, i5, i4),
                PQLFactory.LTE_Int(i4, i6),
                new PQLExtension("field_String", i3, iColumn1, i2)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new FieldCompRule());
        rules.add(new WhereClauseRule());

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(p, env, rules);

        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 12 Less than or equal output: " + (String) res.getEnv().getObject(i2) + " : " + res.getEnv().getInt(i4));
    }

    @Test
    public void test13GreaterThen() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeWriteVar(TYPE_INT, 4);

        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        final int i6 = Env.encodeReadVar(TYPE_INT, 6);
        env.setObject(i5, "population");
        env.setInt(i6, 10 * 1000 * 1000);
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 5));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 6));
        inst.put(5, String.class);
        inst.put(6, int.class);

        env.setObject(iColumns, "capital,countryname,population");

        Join p = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i3),
                //new PQLExtension("fieldgteq_int", i3, i5, i6, i4),
                new PQLExtension("field_int", i3, i5, i4),
                PQLFactory.LT_Int(i6, i4),
                new PQLExtension("field_String", i3, iColumn1, i2)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        p = Optimizer.selectAccessPathRecursively(env, constVars, p, true);

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(p, env, rules);

        rc.build(res.getJoin(), res.getEnv(), inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        assertTrue(rc.exec(res.getEnv()));

        System.out.println("Test 13 Less than or equal output: " + (String) res.getEnv().getObject(i2) + " : " + res.getEnv().getInt(i4));
    }

    @Test
    public void test14Contains() throws RuntimeCreatorException, Throwable {

        final int i1 = Env.encodeWriteVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeWriteVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeWriteVar(TYPE_OBJECT, 13);

        final int i4 = Env.encodeReadVar(TYPE_OBJECT, 14);
        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 15);

        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);

        env.setObject(iColumns, "countryName,capital,languages");
        env.v_object[13] = new PSet();

        env.setObject(i4, "languages");
        env.setObject(i5, "de");

        inst.put(14, String.class);
        inst.put(15, String.class);

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i2), i3)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable, iColumns, iWhereClause, i1),
                new PQLExtension("field_String", i1, i4, i6),
                new PQLExtension("contains_String", i6, i5),
                new PQLExtension("field_String", i1, iColumn2, i2)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, true);

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool(new DatabaseExtension());

        ReplaceResult res = rt.replace(j, env, rules);
        j = res.getJoin();
        env = res.getEnv();

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);// ,BYTECODE_OUTPUT | CODE_COMMENTS | DETAILED_DEBUG);

        assertTrue(rc.exec(env));

        for (String value : (PSet<String>) env.getObject(i3)) {
            System.out.println(value);
        }
    }

}
