/***************************************************************************
 Copyright (C) 2014 chriamue

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/
package database.tests;

import benchmarks.postgresqldatabase.Generator;
import benchmarks.postgresqldatabase.Manual;
import database.DatabaseExtension;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import rc2.RCInterface;
import rc2.bcr.BytecodeCreatorRoutine;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author chmuelle
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PostgresqlTest {

    public PostgresqlTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1CreateBytecode() {
        BytecodeCreatorRoutine.create(new DatabaseExtension());
    }

    @Test
    public void test2Build() throws RuntimeCreatorException, Throwable {
        RCInterface rc = new RCInterface(new DatabaseExtension());

        Env env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final Object o0 = "postgresql";
        final Object o1 = "localhost";
        final Object o2 = "uni";
        final Object o3 = "uni";
        final Object o4 = "uni";
        final Object o5 = "countries";
        final Object o6 = "countryName";

        final int i0 = Env.encodeReadVar(TYPE_OBJECT, 0);
        final int i1 = Env.encodeReadVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeReadVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeReadVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeReadVar(TYPE_OBJECT, 4);
        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        final int i6 = Env.encodeReadVar(TYPE_OBJECT, 6);
        final int i7 = Env.encodeWriteVar(TYPE_OBJECT, 7);

        env.setObject(i0, o0);
        env.setObject(i1, o1);
        env.setObject(i2, o2);
        env.setObject(i3, o3);
        env.setObject(i4, o4);
        env.setObject(i5, o5);
        env.setObject(i6, o6);

        Join p = new PQLExtension("postgresql", i1, i2, i3, i4, i5, i6, i7);
        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        inst.put(0, String.class);
        inst.put(1, String.class);
        inst.put(2, String.class);
        inst.put(3, String.class);
        inst.put(4, String.class);
        inst.put(5, String.class);
        inst.put(6, String.class);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial);

    }

    @Test
    public void test3Exec() throws RuntimeCreatorException, Throwable {
        RCInterface rc = new RCInterface(new DatabaseExtension());

        Env env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final Object o0 = "postgresql";
        final Object o1 = "localhost";
        final Object o2 = "uni";
        final Object o3 = "uni";
        final Object o4 = "uni";
        final Object o5 = "countries";
        final Object o6 = "countryName";

        final int i0 = Env.encodeReadVar(TYPE_OBJECT, 0);
        final int i1 = Env.encodeReadVar(TYPE_OBJECT, 1);
        final int i2 = Env.encodeReadVar(TYPE_OBJECT, 2);
        final int i3 = Env.encodeReadVar(TYPE_OBJECT, 3);
        final int i4 = Env.encodeReadVar(TYPE_OBJECT, 4);
        final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
        final int i6 = Env.encodeReadVar(TYPE_OBJECT, 6);
        final int i7 = Env.encodeWriteVar(TYPE_OBJECT, 7);

        env.setObject(i0, o0);
        env.setObject(i1, o1);
        env.setObject(i2, o2);
        env.setObject(i3, o3);
        env.setObject(i4, o4);
        env.setObject(i5, o5);
        env.setObject(i6, o6);

        Join p = new PQLExtension("postgresql", i1, i2, i3, i4, i5, i6, i7);
        Map<Integer, Class> inst = new HashMap<Integer, Class>();

        inst.put(0, String.class);
        inst.put(1, String.class);
        inst.put(2, String.class);
        inst.put(3, String.class);
        inst.put(4, String.class);
        inst.put(5, String.class);
        inst.put(6, String.class);
        inst.put(7, ResultSet.class);

        rc.build(p, env, inst, RCInterface.ParallelMode.Serial);

        assertTrue(rc.exec(env));
    }

    @Test
    public void test0Manual() throws SQLException {
        final String host = "localhost";
        final String database = "uni";
        final String user = "uni";
        final String password = "uni";
        final String table = "countries";
        final String column = "countryName";

        Connection connection = DriverManager.getConnection("jdbc:postgresql://" + host + "/" + database + "?user=" + user + "&password=" + password);
        Statement statement = connection.createStatement();
        StringBuilder sb = new StringBuilder();
        sb.append("select \"");
        sb.append(column);
        sb.append("\" from ");
        sb.append(table);
        ResultSet resultSet = statement.executeQuery(sb.toString());

        while (resultSet.next()) {
            String value = resultSet.getString(Generator.column);
            System.out.println(value);
        }
    }
}
