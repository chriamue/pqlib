/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database.tests;

import benchmarks.SQL;
import database.DatabaseExtension;
import database.FieldCompRule;
import database.SelectFromRule;
import database.WhereClauseRule;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_DOUBLE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarInfo;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import rc2.RCInterface;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;
import replacetool.AbstractRule;
import replacetool.ReplaceTool;

/**
 *
 * @author chmuelle
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SQLCountriesBenchTest {

    public SQLCountriesBenchTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("org.postgresql.Driver");
            Class.forName("org.sqlite.JDBC");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void test10CountriesMySQLMini() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Denmark");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i2),
                new PQLExtension("like_String", i2, iValue2),
                new PQLExtension("field_String", i1, iColumn3, i3)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i3));
    }

    @Test
    public void test10CountriesMySQLMinix10() throws Throwable {
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();

        test10CountriesMySQLMini();
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();
        test10CountriesMySQLMini();

    }

    @Test
    public void test10CountriesPostgreSQLMini() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /*
         VarSet constVars = new VarSet();
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
         constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
         */
        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Denmark");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i2),
                new PQLExtension("like_String", i2, iValue2),
                new PQLExtension("field_String", i1, iColumn3, i3)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i3));
    }

    @Test
    public void test10CountriesPostgreSQLMinix10() throws Throwable {
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();

        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();
        test10CountriesPostgreSQLMini();

    }

    @Test
    public void test10CountriesSQLiteMini() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /*
         VarSet constVars = new VarSet();
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
         constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
         */
        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "sqlite");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "data/databases/countries.sqlite");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Denmark");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.ConjunctiveBlock(
                new PQLExtension("sqlite", iDatabase, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i2),
                new PQLExtension("like_String", i2, iValue2),
                new PQLExtension("field_String", i1, iColumn3, i3)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i3));
    }

    @Test
    public void test10CountriesSQLiteMinix10() throws Throwable {
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();

        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();
        test10CountriesSQLiteMini();

    }

    @Test
    public void test10CountriesMySQLMiniOptimized() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /*
         VarSet constVars = new VarSet();
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
         constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
         */
        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Denmark");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.ConjunctiveBlock(
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i2),
                new PQLExtension("like_String", i2, iValue2),
                new PQLExtension("field_String", i1, iColumn3, i3)
        );

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool();
        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);
        ReplaceTool.ReplaceResult res = rt.replace(j, env, rules);
        env = res.getEnv();
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i3));
    }

    @Test
    public void test10CountriesMySQLMiniOptimizedx10() throws Throwable {
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();

        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
        test10CountriesMySQLMiniOptimized();
    }

    @Test
    public void test10CountriesPostgreSQLMiniOptimized() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /*
         VarSet constVars = new VarSet();
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
         constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
         */
        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Denmark");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.ConjunctiveBlock(
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i2),
                new PQLExtension("like_String", i2, iValue2),
                new PQLExtension("field_String", i1, iColumn3, i3)
        );
        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        long time = System.currentTimeMillis();
        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool();

        ReplaceTool.ReplaceResult res = rt.replace(j, env, rules);
        env = res.getEnv();
        System.out.println("time: " + (time - System.currentTimeMillis()));
        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i3));
    }

    @Test
    public void test10CountriesPostgreSQLMiniOptimizedx10() throws Throwable {
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();

        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
        test10CountriesPostgreSQLMiniOptimized();
    }

    @Test
    public void test10CountriesSQLiteMiniOptimized() throws Throwable {
        RCInterface rc = null;
        Env env;

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /*
         VarSet constVars = new VarSet();
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
         constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
         constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
         */
        VarSet constVars = new VarSet();
        constVars.insert(iDatabasetype);
        constVars.insert(iHost);
        constVars.insert(iDatabase);
        constVars.insert(iUser);
        constVars.insert(iPassword);
        constVars.insert(iTable1);
        constVars.insert(iTable2);
        constVars.insert(iTable3);
        constVars.insert(iColumns1);
        constVars.insert(iColumns2);
        constVars.insert(iColumns3);
        constVars.insert(iWhereClause1);
        constVars.insert(iWhereClause2);
        constVars.insert(iWhereClause3);
        constVars.insert(iColumn1);
        constVars.insert(iColumn2);
        constVars.insert(iColumn3);
        constVars.insert(iColumn4);
        constVars.insert(iColumn5);
        constVars.insert(iValue1);
        constVars.insert(iValue2);

        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "sqlite");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "data/databases/countries.sqlite");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Denmark");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.ConjunctiveBlock(
                new PQLExtension("sqlite", iDatabase, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i2),
                new PQLExtension("like_String", i2, iValue2),
                new PQLExtension("field_String", i1, iColumn3, i3)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);
        long time = System.currentTimeMillis();

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool();

        ReplaceTool.ReplaceResult res = rt.replace(j, env, rules);
        env = res.getEnv();

        System.out.println("time: " + (time - System.currentTimeMillis()));

        rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);
        System.out.println(j);
        assertTrue(rc.exec(env));

        System.out.println((String) env.getObject(i3));
    }

    @Test
    public void test10CountriesSQLiteMiniOptimizedx10() throws Throwable {
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();

        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
        test10CountriesSQLiteMiniOptimized();
    }

    @Test
    public void test10CountriesMySQLOptimized() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "mysql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "capital");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);

        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_String", i2, iColumn1, i7),
                new PQLExtension("field_String", i2, iColumn4, i3),
                new PQLExtension("like_String", i3, iValue2),
                new PQLExtension("mysql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_String", i1, iColumn1, i6),
                new PQLExtension("like_String", i6, i7),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(i4, iValue1)
        );
        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);
        System.out.println(j);
        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool();

        ReplaceTool.ReplaceResult res = rt.replace(j, env, rules);
        //j = res.getJoin();
        //env = res.getEnv();
        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));
        System.out.println(j);
        System.out.println((String) env.getObject(i3));
        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test13CountriesPostgreSQL() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        env.setObject(i3, "");
        env.setInt(i4, 0);
        env.setObject(i5, "");
        env.setObject(i6, "");
        env.setObject(i7, "");

        inst.put(13, String.class);
        inst.put(14, int.class);
        inst.put(15, String.class);
        inst.put(16, String.class);
        inst.put(17, String.class);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);
        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                new PQLExtension("field_String", i1, iColumn1, i6),
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_String", i2, iColumn1, i7),
                PQLFactory.EQ_String(i7, i6),
                new PQLExtension("field_String", i1, iColumn4, i3),
                PQLFactory.EQ_String(i3, iValue2)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test13CountriesPostgreSQLOptimized() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
        final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "postgresql");
        env.setObject(iHost, "localhost");
        env.setObject(iDatabase, "test");
        env.setObject(iUser, "test");
        env.setObject(iPassword, "test");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        env.setObject(i3, "");
        env.setInt(i4, 0);
        env.setObject(i5, "");
        env.setObject(i6, "");
        env.setObject(i7, "");

        inst.put(13, String.class);
        inst.put(14, int.class);
        inst.put(15, String.class);
        inst.put(16, String.class);
        inst.put(17, String.class);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);
        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                new PQLExtension("field_String", i1, iColumn1, i6),
                new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable1, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_String", i2, iColumn1, i7),
                PQLFactory.EQ_String(i7, i6),
                new PQLExtension("field_String", i2, iColumn4, i3),
                PQLFactory.EQ_String(i3, iValue2)
        );

        j = Optimizer.selectAccessPathRecursively(env, constVars, j, true);

        Set<AbstractRule> rules = new HashSet();
        rules.add(new SelectFromRule());
        rules.add(new WhereClauseRule());
        rules.add(new FieldCompRule());

        ReplaceTool rt = new ReplaceTool();

        ReplaceTool.ReplaceResult res = rt.replace(j, env, rules);
        env = res.getEnv();
        j = res.getJoin();
//        j = Optimizer.selectAccessPathRecursively(env, constVars, j, true);
        System.out.println(j);
        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    @Test
    public void test16CountriesSQLite() throws Throwable {
        RCInterface rc = null;
        Env env;

        final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
        final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
        final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
        final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
        final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
        final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
        final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
        final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
        final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
        final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
        final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);

        final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 35);
        final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 36);
        final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 37);
        final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 38);
        final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 49);
        final int iValue1 = Env.encodeReadVar(TYPE_INT, 40);
        final int iValue2 = Env.encodeReadVar(TYPE_OBJECT, 41);

        Map<Integer, Class> inst = new HashMap<Integer, Class>();
        inst.put(21, String.class);
        inst.put(22, String.class);
        inst.put(23, String.class);
        inst.put(24, String.class);
        inst.put(25, String.class);
        inst.put(26, String.class);
        inst.put(27, String.class);
        inst.put(28, String.class);
        inst.put(29, String.class);
        inst.put(30, String.class);
        inst.put(31, String.class);
        inst.put(32, String.class);
        inst.put(33, String.class);
        inst.put(34, String.class);
        inst.put(35, String.class);
        inst.put(36, String.class);
        inst.put(37, String.class);
        inst.put(38, String.class);
        inst.put(39, String.class);
        inst.put(40, int.class);
        inst.put(41, String.class);

        /////////////////
        try {
            rc = new RCInterface(new DatabaseExtension());

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[]{
                    new int[100], new long[100], new double[100], new Object[100]
                });

        VarSet constVars = new VarSet();
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
        constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
        constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 41));
        VarInfo varInfo = new VarInfo(constVars);

        env.setObject(iDatabasetype, "sqlite");
        env.setObject(iDatabase, "data/databases/countries.sqlite");
        env.setObject(iTable1, "countries");

        env.setObject(iColumns1, "");
        env.setObject(iColumns2, "");
        env.setObject(iColumns3, "");
        env.setObject(iWhereClause1, "");
        env.setObject(iWhereClause2, "");
        env.setObject(iWhereClause3, "");
        env.setObject(iColumn1, "countryName");
        env.setObject(iColumn2, "population");
        env.setObject(iColumn3, "capital");
        env.setObject(iColumn4, "continentName");
        env.setObject(iColumn5, "areaInSqKm");
        env.setInt(iValue1, 42 * 1000 * 1000);
        env.setObject(iValue2, "Europe");

        final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
        final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
        final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
        final int i4 = Env.encodeVar(TYPE_INT, 14);
        final int i5 = Env.encodeVar(TYPE_OBJECT, 15);
        final int i6 = Env.encodeVar(TYPE_OBJECT, 16);
        final int i7 = Env.encodeVar(TYPE_OBJECT, 17);

        env.setObject(i3, "");
        env.setInt(i4, 0);
        env.setObject(i5, "");
        env.setObject(i6, "");
        env.setObject(i7, "");

        inst.put(13, String.class);
        inst.put(14, int.class);
        inst.put(15, String.class);
        inst.put(16, String.class);
        inst.put(17, String.class);

        final int countries = Env.encodeVar(TYPE_OBJECT, 51);
        Join j = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i7), countries)},
                new PQLExtension("sqlite", iDatabase, iTable1, iColumns1, iWhereClause1, i1),
                new PQLExtension("field_int", i1, iColumn2, i4),
                PQLFactory.LT_Int(iValue1, i4),
                new PQLExtension("field_String", i1, iColumn1, i6),
                //PQLFactory.ConjunctiveBlock(
                new PQLExtension("sqlite", iDatabase, iTable1, iColumns2, iWhereClause2, i2),
                new PQLExtension("field_String", i2, iColumn1, i7),
                PQLFactory.EQ_String(i7, i6),
                //new PQLExtension("like_String", i7, i6),

                new PQLExtension("field_String", i1, iColumn4, i3),
                PQLFactory.EQ_String(i3, iValue2)
        //new PQLExtension("like_String", i3, iValue2)
        //new PQLExtension("field_String", i1, iColumn1, i7)
        );

        System.out.println(j);
        j = Optimizer.selectAccessPathRecursively(env, constVars, j, false);
        System.out.println(j);

        rc.build(j, env, inst, RCInterface.ParallelMode.Serial, RCInterface.DYNAMIC_OPTIMIZE);

        assertTrue(rc.exec(env));

        for (String country : (PSet<String>) env.getObject(countries)) {
            System.out.println(country);
        }
    }

    //@Test
    public void test99deleteDB() {
        benchmarks.bonus.Generator.uninstall_sql(SQL.MYSQL);
    }

}
