/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package database;

import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import static edu.umass.pql.PQLFactory.EQ_String;
import edu.umass.pql.VarConstants;
import edu.umass.pql.VarInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;
import replacetool.AbstractRule;

/**
 *
 * @author chriamue
 */
public class FieldCompRule extends AbstractRule implements VarConstants {

    @Override
    public RuleType getRuleType() {
        return RuleType.JoinJoin;
    }

    @Override
    public boolean isLeftCommand(Command command) {
        return ((command.getJoin() instanceof PQLExtension)
                && (command.getJoin().getName().equals("field_int") || command.getJoin().getName().equals("field_String")
                || command.getJoin().getName().equals("field_long") || command.getJoin().getName().equals("field_double")));
    }

    @Override
    public boolean canReplace(Command left, Command right) {
        return ( //
                ((left.getJoin() instanceof PQLExtension)
                && (left.getJoin().getName().equals("field_int") || left.getJoin().getName().equals("field_String")
                || left.getJoin().getName().equals("field_long") || left.getJoin().getName().equals("field_double")))
                //
                && (((right.getJoin().getName().equals("LT_Int")
                || right.getJoin().getName().equals("LT_Long") || right.getJoin().getName().equals("LT_Double"))
                && Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))
                && Env.varType((left.getJoin()).getArg(2)) == Env.varType((right.getJoin()).getArg(0)))
                //
                || ((right.getJoin().getName().equals("LT_Int")
                || right.getJoin().getName().equals("LT_Long") || right.getJoin().getName().equals("LT_Double"))
                && Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(1))
                && Env.varType((left.getJoin()).getArg(2)) == Env.varType((right.getJoin()).getArg(1)))
                //
                || ((right.getJoin().getName().equals("EQ_Int") || (right.getJoin().getName().equals("EQ_String")
                || right.getJoin().getName().equals("EQ_Long") || right.getJoin().getName().equals("EQ_Double")
                || right.getJoin().getName().equals("like_int") || right.getJoin().getName().equals("like_String")
                || right.getJoin().getName().equals("contains_String"))
                && Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))
                && Env.varType((left.getJoin()).getArg(2)) == Env.varType((right.getJoin()).getArg(0))))));
    }

    @Override
    public Command replaceCommands(Command left, Command right) {

        String name = "";
        switch (right.getJoin().getName()) {
            case "LT_Int":
                if (Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))) {
                    name = "fieldlt_int";
                } else {
                    name = "fieldgt_int";
                }
                break;
            case "LT_Long":
                if (Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))) {
                    name = "fieldlt_long";
                } else {
                    name = "fieldgt_long";
                }
                break;
            case "LT_Double":
                if (Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))) {
                    name = "fieldlt_double";
                } else {
                    name = "fieldgt_double";
                }
                break;
            case "LTE_Int":
                if (Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))) {
                    name = "fieldlteq_int";
                } else {
                    name = "fieldgteq_int";
                }
                break;
            case "LTE_Long":
                if (Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))) {
                    name = "fieldlteq_long";
                } else {
                    name = "fieldgteq_long";
                }
                break;
            case "LTE_Double":
                if (Env.varIndex((left.getJoin()).getArg(2)) == Env.varIndex((right.getJoin()).getArg(0))) {
                    name = "fieldlteq_double";
                } else {
                    name = "fieldgteq_double";
                }
                break;
            case "like_int":
            case "EQ_Int":
                name = "fieldeq_int";
                break;
            case "EQ_Long":
                name = "fieldeq_long";
                break;
            case "EQ_Double":
                name = "fieldeq_double";
                break;
            case "like_String":
            case "EQ_String":
                name = "fieldeq_String";
                break;
            case "contains_String":
                name = "fieldcontains_String";
                break;
            default:
                name = "fieldeq_String";
        }
        Join join = null;
        VarInfo varInfo = left.getVarInfo();
        try {
            join = new PQLExtension(name, left.getJoin().getArg(0),
                    left.getJoin().getArg(1),
                    Env.readVar(
                            right.getJoin().getArg(1)),
                    left.getJoin().getArg(2));
            varInfo.getConstantSet().insert(Env.encodeReadVar(
                    Env.varType(right.getJoin().getArg(1)),
                    right.getJoin().getArg(1)));

        } catch (RuntimeCreatorException ex) {
            Logger.getLogger(FieldCompRule.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Command(join, varInfo, left.getEnv());

    }

    @Override
    public Map<EnvContentType, List<Object>> getEnvExtension(Command left, Command right) {
        return new HashMap();
    }

}
