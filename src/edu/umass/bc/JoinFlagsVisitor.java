/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import edu.umass.pql.AbstractBlock;
import edu.umass.pql.VarConstants;
import edu.umass.pql.il.Arithmetic;
import edu.umass.pql.il.Comparison;
import edu.umass.pql.il.Container;
import edu.umass.pql.il.Container.BOOLEAN_ARRAY_SIZE;
import edu.umass.pql.il.Container.BYTE_ARRAY_SIZE;
import edu.umass.pql.il.Container.CHAR_ARRAY_SIZE;
import edu.umass.pql.il.Container.DOUBLE_ARRAY_SIZE;
import edu.umass.pql.il.Container.FLOAT_ARRAY_SIZE;
import edu.umass.pql.il.Container.INT_ARRAY_SIZE;
import edu.umass.pql.il.Container.LONG_ARRAY_SIZE;
import edu.umass.pql.il.Container.MAP_SIZE;
import edu.umass.pql.il.Container.OBJECT_ARRAY_SIZE;
import edu.umass.pql.il.Container.POLY_SIZE;
import edu.umass.pql.il.Container.SET_SIZE;
import edu.umass.pql.il.Container.SHORT_ARRAY_SIZE;
import edu.umass.pql.il.Field;
import edu.umass.pql.il.NoFail.Bool;
import edu.umass.pql.il.Not;
import edu.umass.pql.il.ReductionImpl;
import edu.umass.pql.il.Type;
import edu.umass.pql.il.Type.COERCE_Boolean;
import edu.umass.pql.il.Type.COERCE_Byte;
import edu.umass.pql.il.Type.COERCE_Char;
import edu.umass.pql.il.Type.COERCE_Float;
import edu.umass.pql.il.Type.COERCE_Short;
import rc2.representation.PQLExtension;

/**
 *
 * @author Hilmar
 */
public class JoinFlagsVisitor implements BcFlags, VarConstants {

    public int visit (Arithmetic.ADD_Int e) {
        return TYPE_ADD | TYPE_INT;
    }

    public int visit (Arithmetic.ADD_Long e) {
        return TYPE_ADD | TYPE_LONG;
    }

    public int visit (Arithmetic.ADD_Double e) {
        return TYPE_ADD | TYPE_DOUBLE;
    }

    public int visit (Arithmetic.SUB_Int e) {
        return TYPE_SUB | TYPE_INT;
    }

    public int visit (Arithmetic.SUB_Long e) {
        return TYPE_SUB | TYPE_LONG;
    }

    public int visit (Arithmetic.SUB_Double e) {
        return TYPE_SUB | TYPE_DOUBLE;
    }

    public int visit (Arithmetic.MUL_Int e) {
        return TYPE_MUL | TYPE_INT;
    }

    public int visit (Arithmetic.MUL_Long e) {
        return TYPE_MUL | TYPE_LONG;
    }

    public int visit (Arithmetic.MUL_Double e) {
        return TYPE_MUL | TYPE_DOUBLE;
    }

    public int visit (Arithmetic.DIV_Int e) {
        return TYPE_DIV | TYPE_INT;
    }

    public int visit (Arithmetic.DIV_Long e) {
        return TYPE_DIV | TYPE_LONG;
    }

    public int visit (Arithmetic.DIV_Double e) {
        return TYPE_DIV | TYPE_DOUBLE;
    }

    public int visit (Arithmetic.NEG_Int e) {
        return TYPE_NEG | TYPE_INT;
    }

    public int visit (Arithmetic.NEG_Long e) {
        return TYPE_NEG | TYPE_LONG;
    }

    public int visit (Arithmetic.NEG_Double e) {
        return TYPE_NEG | TYPE_DOUBLE;
    }

    public int visit (Arithmetic.MOD_Int e) {
        return TYPE_MOD | TYPE_INT;
    }

    public int visit (Arithmetic.MOD_Long e) {
        return TYPE_MOD | TYPE_LONG;
    }

    public int visit (Arithmetic.BITINV_Int e) {
        return TYPE_BITINV | TYPE_INT;
    }

    public int visit (Arithmetic.BITINV_Long e) {
        return TYPE_BITINV | TYPE_LONG;
    }

    public int visit (Arithmetic.BITOR_Int e) {
        return TYPE_BITOR | TYPE_INT;
    }

    public int visit (Arithmetic.BITOR_Long e) {
        return TYPE_BITOR | TYPE_LONG;
    }

    public int visit (Arithmetic.BITAND_Int e) {
        return TYPE_BITAND | TYPE_INT;
    }

    public int visit (Arithmetic.BITAND_Long e) {
        return TYPE_BITAND | TYPE_LONG;
    }

    public int visit (Arithmetic.BITXOR_Int e) {
        return TYPE_BITXOR | TYPE_INT;
    }

    public int visit (Arithmetic.BITXOR_Long e) {
        return TYPE_BITXOR | TYPE_LONG;
    }

    public int visit (Arithmetic.BITSHL_Int e) {
        return TYPE_BITSHL | TYPE_INT;
    }

    public int visit (Arithmetic.BITSHL_Long e) {
        return TYPE_BITSHL | TYPE_LONG;
    }

    public int visit (Arithmetic.BITSHR_Int e) {
        return TYPE_BITSHR | TYPE_INT;
    }

    public int visit (Arithmetic.BITSHR_Long e) {
        return TYPE_BITSHR | TYPE_LONG;
    }

    public int visit (Arithmetic.BITSSHR_Int e) {
        return TYPE_BITSSHR | TYPE_INT;
    }

    public int visit (Arithmetic.BITSSHR_Long e) {
        return TYPE_BITSSHR | TYPE_LONG;
    }

    public int visit (Container.INT_RANGE_CONTAINS e) {
        return TYPE_RANGE_CONTAINS | TYPE_INT;
    }

    public int visit (Container.LONG_RANGE_CONTAINS e) {
        return TYPE_RANGE_CONTAINS | TYPE_LONG;
    }

    public int visit (AbstractBlock.PreConjunctive e) {
        return TYPE_CONJUCTIVE;
    }

    public int visit (AbstractBlock.PreDisjunctive e) {
        return TYPE_DISJUNCTIVE;
    }

    public int visit (ReductionImpl e) {
        return TYPE_REDUCTION;
    }

    public int visit (Comparison.EQ_Int e) {
        return TYPE_EQ | TYPE_INT;
    }

    public int visit (Comparison.EQ_Long e) {
        return TYPE_EQ | TYPE_LONG;
    }

    public int visit (Comparison.EQ_Double e) {
        return TYPE_EQ | TYPE_DOUBLE;
    }

    public int visit (Comparison.EQ_Object e) {
        return TYPE_EQ | TYPE_OBJECT;
    }

    public int visit (Comparison.EQ_String e) {
        return TYPE_EQ | TYPE_STRING;
    }

    public int visit (Comparison.NEQ_Int e) {
        return TYPE_NEQ | TYPE_INT;
    }

    public int visit (Comparison.NEQ_Long e) {
        return TYPE_NEQ | TYPE_LONG;
    }

    public int visit (Comparison.NEQ_Double e) {
        return TYPE_NEQ | TYPE_DOUBLE;
    }

    public int visit (Comparison.NEQ_Object e) {
        return TYPE_NEQ | TYPE_OBJECT;
    }

    public int visit (Comparison.NEQ_String e) {
        return TYPE_NEQ | TYPE_STRING;
    }

    public int visit (Comparison.LT_Int e) {
        return TYPE_LT | TYPE_INT;
    }

    public int visit (Comparison.LT_Long e) {
        return TYPE_LT | TYPE_LONG;
    }

    public int visit (Comparison.LT_Double e) {
        return TYPE_LT | TYPE_DOUBLE;
    }

    public int visit (Comparison.LTE_Int e) {
        return TYPE_LTE | TYPE_INT;
    }

    public int visit (Comparison.LTE_Long e) {
        return TYPE_LTE | TYPE_LONG;
    }

    public int visit (Comparison.LTE_Double e) {
        return TYPE_LTE | TYPE_DOUBLE;
    }

    public int visit (Bool e) {
        return TYPE_BOOL;
    }

    public int visit (Not e) {
        return TYPE_NOT;
    }

    public int visit (Container.CONTAINS e) {
        return TYPE_CONTAINS;
    }

    public int visit (Container.LOOKUP e) {
        return TYPE_LOOKUP;
    }

    public int visit (Container.ARRAY_LOOKUP_Int e) {
        return TYPE_ARRAY_LOOKUP | TYPE_INT;
    }

    public int visit (Container.ARRAY_LOOKUP_Long e) {
        return TYPE_ARRAY_LOOKUP | TYPE_LONG;
    }

    public int visit (Container.ARRAY_LOOKUP_Short e) {
        return TYPE_ARRAY_LOOKUP | TYPE_SHORT;
    }

    public int visit (Container.ARRAY_LOOKUP_Char e) {
        return TYPE_ARRAY_LOOKUP | TYPE_CHAR;
    }

    public int visit (Container.ARRAY_LOOKUP_Boolean e) {
        return TYPE_ARRAY_LOOKUP | TYPE_BOOLEAN;
    }

    public int visit (Container.ARRAY_LOOKUP_Byte e) {
        return TYPE_ARRAY_LOOKUP | TYPE_BYTE;
    }

    public int visit (Container.ARRAY_LOOKUP_Float e) {
        return TYPE_ARRAY_LOOKUP | TYPE_FLOAT;
    }

    public int visit (Container.ARRAY_LOOKUP_Double e) {
        return TYPE_ARRAY_LOOKUP | TYPE_DOUBLE;
    }

    public int visit (Container.ARRAY_LOOKUP_Object e) {
        return TYPE_ARRAY_LOOKUP | TYPE_OBJECT;
    }

    public int visit (Container.POLY_LOOKUP e) {
        return TYPE_POLY_LOOKUP;
    }

    public int visit (COERCE_Boolean e) {
        return TYPE_COERCION | TYPE_BOOLEAN;
    }

    public int visit (COERCE_Byte e) {
        return TYPE_COERCION | TYPE_BYTE;
    }

    public int visit (COERCE_Short e) {
        return TYPE_COERCION | TYPE_SHORT;
    }

    public int visit (COERCE_Char e) {
        return TYPE_COERCION | TYPE_CHAR;
    }

    public int visit (COERCE_Float e) {
        return TYPE_COERCION | TYPE_FLOAT;
    }

    public int visit (Field e) {
        return TYPE_FIELD;
    }

    public int visit (POLY_SIZE e) {
        return TYPE_POLY_SIZE;
    }

    public int visit (SET_SIZE e) {
        return TYPE_SET_SIZE;
    }

    public int visit (MAP_SIZE e) {
        return TYPE_MAP_SIZE;
    }

    public int visit (INT_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_INT;
    }

    public int visit (LONG_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_LONG;
    }

    public int visit (DOUBLE_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_DOUBLE;
    }

    public int visit (OBJECT_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_OBJECT;
    }

    public int visit (BOOLEAN_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_BOOLEAN;
    }

    public int visit (SHORT_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_SHORT;
    }

    public int visit (CHAR_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_CHAR;
    }

    public int visit (FLOAT_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_FLOAT;
    }

    public int visit (BYTE_ARRAY_SIZE e) {
        return TYPE_ARRAY_SIZE | TYPE_BYTE;
    }

    public int visit (Type.JAVA_TYPE e) {
        return TYPE_CHECK_JAVA_TYPE;
    }

    public int visit (Type.INT e) {
        return TYPE_CHECK_TYPE | TYPE_INT;
    }

    public int visit (Type.LONG e) {
        return TYPE_CHECK_TYPE | TYPE_LONG;
    }

    public int visit (Type.BOOLEAN e) {
        return TYPE_CHECK_TYPE | TYPE_BOOLEAN;
    }

    public int visit (Type.BYTE e) {
        return TYPE_CHECK_TYPE | TYPE_BYTE;
    }

    public int visit (Type.SHORT e) {
        return TYPE_CHECK_TYPE | TYPE_SHORT;
    }

    public int visit (Type.CHAR e) {
        return TYPE_CHECK_TYPE | TYPE_CHAR;
    }

    public int visitTRUE () {
        return TYPE_TRUE;
    }

    public int visitFALSE () {
        return TYPE_FALSE;
    }

    public int visit (PQLExtension e) {
        /*ExtensionName extensionName = e.getExtension();
        if (extensionName == null)*/
            return (e.getBCRExtension().getId() << 8);
        /*switch (extensionName) {
            default:*/
                //return -1;
        //}
    }

}
