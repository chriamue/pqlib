/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import edu.umass.pql.Env;
import edu.umass.pql.Reductor;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.reductor.MethodAdapterReductor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Hilmar
 */
public class ReturnBehavior implements BcFlags, Opcodes, VarConstants {

    private int type, usedLocalVariables;
    int [][] arguments;
    private Label label;
    Reductor obj [];

    public ReturnBehavior () {
        type = RETURN_RETURN;
    }

    public static ReturnBehavior getJumpBehavior (Label _label) {
        ReturnBehavior newBehavior = new ReturnBehavior ();
        newBehavior.type = RETURN_JUMP;
        newBehavior.label = _label;
        return newBehavior;
    }

    public static ReturnBehavior getNothingBehavior () {
        ReturnBehavior newBehavior = new ReturnBehavior ();
        newBehavior.type = RETURN_DONOTHING;
        return newBehavior;
    }

    public static ReturnBehavior getReductionBehavior (Label _label, int _usedLocalVariables, int [][] _arguments, Reductor _obj []) {
        ReturnBehavior newBehavior = new ReturnBehavior ();
        newBehavior.type = RETURN_REDUCTION;
        newBehavior.label = _label;
        newBehavior.usedLocalVariables = _usedLocalVariables;
        newBehavior.arguments = _arguments;
        newBehavior.obj = _obj;
        return newBehavior;
    }

    public void insertReturnCode (boolean result, MethodVisitor mv, ReturnBehaviorStack behaviorStack) throws Exception {//Stack <ReturnBehavior> behaviorsBefore) throws Exception {
        switch (type) {

            case RETURN_RETURN:
                if (!result)
                {
                    mv.visitInsn( (result ? ICONST_1 : ICONST_0) );
                    mv.visitInsn(IRETURN);
                }
                break;

            case RETURN_JUMP:
                if (!result)
                    mv.visitJumpInsn(GOTO, label);
                break;

            case RETURN_DONOTHING:
                break;

            case RETURN_REDUCTION:
                if (result) {
                    int currentLocalVariable = 1 + usedLocalVariables;
                    for (int i=0; i<arguments.length; i++) {
                        RuntimeCreator.insertComment(mv, "Reductor_iteration");
                        if (arguments[i][2] == REDUCTOR_EXISTS) {
                            mv.visitInsn(ICONST_1);
                            RuntimeCreator.storeValue (mv, arguments[i][1], TYPE_INT);
                        } else if ((arguments[i][2] & 0xFF00) == REDUCTOR_METHOD_ADAPTER) {
                            RuntimeCreator.pushValue(mv, arguments[i][0], (arguments[i][2] & 255) );
                            RuntimeCreator.pushValue(mv, arguments[i][1], (arguments[i][2] & 255) );
                            Method method = (Method)(((MethodAdapterReductor)obj[i]).getReductorObject());
                            String methodPackage = method.toString();
                            methodPackage = methodPackage.substring(0, methodPackage.lastIndexOf("("));
                            methodPackage = methodPackage.substring(methodPackage.lastIndexOf(" ")+1);
                            methodPackage = methodPackage.substring(0, methodPackage.lastIndexOf("."));
                            methodPackage = methodPackage.replace(".", "/");
                            String typeName = ASMFlagTranslations.getTypeSpecifier( (arguments[i][2] & 255) );
                            mv.visitMethodInsn(INVOKESTATIC, methodPackage, method.getName(), "(" + typeName + typeName + ")" + typeName);
                            RuntimeCreator.storeValue(mv, arguments[i][1], (arguments[i][2] & 255) );
                        } else if ((arguments[i][2] & 0xFF0000) != WITH_INNER_REDUCTOR) {
                            if ((arguments[i][2] & 0xFF00) == REDUCTOR_ARRAY) {
                                mv.visitVarInsn(ILOAD, currentLocalVariable );
                                mv.visitInsn(DUP);
                                mv.visitInsn(DUP);
                                mv.visitIincInsn(currentLocalVariable, 2);
                                mv.visitVarInsn(ILOAD, currentLocalVariable );

                                mv.visitLdcInsn(new Integer(32) );
                                mv.visitInsn(IREM);
                                //we don't have to resize the array? => jump to this label
                                Label noNewArrayMerge = new Label();
                                mv.visitJumpInsn(IFNE, noNewArrayMerge);

                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable+1 );
                                mv.visitInsn(ICONST_0);
                                mv.visitIincInsn(currentLocalVariable, 32);
                                mv.visitVarInsn(ILOAD, currentLocalVariable );
                                if ((arguments[i][2] & 0xFF) == TYPE_OBJECT)
                                    mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
                                else
                                    mv.visitIntInsn(NEWARRAY, ASMFlagTranslations.getReductorArray(arguments[i][2]));
                                mv.visitInsn(DUP_X2);
                                mv.visitInsn(ICONST_0);
                                mv.visitVarInsn(ILOAD, currentLocalVariable );
                                mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "arraycopy", "(Ljava/lang/Object;ILjava/lang/Object;II)V");
                                mv.visitInsn(DUP);
                                //mv.visitVarInsn(ASTORE, currentLocalVariable+1 );
                                RuntimeCreator.storeValue(mv, arguments[i][3], TYPE_OBJECT);
                                //RuntimeCreator.storeValue (mv, obj[i].getArg( 2 ), TYPE_OBJECT);

                                mv.visitLabel(noNewArrayMerge);
                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable+1 );
                                mv.visitInsn(SWAP);
                                RuntimeCreator.pushValue(mv, arguments[i][0], (arguments[i][2] & 0xFF));
                                mv.visitInsn(ASMFlagTranslations.getArrayInstruction( (arguments[i][2] & 0xFF) | ARRAY_INSTR_STORE ));
                                mv.visitInsn(ICONST_1);
                                mv.visitInsn(IADD);
                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable+1 );
                                mv.visitInsn(SWAP);
                                RuntimeCreator.pushValue(mv, arguments[i][1], (arguments[i][2] & 0xFF));
                                mv.visitInsn(ASMFlagTranslations.getArrayInstruction( (arguments[i][2] & 0xFF) | ARRAY_INSTR_STORE ));
                                currentLocalVariable += 2;
                            } else {
                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable );
                                RuntimeCreator.pushValue(mv, arguments[i][0], TYPE_OBJECT);

                                if ((arguments[i][2] & 0xFF00) != REDUCTOR_SET)
                                    RuntimeCreator.pushValue(mv, arguments[i][1], TYPE_OBJECT);

                                //RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable );

                                //is instance of PSet/AMap
                                Label isInstance = new Label();
                                //if it is no instance of PSet/AMap we hava a java-intern Map/Set, after dealing with that, we jump over the PSet/AMap part, so jump to endInstanceCheck
                                Label endInstanceCheck = new Label();

                                RuntimeCreator.instanceOf(mv, arguments[i][3], ASMFlagTranslations.getReductorTypeName(arguments[i][2]), isInstance, null, false);
                                //mv.visitTypeInsn(INSTANCEOF, ASMFlagTranslations.getReductorTypeName(arguments[i][2]));
                                //mv.visitJumpInsn(IFNE, isInstance);
                                mv.visitMethodInsn(INVOKEVIRTUAL, ASMFlagTranslations.getReductorJavaTypeName(arguments[i][2]), ASMFlagTranslations.getReductorMethodName(arguments[i][2]), "(Ljava/lang/Object;" + ((arguments[i][2] & 0xFF00) == REDUCTOR_SET ? "" : "Ljava/lang/Object;" ) + ")" + ((arguments[i][2] & 0xFF00) == REDUCTOR_SET ? "Z" : "Ljava/lang/Object;" ) );

                                mv.visitJumpInsn(GOTO, endInstanceCheck);
                                mv.visitLabel(isInstance);

                                mv.visitMethodInsn(INVOKEVIRTUAL, ASMFlagTranslations.getReductorTypeName(arguments[i][2]), ASMFlagTranslations.getReductorMethodName(arguments[i][2]), "(Ljava/lang/Object;" + ((arguments[i][2] & 0xFF00) == REDUCTOR_SET ? "" : "Ljava/lang/Object;" ) + ")" + ((arguments[i][2] & 0xFF00) == REDUCTOR_SET ? "Z" : "Ljava/lang/Object;" ) );
                                mv.visitLabel(endInstanceCheck);
                                mv.visitInsn(POP);
                                currentLocalVariable++;
                            }
                        } else {

                            int reductorFlags = obj[i].accept(new ReductorFlagsVisitor() );

                            if ((reductorFlags & 0xFF00) == REDUCTOR_METHOD_ADAPTER) {
                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable );
                                RuntimeCreator.pushValue(mv, arguments[i][0], TYPE_OBJECT);
                            }

                            RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                            //mv.visitVarInsn(ALOAD, currentLocalVariable );
                            RuntimeCreator.pushValue(mv, arguments[i][0], TYPE_OBJECT);

                            mv.visitMethodInsn(INVOKEVIRTUAL, "edu/umass/pql/container/AMap", "get", "(Ljava/lang/Object;)Ljava/lang/Object;" );

                            if ((reductorFlags & 0xFF00) == REDUCTOR_EXISTS) {
                                mv.visitInsn(POP);
                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable );
                                RuntimeCreator.pushValue(mv, arguments[i][0], TYPE_OBJECT);
                                mv.visitInsn(ICONST_1);
                                RuntimeCreator.castValue(mv, TYPE_INT, TYPE_OBJECT);
                                mv.visitMethodInsn(INVOKEVIRTUAL, "edu/umass/pql/container/AMap", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;" );
                                mv.visitInsn(POP);
                            } else if ((reductorFlags & 0xFF00) == REDUCTOR_METHOD_ADAPTER) {
                                mv.visitInsn(DUP);
                                //the object of the N_Map/N_Defaultmap to send to the method is not empty? => jump to this label
                                Label notEmptyObject = new Label();
                                //jump over the non-null part if we have a NULL
                                Label isNullReady = new Label();
                                mv.visitJumpInsn(IFNONNULL, notEmptyObject);
                                mv.visitInsn(POP);
                                mv.visitInsn(ICONST_0);
                                RuntimeCreator.castValue(mv, TYPE_INT, (reductorFlags & 255));
                                mv.visitJumpInsn(GOTO, isNullReady);
                                mv.visitLabel(notEmptyObject);
                                RuntimeCreator.castValue(mv, TYPE_OBJECT, (reductorFlags & 255));
                                mv.visitLabel(isNullReady);
                                RuntimeCreator.pushValue(mv, obj[i].getArg( 0 ), (reductorFlags & 255) );
                                Method method = (Method)(((MethodAdapterReductor)obj[i]).getReductorObject());
                                String methodPackage = method.toString();
                                methodPackage = methodPackage.substring(0, methodPackage.lastIndexOf("("));
                                methodPackage = methodPackage.substring(methodPackage.lastIndexOf(" ")+1);
                                methodPackage = methodPackage.substring(0, methodPackage.lastIndexOf("."));
                                methodPackage = methodPackage.replace(".", "/");
                                String typeName = ASMFlagTranslations.getTypeSpecifier( (reductorFlags & 255) );
                                mv.visitMethodInsn(INVOKESTATIC, methodPackage, method.getName(), "(" + typeName + typeName + ")" + typeName);
                                RuntimeCreator.castValue(mv, (reductorFlags & 255), TYPE_OBJECT);
                                mv.visitMethodInsn(INVOKEVIRTUAL, "edu/umass/pql/container/AMap", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;" );
                                mv.visitInsn(POP);

                            } else {
                                mv.visitInsn(DUP);
                                //the object of the N_Map/N_Defaultmap is not empty? => jump to this label (otherwise, we create a new PSet/PMap/PDefaultMap)
                                Label notEmptyObject = new Label();
                                mv.visitJumpInsn(IFNONNULL, notEmptyObject);
                                mv.visitInsn(POP);
                                RuntimeCreator.pushValue(mv, arguments[i][3], TYPE_OBJECT);
                                //mv.visitVarInsn(ALOAD, currentLocalVariable );
                                RuntimeCreator.pushValue(mv, arguments[i][0], TYPE_OBJECT);
                                mv.visitTypeInsn(NEW, ASMFlagTranslations.getReductorTypeName(reductorFlags));
                                mv.visitInsn(DUP);
                                if ((reductorFlags & 0xFF00) != REDUCTOR_DEFAULTMAP)
                                    mv.visitMethodInsn(INVOKESPECIAL, ASMFlagTranslations.getReductorTypeName(reductorFlags), "<init>", "()V");
                                else {
                                    RuntimeCreator.pushValue(mv, obj[i].getArg(0), TYPE_OBJECT);
                                    mv.visitMethodInsn(INVOKESPECIAL, ASMFlagTranslations.getReductorTypeName(reductorFlags), "<init>", "(Ljava/lang/Object;)V");
                                }
                                mv.visitInsn(DUP_X2);
                                mv.visitMethodInsn(INVOKEVIRTUAL, "edu/umass/pql/container/AMap", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;" );
                                mv.visitInsn(POP);
                                mv.visitLabel(notEmptyObject);

                                mv.visitTypeInsn(CHECKCAST, ASMFlagTranslations.getReductorTypeName(reductorFlags) );
                                RuntimeCreator.pushValue(mv, obj[i].getArg( 0 + ((reductorFlags & 0xFF00) == REDUCTOR_DEFAULTMAP ? 1 : 0) ), TYPE_OBJECT);
                                RuntimeCreator.pushValue(mv, obj[i].getArg( 1 + ((reductorFlags & 0xFF00) == REDUCTOR_DEFAULTMAP ? 1 : 0) ), TYPE_OBJECT);
                                mv.visitMethodInsn(INVOKEVIRTUAL, ASMFlagTranslations.getReductorTypeName(reductorFlags), "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;" );
                                mv.visitInsn(POP);

                            }

                            currentLocalVariable++;
                        }
                    }
                    behaviorStack.list.get(behaviorStack.list.size()-1).insertReturnCode(false, mv, behaviorStack);
                } else
                    mv.visitJumpInsn(GOTO, label);
                break;

            default:
                throw new Exception("unknown return-behavior.");
        }
    }

    protected int getType () {
        return type;
    }

}
