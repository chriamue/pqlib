/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import edu.umass.pql.VarConstants;
import edu.umass.pql.il.reductor.ArrayReductor;
import edu.umass.pql.il.reductor.DefaultMapReductor;
import edu.umass.pql.il.reductor.Exists;
import edu.umass.pql.il.reductor.MapReductor;
import edu.umass.pql.il.reductor.MethodAdapterReductor;
import edu.umass.pql.il.reductor.SetReductor;
import rc2.representation.PQLReductorExtension;
import rc2.representation.PQLReductorExtension.ReductorExtensionName;

/**
 *
 * @author Hilmar
 */
public class ReductorFlagsVisitor implements BcFlags, VarConstants {

    public int visit (SetReductor e) {
        return REDUCTOR_SET;
    }

    public int visit (ArrayReductor.AnyObject e) {
        return REDUCTOR_ARRAY | TYPE_OBJECT;
    }

    public int visit (ArrayReductor.Int e) {
        return REDUCTOR_ARRAY | TYPE_INT;
    }

    public int visit (ArrayReductor.Long e) {
        return REDUCTOR_ARRAY | TYPE_LONG;
    }

    public int visit (ArrayReductor.Double e) {
        return REDUCTOR_ARRAY | TYPE_DOUBLE;
    }

    public int visit (ArrayReductor.Byte e) {
        return REDUCTOR_ARRAY | TYPE_BYTE;
    }

    public int visit (ArrayReductor.Char e) {
        return REDUCTOR_ARRAY | TYPE_CHAR;
    }

    public int visit (ArrayReductor.Short e) {
        return REDUCTOR_ARRAY | TYPE_SHORT;
    }

    public int visit (ArrayReductor.Boolean e) {
        return REDUCTOR_ARRAY | TYPE_BOOLEAN;
    }

    public int visit (ArrayReductor.Float e) {
        return REDUCTOR_ARRAY | TYPE_FLOAT;
    }

    public int visit (MapReductor e) {
        if (e.getInnerReductor() == null)
            return REDUCTOR_MAP;
        else
            return REDUCTOR_MAP | WITH_INNER_REDUCTOR;
    }

    public int visit (DefaultMapReductor e) {
        if (e.getInnerReductor() == null)
            return REDUCTOR_DEFAULTMAP;
        else
            return REDUCTOR_DEFAULTMAP | WITH_INNER_REDUCTOR;
    }

    public int visit (Exists e) {
        return REDUCTOR_EXISTS;
    }

    public int visit (MethodAdapterReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_OBJECT;
    }

    public int visit (MethodAdapterReductor.IntegerMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_INT;
    }

    public int visit (MethodAdapterReductor.BooleanMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_BOOLEAN;
    }

    public int visit (MethodAdapterReductor.ShortMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_SHORT;
    }

    public int visit (MethodAdapterReductor.CharacterMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_CHAR;
    }

    public int visit (MethodAdapterReductor.ByteMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_BYTE;
    }

    public int visit (MethodAdapterReductor.LongMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_LONG;
    }

    public int visit (MethodAdapterReductor.FloatMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_FLOAT;
    }

    public int visit (MethodAdapterReductor.DoubleMethodReductor e) {
        return REDUCTOR_METHOD_ADAPTER | TYPE_DOUBLE;
    }

    public int visit (PQLReductorExtension e) {
        ReductorExtensionName extensionName = e.getExtension();
        if (extensionName == null)
            return (e.getBCRExtension().getId() << 8);
        switch (extensionName) {
            default:
                return -1;
        }
    }

}
