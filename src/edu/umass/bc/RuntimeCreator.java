/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import org.objectweb.asm.*;
import edu.umass.pql.*;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.Arithmetic;
import edu.umass.pql.il.Field;
import edu.umass.pql.il.NoFail.Bool;
import edu.umass.pql.il.ReductionImpl;
import edu.umass.pql.il.meta.SelectPath;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.objectweb.asm.util.TraceClassVisitor;
import rc2.bcr.EqualPredicate;

/**
 *
 * @author Hilmar
 */
public class RuntimeCreator implements Opcodes, VarConstants, BcFlags  {

    //TODO:


    //compiler, jetzt kommen andere fehler 1/3 durchlaeufen erfolgreich

    //tool zum vergleichen von arrays: datei.txt beide arrays aneinandergehaengt => cat datei.txt | sort | uniq

    /*fertige Optimierungen:    nicht immer aus env-laden, sondern in lokalen variablen
     *                          konstanten inline
     *                          envReplace ab 1000 ist schlecht, doch eher iterativ machen
     *                          IINC statt iadd
     *                          SWAP raus
     *                          checkcast wenn moeglich nur einmal beim register laden
     *                          versuchen, dass sinnlos instance zu loeschen (wir wissen, dass wir ein PSet haben, also koenen wir das direkt nutzen)
    */

    //Optimierungen:

    /* bei L2X: doppeltes laden verhindern (etwas schwieriger als es aussieht..)
     ILOAD 7
         ALOAD 5
         SWAP
         AALOAD
         DUP
         IFNULL L4
         POP
         ILOAD 7
         ALOAD 5
         SWAP
         AALOAD
     */

    //java-map toArray() implemetnieren mit benutzerdefinierter Funktion
    //compiler nochmal schauen..
    //aritmetitest sollte wieder funktionieren

    //package bei codeGenerator mit angeben

    /*
     * Object -> Object * Object (Object lesbar / Object * Object les-und schreibbar)
     * array-lookup nur value auch schreibbar
     * ./javac blub.java => es wird unzip noch benoetigt
     *
     * //benchmarks.webgraph.Generator.init(); in Bench.java musste auskommentiert werden => zuwenig heap..
     *
     * benchmarks in pql implementieren
     * R s <- (0.123, 0.3242, ..)
     * R boxplot(s)
     * in allen benchmarks Manual.java
     * versuchen, per makefile compiler zu erzeugen
     * bool nochmal richtig
     *
     * ==============
     *
     * code etwas "aufraeumen"
     * Strings so einbauen, dass sie im Code der Ausgabe sichtbar sind..
     *
     * Notes:
     * noch nicht implementiert: arrays, die in N_map / Default_n_map als inner_reductor verwendet werden..
     * bei arrays, die %32 != 0 sind, kann es bei 0/0 abfragen zu problemen kommen.. (allgemein, darf auch nicht auf null abgefragt werden..)
     *
     * fehlende Implementierungen:
     * ========
     * (POLY_SIZE/SET_SIZE/MAP_SIZE/..S)
     * JAVA_TYPE / INT / CHAR => typ ueberpruefung
     * INSTANTIATE
     * DisjunctiveBlock / BOOL / NOFAIL / NOT / FORALL
     */



      /*public static evaluate(Ledu/umass/pql/Env;)Z
    GOTO L0
   L1
    NEW edu/umass/pql/container/PSet
    DUP
    INVOKESPECIAL edu/umass/pql/container/PSet.<init> ()V
    DUP
    ASTORE 2
    ASTORE 3
    ALOAD 4
    DUP
    ASTORE 5
    ARRAYLENGTH
    ISTORE 6
    LDC 0
    ISTORE 7
   L2
    ILOAD 6
    ILOAD 7
    IF_ICMPEQ L3
    ILOAD 7
    ALOAD 5
    SWAP
    AALOAD
    DUP
    IFNULL L4
    POP
    ILOAD 7
    ALOAD 5
    SWAP
    AALOAD
    ASTORE 8
    GOTO L5
    ALOAD 2
    ALOAD 8
    ALOAD 2
    INSTANCEOF edu/umass/pql/container/PSet
    IFNE L6
    INVOKEVIRTUAL java/util/Set.add (Ljava/lang/Object;)Z
    GOTO L7
   L6
    INVOKEVIRTUAL edu/umass/pql/container/PSet.add (Ljava/lang/Object;)Z
   L7
    POP
    GOTO L8
    GOTO L5
   L4
    POP
   L9
    ILOAD 7
    LDC 1
    IADD
    ISTORE 7
    GOTO L2
   L3
    GOTO L8
   L5
    ALOAD 8
    DUP
    ASTORE 9
    ARRAYLENGTH
    ISTORE 10
    LDC 0
    ISTORE 11
   L10
    ILOAD 10
    ILOAD 11
    IF_ICMPEQ L11
    ILOAD 11
    ALOAD 9
    SWAP
    BALOAD
    LDC 48
    I2B
    IF_ICMPEQ L12
    GOTO L13
    GOTO L14
   L12
   L14
    ILOAD 11
    ISTORE 12
    GOTO L15
    GOTO L15
   L16
    POP
   L13
    ILOAD 11
    LDC 1
    IADD
    ISTORE 11
    GOTO L10
   L11
    GOTO L9
   L15
    ILOAD 12
    LDC 0
    IF_ICMPGE L17
    GOTO L18
   L17
    ILOAD 12
    LDC 97
    IF_ICMPLE L19
    GOTO L18
   L19
    GOTO L20
   L18
    GOTO L13
   L20
    ILOAD 12
    LDC 1
    IADD
    ISTORE 13
    ALOAD 8
    DUP
    ASTORE 14
    ARRAYLENGTH
    ISTORE 15
    ILOAD 13
    ISTORE 16
   L21
    ILOAD 15
    ILOAD 16
    IF_ICMPEQ L22
    ILOAD 16
    ALOAD 14
    SWAP
    BALOAD
    LDC 49
    I2B
    IF_ICMPEQ L23
    GOTO L24
    GOTO L25
   L23
   L25
    GOTO L26
   L27
    POP
   L24
   L22
    GOTO L13
   L26
    ILOAD 12
    LDC 2
    IADD
    ISTORE 17
    ALOAD 8
    DUP
    ASTORE 18
    ARRAYLENGTH
    ISTORE 19
    ILOAD 17
    ISTORE 20
   L28
    ILOAD 19
    ILOAD 20
    IF_ICMPEQ L29
    ILOAD 20
    ALOAD 18
    SWAP
    BALOAD
    LDC 50
    I2B
    IF_ICMPEQ L30
    GOTO L31
    GOTO L32
   L30
   L32
    GOTO L33
   L34
    POP
   L31
   L29
    GOTO L13
   L33
    ALOAD 2
    ALOAD 8
    ALOAD 2
    INSTANCEOF edu/umass/pql/container/PSet
    IFNE L35
    INVOKEVIRTUAL java/util/Set.add (Ljava/lang/Object;)Z
    GOTO L36
   L35
    INVOKEVIRTUAL edu/umass/pql/container/PSet.add (Ljava/lang/Object;)Z
   L36
    POP
    GOTO L13
   L8
    GOTO L37
    MAXSTACK = 0
    MAXLOCALS = 0
}

#########################
###  Code von Manual  ###
#########################

    NEW edu/umass/pql/container/PSet
    DUP
    INVOKESPECIAL edu/umass/pql/container/PSet.<init> ()V
    ASTORE 1
   L1
    GETSTATIC benchmarks/threegrep/Generator.data_array : [[B
    ASTORE 2
   L2
    ALOAD 2
    ARRAYLENGTH
    ISTORE 3
   L3
    ICONST_0
    ISTORE 4
   L4
    ILOAD 4
    ILOAD 3
    IF_ICMPGE L5
    ALOAD 2
    ILOAD 4
    AALOAD
    ASTORE 5
   L6
    ICONST_0
    ISTORE 6
   L7
    ILOAD 6
    BIPUSH 97
    IF_ICMPGT L8
   L9
    ALOAD 5
    ILOAD 6
    BALOAD
    BIPUSH 48
    IF_ICMPNE L10
    ALOAD 5
    ILOAD 6
    ICONST_1
    IADD
    BALOAD
    BIPUSH 49
    IF_ICMPNE L10
    ALOAD 5
    ILOAD 6
    ICONST_2
    IADD
    BALOAD
    BIPUSH 50
    IF_ICMPNE L10
   L11
    ALOAD 1
    ALOAD 5
    INVOKEVIRTUAL edu/umass/pql/container/PSet.add (Ljava/lang/Object;)Z
    POP
   L12
    GOTO L8
   L10
    IINC 6 1
    GOTO L7
   L8
    IINC 4 1
    GOTO L4
   L5*/

    public static final boolean quietRuntimeCreator = true;
    public static boolean useRuntimeCreator = true;
    public static final boolean writeCommentsInCode = false;

    public static boolean alreadyInitialized = false;

    private static Method pql_eval = null;
    private static Join pql_query = null;
    private static ASMFlagTranslations ast = new ASMFlagTranslations ();
    private static ReturnBehaviorStack returnBehaviorStack;
    private static int usedLocalVariables;
    private static EnvReplace envReplace;
    protected static Env initEnv;
    protected static Map <Integer, String> typeInformation = new HashMap <Integer, String> ();
    protected static Class dynamicClass;

    public static void init (Join new_query, Env _initEnv) {
        initEnv = _initEnv;
        pql_query = new_query;
        pql_eval = null;
        RuntimeClassLoader cl = new RuntimeClassLoader ();
        try {
            dynamicClass = cl.defineClass("Evaluator", genEvalFor(pql_query) );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearTypeInformations() {
        typeInformation = new HashMap <Integer, String> ();
    }

    public static void setTypeInformation (int index, Env env) {
        typeInformation.put(index, env.v_object[index].getClass().getName().replace(".", "/"));
    }

    public static void setTypeInformation (int index, String typeName) {
        typeInformation.put(index, typeName);
    }

    public static boolean test (Env env) {
        try {
            if (pql_eval == null) {
                for (int i=0; i<dynamicClass.getMethods().length; i++) {
                    if (dynamicClass.getMethods()[i].getName().equals("evaluate")) {
                        pql_eval = dynamicClass.getMethods()[i];
                        break;
                    }
                }
            }
            Boolean result = (Boolean)pql_eval.invoke(null, new Object[] { env  });
            /*System.out.println("AAAAAAAAAAAAAAAAAA");
            int fs = 34;
            if (fs!=0)
                throw new Exception("sdfds");*/
            if (!quietRuntimeCreator)
                System.out.println("  Ergebnis: " + result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static byte[] genEvalFor (Join pql_query) throws Exception {

        /*Map <String, String> fs = new HashMap <String, String> ();
        Set <String> fs2 = new HashSet <String> ();
        List <String> fs3 = new ArrayList <String> ();
        fs3.add("abc");
        fs3.add("deffh");
        fs3.add("ghi");
        fs3.stream().filter(new EqualPredicate (new Integer (324)));
        fs3.stream().filter(new Predicate<String>() {
          @Override
          public boolean test(String element) {
              return element.length() > 4;
          }}).forEach(new Consumer <String> () {
              @Override
          public void accept(String element) {
              System.out.println(element);
          }
          });*/

        /*Set<Webdoc>fdsa = Generator.genDocuments();
        Object [] fsa = fdsa.toArray();
        Webdoc[] fasd = (Webdoc[])fsa;*/

        /*int test = 3;
        int abc = 3;
        switch (test) {
            case 0:
                abc = 5;
            case 1:
                abc = 7;
                break;
            default:
                abc = 2;
        }*/


        /*ClassReader cr2 = new ClassReader("edu.umass.bc.RuntimeCreator");
        ClassVisitor cv2 = new ClassWriter(cr2, 0);
        PrintWriter pw2 = new PrintWriter(System.out);
        TraceClassVisitor cw2 = new TraceClassVisitor(cv2, pw2);
        cr2.accept(cw2, 0);
        int i=0;
        if (i != 3452) {
            Thread.sleep(100);
            throw new Exception("BREAK"); }*/


        usedLocalVariables = 1;
        returnBehaviorStack = new ReturnBehaviorStack ();
        envReplace = new EnvReplace(initEnv);

        ClassWriter cv;
        ClassVisitor cw = null;
        if (quietRuntimeCreator) {
            cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
            cw = cv;
        } else {
            cv = new ClassWriter(ClassWriter.COMPUTE_FRAMES|ClassWriter.COMPUTE_MAXS);
            PrintWriter pw = new PrintWriter(System.out);
            cw = new TraceClassVisitor(cv, pw);
        }
        FieldVisitor fv;
        MethodVisitor mv;
        AnnotationVisitor av0;

        cw.visit(V1_6, ACC_PUBLIC, "Evaluator", null, "java/lang/Object", null);

        cw.visitSource("Evaluator.java", null);

        {
            mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(1, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
            mv.visitInsn(RETURN);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLocalVariable("this", "LEvaluator;", null, l0, l1, 0);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        {
            mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "evaluate", "(Ledu/umass/pql/Env;)Z", null, null);
            mv.visitCode();

            envReplace.preCode(mv);
            createASMCode (pql_query, mv);
            envReplace.postCode(mv);
            mv.visitInsn(ICONST_1);
            mv.visitInsn(IRETURN);

            mv.visitMaxs(0, 0);
            mv.visitEnd();
        }
        cw.visitEnd();

        return cv.toByteArray();
    }

    private static void createASMCode (Join query, MethodVisitor mv) throws Exception {
        int flags = -1;
        flags = query.accept(new JoinFlagsVisitor());
        if ((flags & 0xE000) == 0) {
            if ((flags & 0xFF00) <= TYPE_BITSSHR)
                createArithmeticCode(mv, query, flags);
            else
                createComparisonCode(mv, query, flags);
        }
        else
            createSpecialInstructionCode(mv, query, flags);
    }

    private static void createSpecialInstructionCode (MethodVisitor mv, Join query, int flags) throws Exception {

        int type = flags & 0xFF;
        int instruction = (flags & 0xFF00);

        switch (instruction) {
            case TYPE_CONJUCTIVE:
                insertComment(mv, "START_CONJUCTIVE");
                returnBehaviorStack.saveStack();
                Join [] elements = ((AbstractBlock.PreConjunctive)query).getComponents();
                for (int i=0; i<elements.length; i++ ) {
                    createASMCode(elements[i], mv);
                }

                returnBehaviorStack.insertReturnCode(true, mv);
                returnBehaviorStack.restoreStack();
                break;

            case TYPE_BOOL:
                createASMCode( ((Bool)query).getComponentInternal(0), mv);
                break;

            case TYPE_RANGE_CONTAINS:
                insertComment(mv, "START_RANGE_CONTAINS");
                if ((query.getArg(2) & VAR_READ_FLAG) != 0) {
                    //we jump to this label, if we checked, that the iteration-variable is in the range
                    Label checkedLabel = new Label();
                    //jump, if iteration-label is NOT in the range
                    Label notInRangeLabel = new Label();
                    for (int i=0; i<2; i++) {
                        //jump, if we checked one part of the iteration-variable (if it is in the range)  (i = 0: val >= min / i = 1: val <= max )
                        Label checkedLabelTmp = new Label();
                        pushValue(mv, query.getArg(2), type);
                        pushValue(mv, query.getArg(i), type);
                        if (type == TYPE_INT)   mv.visitJumpInsn( (i == 0 ? IF_ICMPGE : IF_ICMPLE), checkedLabelTmp);
                        else {
                            mv.visitInsn(LCMP);
                            mv.visitJumpInsn( (i == 0 ? IFGE : IFLE), checkedLabelTmp);
                        }
                        mv.visitJumpInsn(GOTO, notInRangeLabel);
                        mv.visitLabel(checkedLabelTmp);
                    }
                    mv.visitJumpInsn(GOTO, checkedLabel);
                    mv.visitLabel(notInRangeLabel);
                    returnBehaviorStack.insertReturnCode(false, mv);
                    mv.visitLabel(checkedLabel);
                }
                else {
                    int usedLocalVariablesTmp = usedLocalVariables;
                    usedLocalVariables += (type == TYPE_LONG ? 4 : 2);
                    pushValue(mv, query.getArg(1), type);
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, type), 1 + usedLocalVariablesTmp);
                    pushValue(mv, query.getArg(0), type);
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, type), (type == TYPE_LONG ? 3 : 2) + usedLocalVariablesTmp );
                    //iteration-step of range_contains
                    Label iterationLabel = new Label();
                    //here we check whether the iteration-variable is in the range
                    Label checkLabel = new Label();
                    //we jump here, when the loop ends
                    Label endLabel = new Label();
                    mv.visitJumpInsn(GOTO, checkLabel);

                    mv.visitLabel(iterationLabel);
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, type), (type == TYPE_LONG ? 3 : 2) + usedLocalVariablesTmp );
                    if (type == TYPE_INT)       mv.visitInsn(ICONST_1);
                    else                        mv.visitInsn(LCONST_1);
                    mv.visitInsn( ast.getArithmeticInstruction(TYPE_ADD, type) );
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, type), (type == TYPE_LONG ? 3 : 2) + usedLocalVariablesTmp );

                    mv.visitLabel(checkLabel);
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, type), (type == TYPE_LONG ? 3 : 2) + usedLocalVariablesTmp );
                    storeValue(mv, query.getArg(2), type);
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, type), 1 + usedLocalVariablesTmp );
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, type), (type == TYPE_LONG ? 3 : 2) + usedLocalVariablesTmp );
                    if (type == TYPE_INT)   mv.visitJumpInsn(IF_ICMPGE, endLabel);
                    else {
                        mv.visitInsn(LCMP);
                        mv.visitJumpInsn(IFGE, endLabel);
                    }

                    returnBehaviorStack.insertReturnCode(false, mv);

                    mv.visitLabel(endLabel);
                    returnBehaviorStack.push( ReturnBehavior.getJumpBehavior(iterationLabel) );
                }
                break;

            case TYPE_REDUCTION: {
                insertComment(mv, "START_REDUCTION");
                Join body = ((ReductionImpl)query).getComponentInternal(0);
                Reductor[] reductors = ((ReductionImpl)query).getReductors();
                int startUsedLocalVariables = usedLocalVariables;
                int usedLocalVariablesTmp = usedLocalVariables;
                for (int i=0; i<reductors.length; i++) {
                    int reductorFlags = reductors[i].accept(new ReductorFlagsVisitor() );
                    if ((reductorFlags & 0xFF00) != REDUCTOR_EXISTS && (reductorFlags & 0xFF00) != REDUCTOR_METHOD_ADAPTER)
                        usedLocalVariables += ((reductorFlags & 0xFF00) == REDUCTOR_ARRAY ? 2 : 1);
                }

                int reductorsArguments [][] = new int [reductors.length][4];
                for (int i=0; i<reductors.length; i++) {
                    int reductorFlags = reductors[i].accept(new ReductorFlagsVisitor() );
                    if ((reductorFlags & 0xFF00) == REDUCTOR_EXISTS) {
                        mv.visitInsn(ICONST_0);
                        storeValue(mv, reductors[i].getArg( 1 ), TYPE_INT);
                        reductorsArguments[i][1] = reductors[i].getArg( 1 );
                        reductorsArguments[i][2] = reductorFlags;
                    } else if ((reductorFlags & 0xFF00) == REDUCTOR_METHOD_ADAPTER) {
                        reductorsArguments[i][0] = reductors[i].getArg( 0 );
                        reductorsArguments[i][1] = reductors[i].getArg( 1 );
                        reductorsArguments[i][2] = reductorFlags;
                    } else {
                        if ((reductorFlags & 0xFF00) == REDUCTOR_ARRAY)
                        {
                            mv.visitInsn(ICONST_0);
                            mv.visitVarInsn(ISTORE, 1 + usedLocalVariablesTmp );
                            ++usedLocalVariablesTmp;
                            mv.visitIntInsn(BIPUSH, 32);
                            if ((reductorFlags & 0xFF) == TYPE_OBJECT)
                                mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
                            else
                                mv.visitIntInsn(NEWARRAY, ast.getReductorArray(reductorFlags));
                        }
                        else {
                            mv.visitTypeInsn(NEW, ast.getReductorTypeName(reductorFlags));
                            mv.visitInsn(DUP);
                            if ((reductorFlags & 0xFF00) != REDUCTOR_DEFAULTMAP)
                                mv.visitMethodInsn(INVOKESPECIAL, ast.getReductorTypeName(reductorFlags), "<init>", "()V");
                            else {
                                pushValue(mv, reductors[i].getArg(0), TYPE_OBJECT);
                                mv.visitMethodInsn(INVOKESPECIAL, ast.getReductorTypeName(reductorFlags), "<init>", "(Ljava/lang/Object;)V");
                            }
                        }

                        //====
                        //mv.visitInsn(DUP);
                        //mv.visitVarInsn(ASTORE, 1 + usedLocalVariablesTmp );
                        //====

                        ++usedLocalVariablesTmp;
                        storeValue (mv, reductors[i].getArg( ((reductorFlags & 0xFF00) == REDUCTOR_SET ? 1 : 2) + ((reductorFlags & 0xFF00) == REDUCTOR_DEFAULTMAP ? 1 : 0) ), TYPE_OBJECT);
                        reductorsArguments[i][0] = reductors[i].getArg( 0 + ((reductorFlags & 0xFF00) == REDUCTOR_DEFAULTMAP ? 1 : 0) );
                        if ((reductorFlags & 0xFF00) != REDUCTOR_SET)
                            reductorsArguments[i][1] = reductors[i].getArg( 1 + ((reductorFlags & 0xFF00) == REDUCTOR_DEFAULTMAP ? 1 : 0) );
                        reductorsArguments[i][2] = reductorFlags;
                        reductorsArguments[i][3] = reductors[i].getArg( ((reductorFlags & 0xFF00) == REDUCTOR_SET ? 1 : 2) + ((reductorFlags & 0xFF00) == REDUCTOR_DEFAULTMAP ? 1 : 0) );
                        if ((reductorFlags & 0xFF0000) == WITH_INNER_REDUCTOR)
                            reductors[i] = reductors[i].getInnerReductor();
                    }
                }

                //the body of reduction is finished => jump to this label
                Label endLabelReduction = new Label();
                int sizeStackEntriesBefore = returnBehaviorStack.list.size();
                returnBehaviorStack.saveStack();
                returnBehaviorStack.push( ReturnBehavior.getReductionBehavior(endLabelReduction, startUsedLocalVariables, reductorsArguments, reductors ) );
                createASMCode(body, mv);
                returnBehaviorStack.insertReturnCode(true, mv);
                returnBehaviorStack.restoreStack();
                mv.visitLabel(endLabelReduction);
                while (returnBehaviorStack.list.size() > sizeStackEntriesBefore)
                    returnBehaviorStack.list.remove(returnBehaviorStack.list.size()-1);
                break;
            }

            case TYPE_ARRAY_LOOKUP: {
                insertComment(mv, "START ARRAY-LOOKUP");
                int source_ty = type;
                //every iteration-step jumps here
                Label nextIteration = new Label();
                //the end of the array-lookup
                Label finishContains = new Label();
                pushValue(mv, query.getArg(0), TYPE_OBJECT);

                if (checkCast(mv, query.getArg(0), "[" + ast.getTypeSpecifier( (type) ) )) {
                    //mv.visitTypeInsn(CHECKCAST, "[" + ast.getTypeSpecifier( (type) ) );
                    //envReplace.addPossibleCast(mv, query.getArg(0), "[" + ast.getTypeSpecifier( (type) ));
                    mv.visitInsn(DUP);
                    storeValue(mv, query.getArg(0), TYPE_OBJECT);
                }
                int usedLocalVariablesTmp = usedLocalVariables;
                usedLocalVariables += 2;
                //mv.visitVarInsn(ASTORE, 1 + usedLocalVariablesTmp );

                mv.visitInsn(ARRAYLENGTH);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, TYPE_INT), 2 + usedLocalVariablesTmp);
                if ((query.getArg(1) & VAR_READ_FLAG) == 0)
                    mv.visitInsn(ICONST_0);
                else
                    pushValue(mv, query.getArg(1), TYPE_INT);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, TYPE_INT), 1 + usedLocalVariablesTmp);

                //after iteration jump back to here, so we can check, if iteration-variable is still in range
                Label startLoop = new Label();
                mv.visitLabel(startLoop);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 2 + usedLocalVariablesTmp);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 1 + usedLocalVariablesTmp);

                //end of the loop? => jump here
                Label endArrayCheck = new Label();
                mv.visitJumpInsn(IF_ICMPEQ, endArrayCheck);

                //mv.visitVarInsn(ALOAD, 1 + usedLocalVariablesTmp );
                pushValue(mv, query.getArg(0), TYPE_OBJECT);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 1 + usedLocalVariablesTmp);
                //mv.visitTypeInsn(CHECKCAST, "[" + ast.getTypeSpecifier( (type) ) );
                mv.visitInsn(ast.getArrayInstruction(ARRAY_INSTR_LOAD|type));
                //if element is null, we have to delete this element from stack, before we continue iterating
                Label elementIsNull = new Label();
                if (type == TYPE_OBJECT) {
                    mv.visitInsn(DUP);
                    mv.visitJumpInsn(IFNULL, elementIsNull);
                }

                if ( (query.getArg(2) & VAR_READ_FLAG) != 0 ) {
                    pushValue(mv, query.getArg(2), source_ty);
                    createComparisonCode(mv, null, TYPE_EQ | source_ty, ReturnBehavior.getJumpBehavior(nextIteration));
                } else
                    mv.visitInsn(POP);

                //write something?
                for (int i=0; i<2;i++) {
                    if ( (query.getArg(i+1) & VAR_READ_FLAG) == 0 && query.getArg(i+1) != __ ) {
                        if (i == 1) {
                            //mv.visitVarInsn(ALOAD, 1 + usedLocalVariablesTmp );
                            pushValue(mv, query.getArg(0), TYPE_OBJECT);
                            mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 1 + usedLocalVariablesTmp);
                            //mv.visitTypeInsn(CHECKCAST, "[" + ast.getTypeSpecifier( (type) ) );
                            int arrayType = type;
                            mv.visitInsn(ast.getArrayInstruction(ARRAY_INSTR_LOAD|arrayType));
                            storeValue(mv, query.getArg(i+1), arrayType);
                        } else {
                            mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 1 + usedLocalVariablesTmp);
                            storeValue(mv, query.getArg(i+1), TYPE_INT);
                        }
                    }
                }
                if ( (query.getArg(1) & VAR_READ_FLAG) == 0 || (query.getArg(2) & VAR_READ_FLAG) == 0)
                    mv.visitJumpInsn(GOTO, finishContains);

                returnBehaviorStack.insertReturnCode(true, mv);
                mv.visitJumpInsn(GOTO, finishContains);
                mv.visitLabel(elementIsNull);
                mv.visitInsn(POP);
                mv.visitLabel(nextIteration);
                if ((query.getArg(1) & VAR_READ_FLAG) == 0) {
                    mv.visitIincInsn(1 + usedLocalVariablesTmp, 1);
                    mv.visitJumpInsn(GOTO, startLoop);
                }

                mv.visitLabel(endArrayCheck);
                if ((query.getArg(1) & VAR_READ_FLAG) == 0 || (query.getArg(2) & VAR_READ_FLAG) == 0) {
                    returnBehaviorStack.insertReturnCode(false, mv);
                    returnBehaviorStack.push( ReturnBehavior.getJumpBehavior(nextIteration) );
                } else
                    returnBehaviorStack.insertReturnCode(false, mv);
                mv.visitLabel(finishContains);
                break;
            }

            case TYPE_CONTAINS:
            case TYPE_LOOKUP: {
                insertComment(mv, (instruction == TYPE_CONTAINS ? "START CONTAINS" : "START LOOKUP") );

                mv.visitInsn(ACONST_NULL);
                int usedLocalVariablesTmp = usedLocalVariables;
                usedLocalVariables += 3;
                mv.visitVarInsn(ASTORE, 1 + usedLocalVariablesTmp);
                mv.visitInsn(ICONST_0);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, TYPE_INT), 2 + usedLocalVariablesTmp);
                mv.visitInsn(ICONST_0);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, TYPE_INT), 3 + usedLocalVariablesTmp);

                final int elementLength = (instruction == TYPE_CONTAINS ? 1 : 2);

                int source_ty = (query.getArg(1) >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
                //iteration-label
                Label nextIteration = new Label();
                //the end of the whole command
                Label finishContains = new Label();
                pushValue(mv, query.getArg(0), TYPE_OBJECT);

                if (instruction != TYPE_CONTAINS) {
                    Label isInstance = new Label();
                    instanceOf(mv, query.getArg(0), ast.getContainerName(flags), isInstance, null, true);

                    pushValue(mv, query.getArg(1), TYPE_OBJECT);
                    mv.visitMethodInsn(INVOKEINTERFACE, ast.getJavaContainerName(flags), ast.getJavaContainerMethodName(flags), (instruction == TYPE_CONTAINS ? "(Ljava/lang/Object;)Z" : "(Ljava/lang/Object;)Ljava/lang/Object;") );
                    //jump here, if the Set/Map doesn't contain the searched content
                    Label javaComparisonFailed = new Label();
                    if (instruction == TYPE_CONTAINS)
                        mv.visitJumpInsn(IFEQ, javaComparisonFailed);
                    else {
                        mv.visitInsn(DUP);
                        //in Map we can get a NULL, if the content is not in the Map, but we want to check first if key and value matches, so we have to handle with the NULL
                        Label javaComparisonNotNull = new Label();
                        mv.visitJumpInsn(IFNONNULL, javaComparisonNotNull);
                        mv.visitInsn(POP);
                        mv.visitJumpInsn(GOTO, javaComparisonFailed);

                        mv.visitLabel(javaComparisonNotNull);
                        pushValue(mv, query.getArg(2), TYPE_OBJECT);
                        createComparisonCode(mv, null, TYPE_EQ | TYPE_OBJECT, ReturnBehavior.getJumpBehavior(javaComparisonFailed));
                    }

                    returnBehaviorStack.insertReturnCode(true, mv);
                    mv.visitJumpInsn(GOTO, finishContains);
                    mv.visitLabel(javaComparisonFailed);
                    returnBehaviorStack.insertReturnCode(false, mv);
                    mv.visitJumpInsn(GOTO, finishContains);

                    mv.visitLabel(isInstance);
                    checkCast(mv, query.getArg(0), ast.getContainerName (flags) );
                    //mv.visitTypeInsn(CHECKCAST, ast.getContainerName(flags) );
                }

                //jump, if we have a PSet or AMap
                Label isInstance = new Label();
                //if we have an other Set (other Map is handled in the code above), we use toArray() and jump to this label, so we jump over getRepresentation, what we just need for PSet/AMap
                Label isNotInstance = new Label();
                if (instruction == TYPE_CONTAINS) {
                    instanceOf(mv, query.getArg(0), ast.getContainerName(flags), isInstance, null, true);
                    /*mv.visitInsn(DUP);
                    mv.visitTypeInsn(INSTANCEOF, ast.getContainerName(flags));
                    mv.visitJumpInsn(IFNE, isInstance);   */

                    checkCast(mv, query.getArg(0), "java/util/Set" );
                    //mv.visitTypeInsn(CHECKCAST, "java/util/Set" );
                    mv.visitMethodInsn(INVOKEINTERFACE, "java/util/Set", "toArray", "()[Ljava/lang/Object;" );
                    mv.visitInsn(DUP);
                    mv.visitVarInsn(ASTORE, 1 + usedLocalVariablesTmp );

                    mv.visitJumpInsn(GOTO, isNotInstance);
                    mv.visitLabel(isInstance);
                    checkCast(mv, query.getArg(0), ast.getContainerName(flags) );
                    //mv.visitTypeInsn(CHECKCAST, ast.getContainerName(flags) );
                }
                mv.visitMethodInsn(INVOKEVIRTUAL, ast.getContainerName(flags), "getRepresentation", "()[Ljava/lang/Object;" );
                //mv.visitTypeInsn(CHECKCAST, "[Lbenchmarks/webgraph/Webdoc;" );

                mv.visitInsn(DUP);
                mv.visitVarInsn(ASTORE, 1 + usedLocalVariablesTmp );

                mv.visitLabel(isNotInstance);

                mv.visitInsn(ARRAYLENGTH);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_STORE, TYPE_INT), 2 + usedLocalVariablesTmp);

                //after iteration jump back to here, so we can check, if iteration-variable is still in range
                Label startLoop = new Label();
                mv.visitLabel(startLoop);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 2 + usedLocalVariablesTmp);
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 3 + usedLocalVariablesTmp);

                //end of the loop? => jump to endArrayCheck
                Label endArrayCheck = new Label();
                mv.visitJumpInsn(IF_ICMPLE, endArrayCheck);

                //if element is null, we have to delete this element from stack, before we continue iterating
                Label elementIsNull = new Label();
                mv.visitVarInsn(ALOAD, 1 + usedLocalVariablesTmp );
                mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 3 + usedLocalVariablesTmp);

                mv.visitInsn(ast.getArrayInstruction(ARRAY_INSTR_LOAD|TYPE_OBJECT));
                mv.visitInsn(DUP);
                mv.visitJumpInsn(IFNULL, elementIsNull);

                if ( (query.getArg(1) & VAR_READ_FLAG) != 0 ) {
                    if (instruction != TYPE_ARRAY_LOOKUP)
                        castValue(mv, TYPE_OBJECT, source_ty );
                    pushValue(mv, query.getArg(1), source_ty);
                    createComparisonCode(mv, null, TYPE_EQ | source_ty, ReturnBehavior.getJumpBehavior(nextIteration));
                } else
                    mv.visitInsn(POP);

                if (instruction == TYPE_LOOKUP) {
                    mv.visitIincInsn(3 + usedLocalVariablesTmp, 1);
                    mv.visitVarInsn(ALOAD, 1 + usedLocalVariablesTmp );
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 3 + usedLocalVariablesTmp);
                    mv.visitInsn(ast.getArrayInstruction(ARRAY_INSTR_LOAD|TYPE_OBJECT));
                    mv.visitInsn(DUP);
                    mv.visitJumpInsn(IFNULL, elementIsNull);

                    if ( (query.getArg(2) & VAR_READ_FLAG) != 0 ) {
                        castValue(mv, TYPE_OBJECT, source_ty );
                        pushValue(mv, query.getArg(2), source_ty);
                        createComparisonCode(mv, null, TYPE_EQ | source_ty, ReturnBehavior.getJumpBehavior(nextIteration));
                    } else
                        mv.visitInsn(POP);

                    //write something?
                    for (int i=0; i<2;i++) {
                        if ( (query.getArg(i+1) & VAR_READ_FLAG) == 0 && query.getArg(i+1) != __ ) {
                            //if (i == 1)
                            //    mv.visitIincInsn(3 + usedLocalVariablesTmp, 1);
                            mv.visitVarInsn(ALOAD, 1 + usedLocalVariablesTmp );
                            mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 3 + usedLocalVariablesTmp);
                            if (i == 0) {
                                mv.visitInsn(ICONST_1);
                                mv.visitInsn(ISUB);
                            }
                            mv.visitInsn(ast.getArrayInstruction(ARRAY_INSTR_LOAD|TYPE_OBJECT));
                            storeValue(mv, query.getArg(i+1), TYPE_OBJECT);
                        }
                    }
                    if ( (query.getArg(1) & VAR_READ_FLAG) == 0 || (query.getArg(2) & VAR_READ_FLAG) == 0)
                        mv.visitJumpInsn(GOTO, finishContains);
                } else {
                    if ( (query.getArg(1) & VAR_READ_FLAG) == 0 && query.getArg(1) != __ ) {
                        mv.visitVarInsn(ALOAD, 1 + usedLocalVariablesTmp );
                        mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 3 + usedLocalVariablesTmp);
                        int arrayType = TYPE_OBJECT;
                        mv.visitInsn(ast.getArrayInstruction(ARRAY_INSTR_LOAD|arrayType));
                        storeValue(mv, query.getArg(1), arrayType);
                    }
                    if ( (query.getArg(1) & VAR_READ_FLAG) == 0)
                        mv.visitJumpInsn(GOTO, finishContains);
                }

                returnBehaviorStack.insertReturnCode(true, mv);
                mv.visitJumpInsn(GOTO, finishContains);
                mv.visitLabel(elementIsNull);
                mv.visitInsn(POP);
                mv.visitLabel(nextIteration);

                mv.visitIincInsn(3 + usedLocalVariablesTmp, 1);
                mv.visitJumpInsn(GOTO, startLoop);

                mv.visitLabel(endArrayCheck);
                //we search for one specified key or value and didn't find it yet AND we have a standard-Map
                if (instruction == TYPE_LOOKUP && (((query.getArg(1) & VAR_READ_FLAG) == 0 && query.getArg(2) != __) || ((query.getArg(2) & VAR_READ_FLAG) == 0 && query.getArg(1) != __))) {
                    //we jump to this label, when iteration-variable is not in range anymore OR we have no default-map
                    Label isNotInstanceDefaultmap = new Label();
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 2 + usedLocalVariablesTmp);
                    mv.visitVarInsn(ast.getSpecialCommand(TYPE_LOAD, TYPE_INT), 3 + usedLocalVariablesTmp);
                    mv.visitJumpInsn(IF_ICMPNE, isNotInstanceDefaultmap);

                    instanceOf(mv, query.getArg(0), "edu/umass/pql/container/PDefaultMap", null, isNotInstanceDefaultmap, false);
                    /*pushValue(mv, query.getArg(0), TYPE_OBJECT);
                    mv.visitTypeInsn(INSTANCEOF, "edu/umass/pql/container/PDefaultMap");
                    mv.visitJumpInsn(IFEQ, isNotInstanceDefaultmap);*/

                    for (int i=0; i<2;i++) {
                        if ( (query.getArg(i+1) & VAR_READ_FLAG) == 0 ) {
                            pushValue(mv, query.getArg(0), TYPE_OBJECT);
                            checkCast(mv, query.getArg(0), ast.getReductorTypeName(REDUCTOR_DEFAULTMAP) );
                            //mv.visitTypeInsn(CHECKCAST, ast.getReductorTypeName(REDUCTOR_DEFAULTMAP) );
                            mv.visitMethodInsn(INVOKEVIRTUAL, ast.getReductorTypeName(REDUCTOR_DEFAULTMAP), "getDefault", "()Ljava/lang/Object;");
                            storeValue(mv, query.getArg(i+1), TYPE_OBJECT);
                        }
                    }
                    mv.visitJumpInsn(GOTO, finishContains);
                    mv.visitLabel(isNotInstanceDefaultmap);
                }

                if ((instruction == TYPE_LOOKUP) &&  ( (query.getArg(1) & VAR_READ_FLAG) == 0 || (query.getArg(2) & VAR_READ_FLAG) == 0)) {
                    returnBehaviorStack.insertReturnCode(false, mv);
                    returnBehaviorStack.push( ReturnBehavior.getJumpBehavior(nextIteration) );
                } else if (instruction == TYPE_CONTAINS && (query.getArg(1) & VAR_READ_FLAG) == 0) {
                    returnBehaviorStack.insertReturnCode(false, mv);
                    returnBehaviorStack.push( ReturnBehavior.getJumpBehavior(nextIteration) );
                } else
                    returnBehaviorStack.insertReturnCode(false, mv);
                mv.visitLabel(finishContains);
                break;
            }

            case TYPE_POLY_LOOKUP:
                insertComment(mv, "START POLY_LOOKUP" );
                pushValue(mv, query.getArg(0), TYPE_OBJECT);

                final String [][] checkInstances = { {"java/util/Map", "java/util/Set", "edu/umass/pql/container/PMap", "edu/umass/pql/container/PDefaultMap"}, {"[I"}, {"[J"}, {"[D"}, {"[C"}, {"[S"}, {"[B"}, {"[Z"}, {"[F"}, {"[Ljava/lang/Object;"} };
                final int [] instanceFlags = {TYPE_LOOKUP, TYPE_ARRAY_LOOKUP|TYPE_INT, TYPE_ARRAY_LOOKUP|TYPE_LONG, TYPE_ARRAY_LOOKUP|TYPE_DOUBLE, TYPE_ARRAY_LOOKUP|TYPE_CHAR, TYPE_ARRAY_LOOKUP|TYPE_SHORT, TYPE_ARRAY_LOOKUP|TYPE_BYTE, TYPE_ARRAY_LOOKUP|TYPE_BOOLEAN, TYPE_ARRAY_LOOKUP|TYPE_FLOAT, TYPE_ARRAY_LOOKUP|TYPE_OBJECT};
                //end of the poly-lookup
                Label endCheck = new Label();
                for (int i=0; i<checkInstances.length; i++) {
                    //we iterate through all possible map/set - types, jump to nextCheck to go to the next type
                    Label nextCheck = new Label();
                    //if we have found the instance of the given object => jump to this label
                    Label validSubCheck = new Label();
                    for (int j=0; j<checkInstances[i].length; j++) {
                        instanceOf(mv, query.getArg(0), checkInstances[i][j], validSubCheck, null, true);
                        /*mv.visitInsn(DUP);
                        mv.visitTypeInsn(INSTANCEOF, checkInstances[i][j]);
                        mv.visitJumpInsn(IFNE, validSubCheck);*/
                        if (j == checkInstances[i].length-1)
                            mv.visitJumpInsn(GOTO, nextCheck);
                    }
                    mv.visitLabel(validSubCheck);
                    mv.visitInsn(POP);
                    createSpecialInstructionCode(mv, query, instanceFlags[i]);
                    mv.visitJumpInsn(GOTO, endCheck);
                    mv.visitLabel(nextCheck);
                }
                mv.visitInsn(POP);
                mv.visitTypeInsn(NEW, "java/lang/Exception");
                mv.visitInsn(DUP);
                mv.visitLdcInsn(new String("the specified object is not a valid object for POLY_LOOKUP."));
                mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Exception", "<init>", "(Ljava/lang/String;)V");
                mv.visitInsn(ATHROW);
                mv.visitLabel(endCheck);
                break;

            case TYPE_COERCION:
                insertComment(mv, "START COERCION" );
                pushValue(mv, query.getArg(0), (type != TYPE_FLOAT ? TYPE_INT : TYPE_DOUBLE));
                castValue(mv, (type != TYPE_FLOAT ? TYPE_INT : TYPE_DOUBLE), type, query.getArg(0));
                storeValue(mv, query.getArg(1), (type != TYPE_FLOAT ? TYPE_INT : TYPE_FLOAT) );
                break;

            case TYPE_FIELD:
                insertComment(mv, "START FIELD" );
                Object preField = ((Field)query).getField();
                if (!(preField instanceof java.lang.reflect.Field))
                    throw new Exception("Field member has to be instance of java.lang.reflect.Field");
                java.lang.reflect.Field field = ((java.lang.reflect.Field)preField);

                String fieldName = field.getType().getName();
                //given object has wrong instance, so we cannot read the field => jump to this label
                Label notInstance = new Label();

                if ( (query.getArg(1) & VAR_READ_FLAG) == 0 ) {
                    pushValue(mv, query.getArg(0), TYPE_OBJECT);
                    instanceOf(mv, query.getArg(0), field.getDeclaringClass().getName().replace(".", "/"), null, notInstance, true);
                    /*mv.visitInsn(DUP);
                    mv.visitTypeInsn(INSTANCEOF, field.getDeclaringClass().getName().replace(".", "/") );
                    mv.visitJumpInsn(IFEQ, notInstance);*/
                    checkCast(mv, query.getArg(0), field.getDeclaringClass().getName().replace(".", "/") );
                    //mv.visitTypeInsn(CHECKCAST, field.getDeclaringClass().getName().replace(".", "/") );
                    mv.visitFieldInsn(GETFIELD, field.getDeclaringClass().getName().replace(".", "/"), field.getName(), ast.fieldNameToTypeName(fieldName) );
                    storeValue(mv, query.getArg(1), ast.fieldNameToType(fieldName));
                } else {
                    pushValue(mv, query.getArg(0), TYPE_OBJECT);
                    instanceOf(mv, query.getArg(0), field.getDeclaringClass().getName().replace(".", "/"), null, notInstance, true);
                    /*mv.visitInsn(DUP);
                    mv.visitTypeInsn(INSTANCEOF, field.getDeclaringClass().getName().replace(".", "/") );
                    mv.visitJumpInsn(IFEQ, notInstance);*/
                    checkCast(mv, query.getArg(0), field.getDeclaringClass().getName().replace(".", "/") );
                    //mv.visitTypeInsn(CHECKCAST, field.getDeclaringClass().getName().replace(".", "/") );
                    mv.visitFieldInsn(GETFIELD, field.getDeclaringClass().getName().replace(".", "/"), field.getName(), ast.fieldNameToTypeName(fieldName) );
                    pushValue(mv, query.getArg(1), ast.fieldNameToType(fieldName));
                    createComparisonCode(mv, null, TYPE_EQ | ast.fieldNameToType(fieldName));
                }

                //we finished reading the field => we jump over the case, the given object has wrong instance, so jump to endField
                Label endField = new Label();
                mv.visitJumpInsn(GOTO, endField);
                mv.visitLabel(notInstance);
                mv.visitInsn(POP);
                returnBehaviorStack.insertReturnCode(false, mv);
                mv.visitLabel(endField);
                break;

            default:
                throw new Exception("unknown instruction (code: " + instruction + ")");
        }

    }

    private static void createArithmeticCode (MethodVisitor mv, Join query, int flags) throws Exception {

        int type = flags & 0xFF;
        int instruction = (flags & 0xFF00);
        int totalArgs = ((instruction != TYPE_BITINV && instruction != TYPE_NEG) ? 3 : 2);

        //push one or two values to calculate (argument-amount depends on the operator)
        pushValue(mv, query.getArg(0), type);
        if (instruction != TYPE_BITINV && instruction != TYPE_NEG) {
            if ((instruction == TYPE_BITSHL || instruction == TYPE_BITSHR || instruction == TYPE_BITSSHR) && type == TYPE_LONG)
                pushValue(mv, query.getArg(1), TYPE_INT);
            else
                pushValue(mv, query.getArg(1), type);
        }
        if (instruction == TYPE_BITINV) {
            if (type == TYPE_INT) mv.visitInsn(ICONST_M1);
            else if (type == TYPE_LONG) mv.visitLdcInsn(new Long(-1));
            else throw new Exception("this combination of instruction and type is not implemented (yet).");
        }

        //divisor = 0 ?
        if (instruction == TYPE_DIV || instruction == TYPE_MOD) {
            //divisor is not == 0 => jump to divisorNotZero
            Label divisorNotZero = new Label();
            if (type == TYPE_DOUBLE) {
                mv.visitInsn(DUP2);
                mv.visitInsn(DCONST_0);
                mv.visitInsn(DCMPG);
            } else if (type == TYPE_LONG) {
                mv.visitInsn(DUP2);
                mv.visitInsn(LCONST_0);
                mv.visitInsn(LCMP);
            } else if (type == TYPE_INT)
                mv.visitInsn(DUP);
            mv.visitJumpInsn(IFNE, divisorNotZero);
            for (int i=0; i<2; i++) {
                if (type == TYPE_INT)
                    mv.visitInsn(POP);
                else
                    mv.visitInsn(POP2);
            }
            //pop values, before return
            if ( (query.getArg(totalArgs-1) & VAR_READ_FLAG) == 0 ) {
                mv.visitInsn(POP);
                mv.visitInsn(POP);
            }
            returnBehaviorStack.insertReturnCode(false, mv);
            mv.visitLabel(divisorNotZero);
        }

        //calculate solution
        mv.visitInsn( ast.getArithmeticInstruction( instruction, type) );

        //write-flag => write and return true / otherwise check and return true when solution is correct
        if ( (query.getArg(totalArgs-1) & VAR_READ_FLAG) == 0 )
            storeValue(mv, query.getArg(totalArgs-1), type);
        else {
            pushValue(mv, query.getArg(totalArgs-1), type);
            //we have two read-variables, so we just check if they are equal => they are equal, then jump to checkSucceeds
            Label checkSucceeds = new Label();

            if (type == TYPE_DOUBLE)
                mv.visitInsn(DCMPG);
            else if (type == TYPE_LONG)
                mv.visitInsn(LCMP);
            else if (type == TYPE_INT)
                mv.visitInsn( ast.getArithmeticInstruction( TYPE_SUB, type) );
            mv.visitJumpInsn(IFEQ, checkSucceeds);
            returnBehaviorStack.insertReturnCode(false, mv);
            mv.visitLabel(checkSucceeds);
        }

        returnBehaviorStack.insertReturnCode(true, mv);
    }

    private static void createComparisonCode (MethodVisitor mv, Join query, int flags) throws Exception {
        createComparisonCode (mv, query, flags, null);
    }

    private static void createComparisonCode (MethodVisitor mv, Join query, int flags, ReturnBehavior individualReturnBehavior) throws Exception {

        int type = flags & 0xFF;
        int instruction = (flags & 0xFF00);
        //the end of the comparison-code
        Label endComparisonLabel = new Label();

        if (query != null && (query.getArg(1) & VAR_READ_FLAG) == 0 && instruction == TYPE_EQ) {
            pushValue(mv, query.getArg(0), type);
            storeValue (mv, query.getArg(1), type);
        } else {
            if (query != null) {
                pushValue(mv, query.getArg(0), type);
                pushValue(mv, query.getArg(1), type);
            }
            //jump here, if comparison succeeds
            Label endLabel = new Label();
            if (type == TYPE_FLOAT) {
                mv.visitInsn(F2D);
                mv.visitInsn(DUP2_X1);
                mv.visitInsn(POP2);
                mv.visitInsn(F2D);
                mv.visitInsn(DUP2_X2);
                mv.visitInsn(POP2);
                type = TYPE_DOUBLE;
            }
            if (type == TYPE_LONG || type == TYPE_DOUBLE) {
                mv.visitInsn( (type == TYPE_LONG ? LCMP : DCMPG) );
                switch (instruction) {
                    case TYPE_EQ: mv.visitJumpInsn(IFEQ, endLabel); break;
                    case TYPE_NEQ: mv.visitJumpInsn(IFNE, endLabel); break;
                    case TYPE_LT: mv.visitJumpInsn(IFLT, endLabel); break;
                    case TYPE_LTE: mv.visitJumpInsn(IFLE, endLabel); break;
                    default: throw new Exception("this combination of instruction and type is not implemented (yet).");
                }
            }
            else if (type == TYPE_INT || type == TYPE_CHAR || type == TYPE_BOOLEAN || type == TYPE_BYTE || type == TYPE_SHORT) {
                switch (instruction) {
                    case TYPE_EQ: mv.visitJumpInsn(IF_ICMPEQ, endLabel); break;
                    case TYPE_NEQ: mv.visitJumpInsn(IF_ICMPNE, endLabel); break;
                    case TYPE_LT: mv.visitJumpInsn(IF_ICMPLT, endLabel); break;
                    case TYPE_LTE: mv.visitJumpInsn(IF_ICMPLE, endLabel); break;
                    default: throw new Exception("this combination of instruction and type is not implemented (yet).");
                }
            }
            else if (type == TYPE_OBJECT) {
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "equals", "(Ljava/lang/Object;)Z");
                switch (instruction) {
                    case TYPE_EQ: mv.visitJumpInsn(IFNE, endLabel); break;
                    case TYPE_NEQ: mv.visitJumpInsn(IFEQ, endLabel); break;
                    default: throw new Exception("this combination of instruction and type is not implemented (yet).");
                }
            }
            else if (type == TYPE_STRING) {
                switch (instruction) {
                    case TYPE_EQ: mv.visitJumpInsn(IF_ACMPEQ, endLabel); break;
                    case TYPE_NEQ: mv.visitJumpInsn( (type == TYPE_OBJECT ? IF_ACMPNE : IF_ACMPEQ) , endLabel); break;
                    default: throw new Exception("this combination of instruction and type is not implemented (yet).");
                }
                if (type == TYPE_STRING) {
                    pushValue(mv, query.getArg(0), type);
                    //we have a NULL-string => jump to falseLabel
                    Label falseLabel = new Label();
                    mv.visitJumpInsn( IFNULL, falseLabel);
                    pushValue(mv, query.getArg(0), type);
                    pushValue(mv, query.getArg(1), type);
                    mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "equals", "(Ljava/lang/Object;)Z");
                    mv.visitJumpInsn(IFNE, endLabel);
                    mv.visitLabel(falseLabel);
                }
            } else
                throw new Exception("this combination of instruction and type is not implemented (yet).");

            if (individualReturnBehavior == null)
                returnBehaviorStack.insertReturnCode( (type == TYPE_STRING && instruction == TYPE_NEQ), mv);
            else
                individualReturnBehavior.insertReturnCode( (type == TYPE_STRING && instruction == TYPE_NEQ), mv, null);
            mv.visitJumpInsn(GOTO, endComparisonLabel);
            mv.visitLabel(endLabel);
        }
        if (individualReturnBehavior == null)
            returnBehaviorStack.insertReturnCode(!(type == TYPE_STRING && instruction == TYPE_NEQ), mv);
        else
            individualReturnBehavior.insertReturnCode( !(type == TYPE_STRING && instruction == TYPE_NEQ), mv, null);
        mv.visitLabel(endComparisonLabel);
    }

    protected static void pushValue (MethodVisitor mv, int val, int type) throws Exception {

        if (type == TYPE_STRING)
            type = TYPE_OBJECT;

        final int index = val >> VAR_INDEX_SHIFT;
        final int source_ty = (val >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;

        if (source_ty == __) {
            mv.visitInsn(ICONST_0);
            castValue(mv, TYPE_INT, type, val);
        } else {
            usedLocalVariables = envReplace.pushValue(mv, index, source_ty, ((val & VAR_CONST_FLAG) != 0), usedLocalVariables);
            castValue(mv, source_ty, type, val);
        }
    }

    protected static void storeValue (MethodVisitor mv, int val, int type) throws Exception {
        final int source_ty = (val >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
        storeValue(mv, val, type, source_ty);
    }

    protected static void storeValue (MethodVisitor mv, int val, int type, int sourceType) throws Exception {

        final int index = val >> VAR_INDEX_SHIFT;
        final int source_ty = (val >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
        castValue(mv, type, sourceType, val);
        if (sourceType != source_ty)
            castValue(mv, sourceType, source_ty, val);
        usedLocalVariables = envReplace.storeValue(mv, index, source_ty, ((val & VAR_CONST_FLAG) != 0), usedLocalVariables);
    }

    protected static void insertSpecialTypeCast (MethodVisitor mv, int from, int to, Object val) throws Exception {
        insertComment(mv, "InsertSpecialTypeCast");
        if (from != TYPE_STRING && from != TYPE_FLOAT && from != -1)
            return;
        if (from == TYPE_STRING)
            mv.visitTypeInsn(CHECKCAST, "java/lang/Object");
        else if (to == TYPE_STRING)
            mv.visitTypeInsn(CHECKCAST, "java/lang/String");
        else if (to == TYPE_BOOLEAN) {
            //numerical value is 0, jump to noChange (otherwise set to 1)
            Label noChange = new Label();
            mv.visitInsn(DUP);
            mv.visitJumpInsn(IFEQ, noChange);
            mv.visitInsn(POP);
            mv.visitInsn(ICONST_1);
            mv.visitLabel(noChange);
        }
        else {
            final int index = (val == null ? 0 : (((Integer)val) >> VAR_INDEX_SHIFT) );
            //we have a constant variable and convert it => no I2B or I2S (if integer in right range)
            if (! ((from == -1 && to == TYPE_BYTE && val != null && (( ((Integer)val) & VAR_CONST_FLAG) != 0) && initEnv.v_int[index] >= 0 && initEnv.v_int[index] <= 127 ) ||
                   (from == -1 && to == TYPE_SHORT && val != null && (( ((Integer)val) & VAR_CONST_FLAG) != 0) && initEnv.v_int[index] >= 0 && initEnv.v_int[index] <= 32767 ) ))
                mv.visitInsn(ast.insertSpecialTypeCast(from, to));
        }
    }

    protected static void castValue (MethodVisitor mv, int from, int to) throws Exception {
        castValue(mv, from, to, null);
    }

    protected static void castValue (MethodVisitor mv, int from, int to, int val) throws Exception {
        castValue(mv, from, to, new Integer(val));
    }

    protected static void castValue (MethodVisitor mv, int from, int to, Object val) throws Exception {
        if (from != to) {
            if (from >= 5 || to >= 5) {
                int tmpType [] = {TYPE_OBJECT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_INT, TYPE_DOUBLE};
                if (from >= 5) {
                    insertSpecialTypeCast(mv, from, -1, val);
                    castValue(mv, tmpType[from-5], to, null);
                } else if (to >= 5) {
                    castValue(mv, from, tmpType[to-5], val);
                    insertSpecialTypeCast(mv, -1, to, (from != TYPE_INT || tmpType[to-5] != TYPE_INT ? null : val) );
                }
            }
            else if ( to == TYPE_OBJECT ) {
                switch (from) {
                    case TYPE_INT:
                        mv.visitMethodInsn(INVOKESTATIC, "edu/umass/pql/Env", "canonicalInteger", "(I)Ljava/lang/Integer;");
                        break;
                    case TYPE_LONG:
                        mv.visitMethodInsn(INVOKESTATIC, "edu/umass/pql/Env", "canonicalLong", "(J)Ljava/lang/Long;");
                        break;
                    case TYPE_DOUBLE:
                        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Double", "valueOf", "(D)Ljava/lang/Double;");
                        break;
                    default:
                        throw new Exception("the specified type ('" + from + "') is not supported (yet).");
                }
            } else if ( from == TYPE_OBJECT ) {
                String [] checkInstances = {"java/lang/Number", "java/lang/Boolean", "java/lang/Character"};
                //jump to endCheck, if we could find the instance of the object and converted it successful
                Label endCheck = new Label();
                for (int i=0; i<checkInstances.length; i++) {
                    mv.visitInsn(DUP);
                    mv.visitTypeInsn(INSTANCEOF, checkInstances[i]);
                    //we check all possible object-instances, if the given object is not the current one, jump to notInstance
                    Label notInstance = new Label();
                    mv.visitJumpInsn(IFEQ, notInstance);

                    mv.visitTypeInsn(CHECKCAST, checkInstances[i]);
                    if (i == 0)
                        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Number", ast.getFullTypeSpecifier(to) + "Value", "()" + ast.getTypeSpecifier(to));
                    else if (i == 1) {
                        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z");
                        if (to != TYPE_INT)
                            mv.visitInsn(ast.getTypeCastFlag("I2" + ast.getTypeSpecifier(to)));
                    } else if (i == 2) {
                        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Character", "charValue", "()C");
                        if (to != TYPE_INT)
                            mv.visitInsn(ast.getTypeCastFlag("I2" + ast.getTypeSpecifier(to)));
                    }

                    mv.visitJumpInsn(GOTO, endCheck);
                    mv.visitLabel(notInstance);
                }
                insertDebugInformation(mv, "the specified object cannot be converted in a numerical value.");
                mv.visitTypeInsn(NEW, "java/lang/Exception");
                mv.visitInsn(DUP);
                mv.visitLdcInsn(new String("the specified object cannot be converted in a numerical value."));
                mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Exception", "<init>", "(Ljava/lang/String;)V");
                mv.visitInsn(ATHROW);
                mv.visitLabel(endCheck);
            }
            else
                mv.visitInsn( ast.getTypeCastFlag(ast.getTypeSpecifier(from) + "2" + ast.getTypeSpecifier(to)) );
        }
    }

    protected static boolean checkCast (MethodVisitor mv, int val, String castType) {
        final int index = val >> VAR_INDEX_SHIFT;
        final int source_ty = (val >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
        boolean checkCastNeeded = (!(source_ty == TYPE_OBJECT && typeInformation.containsKey(index)));
        //checkCastNeeded = true;
        if (checkCastNeeded)
            mv.visitTypeInsn(CHECKCAST, castType );
        return checkCastNeeded;
    }

    protected static void instanceOf (MethodVisitor mv, int val, String type, Label isInstance, Label isNotInstance, boolean duplicateFirst) throws Exception {
        final int index = val >> VAR_INDEX_SHIFT;
        final int source_ty = (val >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
        if (source_ty == TYPE_OBJECT && typeInformation.containsKey(index)) {
            //boolean sameInstance = (typeInformation.get(index).equals(type));
            boolean sameInstance = Class.forName(typeInformation.get(index).replace("/", ".")).isAssignableFrom(Class.forName(type.replace("/", ".")));
            if (isInstance != null && sameInstance)
                mv.visitJumpInsn(GOTO, isInstance);
            else if (isNotInstance != null && !sameInstance)
                mv.visitJumpInsn(GOTO, isNotInstance);
        } else {
            if (duplicateFirst)
                mv.visitInsn(DUP);
            else
                pushValue(mv, val, TYPE_OBJECT);
            mv.visitTypeInsn(INSTANCEOF, type);
            if (isNotInstance == null)
                mv.visitJumpInsn(IFNE, isInstance);
            else
                mv.visitJumpInsn(IFEQ, isNotInstance);
        }
    }

    protected static void loadConstantInteger (MethodVisitor mv, int value) {
        final int [] commands = {ICONST_M1, ICONST_0, ICONST_1, ICONST_2, ICONST_3, ICONST_4, ICONST_5};
        if (value >= -1 && value <= 5)
            mv.visitInsn(commands[value+1]);
        else
            mv.visitLdcInsn(new Integer(value));
    }

    protected static void loadConstantLong (MethodVisitor mv, long value) {
        final int [] commands = {LCONST_0, LCONST_1};
        if (value >= 0 && value <= 1)
            mv.visitInsn(commands[(int)value]);
        else
            mv.visitLdcInsn(new Long(value));
    }

    protected static void loadConstantDouble (MethodVisitor mv, double value) {
        final int [] commands = {DCONST_0, DCONST_1};
        if (value == 0.0D)
            mv.visitInsn(DCONST_0);
        else if (value == 1.0D)
            mv.visitInsn(DCONST_1);
        else
            mv.visitLdcInsn(new Double(value));
    }

    protected static void insertDebugInformation(MethodVisitor mv, String str) {
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn(str);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V");
    }

    protected static void insertDebugInformation(MethodVisitor mv, int type) throws Exception {
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "valueOf", "(" + ast.getTypeSpecifier(type) + ")Ljava/lang/String;");
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitInsn(SWAP);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V");
    }

    protected static void insertComment (MethodVisitor mv, String comment) throws Exception {
        if (writeCommentsInCode) {
            mv.visitLdcInsn(comment);
            mv.visitInsn(POP);
        }
    }

}
