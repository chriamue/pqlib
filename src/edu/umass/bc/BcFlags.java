/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

/**
 *
 * @author Hilmar
 */
public interface BcFlags {

    //aditional types
    public static final int TYPE_STRING = 5;
    public static final int TYPE_CHAR = 6;
    public static final int TYPE_SHORT = 7;
    public static final int TYPE_BYTE = 8;
    public static final int TYPE_BOOLEAN = 9;
    public static final int TYPE_FLOAT = 10;
    public static final int TYPE_UNKNOWN = 11;

    //arithmetic/comparison instructions
    public static final int TYPE_ADD	= (0 << 8);
    public static final int TYPE_SUB	= (1 << 8);
    public static final int TYPE_MUL	= (2 << 8);
    public static final int TYPE_DIV	= (3 << 8);
    public static final int TYPE_MOD	= (4 << 8);
    public static final int TYPE_NEG	= (5 << 8);
    public static final int TYPE_BITINV	= (6 << 8);
    public static final int TYPE_BITOR	= (7 << 8);
    public static final int TYPE_BITAND	= (8 << 8);
    public static final int TYPE_BITXOR	= (9 << 8);
    public static final int TYPE_BITSHL	= (10 << 8);
    public static final int TYPE_BITSHR = (11 << 8);
    public static final int TYPE_BITSSHR = (12 << 8);
    public static final int TYPE_EQ = (13 << 8);
    public static final int TYPE_NEQ = (14 << 8);
    public static final int TYPE_LT = (15 << 8);
    public static final int TYPE_LTE = (16 << 8);

    //other instructions
    public static final int TYPE_RANGE_CONTAINS = (32 << 8);
    public static final int TYPE_CONJUCTIVE = (33 << 8);
    public static final int TYPE_STORE = (34 << 8);
    public static final int TYPE_LOAD = (35 << 8);
    public static final int TYPE_REDUCTION = (36 << 8);
    public static final int TYPE_BOOL = (37 << 8);
    public static final int TYPE_CONTAINS = (38 << 8);
    public static final int TYPE_LOOKUP = (39 << 8);
    public static final int TYPE_ARRAY_LOOKUP = (40 << 8);
    public static final int TYPE_POLY_LOOKUP = (41 << 8);
    public static final int TYPE_COERCION = (42 << 8);
    public static final int TYPE_FIELD = (43 << 8);
    public static final int TYPE_POLY_SIZE = (44 << 8);
    public static final int TYPE_MAP_SIZE = (45 << 8);
    public static final int TYPE_SET_SIZE = (46 << 8);
    public static final int TYPE_ARRAY_SIZE = (47 << 8);
    public static final int TYPE_TRUE = (48 << 8);
    public static final int TYPE_FALSE = (49 << 8);
    /*public static final int TYPE_EXTENSION_SQRT = (50 << 8);
    public static final int TYPE_EXTENSION_PRIME_CHECK = (51 << 8);
    public static final int TYPE_EXTENSION_PRIME_RANGE = (52 << 8);
    public static final int TYPE_EXTENSION_STREAM_CONTAINS = (53 << 8);*/
    //public static final int TYPE_EXTENSION_N_DEFAULT_MAP_METHOD_ADAPTER = (54 << 8);
    public static final int TYPE_DISJUNCTIVE = (55 << 8);
    public static final int TYPE_CHECK_TYPE = (56 << 8);
    public static final int TYPE_NOT = (57 << 8);
    public static final int TYPE_CHECK_JAVA_TYPE = (58 << 8);

    //reductors
    public static final int REDUCTOR_SET = (64 << 8);
    public static final int REDUCTOR_ARRAY = (65 << 8);
    public static final int REDUCTOR_MAP = (66 << 8);
    public static final int REDUCTOR_DEFAULTMAP = (67 << 8);
    public static final int REDUCTOR_EXISTS = (68 << 8);
    public static final int REDUCTOR_METHOD_ADAPTER = (69 << 8);
    public static final int REDUCTOR_N_MAP = (70 << 8);
    public static final int REDUCTOR_N_DEFAULTMAP = (71 << 8);

    //additional options
    public static final int WITH_INNER_REDUCTOR = (1 << 16);

    //return behaviors
    public static final int RETURN_RETURN = 1;
    public static final int RETURN_JUMP = 2;
    public static final int RETURN_DONOTHING = 3;
    public static final int RETURN_REDUCTION = 4;

    //array-type
    public static final int ARRAY_INSTR_LOAD = (0 << 8);
    public static final int ARRAY_INSTR_STORE = (1 << 8);

}
