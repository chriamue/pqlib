/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import edu.umass.pql.Env;
import edu.umass.pql.VarConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Hilmar
 */
public class EnvReplace implements Opcodes, VarConstants, BcFlags {
    
    private Label initLabel;
    private Label startLabel;
    private Label endLabel;
    private Map <Integer, List <String> > possibleCasts;
    private ArrayList <Integer> cachedValues;
    //private int localVariableIndex;
    private Env initEnv;
    
    public EnvReplace (Env _initEnv) 
    {
        possibleCasts = new HashMap <Integer, List <String> > ();
        initEnv = _initEnv;
        cachedValues = new ArrayList <Integer> ();
        //localVariableIndex = 1000;        
    }
    
    public void addPossibleCast (MethodVisitor mv, int val, String castType) {
        
        //mv.visitTypeInsn(CHECKCAST, castType);
        
        final int index = val >> VAR_INDEX_SHIFT;
        final int type = (val >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
        final int varIndex = index | type;        
        
        if (!possibleCasts.containsKey(varIndex))
            possibleCasts.put(varIndex, new ArrayList <String> () );
        
        if (!possibleCasts.get(varIndex).contains(castType))
            possibleCasts.get(varIndex).add(castType);
        
    }
    
    public void preCode (MethodVisitor mv)
    {
        initLabel = new Label();
        startLabel = new Label();
        endLabel = new Label();
        mv.visitJumpInsn(GOTO, initLabel);        
        mv.visitLabel(startLabel);
    }
    
    public void postCode (MethodVisitor mv) throws Exception
    {
        mv.visitJumpInsn(GOTO, endLabel);
        mv.visitLabel(initLabel);
        for (int i=0; i<cachedValues.size()/3; i++) {
            int usedLocalVariables = cachedValues.get(i*3);
            int index = cachedValues.get(i*3 + 1);
            int type = cachedValues.get(i*3 + 2);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, "edu/umass/pql/Env", ASMFlagTranslations.getTypeArrayName(type), "[" + ASMFlagTranslations.getTypeSpecifier(type) );
            RuntimeCreator.loadConstantInteger(mv, index);            
            mv.visitInsn( ASMFlagTranslations.getArrayInstruction(type|ARRAY_INSTR_LOAD));
            if (RuntimeCreator.typeInformation.containsKey(index) && type == TYPE_OBJECT) {
                String castType = RuntimeCreator.typeInformation.get(index);
                mv.visitTypeInsn(CHECKCAST, castType );
            }
            /*if (possibleCasts.containsKey( (index|type) )) {
                Label checkCastFinished = new Label();
                for (String possibleCast : possibleCasts.get(index|type)) {
                    if (possibleCast.startsWith("[B"))
                        continue;
                    possibleCast = "[[B";
                    //mv.visitInsn(DUP);
                    //mv.visitTypeInsn(INSTANCEOF, possibleCast );
                    Label isNotInstance = new Label();
                    //mv.visitJumpInsn(IFEQ, isNotInstance);
                    mv.visitTypeInsn(CHECKCAST, possibleCast );
                    mv.visitJumpInsn(GOTO, checkCastFinished);
                    mv.visitLabel(isNotInstance);
                }
                mv.visitLabel(checkCastFinished);
            }*/
            mv.visitVarInsn(ASMFlagTranslations.getSpecialCommand(TYPE_STORE, type), usedLocalVariables);
        }
        mv.visitJumpInsn(GOTO, startLabel);
        mv.visitLabel(endLabel);
        for (int i=0; i<cachedValues.size()/3; i++) {
            int usedLocalVariables = cachedValues.get(i*3);
            int index = cachedValues.get(i*3 + 1);
            int type = cachedValues.get(i*3 + 2);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitFieldInsn(GETFIELD, "edu/umass/pql/Env", ASMFlagTranslations.getTypeArrayName(type), "[" + ASMFlagTranslations.getTypeSpecifier(type) );
            RuntimeCreator.loadConstantInteger(mv, index);
            mv.visitVarInsn(ASMFlagTranslations.getSpecialCommand(TYPE_LOAD, type), usedLocalVariables);
            mv.visitInsn( ASMFlagTranslations.getArrayInstruction(type|ARRAY_INSTR_STORE));            
        }
    }
    
    public int pushValue(MethodVisitor mv, int index, int type, boolean isConst, int usedLocalVariables) throws Exception {
        if (isConst && type != TYPE_OBJECT) {
            if (type == TYPE_INT)
                RuntimeCreator.loadConstantInteger(mv, initEnv.v_int[index] );
            else if (type == TYPE_DOUBLE)
                RuntimeCreator.loadConstantDouble(mv, initEnv.v_double[index] );
            else if (type == TYPE_LONG)
                RuntimeCreator.loadConstantLong(mv, initEnv.v_long[index] );
            return usedLocalVariables;
            //return;
        }
        for (int i=0; i<cachedValues.size()/3; i++) {
            if (cachedValues.get(i*3 + 1) == index && cachedValues.get(i*3 + 2) == type) {
                mv.visitVarInsn(ASMFlagTranslations.getSpecialCommand(TYPE_LOAD, type), cachedValues.get(i*3) );
                return usedLocalVariables;
                //return;
            }
        }
        mv.visitVarInsn(ASMFlagTranslations.getSpecialCommand(TYPE_LOAD, type), usedLocalVariables + 1);
        cachedValues.add(usedLocalVariables + 1);
        //cachedValues.add(localVariableIndex);
        cachedValues.add(index);
        cachedValues.add(type);
        //localVariableIndex += (type == TYPE_DOUBLE || type == TYPE_LONG ? 2 : 1);
        return usedLocalVariables + (type == TYPE_DOUBLE || type == TYPE_LONG ? 2 : 1);
        //return usedLocalVariables;
    }
    
    public int storeValue(MethodVisitor mv, int index, int type, boolean isConst, int usedLocalVariables) throws Exception {
        if (isConst)
            throw new Exception("You can't write a const-variable.");
        for (int i=0; i<cachedValues.size()/3; i++) {
            if (cachedValues.get(i*3 + 1) == index && cachedValues.get(i*3 + 2) == type) {
                mv.visitVarInsn(ASMFlagTranslations.getSpecialCommand(TYPE_STORE, type), cachedValues.get(i*3) );
                return usedLocalVariables;
                //return;
            }
        }
        mv.visitVarInsn(ASMFlagTranslations.getSpecialCommand(TYPE_STORE, type), usedLocalVariables + 1);
        cachedValues.add(usedLocalVariables + 1);
        //cachedValues.add(localVariableIndex);
        cachedValues.add(index);
        cachedValues.add(type);
        //localVariableIndex += (type == TYPE_DOUBLE || type == TYPE_LONG ? 2 : 1);
        return usedLocalVariables + (type == TYPE_DOUBLE || type == TYPE_LONG ? 2 : 1);
        //return usedLocalVariables;
    }
    
    
    
}