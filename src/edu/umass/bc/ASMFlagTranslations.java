/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import edu.umass.pql.VarConstants;
import java.util.Map;
import java.util.Set;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Hilmar
 */
public class ASMFlagTranslations implements BcFlags, VarConstants, Opcodes {
    
    public static int getArithmeticInstruction (int instruction, int type) throws Exception {
        switch (instruction) {
            case TYPE_ADD: {
                switch (type) {
                    case TYPE_INT: return IADD;
                    case TYPE_LONG: return LADD;
                    case TYPE_DOUBLE: return DADD;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_SUB: {
                switch (type) {
                    case TYPE_INT: return ISUB;
                    case TYPE_LONG: return LSUB;
                    case TYPE_DOUBLE: return DSUB;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_MUL: {
                switch (type) {
                    case TYPE_INT: return IMUL;
                    case TYPE_LONG: return LMUL;
                    case TYPE_DOUBLE: return DMUL;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_DIV: {
                switch (type) {
                    case TYPE_INT: return IDIV;
                    case TYPE_LONG: return LDIV;
                    case TYPE_DOUBLE: return DDIV;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_MOD: {
                switch (type) {
                    case TYPE_INT: return IREM;
                    case TYPE_LONG: return LREM;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_NEG: {
                switch (type) {
                    case TYPE_INT: return INEG;
                    case TYPE_LONG: return LNEG;
                    case TYPE_DOUBLE: return DNEG;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITINV: {
                switch (type) {
                    case TYPE_INT: return IXOR;
                    case TYPE_LONG: return LXOR;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITOR: {
                switch (type) {
                    case TYPE_INT: return IOR;
                    case TYPE_LONG: return LOR;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITAND: {
                switch (type) {
                    case TYPE_INT: return IAND;
                    case TYPE_LONG: return LAND;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITXOR: {
                switch (type) {
                    case TYPE_INT: return IXOR;
                    case TYPE_LONG: return LXOR;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITSHL: {
                switch (type) {
                    case TYPE_INT: return ISHL;
                    case TYPE_LONG: return LSHL;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITSHR: {
                switch (type) {
                    case TYPE_INT: return ISHR;
                    case TYPE_LONG: return LSHR;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_BITSSHR: {
                switch (type) {
                    case TYPE_INT: return IUSHR;
                    case TYPE_LONG: return LUSHR;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            default:
                throw new Exception ("this combination of instruction and type is not implemented (yet).");
        }
    }
    
    public static int getStackCommand (int instruction, int type) throws Exception {
        switch (instruction) {
            case POP: {
                switch (type) {
                    case TYPE_INT: return POP;
                    case TYPE_LONG: return POP2;
                    case TYPE_DOUBLE: return POP2;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case DUP: {
                switch (type) {
                    case TYPE_INT: return DUP;
                    case TYPE_LONG: return DUP2;
                    case TYPE_DOUBLE: return DUP2;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case DUP_X1: {
                switch (type) {
                    case TYPE_INT: return DUP_X1;
                    case TYPE_LONG: return DUP2_X2;
                    case TYPE_DOUBLE: return DUP2_X2;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }      
            default:
                throw new Exception ("this combination of instruction and type is not implemented (yet).");
        }
    }
    
    public static int getSpecialCommand (int instruction, int type) throws Exception {
        switch (instruction) {
            case TYPE_STORE: {
                switch (type) {
                    case TYPE_INT: return ISTORE;
                    case TYPE_LONG: return LSTORE;
                    case TYPE_DOUBLE: return DSTORE;
                    case TYPE_OBJECT: return ASTORE;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            case TYPE_LOAD: {
                switch (type) {
                    case TYPE_INT: return ILOAD;
                    case TYPE_LONG: return LLOAD;
                    case TYPE_DOUBLE: return DLOAD;
                    case TYPE_OBJECT: return ALOAD;
                    default: throw new Exception ("this combination of instruction and type is not implemented (yet).");
                }
            }
            default:
                throw new Exception ("this combination of instruction and type is not implemented (yet).");
        }
    }
    
    public static String getTypeArrayName (int type) throws Exception {
        switch (type) {
            case TYPE_INT:
                return "v_int";
            case TYPE_LONG:
                return "v_long";
            case TYPE_DOUBLE:
                return "v_double";
            case TYPE_OBJECT:
                return "v_object";
            default:
                throw new Exception("the specified type is not supported (yet).");
        }
    }
    
    public static String getTypeSpecifier (int type) throws Exception {
        switch (type) {
            case TYPE_INT:
                return "I";
            case TYPE_LONG:
                return "J";
            case TYPE_DOUBLE:
                return "D";
            case TYPE_OBJECT:
                return "Ljava/lang/Object;";
            case TYPE_STRING:
                return "Ljava/lang/String;";
            case TYPE_CHAR:
                return "C";
            case TYPE_SHORT:
                return "S";
            case TYPE_BYTE:
                return "B";
            case TYPE_BOOLEAN:
                return "Z";
            case TYPE_FLOAT:
                return "F";
            default:
                throw new Exception("the specified type is not supported (yet).");
        }
    }
    
    public static String getFullTypeSpecifier (int type) throws Exception {
        switch (type) {
            case TYPE_INT:
                return "int";
            case TYPE_LONG:
                return "long";
            case TYPE_DOUBLE:
                return "double";
            case TYPE_OBJECT:
                return "java.lang.Object";
            case TYPE_STRING:
                return "java.lang.String";
            case TYPE_CHAR:
                return "char";
            case TYPE_SHORT:
                return "short";
            case TYPE_BYTE:
                return "byte";
            case TYPE_BOOLEAN:
                return "boolean";
            case TYPE_FLOAT:
                return "float";
            default:
                throw new Exception("the specified type is not supported (yet).");
        }
    }
    
    public static String fieldNameToTypeName (String fieldName) throws Exception {
        if (fieldName.startsWith("["))
            return fieldName;
        else if (fieldName.equals("int"))
            return "I";
        else if (fieldName.equals("long"))
            return "J";
        else if (fieldName.equals("double"))
            return "D";
        else if (fieldName.equals("char"))
            return "C";
        else if (fieldName.equals("short"))
            return "S";
        else if (fieldName.equals("byte"))
            return "B";
        else if (fieldName.equals("boolean"))
            return "Z";
        else if (fieldName.equals("float"))
            return "F";
        else
            return "L" + fieldName.replace(".", "/") + ";";
    }
    
    public static int fieldNameToType (String fieldName) throws Exception {
        if (fieldName.equals("int"))
            return TYPE_INT;
        else if (fieldName.equals("long"))
            return TYPE_LONG;
        else if (fieldName.equals("double"))
            return TYPE_DOUBLE;
        else if (fieldName.equals("char"))
            return TYPE_CHAR;
        else if (fieldName.equals("short"))
            return TYPE_SHORT;
        else if (fieldName.equals("byte"))
            return TYPE_BYTE;
        else if (fieldName.equals("boolean"))
            return TYPE_BOOLEAN;
        else if (fieldName.equals("float"))
            return TYPE_FLOAT;
        else
            return TYPE_OBJECT;
    }
    
    public static int getArrayInstruction (int flags) throws Exception {
        switch (flags) {
            case (TYPE_INT|ARRAY_INSTR_LOAD):
                return IALOAD;
            case (TYPE_LONG|ARRAY_INSTR_LOAD):
                return LALOAD;
            case (TYPE_DOUBLE|ARRAY_INSTR_LOAD):
                return DALOAD;
            case (TYPE_CHAR|ARRAY_INSTR_LOAD):
                return CALOAD;
            case (TYPE_SHORT|ARRAY_INSTR_LOAD):
                return SALOAD;
            case (TYPE_BOOLEAN|ARRAY_INSTR_LOAD):
                return BALOAD;
            case (TYPE_BYTE|ARRAY_INSTR_LOAD):
                return BALOAD;
            case (TYPE_FLOAT|ARRAY_INSTR_LOAD):
                return FALOAD;
            case (TYPE_OBJECT|ARRAY_INSTR_LOAD):
                return AALOAD;
            case (TYPE_INT|ARRAY_INSTR_STORE):
                return IASTORE;
            case (TYPE_LONG|ARRAY_INSTR_STORE):
                return LASTORE;
            case (TYPE_DOUBLE|ARRAY_INSTR_STORE):
                return DASTORE;
            case (TYPE_OBJECT|ARRAY_INSTR_STORE):
                return AASTORE;
            case (TYPE_CHAR|ARRAY_INSTR_STORE):
                return CASTORE;
            case (TYPE_SHORT|ARRAY_INSTR_STORE):
                return SASTORE;
            case (TYPE_BOOLEAN|ARRAY_INSTR_STORE):
                return BASTORE;
            case (TYPE_BYTE|ARRAY_INSTR_STORE):
                return BASTORE;
            case (TYPE_FLOAT|ARRAY_INSTR_STORE):
                return FASTORE;
            default:
                throw new Exception("the specified combination of array-instruction and type is not supported (yet).");
        }
    }
    
    public static int getTypeCastFlag (String typeCast) throws Exception {
        if (typeCast.equals("I2D"))
            return I2D;
        else if (typeCast.equals("I2J"))
            return I2L;
        else if (typeCast.equals("D2I"))
            return D2I;
        else if (typeCast.equals("D2J"))
            return D2L;
        else if (typeCast.equals("J2I"))
            return L2I;
        else if (typeCast.equals("J2D"))
            return L2D;
        else
            throw new Exception("the specified type-cast ('" + typeCast + "') is not supported (yet).");
    }
    
    public static int insertSpecialTypeCast (int from, int to) throws Exception {
        if (from == -1) {
            switch (to) {
                case TYPE_CHAR:
                    return I2C;
                case TYPE_SHORT:
                    return I2S;
                case TYPE_BYTE:
                    return I2B;
                case TYPE_FLOAT:
                    return D2F;
                default:
                    throw new Exception ("unknown type-cast for special type-cast.");
            }
        } else {
            switch (from) {
                case TYPE_FLOAT:
                    return F2D;
                default:
                    throw new Exception ("unknown type-cast for special type-cast.");
            }
        }
    }
    
    public static String getReductorJavaTypeName (int flag) throws Exception {
        switch (flag & 0xFF00) {
            case REDUCTOR_SET:
                return "java/util/Set";
            case REDUCTOR_MAP:
                return "java/util/Map";
            case REDUCTOR_DEFAULTMAP:
                return "java/util/Map";
            default:
                throw new Exception("unknown reductor-type. (" + flag + ")");
        }
    }
    
    public static String getReductorTypeName (int flag) throws Exception {
        switch (flag & 0xFF00) {
            case REDUCTOR_SET:
                return "edu/umass/pql/container/PSet";
            case REDUCTOR_MAP:
                return "edu/umass/pql/container/PMap";
            case REDUCTOR_DEFAULTMAP:
                return "edu/umass/pql/container/PDefaultMap";          
            default:
                throw new Exception("unknown reductor-type. (" + flag + ")");
        }
    }
    
    public static int getReductorArray (int flag) throws Exception {
        switch (flag & 0xFF) {
            case TYPE_INT:
                return T_INT;
            case TYPE_LONG:
                return T_LONG;
            case TYPE_BOOLEAN:
                return T_BOOLEAN;
            case TYPE_SHORT:
                return T_SHORT;
            case TYPE_CHAR:
                return T_CHAR;
            case TYPE_BYTE:
                return T_BYTE;
            case TYPE_DOUBLE:
                return T_DOUBLE;
            case TYPE_FLOAT:
                return T_FLOAT;
            default:
                throw new Exception("unknown array-reductor-type. (" + flag + ")");
        }
    }
    
    public static String getReductorMethodName (int flag) throws Exception {
        switch (flag & 0xFF00) {
            case REDUCTOR_SET:
                return "add";
            case REDUCTOR_MAP:
                return "put";
            case REDUCTOR_DEFAULTMAP:
                return "put";
            default:
                throw new Exception("unknown reductor-type. (" + flag + ")");
        }
    }
    
    public static String getContainerName (int flag) throws Exception {
        switch (flag & 0xFF00) {
            case TYPE_CONTAINS:
                return "edu/umass/pql/container/PSet";
            case TYPE_LOOKUP:
                return "edu/umass/pql/container/AMap";
            default:
                throw new Exception("unknown type. (" + flag + ")");
        }
    }
    
    public static String getJavaContainerName (int flag) throws Exception {
        switch (flag & 0xFF00) {
            case TYPE_CONTAINS:
                return "java/util/Set";
            case TYPE_LOOKUP:
                return "java/util/Map";
            default:
                throw new Exception("unknown type. (" + flag + ")");
        }
    }
    
    public static String getJavaContainerMethodName (int flag) throws Exception {
        switch (flag & 0xFF00) {
            case TYPE_CONTAINS:
                return "contains";
            case TYPE_LOOKUP:
                return "get";
            default:
                throw new Exception("unknown type. (" + flag + ")");
        }
    }
    
}
