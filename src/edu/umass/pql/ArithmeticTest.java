/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.container.AMap;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.meta.SelectPath;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import org.junit.*;
import static org.junit.Assert.*;

public class ArithmeticTest extends TestBase
{
	@Test
	public void
	testAPI()
	{
		Join p = PQLFactory.ADD_Int(i0w, i1r, i2r);
		checkAPI(p, 2, 3, new int[] { i0w, i1r, i2r });
	}
        
	@Test
	public void
	testIntAdd()
	{
		int e0 = 4;
		int e1 = 5;
		setInts(2, 3, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v2r = i2r;
		int v2w = i2w;
		Join p = PQLFactory.ADD_Int(v0r, v1r, v2r);
		checkFalse(p);
		assertEquals(e0, env.getInt(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
	}
        
        @Test
        public void
        testObjectIntCast()
        {
		int e0 = 4;
		int e1 = 65;
		setInts(2, 3, e0);
                setLongs(2, 3, e0);
                setDoubles(2, 3.1, e0);
                //setObjects(new Integer(2), new String("fds"), new Integer(5));
                setObjects(new Boolean(false), new Character('A'), new Integer(66));
		int v0r = o0r;
		int v1r = o1r;
		int v2r = o2r;
		int v2w = o2w;
		Join p = PQLFactory.ADD_Int(v0r, v1r, v2r);
		checkFalse(p);
		assertEquals(66, env.getInt(o2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
        }
        
        @Test
        public void
        testObjectDoubleCast()
        {
		double e0 = 4;
                double e1 = 65;
		setDoubles(2, 3, e0);
                //setObjects(new Integer(2), new String("fds"), new Integer(5));
                setObjects(new Boolean(false), new Character('A'), new Integer(66));
		int v0r = o0r;
		int v1r = o1r;
		int v2r = o2r;
		int v2w = o2w;
		Join p = PQLFactory.ADD_Int(v0r, v1r, v2r);
		checkFalse(p);
		assertEquals(66, env.getDouble(v2r), 0.0);
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getDouble(v2r), 0.0);
		p.setArg(2, v2r);
		checkTrue(p);
        }

	@Test
	public void
	testWildcard()
	{
		setInts(2, 3, 4);
		int v0r = i0r;
		int v1r = i1r;
		Join p = PQLFactory.ADD_Int(v0r, v1r, __);

		checkTrue(p);
		setInts(7, 3, 4);
		checkTrue(p);
	}

	@Test
	public void
	testLongAdd()
	{
		long e0 = 4l;
		long e1 = 5l;
		setLongs(2l, 3l, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v2r = l2r;
		int v2w = l2w;
		Join p = PQLFactory.ADD_Long(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
	}

	@Test
	public void
	testDoubleAdd()
	{
		double e0 = 4.0;
		double e1 = 5.0;
		setDoubles(1.75, 3.25, e0);
		int v0r = d0r;
		int v1r = d1r;
		int v2r = d2r;
		int v2w = d2w;
		Join p = PQLFactory.ADD_Double(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getDouble(v2r), 0.0);
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getDouble(v2r), 0.0);
		p.setArg(2, v2r);
		checkTrue(p);
	}


	@Test
	public void
	testIntSub()
	{
		int e0 = 4;
		int e1 = 5;
		setInts(8, 3, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v2r = i2r;
		int v2w = i2w;
		Join p = PQLFactory.SUB_Int(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getInt(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
	}


	@Test
	public void
	testLongSub()
	{
		long e0 = 4l;
		long e1 = 5l;
		setLongs(8l, 3l, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v2r = l2r;
		int v2w = l2w;
		Join p = PQLFactory.SUB_Long(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
	}

	@Test
	public void
	testDoubleSub()
	{
		double e0 = 4.0;
		double e1 = 5.0;
		setDoubles(8.25, 3.25, e0);
		int v0r = d0r;
		int v1r = d1r;
		int v2r = d2r;
		int v2w = d2w;
		Join p = PQLFactory.SUB_Double(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getDouble(v2r), 0.0);
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getDouble(v2r), 0.0);
		p.setArg(2, v2r);
		checkTrue(p);
	}


	@Test
	public void
	testIntMul()
	{
		int e0 = 4;
		int e1 = 6;
		setInts(2, 3, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v2r = i2r;
		int v2w = i2w;
		Join p = PQLFactory.MUL_Int(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getInt(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
	}


	@Test
	public void
	testLongMul()
	{
		long e0 = 4l;
		long e1 = 6l;
		setLongs(2l, 3l, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v2r = l2r;
		int v2w = l2w;
		Join p = PQLFactory.MUL_Long(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v2r));
		p.setArg(2, v2r);
		checkTrue(p);
	}

	@Test
	public void
	testDoubleMul()
	{
		double e0 = 4.1;
		double e1 = 5.25;
		setDoubles(1.5, 3.5, e0);
		int v0r = d0r;
		int v1r = d1r;
		int v2r = d2r;
		int v2w = d2w;
		Join p = PQLFactory.MUL_Double(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getDouble(v2r), 0.0);
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getDouble(v2r), 0.0);
		p.setArg(2, v2r);
		checkTrue(p);
	}


	@Test
	public void
	testIntDiv()
	{
		int e0 = 4;
		int e1 = 6;
		setInts(18, 3, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v2r = i2r;
		int v2w = i2w;
		Join p = PQLFactory.DIV_Int(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getInt(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v2r));
		p.setArg(2, v2r);
		checkTrue(p);

		env.setInt(v1r, 0);
		checkFalse(p);
	}


	@Test
	public void
	testLongDiv()
	{
		long e0 = 4l;
		long e1 = 6l;
		setLongs(18l, 3l, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v2r = l2r;
		int v2w = l2w;
		Join p = PQLFactory.DIV_Long(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v2r));
		p.setArg(2, v2r);
		checkTrue(p);

		env.setLong(v1r, 0l);
		checkFalse(p);
	}

	@Test
	public void
	testDoubleDiv()
	{
		double e0 = 4.1;
		double e1 = 5.25;
		setDoubles(18.375, 3.5, e0);
		int v0r = d0r;
		int v1r = d1r;
		int v2r = d2r;
		int v2w = d2w;
		Join p = PQLFactory.DIV_Double(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getDouble(v2r), 0.0);
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getDouble(v2r), 0.0);
		p.setArg(2, v2r);
		checkTrue(p);

		env.setDouble(v1r, 0.0);
		checkFalse(p);
	}

	@Test
	public void
	testIntNeg()
	{
		int e0 = 4;
		int e1 = -2;
		setInts(2, e0, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v1w = i1w;
		Join p = PQLFactory.NEG_Int(v0r, v1r);

		checkFalse(p);
		assertEquals(e0, env.getInt(v1r));
		p.setArg(1, v1w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v1r));
		p.setArg(1, v1r);
		checkTrue(p);
	}

	@Test
	public void
	testLongNeg()
	{
		long e0 = 4l;
		long e1 = -20000000000l;
		setLongs(20000000000l, e0, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v1w = l1w;
		Join p = PQLFactory.NEG_Long(v0r, v1r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v1r));
		p.setArg(1, v1w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v1r));
		p.setArg(1, v1r);
		checkTrue(p);
	}

	@Test
	public void
	testDoubleNeg()
	{
		double e0 = 4.0;
		double e1 = -0.125;
		setDoubles(0.125, e0, e0);
		int v0r = d0r;
		int v1r = d1r;
		int v1w = d1w;
		Join p = PQLFactory.NEG_Double(v0r, v1r);

		checkFalse(p);
		assertEquals(e0, env.getDouble(v1r), 0.0);
		p.setArg(1, v1w);
		checkTrue(p);
		assertEquals(e1, env.getDouble(v1r), 0.0);
		p.setArg(1, v1r);
		checkTrue(p);
	}

	@Test
	public void
	testIntBitInv()
	{
		int e0 = 4;
		int e1 = ~27;
		setInts(27, e0, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v1w = i1w;
		Join p = PQLFactory.BITINV_Int(v0r, v1r);

		checkFalse(p);
		assertEquals(e0, env.getInt(v1r));
		p.setArg(1, v1w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v1r));
		p.setArg(1, v1r);
		checkTrue(p);
	}

	@Test
	public void
	testLongBitInv()
	{
		long e0 = 4l;
		long e1 = ~27l;
		setLongs(27l, e0, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v1w = l1w;
		Join p = PQLFactory.BITINV_Long(v0r, v1r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v1r));
		p.setArg(1, v1w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v1r));
		p.setArg(1, v1r);
		checkTrue(p);
	}


	@Test
	public void
	testIntMod()
	{
		int e0 = 4;
		int e1 = 3;
		setInts(15, 6, e0);
		int v0r = i0r;
		int v1r = i1r;
		int v2r = i2r;
		int v2w = i2w;
		Join p = PQLFactory.MOD_Int(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getInt(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getInt(v2r));
		p.setArg(2, v2r);
		checkTrue(p);

		env.setInt(v1r, 0);
		checkFalse(p);
	}


	@Test
	public void
	testLongMod()
	{
		long e0 = 4l;
		long e1 = 3l;
		setLongs(15l, 6l, e0);
		int v0r = l0r;
		int v1r = l1r;
		int v2r = l2r;
		int v2w = l2w;
		Join p = PQLFactory.MOD_Long(v0r, v1r, v2r);

		checkFalse(p);
		assertEquals(e0, env.getLong(v2r));
		p.setArg(2, v2w);
		checkTrue(p);
		assertEquals(e1, env.getLong(v2r));
		p.setArg(2, v2r);
		checkTrue(p);

		env.setLong(v1r, 0l);
		checkFalse(p);
	}

	public void
	try_ternary_int(Join p, int a, int b, int c)
	{
		p.setArg(0, i0r);
		p.setArg(1, i1r);
		p.setArg(2, i2r);

		setInts(a, b, c-1);

		checkFalse(p);
		assertEquals(c - 1, env.getInt(i2r));
		p.setArg(2, i2w);
		checkTrue(p);
		assertEquals(c, env.getInt(i2r));
		p.setArg(2, i2r);
		checkTrue(p);
	}

	public void
	try_ternary_long(Join p, long a, long b, long c)
	{
		p.setArg(0, l0r);
		p.setArg(1, l1r);
		p.setArg(2, l2r);

		setLongs(a, b, c - 1l);

		checkFalse(p);
		assertEquals(c - 1l, env.getLong(l2r));
		p.setArg(2, l2w);
		checkTrue(p);
		assertEquals(c, env.getLong(l2r));
		p.setArg(2, l2r);
		checkTrue(p);
	}

	@Test
	public void
	testBitOR_Int()
	{
		try_ternary_int(PQLFactory.BITOR_Int(0,0,0), 0x40124, 0x13327, 0x53327);
	}

	@Test
	public void
	testBitOR_Long()
	{
		try_ternary_long(PQLFactory.BITOR_Long(0,0,0), 0x800000040124l, 0x200000013327l, 0xa00000053327l);
	}


	@Test
	public void
	testBitAND_Int()
	{
		try_ternary_int(PQLFactory.BITAND_Int(0,0,0), 0x4092c, 0x63337, 0x40124);
	}

	@Test
	public void
	testBitAND_Long()
	{
		try_ternary_long(PQLFactory.BITAND_Long(0,0,0), 0x20000004092cl, 0xa00000063337l, 0x200000040124l); 
	}


	@Test
	public void
	testBitXOR_Int()
	{
		try_ternary_int(PQLFactory.BITXOR_Int(0,0,0), 0x40124, 0x53327, 0x13203);
	}

	@Test
	public void
	testBitXOR_Long()
	{
		try_ternary_long(PQLFactory.BITXOR_Long(0,0,0), 0x800000040124l, 0xa00000053327l, 0x200000013203l); 
	}


	@Test
	public void
	testBitSHL_Int()
	{
		try_ternary_int(PQLFactory.BITSHL_Int(0,0,0), 0x31337, 8, 0x3133700);
	}

	@Test
	public void
	testBitSHL_Long()
	{
		try_ternary_long(PQLFactory.BITSHL_Long(0,0,0), 0x31337000000000l, 8l, 0x3133700000000000l);
	}

	@Test
	public void
	testBitSSHR_Int()
	{
		try_ternary_int(PQLFactory.BITSSHR_Int(0,0,0), 0xa0000000, 4, 0x0a000000);
	}

	@Test
	public void
	testBitSSHR_Long()
	{
		try_ternary_long(PQLFactory.BITSSHR_Long(0,0,0), 0xa000000000000000l, 4l, 0x0a00000000000000l);
	}

	@Test
	public void
	testBitSHR_Int()
	{
		try_ternary_int(PQLFactory.BITSHR_Int(0,0,0), 0xa0000000, 4, 0xfa000000);
	}

	@Test
	public void
	testBitSHR_Long()
	{
		try_ternary_long(PQLFactory.BITSHR_Long(0,0,0), 0xa000000000000000l, 4l, 0xfa00000000000000l);
	}
}
