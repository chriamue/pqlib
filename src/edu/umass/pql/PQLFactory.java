/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.lang.reflect.Method;
import java.util.*;
import rc2.representation.RuntimeCreatorException;

public class PQLFactory
{
	public enum ParallelisationMode {
		NONPARALLEL,
		SEGMENTED,
		STRIPED,
		UNDEFINED
	};

	public static final ParallelisationMode[] PARALLELISATION_MODES = new ParallelisationMode[] { ParallelisationMode.NONPARALLEL, ParallelisationMode.SEGMENTED, ParallelisationMode.STRIPED };
	public static boolean
	parallelismPermitted()
	{
		return parallelisation_mode != ParallelisationMode.NONPARALLEL;
	}
	public static void
	setParallelisationMode(ParallelisationMode mode)
	{
		parallelisation_mode = mode;
	}
	public static ParallelisationMode
	getParallelisationMode()
	{
		return parallelisation_mode;
	}


	// --------------------------------------------------------------------------------
	// Comparisons
	public static Join EQ_Int(int lhs, int rhs)					{ return new Comparison.EQ_Int(lhs, rhs); }
	public static Join EQ_Long(int lhs, int rhs)					{ return new Comparison.EQ_Long(lhs, rhs); }
	public static Join EQ_Double(int lhs, int rhs)					{ return new Comparison.EQ_Double(lhs, rhs); }
	public static Join EQ_Object(int lhs, int rhs)					{ return new Comparison.EQ_Object(lhs, rhs); }
	public static Join EQ_String(int lhs, int rhs)					{ return new Comparison.EQ_String(lhs, rhs); }

	// We can't use Not to implement these, since a writable rhs would yield different semantics
	public static Join NEQ_Int(int lhs, int rhs)					{ return new Comparison.NEQ_Int(lhs, rhs); }
	public static Join NEQ_Long(int lhs, int rhs)					{ return new Comparison.NEQ_Long(lhs, rhs); }
	public static Join NEQ_Double(int lhs, int rhs)					{ return new Comparison.NEQ_Double(lhs, rhs); }
	public static Join NEQ_Object(int lhs, int rhs)					{ return new Comparison.NEQ_Object(lhs, rhs); }
	public static Join NEQ_String(int lhs, int rhs)					{ return new Comparison.NEQ_String(lhs, rhs); }

	public static Join LT_Int(int lhs, int rhs)					{ return new Comparison.LT_Int(lhs, rhs); }
	public static Join LT_Long(int lhs, int rhs)					{ return new Comparison.LT_Long(lhs, rhs); }
	public static Join LT_Double(int lhs, int rhs)					{ return new Comparison.LT_Double(lhs, rhs); }

	public static Join LTE_Int(int lhs, int rhs)					{ return new Comparison.LTE_Int(lhs, rhs); }
	public static Join LTE_Long(int lhs, int rhs)					{ return new Comparison.LTE_Long(lhs, rhs); }
	public static Join LTE_Double(int lhs, int rhs)					{ return new Comparison.LTE_Double(lhs, rhs); }

	// --------------------------------------------------------------------------------
	// Arithmetic

	public static Join TRUE = Arithmetic.TRUE;
	public static Join FALSE = Arithmetic.FALSE;

	public static Join ADD_Int(int v0, int v1, int result)				{ return new Arithmetic.ADD_Int(v0, v1, result); }
	public static Join ADD_Long(int v0, int v1, int result)				{ return new Arithmetic.ADD_Long(v0, v1, result); }
	public static Join ADD_Double(int v0, int v1, int result)			{ return new Arithmetic.ADD_Double(v0, v1, result); }

	public static Join SUB_Int(int v0, int v1, int result)				{ return new Arithmetic.SUB_Int(v0, v1, result); }
	public static Join SUB_Long(int v0, int v1, int result)				{ return new Arithmetic.SUB_Long(v0, v1, result); }
	public static Join SUB_Double(int v0, int v1, int result)			{ return new Arithmetic.SUB_Double(v0, v1, result); }

	public static Join MUL_Int(int v0, int v1, int result)				{ return new Arithmetic.MUL_Int(v0, v1, result); }
	public static Join MUL_Long(int v0, int v1, int result)				{ return new Arithmetic.MUL_Long(v0, v1, result); }
	public static Join MUL_Double(int v0, int v1, int result)			{ return new Arithmetic.MUL_Double(v0, v1, result); }

	public static Join DIV_Int(int v0, int v1, int result)				{ return new Arithmetic.DIV_Int(v0, v1, result); }
	public static Join DIV_Long(int v0, int v1, int result)				{ return new Arithmetic.DIV_Long(v0, v1, result); }
	public static Join DIV_Double(int v0, int v1, int result)			{ return new Arithmetic.DIV_Double(v0, v1, result); }

	public static Join MOD_Int(int v0, int v1, int result)				{ return new Arithmetic.MOD_Int(v0, v1, result); }
	public static Join MOD_Long(int v0, int v1, int result)				{ return new Arithmetic.MOD_Long(v0, v1, result); }

	public static Join NEG_Int(int arg, int result)					{ return new Arithmetic.NEG_Int(arg, result); }
	public static Join NEG_Long(int arg, int result)				{ return new Arithmetic.NEG_Long(arg, result); }
	public static Join NEG_Double(int arg, int result)				{ return new Arithmetic.NEG_Double(arg, result); }

	public static Join BITINV_Int(int arg, int result)				{ return new Arithmetic.BITINV_Int(arg, result); }
	public static Join BITINV_Long(int arg, int result)				{ return new Arithmetic.BITINV_Long(arg, result); }

	public static Join BITOR_Int(int arg0, int arg1, int result)			{ return new Arithmetic.BITOR_Int(arg0, arg1, result); }
	public static Join BITOR_Long(int arg0, int arg1, int result)			{ return new Arithmetic.BITOR_Long(arg0, arg1, result); }

	public static Join BITAND_Int(int arg0, int arg1, int result)			{ return new Arithmetic.BITAND_Int(arg0, arg1, result); }
	public static Join BITAND_Long(int arg0, int arg1, int result)			{ return new Arithmetic.BITAND_Long(arg0, arg1, result); }

	public static Join BITXOR_Int(int arg0, int arg1, int result)			{ return new Arithmetic.BITXOR_Int(arg0, arg1, result); }
	public static Join BITXOR_Long(int arg0, int arg1, int result)			{ return new Arithmetic.BITXOR_Long(arg0, arg1, result); }

	public static Join BITSHL_Int(int arg0, int arg1, int result)			{ return new Arithmetic.BITSHL_Int(arg0, arg1, result); }
	public static Join BITSHL_Long(int arg0, int arg1, int result)			{ return new Arithmetic.BITSHL_Long(arg0, arg1, result); }

	public static Join BITSHR_Int(int arg0, int arg1, int result)			{ return new Arithmetic.BITSHR_Int(arg0, arg1, result); }
	public static Join BITSHR_Long(int arg0, int arg1, int result)			{ return new Arithmetic.BITSHR_Long(arg0, arg1, result); }

	public static Join BITSSHR_Int(int arg0, int arg1, int result)			{ return new Arithmetic.BITSSHR_Int(arg0, arg1, result); }
	public static Join BITSSHR_Long(int arg0, int arg1, int result)			{ return new Arithmetic.BITSSHR_Long(arg0, arg1, result); }

	// --------------------------------------------------------------------------------
	// Containers
	public static Join INT_RANGE_CONTAINS(int start, int stop, int counter)		{ return new Container.INT_RANGE_CONTAINS(start, stop, counter); }
	public static Join LONG_RANGE_CONTAINS(int start, int stop, int counter)	{ return new Container.LONG_RANGE_CONTAINS(start, stop, counter); }
	public static Join CONTAINS(int set, int element) /* Sets */			{ return new Container.CONTAINS(set, element); }
	public static Join LOOKUP(int map, int key, int value) /* Maps only */		{ return new Container.LOOKUP(map, key, value); }
	public static Join ARRAY_LOOKUP_Int(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Int(array, key, value); }
	public static Join ARRAY_LOOKUP_Long(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Long(array, key, value); }
	public static Join ARRAY_LOOKUP_Short(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Short(array, key, value); }
	public static Join ARRAY_LOOKUP_Char(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Char(array, key, value); }
	public static Join ARRAY_LOOKUP_Boolean(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Boolean(array, key, value); }
	public static Join ARRAY_LOOKUP_Byte(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Byte(array, key, value); }
	public static Join ARRAY_LOOKUP_Float(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Float(array, key, value); }
	public static Join ARRAY_LOOKUP_Double(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Double(array, key, value); }
	public static Join ARRAY_LOOKUP_Object(int array, int key, int value)		{ return new Container.ARRAY_LOOKUP_Object(array, key, value); }
	public static Join POLY_LOOKUP(int map, int key, int value) /* arrays or maps */{ return new Container.POLY_LOOKUP(map, key, value); }

	public static Join POLY_SIZE(int set, int size)					{ return new Container.POLY_SIZE(set, size); }
	public static Join SET_SIZE(int set, int size)					{ return new Container.SET_SIZE(set, size); }
	public static Join MAP_SIZE(int map, int size)					{ return new Container.MAP_SIZE(map, size); }
	public static Join INT_ARRAY_SIZE(int array, int size)				{ return new Container.INT_ARRAY_SIZE(array, size); }
	public static Join LONG_ARRAY_SIZE(int array, int size)				{ return new Container.LONG_ARRAY_SIZE(array, size); }
	public static Join SHORT_ARRAY_SIZE(int array, int size)			{ return new Container.SHORT_ARRAY_SIZE(array, size); }
	public static Join CHAR_ARRAY_SIZE(int array, int size)				{ return new Container.CHAR_ARRAY_SIZE(array, size); }
	public static Join BOOLEAN_ARRAY_SIZE(int array, int size)			{ return new Container.BOOLEAN_ARRAY_SIZE(array, size); }
	public static Join BYTE_ARRAY_SIZE(int array, int size)				{ return new Container.BYTE_ARRAY_SIZE(array, size); }
	public static Join FLOAT_ARRAY_SIZE(int array, int size)			{ return new Container.FLOAT_ARRAY_SIZE(array, size); }
	public static Join DOUBLE_ARRAY_SIZE(int array, int size)			{ return new Container.DOUBLE_ARRAY_SIZE(array, size); }
	public static Join OBJECT_ARRAY_SIZE(int array, int size)			{ return new Container.OBJECT_ARRAY_SIZE(array, size); }

	// --------------------------------------------------------------------------------
	// Types

	// `c' here is supposed to be a type, but compilers may abuse it to store their own type representation
	public static Join JAVA_TYPE(Object c, int it)			{ return new Type.JAVA_TYPE(c, it); }
	public static Join INT(int it)					{ return new Type.INT(it); }
	public static Join LONG(int it)					{ return new Type.LONG(it); }
	public static Join BOOLEAN(int it)				{ return new Type.BOOLEAN(it); }
	public static Join BYTE(int it)					{ return new Type.BYTE(it); }
	public static Join SHORT(int it)				{ return new Type.SHORT(it); }
	public static Join CHAR(int it)					{ return new Type.CHAR(it); }

	public static Join COERCE_Boolean(int src, int dest)		{ return new Type.COERCE_Boolean(src, dest); }
	public static Join COERCE_Byte(int src, int dest)		{ return new Type.COERCE_Byte(src, dest); }
	public static Join COERCE_Short(int src, int dest)		{ return new Type.COERCE_Short(src, dest); }
	public static Join COERCE_Char(int src, int dest)		{ return new Type.COERCE_Char(src, dest); }
	public static Join COERCE_Float(int src, int dest)		{ return new Type.COERCE_Float(src, dest); }

	// field should be java.lang.reflect.Field if it is to be executed, but code generators may choose to abuse this
	public static Join FIELD(Object field, int obj, int result)					{ return new Field(field, obj, result); }
	public static Join FIELD(Class<?> c, String fieldname, int obj, int result)			{ return FIELD(find_field(c, fieldname), obj, result); }
	public static Join INSTANTIATE(java.lang.reflect.Constructor<?> cons, int[] args, int result)	{ return new Instantiation(cons, args, result); }

	// --------------------------------------------------------------------------------
	public static final ParallelisationMode DEFAULT_PARALLELISATION_MODE = ParallelisationMode.SEGMENTED;
	private static ParallelisationMode parallelisation_mode = parseParallelisationMode(System.getProperty("PQL_PARALLELISM_MODE"));

	// --------------------------------------------------------------------------------
	// Control structures
	public static Join flattenBlock(Join block)						{ return AbstractBlock.flattenBlock(block); }

	public static ControlStructure ConjunctiveBlock(Collection<Join> body)			{ return new AbstractBlock.Conjunctive(body); }
	public static ControlStructure ConjunctiveBlock(Join... body)				{ return new AbstractBlock.Conjunctive(body); }
	public static ControlStructure ConjunctiveBlock(Join body)				{ return new AbstractBlock.Conjunctive(new Join[] { body }); }
	public static ControlStructure ConjunctiveBlock()					{ return new AbstractBlock.Conjunctive(new Join[0]); }

	public static ControlStructure DisjunctiveBlock(Collection<Join> body)			{ return new AbstractBlock.Disjunctive(body); }
	public static ControlStructure DisjunctiveBlock(Join... body)				{ return new AbstractBlock.Disjunctive(body); }
	public static ControlStructure DisjunctiveBlockBlock(Join body)				{ return new AbstractBlock.Disjunctive(new Join[] { body }); }
	public static ControlStructure DisjunctiveBlock()					{ return new AbstractBlock.Disjunctive(new Join[0]); }

	public static ControlStructure IfThenElse(Join cond, Join truebr, Join falsebr)		{ return DisjunctiveBlock(ConjunctiveBlock(cond, truebr), ConjunctiveBlock(Not(cond.copyRecursively()), falsebr)); }//{ return new IfThenElse(cond, truebr, falsebr); }
	public static ControlStructure Or(Join lhs, Join rhs)					{ return DisjunctiveBlock(lhs, rhs); }
	public static ControlStructure Bool(Join body, int result)				{ return new NoFail.Bool(body, result); }
	public static ControlStructure NoFail(Join body)					{ return new NoFail(body); }
	public static ControlStructure Not(Join body)						{ return new Not(body); }
	public static Reduction Forall(Join binder, Join evaluator,
				       int fresh_var, int result_var)				{ return Reduction(Reductors.FORALL(Env.readVar(fresh_var), result_var),
														   ConjunctiveBlock(binder,
																    Bool(evaluator, Env.writeVar(fresh_var))
																    )); }
	public static Reduction Exists(Join binder, Join evaluator,
				       int fresh_var, int result_var)				{ return Reduction(Reductors.EXISTS(Env.readVar(fresh_var), result_var),
														   ConjunctiveBlock(binder,
																    Bool(evaluator, Env.writeVar(fresh_var))
																    )); }
	public static Reduction Reduction(edu.umass.pql.Reductor[] reductors,
	 				  Join body)						{ if (!body.hasRandomAccess())
													return new ReductionImpl.Sequential(reductors, body);
												  else switch (parallelisation_mode) {
													  case NONPARALLEL:
														  return new ReductionImpl.Sequential(reductors, unparallel(body));
													  case SEGMENTED:
														  return new ReductionImpl.ParallelSegmented(reductors, unparallel(body));
													  case STRIPED:
														  return new ReductionImpl.ParallelStriped(reductors, unparallel(body));
													  case UNDEFINED:
													  default:
														  throw new RuntimeException("Undefined reduction parallelism:  you are probably writing a test case and forgot to use `TestBase.paratest' to invoke yourself");
													  }
												}
	public static Reduction Reduction(edu.umass.pql.Reductor reductor,
	 				  Join body)						{ return Reduction(new edu.umass.pql.Reductor[] { reductor }, body); }

	public static Reduction Reduction(edu.umass.pql.Reductor reductor,
	 				  Join... body)						{ return Reduction(reductor, ConjunctiveBlock(body)); }

	public static Reduction Reduction(edu.umass.pql.Reductor[] reductors,
	 				  Join... body)						{ return Reduction(reductors, ConjunctiveBlock(body)); }


	// --------------------------------------------------------------------------------
	public static class Reductors {
		public static edu.umass.pql.Reductor FORALL(int input, int output)					{ return new edu.umass.pql.il.reductor.Forall(output, input); }
		public static edu.umass.pql.Reductor EXISTS(int input, int output)					{ return new edu.umass.pql.il.reductor.Exists(output, input); }

		public static edu.umass.pql.Reductor SET(int src, int dest)							{ return new edu.umass.pql.il.reductor.SetReductor(dest, src); }

		public static edu.umass.pql.Reductor ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.AnyObject(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor INT_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Int(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor LONG_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Long(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor DOUBLE_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Double(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor OBJECT_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.AnyObject(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor BYTE_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Byte(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor CHAR_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Char(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor SHORT_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Short(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor BOOLEAN_ARRAY(int src_key, int src_value, int dest)			{ return new edu.umass.pql.il.reductor.ArrayReductor.Boolean(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor FLOAT_ARRAY(int src_key, int src_value, int dest)				{ return new edu.umass.pql.il.reductor.ArrayReductor.Float(dest, src_key, src_value); }

		public static edu.umass.pql.Reductor MAP(int src_key, int src_value, int dest)					{ return new edu.umass.pql.il.reductor.MapReductor(dest, src_key, src_value); }
		public static edu.umass.pql.Reductor N_MAP(int src_key, Reductor src_value_child, int dest)			{ Reductor r = new edu.umass.pql.il.reductor.MapReductor(dest, src_key, Reductor.INPUT_FROM_INNER_REDUCTOR); r.setInnerReductor(src_value_child); return r; }
		public static edu.umass.pql.Reductor DEFAULT_MAP(int deflt, int src_key, int src_value, int dest)		{ return new edu.umass.pql.il.reductor.DefaultMapReductor(dest, deflt, src_key, src_value); }
		public static edu.umass.pql.Reductor N_DEFAULT_MAP(int deflt, int src_key, Reductor src_value_child, int dest)	{ Reductor r = new edu.umass.pql.il.reductor.DefaultMapReductor(dest, deflt, src_key, Reductor.INPUT_FROM_INNER_REDUCTOR); r.setInnerReductor(src_value_child); return r; }

		public static edu.umass.pql.Reductor METHOD_ADAPTER(Object reductor, int input_var, int result_var)	{ return edu.umass.pql.il.reductor.MethodAdapterReductor.make(result_var, reductor, input_var, null); }
		public static edu.umass.pql.Reductor METHOD_ADAPTER(Class<?> _class, String method_name, Class<?> argtype,
								    int input_var, int result_var)			{ try { return edu.umass.pql.il.reductor.MethodAdapterReductor.make(result_var, _class.getDeclaredMethod(method_name, argtype, argtype), input_var, argtype); } catch (NullPointerException e) { System.err.println("Missing DefaultValueX annotation in method " + _class + "." + method_name); throw e; } catch (Exception e) { throw new RuntimeException(e); } }
	}

	// --------------------------------------------------------------------------------

        public static edu.umass.pql.Join EXTENSION(String kind, int ... args)							{ try {
                                                                                                                                    return new rc2.representation.PQLExtension(kind, args);
                                                                                                                                    } catch (RuntimeCreatorException e) {
                                                                                                                                        throw new RuntimeException(e);
                                                                                                                                } };
	/*public static edu.umass.pql.Join EXTENSION(String kind, int ... args)							{ return new rc2.representation.PQLExtension(extensionByName(kind), args); };
	public static  rc2.representation.PQLExtension.ExtensionName extensionByName(String extname)				{ if (extensions_by_name.containsKey(extname))
																	return extensions_by_name.get(extname);
																  throw new RuntimeException("Unsupported extension: `"+extname+"'; supported are: " + rc2.representation.PQLExtension.ExtensionName.values());
																}*/

	// --------------------------------------------------------------------------------

	public static class Meta {
		public static edu.umass.pql.MetaJoin SELECT_PATH(Join... components)					{ return SelectPath.create(components); }
		public static edu.umass.pql.Join SCHEDULE(Join body, MetaJoin... components)				{ return new Schedule(body, components); }
	}

	public static class Analysis {
		public static void setReadFlags(Join join, VarSet input_vars) { ReadFlagSetterVisitor v = new ReadFlagSetterVisitor(input_vars); join.accept(v); if (v.getErrors() != null) throw new RuntimeException("Read flag assignment:  Errors in " + v.getErrors().toString()); }
		/**
		 * Unused writes replaced by wildcard.
		 * Output vars collect variables whose results might actually get read.
		 */
		public static void markUnusedWrites(Join join, VarSet output_vars) { join.accept(new NullWriteSetterVisitor(output_vars)); }
	}

	// ================================================================================
	// strings for PQL code generation

	public static class Gen {
		public static final String PACKAGE = "edu.umass.pql";
		public static final String QUERY_CLASS = PACKAGE + ".Query";
		public static final String FACTORY_CLASS = PACKAGE + ".PQLFactory";
		public static final String REDUCTOR_FACTORY_CLASS = FACTORY_CLASS + "$Reductors";
		public static final String ENV_CLASS = PACKAGE + ".Env";
		public static final String JOIN_CLASS = PACKAGE + ".Join";
		public static final String REDUCTION_CLASS = PACKAGE + ".Reduction";
		public static final String CONTROL_STRUCTURE_CLASS = PACKAGE + ".ControlStructure";
		public static final String CONJUNCTIVE_BLOCK_CLASS = PACKAGE + ".AbstractBlock$Conjunctive";
		public static final String DISJUNCTIVE_BLOCK_CLASS = PACKAGE + ".AbstractBlock$Disjunctive";
		public static final String PREDICATE_CLASS = PACKAGE + ".il.Predicate";
		public static final String REDUCTOR_CLASS = PACKAGE + ".Reductor";
	}

	// ================================================================================
	// --------------------------------------------------------------------------------
	// helpers

	public static Join Conjunction(List<Join> body) { if (body.size() == 0)
									return TRUE;
								else if (body.size() == 1)
									return body.get(0);
								else
									return ConjunctiveBlock(body);
								}


	public static Join Disjunction(List<Join> body) { if (body.size() == 0)
									return FALSE;
								else if (body.size() == 1)
									return body.get(0);
								else
									return DisjunctiveBlock(body);
								}



	private static final java.lang.reflect.Field
	find_field(Class<?> c, String fieldname)
	{
		java.lang.reflect.Field field;
		try {
			field = c.getField(fieldname);
		} catch (NoSuchFieldException e) {
			field = null;
			for (java.lang.reflect.Field f : c.getDeclaredFields())
				if (f.getName().equals(fieldname)) {
					field = f;
					break;
				}
			if (field == null)
				throw new RuntimeException(e);
		}
		return field;
	}


	public static final Join
	unparallel(Join join)
	{
		if (join instanceof ControlStructure) {
			ControlStructure cs = (ControlStructure) join;

			if (cs instanceof ReductionImpl.Parallel) {
				ReductionImpl red = (ReductionImpl) cs;
				cs = new ReductionImpl.Sequential(red.getReductors(),
								  red.getComponent(0));
			}

			for (int i = 0; i < cs.getComponentsNr(); i++) {
				cs.setComponent(i, unparallel(cs.getComponent(i)));
			}

			return cs;
		}
		return join;
	}

	public static final ParallelisationMode
	parseParallelisationMode(String pms)
	{
		boolean silent = false;
		if (pms == null)
			return DEFAULT_PARALLELISATION_MODE;

		ParallelisationMode result = null;

		String[] segments = pms.split(",");

		for (String s : segments) {
			if (s.equalsIgnoreCase("none"))
				result = ParallelisationMode.NONPARALLEL;
			else if (s.equalsIgnoreCase("segmented"))
				result = ParallelisationMode.SEGMENTED;
			else if (s.equalsIgnoreCase("striped"))
				result = ParallelisationMode.STRIPED;
			else if (s.equals("silent"))
				silent = true;
			else if (s.equals("verbose"))
				silent = false;
			else
				throw new RuntimeException("Unknown parallelism mode: `" + s + "'");
		}

		if (result == null)
			throw new RuntimeException("Did not set parallelism mode!");

		if (!silent)
			System.out.println("Parallelism mode: " + result);
		return result;
	}

	/*static HashMap<String, rc2.representation.PQLExtension.ExtensionName> extensions_by_name = new HashMap<String, rc2.representation.PQLExtension.ExtensionName>();
	static {
		for (rc2.representation.PQLExtension.ExtensionName en : rc2.representation.PQLExtension.ExtensionName.values()) {
			extensions_by_name.put(en.toString(), en);
		}
	}*/
}
