/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import edu.umass.bc.JoinFlagsVisitor;

/**
 * A Join is an iterator over all possible bindings for various variables.
 * Semantically, it is the nondeterminism monad.
 *
 * @author creichen
 */
public abstract class Join implements Cloneable, PQLParameterized
{
	/**
	 * Number of arguments for this iterator that the iterator might write to
	 *
	 * @return The number of arguments for this iterator.  These are always the end of the argument list.
	 */
	public final int
	getWritableArgsNr()
	{
		return this.getArgsNr() - this.getReadArgsNr();
	}

	//@Override  // JDK 1.5 is confused about this
        public final boolean
	isValidArg(int index)
	{
		return !Env.isWildcard(this.getArg(index));
	}

	/**
	 * Number of arguments for this iterator
	 *
	 * @return The number of arguments for this iterator
	 */
	public abstract int
	getArgsNr();

	/**
	 * Number of read-only arguments for this iterator
	 *
	 * @return The number of arguments for this iterator that must be read-only
	 */
	public abstract int
	getReadArgsNr();

	/**
	 * change one of the arguments for the iterator
	 *
	 * @param i Index of the argument to change
	 * @param v New argument variable
	 */
	public abstract void
	setArg(int i, int v);

        /**
         *
         * @param visitor
         * @return the flags of the specified join
         */
        public int accept(JoinFlagsVisitor visitor)
        {
            return 0;
        }

	/**
	 * Binary symmetric operator?
	 */
	public boolean
	isSymmetric()
	{
		return false;
	}

	public final void
	makeReadArg(int i)
	{
		this.setArg(i, Env.readVar(this.getArg(i)));
	}

	public final void
	makeWriteArg(int i)
	{
		this.setArg(i, Env.writeVar(this.getArg(i)));
	}

	/**
	 * Adds all variables that this join must read to the specified set
	 *
	 */
	public void
	addReadDependencies(VarSet set, VarSet exclusions)
	{
		for (int i = 0; i < this.getReadArgsNr(); i++) {
			final int v = this.getArg(i);
			if (exclusions == null || !exclusions.contains(v))
				set.insert(this.getArg(i));
		}

		if (exclusions != null)
			for (int i = this.getReadArgsNr(); i < this.getArgsNr(); i++)
				exclusions.insert(this.getArg(i));
	}

	public final void
	addReadDependencies(VarSet set)
	{
		addReadDependencies(set, null);
	}

	public void
	addWriteDependencies(VarSet set)
	{
		for (int i = this.getReadArgsNr(); i < this.getArgsNr(); i++) {
			final int v = this.getArg(i);
			set.insert(this.getArg(i));
		}
	}

	/**
	 * Adds all variables the join is configured to read (i.e., considering the read flag)
	 */
	public void
	addActualReads(VarSet set, VarSet exclusions)
	{
		for (int i = 0; i < this.getArgsNr(); i++) {
			final int v = this.getArg(i);
			if (Env.isReadVar(v) && !Env.isWildcard(v)
			    && (exclusions == null || !exclusions.contains(v)))
				set.insert(this.getArg(i));
		}

		if (exclusions != null)
			for (int i = this.getReadArgsNr(); i < this.getArgsNr(); i++)
				if (Env.isWriteVar(this.getArg(i)) && !Env.isWildcard(this.getArg(i)))
					exclusions.insert(this.getArg(i));
	}

	/**
	 * Adds all variables that this join must read to the specified set
	 *
	 */
	public void
	addAllVariables(VarSet set)
	{
		for (int i = 0; i < this.getArgsNr(); i++)
			set.insert(this.getArg(i));
	}

	/**
	 * read one of the arguments for the iterator
	 *
	 * @param i Index of the argument to read
	 * @return Argument variable
	 */
	public abstract int
	getArg(int i);


	/**
	 * Resets and initialises the Join operator for sequential access.
	 *
	 * Precondition: between two calls to `reset()',
	 * the bindings of all read-arguments remain constant.
	 * Postcondition: `next()' will never produce the same bindings between two calls to `reset()'.
	 *
	 * @param env The environment fixing all `read' variables
	 */
	public abstract void
	reset(Env env);

	/**
	 * Resets the join operator for random access (if possible).
	 *
	 * If `hasRandomAccess', this operation prepares the operator for `getAt' operations.
	 */
	public void
	resetForRandomAccess(Env env)
	{
		this.reset(env);
	}

	/**
	 * Iterates through all possibilities for the iterator
	 *
	 * Invariant:  See `reset'.
	 *
	 * @param env The environment.
	 * @return true iff a fresh binding was effected.
	 */
	public abstract boolean
	next(Env env);

	/**
	 * Accept a visitor
	 *
	 * @param visitor the visitor to accept
	 */
	public abstract void
	accept(JoinVisitor visitor);

	/**
	 * Return total number of possibilities that we will be stepping over via `next', i.e., the maximum range (plus one) for `getAt'.
	 *
	 * This operation may assume all bindings in `env' to be constants
	 *
	 * @return number of possibilities, or a negative value for special operators with a very large range (GETSIZE_INT, GETSIZE_LONG)
	 */
	@SuppressWarnings("unused")
	public int
	getFanout(Env env)
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * If returned by getSize, this indicates that the Join operator is INT, i.e., it binds argument zero to all possible ints.
	 */
	public static final int GETSIZE_INT	= -1;

	/**
	 * If returned by getSize, this indicates that the Join operator is LONG, i.e., it binds argument zero to all possible ints.
	 */
	public static final int GETSIZE_LONG	= -2;


	/**
	 * Determine whether random access is supported.
	 *
	 * If this operation returns true, getFanout(), getAtIndex() and moveToIndex() must be supported.
	 */
	public boolean
	hasRandomAccess()
	{
		return false;
	}

	/**
	 * Construct a deep copy of this join
	 *
	 * @return a deep clone of the join
	 */
	public abstract Join
	copyRecursively();


	/**
	 * Helper method for copyRecursively
	 */
	protected Join
	copy()
	{
		return (Join) this.clone();
	}

	public Join
	clone()
	{
		try {
			return (Join) super.clone();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static final int GETAT_NONE = 0; // no binding
	public static final int GETAT_ONE = 1; // one and only one binding
	public static final int GETAT_ANY = 2; // one binding, but there may be more

	/**
	 * Initialises the join for accessing all values at a specific index
	 *
	 * @param env The env to bind to
	 * @param offset The offset to investigate
	 */
	@SuppressWarnings("unused")
	public void
	moveToIndex(Env env, int offset)
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Random access ot the possibilities for this join.
	 *
	 * This operation need only be supported if hasRandomAccess is supported.
	 * It must be called only after moveToIndex() for the very same index.
	 *
	 * @param env The env to bind to
	 * @param offset The offset to investigate.  This will be no less than 0 and less than getSize();
	 * @return GETAT_NONE if the binding failed, GETAT_ONE if there was exactly one binding, and GETAT_ANY
	 * if there was a binding but there may be more
	 */
	@SuppressWarnings("unused")
 	public int
	getAtIndex(Env env, int offset)
	{
		throw new UnsupportedOperationException();
	}

	public boolean
	isBlock()
	{
		return this instanceof AbstractBlock;
	}

	public boolean
	isControlStructure()
	{
		return this instanceof ControlStructure;
	}

	/**
	 * Get the name of the static PQLFactory constructor method.  Returns `null' if the name is ambiguous/problematic.
	 */
	public abstract String
	getConstructorName();


	/**
	 * Determines which, if any, parameter of this join would participate in range inference (a source language facility)
	 *
	 * @return Index of the parameter participating in range inference; -1 otherwise.
	 */
	public int
	getRangeInferenceParameter()
	{
		return -1;
	}

	// --------------------------------------------------------------------------------
	// Access path selection helper API

	/**
	 * Determines whether all inner blocks have already selected their access path
	 */
	public boolean
	accessPathSelected()
	{
		return true;
	}

	/**
	 * Return total number of possibilities that we will be stepping over via `next'
	 *
	 * @param var_info Information about variable bindings
	 * @param env Environment with concrete bindings
	 *
	 * @return number of possibilities
	 */
	public abstract double
	getSize(VarInfo var_info, Env env);

	/**
	 * Likelihood that one binding attempt for this join will be successful
	 *
	 * @param var_info Information about variable bindings
	 * @param env Environment with concrete bindings
	 *
	 * @return selectivity
	 */
	public abstract double
	getSelectivity(VarInfo var_info, Env env);

	public static final double INEQUALITY_SELECTIVITY	= 0.5;
	public static final double INT_JOIN_SELECTIVITY		= 0.01;
	public static final double CHAR_JOIN_SELECTIVITY	= 1.0 / 65536.0;
	public static final double SHORT_JOIN_SELECTIVITY	= 1.0 / 65536.0;
	public static final double BYTE_JOIN_SELECTIVITY	= 1.0 / 256.0;
	public static final double BOOLEAN_JOIN_SELECTIVITY	= 0.5;
	public static final double LONG_JOIN_SELECTIVITY	= 0.001;
	public static final double OBJECT_JOIN_SELECTIVITY	= 0.001;
	public static final double ELEMENT_JOIN_SELECTIVITY	= OBJECT_JOIN_SELECTIVITY;
	public static final double FLOAT_JOIN_SELECTIVITY	= 0.0001;
	public static final double DEFAULT_SET_SIZE		= 1000.0;
	public static final double INSTANCE_OF_SELECTIVITY	= 0.1;

	/**
	 * Estimated number of uncached memory accesses needed to produce one binding of this join
	 *
	 * @param var_info Information about variable bindings
	 * @param env Environment with concrete bindings
	 *
	 * @return estimated number of memory accesses
	 */
	public abstract double
	getAccessCost(VarInfo var_info, Env env);

	public final double
	getFullCost(VarInfo var_info, Env env)
	{
		return this.getSize(var_info, env) * this.getSelectivity(var_info, env) * (1 + this.getAccessCost(var_info, env));
	}

	/**
	 * Name of the PQLFactory constructor to call for this
	 */
	public String
	getName()
	{
		return this.getClass().getSimpleName();
	}

	public String
	getID()
	{
		return super.toString();
	}

	private Object decoration;

	public void
	setDecoration(Object obj)
	{
		this.decoration = obj;
	}

	public Object
	getDecoration()
	{
		return this.decoration;
	}

	@Override
	public String
	toString()
	{
		return toStringDefault();
	}

	protected String
	toStringDefault()
	{
		StringBuffer buf = new StringBuffer(this.getName());
		buf.append("(");
		boolean first_arg = true;
		for (int i = 0; i < this.getArgsNr(); i++) {
			int arg = this.getArg(i);
			if (first_arg)
				first_arg = false;
			else
				buf.append((i == this.getReadArgsNr()) ? "; " : ", ");
			buf.append(Env.showVar(arg));
		}
		buf.append(")");
		return buf.toString();
	}

	/**
	 * Compute selectivity by from user-supplied heuristics based on whether the read/write flag is set on a particular parameter
	 *
	 * @param var The variable to test for whether it is a read- or a write-variable
	 */
	protected double
	getSelectivityFromReadFlag(int var, double if_write, double if_read)
	{

		if (Env.isReadVar(this.getArg(var)))
			return if_read;
		else
			return if_write;
	}

	public static abstract class BI0 extends Join {
		public BI0()
		{
		}

		public int
		getArgsNr()
		{
			return 0;
		}

		public int
		getReadArgsNr()
		{
			return 0;
		}

		public void
		setArg(int i, int v)
		{
			throw new RuntimeException();
		}

		public int
		getArg(int i)
		{
			throw new RuntimeException();
		}

		public Join
		copyRecursively()
		{
			return this.copy();
		}

		@Override public String
		getConstructorName()
		{
			return this.getName();
		}
	}

	public static abstract class BI2 extends Join {
		protected int v0, v1;

		/**
		 * Constructs a new comparison
		 */
		public BI2(int v0, int v1)
		{
			this.v0 = v0;
			this.v1 = v1;
		}

		public int
		getArgsNr()
		{
			return 2;
		}

		public int
		getReadArgsNr()
		{
			return 2;
		}

		public void
		setArg(int i, int v)
		{
			if (i == 0)
				v0 = v;
			else if (i == 1)
				v1 = v;
			else throw new RuntimeException("Invalid index: " + i);
		}

		public Join
		copyRecursively()
		{
			return this.copy();
		}

		public int
		getArg(int i)
		{
			if (i == 0)
				return v0;
			else if (i == 1)
				return v1;
			else throw new RuntimeException("Invalid index: " + i);
		}

		@Override public String
		getConstructorName()
		{
			return this.getName();
		}
	}


	public static abstract class BI3 extends Join {
		protected int v0, v1, v2;

		public BI3(int v0, int v1, int v2)
		{
			this.v0 = v0;
			this.v1 = v1;
			this.v2 = v2;
		}

		public int
		getArgsNr()
		{
			return 3;
		}

		public int
		getReadArgsNr()
		{
			return 3;
		}

		public Join
		copyRecursively()
		{
			return this.copy();
		}

		public void
		setArg(int i, int v)
		{
			if (i == 0)
				v0 = v;
			else if (i == 1)
				v1 = v;
			else if (i == 2)
				v2 = v;
			else throw new RuntimeException("Invalid index: " + i);
		}

		public int
		getArg(int i)
		{
			if (i == 0)
				return v0;
			else if (i == 1)
				return v1;
			else if (i == 2)
				return v2;
			else throw new RuntimeException("Invalid index: " + i);
		}

		@Override public String
		getConstructorName()
		{
			return this.getName();
		}
	}
}
