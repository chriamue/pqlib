/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import org.junit.*;
import static org.junit.Assert.*;

public class ComparisonTest extends TestBase
{
	@Test
	public void
	testAPI()
	{
		Join p = PQLFactory.EQ_Int(i0w, i1r);
		assertEquals(2, p.getArgsNr());
		assertEquals(i0w, p.getArg(0));
		assertEquals(i1r, p.getArg(1));

		p.setArg(0, l1w);
		p.setArg(1, l0r);

		assertEquals(l1w, p.getArg(0));
		assertEquals(l0r, p.getArg(1));
	}

	@Test
	public void
	testEQ_Int()
	{
		env.setInt(i0w, 0);
		env.setInt(i1w, 0);
		env.setInt(i2w, 1);

		env.setInt(o0w, 1);
		checkTrue(PQLFactory.EQ_Int(i2w, o0r));

		checkTrue(PQLFactory.EQ_Int(i0w, i1r));
		checkFalse(PQLFactory.EQ_Int(i0w, i2r));
		assertEquals(1, env.getInt(i2r));
		checkTrue(PQLFactory.EQ_Int(i0w, i2w));
		assertEquals(0, env.getInt(i2r));

		env.setDouble(d0w, 0.2);
		env.setDouble(d1w, 1.2);

		checkTrue(PQLFactory.EQ_Int(i0r, d0r));
		assertEquals(0.2, env.getDouble(d0r), 0.0);
		checkFalse(PQLFactory.EQ_Int(i0r, d1r));
		assertEquals(1.2, env.getDouble(d1r), 0.0);
		checkTrue(PQLFactory.EQ_Int(i0r, d1w));
		assertEquals(0.0, env.getDouble(d1r), 0.0);

		final Object o = new Integer(0);
		env.setObject(o0w, o);
		checkTrue(PQLFactory.EQ_Int(i0w, o0r));
		checkTrue(PQLFactory.EQ_Int(o0w, i0r));
	}

	@Test
	public void
	testEQ_Long()
	{
		env.setLong(l0w, 0l);
		env.setLong(l1w, 0l);
		env.setLong(l2w, 1l);

		env.setInt(o0w, 1);
		checkTrue(PQLFactory.EQ_Long(l2w, o0r));

		checkTrue(PQLFactory.EQ_Long(l0w, l1r));
		checkFalse(PQLFactory.EQ_Long(l0w, l2r));
		assertEquals(1l, env.getLong(l2r));
		checkTrue(PQLFactory.EQ_Long(l0w, l2w));
		assertEquals(0l, env.getLong(l2r));

		env.setDouble(d0w, 0.2);
		env.setDouble(d1w, 1.2);

		checkTrue(PQLFactory.EQ_Long(l0r, d0r));
		assertEquals(0.2, env.getDouble(d0r), 0.0);
		checkFalse(PQLFactory.EQ_Long(l0r, d1r));
		assertEquals(1.2, env.getDouble(d1r), 0.0);
		checkTrue(PQLFactory.EQ_Long(l0r, d1w));
		assertEquals(0.0, env.getDouble(d1r), 0.0);
	}

	@Test
	public void
	testEQ_Double()
	{
		env.setDouble(d0w, 0.0);
		env.setDouble(d1w, 0.0);
		env.setDouble(d2w, 1.0);

		env.setInt(o0w, 1);
		checkTrue(PQLFactory.EQ_Double(d2w, o0r));

		checkTrue(PQLFactory.EQ_Double(d0w, d1r));
		checkFalse(PQLFactory.EQ_Double(d0w, d2r));
		assertEquals(1.0, env.getInt(d2r), 0.0);
		checkTrue(PQLFactory.EQ_Double(d0w, d2w));
		assertEquals(0.0, env.getInt(d2r), 0.0);
	}

	@Test
	public void
	testEQ_Object()
	{
		env.setInt(i0w, 0);
		final Object o = new Integer(0);
		env.setObject(o0w, o);
		env.setObject(o1w, o);
		env.setObject(o2w, new Integer(0));

		checkTrue(PQLFactory.EQ_Object(o0w, o1r));
		checkFalse(PQLFactory.EQ_Object(o0w, o2r));

		env.setInt(i1w, 1);
		checkFalse(PQLFactory.EQ_Object(o2w, i1r));
		assertEquals(1, env.getInt(i1r));
		checkTrue(PQLFactory.EQ_Object(o2w, i1w));
		assertEquals(0, env.getInt(i1r));
		assertEquals(0, env.getInt(o2r));
	}

	@Test
	public void
	testNEQ_Int()
	{
		env.setInt(i0w, 0);
		env.setInt(i1w, 0);
		env.setInt(i2w, 1);

		checkFalse(PQLFactory.NEQ_Int(i0w, i1w));
		checkTrue(PQLFactory.NEQ_Int(i0w, i2w));

		assertEquals(0, env.getInt(i0w));
		assertEquals(0, env.getInt(i1w));
		assertEquals(1, env.getInt(i2w));
	}

	@Test
	public void
	testNEQ_Long()
	{
		env.setLong(l0w, 0l);
		env.setLong(l1w, 0l);
		env.setLong(l2w, 1l);

		checkFalse(PQLFactory.NEQ_Long(l0w, l1w));
		checkTrue(PQLFactory.NEQ_Long(l0w, l2w));

		assertEquals(0l, env.getLong(l0w));
		assertEquals(0l, env.getLong(l1w));
		assertEquals(1l, env.getLong(l2w));
	}

	@Test
	public void
	testNEQ_Double()
	{
		env.setDouble(d0w, 0.0);
		env.setDouble(d1w, 0.0);
		env.setDouble(d2w, 1.0);

		checkFalse(PQLFactory.NEQ_Double(d0w, d1w));
		checkTrue(PQLFactory.NEQ_Double(d0w, d2w));

		assertEquals(0.0, env.getDouble(d0w), 0.0);
		assertEquals(0.0, env.getDouble(d1w), 0.0);
		assertEquals(1.0, env.getDouble(d2w), 0.0);
	}

	@Test
	public void
	testNEQ_Object()
	{
		String s = "foo";
		env.setObject(o0w, null);
		env.setObject(o1w, null);
		env.setObject(o2w, s);

		checkFalse(PQLFactory.NEQ_Object(o0w, o1w));
		checkTrue(PQLFactory.NEQ_Object(o0w, o2w));

		assertSame(null, env.getObject(o0w));
		assertSame(null, env.getObject(o1w));
		assertSame(s, env.getObject(o2w));
	}

	@Test
	public void
	testLT_Int()
	{
		env.setInt(i0w, 0);
		env.setInt(i1w, 1);
		env.setInt(i2w, 2);

		checkTrue(PQLFactory.LT_Int(i0w, i1w));
		assertEquals(0, env.getInt(i0w));
		assertEquals(1, env.getInt(i1w));

		checkFalse(PQLFactory.LT_Int(i2w, i1w));
		env.setInt(i0w, 1);
		checkFalse(PQLFactory.LT_Int(i0w, i1w));
	}

	@Test
	public void
	testLTE_Int()
	{
		env.setInt(i0w, 0);
		env.setInt(i1w, 1);
		env.setInt(i2w, 2);

		checkTrue(PQLFactory.LTE_Int(i0w, i1w));
		assertEquals(0, env.getInt(i0w));
		assertEquals(1, env.getInt(i1w));

		checkFalse(PQLFactory.LTE_Int(i2w, i1w));
		env.setInt(i0w, 1);
		checkTrue(PQLFactory.LTE_Int(i0w, i1w));
	}

	@Test
	public void
	testLT_Long()
	{
		long l1 = 1125895611875329l;
		env.setLong(l0w, 0);
		env.setLong(l1w, l1);
		env.setLong(l2w, 1125899906842624l); // more than the above, but less than the above if we truncate to 32 bits

		checkTrue(PQLFactory.LT_Long(l0w, l1w));
		assertEquals(0l, env.getLong(l0w));
		assertEquals(l1, env.getLong(l1w));

		checkFalse(PQLFactory.LT_Long(l2w, l1w));
		env.setLong(l0w, l1);
		checkFalse(PQLFactory.LT_Long(l0w, l1w));
	}

	@Test
	public void
	testLTE_Long()
	{
		long l1 = 1125895611875329l;
		env.setLong(l0w, 0);
		env.setLong(l1w, l1);
		env.setLong(l2w, 1125899906842624l); // more than the above, but less than the above if we truncate to 32 bits

		checkTrue(PQLFactory.LTE_Long(l0w, l1w));
		assertEquals(0l, env.getLong(l0w));
		assertEquals(l1, env.getLong(l1w));

		checkFalse(PQLFactory.LTE_Long(l2w, l1w));
		env.setLong(l0w, l1);
		checkTrue(PQLFactory.LTE_Long(l0w, l1w));
	}

	@Test
	public void
	testLT_Double()
	{
		double l1 = 0.00001;
		env.setDouble(d0w, 0);
		env.setDouble(d1w, l1);
		env.setDouble(d2w, 0.00002);

		checkTrue(PQLFactory.LT_Double(d0w, d1w));
		assertEquals(0, env.getDouble(d0w), 0.0);
		assertEquals(l1, env.getDouble(d1w), 0.0);

		checkFalse(PQLFactory.LT_Double(d2w, d1w));
		env.setDouble(d0w, l1);
		checkFalse(PQLFactory.LT_Double(d0w, d1w));
	}

	@Test
	public void
	testLTE_Double()
	{
		double l1 = 0.00001;
		env.setDouble(d0w, 0);
		env.setDouble(d1w, l1);
		env.setDouble(d2w, 0.00002);

		checkTrue(PQLFactory.LTE_Double(d0w, d1w));
		assertEquals(0.0, env.getDouble(d0w), 0.0);
		assertEquals(l1, env.getDouble(d1w), 0.0);

		checkFalse(PQLFactory.LTE_Double(d2w, d1w));
		env.setDouble(d0w, l1);
		checkTrue(PQLFactory.LTE_Double(d0w, d1w));
	}

	@Test
	public void
	testEQ_String()
	{
		setObjects(new String("foo"),
			   new String("foo"),
			   new String("bar"));

		checkTrue(PQLFactory.EQ_String(o0w, o1r));
		checkFalse(PQLFactory.EQ_String(o0w, o2r));
	}


	@Test
	public void
	testNEQ_String()
	{
		setObjects(new String("foo"),
			   new String("foo"),
			   new String("bar"));

		checkFalse(PQLFactory.NEQ_String(o0w, o1r));
		checkTrue(PQLFactory.NEQ_String(o0w, o2r));
	}

}
