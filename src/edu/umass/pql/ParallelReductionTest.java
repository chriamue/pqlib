/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.util.*;
import edu.umass.pql.container.*;
import org.junit.*;
import static org.junit.Assert.*;

public class ParallelReductionTest extends TestBase
{
	static java.lang.reflect.Method sumInt = ReductionTest.getSumIntMethod();

	@Test public void testForallFalse() { paratest("para_testForallFalse"); }

	public void
	para_testForallFalse()
	{
		setInts(0, 1, 1000, 777);
		Reduction red = PQLFactory.Forall(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.NEQ_Int(i0r, i3r),
					       i4r, i1r);

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertFalse(red.next(env));
	}

	@Test public void testForallTrue() { paratest("para_testForallTrue"); }

	public void
	para_testForallTrue()
	{
		setInts(0, 1, 1000, 7777777);
		Reduction red = PQLFactory.Forall(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.NEQ_Int(i0r, i3r),
					       i4r, i1r);

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
	}

	@Test public void testExistsTrue() { paratest("para_testExistsTrue"); }

	public void
	para_testExistsTrue()
	{
		setInts(0, 1, 1000, 777);
		Reduction red = PQLFactory.Exists(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.EQ_Int(i0r, i3r),
					       i4r, i1r);

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
	}

	@Test public void testExistsTrueStart() { paratest("para_testExistsTrueStart"); }

	public void
	para_testExistsTrueStart()
	{
		setInts(0, 1, 1000, 777);
		Reduction red = PQLFactory.Exists(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.EQ_Int(i0r, i1r),
					       i4r, i1r);

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
	}

	@Test public void testExistsTrueEnd() { paratest("para_testExistsTrueEnd"); }

	public void
	para_testExistsTrueEnd()
	{
		setInts(0, 1, 1000, 777);
		Reduction red = PQLFactory.Exists(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.EQ_Int(i0r, i2r),
					       i4r, i1r);

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
	}

	@Test public void testExistsFalse() { paratest("para_testExistsFalse"); }

	public void
	para_testExistsFalse()
	{
		setInts(0, 1, 1000, 7777777);
		Reduction red = PQLFactory.Exists(PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
					       PQLFactory.EQ_Int(i0r, i3r),
					       i4r, i1r);

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertFalse(red.next(env));
	}

	// -- manifest

	final int size = 5000000;

	@Test public void testForallManifestFalse() { paratest("para_testForallManifestFalse"); }

	public void
	para_testForallManifestFalse()
	{
		setLongs(2l, 2l, 2l);
		setInts(0, 1, size, (size / Env.THREADS_NR) - 1);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i4r, l0w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.Bool(PQLFactory.NEQ_Int(i0r, i3r), i4w)
						  );

		//		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(0, this.env.getInt(l0r));
	}

	@Test public void testForallManifestTrue() { paratest("para_testForallManifestTrue"); }

	public void
	para_testForallManifestTrue()
	{
		setLongs(2l, 2l, 2l);
		setInts(0, 1, size, 7777777);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i4r, l0w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.Bool(PQLFactory.NEQ_Int(i0r, i3r), i4w)
						  );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(1, this.env.getInt(l0r));
	}


	@Test public void testExistsManifestTrue() { paratest("para_testExistsManifestTrue"); }

	public void
	para_testExistsManifestTrue()
	{
		setLongs(2l, 2l, 2l);
		setInts(0, 1, size, 777);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.EXISTS(i4r, l0w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.Bool(PQLFactory.EQ_Int(i0r, i3r), i4w)
						  );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(1, this.env.getInt(l0r));
	}

	@Test public void testExistsManifestFalse() { paratest("para_testExistsManifestFalse"); }

	public void
	para_testExistsManifestFalse()
	{
		setLongs(2l, 2l, 2l);
		setInts(0, 1, size, 7777777);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.EXISTS(i4r, l0w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.Bool(PQLFactory.EQ_Int(i0r, i3r), i4w)
						  );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(0, this.env.getInt(l0r));
	}

	// ================================================================================

	@DefaultValueInt(-1)
	public static int
	maxInt(int l, int r)
	{
		return l > r ? l : r;
	}

	static java.lang.reflect.Method maxInt;
        {
                try {
                        maxInt = ParallelReductionTest.class.getDeclaredMethod("maxInt", int.class, int.class);
                } catch (Exception e) {
                        throw new RuntimeException(e);
                }
        }


	@Test public void testMethodAdapter() { paratest("para_testMethodAdapter"); }

	public void
	para_testMethodAdapter()
	{
		final int arraysize = 1000;
		int[] array = new int[arraysize];
		for (int i = 0; i < arraysize; i++)
			array[i] = i % 12;
		array[(arraysize >> 1) + 17] = 23;
		setObjects(array, null, null);
		setInts(0, 0, arraysize - 1, 7777);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(maxInt, i3r, i4w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.ARRAY_LOOKUP_Int(o0r, i0r, i3w)
						  );

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(23, this.env.getInt(i4r));
	}

	@Test public void testSetElements() { paratest("para_testSetElements"); }

	/*
	 * forall x: set.contains(x) && x > 0
	 *
	 * Reduce[FORALL(?i3): !i2]: { CONTAINS(?o0; !i1); Bool(!i3): LT_Int(?i0, ?i1); };;
	 */
	public void
	para_testSetElements()
	{
		final Set<Integer> set = new HashSet<Integer>();
		set.add(1);
		set.add(3);
		set.add(5);
		set.add(7);
		set.add(9);
		set.add(22);

		setObjects(set, null, null);
		setInts(0, 0, 0);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i3r, i2w),
						     PQLFactory.ConjunctiveBlock(PQLFactory.CONTAINS(o0r, i1w),
										 PQLFactory.Bool(PQLFactory.LT_Int(i0r, i1r), i3w))
						     );
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(1, env.getInt(i2r));
	}

	// ================================================================================

	@Test public void testSetGen() { paratest("para_testSetGen"); }

	public void
	para_testSetGen()
	{
		final int arraysize = 1000;
		final int modsize = 12;
		int[] array = new int[arraysize];
		for (int i = 0; i < arraysize; i++)
			array[i] = i % modsize;
		setObjects(array, null, null);
		setInts(0, 0, arraysize - 1, 7777);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.SET(i3r, o0w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.ARRAY_LOOKUP_Int(o0r, i0r, i3w)
						  );

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		Set<?> set = (Set<?>) this.env.getObject(o0r);
		assertEquals(modsize, set.size());
		for (int i = 0; i < modsize; i++)
			assertTrue("set element " + i, set.contains(i));
	}

	@Test public void testMapGen() { paratest("para_testMapGen"); }

	public void
	para_testMapGen()
	{
		final int arraysize = 1000;
		final int modsize = 12;
		int[] array = new int[arraysize];
		for (int i = 0; i < arraysize; i++)
			array[i] = i % modsize;
		setObjects(array, null, null);
		setInts(0, 0, arraysize - 1, 7777);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.MAP(i0r, i3r, o0w),
						  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w),
						  PQLFactory.ARRAY_LOOKUP_Int(o0r, i0r, i3w)
						  );

		assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		AMap<?, ?> map = (AMap<?, ?>) this.env.getObject(o0r);
		assertNotNull(map);
		assertEquals(arraysize, map.size());
		for (int i = 0; i < arraysize; i++) {
			assertTrue("map element " + i, map.get(i).equals(i % modsize));
		}
	}

	// ================================================================================
	// nested reductors

	@Test public void testNestedMapSum() { paratest("para_testNestedMapSum"); }


	public void
	para_testNestedMapSum()
	{
		int[] data = new int[] {
			1, 3, 5, 7, 9,
			11, 13, 17,
			22, 25
		};

		final Map<Integer, Integer> expected = new HashMap<Integer, Integer>();
		for (int i = 0; i < data.length; i++) {
			Integer Icount = expected.get(data[i] / 10);
			int count = Icount == null ? 0 : Icount.intValue();
			++count;
			expected.put(data[i] / 10, count);
		}

		setObjects(data, null, null);
		setInts(0, 1, 0, 0, 10);

		Reductor sum = PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i1r, i0w);
		Reductor map = PQLFactory.Reductors.MAP(i2r, Reductor.INPUT_FROM_INNER_REDUCTOR, o1w);
		map.setInnerReductor(sum);
		Join join = PQLFactory.Reduction(map,
					      PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i0w),
					      PQLFactory.DIV_Int(i0r, i4r, i2w)
					      );

		env.setObject(o0w, data);
		join.reset(env);
		join.next(env);

		assertEquals(expected, env.getObject(o1r));
	}

	// // --

	@Test public void testNestedMapSet() { paratest("para_testNestedMapSet"); }

	public static Join // source: o0r, dest: o1w
	pql_comp_computeWordToContainingDocumentIDs()
	{
		Reductor set = PQLFactory.Reductors.SET(i0r, __);
		Reductor map = PQLFactory.Reductors.MAP(o3r, Reductor.INPUT_FROM_INNER_REDUCTOR, o1w);
		map.setInnerReductor(set);
		return PQLFactory.Reduction(map,
					 PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o2w),
					 PQLFactory.ARRAY_LOOKUP_Object(o2r, __, o3w));
					 
	}

	@SuppressWarnings("unchecked")
	public Map<String, Set<Integer>>
	pql_computeWordToContainingDocumentIDs(String[][] documents)
	{
		Join join = pql_comp_computeWordToContainingDocumentIDs();
		setObjects(documents, null, null);
		join.reset(env);
		join.next(env);
		return (Map<String, Set<Integer>>) env.getObject(o1r);
	}

	public void
	para_testNestedMapSet()
	{
		String[][] documents = generateDocuments(4, 256);
		assertEquals(computeWordToContainingDocumentIDs(documents),
			     pql_computeWordToContainingDocumentIDs(documents));
	}

	// --

	@Test public void testNestedMapMapSum() { paratest("para_testNestedMapMapSum"); }

	public static Join // source: o0r, dest: o1w, i4r: 1
	pql_comp_computeWordToDocumentToFrequency()
	{
		Reductor sum = PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i4r, i3w);
		Reductor inner_map = PQLFactory.Reductors.MAP(i1r, Reductor.INPUT_FROM_INNER_REDUCTOR, o4w);
		inner_map.setInnerReductor(sum);
		Reductor outer_map = PQLFactory.Reductors.MAP(o3r, Reductor.INPUT_FROM_INNER_REDUCTOR, o1w);
		outer_map.setInnerReductor(inner_map);
		Reduction red = PQLFactory.Reduction(new Reductor[] {
								   outer_map },
						  PQLFactory.ARRAY_LOOKUP_Object(o0r, i1w, o2w),
						  PQLFactory.ARRAY_LOOKUP_Object(o2r, __, o3w));

		return red;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Map<Integer, Integer>>
	pql_computeWordToDocumentToFrequency(String[][] documents)
	{
		setInts(0, 0, 0, 0, 1);
		Join join = pql_comp_computeWordToDocumentToFrequency();
		setObjects(documents, null, null);
		join.reset(env);
		join.next(env);
		return (Map<String, Map<Integer, Integer>>) env.getObject(o1r);
	}

	public void
	para_testNestedMapMapSum()
	{
		String[][] documents = generateDocuments(16, 64);
		assertEquals(computeWordToDocumentToFrequency(documents),
			     pql_computeWordToDocumentToFrequency(documents));
	}

	public static String[][]
	generateDocuments(final int words_nr, final int documents_nr)
	{
		String[] words = new String[words_nr];
		char[] data = new char[1];
		data[0] = 'A';
		for (int i = 0; i < words_nr; i++) {
			words[i] = new String(data);

			int j = 0;
			while (++data[j] > 'Z') {
				data[j] = 'A';
				if (++j >= data.length) {
					data = new char[data.length + 1];
					for (j = 0; j < data.length; j++)
						data[j] = 'A';
					break;
				}
			}
		}

		String[][] documents = new String[documents_nr][];

		Random rand = new Random(42);

		for (int i = 0; i < documents_nr; i++) {
			final String[] doc = new String[(words_nr >> 5) + 1 + rand.nextInt((words_nr >> 2) + 1)];
			for (int j = 0; j < doc.length; j++) {
				doc[j] = words[rand.nextInt(1 + rand.nextInt(words_nr))];
			}
			documents[i] = doc;
		}

		return documents;
	}

	public static Map<String, Set<Integer>>
	computeWordToContainingDocumentIDs(String[][] documents)
	{
		final HashMap<String, Set<Integer>> retval = new HashMap<String, Set<Integer>>();

		for (int doc_nr = 0; doc_nr < documents.length; doc_nr++) {
			final String[] doc = documents[doc_nr];
			for (String word : doc) {
				Set<Integer> count_set = retval.get(word);
				if (count_set == null) {
					count_set = new HashSet<Integer>();
					retval.put(word, count_set);
				}
				count_set.add(doc_nr);
			}
		}

		return retval;
	}

	public static Map<String, Map<Integer, Integer>>
	computeWordToDocumentToFrequency(String[][] documents)
	{
		final HashMap<String, Map<Integer, Integer>> retval = new HashMap<String, Map<Integer, Integer>>();

		for (int doc_nr = 0; doc_nr < documents.length; doc_nr++) {
			final String[] doc = documents[doc_nr];
			for (String word : doc) {
				Map<Integer, Integer> in_doc_freq = retval.get(word);
				if (in_doc_freq == null) {
					in_doc_freq = new HashMap<Integer, Integer>();
					retval.put(word, in_doc_freq);
				}
				if (in_doc_freq.containsKey(doc_nr))
					in_doc_freq.put(doc_nr, 1 + in_doc_freq.get(doc_nr));
				else
					in_doc_freq.put(doc_nr, 1);
			}
		}

		return retval;
	}

	// ----------------------------------------------------------------------------------------------------
	// ================================================================================

	@DefaultValueInt(0)
	public static int
	addInt(int a, int b)
	{
		return a + b;
	}

	@Test public void testLongIntReduction() { paratest("para_testLongIntReduction"); }

	public void
	para_testLongIntReduction()
	{
		long[] a = new long[] { 1, 1, 1, 2, 1 };
		
		setObjects(a, null, null);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(ParallelReductionTest.class,
											 "addInt", int.class,
											 l0r, i1w),
						     PQLFactory.ARRAY_LOOKUP_Long(o0r, l1w, l0w)
						     );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(6, this.env.getInt(i1r));
	}

	// ================================================================================

	@DefaultValueShort(0)
	public static short
	addShort(short a, short b)
	{
		return (short) (((int)a)+((int)b));
	}

	@Test public void testShortReduction() { paratest("para_testShortReduction"); }

	public void
	para_testShortReduction()
	{
		short[] a = new short[] { 1, 1, 1, 2, 1 };
		
		setObjects(a, null, null);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(ParallelReductionTest.class,
											 "addShort", short.class,
											 i0r, i1w),
						     PQLFactory.ARRAY_LOOKUP_Short(o0r, l0w, i0w)
						     );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(6, this.env.getInt(i1r));
	}

	// --------------------------------------------------------------------------------

	@DefaultValueByte(0)
	public static byte
	addByte(byte a, byte b)
	{
		return (byte) (((int)a)+((int)b));
	}

	@Test public void testByteReduction() { paratest("para_testByteReduction"); }

	public void
	para_testByteReduction()
	{
		byte[] a = new byte[] { 1, 1, 1, 2, 1 };
		
		setObjects(a, null, null);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(ParallelReductionTest.class,
											 "addByte", byte.class,
											 i0r, i1w),
						     PQLFactory.ARRAY_LOOKUP_Byte(o0r, l0w, i0w)
						     );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(6, this.env.getInt(i1r));
	}

	// --------------------------------------------------------------------------------

	@DefaultValueFloat(0.0f)
	public static float
	addFloat(float a, float b)
	{
		return a+b;
	}

	@Test public void testFloatReduction() { paratest("para_testFloatReduction"); }

	public void
	para_testFloatReduction()
	{
		float[] a = new float[] { 1.1f, 1.1f, 1.1f, 2.1f, 1.1f };
		
		setObjects(a, null, null);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(ParallelReductionTest.class,
											 "addFloat", float.class,
											 d0r, d1w),
						     PQLFactory.ARRAY_LOOKUP_Float(o0r, l0w, d0w)
						     );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(6.5, this.env.getDouble(d1r), 0.1);
	}

	// --------------------------------------------------------------------------------

	@DefaultValueChar('\00')
	public static char
	maxChar(char a, char b)
	{
		return (a > b) ? a : b;
	}

	@Test public void testCharReduction() { paratest("para_testCharReduction"); }

	public void
	para_testCharReduction()
	{
		char[] a = new char[] { 'A', 'a', 'B', '0' };
		
		setObjects(a, null, null);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(ParallelReductionTest.class,
											 "maxChar", char.class,
											 i0r, i1w),
						     PQLFactory.ARRAY_LOOKUP_Char(o0r, l0w, i0w)
						     );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals('a', Character.toChars(this.env.getInt(i1r))[0]);
	}

	// --------------------------------------------------------------------------------

	@DefaultValueBoolean(true)
	public static boolean
	booleanAnd(boolean a, boolean b)
	{
		return a && b;
	}

	@Test public void testBooleanReduction() { paratest("para_testBooleanReduction"); }

	public void
	para_testBooleanReduction()
	{
		boolean[] a = new boolean[] { true, true, true };
		
		setObjects(a, null, null);
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(ParallelReductionTest.class,
											 "booleanAnd", boolean.class,
											 i0r, i1w),
						     PQLFactory.ARRAY_LOOKUP_Boolean(o0r, l0w, i0w)
						     );

		//assertTrue(!PQLFactory.parallelismPermitted() || red instanceof edu.umass.pql.il.ReductionImpl.Parallel);
		red.reset(this.env);
		assertTrue(red.next(env));
		assertEquals(1, this.env.getInt(i1r));
	}

}