/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class DependencyMapTest extends TestBase
{
	private static final class K
	{
		public int key;
		public VarSet vars;

		public K(int var, int... args)
		{
			this.key = var;
			this.vars = new VarSet(args);
		}
	}

	public void
	check(Join j, K... mappings)
	{
		Map<Integer, VarSet> depmap = Optimizer.bindingDependencyMap(j);

		if (mappings.length != depmap.keySet().size())
			System.err.println("depmap = " + depmap);
		assertEquals(mappings.length, depmap.keySet().size());

		for (K ks : mappings) {
			assertTrue(Env.isReadVar(ks.key));
			assertNotNull(depmap.get(ks.key));
			assertEquals(ks.vars, depmap.get(ks.key));
		}
	}

	// ================================================================================

	@Test
	public void
	testTrivial()
	{
		check(LT_Int(i0r, i1r), new K[0]);
	}

	@Test
	public void
	testOne1()
	{
		check(EQ_Int(i0r, i1r),
		      new K(i1r, i0r)
		      );
	}

	@Test
	public void
	testOne2()
	{
		check(ARRAY_LOOKUP_Int(o0r, i1r, i2r),
		      new K(i1r, o0r),
		      new K(i2r, o0r)
		      );
	}


	@Test
	public void
	testSequence()
	{
		check(ConjunctiveBlock(ARRAY_LOOKUP_Object(o1r, __, o0r),
				       ARRAY_LOOKUP_Int(o0r, i1r, i2r),
				       ADD_Int(i1r, i2r, i3r)),
		      new K(o0r, o1r),
		      new K(i1r, o0r, o1r),
		      new K(i2r, o0r, o1r),
		      new K(i3r, o0r, i2r, i1r, o0r, o1r)
		      );
	}

	@Test
	public void
	testDisjunctionA()
	{
		check(ConjunctiveBlock(ARRAY_LOOKUP_Object(o1r, __, o0r),
				       DisjunctiveBlock(ARRAY_LOOKUP_Int(o0r, __, i2r),
							OBJECT_ARRAY_SIZE(o1r, i2r)),
				       ADD_Int(i2r, i2r, i3r)),

		      new K(o0r, o1r),
		      new K(i2r, o0r, o1r),
		      new K(i3r, o0r, i2r, o0r, o1r)
		      );
	}

	@Test
	public void
	testDisjunctionB()
	{
		check(ConjunctiveBlock(ARRAY_LOOKUP_Object(o1r, __, o0r),
				       DisjunctiveBlock(OBJECT_ARRAY_SIZE(o1r, i2r),
							ARRAY_LOOKUP_Int(o0r, __, i2r)),
				       ADD_Int(i2r, i2r, i3r)),

		      new K(o0r, o1r),
		      new K(i2r, o0r, o1r),
		      new K(i3r, o0r, i2r, o0r, o1r)
		      );
	}

	@Test
	public void
	testNot()
	{
		check(Not(ConjunctiveBlock(ARRAY_LOOKUP_Object(o1r, __, o0r),
					   DisjunctiveBlock(OBJECT_ARRAY_SIZE(o1r, i2r),
							    ARRAY_LOOKUP_Int(o0r, __, i2r)),
					   ADD_Int(i2r, i2r, i3r))),

		      new K(o0r, o1r),
		      new K(i2r, o0r, o1r),
		      new K(i3r, o0r, i2r, o0r, o1r)
		      );
	}

	@Test
	public void
	testBool()
	{
		check(ConjunctiveBlock(Bool(ConjunctiveBlock(ARRAY_LOOKUP_Object(o1r, __, o0r),
							     DisjunctiveBlock(OBJECT_ARRAY_SIZE(o1r, i2r),
									      ARRAY_LOOKUP_Int(o0r, __, i2r)),
							     ADD_Int(i2r, i2r, i3r)), l2r),
				       EQ_Int(l2r, l3r)
				       ),

		      new K(o0r, o1r),
		      new K(i2r, o0r, o1r),
		      new K(i3r, o0r, i2r, o0r, o1r),
		      new K(l3r, l2r) // or should we depend on everything in the block?  Hmm.  For our current purposes this seems to work
		      );
	}

	@Test
	public void
	testNoFail()
	{
		check(NoFail(ConjunctiveBlock(ARRAY_LOOKUP_Object(o1r, __, o0r),
					      DisjunctiveBlock(OBJECT_ARRAY_SIZE(o1r, i2r),
							       ARRAY_LOOKUP_Int(o0r, __, i2r)),
					      ADD_Int(i2r, i2r, i3r))),

		      new K(o0r, o1r),
		      new K(i2r, o0r, o1r),
		      new K(i3r, o0r, i2r, o0r, o1r)
		      );
	}

	@Test
	public void
	testReduction()
	{
		paratest("proto_testReduction");
	}

	public void
	proto_testReduction()
	{
		check(Reduction(PQLFactory.Reductors.SET(i0r, o0w),
				INT_RANGE_CONTAINS(i1r, i2r, i0r)),
		      new K(i0r, i1r, i2r),
		      new K(o0r, i0r, i1r, i2r)
		      );
	}
}