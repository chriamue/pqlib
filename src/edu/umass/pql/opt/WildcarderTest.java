/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class WildcarderTest extends TestBase
{
	public void
	check(Join j, VarSet set, Join other)
	{
		Optimizer.wildcard(j, set);
		assertEquals(other.toString(), j.toString());
	}

	// ================================================================================

	@Test
	public void
	testTrivial()
	{
		check(
		      LT_Int(i0r, i1r)
		      ,new VarSet(),
		      LT_Int(i0r, i1r)
		      );
	}

	@Test
	public void
	testSimple()
	{
		check(
		      EQ_Int(i0r, i1r)
		      ,new VarSet(),
		      EQ_Int(i0r, __)
		      );
	}

	// conjunction
	@Test
	public void
	testConjunction0()
	{
		check(
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, i3r)
				       )
		      ,new VarSet(i0r),
		      ConjunctiveBlock(
				       EQ_Int(i0r, __),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, __)
				       )
		      );
	}

	@Test
	public void
	testConjunction1()
	{
		check(
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, i3r)
				       )
		      ,new VarSet(i1r),
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, __)
				       )
		      );
	}

	@Test
	public void
	testConjunction2()
	{
		check(
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, i3r)
				       )
		      ,new VarSet(i3r),
		      ConjunctiveBlock(
				       EQ_Int(i0r, __),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, i3r)
				       )
		      );
	}

	@Test
	public void
	testConjunction3()
	{
		check(
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, i3r)
				       )
		      ,new VarSet(i3r, i1r),
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       CONTAINS(o0r, i3r)
				       )
		      );
	}


	// disjunction
	@Test
	public void
	testDisjunction()
	{
		check(
		      DisjunctiveBlock(
				       EQ_Int(i0r, i3r),
				       ConjunctiveBlock(
							CONTAINS(o0r, i3r),
							CONTAINS(o0r, i4r)
							)
				       )
		      ,new VarSet(i3r),
		      DisjunctiveBlock(
				       EQ_Int(i0r, i3r),
				       ConjunctiveBlock(
							CONTAINS(o0r, i3r),
							CONTAINS(o0r, __)
							)
				       )
		      );
	}

	// nested conjunction
	@Test
	public void
	testNestedConjunction()
	{
		check(
		      ConjunctiveBlock(
				       CONTAINS(o1r, o0r),
				       ConjunctiveBlock(
							CONTAINS(o0r, i3r),
							CONTAINS(o0r, i4r)
							)
				       )
		      ,new VarSet(i3r, o0r),
		      ConjunctiveBlock(
				       CONTAINS(o1r, o0r),
				       ConjunctiveBlock(
							CONTAINS(o0r, i3r),
							CONTAINS(o0r, __)
							)
				       )
		      );
	}

	// NoFail
	@Test
	public void
	testNoFail()
	{
		check(
		      NoFail(
			     ConjunctiveBlock(
					      CONTAINS(o1r, o0r),
					      CONTAINS(o0r, i3r),
					      CONTAINS(o0r, i4r)
					      )
			     )
		      ,new VarSet(i3r, o0r),
		      NoFail(
			     ConjunctiveBlock(
					      CONTAINS(o1r, o0r),
					      CONTAINS(o0r, i3r),
					      CONTAINS(o0r, i4r)
					      )
			     )
		      );
	}

	// reduction
	@Test
	public void
	testReduction()
	{
		paratest("proto_testReduction");
	}

	public void
	proto_testReduction()
	{
		check(
		      Reduction(Reductors.SET(i0r, o2r),
				CONTAINS(o1r, o0r),
				CONTAINS(o0r, i0r))
		      ,new VarSet(i3r),
		      Reduction(Reductors.SET(i0r, __),
				CONTAINS(o1r, o0r),
				CONTAINS(o0r, i0r))
		      );
	}
}