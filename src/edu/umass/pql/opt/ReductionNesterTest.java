/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class ReductionNesterTest extends TestBase
{
	public void
	check(Reduction j, Join other)
	{
		Optimizer.tryNestReduction(j);
		assertEquals(other.toString(), j.toString());
	}

	// ================================================================================

	// reduction
	@Test
	public void
	testSimpleReduction()
	{
		paratest("proto_testSimpleReduction");
	}

	public void
	proto_testSimpleReduction()
	{
		check(
		      Reduction(Reductors.SET(i0r, o2r),
				CONTAINS(o1r, o0r),
				CONTAINS(o0r, i0r))
		      ,
		      Reduction(Reductors.SET(i0r, o2r),
				CONTAINS(o1r, o0r),
				CONTAINS(o0r, i0r))
		      );
	}


	@Test
	public void
	testEligibleReduction()
	{
		paratest("proto_testEligibleReduction");
	}

	public void
	proto_testEligibleReduction()
	{
		Reductor merged = Reductors.MAP(i0r, Reductor.INPUT_FROM_INNER_REDUCTOR, o3w);
		merged.setInnerReductor(Reductors.SET(i1r, o2w));

		check(
		      Reduction(Reductors.MAP(i0r, o2r, o3w),
				CONTAINS(o1r, i0w),
				Reduction(Reductors.SET(i1r, o2w),
					  CONTAINS(o0r, i1w)))
		      ,
		      Reduction(merged,
				CONTAINS(o1r, i0w),
				CONTAINS(o0r, i1w))
		      );
	}

	@Test
	public void
	testIneligibleReduction()
	{
		paratest("proto_testIneligibleReduction");
	}

	public void
	proto_testIneligibleReduction()
	{
		check(
		      Reduction(Reductors.MAP(i0r, o2r, o3w),
				CONTAINS(o1r, i0w),
				Reduction(Reductors.SET(i1r, o2w),
					  CONTAINS(o0r, i1w)),
				EQ_Object(o2r, o1r) // slightly nonsensical, but the point is that o2 gets used again
				)
		      ,
		      Reduction(Reductors.MAP(i0r, o2r, o3w),
				CONTAINS(o1r, i0w),
				Reduction(Reductors.SET(i1r, o2w),
					  CONTAINS(o0r, i1w)),
				EQ_Object(o2r, o1r)
				)
		      );
	}
}