/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class CSETest extends TestBase
{
	final static String UNCHANGED = null;

	// UNCHANGED as expected stands for `unchanged'
	public void
	check(Join j, String ... expected_parts)
	{
		String expected;

		if (expected_parts.length == 0)
			expected = "{}";
		else if (expected_parts.length == 1)
			if (expected_parts[0] == UNCHANGED)
				expected = j.toString();
			else
				expected = expected_parts[0];
		else {
			String s = "{";
			for (String st : expected_parts)
				s = s + " " + st + "; ";
			s = s + "}";
			expected = s;
		}

		String result = Optimizer.cse(j).toString();
		assertEquals(expected, result);
	}

	// ================================================================================

	@Test
	public void
	testTrivial()
	{
		check(LT_Int(i0r, i1r),
		      UNCHANGED);
	}

	// conjunction without merge opportunity
	@Test
	public void
	testConjunction0()
	{
		check(ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LT_Int(i1r, i2r),
				       LT_Int(i1r, i3r),
				       LT_Int(i3r, i2r)
				       ),
		      UNCHANGED
		      );
	}

	// conjunction without merge opportunity (would require fusing vars)
	@Test
	public void
	testConjunction1()
	{
		check(ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LOOKUP(o0r, i1r, i3r),
				       LT_Int(i1r, i3r),
				       LOOKUP(o0r, i1r, i2r),
				       LT_Int(i1r, i2r)
				       ),
		      UNCHANGED
		      );
	}

	// conjunction with trivial merge opportunity (identical)
	@Test
	public void
	testConjunction2()
	{
		check(ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LOOKUP(o0r, i1r, i3r),
				       LT_Int(i1r, i3r),
				       LOOKUP(o0r, i1r, i2r),
				       LT_Int(i1r, i3r)
				       ),
		      "EQ_Int(?i0; ?i1)",
		      "LOOKUP(?o0; ?i1, ?i3)",
		      "LT_Int(?i1, ?i3)",
		      "LOOKUP(?o0; ?i1, ?i2)"
		      );
	}

	// conjunction with multiple merge opportunities
	@Test
	public void
	testConjunction3()
	{
		check(ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LOOKUP(o0r, i1r, i2r),
				       LT_Int(i1r, i2r),
				       LOOKUP(o0r, i1r, i2r),
				       LT_Int(i1r, i2r)
				       ),
		      "EQ_Int(?i0; ?i1)",
		      "LOOKUP(?o0; ?i1, ?i2)",
		      "LT_Int(?i1, ?i2)"
		      );
	}

	// conjunction with fusion opportunity: replace wildcard with var
	@Test
	public void
	testConjunction4()
	{
		check(ConjunctiveBlock(
				       LOOKUP(o0r, i0r, __),
				       EQ_Int(i0r, i1r),
				       LOOKUP(o0r, i0r, i2r),
				       LT_Int(i1r, i2r)
				       ),
		      "LOOKUP(?o0; ?i0, ?i2)",
		      "EQ_Int(?i0; ?i1)",
		      "LT_Int(?i1, ?i2)"
		      );
	}

	// conjunction with multiple fusion opportunity: replace wildcard with var
	@Test
	public void
	testConjunction5()
	{
		check(ConjunctiveBlock(
				       LOOKUP(o0r, __, i2r),
				       EQ_Int(i0r, i1r),
				       LOOKUP(o0r, __, i2r),
				       EQ_Int(i0r, __),
				       LOOKUP(o0r, i0r, i2r)
				       ),
		      "LOOKUP(?o0; ?i0, ?i2)",
		      "EQ_Int(?i0; ?i1)"
		      );
	}

	// conjunction with faux fusion opportunity: would make independent queries dependent
	@Test
	public void
	testConjunction6()
	{
		check(ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       LOOKUP(o0r, __, i2r),
				       LT_Int(i1r, i2r),
				       LOOKUP(o0r, i3r, __)
				       ),
		      UNCHANGED
		      );
	}

	// disjunction ignored
	@Test
	public void
	testDisjunction()
	{
		check(ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       DisjunctiveBlock(
							LOOKUP(o0r, __, i2r),
							INT_RANGE_CONTAINS(l0r, l1r, i2r)
							),
				       LOOKUP(o0r, i1r, i2r)
				       ),
		      UNCHANGED
		      );
	}

	// nested conjunction merge opportunity preserved
	@Test
	public void
	testNestedConjunction()
	{
		check(ConjunctiveBlock(
				       CONTAINS(o1r, o0r),
				       ConjunctiveBlock(EQ_Int(l0r, i1r),
							CONTAINS(o1r, o0r),
							LOOKUP(o0r, __, i2r)),
				       EQ_Int(l0r, __),
				       LOOKUP(o0r, i0r, i2r)
				       ),
		      "CONTAINS(?o1; ?o0)",
		      "EQ_Int(?l0; ?i1)",
		      "LOOKUP(?o0; ?i0, ?i2)"
		      );
	}

	// elim pointless True
	@Test
	public void
	testTrueElimination()
	{
		check(ConjunctiveBlock(
				       CONTAINS(o1r, o0r),
				       TRUE,
				       ConjunctiveBlock(EQ_Int(l0r, i1r),
							CONTAINS(o1r, o0r),
							TRUE,
							LOOKUP(o0r, __, i2r)),
				       EQ_Int(l0r, __),
				       LOOKUP(o0r, i0r, i2r)
				       ),
		      "CONTAINS(?o1; ?o0)",
		      "EQ_Int(?l0; ?i1)",
		      "LOOKUP(?o0; ?i0, ?i2)"
		      );
	}

	// NoFail merge opportunity ignored
	@Test
	public void
	testNoFail()
	{
		check(ConjunctiveBlock(
				       CONTAINS(o1r, o0r),
				       NoFail(ConjunctiveBlock(EQ_Int(l0r, i1r),
							       CONTAINS(o1r, o0r),
							       LOOKUP(o0r, __, i2r))),
				       EQ_Int(l0r, __),
				       LOOKUP(o0r, i0r, i2r)
				       ),
		      UNCHANGED
		      );
	}

	// reduction
	@Test
	public void
	testReduction()
	{
		paratest("proto_testReduction");
	}

	public void
	proto_testReduction()
	{
		check(ConjunctiveBlock(
				       CONTAINS(o1r, o0r),
				       Reduction(Reductors.SET(i0r, o2r),
						 CONTAINS(o1r, o0r),
						 CONTAINS(o0r, i0r),
						 CONTAINS(o0r, i0r))),
		      "CONTAINS(?o1; ?o0)",
		      "Reduce[SET(?i0): ?o2]: { CONTAINS(?o1; ?o0);  CONTAINS(?o0; ?i0); }" 
		      );
	}
}