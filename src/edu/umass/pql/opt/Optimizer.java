/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;
import replacetool.ReplaceTool;

public class Optimizer
{
	/**
	 * Compute the set binding of dependencies for all variables in the join, normalised into read-variables.
	 *
	 * A binding dependency is a variable that MUST be bound in order to bind the relevant variable.  For example,
	 * when doing an array lookup, only the array object is a binding dependency of the lookup result, not the index.
	 *
	 * @param join The join to analyse
	 */
	public static Map<Integer, VarSet>
	bindingDependencyMap(Join join)
	{
		return DependencyAnalysis.dependencies(join);
	}


	/**
	 * Extract a sub-join that binds all of the specified registers.
	 *
	 */
	public static Join
	backwardSliceRangeInference(Join join, VarSet registers)
	{
		return BackwardSlice.backwardSlice(join, registers, JoinPredicate.rangeInference(registers));
	}

	/**
	 * Eliminate common subexpressions in a join
	 *
	 * May modify the input join
	 */
	public static Join
	cse(Join join)
	{
		return CSE.optimize(join);
	}

	/**
	 * Replace all bindings to variables that are not wanted by wildcards
	 *
	 * All read-only variables are preserved, as are all implicitly needed variables.
	 *
	 * @param join The join to modify
	 * @param wanted_variables The variables whose bindings we care about
	 */
	public static void
	wildcard(Join join, VarSet wanted_variables)
	{
		Wildcarder.wildcard(join, wanted_variables);
	}

	/**
	 * Optimise a single sequence of joins (nonrecursively)
	 */
	public static Join
	selectAccessPath(final Env env, final VarInfo vi, final boolean assume_parallel, final Join... joins)
	{
		final SelectPath path = SelectPath.create(joins);
		path.setVarInfo(new VarInfo());
		path.schedule(vi, env, assume_parallel);
		return path.getComponent(0);
	}

        public static Join
	selectAccessPathRecursively(final Env env, Join body, boolean want_parallelism) {
            return selectAccessPathRecursively(env, ReplaceTool.calculateConstantSet(body), body, want_parallelism);
        }

	public static Join
	selectAccessPathRecursively(final Env env, final VarSet pre_constants, Join body, boolean want_parallelism)
	{
		final VarSet constants = pre_constants.copy();
		final VarInfo varinfo = new VarInfo(constants);

		class APSelector extends JoinVisitor
		{
			Join result;
			boolean can_para = false;
			boolean want_para;

			public APSelector(boolean para)
			{
				this.want_para = para;
			}

			@Override
			public void
			visitDefault(Join join)
			{
				this.result = join;
				this.result.addAllVariables(constants);
			}

			@Override
			public void
			visitConjunctiveBlock(AbstractBlock.Conjunctive b)
			{
				final VarSet temp_constants = constants.copy();
				final boolean try_to_parallelise = this.can_para && this.want_para;
				this.can_para = false;
				if (try_to_parallelise) // only try once, on the top level
					this.want_para = false;
				visitControlStructure(b);
				Join[] joins = new Join[b.getComponentsNr()];

				for (int i = 0; i < joins.length; i++)
					joins[i] = b.getComponent(i);

				varinfo.setConstants(temp_constants); // ignore local bindings, since we want to re-schedule this block
				this.result = selectAccessPath(env, varinfo, try_to_parallelise, joins);
				varinfo.setConstants(constants);
			}

			@Override
			public void
			visitReduction(Reduction b)
			{
				for (int i = 0; i < b.getComponentsNr(); i++) {
					this.can_para = true;
					b.getComponent(i).accept(this);
					b.setComponent(i, this.result);
				}
				visitDefault(b);
			}

			@Override
			public void
			visitControlStructure(ControlStructure b)
			{
				for (int i = 0; i < b.getComponentsNr(); i++) {
					b.getComponent(i).accept(this);
					b.setComponent(i, this.result);
				}
				visitDefault(b);
			}
		};

		APSelector as = new APSelector(want_parallelism);
		body.accept(as);
		return as.result;
	}
	/**
	 * Remove unnecessary coercions and type checks from a join
	 */
	public static <T> Join
	removeUnnecessaryTypeChecks(Join j, TypeSystem<T> tsys)
	{
		return new TypeOpRemover<T>(tsys).run(j);
	}

	/**
	 * Try to merge an eligible reductor with an eligible inner reduction to produce a nested reduction
	 */
	public static Reduction
	tryNestReduction(Reduction r)
	{
		return ReductionNester.tryToNest(r);
	}

	public static Join
	nestReductionRecursively(Join body)
	{
		class Nester extends JoinVisitor
		{
			Join result;

			@Override
			public void
			visitDefault(Join join)
			{
				this.result = join;
			}

			@Override
			public void
			visitReduction(Reduction r)
			{
				for (int i = 0; i < r.getComponentsNr(); i++) {
					r.getComponent(i).accept(this);
					r.setComponent(i, this.result);
				}
				this.result = tryNestReduction(r);
			}

			@Override
			public void
			visitControlStructure(ControlStructure b)
			{
				for (int i = 0; i < b.getComponentsNr(); i++) {
					b.getComponent(i).accept(this);
					b.setComponent(i, this.result);
				}
				visitDefault(b);
			}
		};

		Nester as = new Nester();
		body.accept(as);
		return as.result;
	}
}