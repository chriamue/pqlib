/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

/**
 * Common subexpression elimination (and merging) optimisation implementation
 */
public final class CSE extends JoinVisitor
{
	public static Join
	optimize(Join join)
	{
		CSE cse = new CSE();
		join.accept(cse);
		if (cse.result == null)
			return PQLFactory.TRUE;
		else
			return cse.result;
	}



	// ================================================================================
	// PQL visitor

	Join result = null;
	ConjunctiveCSEBlock cseblock = new ConjunctiveCSEBlock();
	int cseblock_depth = 0; // nested conjunctive blocks are flattened; cseblock_depth counts what conjunctive block nesting depth we're at

	public void visitDefault(Join j)
	{
		if (j == PQLFactory.TRUE)
			this.result = null;
		else
			this.result = j;
	}

	@Override
	public void
	visitConjunctiveBlock(AbstractBlock.Conjunctive block)
	{
		// increase cseblock_depth as part of flattening nested blocks
		++this.cseblock_depth;
		for (int i = 0; i < block.getComponentsNr(); i++) {
			Join join = block.getComponent(i);
			join.accept(this);
			// Insert into cseblock sequence
			if (this.result != null)
				this.cseblock.add(this.result);
		}
		--this.cseblock_depth;

		if (this.cseblock_depth == 0) {
			this.result = this.cseblock.extract();
			this.cseblock = null;
		} else
			this.result = null;
	}

	@Override
	public void
	visitControlStructure(ControlStructure block)
	{
		ConjunctiveCSEBlock old_cseblock = this.cseblock;
		int old_cseblock_depth = this.cseblock_depth;
		for (int i = 0; i < block.getComponentsNr(); i++) {
			this.cseblock_depth = 0;
			this.cseblock = new ConjunctiveCSEBlock();
			block.getComponent(i).accept(this);
			block.setComponent(i, this.result);
		}
		this.result = block;
		this.cseblock_depth = old_cseblock_depth;
		this.cseblock = old_cseblock;
	}

	// ================================================================================

	//
	static class ConjunctiveCSEBlock
	{
		ArrayList<Join> join_list = new ArrayList<Join>();
		HashMap<String, HashSet<Join>> absorbing_joins = new HashMap<String, HashSet<Join>>();

		public void
		add(Join join)
		{
			if (!join.isControlStructure()) {
				HashSet<Join> potential_absorbents = this.absorbing_joins.get(join.getName());
				if (potential_absorbents != null) {
					for (Join absorber : potential_absorbents) {
						if (couldAbsorbJoin(join, absorber))
							return; // join is redundant
						else if (couldAbsorbJoin(absorber, join)) {
							doAbsorbJoin(absorber, join); // update existing join to also do the work of `join'
							return; // join functionality has been absorbed into earlier join
						}
					}
				}
			}

			doAppend(join);
		}

		void
		doAppend(Join join)
		{
			this.join_list.add(join);
			if (!join.isControlStructure()) {
				HashSet<Join> js = absorbing_joins.get(join.getName());
				if (js == null) {
					js = new HashSet<Join>();
					absorbing_joins.put(join.getName(), js);
				}
				js.add(join);
			}
		}


		static boolean // this_join binds less than or equal than other_join
		couldAbsorbJoin(Join this_join, Join other_join)
		{
			if (this_join.isControlStructure())
				return false;
			if (!this_join.getName().equals(other_join.getName()))
				return false;
			if (this_join.getArgsNr() != other_join.getArgsNr())
				return false;

			for (int i = 0; i < this_join.getArgsNr(); i++) {
				int arg = this_join.getArg(i);
				if (arg != other_join.getArg(i)
				    && !Env.isWildcard(arg))
					return false;
			}
			return true;
		}

		static void
		doAbsorbJoin(Join this_join, Join other_join)
		{
			for (int i = 0; i < this_join.getArgsNr(); i++)
				this_join.setArg(i, other_join.getArg(i));
		}

		public Join extract()
		{
			if (this.join_list.size() == 1)
				return this.join_list.get(0);
			else
				return PQLFactory.ConjunctiveBlock(this.join_list);
		}
	}
}
