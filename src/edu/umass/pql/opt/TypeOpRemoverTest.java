/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class TypeOpRemoverTest extends TestBase
{
	static final int TYPE_INT = 1;
	static final int TYPE_LONG = 2;
	static final int TYPE_BYTE = 3;
	static final int TYPE_CHAR = 4;
	static final int TYPE_SHORT = 5;
	static final int TYPE_BOOLEAN = 6;

	public static class TSystem implements TypeSystem<Integer>
	{
		public HashMap<Integer, Integer> types = new HashMap<Integer, Integer>();

		public TSystem(int[] args)
		{
			for (int i = 0; i < args.length; i += 2) {
				types.put(Env.readVar(args[i]), args[i+1]);
			}
		}

		public Integer getType(int var) { return types.get(Env.readVar(var)); }
		public Integer getIntType() { return TYPE_INT; }
		public Integer getCharType() { return TYPE_CHAR; }
		public Integer getBooleanType() { return TYPE_BOOLEAN; }
		public Integer getByteType() { return TYPE_BYTE; }
		public Integer getShortType() { return TYPE_SHORT; }
		public Integer getLongType() { return TYPE_LONG; }
		public Integer getTypeForJAVA_TYPE(Object t) { return null; }

		public boolean
		isSubtypeOf(Integer lhs, Integer rhs)
		{
			if (lhs == null || rhs == null)
				return false;

			if (lhs == rhs)
				return true;

			switch (lhs) {
			case TYPE_BOOLEAN:
				return rhs == TYPE_BOOLEAN;

			case TYPE_BYTE: // already treated BYTE == BYTE earlier
			case TYPE_SHORT: // SHORT and CHAR should be convertible
			case TYPE_CHAR:
				return (rhs == TYPE_SHORT || rhs == TYPE_INT || rhs == TYPE_CHAR || rhs == TYPE_LONG);

			case TYPE_INT:
				return (rhs == TYPE_INT || rhs == TYPE_LONG);

			case TYPE_LONG:
			default:
				return false;
			}
		}
	}

	public static TSystem
	TypeInfo(int ... args)
	{
		return new TSystem(args);
	}

	public void
	check(Join j, TSystem type_system, Join expected)
	{
		Join nj = Optimizer.<Integer>removeUnnecessaryTypeChecks(j, type_system);
		assertEquals(expected.toString(), nj.toString());
	}

	// ================================================================================

	@Test
	public void
	testTrivial()
	{
		check(
		      LT_Int(i0r, i1r)
		      ,TypeInfo(),
		      LT_Int(i0r, i1r)
		      );
	}

	@Test
	public void
	testSimple()
	{
		check(
		      INT(i0r)
		      ,TypeInfo(i0r, TYPE_INT),
		      TRUE
		      );
	}

	// conjunction
	@Test
	public void
	testConjunction0()
	{
		check(
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1r),
				       INT(i0r)
				       )
		      ,TypeInfo(i0r, TYPE_INT),
		      EQ_Int(i0r, i1r)
		      );
	}

	// conjunction
	@Test
	public void
	testConjunction1()
	{
		check(
		      ConjunctiveBlock(
				       EQ_Int(i0r, i1w),
				       INT(i0r)
				       )
		      ,TypeInfo(i0r, TYPE_BYTE),
		      EQ_Int(i0r, i1w)
		      );
	}


	// reduction
	@Test
	public void
	testReduction()
	{
		paratest("proto_testReduction");
	}

	public void
	proto_testReduction()
	{
		check(
		      Reduction(Reductors.SET(i0r, o2r),
				CONTAINS(o1r, o0r),
				INT(o0r),
				CONTAINS(o0r, i0r))
		      ,TypeInfo(o0r, TYPE_SHORT),
		      Reduction(Reductors.SET(i0r, o2r),
				CONTAINS(o1r, o0r),
				CONTAINS(o0r, i0r))
		      );
	}
}