/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class BackwardSliceTest extends TestBase
{
	void
	check(Join j,
	      String s,
	      int ... regs)
	{
		assertEquals(s, Optimizer.backwardSliceRangeInference(j, new VarSet(regs)).toString());
	}

	// ================================================================================

	@Test
	public void
	testSimpleEmpty0()
	{
		check(ADD_Int(i0r, i1r, i2r),
		      "TRUE()",
		      i0r);
	}

	@Test
	public void
	testSimpleEmpty1()
	{
		check(ADD_Int(i0r, i1r, i2r),
		      "TRUE()",
		      i2r);
	}

	@Test
	public void
	testSimpleOne()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       CONTAINS(o0r, i2r),
				       ADD_Int(i2r, i2r, i3r)
				       ),
		      "CONTAINS(?o0; ?i2)",
		      i2r);
	}

	// nontrivial slice for source
	@Test
	public void
	testIndirectDep0()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       CONTAINS(o1r, o0r),
				       CONTAINS(o0r, i2r),
				       ADD_Int(i2r, i2r, i3r)
				       ),
		      "{ CONTAINS(?o1; ?o0);  CONTAINS(?o0; ?i2); }",
		      i2r);
	}

	@Test
	public void
	testIndirectDep1()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, l2r),
				       ADD_Int(i0r, i0r, l0r),
				       CONTAINS(o1r, o0r),
				       CONTAINS(o0r, i2r),
				       INT_RANGE_CONTAINS(l0r, l1r, i2r),
				       ADD_Int(i2r, i2r, i3r)
				       ),
		      "{ ADD_Int(?i0, ?i0; ?l0);  CONTAINS(?o1; ?o0);  CONTAINS(?o0; ?i2);  INT_RANGE_CONTAINS(?l0, ?l1; ?i2); }",
		      i2r);
	}

	// disjunction
	@Test
	public void
	testDisjunction()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       ADD_Int(i0r, i0r, l1r),
				       DisjunctiveBlock(CONTAINS(o0r, i3r),
							INT_RANGE_CONTAINS(i2r, l0r, i3r))),
		      "{ ADD_Int(?i0, ?i1; ?i2);  [ CONTAINS(?o0; ?i3) | INT_RANGE_CONTAINS(?i2, ?l0; ?i3) ]; }",
		      i3r);
	}

	// negation in trace
	@Test
	public void
	testNegation()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       Not(CONTAINS(o0r, i1r))),
		      "{}",
		      i2r);
	}

	// bool in trace
	@Test
	public void
	testBoolNot()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       Bool(Not(CONTAINS(o0r, i1r)), i3r),
				       INT_RANGE_CONTAINS(i2r, i3r, i4r)),
		      "{ ADD_Int(?i0, ?i1; ?i2);  Bool(?i3): Not(): CONTAINS(?o0; ?i1);;;;;  INT_RANGE_CONTAINS(?i2, ?i3; ?i4); }",
		      i4r);
	}

	@Test
	public void
	testBoolUnused()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       Bool(Not(CONTAINS(o0r, i1r)), i3r),
				       INT_RANGE_CONTAINS(i1r, i2r, i4r)),
		      "{ ADD_Int(?i0, ?i1; ?i2);  INT_RANGE_CONTAINS(?i1, ?i2; ?i4); }",
		      i4r);
	}

	// bool in trace
	@Test
	public void
	testBoolInnerUse()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       Bool(CONTAINS(o0r, i4r), i3r),
				       INT_RANGE_CONTAINS(i0r, i2r, i4r)),
		      "{ ADD_Int(?i0, ?i1; ?i2);  Bool(_): CONTAINS(?o0; ?i4);;;  INT_RANGE_CONTAINS(?i0, ?i2; ?i4); }",
		      i4r);
	}

	// bool in trace
	@Test
	public void
	testBoolInnerUseNegated()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       Bool(Not(CONTAINS(o0r, i4r)), i3r),
				       INT_RANGE_CONTAINS(i0r, i2r, i4r)),
		      "{ ADD_Int(?i0, ?i1; ?i2);  INT_RANGE_CONTAINS(?i0, ?i2; ?i4); }",
		      i4r);
	}

	@Test
	public void
	testBoolBlock()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i1r, i2r),
				       Bool(ConjunctiveBlock(Not(CONTAINS(o0r, i4r)),
							     CONTAINS(o1r, i4r),
							     CONTAINS(o0r, o2r)
							     ), i3r),
				       INT_RANGE_CONTAINS(i0r, i2r, i4r)),
		      "{ ADD_Int(?i0, ?i1; ?i2);  Bool(_): CONTAINS(?o1; ?i4);;;  INT_RANGE_CONTAINS(?i0, ?i2; ?i4); }",
		      i4r);
	}

	// reduction in trace
	@Test
	public void
	testReduction()
	{
		paratest("para_testReduction");
	}

	public void
	para_testReduction()
	{
		check(ConjunctiveBlock(ADD_Int(i0r, i0r, l0r),
				       Reduction(Reductors.SET(i1r, o0r),
						 INT_RANGE_CONTAINS(l2r, l2r, i1r)
						 ),
				       ADD_Int(i0r, i0r, l1r),
				       CONTAINS(o0r, i2r)),
		      "{ Reduce[SET(?i1): ?o0]: INT_RANGE_CONTAINS(?l2, ?l2; ?i1);  CONTAINS(?o0; ?i2); }",
		      i2r);
	}

}