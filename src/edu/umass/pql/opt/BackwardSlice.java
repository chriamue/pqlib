/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

public class BackwardSlice extends JoinVisitor
{
	VarSet vars_of_interest;
	VarSet registers; // only used for wildcarding at getResult()
	JoinPredicate join_checker;
	Map<Integer, VarSet> dep_map;
	Join result = null;

	final VarSet EMPTY_VARSET = new VarSet();

	Join
	getResult()
	{
		if (this.result == null)
			return PQLFactory.TRUE;
		else {
			this.vars_of_interest.addAll(registers);
			Optimizer.wildcard(this.result, this.vars_of_interest);
			return this.result;
		}
	}

	VarSet
	deps(int i)
	{
		VarSet v = dep_map.get(i);
		if (v == null)
			return EMPTY_VARSET;
		else
			return v;
	}

	BackwardSlice(VarSet registers, JoinPredicate joinChecker, Map<Integer, VarSet> dependencyMap)
	{
		this.dep_map = dependencyMap;
		this.vars_of_interest = new VarSet();
		this.registers = registers;
		this.join_checker = joinChecker;

		// deps are already transitive, so this suffic
		//		System.err.println("regs = " + registers);
		//		System.err.println("deps = " + dependencyMap);
		for (int var : registers) {
			var = Env.readVar(var);
			//			System.err.println("reg : " + var);
			this.vars_of_interest.addAll(deps(Env.readVar(var)));
		}
		//		System.err.println("voi = " + this.vars_of_interest);
	}

	public static Join
	backwardSlice(Join join, VarSet registers, JoinPredicate joinChecker)
	{
		BackwardSlice slicer = new BackwardSlice(registers, joinChecker, Optimizer.bindingDependencyMap(join));
		join.accept(slicer);
		return slicer.getResult();
	}

	// --------------------------------------------------------------------------------

	ArrayList<Join>
	examineBlock(AbstractBlock block)
	{
		ArrayList<Join> result = new ArrayList<Join>();
		final Join[] blocks = block.getComponents();

		for (Join sub : blocks) {
			sub.accept(this);
			if (this.result != null)
				result.add(this.result);
		}

		return result;
	}

	boolean
	bindsVarOfInterest(PQLParameterized j)
	{
		for (int i = j.getReadArgsNr(); i < j.getArgsNr(); i++)
			if (j.isValidArg(i)
			    && this.vars_of_interest.contains(j.getArg(i)))
				return true;
		return false;
	}

	@Override
	public void visitDefault(Join it)
	{
		if (this.join_checker.contains(it)
		    || bindsVarOfInterest(it))
			this.result = it.copyRecursively();
		else
			this.result = null;
	}

	@Override
	public void visitField(Field fld) { visitDefault(fld); }
	@Override
	public void visitJavaType(Type.JAVA_TYPE jty) { visitDefault(jty); }
	@Override
	public void visitInstantiation(Instantiation ins) { visitDefault(ins); }

	@Override
	public void visitControlStructure(ControlStructure sp) { throw new RuntimeException("Shouldn't be called"); }
	@Override
	public void visitAnyBlock(AbstractBlock b) { throw new RuntimeException("Shouldn't be called"); }

	@Override
	public void
	visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
		ArrayList<Join> r = examineBlock(b);
		if (r.size() == 0)
			this.result = null;
		if (r.size() == 1)
			this.result = r.get(0);
		else
			this.result = PQLFactory.ConjunctiveBlock(r);
	}

	@Override
	public void visitDisjunctiveBlock(AbstractBlock.Disjunctive b)
	{
		ArrayList<Join> r = examineBlock(b);
		if (r.size() == 0)
			this.result = null;
		if (r.size() == 1)
			this.result = r.get(0);
		else
			this.result = PQLFactory.DisjunctiveBlock(r);
	}

	@Override
	public void visitSelectPath(SelectPath sp)
	{
		sp.getComponent(0).accept(this);
	}

	@Override
	public void visitSchedule(Schedule sp)
	{
		sp.getComponent(0).accept(this);
	}

	@Override
	public void visitBool(NoFail.Bool b)
	{
		if (bindsVarOfInterest(b))
			this.result = b.copyRecursively();
		else {
			b.getComponent(0).accept(this);
			if (this.result == null)
				return;
			else
				this.result = PQLFactory.Bool(this.result, b.getArg(0));
		}
	}

	@Override
	public void visitNot(Not n)
	{
		this.result = null;
	}

	@Override
	public void visitNoFail(NoFail b)
	{
		b.getComponent(0).accept(this);
		if (this.result == null)
			return;
		else
			this.result = PQLFactory.NoFail(this.result);
	}

	boolean
	reductorBindsVarOfInterest(Reductor r)
	{
		if (bindsVarOfInterest(r))
			return true;
		if (r.getInnerReductor() != null)
			return this.reductorBindsVarOfInterest(r.getInnerReductor());
		return false;
	}

	@Override
	public void visitReduction(Reduction red)
	{
		for (Reductor r: red.getReductors()) {
			if (this.reductorBindsVarOfInterest(r)) {
				this.result = red.copyRecursively();
				return;
			}
		}

		this.result = null;
	}
}