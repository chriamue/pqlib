/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import edu.umass.bc.RuntimeCreator;
import org.junit.*;
import static org.junit.Assert.*;
import edu.umass.pql.il.Predicate;
import java.lang.reflect.*;

public class TestBase implements TableConstants, VarConstants
{

	protected Env env;
	public final static int __ = 0;
	public final static int i0r = Env.encodeReadVar(TYPE_INT, 0);
	public final static int i0w = Env.encodeWriteVar(TYPE_INT, 0);
	public final static int i1r = Env.encodeReadVar(TYPE_INT, 1);
	public final static int i1w = Env.encodeWriteVar(TYPE_INT, 1);
	public final static int i2r = Env.encodeReadVar(TYPE_INT, 2);
	public final static int i2w = Env.encodeWriteVar(TYPE_INT, 2);
	public final static int i3r = Env.encodeReadVar(TYPE_INT, 3);
	public final static int i3w = Env.encodeWriteVar(TYPE_INT, 3);
	public final static int i4r = Env.encodeReadVar(TYPE_INT, 4);
	public final static int i4w = Env.encodeWriteVar(TYPE_INT, 4);
	public final static int i5r = Env.encodeReadVar(TYPE_INT, 5);
	public final static int i5w = Env.encodeWriteVar(TYPE_INT, 5);
	public final static int i6r = Env.encodeReadVar(TYPE_INT, 6);
	public final static int i6w = Env.encodeWriteVar(TYPE_INT, 6);

	public final static int l0r = Env.encodeReadVar(TYPE_LONG, 0);
	public final static int l0w = Env.encodeWriteVar(TYPE_LONG, 0);
	public final static int l1r = Env.encodeReadVar(TYPE_LONG, 1);
	public final static int l1w = Env.encodeWriteVar(TYPE_LONG, 1);
	public final static int l2r = Env.encodeReadVar(TYPE_LONG, 2);
	public final static int l2w = Env.encodeWriteVar(TYPE_LONG, 2);
	public final static int l3r = Env.encodeReadVar(TYPE_LONG, 3);
	public final static int l3w = Env.encodeWriteVar(TYPE_LONG, 3);
	public final static int l4r = Env.encodeReadVar(TYPE_LONG, 4);
	public final static int l4w = Env.encodeWriteVar(TYPE_LONG, 4);

	public final static int d0r = Env.encodeReadVar(TYPE_DOUBLE, 0);
	public final static int d0w = Env.encodeWriteVar(TYPE_DOUBLE, 0);
	public final static int d1r = Env.encodeReadVar(TYPE_DOUBLE, 1);
	public final static int d1w = Env.encodeWriteVar(TYPE_DOUBLE, 1);
	public final static int d2r = Env.encodeReadVar(TYPE_DOUBLE, 2);
	public final static int d2w = Env.encodeWriteVar(TYPE_DOUBLE, 2);

	public final static int o0r = Env.encodeReadVar(TYPE_OBJECT, 0);
	public final static int o0w = Env.encodeWriteVar(TYPE_OBJECT, 0);
	public final static int o1r = Env.encodeReadVar(TYPE_OBJECT, 1);
	public final static int o1w = Env.encodeWriteVar(TYPE_OBJECT, 1);
	public final static int o2r = Env.encodeReadVar(TYPE_OBJECT, 2);
	public final static int o2w = Env.encodeWriteVar(TYPE_OBJECT, 2);
	public final static int o3r = Env.encodeReadVar(TYPE_OBJECT, 3);
	public final static int o3w = Env.encodeWriteVar(TYPE_OBJECT, 3);
	public final static int o4r = Env.encodeReadVar(TYPE_OBJECT, 4);
	public final static int o4w = Env.encodeWriteVar(TYPE_OBJECT, 4);
	public final static int o5r = Env.encodeReadVar(TYPE_OBJECT, 5);
	public final static int o5w = Env.encodeWriteVar(TYPE_OBJECT, 5);

	private Env backup_env;

        private static boolean clonesDeactivated = true;

	private void
	backupState()
	{
		this.backup_env = this.env.copy();
	}

	private void
	restoreState()
	{
		this.env = this.backup_env.copy();
	}

	@Before
	public void
	setUp()
	{
		PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.UNDEFINED); // force iteration
		env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
			       new Object[] {
				       new int[7],
				       new long[5],
				       new double[3],
				       new Object[6]
			       });
	}

	final static int NUMBER_OF_CLONES = 2; // # of times we test a clone

	public void
	paratest(String method_name)
	{
		for (PQLFactory.ParallelisationMode mode : PQLFactory.PARALLELISATION_MODES) {
			PQLFactory.setParallelisationMode(mode);

			boolean found = false;

			for (Method m : this.getClass().getDeclaredMethods())
				if (m.getName().equals(method_name)) {
					try {
						if (found)
							throw new RuntimeException("Unit test method `" + method_name + "' overloaded");
						m.invoke(this);
						found = true;
					} catch (Exception e) {
						throw new RuntimeException("In parallelisation mode " + mode, e);
					}
				}
			if (!found)
				throw new RuntimeException("Unit test method `" + method_name + "' not found");
		}
		PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.UNDEFINED);
	}


	void
	testWithClones(Join join, String method_name, Object[] args)
	{
		Method my_method = null;

		// FIXME: we shouldn't be checking only declared methods.
		for (Method m : this.getClass().getDeclaredMethods()) {
			if (m.getName().equals(method_name)) {
				my_method = m;
				break;
			}
		}
		if (my_method == null)
			for (Method m : TestBase.class.getDeclaredMethods()) {
				if (m.getName().equals(method_name)) {
					my_method = m;
					break;
				}
			}
		if (my_method == null)
			throw new RuntimeException("Could not find method `" + method_name + "' to reflect on!");

		Object[] all_args = new Object[args.length + 1];
		for (int i = 0; i < args.length; i++)
			all_args[i + 1] = args[i];

		Join clone = join.copyRecursively();
		Join clone2 = clone.copyRecursively();

		backupState();

		reset();
		restoreState();
		try {
			all_args[0] = join;
			my_method.invoke(this, all_args);
		} catch (InvocationTargetException t) {
			if (t.getTargetException() instanceof RuntimeException)
				throw ((RuntimeException) t.getTargetException());
			else
				throw new RuntimeException(t.getTargetException());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

                if (!clonesDeactivated) {
                    reset();
                    restoreState();
                    try {
                            all_args[0] = clone;
                            my_method.invoke(this, all_args);
                    } catch (Throwable t) {
                            throw new RuntimeException("on first clone", t);
                    }

                    reset();
                    restoreState();
                    try {
                            all_args[0] = clone2;
                            my_method.invoke(this, all_args);
                    } catch (Throwable t) {
                            throw new RuntimeException("on clone of clone", t);
                    }
                }
	}
	static int ctestc = 0;

	/**
	 * Setup method: to override by clients
	 */
	protected void
	reset() {}

	void
	doCheckTrueP(Predicate p)
	{
		assertTrue(p.test(env));
	}

	void
	checkTrue(Predicate p)
	{
		testWithClones(p, "doCheckTrueP", new Object[0]);
	}

	void
	doCheckTrueJ(Join j)
	{
		testNonempty(j);
	}

	protected void
	checkTrue(Join j)
	{
		testWithClones(j, "doCheckTrueJ", new Object[0]);
	}

	void
	doCheckFalseP(Predicate p)
	{
		assertFalse(p.test(env));
	}

	void
	checkFalse(Predicate p)
	{
		testWithClones(p, "doCheckFalseJ", new Object[0]);
	}

	void
	doCheckFalseJ(Join j)
	{
		testEmpty(j);
	}

	protected void
	checkFalse(Join j)
	{
		testWithClones(j, "doCheckFalseJ", new Object[0]);
	}


	final int max_count = 1000000;


	void
	testCount(int count, Join it)
	{
		testWithClones(it, "doTestCount", new Object[] { count });
	}

	void
	doTestCount(Join it, int count)
	{
		checkMetaInfo(it);
		int c = 0;
		it.reset(this.env);
		while (it.next(this.env)) {
			++c;
			if (c > max_count)
				throw new RuntimeException("More than " + max_count + " bindings in iterator count; infinite loop?");
		}

		assertEquals(count, c);
	}

	void
	testCountHuge(Join it)
	{
		checkMetaInfo(it);
		int c = 0;
		it.reset(this.env);
		while (it.next(this.env)) {
			++c;
			if (c > max_count)
				return;
		}
		throw new RuntimeException("Fewer than " + max_count + " bindings: " + c);
	}

	void
	doTestEmpty(Join it)
	{
		checkMetaInfo(it);
                if (RuntimeCreator.useRuntimeCreator) {
                    if (!RuntimeCreator.alreadyInitialized)
                        RuntimeCreator.init( it, env );
                    assertFalse(RuntimeCreator.test(env));
                }
                else {
                    it.reset(this.env);
                    assertFalse(it.next(this.env));
                }
	}

	void
	testEmpty(Join it)
	{
		testWithClones(it, "doTestEmpty", new Object[] { });
	}

	void
	doTestNonempty(Join it)
	{
		checkMetaInfo(it);
                if (RuntimeCreator.useRuntimeCreator) {
                    if (!RuntimeCreator.alreadyInitialized)
                        RuntimeCreator.init( it, env );

                    assertTrue(RuntimeCreator.test(env));
                }
                else {
                    it.reset(this.env);
                    assertTrue(it.next(this.env));
                }
	}

	void
	testNonempty(Join it)
	{
		testWithClones(it, "doTestNonempty", new Object[] { });
	}

	public void
	checkNoRandomAccess(Join j)
	{
		testWithClones(j, "doCheckNoRandomAccess", new Object[] { });
	}

	public void
	checkFactory(PQLParameterized j, String factory_class, String consname)
	{
		try {
			Class<?> fclass = Class.forName(factory_class);

			Object[] args = new Object[j.getArgsNr()];
			Class[] arg_types = new Class[j.getArgsNr()];
			for (int i = 0; i < args.length; i++) {
				args[i] = j.getArg(i);
				arg_types[i] = int.class;
			}

			Method m = fclass.getMethod(consname, arg_types);
			Object result = m.invoke(null, args);

			assertEquals(j.getClass(), result.getClass());
			assertEquals(j.toString(), result.toString());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void
	checkMetaInfo(Join j)
	{
		VarInfo vi = new VarInfo();
		if (j.getConstructorName() != null)
			checkFactory(j,  PQLFactory.Gen.FACTORY_CLASS, j.getConstructorName());
		assertTrue(j.accessPathSelected());
		assertTrue(j.getSize(vi, env) >= 0.0);
		assertTrue(j.getAccessCost(vi, env) >= 0.0);
		assertTrue(j.getFullCost(vi, env) >= 0.0);
		assertTrue(j.getSelectivity(vi, env) >= 0.0);
		assertTrue(j.getSelectivity(vi, env) <= 1.0);
	}

	public void
	doCheckNoRandomAccess(Join j)
	{
		checkMetaInfo(j);
		assertFalse(j.hasRandomAccess());
		try {
			j.getFanout(env);
			fail();
		} catch (RuntimeException __) {};
		// try {
		// 	j.getAtIndex(env, 1);
		// 	fail();
		// } catch (RuntimeException __) {};
		// try {
		// 	j.moveToIndex(env, 1);
		// 	fail();
		// } catch (RuntimeException __) {};
	}

	public void
	checkAPI(Join join, int read_args_nr, int args_nr, int[] expected)
	{
		testWithClones(join, "doCheckAPI", new Object[] { read_args_nr, args_nr, expected });
	}

	public void
	doCheckAPI(Join bi, int read_args_nr, int args_nr, int[] expected)
	{
		assertEquals(args_nr, bi.getArgsNr());
		assertEquals(read_args_nr, bi.getReadArgsNr());
		assertEquals(args_nr, expected.length);
		assertEquals(args_nr - read_args_nr, bi.getWritableArgsNr());
		int i = 0;

		try {
			for (i = 0; i < expected.length; i++)
				assertEquals(expected[i], bi.getArg(i));

			for (i = 0; i < expected.length; i++)
				bi.setArg(i, Env.encodeReadVar(VarConstants.TYPE_INT, i));

			for (i = 0; i < expected.length; i++)
				assertEquals(Env.encodeReadVar(VarConstants.TYPE_INT, i),
					     bi.getArg(i));

			for (i = 0; i < expected.length; i++)
				bi.setArg(i, Env.encodeReadVar(VarConstants.TYPE_DOUBLE, i));

			for (i = 0; i < expected.length; i++)
				assertEquals(Env.encodeReadVar(VarConstants.TYPE_DOUBLE, i),
					     bi.getArg(i));

			for (i = 0; i < expected.length; i++)
				bi.setArg(i, expected[i]);

			for (i = 0; i < expected.length; i++)
				assertEquals(expected[i], bi.getArg(i));

		} catch (Error e) {
			System.err.print("i = " + i + ", with args supposedly being {");
			for (i = 0; i < expected.length; i++) {
				if (i > 0)
					System.err.print(", ");
				System.err.print(Env.showVar(bi.getArg(i)));
			}
			System.err.println("}");
			throw e;
		}


	}


	// --------------------------------------------------------------------------------

	void
	setInts(int v0, int v1, int v2)
	{
		env.setInt(i0r, v0);
		env.setInt(i1r, v1);
		env.setInt(i2r, v2);
	}

	void
	setInts(int v0, int v1, int v2, int v3)
	{
		setInts(v0, v1, v2);
		env.setInt(i3r, v3);
	}

	void
	setInts(int v0, int v1, int v2, int v3, int v4)
	{
		setInts(v0, v1, v2, v3);
		env.setInt(i4r, v4);
	}

	void
	setLongs(long v0, long v1, long v2)
	{
		env.setLong(l0r, v0);
		env.setLong(l1r, v1);
		env.setLong(l2r, v2);
	}

	void
	setLongs(long v0, long v1, long v2, long v3, long v4)
	{
		setLongs(v0, v1, v2);
		env.setLong(l3r, v3);
		env.setLong(l4r, v4);
	}

	void
	setDoubles(double v0, double v1, double v2)
	{
		env.setDouble(d0r, v0);
		env.setDouble(d1r, v1);
		env.setDouble(d2r, v2);
	}

	void
	setObjects(Object v0, Object v1, Object v2)
	{
		env.setObject(o0r, v0);
		env.setObject(o1r, v1);
		env.setObject(o2r, v2);
	}
}
