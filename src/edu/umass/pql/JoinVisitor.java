/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;

/**
 * Visitor for binding iterators
 */
public abstract class JoinVisitor
{
	/**
	 * Visits a standard binding iterator, such as an arithmetic operator.
	 *
	 * All classes that do not have their own specialised `visit' methods
	 * default here.
	 *
	 * @param it The iterator to visit
	 */
	public abstract void visitDefault(Join it);

	public void visitField(Field fld) { visitDefault(fld); }

	public void visitJavaType(Type.JAVA_TYPE jty) { visitDefault(jty); }

	public void visitInstantiation(Instantiation ins) { visitDefault(ins); }

	public void visitControlStructure(ControlStructure sp) { visitDefault(sp); }

	public void visitReduction(Reduction red) { visitControlStructure(red); }

	public void visitAnyBlock(AbstractBlock b) { visitControlStructure(b); }

	public void visitConjunctiveBlock(AbstractBlock.Conjunctive b) { visitAnyBlock(b); }

	public void visitDisjunctiveBlock(AbstractBlock.Disjunctive b) { visitAnyBlock(b); }

	public void visitNot(Not n) { visitControlStructure(n); }

	public void visitNoFail(NoFail b) { visitControlStructure(b); }

	public void visitBool(NoFail.Bool b) { visitNoFail(b); }

	public void visitSelectPath(SelectPath sp) { visitControlStructure(sp); }

	public void visitSchedule(Schedule sp) { visitControlStructure(sp); }

	// ----------------------------------------

	public void visitTrue(Join j) { visitDefault(j); }
	public void visitFalse(Join j) { visitDefault(j); }
	public void visitAdd(Join j) { visitDefault(j); }
	public void visitSub(Join j) { visitDefault(j); }
	public void visitMul(Join j) { visitDefault(j); }
	public void visitDiv(Join j) { visitDefault(j); }
	public void visitMod(Join j) { visitDefault(j); }
	public void visitNeg(Join j) { visitDefault(j); }
	public void visitBitInv(Join j) { visitDefault(j); }
	public void visitBitOr(Join j) { visitDefault(j); }
	public void visitBitXor(Join j) { visitDefault(j); }
	public void visitBitAnd(Join j) { visitDefault(j); }
	public void visitBitShl(Join j) { visitDefault(j); }
	public void visitBitShr(Join j) { visitDefault(j); }
	public void visitBitSshr(Join j) { visitDefault(j); }

	public void visitEq(Join j) { visitDefault(j); }
	public void visitEqEquals(Join j) { visitDefault(j); }
	public void visitNeq(Join j) { visitDefault(j); }
	public void visitNeqEquals(Join j) { visitDefault(j); }
	public void visitLt(Join j) { visitDefault(j); }
	public void visitLte(Join j) { visitDefault(j); }

	public void visitArrayLookup(Join j) { visitDefault(j); }
	public void visitMapLookup(Join j) { visitDefault(j); }
	public void visitPolyLookup(Join j) { visitDefault(j); }
	public void visitSetContains(Join j) { visitDefault(j); }
	public void visitRangeContains(Join j) { visitDefault(j); }
	public void visitPolySize(Join j) { visitDefault(j); }
	public void visitArraySize(Join j) { visitDefault(j); }
	public void visitSetSize(Join j) { visitDefault(j); }
	public void visitMapSize(Join j) { visitDefault(j); }

	public void visitTypeChar(Join j) { visitDefault(j); }
	public void visitCoerceChar(Join j) { visitDefault(j); }
	public void visitTypeShort(Join j) { visitDefault(j); }
	public void visitCoerceShort(Join j) { visitDefault(j); }
	public void visitTypeByte(Join j) { visitDefault(j); }
	public void visitCoerceByte(Join j) { visitDefault(j); }
	public void visitTypeBoolean(Join j) { visitDefault(j); }
	public void visitCoerceBoolean(Join j) { visitDefault(j); }
	public void visitCoerceFloat(Join j) { visitDefault(j); }
	public void visitTypeInt(Join j) { visitDefault(j); }
	public void visitTypeLong(Join j) { visitDefault(j); }

	public void visitCustomDecorator(CustomDecoratorJoin j) { throw new RuntimeException("Custom decorators are probably not things you want to visit in " + this.getClass()); }
}
