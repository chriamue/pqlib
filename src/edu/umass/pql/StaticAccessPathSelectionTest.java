/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

import edu.umass.pql.il.*;
import edu.umass.pql.opt.Optimizer;
import org.junit.*;
import java.util.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class StaticAccessPathSelectionTest extends TestBase
{
	static int[] data0 = { 10, 11, 12, 13, 14, 15, 21, 22, 24 };
	static int[] data1 = { 2, 12, 22, 32, 42 };
	static int[] data2 = new int[1024];
	static Set<Integer> set0 = new HashSet<Integer>();
	static Set<Integer> set1 = new HashSet<Integer>();
	static {
		set0.add(1);
		set0.add(11);
		set0.add(21);

		for (int i = 3; i < 1000; i++)
			set1.add(i);

		System.arraycopy(data0, 0, data2, 0, data0.length);
	};

	public static Join
	schedule(Join j, int ... vars)
	{
		return Optimizer.selectAccessPathRecursively(Env.EMPTY,
							     new VarSet(vars),
							     j,
							     false);
	}

	public static Join
	schedulePara(Join j, int ... vars)
	{
		return Optimizer.selectAccessPathRecursively(Env.EMPTY,
							     new VarSet(vars),
							     j,
							     true);
	}

	// --------------------------------------------------------------------------------
	// actual tests start here

	@Test
	public void
	testTrivial()
	{
		Join join = TRUE;
		testCount(1, schedule(join));
	}

	@Test
	public void
	testLookup()
	{
		setObjects(set0, null, null);
		Join join = CONTAINS(o0r, i1w);
		testCount(set0.size(), schedule(join, o0r));
	}

	@Test
	public void
	testOuterJoinLookup()
	{
		setObjects(set0, set1, null);
		Join join = ConjunctiveBlock(CONTAINS(o0r, i1w),
					     CONTAINS(o1r, i2w));
		Join result_join = schedule(join, o0r, o1r);
		testCount(set0.size() * set1.size(), result_join);
	}

	// INT
	@Test
	public void
	testDangerouslySlowIfWrong()
	{
		setObjects(set0, set1, null);
		Join join = ConjunctiveBlock(INT(i1w),
					     INT(i2w),
					     CONTAINS(o0r, i1w),
					     CONTAINS(o1r, i2w));
		Join result_join = schedule(join, o0r, o1r);
		testCount(set0.size() * set1.size(), result_join);
	}

	// nested INT
	@Test
	public void
	testDangerouslySlowIfWrongInBlock()
	{
		setObjects(set0, set1, null);
		Join join = ConjunctiveBlock(ConjunctiveBlock(INT(i1w),
							      CONTAINS(o0r, i1w)),
					     ConjunctiveBlock(INT(i2w),
							      CONTAINS(o1r, i2w)));
		Join result_join = schedule(join, o0r, o1r);
		testCount(set0.size() * set1.size(), result_join);
	}


	// advanced equality handling

	// @Test
	// public void
	// testFlipEQ()
	// {
	// 	Join join = ConjunctiveBlock(EQ_Int(i0r, i1r));
	// 	Join result_join = schedule(join, i1r);
	// 	testCount(1, result_join);
	// }

	// @Test
	// public void
	// testFlipEQComplex()
	// {
	// 	Join join = ConjunctiveBlock(EQ_Int(i0r, i1r),
	// 				     MUL_Int(i2r, i2r, i1r)
	// 				     );
	// 	Join result_join = schedule(join, i2r);
	// 	testCount(1, result_join);
	// }

}