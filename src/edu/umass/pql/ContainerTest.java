/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

public class ContainerTest extends TestBase
{

	@Test
	public void
	testAPI()
	{
		Join p = PQLFactory.CONTAINS(o0w, l1w);
		assertEquals(2, p.getArgsNr());
		assertEquals(1, p.getReadArgsNr());
		assertEquals(o0w, p.getArg(0));
		assertEquals(l1w, p.getArg(1));

		p.setArg(0, o1w);
		p.setArg(1, l0r);

		assertEquals(o1w, p.getArg(0));
		assertEquals(l0r, p.getArg(1));
	}

	@Test
	public void
	testSimpleContainment()
	{
		Set<Object> set = new HashSet<Object>();
		Object o0 = new Object();
		Object o1 = "foo";
		setObjects(set, o0, o1);
		set.add(o1);

		testEmpty(PQLFactory.CONTAINS(o0r, o1r));
		testCount(1, PQLFactory.CONTAINS(o0r, o2r));
	}

	Map<Integer, Object> m = new HashMap<Integer, Object>();

	String one = "one";
	String two = "two";
	String three = "three";

	{
		m.put(1, one);
		m.put(2, two);
		m.put(3, three);
		m.put(-3, three);
	}

	@Test
	public void
	testMapCheck()
	{
		setObjects(m, one, two);
		setInts(1, 2, 3);
		testEmpty(PQLFactory.LOOKUP(o0r, i0r, o2r));
		testCount(1, PQLFactory.LOOKUP(o0r, i0r, o1r));
	}

	@Test
	public void
	testMapLookup()
	{
		setObjects(m, null, null);
		setInts(1, 2, 3);
		assertSame(null, env.getObject(o1r));
		testCount(1, PQLFactory.LOOKUP(o0r, i0r, o1w));
		assertSame(one, env.getObject(o1r));
		testCount(1, PQLFactory.LOOKUP(o0r, i1r, o1w));
		assertSame(two, env.getObject(o1r));
	}

	@Test
	public void
	testMapSearchKey()
	{
		setObjects(m, one, three);
		testCount(0, PQLFactory.LOOKUP(o0r, i0w, o0r));
		testCount(1, PQLFactory.LOOKUP(o0r, i0w, o1r));
		testCount(2, PQLFactory.LOOKUP(o0r, i0w, o2r));
	}

	@Test
	public void
	testTrueMapSearchKey()
	{
		setObjects(m, one, three);
		testCount(2, PQLFactory.ConjunctiveBlock(PQLFactory.TRUE, PQLFactory.LOOKUP(o0r, i0w, o2r)));
	}

	@Test
	public void
	testMapIterate()
	{
		setObjects(m, one, two);
		testCount(4, PQLFactory.LOOKUP(o0r, i0w, o1w));
	}



	@Test public void testMapExists() { paratest("para_testMapExists"); }

	public void
	para_testMapExists()
	{
		setInts(0, 0, 0, 1);
		setObjects(m, null, one);
		testCount(1, PQLFactory.Exists(PQLFactory.LOOKUP(o0r, i0w, o1w),
					    PQLFactory.EQ_Object(o1r, o2r),
					    i4r, i3r));
		setObjects(m, null, "foobar");
		testCount(0, PQLFactory.Exists(PQLFactory.LOOKUP(o0r, i0w, o1w),
					    PQLFactory.EQ_Object(o1r, o2r),
					    i4r, i3r));
		setObjects(m, null, three);
		testCount(1, PQLFactory.Exists(PQLFactory.LOOKUP(o0r, i0w, o1w),
					    PQLFactory.EQ_Object(o1r, o2r),
					    i4r, i3r));
	}



	//================================================================================
	// Object array

	Object[] a_o = new Object[5];

	{
		a_o[1] = one;
		a_o[2] = two;
		a_o[3] = three;
		a_o[4] = three;
	}

	@Test
	public void
	testObjectArrayCheck()
	{
		setObjects(a_o, one, two);
		setInts(1, 2, 3);
		testEmpty(PQLFactory.ARRAY_LOOKUP_Object(o0r, i0r, o2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Object(o0r, i0r, o1r));
	}

	@Test
	public void
	testObjectArrayLookup()
	{
		setObjects(a_o, null, null);
		setInts(1, 2, 3);
		assertSame(null, env.getObject(o1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Object(o0r, i0r, o1w));
		assertSame(one, env.getObject(o1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Object(o0r, i1r, o1w));
		assertSame(two, env.getObject(o1r));
	}

	@Test
	public void
	testObjectArraySearchKey()
	{
		setObjects(a_o, one, three);
		testCount(0, PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o0r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o1r));
		testCount(2, PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o2r));
	}

	@Test
	public void
	testObjectArrayIterate()
	{
		setObjects(a_o, one, two);
		testCount(5, PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o1w));
	}

	@Test public void testObjectArrayExists() { paratest("para_testObjectArrayExists"); }

	public void
	para_testObjectArrayExists()
	{
		setObjects(a_o, null, one);
		setInts(0, 0, 0, 1);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o1w),
					    PQLFactory.EQ_Object(o1r, o2r),
					    i4r, i3r));
		setObjects(a_o, null, "foobar");
		testCount(0, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o1w),
					    PQLFactory.EQ_Object(o1r, o2r),
					    i4r, i3r));
		setObjects(a_o, null, three);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Object(o0r, i0w, o1w),
					    PQLFactory.EQ_Object(o1r, o2r),
					    i4r, i3r));

	}

	//================================================================================
	// Int array

	int[] a_i = new int[5];

	{
		a_i[1] = 10;
		a_i[2] = 11;
		a_i[3] = 12;
		a_i[4] = 11;
	}

	@Test
	public void
	testIntArrayCheck()
	{
		setObjects(a_i, null, null);
		setInts(1, 2, 10);
		testEmpty(PQLFactory.ARRAY_LOOKUP_Int(o0r, i0r, i1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Int(o0r, i0r, i2r));
	}

	@Test
	public void
	testIntArrayLookup()
	{
		setObjects(a_i, null, null);
		setInts(1, 2, 0);
		assertEquals(0, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Int(o0r, i0r, i2w));
		assertEquals(10, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Int(o0r, i1r, i2w));
		assertEquals(11, env.getInt(i2r));
	}

	@Test
	public void
	testIntArraySearchKey()
	{
		setObjects(a_i, null, null);
		setInts(-1, 10, 11);
		testCount(0, PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i0r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i1r));
		testCount(2, PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i2r));
	}

	@Test
	public void
	testIntArrayIterate()
	{
		setObjects(a_i, one, two);
		testCount(5, PQLFactory.ARRAY_LOOKUP_Int(o0r, __, __));
	}

	@Test public void testIntArrayExists() { paratest("para_testIntArrayExists"); }

	public void
	para_testIntArrayExists()
	{
		setObjects(a_i, null, null);
		setInts(0, 0, 10, 1);
		testCount(1, PQLFactory.Exists( PQLFactory.ARRAY_LOOKUP_Int(o0r, i0w, i1w),
					     PQLFactory.EQ_Int(i1r, i2r),
					    i4r, i3r));
		setInts(0, 0, 17);
		testCount(0, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Int(o0r, i0w, i1w),
					    PQLFactory.EQ_Int(i1r, i2r),
					    i4r, i3r));
		setInts(0, 0, 11);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Int(o0r, i0w, i1w),
					    PQLFactory.EQ_Int(i1r, i2r),
					    i4r, i3r));
	}

	//================================================================================
	// Long array

	long[] a_l = new long[5];

	{
		a_l[1] = 10000000000000l;
		a_l[2] = 11l;
		a_l[3] = 12l;
		a_l[4] = 11l;
	}

	@Test
	public void
	testLongArrayCheck()
	{
		setObjects(a_l, null, null);
		setInts(1, 2, 0);
		setLongs(0l, 0l, 11l);
		testEmpty(PQLFactory.ARRAY_LOOKUP_Long(o0r, i1r, l1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Long(o0r, i1r, l2r));
	}

	@Test
	public void
	testLongArrayLookup()
	{
		setObjects(a_l, null, null);
		setInts(1, 2, 0);
		setLongs(0, 0, 0);
		assertEquals(0l, env.getLong(l2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Long(o0r, i0r, l2w));
		assertEquals(10000000000000l, env.getLong(l2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Long(o0r, i1r, l2w));
		assertEquals(11l, env.getLong(l2r));
	}

	@Test
	public void
	testLongArraySearchKey()
	{
		setObjects(a_l, null, null);
		setLongs(-1l, 10000000000000l, 11l);
		testCount(0, PQLFactory.ARRAY_LOOKUP_Long(o0r, __, l0r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Long(o0r, __, l1r));
		testCount(2, PQLFactory.ARRAY_LOOKUP_Long(o0r, __, l2r));
	}

	@Test
	public void
	testPolyMapInt()
	{
		Map<String, Integer> m = new HashMap<String, Integer>();
		m.put("foo", 17);
		m.put("bar", 22);
		m.put("a", 1);
		m.put("b", 2);
		setObjects(null, null, new Object[] { new int[] { 1, 3, 17 },
						      new long[] { 2l, 19l },
						      new byte[] { 5, 12 },
						      new short[] { 1, 2, 3, 4, 10 },
						      new Object[] { 1, 9, 100 },
						      new char[] { 'a' },
						      new boolean[] { true, false },
						      m });
		setInts(0, 0, 10);
		Join j = PQLFactory.ConjunctiveBlock(PQLFactory.ARRAY_LOOKUP_Object(o2r, __, o1w),
						     PQLFactory.POLY_LOOKUP(o1r, o0w, i1w),
						     PQLFactory.LT_Int(i1r, i2r));
		testCount(14, j);
	}

	@Test
	public void
	testPolyMapFloat()
	{
		Map<String, Double> m = new HashMap<String, Double>();
		m.put("foo", 17.5);
		m.put("bar", 22.5);
		m.put("a", 1.5);
		m.put("b", 10.1);
		setObjects(null, null, new Object[] { new int[] { 1, 3, 17 },
						      new float[] { 2.1f, 9.8f },
						      new double[] { 5.5, 12.0 },
						      new Object[] { 1, 9, 100.1 },
						      m });
		setDoubles(0, 0, 10.0);
		Join j = PQLFactory.ConjunctiveBlock(PQLFactory.ARRAY_LOOKUP_Object(o2r, __, o1w),
						     PQLFactory.POLY_LOOKUP(o1r, o0w, d1w),
						     PQLFactory.LT_Double(d1r, d2r));
		testCount(8, j);
	}

	@Test
	public void
	testLongArrayIterate()
	{
		setObjects(a_l, one, two);
		testCount(5, PQLFactory.ARRAY_LOOKUP_Long(o0r, __, __));
	}

	@Test public void testLongArrayExists() { paratest("para_testLongArrayExists"); }

	public void
	para_testLongArrayExists()
	{
		setObjects(a_l, null, null);
		setLongs(0, 0, 10000000000000l);
		setInts(0, 0, 0, 1);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Long(o0r, i0w, l1w),
					    PQLFactory.EQ_Long(l1r, l2r),
					    i4r, i3r));
		setLongs(0, 0, 17l);
		testCount(0, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Long(o0r, i0w, l1w),
					    PQLFactory.EQ_Long(l1r, l2r),
					    i4r, i3r));
		setLongs(0, 0, 11l);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Long(o0r, i0w, l1w),
					    PQLFactory.EQ_Long(l1r, l2r),
					    i4r, i3r));
	}

	//================================================================================
	// Double array

	double[] a_d = new double[5];

	{
		a_d[1] = 0.125;
		a_d[2] = 1.25;
		a_d[3] = 7.75;
		a_d[4] = 1.25;
	}

	@Test
	public void
	testDoubleArrayCheck()
	{
		setObjects(a_d, null, null);
		setInts(1, 2, 0);
		setDoubles(0.0, 0.0, 1.25);
		testEmpty(PQLFactory.ARRAY_LOOKUP_Double(o0r, i1r, d1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Double(o0r, i1r, d2r));
	}

	@Test
	public void
	testDoubleArrayLookup()
	{
		setObjects(a_d, null, null);
		setInts(1, 2, 0);
		setDoubles(0.0, 0.0, 0.0);
		assertEquals(0.0, env.getDouble(d2r), 0.0);
		testCount(1, PQLFactory.ARRAY_LOOKUP_Double(o0r, i0r, d2w));
		assertEquals(0.125, env.getDouble(d2r), 0.0);
		testCount(1, PQLFactory.ARRAY_LOOKUP_Double(o0r, i1r, d2w));
		assertEquals(1.25, env.getDouble(d2r), 0.0);
	}

	@Test
	public void
	testDoubleArraySearchKey()
	{
		setObjects(a_d, null, null);
		setDoubles(-1.0, 0.125, 1.25);
		testCount(0, PQLFactory.ARRAY_LOOKUP_Double(o0r, __, d0r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Double(o0r, __, d1r));
		testCount(2, PQLFactory.ARRAY_LOOKUP_Double(o0r, __, d2r));
	}

	@Test
	public void
	testDoubleArrayIterate()
	{
		setObjects(a_d, one, two);
		testCount(5, PQLFactory.ARRAY_LOOKUP_Double(o0r, __, __));
	}

	@Test public void testDoubleArrayExists() { paratest("para_testDoubleArrayExists"); }

	public void
	para_testDoubleArrayExists()
	{
		setObjects(a_d, null, null);
		setDoubles(0.0, 0.0, 0.125);
		setInts(0, 0, 0, 1);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Double(o0r, i0w, d1w),
					    PQLFactory.EQ_Double(d1r, d2r),
					    i4r, i3r));
		setDoubles(0.0, 0.0, 17.125);
		testCount(0, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Double(o0r, i0w, d1w),
					    PQLFactory.EQ_Double(d1r, d2r),
					    i4r, i3r));
		setDoubles(0.0, 0.0, 1.25);
		testCount(1, PQLFactory.Exists(PQLFactory.ARRAY_LOOKUP_Double(o0r, i0w, d1w),
					    PQLFactory.EQ_Double(d1r, d2r),
					    i4r, i3r));
	}


	//================================================================================
	// Boolean array

	boolean[] a_boolean = new boolean[] { true, false, true, false };

	@Test
	public void
	testBooleanArrayCheck()
	{
		setObjects(a_boolean, null, null);
		setInts(1,
			1,  // wrong value at 1
			0); // right value at 1
		testEmpty(PQLFactory.ARRAY_LOOKUP_Boolean(o0r, i0r, i1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Boolean(o0r, i0r, i2r));
	}

	@Test
	public void
	testBooleanArrayLookup()
	{
		setObjects(a_boolean, null, null);
		setInts(0, 3, 0);
		assertEquals(0, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Boolean(o0r, i0r, i2w));
		assertEquals(1, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Boolean(o0r, i1r, i2w));
		assertEquals(0, env.getInt(i2r));
	}

	//================================================================================
	// Byte array

	byte[] a_byte = new byte[] { -17, 64, 32, 11 };

	@Test
	public void
	testByteArrayCheck()
	{
		setObjects(a_byte, null, null);
		setInts(1,
			11,  // wrong value at 1
			64); // right value at 1
		testEmpty(PQLFactory.ARRAY_LOOKUP_Byte(o0r, i0r, i1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Byte(o0r, i0r, i2r));
	}

	@Test
	public void
	testByteArrayLookup()
	{
		setObjects(a_byte, null, null);
		setInts(0, 2, 0);
		assertEquals(0, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Byte(o0r, i0r, i2w));
		assertEquals(-17, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Byte(o0r, i1r, i2w));
		assertEquals(32, env.getInt(i2r));
	}

	//================================================================================
	// Char array

	char[] a_char = new char[] { 'a', 'c', 'x', 'Z' };

	@Test
	public void
	testCharArrayCheck()
	{
		setObjects(a_char, null, null);
		setInts(1,
			'0',  // wrong value at 1
			'c'); // right value at 1
		testEmpty(PQLFactory.ARRAY_LOOKUP_Char(o0r, i0r, i1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Char(o0r, i0r, i2r));
	}

	@Test
	public void
	testCharArrayLookup()
	{
		setObjects(a_char, null, null);
		setInts(0, 2, 0);
		assertEquals(0, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Char(o0r, i0r, i2w));
		assertEquals('a', env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Char(o0r, i1r, i2w));
		assertEquals('x', env.getInt(i2r));
	}

	//================================================================================
	// Short array

	short[] a_short = new short[] { -1700, 64, 3200, 11 };

	@Test
	public void
	testShortArrayCheck()
	{
		setObjects(a_short, null, null);
		setInts(1,
			11, // wrong value at 1
			64);// right value at 1
		testEmpty(PQLFactory.ARRAY_LOOKUP_Short(o0r, i0r, i1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Short(o0r, i0r, i2r));
	}

	@Test
	public void
	testShortArrayLookup()
	{
		setObjects(a_short, null, null);
		setInts(0, 2, 0);
		assertEquals(0, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Short(o0r, i0r, i2w));
		assertEquals(-1700, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Short(o0r, i1r, i2w));
		assertEquals(3200, env.getInt(i2r));
	}

	//================================================================================
	// Float array

	float[] a_float = new float[] { -1.5f, 11.25f, 3200.0f, 11.0f };

	@Test
	public void
	testFloatArrayCheck()
	{
		setObjects(a_float, null, null);
		setInts(1, 0, 0);
		setDoubles(0.0,
			   11.0,   // wrong value at 1
			   11.25); // right value at 1
		testEmpty(PQLFactory.ARRAY_LOOKUP_Float(o0r, i0r, d1r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Float(o0r, i0r, d2r));
	}

	@Test
	public void
	testFloatArrayLookup()
	{
		setObjects(a_float, null, null);
		setInts(0, 1, 0);
		assertEquals(0, env.getInt(i2r));
		testCount(1, PQLFactory.ARRAY_LOOKUP_Float(o0r, i0r, d2w));
		assertEquals(-1.5f, env.getDouble(d2r), 0.0);
		testCount(1, PQLFactory.ARRAY_LOOKUP_Float(o0r, i1r, d2w));
		assertEquals(11.25f, env.getDouble(d2r), 0.0);
	}


	// ================================================================================
	// ranges (int)
	@Test
	public void
	testIntRangeContainment()
	{
		setInts(2, 3, 4);
		testCount(1, PQLFactory.INT_RANGE_CONTAINS(i0r, i2r, i0r));
		testCount(1, PQLFactory.INT_RANGE_CONTAINS(i0r, i2r, i1r));
		testCount(1, PQLFactory.INT_RANGE_CONTAINS(i0r, i2r, i2r));
	}

	@Test
	public void
	testIntRangeBorderlines()
	{
		setInts(2, Integer.MAX_VALUE - 1, Integer.MAX_VALUE);
		testCount(2, PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w));
		setInts(2, Integer.MIN_VALUE, Integer.MIN_VALUE + 1);
		testCount(2, PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i0w));
	}

	@Test
	public void
	testIntRangeIteration()
	{
		setInts(2, 3, 4);
		testCount(3, PQLFactory.INT_RANGE_CONTAINS(i0r, i2r, i1w));
	}

	@Test
	public void
	testIntRangeIntersection()
	{
		setInts(5, 0, 10);
		setLongs(1, 0, 7);
		// elements { 5, 6, 7 }
		testCount(3, PQLFactory.ConjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(i0r, i2r, i1w),
					PQLFactory.INT_RANGE_CONTAINS(l0r, l2r, i1r)
				}));
	}

	// ================================================================================
	// ranges (long)
	@Test
	public void
	testLongRangeContainment()
	{
		setLongs(10000000002l, 10000000003l, 10000000004l);
		testCount(1, PQLFactory.LONG_RANGE_CONTAINS(l0r, l2r, l0r));
		testCount(1, PQLFactory.LONG_RANGE_CONTAINS(l0r, l2r, l1r));
		testCount(1, PQLFactory.LONG_RANGE_CONTAINS(l0r, l2r, l2r));
	}

	@Test
	public void
	testLongRangeBorderlines()
	{
		setLongs(0l, Long.MAX_VALUE - 1, Long.MAX_VALUE);
		testCount(2, PQLFactory.LONG_RANGE_CONTAINS(l1r, l2r, l0w));
		setLongs(0l, Long.MIN_VALUE, Long.MIN_VALUE + 1);
		testCount(2, PQLFactory.LONG_RANGE_CONTAINS(l1r, l2r, l0w));
	}

	@Test
	public void
	testLongRangeIteration()
	{
		setLongs(10000000002l, 0l, 10000000004l);
		testCount(3, PQLFactory.LONG_RANGE_CONTAINS(l0r, l2r, l1w));
	}

	@Test
	public void
	testLongRangeIntersection()
	{
		setLongs(10000000005l, 0l, 10000000010l, 10000000001l, 10000000007l);
		// elements { 5, 6, 7 }
		testCount(3, PQLFactory.ConjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(l0r, l2r, l1w),
					PQLFactory.INT_RANGE_CONTAINS(l3r, l4r, l1r)
				}));
	}

	// ================================================================================
	// Container sizes
	@Test
	public void
	testSetSize()
	{
		Set<Object> set = new HashSet<Object>();
		set.add("foo");
		set.add("bar");

		Object container = set;
		int size = 2;
		Join bi = PQLFactory.SET_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testMapSize()
	{
		Object container = this.m;
		int size = 4;
		Join bi = PQLFactory.MAP_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testObjectArraySize()
	{
		Object container = a_o;
		int size = 5;
		Join bi = PQLFactory.OBJECT_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testIntArraySize()
	{
		Object container = a_i;
		int size = 5;
		Join bi = PQLFactory.INT_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testLongArraySize()
	{
		Object container = a_l;
		int size = 5;
		Join bi = PQLFactory.LONG_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	private void
	doTestSizeImpl(Object container, int size, Join bi)
	{
		setObjects(container, null, null);
		setInts(0, 0, 0);
		testCount(0, bi);
		assertEquals(0, env.getInt(i0r));
		bi.setArg(1, i0w);
		testCount(1, bi);
		assertEquals(size, env.getInt(i0r));
		bi.setArg(1, i0r);
		testCount(1, bi);
		assertEquals(size, env.getInt(i0r));
	}

	private void
	doTestSize(Object container, int size, Join default_join)
	{
		final Join dynamic_join = PQLFactory.POLY_SIZE(default_join.getArg(0), 
							       default_join.getArg(1));

		doTestSizeImpl(container, size, default_join);
		doTestSizeImpl(container, size, dynamic_join);
	}

	@Test public void testDynamicMultiSize() { paratest("para_testDynamicMultiSize"); }

	public void
	para_testDynamicMultiSize()
	{
		HashSet<Object> aux_set = new HashSet<Object>();
		aux_set.add("foo");
		aux_set.add("bar");
		aux_set.add(1);
		aux_set.add(new Object());

		HashSet<Object> set = new HashSet<Object>();
		set.add(aux_set);
		set.add(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
		set.add(new double[] { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0 });
		set.add(new Object[] { "one", "two", 3, 4, "five", "six" });


		setObjects(set, null, null);

		Join j = PQLFactory.Reduction(PQLFactory.Reductors.SET(i0r, o1w),
					      PQLFactory.CONTAINS(o0r, o2w),
					      PQLFactory.POLY_SIZE(o2r, i0w));
		testCount(1, j);
		Set<?> result = (Set<?>) env.getObject(o1r);
		assertEquals(4, result.size());
		assertTrue(result.contains(4));
		assertTrue(result.contains(6));
		assertTrue(result.contains(8));
		assertTrue(result.contains(10));
	}

	@Test
	public void
	testDoubleArraySize()
	{
		Object container = a_d;
		int size = 5;
		Join bi = PQLFactory.DOUBLE_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testBooleanArraySize()
	{
		Object container = a_boolean;
		int size = a_boolean.length;
		Join bi = PQLFactory.BOOLEAN_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testShortArraySize()
	{
		Object container = a_short;
		int size = a_short.length;
		Join bi = PQLFactory.SHORT_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testCharArraySize()
	{
		Object container = a_char;
		int size = a_char.length;
		Join bi = PQLFactory.CHAR_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testFloatArraySize()
	{
		Object container = a_float;
		int size = a_float.length;
		Join bi = PQLFactory.FLOAT_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}

	@Test
	public void
	testByteArraySize()
	{
		Object container = a_byte;
		int size = a_byte.length;
		Join bi = PQLFactory.BYTE_ARRAY_SIZE(o0r, i0r);
		doTestSize(container, size, bi);
	}
}
