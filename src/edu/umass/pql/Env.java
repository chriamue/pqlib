/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;
import java.util.HashMap;

/**
 * PQL environment:  contains variable bindings.
 * PQL queries, as well as several static helper functions.
 *
 * @author creichen@gmail.com
 * @version 0.1.0
 */
public final class Env
	implements VarConstants
{
	// storage
	public int[] v_int;
	public long[] v_long;
	public double[] v_double;
	public Object[] v_object;

	public static final int WILDCARD = encodeVar(TYPE_WILDCARD, 0);

	public static final Env EMPTY = new Env(0, new Object[] {});

	public Env(int flags, Object[] tables)
	{
		this.set_tables(flags, tables);
	}

	public Env()
	{
	}

	public Env
	copy()
	{
		Env retval = new Env();

		if (this.v_int != null) {
			retval.v_int = new int[this.v_int.length];
			System.arraycopy(this.v_int, 0, retval.v_int, 0, this.v_int.length);
		}

		if (this.v_long != null) {
			retval.v_long = new long[this.v_long.length];
			System.arraycopy(this.v_long, 0, retval.v_long, 0, this.v_long.length);
		}

		if (this.v_double != null) {
			retval.v_double = new double[this.v_double.length];
			System.arraycopy(this.v_double, 0, retval.v_double, 0, this.v_double.length);
		}

		if (this.v_object != null) {
			retval.v_object = new Object[this.v_object.length];
			System.arraycopy(this.v_object, 0, retval.v_object, 0, this.v_object.length);
		}

		return retval;
	}

	private void
	set_tables(int flags, Object[] tables)
	{
		int type = 0;
		for (Object table : tables) {
			while ((flags != 0)
			       && (0 == (flags & 1))) {
				flags >>= 1;
				++type;
			}

			if (flags == 0)
				throw new RuntimeException("Don't know what to do with table of type " + table.getClass() + " : missing flags");

			switch (type) {
			case TYPE_INT: this.v_int = (int []) table;
				break;

			case TYPE_LONG: this.v_long = (long []) table;
				break;

			case TYPE_DOUBLE: this.v_double = (double []) table;
				break;

			case TYPE_OBJECT: this.v_object = (Object []) table;
				break;

			default:
				throw new RuntimeException("Don't know how to initialise a table at shift offset " + type);
			}

			flags >>= 1;
			++type;
		}

		if (flags != 0)
			throw new RuntimeException("More flags than tables in the initialiser");
	}

	public static String
	showVarType(int type)
	{
		switch (type) {
		case TYPE_WILDCARD:	return "_";
		case TYPE_INT:		return "i";
		case TYPE_LONG:		return "l";
		case TYPE_DOUBLE:	return "d";
		case TYPE_OBJECT:	return "o";
		default:		return "{unknown-type?" + type + "?}";
		}
	}

	public static boolean
	isWildcard(int var)
	{
		final int ty = (var >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		return ty == TYPE_WILDCARD;
	}

	public static String
	showVar(int var)
	{
		if (isWildcard(var))
			return "_";

		final boolean read = (var & VAR_READ_FLAG) != 0;
		final int ty = (var >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		final int index = var >> VAR_INDEX_SHIFT;

		return  (read? "?" : "!") + showVarType(ty) + index;
	}

	public static int
	encodeVar(int table, int index)
	{
		return (index << VAR_INDEX_SHIFT) | ((table & VAR_TABLE_MASK) << VAR_TABLE_SHIFT);
	}

	/**
	 * Marks a variable as `final' (i.e., not used after this point)
	 */
	public static int
	setFinal(int var)
	{
		return var | VAR_FINAL_FLAG;
	}

	/**
	 * Marks a variable as not `final' (i.e., mark that the variable may be used after this point)
	 */
	public static int
	resetFinal(int var)
	{
		return var & ~VAR_FINAL_FLAG;
	}

	/**
	 * Test whether a variable is `final' (i.e., used after this point)
	 */
	public static boolean
	isFinal(int var)
	{
		return (var & VAR_FINAL_FLAG) == VAR_FINAL_FLAG;
	}

	public static int
	encodeReadVar(int table, int index)
	{
		return readVar ((index << VAR_INDEX_SHIFT) | ((table & VAR_TABLE_MASK) << VAR_TABLE_SHIFT));
	}

	public static int
	encodeWriteVar(int table, int index)
	{
		return writeVar ((index << VAR_INDEX_SHIFT) | ((table & VAR_TABLE_MASK) << VAR_TABLE_SHIFT));
	}

	public static int
	varType(int var)
	{
		return (var >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
	}

	public static int
	varIndex(int var)
	{
		return (var >> VAR_INDEX_SHIFT) & VAR_INDEX_MASK;
	}

	public static int
	readVar(int var)
	{
		return var | VAR_READ_FLAG;
	}

	public static int
	writeVar(int var)
	{
		return var & ~VAR_READ_FLAG;
	}

	public static boolean
	isReadVar(int var)
	{
		return (var & VAR_READ_FLAG) != 0;
	}

	public static boolean
	isWriteVar(int var)
	{
		return (var & VAR_READ_FLAG) == 0;
	}

	// --------------------------------------------------------------------------------
	// Getters

	private static final long
	long_value(Object obj)
	{
		if (obj instanceof Number)
			return ((Number)obj).longValue();
		else if (obj == Boolean.FALSE)
			return 0l;
		else if (obj == Boolean.TRUE)
			return 1l;
		else if (obj instanceof Character)
			return ((Character) obj).charValue();
		else if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue()? 1l : 0l;
		else throw new RuntimeException("Trying to coerce object of type `" + obj.getClass() + "'");
	}

	private static final int
	int_value(Object obj)
	{
		if (obj instanceof Number)
			return ((Number)obj).intValue();
		else if (obj == Boolean.FALSE)
			return 0;
		else if (obj == Boolean.TRUE)
			return 1;
		else if (obj instanceof Character)
			return ((Character) obj).charValue();
		else if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue()? 1 : 0;
		else throw new RuntimeException("Trying to coerce object of type `" + obj.getClass() + "'");

	}

	private static final double
	double_value(Object obj)
	{
		if (obj instanceof Number)
			return ((Number)obj).doubleValue();
		else if (obj == Boolean.FALSE)
			return 0.0;
		else if (obj == Boolean.TRUE)
			return 1.0;
		else if (obj instanceof Character)
			return ((Character) obj).charValue();
		else if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue()? 1.0 : 0.0;
		else throw new RuntimeException("Trying to coerce object of type `" + obj.getClass() + "'");

	}

	public static final boolean
	interpretBoolean(Object value)
	{
		if (value instanceof Boolean)
			return ((Boolean) value).booleanValue();
		else
			return ((Number)value).intValue() != 0? true : false;
	}

	public int
	getInt(int i)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		// fast path
		if (source_ty == TYPE_INT)
			return this.v_int[index];
		return (int) getLong(i);
	}

	public long
	getLong(int i)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		// fast path
		if (source_ty == TYPE_LONG)
			return this.v_long[index];
		switch (source_ty) {
		case TYPE_WILDCARD:	throw new RuntimeException("Trying to read wildcard");
		case TYPE_INT:		return this.v_int[index];
		case TYPE_LONG:		return this.v_long[index];
		case TYPE_DOUBLE:	return (long) this.v_double[index];
		case TYPE_OBJECT:	return long_value(this.v_object[index]);

		default: throw new RuntimeException("Unsupported coercion from type " + source_ty);
		}
	}

	public double
	getDouble(int i)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		// fast path
		if (source_ty == TYPE_DOUBLE)
			return this.v_double[index];
		switch (source_ty) {
		case TYPE_WILDCARD:	throw new RuntimeException("Trying to read wildcard");
		case TYPE_INT:		return this.v_int[index];
		case TYPE_LONG:		return this.v_long[index];
		case TYPE_DOUBLE:	return this.v_double[index];
		case TYPE_OBJECT:	return double_value(this.v_object[index]);

		default: throw new RuntimeException("Unsupported coercion from type " + source_ty);
		}
	}

	public Object
	getObject(int i)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;

		switch (source_ty) {
		case TYPE_WILDCARD:	throw new RuntimeException("Trying to read wildcard");
		case TYPE_INT:		return canonicalInteger(this.v_int[index]);
		case TYPE_LONG:		return canonicalLong(this.v_long[index]);
		case TYPE_DOUBLE:	return Double.valueOf(this.v_double[index]);
		case TYPE_OBJECT:	return this.v_object[index];

		default: throw new RuntimeException("Unexpected table ID " + source_ty);
		}
	}

	// ================================================================================
	// --------------------------------------------------------------------------------
	// Canonicalisation
	public static final int MAX_CANONICAL_ARRAY_SIZE = 65536;

	static HashMap<Integer, Integer> int_canonical_map = new HashMap<Integer, Integer>();
	static Integer[] int_canonical_array = new Integer[MAX_CANONICAL_ARRAY_SIZE];

	public static final Integer
	canonicalInteger(int i)
	{
		if (i >= -128 && i < 128)  // built-in canonicalisation
			return Integer.valueOf(i);
		if (i >= 0 && i < MAX_CANONICAL_ARRAY_SIZE) { // array canonicalisation
			if (int_canonical_array[i] != null)
				return int_canonical_array[i];
			synchronized (int_canonical_array) {
				if (int_canonical_array[i] != null)
					return int_canonical_array[i];
				int_canonical_array[i] = Integer.valueOf(i);
				return int_canonical_array[i];
			}
		} // Map canonicalisation
		Integer result = int_canonical_map.get(i);
		if (result == null)
			synchronized (int_canonical_map) {
				result = int_canonical_map.get(i);
				if (result == null)
					result = Integer.valueOf(i);
				int_canonical_map.put(i, result);
			}
		return result;
	}


	static HashMap<Long, Long> long_canonical_map = new HashMap<Long, Long>();
	static Long[] long_canonical_array = new Long[MAX_CANONICAL_ARRAY_SIZE];

	public static final Long
	canonicalLong(long l)
	{
		if (l >= -128l && l < 128l)  // built-in canonicalisation
			return Long.valueOf(l);
		if (l >= 0 && l < MAX_CANONICAL_ARRAY_SIZE) { // array canonicalisation
			int i = (int) l;
			if (long_canonical_array[i] != null)
				return long_canonical_array[i];
			synchronized (long_canonical_array) {
				if (long_canonical_array[i] != null)
					return long_canonical_array[i];
				long_canonical_array[i] = Long.valueOf(i);
				return long_canonical_array[i];
			}
		} // Map canonicalisation
		Long result = long_canonical_map.get(l);
		if (result == null)
			synchronized (long_canonical_map) {
				result = long_canonical_map.get(l);
				if (result == null)
					result = Long.valueOf(l);
				long_canonical_map.put(l, result);
			}
		return result;
	}


	// ================================================================================
	// --------------------------------------------------------------------------------
	// Setters

	public void
	setInt(int i, int v)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		// fast path
		if (source_ty == TYPE_INT)
			this.v_int[index] = v;
		else switch (source_ty) {
			case TYPE_WILDCARD:	break;
			case TYPE_INT:		this.v_int[index] = v;
				break;
			case TYPE_LONG:		this.v_long[index] = v;
				break;
			case TYPE_DOUBLE:	this.v_double[index] = 1.0 * v;
				break;
			case TYPE_OBJECT:	this.v_object[index] = canonicalInteger(v);
				break;

			default: throw new RuntimeException("Unsupported coercion to type " + source_ty);
			}
	}

	public void
	setLong(int i, long v)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		// fast path
		if (source_ty == TYPE_LONG)
			this.v_long[index] = v;
		else switch (source_ty) {
		case TYPE_WILDCARD:	break;
		case TYPE_INT:		this.v_int[index] = (int) v;
			break;
		case TYPE_LONG:		this.v_long[index] = v;
			break;
		case TYPE_DOUBLE:	this.v_double[index] = 1.0 * v;
			break;
		case TYPE_OBJECT:	this.v_object[index] = canonicalLong(v);
			break;

		default: throw new RuntimeException("Unsupported coercion to type " + source_ty);
		}
	}

	public void
	setDouble(int i, double v)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;
		// fast path
		if (source_ty == TYPE_DOUBLE)
			v_double[index] = v;
		else switch (source_ty) {
		case TYPE_WILDCARD:	break;
		case TYPE_INT:		this.v_int[index] = (int) v;
			break;
		case TYPE_LONG:		this.v_long[index] = (long) v;
			break;
		case TYPE_DOUBLE:	this.v_double[index] = v;
			break;
		case TYPE_OBJECT:	this.v_object[index] = Double.valueOf(v);
			break;
		default: throw new RuntimeException("Unsupported coercion from type " + source_ty);
		}
	}

	public void
	setObject(int i, Object v)
	{
		final int index = i >> VAR_INDEX_SHIFT;
		final int source_ty = (i >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;

		switch (source_ty) {
		case TYPE_WILDCARD:	break;
		case TYPE_INT:		this.v_int[index] = int_value(v);
			break;
		case TYPE_LONG:		this.v_long[index] = long_value(v);
			break;
		case TYPE_DOUBLE:	this.v_double[index] = double_value(v);
			break;
		case TYPE_OBJECT:	this.v_object[index] = v;
			break;

		default: throw new RuntimeException("Unexpected table ID " + source_ty);
		}
	}

	// --------------------------------------------------------------------------------
	// Unification primitives

	public boolean
	unifyInt(int i, int v)
	{
		if ((i & VAR_READ_FLAG) == 0) {
			this.setInt(i, v);
			return true;
		} else {
			return v == this.getInt(i);
		}
	}

	public boolean
	unifyLong(int i, long v)
	{
		if ((i & VAR_READ_FLAG) == 0) {
			this.setLong(i, v);
			return true;
		} else {
			return v == this.getLong(i);
		}
	}

	public boolean
	unifyDouble(int i, double v)
	{
		if ((i & VAR_READ_FLAG) == 0) {
			this.setDouble(i, v);
			return true;
		} else {
			return v == this.getDouble(i);
		}
	}

	public boolean
	unifyObject(int i, Object v)
	{
		if ((i & VAR_READ_FLAG) == 0) {
			this.setObject(i, v);
			return true;
		} else {
			return v == this.getObject(i);
		}
	}

	/**
	 * Coerces an object to an assignment-compatible primitive
	 *
	 * @param var The variable to read from
	 * @param type The type to coerce to
	 * @return A coercion-compatible object, if possible
	 * @throws ClassCastException on failure
	 */
	public Object
	getCoercedObject(int var, Class<?> type)
	{
		if (type == int.class || type == Integer.class)
			return this.getInt(var);

		if (type == long.class || type == Long.class)
			return this.getLong(var);

		if (type == double.class || type == Double.class)
			return this.getLong(var);

		if (type == double.class || type == Double.class)
			return this.getLong(var);

		if (type == float.class || type == Float.class)
			return Float.valueOf((float) this.getDouble(var));

		if (type == short.class || type == Short.class)
			return Short.valueOf((short) this.getInt(var));

		if (type == byte.class || type == Byte.class)
			return Byte.valueOf((byte) this.getInt(var));

		Object o = this.getObject(var);
		if (type == char.class || type == Character.class
		    && o instanceof Number)
			return Character.valueOf((char) ((Number)o).intValue());

		if (o == null)
			return o;

		if (type.isAssignableFrom(o.getClass()))
			return o;

		throw new ClassCastException();
	}

	// --------------------------------------------------------------------------------
	// Auxiliary: primitive extraction

	public float
	getFloat(int reg)
	{
		return (float) getDouble(reg);
	}

	public boolean
	getBoolean(int reg)
	{
		return getInt(reg) != 0;
	}

	public char
	getChar(int reg)
	{
		return (char) getInt(reg);
	}

	public short
	getShort(int reg)
	{
		return (short) getInt(reg);
	}

	public byte
	getByte(int reg)
	{
		return (byte) getInt(reg);
	}

	public boolean
	unifyByType(int type, int var, Object obj)
	{
		switch (type) {
		case TYPE_INT:
			return this.unifyInt(var, int_value(obj));
		case TYPE_LONG:
			return this.unifyObject(var, long_value(obj));
		case TYPE_DOUBLE:
			return this.unifyObject(var, double_value(obj));
		case TYPE_OBJECT:
			return this.unifyObject(var, obj);
		default:
			throw new RuntimeException();
		}
	}

	// --------------------------------------------------------------------------------
	/**
	 * Checks whether space has been allocated to fill a particular variable
	 */
	public boolean
	isValid(int var)
	{
		final int index = var >> VAR_INDEX_SHIFT;
		final int source_ty = (var >> VAR_TABLE_SHIFT) & VAR_TABLE_MASK;

		switch (source_ty) {
		case TYPE_WILDCARD:	return false;
		case TYPE_INT:		return this.v_int != null && index >= 0 && index < this.v_int.length;
		case TYPE_LONG:		return this.v_long != null && index >= 0 && index < this.v_long.length;
		case TYPE_DOUBLE:	return this.v_double != null && index >= 0 && index < this.v_double.length;
		case TYPE_OBJECT:	return this.v_object != null && index >= 0 && index < this.v_object.length;

		default: throw new RuntimeException("Unexpected table ID " + source_ty);
		}
	}

	public String
	toString()
	{
		StringBuffer buf = new StringBuffer();

		if (v_int != null) {
			buf.append("v_int = [");
			for (int i = 0; i < v_int.length; i++) {
				buf.append("i" + i + "=" + v_int[i]);
				if (i+1 < v_int.length)
					buf.append(", ");
			}
			buf.append("]\n");
		}

		if (v_long != null) {
			buf.append("v_long = [");
			for (int i = 0; i < v_long.length; i++) {
				buf.append("l" + i + "=" + v_long[i]);
				if (i+1 < v_long.length)
					buf.append(", ");
			}
			buf.append("]\n");
		}

		if (v_double != null) {
			buf.append("v_double = [");
			for (int i = 0; i < v_double.length; i++) {
				buf.append("d" + i + "=" + v_double[i]);
				if (i+1 < v_double.length)
					buf.append(", ");
			}
			buf.append("]\n");
		}

		if (v_object != null) {
			buf.append("v_object = [");
			for (int i = 0; i < v_object.length; i++) {
				buf.append("o" + i + "=" + v_object[i] + ((v_object[i] == null)? "" : (":" + v_object[i].getClass())));
				if (i+1 < v_object.length)
					buf.append(", ");
			}
			buf.append("]\n");
		}

		return buf.toString();
	}

	// ================================================================================

	public static final int DEFAULT_THREADS_NR = Runtime.getRuntime().availableProcessors();
	public static int THREADS_NR = getThreadsNr();


	private static final int
	getThreadsNr()
	{
		String ts = System.getProperty("PQL_THREADS");

		if (ts != null) {
			int retval = Integer.parseInt(ts);
			System.err.println("THREAD OVERRIDE: using " + retval + " / " + DEFAULT_THREADS_NR + " threads");
			return retval;
		} else
			return DEFAULT_THREADS_NR;
	}

	public static final void
	setThreadsNr(int nr)
	{
		THREADS_NR = nr;
	}
}
