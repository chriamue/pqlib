/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

import edu.umass.pql.il.*;
import org.junit.*;
import java.util.*;
import static org.junit.Assert.*;

public class AccessPathSelectionTest extends TestBase
{
	static ArrayList<MetaJoin>
	selectPathPermutations(Join... joins)
	{
		Join[] tmp = new Join[joins.length];
		ArrayList<MetaJoin> results = new ArrayList<MetaJoin>();
		tryPermutations(results, joins, tmp, 0);
		return results;
	}

	static Join[]
	copyArray(Join[] array)
	{
		Join[] fresh = new Join[array.length];
		System.arraycopy(array, 0, fresh, 0, array.length);
		return fresh;
	}

	static void
	tryPermutations(ArrayList<MetaJoin> accumulator, Join[] src, Join[] dest, int offset)
	{
		if (offset == dest.length) {
			accumulator.add(PQLFactory.Meta.SELECT_PATH(copyArray(dest)));
		} else {
			final int remaining_choices = dest.length - offset;
			for (int i = 0; i < remaining_choices; i++) {
				final Join choice = src[i];
				dest[offset] = choice;
				src[i] = src[remaining_choices - 1];
				tryPermutations(accumulator, src, dest, offset + 1);
				src[remaining_choices - 1] = src[i];
				src[i] = choice;
			}
		}
	}

	static int[] data0 = { 10, 11, 12, 13, 14, 15, 21, 22, 24 };
	static int[] data1 = { 2, 12, 22, 32, 42 };
	static int[] data2 = new int[1024];
	static Set<Integer> set0 = new HashSet<Integer>();
	static Set<Integer> set1 = new HashSet<Integer>();
	static {
		set0.add(1);
		set0.add(11);
		set0.add(21);

		for (int i = 3; i < 1000; i++)
			set1.add(i);

		System.arraycopy(data0, 0, data2, 0, data0.length);
	};

	private static String
	extractPathBody(String s)
	{
		final int prefix_offset = s.indexOf("]={");
		if (prefix_offset < 0)
			return null;
		String prefix = s.substring(0, prefix_offset + 3);
		String centre = s.substring(prefix_offset + 3);

		int brace_count = 0;
		int offset = 0;
		int new_offset;
		while ((new_offset = prefix.indexOf("{", offset)) > -1) {
			offset = new_offset + 1;
			assert new_offset >= 0;
			++brace_count;
		}

		offset = centre.length();
		while (brace_count-- > 0) {
			new_offset = centre.lastIndexOf("}", offset);
			offset = new_offset;
			assert offset >= 0;
		}

		return centre.substring(0, offset - 1).trim();
	}

	private static void
	assertScheduledBodyN(String expected, Join j, int n)
	{
		String actual = j.toString();
		while (n-- > 0)
			actual = extractPathBody(actual);
		assertEquals(expected, actual);
	}

	private static void
	assertScheduledBody(String expected, Join j)
	{
		assertScheduledBodyN(expected, j, 1);
	}


	public void
	schedule(Join join, int[] vars)
	{
		VarSet vs = new VarSet(vars);
		join.accept(new ReadFlagSetterVisitor(vs));
		join.reset(this.env);
	}

	// --------------------------------------------------------------------------------
	// actual tests start here

	@Test
	public void
	testNull()
	{
		Join join = PQLFactory.TRUE;
		schedule(join, new int[] {});
		assertEquals("TRUE()", join.toString());
	}

	@Test
	public void
	testScheduleOneTrivial()
	{
		MetaJoin select = PQLFactory.Meta.SELECT_PATH(PQLFactory.TRUE);
		Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
		schedule(join, new int[] {});
		assertEquals("%SCHEDULE { %SELECT_PATH[(TRUE())]={ TRUE(); }}", join.toString());
		testCount(1, join);
	}

	@Test
	public void
	testScheduleOne()
	{
		setInts(0, 1, 0);
		MetaJoin select = PQLFactory.Meta.SELECT_PATH(PQLFactory.LT_Int(i0w, i1w));
		Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
		schedule(join, new int[] { i0r, i1r });
		assertEquals("%SCHEDULE { %SELECT_PATH[(LT_Int(?i0, ?i1))]={ LT_Int(?i0, ?i1); }}", join.toString());
		testCount(1, join);
	}

	@Test
	public void
	testScheduleTwo()
	{
		setObjects(data2, set0, null);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.ARRAY_LOOKUP_Int(o0w, __, i1w),
							      PQLFactory.CONTAINS(o1w, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r });
			assertScheduledBody("CONTAINS(?o1; !i1);  ARRAY_LOOKUP_Int(?o0; __, ?i1);", join);
			testCount(2, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test
	public void
	testScheduleThree()
	{
		setObjects(data0, set1, null);
		setInts(0, 0, 5);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.ARRAY_LOOKUP_Int(o0w, i3w, i1w),
							      PQLFactory.LT_Int(i3w, i2w),
							      PQLFactory.CONTAINS(o1w, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i2w });
			assertScheduledBody("ARRAY_LOOKUP_Int(?o0; !i3, !i1);  LT_Int(?i3, ?i2);  CONTAINS(?o1; ?i1);", join);
			testCount(5, join);
		}
		assertEquals("permutations", 6, permutations);
	}

	@Test
	public void
	testBlock()
	{
		setObjects(data0, set1, null);
		setInts(0, 0, 5);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.ARRAY_LOOKUP_Int(o0w, i3w, i1w),
							      PQLFactory.ConjunctiveBlock(PQLFactory.LT_Int(i3w, i2w),
											  PQLFactory.LT_Int(i3w, i2w),
											  PQLFactory.LT_Int(i3w, i2w)),
							      PQLFactory.CONTAINS(o1w, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i2w });
			assertScheduledBody("ARRAY_LOOKUP_Int(?o0; !i3, !i1);  { LT_Int(?i3, ?i2);  LT_Int(?i3, ?i2);  LT_Int(?i3, ?i2); };  CONTAINS(?o1; ?i1);", join);
			testCount(5, join);
		}
		assertEquals("permutations", 6, permutations);
	}

	@Test
	public void
	testBlock2()
	{
		setObjects(data0, set1, null);
		setInts(0, 0, 5);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.ConjunctiveBlock(PQLFactory.ARRAY_LOOKUP_Int(o0w, i3w, i1w)),
							      PQLFactory.LT_Int(i3w, i2w),
							      PQLFactory.CONTAINS(o1w, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i2w });
			assertScheduledBody("{ ARRAY_LOOKUP_Int(?o0; !i3, !i1); };  LT_Int(?i3, ?i2);  CONTAINS(?o1; ?i1);", join);
			testCount(5, join);
		}
		assertEquals("permutations", 6, permutations);
	}

	@Test
	public void
	testDisjointBlock()
	{
		setObjects(data0, set1, null);

		setInts(0, 0, 5);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.CONTAINS(o1w, i0w), // large
							      PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i3w), // tiny
							      PQLFactory.DisjunctiveBlock(PQLFactory.ARRAY_LOOKUP_Int(o0w, l0w, i4w), // small
											  PQLFactory.INT_RANGE_CONTAINS(i1r, i2r, i4w) // tiny
											  ),
							      PQLFactory.LTE_Int(i3w, i4w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i2w, i1w });
			assertScheduledBody("INT_RANGE_CONTAINS(?i1, ?i2; !i3);  [ ARRAY_LOOKUP_Int(?o0; !l0, !i4) | INT_RANGE_CONTAINS(?i1, ?i2; !i4) ];  LTE_Int(?i3, ?i4);  CONTAINS(?o1; !i0);", join);
		}
		assertEquals("permutations", 24, permutations);
	}

	@Test
	public void
	testBool()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.Bool(PQLFactory.ARRAY_LOOKUP_Int(o0w, __, i1w), i4w),
							      PQLFactory.Bool(PQLFactory.CONTAINS(o1w, i1w), i4w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i1r });
			assertScheduledBody("Bool(!i4): ARRAY_LOOKUP_Int(?o0; __, ?i1);;;  Bool(?i4): CONTAINS(?o1; ?i1);;;", join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test
	public void
	testNot()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.Not(PQLFactory.ARRAY_LOOKUP_Int(o0w, __, i1w)),
							      PQLFactory.Bool(PQLFactory.CONTAINS(o1w, i1w), i4w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i1w });
			assertScheduledBody("Not(): ARRAY_LOOKUP_Int(?o0; __, ?i1);;;  Bool(!i4): CONTAINS(?o1; ?i1);;;", join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test
	public void
	testNotBlock()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5);

		int permutations = 0;
		final MetaJoin select = PQLFactory.Meta.SELECT_PATH(new Join[] {
				PQLFactory.EQ_Int(l0w, i1w),
				PQLFactory.Not(PQLFactory.ARRAY_LOOKUP_Int(o0w, __, i1w)),
				PQLFactory.Bool(PQLFactory.CONTAINS(o1w, i1w), i4w),
			});

		Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
		schedule(join, new int[] { o0r, o1r, l0w });
		assertScheduledBody("EQ_Int(?l0; !i1);  Not(): ARRAY_LOOKUP_Int(?o0; __, ?i1);;;  Bool(!i4): CONTAINS(?o1; ?i1);;;", join);
		testCount(1, join);
	}

	@Test
	public void
	testBasic4()
	{
		setObjects(data0, set1, null);
		setInts(0, 0, 5, 0, 10000);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.ARRAY_LOOKUP_Int(o0w, __, i1w),
							      PQLFactory.CONTAINS(o1w, i1w),
							      PQLFactory.INT_RANGE_CONTAINS(i3r, i4r, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0r, o1r, i3w, i4w });
			assertScheduledBody("ARRAY_LOOKUP_Int(?o0; __, !i1);  INT_RANGE_CONTAINS(?i3, ?i4; ?i1);  CONTAINS(?o1; ?i1);", join);
			testCount(9, join);
		}
		assertEquals("permutations", 6, permutations);
	}

	@Test
	public void
	testBasic5()
	{
		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.BYTE(i0w),
							      PQLFactory.BYTE(i1w),
							      PQLFactory.MUL_Int(i0w, i0w, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { });
			assertScheduledBody("BYTE(!i0);  MUL_Int(?i0, ?i0; !i1);  BYTE(?i1);", join);
		}
		assertEquals("permutations", 6, permutations);
	}

	@Test
	public void
	testSelectivity()
	{
		setObjects(data0, set0, null);
		setInts(0, 11, 5, 0, 10000);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.JAVA_TYPE(Integer.class, i1r),
							      PQLFactory.CONTAINS(o1w, i1w))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o1w, i1w });
			assertScheduledBody("JAVA_TYPE<Integer>(?i1);  CONTAINS(?o1; ?i1);", join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	// ================================================================================
	// reductions

	@Test public void testSimpleReduction() { paratest("para_testSimpleReduction"); }
	public void
	para_testSimpleReduction()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5, 0, 10000);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i0w, i1r),
										PQLFactory.CONTAINS(o1w, i2w),
										PQLFactory.Bool(PQLFactory.INT_RANGE_CONTAINS(i3w, i4w, i2w), i0w)),
							      PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i5w, i1r),
										PQLFactory.CONTAINS(o1w, i6w),
										PQLFactory.Bool(PQLFactory.JAVA_TYPE(Integer.class, i6r), i5w)))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0w, o1w, i3w, i4w });
			assertScheduledBody("Reduce[FORALL(?i0): !i1]: { CONTAINS(?o1; !i2);  Bool(!i0): INT_RANGE_CONTAINS(?i3, ?i4; ?i2);;; };"
					    + "  Reduce[FORALL(?i5): ?i1]: { CONTAINS(?o1; !i6);  Bool(!i5): JAVA_TYPE<Integer>(?i6);;; };",
					    join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test public void testFusedReduction() { paratest("para_testFusedReduction"); }
	public void
	para_testFusedReduction()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5, 0, 10000);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.Reduction(new Reductor[] { PQLFactory.Reductors.FORALL(i0w, i1r), PQLFactory.Reductors.MAP(l0w, i2w, o2r) },
										PQLFactory.CONTAINS(o1w, i2w),
										PQLFactory.MUL_Int(i2w, i2w, l0w),
										PQLFactory.Bool(PQLFactory.INT_RANGE_CONTAINS(i3w, i4w, i2w), i0w)),
							      PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i5w, i1r),
										PQLFactory.CONTAINS(o1w, i6w),
										PQLFactory.Bool(PQLFactory.JAVA_TYPE(Integer.class, i6r), i5w)))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0w, o1w, i3w, i4w });
			assertScheduledBody("Reduce[FORALL(?i0): !i1; MAP(?l0, ?i2): !o2]: { CONTAINS(?o1; !i2);  MUL_Int(?i2, ?i2; !l0);  Bool(!i0): INT_RANGE_CONTAINS(?i3, ?i4; ?i2);;; };"
					    + "  Reduce[FORALL(?i5): ?i1]: { CONTAINS(?o1; !i6);  Bool(!i5): JAVA_TYPE<Integer>(?i6);;; };",
					    join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test public void testFusedDefaultMap() { paratest("para_testFusedDefaultMap"); }
	public void
	para_testFusedDefaultMap()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5, 0, 10000);

		int permutations = 0;
		for (MetaJoin select : selectPathPermutations(PQLFactory.Reduction(new Reductor[] { PQLFactory.Reductors.FORALL(i1w, i0r), PQLFactory.Reductors.DEFAULT_MAP(l1w, l0w, i2w, o2r) },
										PQLFactory.CONTAINS(o1w, i2w),
										PQLFactory.MUL_Int(i2w, i2w, l0w),
										PQLFactory.Bool(PQLFactory.INT_RANGE_CONTAINS(i3w, i4w, i2w), i1w)),
							      PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i5w, i0r),
										PQLFactory.CONTAINS(o1w, i6w),
										PQLFactory.Bool(PQLFactory.JAVA_TYPE(Integer.class, i6r), i5w)))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0w, o1w, i3w, i4w, l1w });
			assertScheduledBody("Reduce[FORALL(?i1): !i0; DEFAULT_MAP(?l1; ?l0, ?i2): !o2]: { CONTAINS(?o1; !i2);  MUL_Int(?i2, ?i2; !l0);  Bool(!i1): INT_RANGE_CONTAINS(?i3, ?i4; ?i2);;; };"
					    + "  Reduce[FORALL(?i5): ?i0]: { CONTAINS(?o1; !i6);  Bool(!i5): JAVA_TYPE<Integer>(?i6);;; };",
					    join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test public void testFusedNestedMap() { paratest("para_testFusedNestedMap"); }
	public void
	para_testFusedNestedMap()
	{
		setObjects(data0, set0, null);
		setInts(0, 0, 5, 0, 10000);

		int permutations = 0;
		Reductor map = PQLFactory.Reductors.MAP(l0w, Reductor.INPUT_FROM_INNER_REDUCTOR, o2r);
		Reductor set = PQLFactory.Reductors.SET(i2w, i5w);
		map.setInnerReductor(set);
		for (MetaJoin select : selectPathPermutations(PQLFactory.Reduction(new Reductor[] { PQLFactory.Reductors.FORALL(i0w, i1r), map },
										PQLFactory.CONTAINS(o1w, i2w),
										PQLFactory.MUL_Int(i2w, i2w, l0w),
										PQLFactory.Bool(PQLFactory.INT_RANGE_CONTAINS(i3w, i4w, i2w), i0w)),
							      PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i5w, i1r),
										PQLFactory.CONTAINS(o1w, i6w),
										PQLFactory.Bool(PQLFactory.JAVA_TYPE(Integer.class, i6r), i5w)))) {
			++permutations;

			Join join = PQLFactory.Meta.SCHEDULE(select.join(), select);
			schedule(join, new int[] { o0w, o1w, i3w, i4w });
			assertScheduledBody("Reduce[FORALL(?i0): !i1; MAP(?l0, SET(?i2): __): !o2]: { CONTAINS(?o1; !i2);  MUL_Int(?i2, ?i2; !l0);  Bool(!i0): INT_RANGE_CONTAINS(?i3, ?i4; ?i2);;; };"
					    + "  Reduce[FORALL(?i5): ?i1]: { CONTAINS(?o1; !i6);  Bool(!i5): JAVA_TYPE<Integer>(?i6);;; };",
					    join);
			testCount(1, join);
		}
		assertEquals("permutations", 2, permutations);
	}

	@Test public void testNestedPathSelection() { paratest("para_testNestedPathSelection"); }
	public void
	para_testNestedPathSelection()
	{
		Set<Set<Integer>> setofsets = new HashSet<Set<Integer>>();
		setofsets.add(set0);
		setofsets.add(set1);
		setObjects(null, setofsets, null);
		setInts(0, 0, 0, 5, 10000);
		int permutations = 0;

		for (MetaJoin inner_select : selectPathPermutations(PQLFactory.CONTAINS(o1r, o2r),
								    PQLFactory.CONTAINS(o2r, i5r),
								    PQLFactory.INT_RANGE_CONTAINS(i2r, i3r, i5r))) {
			final Join inner_schedule = PQLFactory.Meta.SCHEDULE(inner_select.join(), inner_select);

			for (MetaJoin outer_select : selectPathPermutations(PQLFactory.Reduction(PQLFactory.Reductors.SET(i5r, o0w), inner_schedule),
									    PQLFactory.INT_RANGE_CONTAINS(i3r, i4r, i3r),
									    PQLFactory.JAVA_TYPE(Integer.class, i3r))) {
				++permutations;
				final Join join = PQLFactory.Meta.SCHEDULE(outer_select.join(), outer_select);

				setObjects(null, setofsets, null);
				schedule(join, new int[] { o1w, i2r, i3w, i4w });
				testCount(1, join);
				Set<?> resultset = (Set<?>) env.getObject(o0r);
				assertEquals("result set size", 4, resultset.size());
				assertTrue(resultset.contains(1));
				assertTrue(resultset.contains(3));
				assertTrue(resultset.contains(4));
				assertTrue(resultset.contains(5));

				inner_select.unschedule();
				assertScheduledBodyN("INT_RANGE_CONTAINS(?i3, ?i4; ?i3);  JAVA_TYPE<Integer>(?i3);  Reduce[SET(?i5): !o0]: "
						     + inner_schedule.toString() + ";",
						     join, 1);
			}
		}
		assertEquals("permutations", 36, permutations);
	}

	@Test public void testNestedPathSelection2() { paratest("para_testNestedPathSelection2"); }
	public void
	para_testNestedPathSelection2()
	{
		Set<Set<Integer>> setofsets = new HashSet<Set<Integer>>();
		Set<Integer> rangeset = new HashSet<Integer>();
		setofsets.add(set0);
		setofsets.add(set1);
		rangeset.add(1);
		rangeset.add(10);
		setObjects(null, setofsets, rangeset);
		setLongs(1024, 1023, 1);
		int permutations = 0;

		final MetaJoin select =
			PQLFactory.Meta.SELECT_PATH(PQLFactory.CONTAINS(o1r, o3r),
						    PQLFactory.CONTAINS(o3r, i5r),
						    PQLFactory.INT_RANGE_CONTAINS(i2r, i3r, i5r),
						    PQLFactory.CONTAINS(o2r, i0r),
						    PQLFactory.MUL_Int(i0r, l0r, i2r),
						    PQLFactory.ADD_Int(i2r, l1r, i3r));

		final Join sjoin = PQLFactory.Meta.SCHEDULE(select.join(), select);
		final Join join = PQLFactory.Reduction(PQLFactory.Reductors.SET(i0r, o0w), sjoin);

		schedule(join, new int[] { o1w, o2w, l0r, l1r, l2r });
		//System.err.print(join);

	}
}