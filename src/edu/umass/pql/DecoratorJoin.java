/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

public abstract class DecoratorJoin extends ControlStructure
{
	protected Join body = null;

	public
	DecoratorJoin(Join body)
	{
		this.body = body;
	}

	@Override
	public int getComponentsNr() { return this.body == null ? 0 : 1; }
	@Override
	public Join getComponentInternal(int __) { return body; }
	@Override
	public void setComponentInternal(int __, Join new_body) { this.body = new_body; }

	@Override
	public DecoratorJoin
	copyRecursively()
	{
		DecoratorJoin mj = (DecoratorJoin) super.copy();
		mj.body = this.body == null ? null : this.body.copyRecursively();
		mj.postCopy();
		return mj;
	}

	protected void
	postCopy() {}; // copy/update local fields after a `copyRecursively' as needed

	// ----------------------------------------

	@Override
	public void
	addReadDependencies(VarSet set, VarSet exclusions)
	{
		if (this.body != null)
			this.body.addReadDependencies(set, exclusions);
	}

	@Override
	public void
	addWriteDependencies(VarSet set)
	{
		if (this.body != null)
			this.body.addWriteDependencies(set);
	}

	// --------------------------------------------------------------------------------
	// info

	@Override
	public double
	getSelectivity(VarInfo vi, Env env)
	{
		return this.body.getSelectivity(vi, env);
	}

	@Override
	public double
	getSize(VarInfo vi, Env env)
	{
		return this.body.getSize(vi, env);
	}


	@Override
	public double
	getAccessCost(VarInfo vi, Env env)
	{
		return this.body.getAccessCost(vi, env);
	}

	// --------------------------------------------------------------------------------
	// sequential access

	@Override
	public boolean
	next(Env env)
	{
		return this.body.next(env);
	}

	@Override
	public void
	reset(Env env)
	{
		this.body.reset(env);
	}

	// --------------------------------------------------------------------------------
	// parallel access

	@Override
	public void
	resetForRandomAccess(Env env)
	{
		this.body.resetForRandomAccess(env);
	}

	@Override
	public int
	getFanout(Env env)
	{
		return this.body.getFanout(env);
	}

	@Override
	public boolean
	hasRandomAccess()
	{
		return this.body.hasRandomAccess();
	}

	@Override
	public void
	moveToIndex(Env env, int offset)
	{
		this.body.moveToIndex(env, offset);
	}

	@Override
 	public int
	getAtIndex(Env env, int offset)
	{
		return this.body.getAtIndex(env, offset);
	}
}