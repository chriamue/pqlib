/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

/**
 * Control structures are binding iterators without regular arguments, but with inner
 * blocks.
 *
 * @author creichen
 */
public abstract class ControlStructure extends Join
{
	public int getArgsNr() { return 0; }
	public int getReadArgsNr() { return 0;}
	public void setArg(int i, int v) { throw new RuntimeException("No arguments allowed in control structure"); };
	public int getArg(int i) { throw new RuntimeException("No arguments allowed in control structure"); };

	/**
	 * Determine number of inner blocks
	 *
	 * @return nuber of inner blocks
	 */
	public abstract int
	getComponentsNr();

	public Join
	getComponent(int i)
	{
		if (i < 0 || i >= this.getComponentsNr())
			throw new RuntimeException("Invalid block number: " + i + " from " + this);
		return this.getComponentInternal(i);
	}

	public void
	setComponent(int i, Join bi)
	{
		if (i < 0 || i >= this.getComponentsNr())
			throw new RuntimeException("Invalid block number: " + i + " from " + this);
		setComponentInternal(i, bi);
	}

	protected abstract Join
	getComponentInternal(int i);

	protected abstract void
	setComponentInternal(int i, Join bi);

	@Override
	public void
	addReadDependencies(VarSet set, VarSet exclusions)
	{
		if (exclusions == null)
			exclusions = new VarSet();

		for (int i = 0; i < this.getComponentsNr(); i++)
			this.getComponent(i).addReadDependencies(set, exclusions);

		super.addReadDependencies(set, exclusions);
	}

	@Override
	public void
	addActualReads(VarSet set, VarSet exclusions)
	{
		if (exclusions == null)
			exclusions = new VarSet();

		for (int i = 0; i < this.getComponentsNr(); i++)
			this.getComponent(i).addActualReads(set, exclusions);

		super.addActualReads(set, exclusions);
	}

	@Override
	public void
	addAllVariables(VarSet set)
	{
		super.addAllVariables(set);
		for (int i = 0; i < this.getComponentsNr(); i++)
			this.getComponent(i).addAllVariables(set);
	}


	@Override
	public void
	accept(JoinVisitor visitor)
	{
		throw new RuntimeException("Control structure forgot to override `accept'");
	}

	@Override
	public boolean
	accessPathSelected()
	{
		for (int i = 0; i < this.getComponentsNr(); i++)
			if (!this.getComponentInternal(i).accessPathSelected())
				return false;
		return true;
	}

	@Override
	public String
	getConstructorName()
	{
		return null;
	}

	@Override
	public String
	toString()
	{
		StringBuffer buf = new StringBuffer(super.toString());
		buf.append(": ");
		for (int i = 0; i < this.getComponentsNr(); i++) {
			buf.append(this.getComponent(i));
			buf.append(";;");
			if (i < this.getComponentsNr() - 1)
				buf.append(" ");
		}
		return buf.toString();
	}
}
