/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

/**
 * Custom linear-access decorator, for use with custom-generated nonparallel joins that provide an
 * optimised version of the semantics of the join the decorator wraps.
 */
public abstract class CustomDecoratorJoin extends DecoratorJoin
{
	protected int state = 0;

	public
	CustomDecoratorJoin(Join body)
	{
		super(body);
	}

	@Override
	public DecoratorJoin
	copyRecursively()
	{
		CustomDecoratorJoin mj = (CustomDecoratorJoin) super.copyRecursively();
		return mj;
	}

	@Override
	public void
	accept(JoinVisitor v)
	{
		v.visitCustomDecorator(this);
	}

	// --------------------------------------------------------------------------------
	// Components

	@Override
	public Join
	getComponent(int nr)
	{
		return ((ControlStructure)this.body).getComponent(nr);
	}

	@Override
	public void
	setComponent(int nr, Join j)
	{
		if (this.body instanceof ControlStructure)
			((ControlStructure)this.body).setComponent(nr, j);
		else
			throw new UnsupportedOperationException();
	}

	@Override
	public int
	getComponentsNr()
	{
		if (this.body instanceof ControlStructure)
			return ((ControlStructure)this.body).getComponentsNr();
		else
			return 0;
	}

	// --------------------------------------------------------------------------------
	// sequential access

	@Override
	public abstract boolean
	next(Env env);

	@Override
	public void
	reset(Env env)
	{
		this.state = 0;
	}

	// --------------------------------------------------------------------------------
	// parallel access

	@Override
	public boolean
	hasRandomAccess()
	{
		return false;
	}

	public Object result; // result variable; may or may not be used
}