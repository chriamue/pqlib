/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.util.*;
import org.junit.*;
import java.lang.reflect.Array;
import static org.junit.Assert.*;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;

public class RandomAccessTest extends TestBase
{

	@Test
	public void
	testTruthConstants()
	{
		checkNoRandomAccess(PQLFactory.TRUE);
		checkNoRandomAccess(PQLFactory.FALSE);
	}

	@Test
	public void
	testArithmetic()
	{
		checkNoRandomAccess(PQLFactory.ADD_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.ADD_Long(l0r, l1r, l2w));
		checkNoRandomAccess(PQLFactory.ADD_Double(d0r, d1r, d2w));

		checkNoRandomAccess(PQLFactory.SUB_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.SUB_Long(l0r, l1r, l2w));
		checkNoRandomAccess(PQLFactory.SUB_Double(d0r, d1r, d2w));

		checkNoRandomAccess(PQLFactory.MUL_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.MUL_Long(l0r, l1r, l2w));
		checkNoRandomAccess(PQLFactory.MUL_Double(d0r, d1r, d2w));

		checkNoRandomAccess(PQLFactory.DIV_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.DIV_Long(l0r, l1r, l2w));
		checkNoRandomAccess(PQLFactory.DIV_Double(d0r, d1r, d2w));

		checkNoRandomAccess(PQLFactory.MOD_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.MOD_Long(l0r, l1r, l2w));

		checkNoRandomAccess(PQLFactory.NEG_Int(i0r, i2w));
		checkNoRandomAccess(PQLFactory.NEG_Long(l0r, l2w));
		checkNoRandomAccess(PQLFactory.NEG_Double(d0r, d2w));

		checkNoRandomAccess(PQLFactory.BITINV_Int(i0r, i2w));
		checkNoRandomAccess(PQLFactory.BITINV_Long(l0r, l2w));

		checkNoRandomAccess(PQLFactory.BITOR_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.BITOR_Long(l0r, l1r, l2w));

		checkNoRandomAccess(PQLFactory.BITAND_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.BITAND_Long(l0r, l1r, l2w));

		checkNoRandomAccess(PQLFactory.BITXOR_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.BITXOR_Long(l0r, l1r, l2w));

		checkNoRandomAccess(PQLFactory.BITSHL_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.BITSHL_Long(l0r, l1r, l2w));

		checkNoRandomAccess(PQLFactory.BITSHR_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.BITSHR_Long(l0r, l1r, l2w));

		checkNoRandomAccess(PQLFactory.BITSSHR_Int(i0r, i1r, i2w));
		checkNoRandomAccess(PQLFactory.BITSSHR_Long(l0r, l1r, l2w));
	}

	@Test
	public void
	testComparisons()
	{
		checkNoRandomAccess(PQLFactory.EQ_Int(i0r, i1r));
		checkNoRandomAccess(PQLFactory.EQ_Long(l0r, l1r));
		checkNoRandomAccess(PQLFactory.EQ_Double(d0r, d1r));
		checkNoRandomAccess(PQLFactory.EQ_Object(o0r, o1r));
		checkNoRandomAccess(PQLFactory.EQ_String(o0r, o1r));

		checkNoRandomAccess(PQLFactory.NEQ_Int(i0r, i1r));
		checkNoRandomAccess(PQLFactory.NEQ_Long(l0r, l1r));
		checkNoRandomAccess(PQLFactory.NEQ_Double(d0r, d1r));
		checkNoRandomAccess(PQLFactory.NEQ_Object(o0r, o1r));
		checkNoRandomAccess(PQLFactory.NEQ_String(o0r, o1r));

		checkNoRandomAccess(PQLFactory.LT_Int(i0r, i1r));
		checkNoRandomAccess(PQLFactory.LT_Long(l0r, l1r));
		checkNoRandomAccess(PQLFactory.LT_Double(d0r, d1r));

		checkNoRandomAccess(PQLFactory.LTE_Int(i0r, i1r));
		checkNoRandomAccess(PQLFactory.LTE_Long(l0r, l1r));
		checkNoRandomAccess(PQLFactory.LTE_Double(d0r, d1r));
	}


	@Test
	public void
	testSizes()
	{
		checkNoRandomAccess(PQLFactory.SET_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.MAP_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.INT_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.LONG_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.SHORT_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.CHAR_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.BOOLEAN_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.BYTE_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.FLOAT_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.DOUBLE_ARRAY_SIZE(o0r, i1w));
		checkNoRandomAccess(PQLFactory.OBJECT_ARRAY_SIZE(o0r, i1w));
	}

	int x;
	public RandomAccessTest(){}

	@Test
	public void
	testReflexive() throws Throwable
	{
		checkNoRandomAccess(PQLFactory.FIELD(RandomAccessTest.class, "x", o0r, i1w));
		checkNoRandomAccess(PQLFactory.INSTANTIATE(RandomAccessTest.class.getConstructor(), new int[] {}, o0w));
	}

	@Test
	public void
	testCoercions()
	{
		checkNoRandomAccess(PQLFactory.COERCE_Boolean(o0r, i1w));
		checkNoRandomAccess(PQLFactory.COERCE_Byte(o0r, i1w));
		checkNoRandomAccess(PQLFactory.COERCE_Short(o0r, i1w));
		checkNoRandomAccess(PQLFactory.COERCE_Char(o0r, i1w));
		checkNoRandomAccess(PQLFactory.COERCE_Float(o0r, i1w));
	}

	@Test
	public void
	testIntRange()
	{
		setInts(17, 31, 0);
		checkParallel(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w),
			      (31 - 17) + 1, new int[] { i2r }, new Object[] {
				      17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
			      }, true, true);
	}

	@Test
	public void
	testLongRange()
	{
		setLongs(100000000017l, 100000000032l, 0l);
		checkParallel(PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
			      (32 - 17) + 1, new int[] { l2r }, new Object[] {
				      100000000017l,100000000018l,100000000019l,100000000020l,
				      100000000021l,100000000022l,100000000023l,100000000024l,
				      100000000025l,100000000026l,100000000027l,100000000028l,
				      100000000029l,100000000030l,100000000031l,100000000032l
			      }, true, true);
	}

	final static Object[] t_keys =   new Object[] { 1, 42l,           "shinji", 4.47f,      'x'};
	final static Object[] t_values = new Object[] { 8, new Object(),  "flurb",  new int[2], Integer.class};

	public void
	testLookup(Map<Object, Object> map)
	{
		setObjects(map, null, null);
		Object[] expected = new Object[t_keys.length * 2];
		for (int i = 0; i < t_keys.length; i++) {
			map.put(t_keys[i], t_values[i]);
			expected[i * 2] = t_keys[i];
			expected[i * 2 + 1] = t_values[i];
		}
		checkParallel(PQLFactory.LOOKUP(o0r, o1w, o2w),
			      UNKNOWN_COUNT, new int[] { o1r, o2r }, expected, true, false);

		for (int k = 0; k < t_keys.length; k++) {
			setObjects(map, t_keys[k], null);
			checkParallel(PQLFactory.LOOKUP(o0r, o1r, o2w),
				      UNKNOWN_COUNT, new int[] {  o2r }, new Object[] { t_values[k] }, true, false);

			setObjects(map, null, t_values[k]);
			checkParallel(PQLFactory.LOOKUP(o0r, o1w, o2r),
				      UNKNOWN_COUNT, new int[] {  o1r }, new Object[] { t_keys[k] }, true, false);

			setObjects(map, t_keys[k], t_values[k]);
			checkParallel(PQLFactory.LOOKUP(o0r, o1r, o2r),
				      UNKNOWN_COUNT, new int[] {  o1r, o2r }, new Object[] { t_keys[k], t_values[k] }, true, false);
		}
	}

	@Test
	public void
	testLookupHashmap()
	{
		testLookup(new HashMap<Object, Object>());
	}


	@Test
	public void
	testLookupPMap()
	{
		testLookup(new PMap<Object, Object>());
	}


	private class Reinterpreter { public Object reinterpret(Object o) { return o; }; }

	public void
	testArrayLookup(Object array, Join join, Reinterpreter reinterpreter)
	{
		if (reinterpreter == null)
			reinterpreter = new Reinterpreter();
		final int size = Array.getLength(array);
		Object[] values = new Object[size];
		for (int i = 0; i < size; i++)
			values[i] = Array.get(array, i);

		setObjects(array, null, null);
		Object[] expected = new Object[size * 2];
		for (int i = 0; i < size; i++) {
			expected[i * 2] = i;
			expected[i * 2 + 1] = reinterpreter.reinterpret(values[i]);
		}
		join.setArg(0, o0r);
		join.setArg(1, o1w);
		join.setArg(2, o2w);
		checkParallel(join,
			      size, new int[] { o1r, o2r }, expected,
			      true, true);

		for (int k = 0; k < size; k++) {
			join.setArg(1, o1r);
			join.setArg(2, o2w);
			setObjects(array, k, null);
			checkParallel(join,
				      UNKNOWN_COUNT, new int[] {  o2r }, new Object[] { reinterpreter.reinterpret(values[k]) },
				      true, false);

			join.setArg(1, o1w);
			join.setArg(2, o2r);
			setObjects(array, null, reinterpreter.reinterpret(values[k]));
			checkParallel(join,
				      UNKNOWN_COUNT, new int[] {  o1r }, new Object[] { k },
				      true, false);

			join.setArg(1, o1r);
			join.setArg(2, o2r);
			setObjects(array, k, reinterpreter.reinterpret(values[k]));
			checkParallel(join,
				      UNKNOWN_COUNT, new int[] {  o1r }, new Object[] { k },
				      true, false);
		}
	}

	@Test
	public void
	testArrayLookupInt()
	{
		testArrayLookup(new int[] { 3, 7, 42, 19, 22, -99, 65536 }, PQLFactory.ARRAY_LOOKUP_Int(0, 0, 0), null);
	}


	private Reinterpreter int_reinterpreter = new Reinterpreter() {
			public Object reinterpret(Object o) {
				return ((Number)o).intValue();
			}
		};

	private Reinterpreter char_to_int_reinterpreter = new Reinterpreter() {
			public Object reinterpret(Object o) {
				return (int) (((Character)o).charValue());
			}
		};

	private Reinterpreter bool_to_int_reinterpreter = new Reinterpreter() {
			public Object reinterpret(Object o) {
				return (int) (((Boolean)o).booleanValue() ? 1 : 0);
			}
		};


	private Reinterpreter float_to_double_reinterpreter = new Reinterpreter() {
			public Object reinterpret(Object o) {
				return (double) (((Float)o).floatValue());
			}
		};

	@Test
	public void
	testArrayLookupChar()
	{
		testArrayLookup(new char[] { 'a', 'x', 'z', 'v', '0' }, PQLFactory.ARRAY_LOOKUP_Char(0, 0, 0), char_to_int_reinterpreter);
	}


	@Test
	public void
	testArrayLookupBoolean()
	{
		testArrayLookup(new boolean[] { true, false }, PQLFactory.ARRAY_LOOKUP_Boolean(0, 0, 0), bool_to_int_reinterpreter);
	}


	@Test
	public void
	testArrayLookupShort()
	{
		testArrayLookup(new short[] { -3, 32767, 11, 17, 23 }, PQLFactory.ARRAY_LOOKUP_Short(0, 0, 0), int_reinterpreter);
	}


	@Test
	public void
	testArrayLookupByte()
	{
		testArrayLookup(new byte[] { -9, 4, 64, 32 }, PQLFactory.ARRAY_LOOKUP_Byte(0, 0, 0), int_reinterpreter);
	}


	@Test
	public void
	testArrayLookupLong()
	{
		testArrayLookup(new long[] { -9999999999l, -1l, 0l, 65536l, 0xfffffffff0101l }, PQLFactory.ARRAY_LOOKUP_Long(0, 0, 0), null);
	}


	@Test
	public void
	testArrayLookupDouble()
	{
		testArrayLookup(new double[] { 3.14159265358979, 0.1, -111.42, 4e+42 }, PQLFactory.ARRAY_LOOKUP_Double(0, 0, 0), null);
	}


	@Test
	public void
	testArrayLookupFloat()
	{
		testArrayLookup(new float[] { 11.0f, 0.998f, 1024.0f, -3e-20f }, PQLFactory.ARRAY_LOOKUP_Float(0, 0, 0), float_to_double_reinterpreter);
	}


	@Test
	public void
	testArrayLookupObject()
	{
		testArrayLookup(new Object[] { 1, new Object(), "foo", Long.class, PQLFactory.TRUE }, PQLFactory.ARRAY_LOOKUP_Object(0, 0, 0), null);
	}



	public void
	testContainment(Set<Object> set)
	{
		setObjects(set, null, null);
		Object[] expected = t_values;
		for (int i = 0; i < t_keys.length; i++)
			set.add(t_values[i]);

		checkParallel(PQLFactory.CONTAINS(o0r, o1w),
			      UNKNOWN_COUNT, new int[] { o1r }, expected,
			      true, false);

		for (int k = 0; k < t_keys.length; k++) {
			setObjects(set, t_values[k], null);
			checkParallel(PQLFactory.CONTAINS(o0r, o1r),
				      UNKNOWN_COUNT, new int[] { o1r }, new Object[] { t_values[k] },
				      true, false);
		}
	}


	@Test
	public void
	testContainmentPSet()
	{
		testContainment(new PSet<Object>());
	}

	@Test
	public void
	testContainmentHashSet()
	{
		testContainment(new HashSet<Object>());
	}


	@Test
	public void
	testInt()
	{
		assertTrue(PQLFactory.INT(i0r).hasRandomAccess());
		assertEquals(Join.GETSIZE_INT, PQLFactory.INT(i0r).getFanout(this.env));
	}

	
	@Test
	public void
	testLong()
	{
		assertTrue(PQLFactory.LONG(l0r).hasRandomAccess());
		assertEquals(Join.GETSIZE_LONG, PQLFactory.LONG(l0r).getFanout(this.env));
	}

	public void
	testIntegralType(Join join, int start, int size)
	{
		Object[] results = new Object[size];
		for (int i = 0; i < size; i++)
			results[i] = start + i;

		join.setArg(0, i0w);
		checkParallel(join,
			      size, new int[] { i0w }, results,
			      true, true);
	}

	@Test
	public void
	testBoolean()
	{
		testIntegralType(PQLFactory.BOOLEAN(i0w), 0, 2);
	}

	@Test
	public void
	testByte()
	{
		testIntegralType(PQLFactory.BYTE(i0w), -128, 256);
	}

	@Ignore
	@Test
	public void
	testShort()
	{
		testIntegralType(PQLFactory.SHORT(i0w), -32768, 65536);
	}

	@Ignore
	@Test
	public void
	testChar()
	{
		testIntegralType(PQLFactory.CHAR(i0w), 0, 65536);
	}

	// --------------------------------------------------------------------------------
	// --------------------------------------------------------------------------------

	// control structures

	@Test
	public void
	testBoolSimple()
	{
		checkNoRandomAccess(PQLFactory.Bool(PQLFactory.ADD_Int(i0r, i1r, i2r), i3w));
	}

	@Test
	public void
	testBoolComplex()
	{
		setInts(1, 13, 0, -1);
		checkParallel(PQLFactory.Bool(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2r), i3w),
			      13, new int[] { i3r }, new Object[] {
				      0,0,0,0,0,0,0,0,0,0,0,0,0
			      }, true, true);

		setInts(1, 13, 7, -1);
		checkParallel(PQLFactory.Bool(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2r), i3w),
			      13, new int[] { i3r }, new Object[] {
				      0,0,0,0,0,0,0,0,0,0,0,0,1
			      }, true, true);
	}



	@Test
	public void
	testNoFailSimple()
	{
		checkNoRandomAccess(PQLFactory.NoFail(PQLFactory.ADD_Int(i0r, i1r, i2r)));
	}

	@Test
	public void
	testNoFailComplex()
	{
		setInts(1, 13, 0, -1);
		checkParallel(PQLFactory.NoFail(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2r)),
			      13, new int[] { }, new Object[] { null, null, null, null, null, null, null, null, null, null, null, null, null },
			      true, true);

		setInts(1, 13, 7, -1);
		checkParallel(PQLFactory.NoFail(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2r)),
			      13, new int[] { }, new Object[] { null, null, null, null, null, null, null, null, null, null, null, null, null },
			      true, true);
	}


	@Test
	public void
	testNotSimple()
	{
		checkNoRandomAccess(PQLFactory.Not(PQLFactory.ADD_Int(i0r, i1r, i2r)));
	}

	@Test
	public void
	testNotComplex()
	{
		setInts(1, 13, 0, -1);
		checkParallel(PQLFactory.Not(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2r)),
			      13, new int[] { }, new Object[] { null, null, null, null, null, null, null, null, null, null, null, null, null },
			      true, true);

		setInts(1, 13, 7, -1);
		checkParallel(PQLFactory.Not(PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2r)),
			      13, new int[] { }, new Object[] { null, null, null, null, null, null, null, null, null, null, null, null },
			      true, false);
	}


	@Test
	public void
	testConjunctiveBlockZero()
	{
		checkNoRandomAccess(PQLFactory.ConjunctiveBlock());
	}

	@Test
	public void
	testConjunctiveBlockNoParallel()
	{
		checkNoRandomAccess(PQLFactory.ConjunctiveBlock(new Join[] {
					PQLFactory.ADD_Int(i2r, i2r, i3w)
				}));
	}


	@Test
	public void
	testConjunctiveBlockParallel_1_of_1()
	{
		setInts(1, 7, 2, 0);
		checkParallel(PQLFactory.ConjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w)
				}),
			7, new int[] { i2r }, new Object[] { 1, 2, 3, 4, 5, 6, 7 },
			true, true);
	}

	@Test
	public void
	testConjunctiveBlockParallel_1_of_2()
	{
		setInts(1, 7, 2, 0);
		checkParallel(PQLFactory.ConjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w),
					PQLFactory.ADD_Int(i2r, i2r, i3w)
				}),
			7, new int[] { i3r }, new Object[] { 2, 4, 6, 8, 10, 12, 14 },
			true, true);
	}

	@Test
	public void
	testConjunctiveBlockParallel_1_of_3()
	{
		setInts(1, 7, 2, 0);
		setLongs(1l, 2l, 2l);
		checkParallel(PQLFactory.ConjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w),
					PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w),
					PQLFactory.ADD_Int(i2r, i2r, i3w)
				}),
			7, new int[] { i3r, l2r }, new Object[] { 2, 1l, 2, 2l,
								  4, 1l, 4, 2l,
								  6, 1l, 6, 2l,
								  8, 1l, 8, 2l,
								  10, 1l, 10, 2l,
								  12, 1l, 12, 2l,
								  14, 1l, 14, 2l },
			true, true);
	}

	// @Test
	// public void
	// testConjunctiveBlockParallel_2_of_3()
	// {
	// 	setInts(1, 4, 2, 0);
	// 	setLongs(0l, 3l, 0l); 
	// 	checkParallel(PQLFactory.ConjunctiveBlock(new Join[] {
	// 				PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w),
	// 				PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w),
	// 				PQLFactory.MUL_Int(i2r, l2r, i3w)
	// 			}),
	// 		16, new int[] { i3r }, new Object[] { 0, 0, 0, 0,
	// 						      1, 2, 3, 4,
	// 						      2, 4, 6, 8,
	// 						      3, 9, 12, 16 },
	// 		true, true);
	// }

	// @Test
	// public void
	// testConjunctiveBlockParallel_2_of_4()
	// {
	// 	setLongs(0l, 3l, 0l); 
	// 	checkParallel(PQLFactory.ConjunctiveBlock(new Join[] {
	// 				PQLFactory.ADD_Int(i2r, i2r, i1w
	// 				PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w),
	// 				PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w),
	// 				PQLFactory.MUL_Int(i2r, l2r, i3w)
	// 			}),
	// 		16, new int[] { i3r }, new Object[] {  0,  0,  0,  0,
	// 						       1,  2,  3,  4,
	// 						       2,  4,  6,  8,
	// 						       3,  9, 12, 16 },
	// 		true, true);
	// }

	// ----------------------------------------

	// @Test
	// public void
	// testDisjunctiveBlockZero()
	// {
	// 	checkNoRandomAccess(PQLFactory.DisjunctiveBlock());
	// }

	// @Test
	// public void
	// testDisjunctiveBlockNoParallel()
	// {
	// 	checkNoRandomAccess(PQLFactory.DisjunctiveBlock(new Join[] {
	// 				PQLFactory.ADD_Int(i2r, i2r, i3w)
	// 			}));
	// }


	@Test
	public void
	testDisjunctiveBlockParallel_1_of_1()
	{
		setInts(1, 7, 2, 0);
		checkParallel(PQLFactory.DisjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w)
				}),
			7, new int[] { i2r }, new Object[] { 1, 2, 3, 4, 5, 6, 7 },
			true, true);
	}

	@Test
	public void
	testDisjunctiveBlockParallel_1_of_2()
	{
		setInts(1, 7, 2, 9, 11);
		checkParallel(PQLFactory.DisjunctiveBlock(new Join[] {
					PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w),
					PQLFactory.INT_RANGE_CONTAINS(i3r, i4r, i2w)
				}),
			10, new int[] { i2r }, new Object[] { 1, 2, 3, 4, 5, 6, 7, 9, 10, 11 },
			true, true);
	}

	@Test
	public void
	testDisjunctiveBlockRandomAccess_Partial()
	{
		setInts(1, 4, 2, 9, 11);
		int[] access_order = new int[] { 4, 1, 7, 0, 2, 3, 6, 5 }; // `statically randomised'
		int[] expected =     new int[] { 1, 2, 3, 4, 1, 9, 10, 11 };

		final Join j0 = PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, i2w);
		final Join j1 = PQLFactory.EQ_Int(i0r, i2w);
		final Join j2 = PQLFactory.INT_RANGE_CONTAINS(i3r, i4r, i2w);

		assertTrue(j0.hasRandomAccess());
		assertFalse(j1.hasRandomAccess());
		assertTrue(j2.hasRandomAccess());
		
		final ControlStructure disjunction = 
			PQLFactory.DisjunctiveBlock(j0, j1, j2);

		final VarInfo vi = new VarInfo(new VarSet(i0r, i1r, i3r, i4r));

		disjunction.resetForRandomAccess(env);
		assertTrue(disjunction.hasRandomAccess());
		assertEquals((int) expected.length, (int) disjunction.getSize(vi, env));

		for (int index : access_order) {
			assertTrue(index >= 0 && index < expected.length);
			assertFalse(expected[index] == 0);
			final int expected_value = expected[index];
			expected[index] = 0;

			disjunction.moveToIndex(env, index);
			int rv = disjunction.getAtIndex(env, index);
			assertTrue(rv != Join.GETAT_NONE);
			assertEquals(expected_value, env.getInt(i2r));
		}
	}

	// ================================================================================
	// ================================================================================
	// --------------------------------------------------------------------------------
	static final int UNKNOWN_COUNT = -1;
	static final int MAX_CORES = 4;

	public void
	checkParallel(Join join, int expected_count, int[] variables_to_check, Object[] values_to_check, boolean any_true, boolean all_true)
	{
		testWithClones(join, "doTestParallelRetrieval", new Object[] { expected_count, variables_to_check, values_to_check, any_true, all_true });
	}

	static final int ANY = 0;
	static final int ALL = 1;

	public void
	doTestParallelRetrieval(Join join, int expected_count, int[] variables_to_check, Object[] values_to_check, boolean any_true, boolean all_true)
	{
		join.resetForRandomAccess(this.env);
		assertTrue("hasRandomAccess()", join.hasRandomAccess());
		final int count = join.getFanout(this.env);
		assertTrue("has nontrivial getCount()", count >= 0);
		if (expected_count != UNKNOWN_COUNT)
			assertEquals("getFanout()", expected_count, count);

		ArrayList<Object[]> expected_result = new ArrayList<Object[]>();

		if (variables_to_check.length == 0)
			for (int i = 0; i < values_to_check.length; i++)
				expected_result.add(new Object[0]);
		else {
			final int expected_length = values_to_check.length / variables_to_check.length;
			for (int i = 0; i < expected_length; i++) {
				Object[] orep = new Object[variables_to_check.length];
				final int base_offset = variables_to_check.length * i;
				for (int k = 0; k < variables_to_check.length; k++)
					orep[k] = values_to_check[base_offset + k];
				expected_result.add(orep);
			}
		}

		boolean[] retvals;

		for (int cores_nr = 1; cores_nr < MAX_CORES; cores_nr++) {

			ArrayList<Object[]> ol;

			ol = new ArrayList<Object[]>();
			retvals = new boolean[] { false, true };
			parallelCollectBlocksSequential(join, cores_nr, count, ol, variables_to_check, retvals);
			assertCorrectnessOfParallelRetrieval("blocks-sequential/cores=" + cores_nr, ol, expected_result, retvals, any_true, all_true);

			ol = new ArrayList<Object[]>();
			retvals = new boolean[] { false, true };
			parallelCollectBlocksInterspersed(join, cores_nr, count, ol, variables_to_check, retvals);
			assertCorrectnessOfParallelRetrieval("blocks-interspersed/cores=" + cores_nr, ol, expected_result, retvals, any_true, all_true);

			ol = new ArrayList<Object[]>();
			retvals = new boolean[] { false, true };
			parallelCollectStripesSequential(join, cores_nr, count, ol, variables_to_check, retvals);
			assertCorrectnessOfParallelRetrieval("stripes-sequential/cores=" + cores_nr, ol, expected_result, retvals, any_true, all_true);

			ol = new ArrayList<Object[]>();
			retvals = new boolean[] { false, true };
			parallelCollectStripesInterspersed(join, cores_nr, count, ol, variables_to_check, retvals);
			assertCorrectnessOfParallelRetrieval("stripes-interspersed/cores=" + cores_nr, ol, expected_result, retvals, any_true, all_true);
		}
	}

	private Join[]
	makeJoins(Join j, int count)
	{
		Join[] retval = new Join[count];
		for (int i = 0; i < count; i++)
			retval[i] = j.copyRecursively();
		return retval;
	}

	private Env[]
	makeEnvs(int count)
	{
		Env[] retval = new Env[count];
		for (int i = 0; i < count; i++)
			retval[i] = this.env.copy();
		return retval;
	}

	private String
	tshow(Object[] ol)
	{
		String s = "<";
		for (int i = 0; i < ol.length; i++) {
			if (i > 0)
				s += ", ";
			s += ol[i];
		}
		return s + ">";
	}


	private boolean
	teq(Object[] a, Object[] b)
	{
		for (int i = 0; i < a.length; i++)
			if (a[i] == null)
				return b[i] == null;
			else
				if (!a[i].equals(b[i]))
					return false;
		return true;
	}

	private boolean
	telim(ArrayList<Object[]> expected_list, Object[] key)
	{
		for (int i = 0; i < expected_list.size(); i++) {
			Object[] r = expected_list.get(i);
			if (teq(r, key)) {
				expected_list.remove(r);
				return true;
			}
		}
		return false;
	}


	private void
	assertCorrectnessOfParallelRetrieval(String phase, ArrayList<Object[]> actual, final ArrayList<Object[]> expected_src, boolean[] retvals, boolean expected_any_true, boolean expected_all_true)
	{
		String errors = "";
		ArrayList<Object[]> expected = new ArrayList<Object[]>(expected_src);

		assertEquals("mismatch in ANY", expected_any_true, retvals[ANY]);
		assertEquals("mismatch in ALL", expected_all_true, retvals[ALL]);

		for (Object[] a : actual) {
			if (!telim(expected, a))
				errors = errors + "- Found unexpected element " + tshow (a) + " ("+phase+")\n";
		}

		for (Object[] b : expected)
			errors = errors + "- Missed expected element " + tshow (b) + "("+phase+")\n";

		if (!errors.equals("")) {
			System.err.println(errors);
			fail(errors);
		}
	}

	private void
	parallelCollectBlocksSequential(Join join, int cores_nr, final int total_count, ArrayList<Object[]> collection, int[] variables_to_check, boolean[] retvals)
	{
		Join[] joins = makeJoins(join, cores_nr);
		Env[] envs = makeEnvs(cores_nr);

		int count_per_core = total_count / cores_nr;
		int cores_with_one_extra = total_count - (count_per_core * cores_nr);

		for (int core = 0; core < cores_nr; core++) {
			int start_offset = core * count_per_core;

			int total_core_count = count_per_core;

			if (core < cores_with_one_extra) {
				++total_core_count;
				start_offset += core;
			} else
				start_offset += cores_with_one_extra;

			for (int i = 0; i < total_core_count; i++) {
				final int offset = start_offset + i;
				collectJoin(joins[core], envs[core], offset, collection, variables_to_check, retvals);
			}
		}
	}

	private void
	parallelCollectBlocksInterspersed(Join join, int cores_nr, final int total_count, ArrayList<Object[]> collection, int[] variables_to_check, boolean[] retvals)
	{
		Join[] joins = makeJoins(join, cores_nr);
		Env[] envs = makeEnvs(cores_nr);

		int count_per_core = total_count / cores_nr;
		int cores_with_one_extra = total_count - (count_per_core * cores_nr);

		for (int i = 0; i < count_per_core + 1; i++) {
			int max_cores_nr = cores_nr;
			if (i == count_per_core)
				max_cores_nr = cores_with_one_extra;

			for (int core = 0; core < max_cores_nr; core++) {
				int offset = core * count_per_core + i;

				if (core < cores_with_one_extra) {
					offset += core;
				} else
					offset += cores_with_one_extra;

				collectJoin(joins[core], envs[core], offset, collection, variables_to_check, retvals);
			}
		}
	}

	private void
	parallelCollectStripesSequential(Join join, int cores_nr, final int total_count, ArrayList<Object[]> collection, int[] variables_to_check, boolean[] retvals)
	{
		Join[] joins = makeJoins(join, cores_nr);
		Env[] envs = makeEnvs(cores_nr);

		int count_per_core = total_count / cores_nr;
		int cores_with_one_extra = total_count - (count_per_core * cores_nr);

		for (int core = 0; core < cores_nr; core++) {
			final int start_offset = core;
			final int stride = cores_nr;

			int total_core_count = count_per_core;
			if (core < cores_with_one_extra)
				++total_core_count;

			for (int i = 0; i < total_core_count; i++) {
				final int offset = start_offset + i * stride;
				collectJoin(joins[core], envs[core], offset, collection, variables_to_check, retvals);
			}
		}
	}

	private void
	parallelCollectStripesInterspersed(Join join, int cores_nr, final int total_count, ArrayList<Object[]> collection, int[] variables_to_check, boolean[] retvals)
	{
		Join[] joins = makeJoins(join, cores_nr);
		Env[] envs = makeEnvs(cores_nr);

		int count_per_core = total_count / cores_nr;
		int cores_with_one_extra = total_count - (count_per_core * cores_nr);

		for (int i = 0; i < count_per_core + 1; i++) {
			int max_cores_nr = cores_nr;
			if (i == count_per_core)
				max_cores_nr = cores_with_one_extra;

			for (int core = 0; core < max_cores_nr; core++) {
				int offset = core + cores_nr * i;

				collectJoin(joins[core], envs[core], offset, collection, variables_to_check, retvals);
			}
		}
	}

	private void
	collectJoin(Join join, Env local_env, int offset, ArrayList<Object[]> collection, int[] variables_to_check, boolean[] retvals)
	{
		join.moveToIndex(local_env, offset);
		boolean multi = false;
		while (true) {
			boolean abort = false;
			switch (join.getAtIndex(local_env, offset)) {
			case Join.GETAT_ONE:
				abort = true;
				// fall through
			case Join.GETAT_ANY:
				Object[] result = new Object[variables_to_check.length];
				for (int i = 0; i < variables_to_check.length; i++)
					result[i] = local_env.getObject(variables_to_check[i]);
				collection.add(result);
				retvals[ANY] = true;
				if (abort)
					return;
				multi = true;
				break;

			case Join.GETAT_NONE:
				if (!multi)
					retvals[ALL] = false;
				return;
			}
		}
	}
}
