/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.meta;
import edu.umass.pql.il.*;
import edu.umass.pql.*;

public class Schedule extends DecoratorJoin
{
	MetaJoin[] entities_to_schedule;
	protected VarInfo var_info = null; // initialised in setVarInfo

	public void
	setVarInfo(VarInfo vi)
	{
		this.var_info = vi;
	}

	public
	Schedule(Join body, MetaJoin[] entities_to_schedule)
	{
		super(body);
		this.entities_to_schedule = entities_to_schedule;
	}

	@Override
	public void
	reset(Env env)
	{
		this.scheduleAll(env);
		super.reset(env);
	}

	@Override
	public void
	resetForRandomAccess(Env env)
	{
		super.resetForRandomAccess(env);
		this.scheduleAll(env);
	}

	private void
	scheduleAll(Env env)
	{
		assert this.var_info != null;
		for (MetaJoin e : this.entities_to_schedule)
			e.schedule(this.var_info, env);
	}

	@Override
	public void
	accept(JoinVisitor visitor)
	{
		visitor.visitSchedule(this);
	}

	@Override
	public String
	toString()
	{
		return "%SCHEDULE { " + this.body.toString() + "}";
	}
}