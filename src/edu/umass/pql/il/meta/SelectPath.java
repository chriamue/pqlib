/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.meta;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import java.util.HashMap;
import java.util.Map;

public class SelectPath extends DecoratorJoin implements MetaJoin
{
	public static final int MAX_BEST_CHOICES = 16; // We use dynamic progamming to pick the best scheduling choices; this is the maximum number of `best choices' we maintain

	protected final Join[] initial_components;
	protected final VarSet[] read_dependencies;
	protected final VarSet[] all_variables;
	protected VarInfo var_info = null; // initialised in setVarInfo
	private static final boolean DEBUG = false;
	private static final boolean DEBUG_DETAIL = false;

	@Override
	public double
	getSelectivity(VarInfo vi, Env env)
	{
		if (this.body == null)
			this.schedule(vi, env);
		return super.getSelectivity(vi, env);
	}

	public Join[]
	getComponentsToSchedule()
	{
		return this.initial_components;
	}

	@Override
	public double
	getSize(VarInfo vi, Env env)
	{
		if (this.body == null)
			this.schedule(vi, env);
		return super.getSize(vi, env);
	}


	@Override
	public double
	getAccessCost(VarInfo vi, Env env)
	{
		if (this.body == null)
			this.schedule(vi, env);
		return super.getAccessCost(vi, env);
	}


	@Override
	public boolean
	hasRandomAccess()
	{
		for (Join join : this.initial_components)
			if (join.hasRandomAccess())
				return true;
		return false;
	}

	public void
	setVarInfo(VarInfo vi)
	{
		this.var_info = vi;
	}

	public Join
	join()
	{
		return this;
	}

	public void
	unschedule()
	{
		this.body = null;
	}

	@Override
	public String
	toString()
	{
		StringBuffer buf = new StringBuffer("%SELECT_PATH[");
		boolean first = true;
		for (Join j : initial_components) {
			if (first)
				first = false;
			else
				buf.append(", ");
			buf.append("(");
			buf.append(j.toString());
			buf.append(")");
		}
		buf.append("]");
		if (this.body != null) {
			buf.append("=");
			buf.append(this.body.toString());
		}
		return buf.toString();
	}

	/**
	 * Constructs a new SelectPath object
	 *
	 * @param components The individual joins that should be ordered.  Joins whose read variables are satisfied should come first.
	 * @param read_dependencies Sets of variables that the components depend on having been initialised.
	 * @param all_variables The set of all variables expected to be read or written to by the join
	 */
	public
	SelectPath(Join[] components, VarSet[] read_dependencies, VarSet[] all_variables)
	{
		super(null);
		this.initial_components = components;
		this.read_dependencies = read_dependencies;
		this.all_variables = all_variables;
	}

	public Join[]
	getComponents()
	{
		return this.initial_components;
	}

	/**
	 * Control-dependent blocks (Not, Bool, NoFail) need special treatment:  any variable referenced in their
	 * components is a read dependency, since otherwise they might get re-ordered with joins that determine
	 * their effect on control flow.
	 */
	static boolean
	isControlDependency(Join j)
	{
		ControlDepChecker cdc = new ControlDepChecker();
		j.accept(cdc);
		return cdc.is_control_dependency;
	}
	private final static class ControlDepChecker extends JoinVisitor
	{
		public boolean is_control_dependency = false;
		@Override public void visitDefault(Join j) {}
		@Override public void visitNot(Not j) { this.is_control_dependency = true; }
		@Override public void visitNoFail(NoFail j) { this.is_control_dependency = true; }
		@Override public void visitBool(NoFail.Bool j) { this.is_control_dependency = true; }
	};


	public static SelectPath
	create(Join[] components)
	{
		final VarSet[] read_dependencies = new VarSet[components.length];
		final VarSet[] all_variables = new VarSet[components.length];
		final VarSet defined_earlier = new VarSet();

		for (int i = 0; i < components.length; i++) {
			final VarSet vs = new VarSet();
			read_dependencies[i] = vs;
			components[i].addReadDependencies(vs);
			if (isControlDependency(components[i])) {
				VarSet inner_deps = new VarSet();
				ControlStructure cs = (ControlStructure) components[i];
				for (int j = 0; j < cs.getComponentsNr(); j++)
					cs.getComponent(j).addAllVariables(inner_deps);
				inner_deps.retainAll(defined_earlier);
				vs.addAll(inner_deps);
			}

			final VarSet vs2 = new VarSet();
			all_variables[i] = vs2;
			components[i].addAllVariables(vs2);
			components[i].addWriteDependencies(defined_earlier);
			if (DEBUG_DETAIL) {
				System.err.println(" -- #" + i + ":" + components[i]);
				System.err.println("    read-deps : " + read_dependencies[i]);
				System.err.println("    all-vars  : " + vs2);
				System.err.println("  ..write-deps: " + defined_earlier);
			}

		}
		return new SelectPath(components, read_dependencies, all_variables);
	}

	public void
	schedule(VarInfo new_var_info, Env env, boolean assume_parallel)
	{
		this.var_info.getConstantSet().addAll(new_var_info.getConstantSet());
		if (DEBUG_DETAIL)
			System.err.println("Scheduling for parallel=" + assume_parallel + ", varinfo: " + new_var_info);
		this.body = (new Scheduler(this.var_info, env, assume_parallel)).doSchedule();
		if (DEBUG)
			System.err.println("Body is: " + this.body);
		this.body.accept(new ReadFlagSetterVisitor(this.var_info.getConstantSet().copy()));
	}

	@Override
	public void
	schedule(VarInfo new_var_info, Env env)
	{
		schedule(new_var_info, env, false);
	}

	@Override
	public SelectPath
	copyRecursively()
	{
		final SelectPath retval = (SelectPath) super.copyRecursively();
		return retval;
	}


	// --------------------------------------------------------------------------------
	// visitor
	@Override
	public void
	accept(JoinVisitor visitor)
	{
		visitor.visitSelectPath(this);
	}


	private final class
	Scheduler
	{
		boolean parallel_mode;
		public static final double PARALLEL_TWEAK_FACTOR = 0.25; // Roughly: factor we expect parallelism to speed us up

		public
		Scheduler(VarInfo var_info, Env env, boolean parallel_mode)
		{
			this.initially_accessible_variables = var_info.getConstantSet();
			this.parallel_mode = parallel_mode;
			if (DEBUG && parallel_mode)
				System.err.println(">> Scheduler: Setting PARALLEL MODE");
			this.env = env;
		}

		Env env;
		VarSet initially_accessible_variables;

		VarInfo var_info = new VarInfo();
		double[] cost = new double[MAX_BEST_CHOICES];
		int[][] rankings = new int[MAX_BEST_CHOICES][initial_components.length];
		int[][] last_rankings = new int[MAX_BEST_CHOICES][initial_components.length];
		// # of ranks: number of choices we're currently considering for DP.
		// # of components: length of our choice sequences.
		// The entries are ranked in order of increasing cost.

		int components_nr = 0;
		int rankings_nr = 0;
		int last_rankings_nr = 0;
                Map <Join, Double> cachedSize = new HashMap <Join, Double> ();

		VarSet vsc = new VarSet();

		boolean
		setupJoin(Join join, int join_index, VarSet readable_variables)
		{
			// for (int v = 0; v < join.getArgsNr(); v++) {
			// 	if (readable_variables.contains(join.getArg(v)))
			// 		join.makeReadArg(v);
			// 	else {
			// 		join.makeWriteArg(v);
			// 		readable_variables.insert(join.getArg(v));
			// 		if (v < join.getReadArgsNr())
			// 			return false;
			// 	}
			// }
			join.accept(new ReadFlagSetterVisitor(readable_variables.copy()));
			readable_variables.addAll(all_variables[join_index]);
			return true;
		}

		VarSet
		getInitialVariables()
		{
			final VarSet readable_variables = this.vsc;
			readable_variables.clear();
			readable_variables.addAll(this.initially_accessible_variables);
			return readable_variables;
		}

		double // return negative if the choice is not valid
		computeComponentsAccessCost(int old_rank_nr, int next_choice)
		{
			double size = 1.0;
			double selectivity = 1.0;
			double cost = 0.0;

			boolean initial_op_is_parallel = false;

			final VarSet readable_variables = getInitialVariables();
			this.var_info.setConstants(readable_variables.copy());

			for (int i = 0; i <= this.components_nr; i++) {
				final int join_index;

				if (i < this.components_nr)
					join_index = this.last_rankings[old_rank_nr][i];
				else {
					join_index = next_choice;
					final VarSet join_dependencies = read_dependencies[next_choice];
					if (!readable_variables.containsAll(join_dependencies)) {
						if (DEBUG_DETAIL)
							System.err.println(" -- failed deps:  needed " + join_dependencies + " and had " + readable_variables);
						return -1.0;
					}
				}
				final Join join = initial_components[join_index];

				if (!setupJoin(join, join_index, readable_variables))
					return -1.0;

				try {
                                        double local_size = 0;
                                        boolean foundCached = false;
                                        for (Join cachedJoin : cachedSize.keySet()) {
                                            if (cachedJoin == join) {
                                                local_size = cachedSize.get(join);
                                                foundCached = true;
                                                break;
                                            }
                                        }
                                        if (!foundCached) {
                                            local_size = join.getSize(this.var_info, this.env);
                                            cachedSize.put(join, local_size);
                                        }
					final double local_selectivity = join.getSelectivity(this.var_info, this.env);
					final double local_cost = join.getAccessCost(this.var_info, this.env);
					if (i == 0 && join.hasRandomAccess()) {
						initial_op_is_parallel = true;
						if (DEBUG_DETAIL)
							System.err.println(">> Parallel:  initial join is parallel; will para:"+ this.parallel_mode +", join=" + join);
					}

					cost += local_cost * size * selectivity;
					size *= local_size;
					selectivity *= local_selectivity;
				} catch (RuntimeException e) {
					e.printStackTrace();
					System.err.println("While computing join properties for:");
					System.err.println("Join: " + join);
					System.err.println("  vi: " + this.var_info);
					System.err.println("  rv: " + readable_variables);
					System.err.println(" env: " + this.env);
					throw e;
				}
			}
			if (this.parallel_mode && initial_op_is_parallel) {
				cost *= PARALLEL_TWEAK_FACTOR;
			}

			return cost;
		}

		public Join[]
		extractBest()
		{
			final int rank_nr = 0; // best rank
			Join[] result = new Join[initial_components.length];

			final VarSet readable_variables = getInitialVariables();

			for (int i = 0; i < initial_components.length; i++) {
				final Join join;

				join = initial_components[this.last_rankings[rank_nr][i]];

				setupJoin(join, this.last_rankings[rank_nr][i], readable_variables);

				result[i] = join;
			}
			return result;
		}

		int
		findCostOffset(double cost)
		{
			int max = this.rankings_nr;
			int min = 0;

			int delta = max - min;

			while (delta > 0) {
				int offset = min + (delta >> 1);

				if (cost < this.cost[offset])
					max = offset;
				else
					min = offset + 1;

				delta = max - min;
			}

			return min;
		}

		void
		insertRankIfGood(int old_rank_nr, int new_component_nr, double cost)
		{
			if (this.rankings_nr < MAX_BEST_CHOICES
			    || this.cost[this.rankings_nr - 1] > cost) {
				final int[] my_entry = this.rankings[MAX_BEST_CHOICES - 1];
				System.arraycopy(last_rankings[old_rank_nr], 0, my_entry, 0, this.components_nr);
				my_entry[this.components_nr] = new_component_nr;
				if (DEBUG)
					System.err.println("+ New component " + new_component_nr + " inserted, offset " + this.rankings_nr);

				final int my_entry_position = findCostOffset(cost);

				for (int i = MAX_BEST_CHOICES - 1; i > my_entry_position; i--) {
					this.cost[i] = this.cost[i - 1];
					this.rankings[i] = this.rankings[i - 1];
				}
				if (DEBUG)
					System.err.println("+ at #" + my_entry_position + " with cost " + cost);
				this.cost[my_entry_position] = cost;
				this.rankings[my_entry_position] = my_entry;

				if (this.rankings_nr < MAX_BEST_CHOICES)
					++this.rankings_nr;
			}
		}

		int // from last_rankings
		setSelectionChoices(int[] choices, int old_rank_nr)
		{
			for (int i = 0; i < initial_components.length; i++)
				choices[i] = i;
			int choices_nr = initial_components.length;

			for (int i = 0; i < this.components_nr; i++)
				choices[this.last_rankings[old_rank_nr][i]] = -1;

			while (choices_nr > 0
			       && choices[choices_nr - 1] == -1)
				--choices_nr;

			for (int i = 0; i < choices_nr; i++)
				if (choices[i] == -1) {
					final int replacement = choices[--choices_nr];
					if (replacement == -1) {
						return i;
					}
					choices[i] = replacement;
				}

			// VarSet readable_variables = getInitialVariables();

			// for (int i = 0; i < choices_nr; i++) {
			// 	//final Join j = initial_components[choices[i]];
			// 	final VarSet j_dependencies = read_dependencies[choices[i]];

			// 	if (!readable_variables.containsAll(j_dependencies)) // not all read dependencies satisfied?
			// 		choices[this.last_rankings[old_rank_nr][i]] = choices[--choices_nr];
			// }

			return choices_nr;
		}

		private void
		dump()
		{
			System.err.println("/----------------------------------------\\");
			System.err.println("Access path uses:");
			for (int i = 0; i < initial_components.length; i++) {
				final Join join = initial_components[i];

				final double local_size = join.getSize(this.var_info, this.env);
				final double local_selectivity = join.getSelectivity(this.var_info, this.env);
				final double local_cost = join.getAccessCost(this.var_info, this.env);

				System.err.println("#" + i + ": " + join.toString() + "(size=" + local_size + "; selectivity=" + local_selectivity + "; item-cost=" + local_cost + ")");
			}

			final int real_components_nr = (this.components_nr == initial_components.length) ? this.components_nr : this.components_nr + 1;

			System.err.println("\nAccess path rankings:");
			for (int r = 0; r < this.rankings_nr; r++) {
				double score = cost[r];
				int[] rank = this.rankings[r];
				System.err.print(score + "\t");
				for (int j = 0; j < real_components_nr; j++)
					System.err.print("  #" + rank[j]);
				System.err.println();

				final VarSet readable_variables = getInitialVariables();
				this.var_info.setConstants(readable_variables.copy());

				double size = 1.0;
				double selectivity = 1.0;
				double cost = 0.0;

				for (int i = 0; i < real_components_nr; i++) {
					final Join join = initial_components[rank[i]];

					setupJoin(join, rank[i], readable_variables);

					final double local_size = join.getSize(this.var_info, this.env);
					final double local_selectivity = join.getSelectivity(this.var_info, this.env);
					final double local_cost = join.getAccessCost(this.var_info, this.env);

					cost += local_cost * size * selectivity;
					size *= local_size;
					selectivity *= local_selectivity;

					System.err.println("\t" + cost + "\t<- #" + rank[i] + ": " + join.toString() + "(size=" + local_size + "; selectivity=" + local_selectivity + "; item-cost=" + local_cost + ")");
				}
			}

			System.err.println("\\----------------------------------------/");
		}


		public AbstractBlock
		doSchedule()
		{
			if (DEBUG_DETAIL)
				System.err.println("\n\n\n\n");
			this.last_rankings_nr = 1;
			int selection_choices[] = new int[initial_components.length];

			for (components_nr = 0; components_nr < initial_components.length; components_nr++) {
				this.rankings_nr = 0;
				if (DEBUG_DETAIL)
					System.err.println("\n==========================================================================================");
				for (int old_rank = 0; old_rank < this.last_rankings_nr; old_rank++) {
					// for each old `best path' indicated by `old_rank', identify all opportunities
					final int selection_choices_nr = setSelectionChoices(selection_choices, old_rank);
					if (DEBUG_DETAIL) {
						System.err.println("************************************************");
						System.err.print("  rank #" + old_rank + " choices: ");
						for (int i = 0; i < selection_choices_nr; i++)
							System.err.print("  " + selection_choices[i]);
						System.err.println();
					}
					for (int i = 0; i < selection_choices_nr; i++) {
						// Try all opportunities
						final int new_selection = selection_choices[i];
						double cost = this.computeComponentsAccessCost(old_rank, new_selection);
						if (cost < 0.0) {
							if (DEBUG_DETAIL)
								System.err.println(">>>> skipping old-rank " + old_rank + " plus " + new_selection);
						} else {
							if (DEBUG_DETAIL)
								System.err.println(">>>> cost " + cost + " for old-rank " + old_rank + " plus " + new_selection);
							this.insertRankIfGood(old_rank, new_selection, cost);
						}
					}
				}
				if (DEBUG_DETAIL)
					dump();

				if (this.rankings_nr <= 0) {
					System.err.println(this.var_info.toString());
					this.dump();
					throw new RuntimeException("Access path selection failed");
				}

				// prepare next round
				this.last_rankings_nr = this.rankings_nr;
				int[][] temp = this.last_rankings;
				this.last_rankings = this.rankings;
				this.rankings = temp;
			}

			this.rankings = this.last_rankings;

			if (DEBUG_DETAIL)
				dump();

			return (AbstractBlock) PQLFactory.ConjunctiveBlock(extractBest());
		}
	}

}