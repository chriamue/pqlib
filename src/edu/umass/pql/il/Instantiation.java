/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.pql.*;
import java.lang.reflect.Constructor;

/**
 * Field subscription
 */
public class Instantiation extends Predicate
{
	private Constructor<?> constructor;
	int result;
	int[] args;
	Object[] arg_v;
	Class<?>[] arg_types;

	public
	Instantiation(Constructor<?> cons, int[] args, int result)
	{
		this.constructor = cons;
		this.result = result;
		this.args = args;
		this.arg_types = cons.getParameterTypes();
		this.arg_v = new Object[args.length];
		if (args.length != arg_types.length)
			throw new IllegalArgumentException("Argument mismatch in Instantiation");
	}

	@Override
	public void
	setArg(int i, int v)
	{
		if (i < args.length)
			args[i] = v;
		else if (i == args.length)
			result = v;
		else throw new ArrayIndexOutOfBoundsException(i);
	}

	@Override
	public int
	getArg(int i)
	{
		if (i < args.length)
			return args[i];
		else if (i == args.length)
			return result;
		else throw new ArrayIndexOutOfBoundsException(i);
	}

	@Override
	public int
	getArgsNr()
	{
		return args.length + 1;
	}

	@Override
	public int
	getReadArgsNr()
	{
		return args.length;
	}

	public boolean
	test(Env env)
	{
		try {
			for (int i = 0; i < args.length; i++)
				arg_v[i] = env.getCoercedObject(this.args[i], arg_types[i]);
			return env.unifyObject(this.result, this.constructor.newInstance(arg_v));
		}
		catch (IllegalAccessException __) {}
		catch (InstantiationException __) {}
		catch (java.lang.reflect.InvocationTargetException __) {}

		return false;
	}

	@Override
	public void accept(JoinVisitor visitor)
	{
		visitor.visitInstantiation(this);
	}

	@Override
	public String
	getConstructorName()
	{
		return null;
	}

	@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0; }
	@Override public double	getAccessCost(VarInfo var_info, Env env) { return 10.0; }
}