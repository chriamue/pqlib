/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.*;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import java.util.*;

public abstract class Container
{
	public final static class CONTAINS extends Join.BI2 {
		public CONTAINS (int v0, int v1) { super(v0, v1); }

		Set<?> set;
		Object[] pset_data;
		Iterator<?> iterator;

		public int getReadArgsNr() { return 1; }

		@Override
		public int
		getRangeInferenceParameter()
		{
			return 1;
		}

		public void
		reset(Env env)
		{
			this.pset_data = null;
			set = (Set<?>) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				iterator = null;
			else
				iterator = set.iterator();
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (iterator == null) {
				if (set == null)
					return false;
				if (set.contains(env.getObject(this.v1))) {
					set = null;
					return true;
				} else
					return false;
			} else {
				if (!iterator.hasNext())
					return false;
				env.setObject(this.v1, iterator.next());
				return true;
			}
		}

		@Override
		public boolean
		hasRandomAccess()
		{
			return true;
		}

		@Override
		@SuppressWarnings("unchecked")
		public void
		resetForRandomAccess(Env env)
		{
			this.set = (Set<Object>) env.getObject(this.v0);
			PSet<Object> pset;
			if (this.set instanceof PSet)
				pset = (PSet) this.set;
			else {
				pset = new PSet();
				pset.addAll(this.set);
			}
			this.pset_data = pset.getRepresentation();
		}


		@Override
		public int
		getFanout(Env env)
		{
			return this.pset_data.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
			final Object key = pset_data[offset];
			if (key == null)
				return GETAT_NONE;
		        return env.unifyObject(this.v1, key) ? GETAT_ONE : GETAT_NONE;
		}

		@Override public double
		getSelectivity(VarInfo var_info, Env env)
		{
			return getSelectivityFromReadFlag(1, 1.0, OBJECT_JOIN_SELECTIVITY);
		}

		@Override public double
		getAccessCost(VarInfo var_info, Env env)
		{
			return getSelectivityFromReadFlag(1, 4.0, 10.0);
		}

		@Override public double
		getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(1)))
				return 1.0;
			else if (var_info.isConstant(this.getArg(0)) && env.isValid(this.getArg(0))) {
				reset(env);
				return this.set.size();
			} else
				return DEFAULT_SET_SIZE;
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitSetContains(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class POLY_LOOKUP extends DecoratorJoin
	{
		Join[] poly_joins = new Join[10];

		enum AccessType {
			MAP(0)		{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.LOOKUP(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			INT_ARRAY(1)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Int(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			LONG_ARRAY(2)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Long(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			SHORT_ARRAY(3)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Short(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			CHAR_ARRAY(4)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Char(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			BYTE_ARRAY(5)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Byte(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			BOOLEAN_ARRAY(6){ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Boolean(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			FLOAT_ARRAY(7)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Float(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			DOUBLE_ARRAY(8)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Double(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } },
			OBJECT_ARRAY(9)	{ protected Join createFresh(POLY_LOOKUP l) { return PQLFactory.ARRAY_LOOKUP_Object(l.body.getArg(0), l.body.getArg(1), l.body.getArg(2)); } };

			AccessType(int i) { this.index = i; }
			int index;
			protected abstract Join createFresh(POLY_LOOKUP l);

			public Join create(POLY_LOOKUP l)
			{
				if (l.poly_joins[this.index] == null)
					l.poly_joins[this.index] = this.createFresh(l);
				return l.poly_joins[this.index];
			}
		};

		AccessType access_type;

		public POLY_LOOKUP(int v0, int v1, int v2)
		{
			super(PQLFactory.LOOKUP(v0, v1, v2));
			this.access_type = AccessType.MAP; // default assumption
		}

		private final void
		changeType(AccessType t)
		{
			if (access_type == t)
				return;
			this.access_type = t;
			this.body = t.create(this);
		}

		@Override
		public int
		getRangeInferenceParameter()
		{
			return 1;
		}

		@Override
		public int getComponentsNr() { return 0; }

		@Override
		public int
		getArg(int i)
		{
			return this.body.getArg(i);
		}

		@Override
		public void
		setArg(int i, int v)
		{
			this.body.setArg(i, v);
		}

		@Override
		public int
		getArgsNr()
		{
			return 3;
		}

		@Override
		public int
		getReadArgsNr()
		{
			return 1;
		}

		@Override
		public String
		toString()
		{
			return this.toStringDefault();
		}

		private final void
		updateType(Env env)
		{
			final Object v = env.getObject(this.body.getArg(0));
			if (v instanceof java.util.Map)
				changeType(AccessType.MAP);
			else if (v instanceof int[])
				changeType(AccessType.INT_ARRAY);
			else if (v instanceof long[])
				changeType(AccessType.LONG_ARRAY);
			else if (v instanceof short[])
				changeType(AccessType.SHORT_ARRAY);
			else if (v instanceof char[])
				changeType(AccessType.CHAR_ARRAY);
			else if (v instanceof byte[])
				changeType(AccessType.BYTE_ARRAY);
			else if (v instanceof boolean[])
				changeType(AccessType.BOOLEAN_ARRAY);
			else if (v instanceof float[])
				changeType(AccessType.FLOAT_ARRAY);
			else if (v instanceof double[])
				changeType(AccessType.DOUBLE_ARRAY);
			else
				changeType(AccessType.OBJECT_ARRAY);
		}

		@Override
		public void
		resetForRandomAccess(Env env)
		{
			updateType(env);
			this.body.resetForRandomAccess(env);
		}

		@Override
		public void
		reset(Env env)
		{
			updateType(env);
			this.body.reset(env);
		}

		@Override
		public String
		getConstructorName()
		{
			return this.getName();
		}

		@Override public void accept(JoinVisitor visitor) { visitor.visitPolyLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class INT_RANGE_CONTAINS extends Join.BI3 {
		public INT_RANGE_CONTAINS(int v0, int v1, int v2) { super (v0, v1, v2); }
		public int getReadArgsNr() { return 2; }

		@Override
		public int
		getRangeInferenceParameter()
		{
			return 2;
		}


		private int min, current, max;

		public void
		reset(Env env)
		{
			this.min = this.current = env.getInt(this.v0);
			this.max = env.getInt(this.v1);
		}

		private void
		finish()
		{
			// guarantee termination
			this.current = 1; this.max = 0;
		}

		@Override
		public boolean
		hasRandomAccess()
		{
			return true;
		}

		@Override
		public int
		getFanout(Env env)
		{
			return (this.max - this.min) + 1;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
			return env.unifyInt(this.v2, this.min + offset) ? GETAT_ONE : GETAT_NONE;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (this.current > this.max)
				return false;
			if (Env.isReadVar(this.v2)) {
				final int v = env.getInt(this.v2);
				final boolean retval = v >= this.current && v <= this.max;
				this.finish();
				return retval;
			}
			if (!env.unifyInt(this.v2, this.current))
				return false;
			if (this.current == this.max)
				this.finish();
			else
				++this.current;
			return true;
		}

		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public double	getAccessCost(VarInfo var_info, Env env) { return 0.1; }
		@Override public double	getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(2)))
				return 1.0;
                        else if (var_info.isConstant(this.getArg(0))
				 && env.isValid(this.getArg(0))
				 && var_info.isConstant(this.getArg(1))) {
				reset(env);
				return this.getFanout(env);
			} else
				return 1000.0;
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitRangeContains(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class LONG_RANGE_CONTAINS extends Join.BI3 {
		public LONG_RANGE_CONTAINS(int v0, int v1, int v2) { super (v0, v1, v2); }
		public int getReadArgsNr() { return 2; }

		private long min, current, max;

		@Override
		public int
		getRangeInferenceParameter()
		{
			return 2;
		}

		public void
		reset(Env env)
		{
			this.min = this.current = env.getLong(this.v0);
			this.max = env.getLong(this.v1);
		}

		private void
		finish()
		{
			// guarantee termination
			this.current = 1; this.max = 0;
		}

		@Override
		public boolean
		hasRandomAccess()
		{
			return true;
		}

		@Override
		public int
		getFanout(Env env)
		{
			return (int) ((this.max - this.min) + 1l); // maybe make everything long at one point?
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
			return env.unifyLong(this.v2, this.min + offset) ? GETAT_ONE : GETAT_NONE;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (this.current > this.max)
				return false;
			if (Env.isReadVar(this.v2)) {
				final long v = env.getLong(this.v2);
				final boolean retval = v >= this.current && v <= this.max;
				this.finish();
				return retval;
			}
			env.setLong(this.v2, this.current);
			if (this.current == this.max)
				this.finish();
			else
				++this.current;
			return true;
		}

		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, OBJECT_JOIN_SELECTIVITY); }
		@Override public double	getAccessCost(VarInfo var_info, Env env) { return 0.1; }
		@Override public double	getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(2)))
				return 1.0;
			else if (var_info.isConstant(this.getArg(0))
				 && env.isValid(this.getArg(0))
				 && var_info.isConstant(this.getArg(1))) {
				reset(env);
				return this.getFanout(env);
			} else
				return 1000.0;
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitRangeContains(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static abstract class GeneralLookup extends Join.BI3 {
		public GeneralLookup (int v0, int v1, int v2) { super(v0, v1, v2); }
		public int getReadArgsNr() { return 1; }

		@Override
		public int
		getRangeInferenceParameter()
		{
			return 1;
		}


		@Override
		public boolean
		hasRandomAccess()
		{
			return true;
		}

		protected abstract double getDefaultSelectivity();
		protected abstract int getInnerSize();

		@Override public double
		getSelectivity(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(1))) {
				// index fixed
				return getSelectivityFromReadFlag(2, 1.0, this.getDefaultSelectivity());
			} else
				return getSelectivityFromReadFlag(2, 1.0, 1.0 / this.getSize(var_info, env));
		}

		@Override public double	getAccessCost(VarInfo var_info, Env env) { return 1.0; }

		@Override public double
		getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(1)))
				return 1.0;
			else if (var_info.isConstant(this.getArg(0))
				 && env.isValid(this.getArg(0))) {
				this.reset(env);
				return this.getInnerSize();
			} else
				return DEFAULT_SET_SIZE;
		}
	}

	public final static class LOOKUP extends GeneralLookup {
		public LOOKUP (int v0, int v1, int v2) { super(v0, v1, v2); }

		Map<Object, Object> map;
		Object[] pmap_data;
		Iterator<Map.Entry<Object, Object>> iterator;

		@Override
		public Join
		copyRecursively()
		{
			LOOKUP clone = (LOOKUP) this.copy();
			if (this.iterator != null)
				clone.iterator = map.entrySet().iterator();
			return clone;
		}

		@Override protected double getDefaultSelectivity() { return OBJECT_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.size(); }

		@SuppressWarnings("unchecked")
		public void
		reset(Env env)
		{
			this.map = (Map<Object, Object>) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				iterator = null;
			else
				iterator = map.entrySet().iterator();
		}

		@Override
		@SuppressWarnings("unchecked")
		public void
		resetForRandomAccess(Env env)
		{
			this.map = (Map<Object, Object>) env.getObject(this.v0);
			PMap<Object, Object> pmap;
			if (this.map instanceof PMap)
				pmap = (PMap) this.map;
			else {
				pmap = new PMap();
				pmap.putAll(this.map);
			}
			this.pmap_data = pmap.getRepresentation();
		}


		@Override
		public int
		getFanout(Env env)
		{
			return this.pmap_data.length >> 1;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
			offset <<= 1;
			final Object key = pmap_data[offset];
			if (key == null)
				return GETAT_NONE;
			final Object value = pmap_data[offset + 1];
		        return (env.unifyObject(this.v1, key)
				&& env.unifyObject(this.v2, value)) ? GETAT_ONE : GETAT_NONE;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.iterator == null) {
				boolean retval = env.unifyObject(this.v2, map.get(env.getObject(this.v1)));
				map = null;
				return retval;
			} else {
				while (iterator.hasNext()) {
					final Map.Entry<Object, Object> entry = iterator.next();
					if (env.unifyObject(this.v1, entry.getKey())
					    && env.unifyObject(this.v2, entry.getValue()))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitMapLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class ARRAY_LOOKUP_Int extends GeneralLookup {
		public ARRAY_LOOKUP_Int (int v0, int v1, int v2) { super(v0, v1, v2); }

		int[] map;
		int index;

		@Override
		public void
		resetForRandomAccess(Env env)
		{
			this.reset(env);
		}

		@Override
		public void
		reset(Env env)
		{
			this.map = (int[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		@Override protected double getDefaultSelectivity() { return INT_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyInt(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyInt(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final int obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyInt(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public final static class ARRAY_LOOKUP_Long extends GeneralLookup {
		public ARRAY_LOOKUP_Long (int v0, int v1, int v2) { super(v0, v1, v2); }

		long[] map;
		int index;

		@Override protected double getDefaultSelectivity() { return LONG_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyLong(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (long[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyLong(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final long obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyLong(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public final static class ARRAY_LOOKUP_Double extends GeneralLookup {
		public ARRAY_LOOKUP_Double (int v0, int v1, int v2) { super(v0, v1, v2); }

		double[] map;
		int index;

		@Override protected double getDefaultSelectivity() { return FLOAT_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyDouble(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (double[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyDouble(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final double obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyDouble(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class ARRAY_LOOKUP_Float extends GeneralLookup {
		public ARRAY_LOOKUP_Float (int v0, int v1, int v2) { super(v0, v1, v2); }

		float[] map;
		int index;

		@Override protected double getDefaultSelectivity() { return FLOAT_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyDouble(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (float[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyDouble(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final float obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyDouble(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class ARRAY_LOOKUP_Char extends GeneralLookup {
		public ARRAY_LOOKUP_Char (int v0, int v1, int v2) { super(v0, v1, v2); }

		char[] map;
		int index;


		@Override protected double getDefaultSelectivity() { return CHAR_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyInt(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (char[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyInt(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final char obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyInt(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class ARRAY_LOOKUP_Boolean extends GeneralLookup {
		public ARRAY_LOOKUP_Boolean (int v0, int v1, int v2) { super(v0, v1, v2); }

		boolean[] map;
		int index;

		@Override protected double getDefaultSelectivity() { return BOOLEAN_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyInt(this.v2, this.map[offset] ? 1 : 0)) ? GETAT_ONE : GETAT_NONE;
		}


		public void
		reset(Env env)
		{
			this.map = (boolean[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyInt(this.v2, map[env.getInt(this.v1)] ? 1 : 0);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final boolean obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyInt(this.v2, obj ? 1 : 0))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class ARRAY_LOOKUP_Short extends GeneralLookup {
		public ARRAY_LOOKUP_Short (int v0, int v1, int v2) { super(v0, v1, v2); }

		short[] map;
		int index;


		@Override protected double getDefaultSelectivity() { return SHORT_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyInt(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (short[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyInt(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final short obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyInt(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public final static class ARRAY_LOOKUP_Byte extends GeneralLookup {
		public ARRAY_LOOKUP_Byte (int v0, int v1, int v2) { super(v0, v1, v2); }

		byte[] map;
		int index;


		@Override protected double getDefaultSelectivity() { return BYTE_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyInt(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (byte[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyInt(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final byte obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyInt(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public final static class ARRAY_LOOKUP_Object extends GeneralLookup {
		public ARRAY_LOOKUP_Object (int v0, int v1, int v2) { super(v0, v1, v2); }

		Object[] map;
		int index;


		@Override protected double getDefaultSelectivity() { return OBJECT_JOIN_SELECTIVITY; }
		@Override protected int getInnerSize() { return this.map.length; }

		@Override
		public int
		getFanout(Env env)
		{
			return this.map.length;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int offset)
		{
		        return (env.unifyInt(this.v1, offset)
				&& env.unifyObject(this.v2, this.map[offset])) ? GETAT_ONE : GETAT_NONE;
		}

		public void
		reset(Env env)
		{
			this.map = (Object[]) env.getObject(this.v0);
			if (Env.isReadVar(this.v1))
				index = -1;
			else
				index = 0;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			if (map == null)
				return false;
			if (this.index == -1) {
				boolean retval = env.unifyObject(this.v2, map[env.getInt(this.v1)]);
				map = null;
				return retval;
			} else {
				while (index < map.length) {
					final Object obj = map[index];
					if (env.unifyInt(this.v1, index++)
					    && env.unifyObject(this.v2, obj))
						return true;
				}
				map = null;
				return false;
			}
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitArrayLookup(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static abstract class GeneralSize extends Predicate.Predicate2 {
		public GeneralSize(int v0, int v1) { super (v0, v1); }
		@Override
		public int getReadArgsNr() { return 1; }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public double	getAccessCost(VarInfo var_info, Env env) { return 1.0; }
	}

	public static class POLY_SIZE extends GeneralSize {
		public POLY_SIZE(int v0, int v1) { super(v0, v1); }
		@Override
		public boolean
		test(Env env)
		{
			final Object container = env.getObject(this.v0);
			final int size;
			if (container instanceof java.util.Map<?, ?>)
				size = ((java.util.Map<?, ?>)container).size();
			else if (container instanceof java.util.Set<?>)
				size = ((java.util.Set<?>)container).size();
			else
				size = java.lang.reflect.Array.getLength(container);

			return env.unifyInt(this.v1, size);
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitPolySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class SET_SIZE extends GeneralSize {
		public SET_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((Set<?>) env.getObject(this.v0)).size()); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitSetSize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class MAP_SIZE extends GeneralSize {
		public MAP_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((Map<?, ?>) env.getObject(this.v0)).size()); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitMapSize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class INT_ARRAY_SIZE extends GeneralSize {
		public INT_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((int[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class LONG_ARRAY_SIZE extends GeneralSize {
		public LONG_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((long[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class DOUBLE_ARRAY_SIZE extends GeneralSize {
		public DOUBLE_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((double[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class OBJECT_ARRAY_SIZE extends GeneralSize {
		public OBJECT_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((Object[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class BOOLEAN_ARRAY_SIZE extends GeneralSize {
		public BOOLEAN_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((boolean[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class SHORT_ARRAY_SIZE extends GeneralSize {
		public SHORT_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((short[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class CHAR_ARRAY_SIZE extends GeneralSize {
		public CHAR_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((char[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class FLOAT_ARRAY_SIZE extends GeneralSize {
		public FLOAT_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((float[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static class BYTE_ARRAY_SIZE extends GeneralSize {
		public BYTE_ARRAY_SIZE(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(this.v1, ((byte[]) env.getObject(this.v0)).length); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitArraySize(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

}