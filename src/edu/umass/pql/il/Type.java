/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.pql.*;
import java.util.*;

public abstract class Type extends Join implements VarConstants
{
	int v;
	private
	Type(int v)
	{
		this.v = v;
	}

	public  int
	getArgsNr()
	{
		return 1;
	}

	public  int
	getReadArgsNr()
	{
		return 0;
	}

	public  void
	setArg(int i, int v)
	{
		if (i == 0)
			this.v = v;
		else
			throw new RuntimeException("Invalid argument");
	}

	protected final boolean
	isCheck()
	{
		return Env.isReadVar(this.v);
	}

	public  int
	getArg(int i)
	{
		if (i == 0)
			return this.v;
		else
			throw new RuntimeException("Invalid argument");
	}

	public Join
	copyRecursively()
	{
		return this.copy();
	}

	public abstract void
	reset(Env env);

	public abstract boolean
	next(Env env);

	private static abstract class Coercion extends Predicate.Predicate2 {
		public int getReadArgsNr() { return 1; }
		public Coercion (int v0, int v1) { super(v0, v1); }
	}

	public static final class COERCE_Boolean extends Coercion {
		public COERCE_Boolean(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1, (env.getInt(v0) != 0)? 1 : 0); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, BOOLEAN_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitCoerceBoolean(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public static final class COERCE_Byte extends Coercion {
		public COERCE_Byte(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1,(byte) (env.getInt(v0))); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, BYTE_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitCoerceByte(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class COERCE_Short extends Coercion {
		public COERCE_Short(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1,(short) (env.getInt(v0))); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, SHORT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitCoerceShort(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class COERCE_Char extends Coercion {
		public COERCE_Char(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1,(char) (env.getInt(v0))); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, CHAR_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitCoerceChar(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class COERCE_Float extends Coercion {
		public COERCE_Float(int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyDouble(v1,(float) (env.getDouble(v0))); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, FLOAT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitCoerceFloat(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public static final class JAVA_TYPE extends Type
	{
		private Object ptype;
		private Class<?> type;
		private EnumSet<?> enum_set;
		private Iterator<?> it;
		private boolean done;

		public JAVA_TYPE(Object c, int v)
		{
			super(v);
			this.ptype = c;
		}

		public Object getType()
		{
			return this.ptype;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void
		reset(Env env)
		{
			if (this.ptype == null)
				throw new RuntimeException("Null type");

			if (this.ptype instanceof Class) {
				this.type = (Class<?>) this.ptype;

				if (Env.isWriteVar(this.v)) {
					if (this.type.isEnum()) {
						this.enum_set =(EnumSet) EnumSet.allOf((Class<Enum>)this.type);
						it = this.enum_set.iterator();
					} else
						throw new MissingFeatureException("iteration over non-enums");
				} else done = false;
			}
		}

		@Override
		public boolean
		next(Env env)
		{
			if (this.enum_set == null) {
				if (done)
					return false;
				done = true;
				return type.isInstance(env.getObject(this.v));
			} else
				if (it.hasNext()) {
					env.setObject(this.v, it.next());
					return true;
				} else return false;
		}

		@Override
		public double
		getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return 1.0;
			else
				return DEFAULT_SET_SIZE;
		}

		@Override
		public double
		getSelectivity(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return INSTANCE_OF_SELECTIVITY;
			else
				return 1.0;
		}

		@SuppressWarnings("unchecked")
		@Override
		public String
		getName()
		{
			if (this.type == null) {
				if (this.ptype instanceof Class)
					this.type = (Class<?>) this.ptype;
				else
					return "JAVA_TYPE<" + this.ptype + ">";
			}
			return "JAVA_TYPE<" + type.getSimpleName() + ">";
		}

		@Override
		public double
		getAccessCost(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return 1.0;
			else
				return 10.0;
		}

		@Override public String
		getConstructorName()
		{
			return null;
		}

		@Override
		public void accept(JoinVisitor visitor)
		{
			visitor.visitJavaType(this);
		}

                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }

		public int
		getReadArgsNr()
		{
			return 1; // this is not entirely correct, but depending on the backend it's probably our best choice for now.
		}

	}

	private static abstract class IntType extends Type
	{
		private int count;
		private boolean done;
		public IntType(int v) { super (v); }

		protected abstract int firstValue();
		protected abstract int lastValuePlusOne();

		protected double
		size()
		{
			return (lastValuePlusOne() * 1.0) - (1.0 * firstValue());
		}

		@Override
		public double
		getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return 1.0;
			else
				return size();
		}

		@Override
		public double
		getSelectivity(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return 1.0 / size();
			else
				return 1.0;
		}

		@Override
		public double
		getAccessCost(VarInfo var_info, Env env)
		{
				return 0.1;
		}

		@Override
		public final void
		reset(Env env)
		{
			this.done = false;
			this.count = this.firstValue();
		}

		protected boolean
		inRange(Env env)
		{
			final int x = env.getInt(this.v);
			return x >= firstValue() && x < lastValuePlusOne();
		}

		@Override
		public int
		getFanout(Env __)
		{
			return this.lastValuePlusOne() - this.firstValue();
		}

		@Override
		public boolean
		hasRandomAccess()
		{
			return true;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int index)
		{
			env.setInt(this.v, index + this.firstValue());
			return GETAT_ONE;
		}

		@Override
		public boolean
		next(Env env)
		{
			if (this.done)
				return false;
			if (this.isCheck()) {
				this.done = true;

				switch (Env.varType(this.v)) {
				case TYPE_INT:		return this.inRange(env);
				case TYPE_LONG:		return env.getLong(this.v) == (env.getInt(this.v))
								&& this.inRange(env);
				case TYPE_DOUBLE:	return env.getDouble(this.v) == (env.getInt(this.v))
								&& this.inRange(env);
				case TYPE_OBJECT:	{
					final Object o = env.getObject(this.v);
					if (o == null)
						return false;

					if (o instanceof Character)
						return this.inRange(env);
					if (o instanceof Boolean)
						return true;
					if (!(o instanceof Number))
						return false;

					final Number n = (Number) o;
					return ((n instanceof Integer)
						|| ((n instanceof Long) && (n.longValue() == (n.intValue())))
						|| ((n instanceof Double) && (n.doubleValue() == (n.intValue())))
						|| ((n instanceof Short))
						|| ((n instanceof Byte))
						|| ((n instanceof Float) && (n.floatValue() == (n.intValue()))))
						&& this.inRange(env);
				}
				default:		throw new RuntimeException("Unexpected type");
				}
			} else {
				env.setInt(this.v, this.count++);
				if (this.count == this.lastValuePlusOne())
					this.done = true;
				return true;
			}
		}

		@Override
		public String
		getConstructorName()
		{
			return this.getName();
		}
	}

	public static final class INT extends IntType
	{
		public INT(int v) { super(v); }

		protected int firstValue() { return Integer.MIN_VALUE; };
		protected int lastValuePlusOne() { return Integer.MIN_VALUE; };
		protected boolean inRange(Env __) { return true; }

		@Override
		protected double
		size()
		{
			return 1.0 +  (Integer.MAX_VALUE * 1.0) - (Integer.MIN_VALUE * 1.0);
		}


		@Override
		public int
		getFanout(Env __)
		{
			return Join.GETSIZE_INT;
		}

		@Override public void accept(JoinVisitor visitor) { visitor.visitTypeInt(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class LONG extends Type
	{
		public LONG(int v) { super(v); }
		private long count;
		private boolean done;

		protected final double
		size()
		{
			return (Long.MAX_VALUE * 1.0) - (1.0 * Long.MIN_VALUE);
		}

		@Override
		public double
		getSize(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return 1.0;
			else
				return size();
		}

		@Override
		public double
		getSelectivity(VarInfo var_info, Env env)
		{
			if (Env.isReadVar(this.getArg(0)))
				return 1.0 / size();
			else
				return 1.0;
		}

		@Override
		public double
		getAccessCost(VarInfo var_info, Env env)
		{
				return 0.1;
		}


		@Override
		public int
		getFanout(Env __)
		{
			return GETSIZE_LONG;
		}

		@Override
		public boolean
		hasRandomAccess()
		{
			return true;
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
		}

		@Override
		public int
		getAtIndex(Env env, int index)
		{
			env.setLong(this.v, index);
			return GETAT_ONE;
		}


		@Override
		public void
		reset(Env env)
		{
			this.done = false;
			this.count = Long.MIN_VALUE;
		}

		@Override
		public boolean
		next(Env env)
		{
			if (this.done)
				return false;
			if (this.isCheck()) {
				this.done = true;

				switch (Env.varType(this.v)) {
				case TYPE_INT:
				case TYPE_LONG:		return true;
				case TYPE_DOUBLE:	return env.getDouble(this.v) == (env.getLong(this.v));
				case TYPE_OBJECT:	{
					final Object o = env.getObject(this.v);
					if (o == null)
						return false;
					if (o instanceof Boolean)
						return true;
					if (o instanceof Character)
						return true;
					if (!(o instanceof Number))
						return false;

					final Number n = (Number) o;
					return ((n instanceof Integer)
						|| ((n instanceof Long))
						|| ((n instanceof Double) && (n.doubleValue() == (n.longValue())))
						|| ((n instanceof Short))
						|| ((n instanceof Byte))
						|| ((n instanceof Float) && (n.floatValue() == (n.longValue()))));
				}
				default:		throw new RuntimeException("Unexpected type");
				}
			} else {
				env.setLong(this.v, this.count++);
				if (this.count == Long.MIN_VALUE)
					this.done = true;
				return true;
			}
		}

		@Override
		public String
		getConstructorName()
		{
			return this.getName();
		}
		@Override public void accept(JoinVisitor visitor) { visitor.visitTypeLong(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}



	public static final class BOOLEAN extends IntType
	{
		public BOOLEAN(int v) { super(v); }

		protected int firstValue() { return 0; };
		protected int lastValuePlusOne() { return 2; };
		@Override public void accept(JoinVisitor visitor) { visitor.visitTypeBoolean(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public static final class BYTE extends IntType
	{
		public BYTE(int v) { super(v); }

		protected int firstValue() { return Byte.MIN_VALUE; };
		protected int lastValuePlusOne() { return Byte.MAX_VALUE + 1; };
		@Override public void accept(JoinVisitor visitor) { visitor.visitTypeByte(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class SHORT extends IntType
	{
		public SHORT(int v) { super(v); }

		protected int firstValue() { return Short.MIN_VALUE; };
		protected int lastValuePlusOne() { return Short.MAX_VALUE + 1; };

		@Override public void accept(JoinVisitor visitor) { visitor.visitTypeShort(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public static final class CHAR extends IntType
	{
		public CHAR(int v) { super(v); }

		protected int firstValue() { return Character.MIN_VALUE; };
		protected int lastValuePlusOne() { return Character.MAX_VALUE + 1; };

		@Override public void accept(JoinVisitor visitor) { visitor.visitTypeChar(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}
}