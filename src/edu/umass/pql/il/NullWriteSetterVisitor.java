/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.pql.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

/**
 * Takes a program representation that has read and write flags set properly and replaces all unused writes by wildcard writes.
 *
 */
public final class NullWriteSetterVisitor extends BackwardsSubstitutionVisitor
{
	private VarSet read_vars; // variables that are known to be defined

	/**
	 * Creates a new NullWriteSetterVisitor.
	 *
	 * @param read_variables: the initial set of read variables (output variables for the query).
	 */
	public NullWriteSetterVisitor(VarSet read_vars)
	{
		this.read_vars = read_vars;
	}

	@Override
	public int
	subst(int var)
	{
		if (Env.isReadVar(var))
			this.read_vars.insert(var);
		else if (!this.read_vars.contains(var))
			return Env.WILDCARD;
		return var;
	}
}
