/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.pql.container.*;
import edu.umass.pql.*;
import java.util.*;

/**
 * Constructs a map.
 */
public abstract class GenericMapReductor<K, V> extends DefaultReductor
{
	protected AMap<K, V> collector;
	protected Reductor inner_reductor = null; // used during `collect', i.e., during regular processing
	protected Reductor merge_reductor = EqualityReductor.DEFAULT; // used when merging cloned instances of this same reductor

	/**
	 * @param src_key map key
	 * @param src_value map value
	 */
	public GenericMapReductor(int dest, int src_key, int src_value)
	{
		super(dest, new int[] { src_key, src_value });
		if (src_value == INPUT_FROM_INNER_REDUCTOR)
			merge_reductor = null; // must be initialised by later call to setInnerReductor()
	}

	@Override
	public void
	setInnerReductor(Reductor inner)
	{
		for (int i = inner.getReadArgsNr(); i < inner.getArgsNr(); i++)
			inner.setArg(i, Env.WILDCARD);
		this.inner_reductor = inner;
		this.merge_reductor = inner;
	}

	@Override
	public Reductor
	getInnerReductor()
	{
		return this.inner_reductor;
	}

	@Override
	protected void
	reset()
	{
		this.collector = new PMap<K, V>();
	}

	protected void
	insertEntryIntoCollector(final K key, final V value)
	{
		this.collector.put(key, value);
	}

	@SuppressWarnings("unchecked")
	protected final void
	addEntry(final K key, V new_value)
	{
		if (this.collector.containsKey(key)) {
			final Object old_value = this.collector.get(key);
			try {
				new_value = (V) this.merge_reductor.merge(old_value, new_value);
			} catch (AmbiguousMapKeyException __) {
				throw new AmbiguousMapKeyException(key);
			}
		}
		this.insertEntryIntoCollector(key, new_value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void
	collect(Env env)
	{
		final K key = (K) env.getObject(this.getInputVar(0));
		if (this.inner_reductor == null) {
			final V value = (V) env.getObject(this.getInputVar(1));
			this.addEntry(key, value);
		} else {
			final V old_value = this.collector.get(key);
			this.collector.put(key, (V) this.inner_reductor.mergeBindingInto(env, old_value));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public GenericMapReductor<K, V>
	copy()
	{
		GenericMapReductor<K, V> retval = (GenericMapReductor<K, V>) super.copy();
		if (this.collector != null)
			retval.collector = this.collector.clone();

		if (this.inner_reductor == this.merge_reductor)
			retval.merge_reductor = retval.inner_reductor = this.inner_reductor.copy();
		else {
			if (this.inner_reductor != null && this.inner_reductor != EqualityReductor.DEFAULT)
				retval.inner_reductor = retval.inner_reductor.copy();
			if (this.merge_reductor != null && this.inner_reductor != EqualityReductor.DEFAULT)
				retval.merge_reductor = retval.merge_reductor.copy();
		}

		return retval;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void
	setAggregateInternal(Object other)
	{
		this.collector = (AMap<K, V>) other;
	}

	@Override
	public Object
	getAggregate()
	{
		return this.collector;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void
	absorbAggregate(Object other)
	{
		if (other instanceof AMap) {
			AMap nbhm = (AMap) other;
			final Object[] kvs = nbhm.getRepresentation();
			for (int offset = 0; offset < kvs.length - 1; offset += 2)
				if (kvs[offset] != null)
					this.addEntry((K) kvs[offset], (V) kvs[offset + 1]);
			return;
		}
		// this in particular will benefit from re-implementation...
		for (Map.Entry<K, V> entry : ((Map<K, V>) other).entrySet()) {
			this.addEntry(entry.getKey(), entry.getValue());
		}
	}
}
