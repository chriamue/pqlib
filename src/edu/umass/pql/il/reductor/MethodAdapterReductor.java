/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.*;
import java.lang.reflect.Method;

/**
 * A MethodAdapterReductor turns a method into a reductor, as follows:
 *
 * It expects the method to have a DefaultValue annotation of type T,
 * to take two arguments of type T, and to return a T.
 *
 */
public class MethodAdapterReductor extends DefaultReductor
{
	protected Method reductor;
	private Object[] args;
	private Object default_value;
	private Object reductor_object;


	public Object
	getReductorObject()
	{
		return this.reductor_object;
	}

	@Override
	public MethodAdapterReductor
	copy()
	{
		try {
			MethodAdapterReductor alt = (MethodAdapterReductor) super.copy();
			alt.args = new Object[2];
			return alt;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static MethodAdapterReductor
	make(int result_var, Object reductor, int input_var,
	     Class<?> type)
	{
		if (type == null) {
			if (reductor instanceof Method) {
				Method m = (Method) reductor;
				type = m.getReturnType();
			}
		}
		if (type == null)
			type = Object.class;

		if (type == boolean.class || type == Boolean.class)
			return new BooleanMethodReductor(result_var, reductor, input_var);

		if (type == int.class || type == Integer.class)
			return new IntegerMethodReductor(result_var, reductor, input_var);

		if (type == short.class || type == Short.class)
			return new ShortMethodReductor(result_var, reductor, input_var);

		if (type == byte.class || type == Byte.class)
			return new ByteMethodReductor(result_var, reductor, input_var);

		if (type == char.class || type == Character.class)
			return new CharacterMethodReductor(result_var, reductor, input_var);

		if (type == long.class || type == Long.class)
			return new LongMethodReductor(result_var, reductor, input_var);

		if (type == float.class || type == Float.class)
			return new FloatMethodReductor(result_var, reductor, input_var);

		if (type == double.class || type == Double.class)
			return new DoubleMethodReductor(result_var, reductor, input_var);

		return new MethodAdapterReductor(result_var, reductor, input_var);
	}

	/**
	 * @param result_var Variable to store the result in
	 * @param reductor Operator (static method) to collect
	 * @param input_vars additional parameters to the reductor
	 */
	protected
	MethodAdapterReductor(int result_var,
			      Object reductor,
			      int input_var)
	{
		super(result_var, new int[] { input_var });
		this.reductor_object = reductor;

		if (reductor instanceof Method)
			initialise_with((Method) reductor);
	}

	/**
	 * @param result_var Variable to store the result in
	 * @param reductor Operator (static method) to collect
	 * @param input_vars additional parameters to the reductor
	 */
	public MethodAdapterReductor(int result_var,
				     Method reductor,
				     int input_var)
	{
		super(result_var, new int[] { input_var });
		this.reductor_object = reductor;

		initialise_with(reductor);
	}

	public Object
	getDefault()
	{
		return null;
	}

	@Override public final void accept(ReductorVisitor visitor) { visitor.visitMethodAdapter(this); }

	void
	initialise_with(Method reductor)
	{
		this.reductor = reductor;

		this.default_value = getDefault();

		this.args = new Object[2];
	}

	@Override
	public final void
	absorbAggregate(Object input)
	{
		args[0] = this.result;
		args[1] = input;
		try {
			this.result = this.reductor.invoke(null, args);
		} catch (Exception e) {
			System.err.println("Failed reductor "+ this.reductor + " on ("
					   + args[0] + " : " + (args[0] == null ? "<nulltype>"  : args[0].getClass())
					   + ", "
					   + args[1] + " : " + (args[1] == null ? "<nulltype>"  : args[1].getClass())
					   + ")");
			throw new RuntimeException(e);
		}
	}

	@Override
	public void
	collect(Env env)
	{
		this.absorbAggregate(env.getObject(this.getInputVar(0)));
	}

	@Override
	protected void
	reset()
	{
		this.result = default_value;
	}

	@Override
	public void
	setAggregateInternal(Object other)
	{
		this.result = other;
	}

	@Override
	public boolean
	isMethodAdapter()
	{
		return true;
	}

	@Override
	public Object
	getAggregate()
	{
		return this.result;
	}
	@Override public String getName() { return "METHOD<" + (this.reductor == null? this.reductor_object.toString() : this.reductor.getName()) + "," + this.default_value + ">"; };
        @Override public int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }

	public static final class IntegerMethodReductor extends MethodAdapterReductor
	{
		public IntegerMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return new Integer(((DefaultValueInt)this.reductor.getAnnotation(DefaultValueInt.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(env.getInt(this.getInputVar(0))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class BooleanMethodReductor extends MethodAdapterReductor
	{
		public BooleanMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return (((DefaultValueBoolean)this.reductor.getAnnotation(DefaultValueBoolean.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(env.getInt(this.getInputVar(0)) == 0 ? false : true); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class ShortMethodReductor extends MethodAdapterReductor
	{
		public ShortMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return new Short(((DefaultValueShort)this.reductor.getAnnotation(DefaultValueShort.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(new Short((short) env.getInt(this.getInputVar(0)))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class CharacterMethodReductor extends MethodAdapterReductor
	{
		public CharacterMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return (((DefaultValueChar)this.reductor.getAnnotation(DefaultValueChar.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(new Character((char) env.getInt(this.getInputVar(0)))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class ByteMethodReductor extends MethodAdapterReductor
	{
		public ByteMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return new Byte(((DefaultValueByte)this.reductor.getAnnotation(DefaultValueByte.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(new Byte((byte) env.getInt(this.getInputVar(0)))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class LongMethodReductor extends MethodAdapterReductor
	{
		public LongMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return new Long(((DefaultValueLong)this.reductor.getAnnotation(DefaultValueLong.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(env.getLong(this.getInputVar(0))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class FloatMethodReductor extends MethodAdapterReductor
	{
		public FloatMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return new Float(((DefaultValueFloat)this.reductor.getAnnotation(DefaultValueFloat.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(new Float((float) env.getDouble(this.getInputVar(0)))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class DoubleMethodReductor extends MethodAdapterReductor
	{
		public DoubleMethodReductor(int a, Object b, int c) { super (a, b, c); }
		@Override public Object getDefault() { return new Double(((DefaultValueDouble)this.reductor.getAnnotation(DefaultValueDouble.class)).value()); }
		@Override public void collect(Env env) { this.absorbAggregate(env.getDouble(this.getInputVar(0))); }
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }

	}

}
