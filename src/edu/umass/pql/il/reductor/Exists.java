/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.*;

public final class Exists extends DefaultReductor
{
	private boolean success = false;

	public Exists(int output, int input) { super(output, new int[] { input }); }
	public final void reset() { success = false; };
	public final boolean succeed(Env env) { success |= env.getInt(this.getInputVar(0)) != 0; return success; }
	public final void collect(Env __) { throw new RuntimeException("Shouldn't be called"); }
	public final void absorbAggregate(Object other_aggregate) { if (other_aggregate != Integer.valueOf(0)) this.success = true; }
	public final Object getAggregate() { return this.success ? Integer.valueOf(1) : Integer.valueOf(0); }
	public final void setAggregateInternal(Object new_aggregate) { this.success = new_aggregate != Integer.valueOf(0); }
	@Override public final void accept(ReductorVisitor visitor) { visitor.visitExists(this); }        
        @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	@Override public String getName() { return "EXISTS"; };
}
