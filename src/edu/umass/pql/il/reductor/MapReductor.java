/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;

import edu.umass.bc.ReductorFlagsVisitor;

public final class MapReductor extends GenericMapReductor<Object, Object>
{
	public MapReductor(int dest, int src_key, int src_value) { super(dest, src_key, src_value); }
	@Override public String getName() { return "MAP"; };
	@Override public int getInnerReductorIndex() { return 1; }
	@Override public final void accept(ReductorVisitor visitor) { visitor.visitMap(this); }
        @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
}
