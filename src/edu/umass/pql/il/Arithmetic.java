/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.*;

public abstract class Arithmetic
{
	private static abstract class LogicalLiteral extends Predicate {
		public int getArgsNr() { return 0; }
		public int getReadArgsNr() { return 0; }
		public int getArg(int __) { throw new RuntimeException("No args!"); }
		public void setArg(int __, int ___) { throw new RuntimeException("No args!"); }
		public Join copy() { return this; }
		@Override public double getAccessCost(VarInfo var_info, Env env) { return 0.1; }
		@Override public String getConstructorName() { return null; }

	}

	public static final LogicalLiteral TRUE = new LogicalLiteral() {
			public final boolean
			test(Env __) { return true; }
			@Override public double	getSize(VarInfo var_info, Env env) { return 1.0; }
			@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0; }
			@Override public String getName() { return "TRUE"; };
			@Override public void accept(JoinVisitor visitor) { visitor.visitTrue(this); }
                        @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visitTRUE(); }
		};

	public static final LogicalLiteral FALSE = new LogicalLiteral() {
			public final boolean
			test(Env __) { return false; }
			@Override public double	getSize(VarInfo var_info, Env env) { return 0.1; }
			@Override public double	getSelectivity(VarInfo var_info, Env env) { return 0.0; }
			@Override public String getName() { return "FALSE"; };
			@Override public void accept(JoinVisitor visitor) { visitor.visitFalse(this); }
                        @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visitFALSE(); }
		};


	private static abstract class Arith extends Predicate.Predicate3 {
		public int getReadArgsNr() { return 2; }

		public Arith (int v0, int v1, int v2) { super(v0, v1, v2); }
	}

	public static final class ADD_Int extends Arith {
		public ADD_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) + env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitAdd(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class ADD_Long extends Arith {
		public ADD_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) + env.getLong(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitAdd(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class ADD_Double extends Arith {
		public ADD_Double (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyDouble(v2, env.getDouble(v0) + env.getDouble(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, FLOAT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitAdd(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}



	public static final class SUB_Int extends Arith {
		public SUB_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) - env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitSub(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class SUB_Long extends Arith {
		public SUB_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) - env.getLong(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitSub(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class SUB_Double extends Arith {
		public SUB_Double (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyDouble(v2, env.getDouble(v0) - env.getDouble(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, FLOAT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitSub(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}




	public static final class MUL_Int extends Arith {
		public MUL_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) * env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitMul(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class MUL_Long extends Arith {
		public MUL_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) * env.getLong(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitMul(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class MUL_Double extends Arith {
		public MUL_Double (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyDouble(v2, env.getDouble(v0) * env.getDouble(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, FLOAT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitMul(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}




	public static final class DIV_Int extends Arith {
		public DIV_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) {
			final int divisor = env.getInt(v1);
			if (divisor == 0)
				return false;
			else
				return env.unifyInt(v2, env.getInt(v0) / divisor);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitDiv(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class DIV_Long extends Arith {
		public DIV_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) {
			final long divisor = env.getLong(v1);
			if (divisor == 0l)
				return false;
			else
				return env.unifyLong(v2, env.getLong(v0) / divisor);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitDiv(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class DIV_Double extends Arith {
		public DIV_Double (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) {
			final double divisor = env.getDouble(v1);
			if (divisor == 0.0)
				return false;
			else
				return env.unifyDouble(v2, env.getDouble(v0) / divisor);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, FLOAT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitDiv(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


	public static final class MOD_Int extends Arith {
		public MOD_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) {
			final int divisor = env.getInt(v1);
			if (divisor == 0)
				return false;
			else
				return env.unifyInt(v2, env.getInt(v0) % divisor);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitMod(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class MOD_Long extends Arith {
		public MOD_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) {
			final long divisor = env.getLong(v1);
			if (divisor == 0l)
				return false;
			else
				return env.unifyLong(v2, env.getLong(v0) % divisor);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitMod(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}




	private static abstract class UnArith extends Predicate.Predicate2 {
		public int getReadArgsNr() { return 1; }

		public UnArith (int v0, int v1) { super(v0, v1); }

		@Override public double	getSize(VarInfo var_info, Env env) { return 1.0; }
		@Override public double getAccessCost(VarInfo var_info, Env env) { return 0.1; }
	}

	// negation: - operator
	public static final class NEG_Int extends UnArith {
		public NEG_Int (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1, - env.getInt(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeg(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class NEG_Long extends UnArith {
		public NEG_Long (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyLong(v1, - env.getLong(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeg(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class NEG_Double extends UnArith {
		public NEG_Double (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyDouble(v1, - env.getDouble(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, FLOAT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeg(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	// bit inversion: ~ operator
	public static final class BITINV_Int extends UnArith {
		public BITINV_Int (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1, ~ env.getInt(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitInv(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITINV_Long extends UnArith {
		public BITINV_Long (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyLong(v1, ~ env.getLong(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitInv(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	//
	public static final class BITOR_Int extends Arith {
		public BITOR_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) | env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitOr(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITOR_Long extends Arith {
		public BITOR_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) | env.getLong(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitOr(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	//
	public static final class BITAND_Int extends Arith {
		public BITAND_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) & env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitAnd(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITAND_Long extends Arith {
		public BITAND_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) & env.getLong(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitAnd(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	//
	public static final class BITXOR_Int extends Arith {
		public BITXOR_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) ^ env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitXor(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITXOR_Long extends Arith {
		public BITXOR_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) ^ env.getLong(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitXor(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	//
	public static final class BITSHL_Int extends Arith {
		public BITSHL_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) << env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitShl(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITSHL_Long extends Arith {
		public BITSHL_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) << env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitShl(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	//
	public static final class BITSHR_Int extends Arith {
		public BITSHR_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) >> env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitShr(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITSHR_Long extends Arith {
		public BITSHR_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) >> env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitShr(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	//
	public static final class BITSSHR_Int extends Arith {
		public BITSSHR_Int (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyInt(v2, env.getInt(v0) >>> env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, INT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitSshr(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}

	public static final class BITSSHR_Long extends Arith {
		public BITSSHR_Long (int v0, int v1, int v2) { super(v0, v1, v2); }
		public boolean test(Env env) { return env.unifyLong(v2, env.getLong(v0) >>> env.getInt(v1)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(2, 1.0, LONG_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitBitSshr(this); }
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}


}
