/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.*;

public abstract class ReductionImpl extends Reduction
{
	public static final boolean DEBUG = false;

	protected Join body;
	protected int initialised_count;
	protected Reductor[] reductors;

	static Thread[] thread_pool = initThreadPool();


	public void
	addReductor(Reductor new_reductor)
	{
		final Reductor[] old_reductors = this.reductors;
		this.reductors = new Reductor[old_reductors.length + 1];
		System.arraycopy(old_reductors, 0, this.reductors, 0, old_reductors.length);
		this.reductors[this.reductors.length - 1] = new_reductor;
	}

        public void
	removeReductor(Reductor remove_reductor)
	{
		final Reductor[] old_reductors = this.reductors;
		this.reductors = new Reductor[old_reductors.length - 1];
                int insertIndex = 0;
		for (Reductor old_reductor : old_reductors)
                    if (old_reductor != remove_reductor)
                        this.reductors[insertIndex++] = old_reductor;
	}

	public Join
	copyRecursively()
	{
		ReductionImpl clone = (ReductionImpl) this.copy();
		clone.reductors = new Reductor[reductors.length];
		clone.body = this.body.copyRecursively();

		for (int i = 0; i < this.reductors.length; i++)
			clone.reductors[i] = this.reductors[i].copy();

		return clone;
	}

	static Thread[]
	initThreadPool()
	{
		Thread[] tp = new Thread[Env.THREADS_NR];
		for (int i = 0; i < tp.length; i++)
			tp[i] = new Thread();
		return tp;
	}

	/**
	 * Provides the number of variables that the reductor reads from outside of its loop, if any.
	 *
	 * These variables are always at the beginning of the variable list.
	 *
	 * @return Number of read-variables that must be initialised for the `Reduce' that this reductor is contained in
	 */
	@Override
	public int
	getOuterReadArgsNr()
	{
		int acc = 0;
		for (Reductor r : reductors)
			acc += r.getOuterReadArgsNr();
		return acc;
	}

	@Override
	public int
	getArgsNr()
	{
		int acc = 0;
		for (Reductor r : reductors)
			acc += r.getArgsNr();
		return acc;
	}

	@Override
	public int
	getReadArgsNr()
	{
		int acc = 0;
		for (Reductor r : reductors)
			acc += r.getReadArgsNr();
		return acc;
	}

	@Override
	public double
	getSize(VarInfo var_info, Env env)
	{
		return 1.0;
	}

	@Override
	public double
	getSelectivity(VarInfo var_info, Env env)
	{
		return 1.0;
	}

	@Override
	public double
	getAccessCost(VarInfo var_info, Env env)
	{
		return this.body.getFullCost(var_info, env);
	}


	private int
	getVariableIndex(Reductor[] reductor /* return value */, int i)
	{
		int acc = 0;

		for (Reductor r : reductors) {
			if (i >= acc && i < acc + r.getOuterReadArgsNr()) {
				reductor[0] = r;
				return i - acc;
			}
			acc += r.getOuterReadArgsNr();
		}

		for (Reductor r : reductors) {
			if (i >= acc && i < acc + r.getInnerReadArgsNr()) {
				reductor[0] = r;
				return i - acc + r.getOuterReadArgsNr();
			}
			acc += r.getInnerReadArgsNr();
		}

		for (Reductor r : reductors) {
			if (i >= acc && i < acc + 1 /*r.getWritableArgsNr() */) {
				reductor[0] = r;
				return i - acc + r.getReadArgsNr();
			}
			acc += 1 /*r.getWritableArgsNr()*/;
		}

		throw new IllegalArgumentException("" + i);
	}

	@Override
	public int
	getArg(int i)
	{
		Reductor red[] = new Reductor[1];
		int index = getVariableIndex(red, i);

		return red[0].getArg(index);
	}

	@Override
	public void setArg(int i, int v)
	{
		Reductor red[] = new Reductor[1];
		int index = getVariableIndex(red, i);

		red[0].setArg(index, v);
	}

	/**
	 * Creates a new reduction
	 *
	 * @param reductors The set of reductors
	 * @param body The iterator producing all possible bindings
	 */
	public
	ReductionImpl(Reductor[] reductors, Join body)
	{
		this.body = body;
		this.reductors = reductors;
	}

	public Reductor[]
	getReductors()
	{
		return this.reductors;
	}

	public int
	getComponentsNr() { return 1; }

	public Join
	getComponentInternal(int i)
	{
		return this.body;
	}

	public void
	setComponentInternal(int i, Join new_block)
	{
		this.body = new_block;
	}

	@Override
	public void
	reset(Env env)
	{
            if (!RuntimeCreator.useRuntimeCreator) {
		for (Reductor r : this.reductors)
			r.setup(env);
		this.body.reset(env);
		this.initialised_count = this.reductors.length;
            }
	}

	public boolean
	next(Env env)
	{
                if (RuntimeCreator.useRuntimeCreator) {
                    RuntimeCreator.init( this, env );
                    return RuntimeCreator.test(env);
                }

		final long start_time = System.currentTimeMillis();
		final boolean retval = this.nextI(env);
		final long stop_time = System.currentTimeMillis();
		if (DEBUG)
			System.out.println("runtime: " + (stop_time - start_time));
		return retval;
	}

	private final boolean
	nextI(Env env)
	{
		if (this.initialised_count == 0)
			return false;

		final Join _body = this.body;
		final Reductor[] _reductors = this.reductors;

		_body.reset(env);
		outer:
		while (_body.next(env)) {
			for (Reductor r : _reductors)
				if (r.isEnabled()
				    && r.succeed(env)) {
					r.disable();
					//retval |= r.eval(env);
					if (0 == --this.initialised_count)
						break outer;
				}
		}
		this.initialised_count = 0;

		boolean retval = true;
		for (Reductor r : _reductors)
			if (!r.eval(env))
				retval = false;

		return retval;
	}

	@Override
	public String getName() { throw new UnsupportedOperationException(); }

	@Override
	public String
	toString()
	{
		StringBuffer buf = new StringBuffer("Reduce[");
		for (int i = 0; i < reductors.length; i++) {
			buf.append(this.reductors[i].toString());
			if (i < reductors.length - 1)
				buf.append("; ");
		}
		buf.append("]: ");
		buf.append(this.body.toString());
		return buf.toString();
	}

	@Override
	public void
	accept(JoinVisitor visitor)
	{
		visitor.visitReduction(this);
	}

        @Override
        public int accept (JoinFlagsVisitor visitor) {
            return visitor.visit(this);
        }

	public static final class Sequential extends ReductionImpl
	{
		public boolean
		isParallel()
		{
			return false;
		}

		public Sequential(Reductor[] reductors, Join body)
		{
			super(reductors, body);
		}
	}

	public abstract static class Parallel extends ReductionImpl
	{
		public Parallel(Reductor[] reductors, Join body)
		{
			super(reductors, body);
		}

		public boolean
		isParallel()
		{
			return true;
		}

		@Override
		public void
		reset(Env env)
		{
			this.initialised_count = this.reductors.length;

			if (!this.hasRunnerArray() || Env.THREADS_NR > this.getRunnersNr()) {
				this.allocateRunnerArray(Env.THREADS_NR);
				for (int i = 0; i < Env.THREADS_NR; i++) {
					final ReductionRunner runner = this.allocateRunner(i);
					runner.setMustCloneBodyAndEnv(i > 0);
					runner.setBody(this.reductors, this.body);

					runner.setThread(ReductionImpl.thread_pool[i]);
					runner.reset(env);
				}
			} else
				for (int i = 0; i < Env.THREADS_NR; i++) {
					final ReductionRunner runner = this.getRunner(i);
					runner.reset(env);
					runner.setMustCloneEnv(i > 0);
				}
		}

		// computes result, stores in `main' reductors
		protected final void
		doRun(Env env)
		{
			final long starttime = System.currentTimeMillis();
			final int size = Env.THREADS_NR;
			ReductionRunner.setThreadsToInitialise(size);
			// map
			for (int i = size - 1; i >= 0; i--) {
				this.getRunner(i).setEnv(env);
				this.getRunner(i).start();
			}
			final long setuptime = System.currentTimeMillis();
			for (int i = 0; i < size; i++)
				this.getRunner(i).join();

			final long jointime = System.currentTimeMillis();
			if (DEBUG) {
				for (int i = 0; i < size; i++)
					System.out.println("Thread #" + i + ":\t"
							   + this.getRunner(i).lastRuntime());
			}

			final long prereduce_time = System.currentTimeMillis();
			// reduce
			for (int delta = 1; delta < size; delta <<= 1) {
				final int stride = delta << 1;

				for (int i = 0; i < size - delta; i += stride)
					this.getRunner(i).mergeFrom(this.getRunner(i + delta));
				for (int i = 0; i < size - delta; i++)
					this.getRunner(i).join();
			}
			final long reducetime = System.currentTimeMillis();
			if (DEBUG) {
				System.out.println("Setup : " + (setuptime - starttime));
				System.out.println("Join  : " + (jointime - setuptime));
				System.out.println("Reduce: " + (reducetime - prereduce_time));
				System.out.println("!TOTAL: " + (reducetime - starttime));
			}
		}

		protected abstract boolean
		hasRunnerArray();

		protected abstract int
		getRunnersNr();

		protected abstract void
		allocateRunnerArray(int size);

		protected abstract ReductionRunner
		allocateRunner(int offset);

		protected abstract ReductionRunner
		getRunner(int offset);

		protected static abstract class ReductionRunner implements Runnable
		{
			public Reductor[] reductors;
			protected Thread thread;

			ReductionRunner other;
			boolean map_mode = true; // in this mode, we run `compute'.  Otherwise we merge with `other'.

			protected Env env;
			public Join body;
			private boolean must_clone_body, must_clone_env;

			private volatile static int initialisation_count;

			private static synchronized void
			decInitialisationCount()
			{
				--initialisation_count;
			}

			public void
			setMustCloneBodyAndEnv(boolean must_clone)
			{
				this.must_clone_body = this.must_clone_env = must_clone;
			}


			public void
			setMustCloneEnv(boolean must_clone)
			{
				this.must_clone_env = must_clone;
			}

			public void
			setThread(Thread t)
			{
				this.thread = t;
			}

			public void
			reset(Env env)
			{
				this.map_mode = true;
				for (Reductor r : this.reductors)
					r.setup(env);
				this.body.reset(env);
			}

			public void
			mergeFrom(ReductionRunner other)
			{
				this.other = other;
				this.map_mode = false;
				this.start();
			}

			public void
			setBody(Reductor[] reductors, Join b)
			{
				this.reductors = reductors;
				this.body = b;
			}

			public void
			setEnv(Env env)
			{
				this.env = env;
			}

			protected void
			cloneEnv()
			{
				this.env = this.env.copy();
			}

			protected void
			cloneBody()
			{
				synchronized (ReductionRunner.class) {
				final Reductor[] src_reductors = this.reductors;
				final Join b = this.body;

				this.reductors = new Reductor[src_reductors.length];
				for (int i = 0; i < src_reductors.length; i++)
					this.reductors[i] = src_reductors[i].copy();

				this.body = b.copyRecursively();
				}
			}

			public void
			setResult(boolean r)
			{
				this.result = r;
			}

			private boolean result;

			public boolean
			getResult()
			{
				return this.result;
			}

			abstract protected void
			compute();

			/**
			 * Set the numer of threads to run.  This count sets up a spin-lock so that all threads have initialised before starting computation.
			 *
			 */
			public static synchronized void
			setThreadsToInitialise(int count)
			{
				initialisation_count = count;
			}

			protected void
			waitToStart()
			{
				decInitialisationCount();
				while (initialisation_count > 0) {};
			}

			public void
			start()
			{
				this.thread = new Thread(this);

				this.thread.start();
			}

			public void
			join()
			{
				try {
					this.thread.join();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

			protected long last_start, last_finish, last_setup;

			public String
			lastRuntime()
			{
				return (last_finish - last_start)
					+ "\t= setup:\t"
					+ this.lastSetupTime()
					+ " + eval:\t"
					+ this.lastComputationTime();
			}

			public long
			lastSetupTime()
			{
				return last_setup - last_start;
			}

			public long
			lastComputationTime()
			{
				return last_finish - last_setup;
			}

			//@Override // commented out for JDK 1.5 bug
			public void
			run()
			{
				this.last_start = System.currentTimeMillis();
				if (this.map_mode) {
					if (this.must_clone_body)
						this.cloneBody();

					if (this.must_clone_env)
						this.cloneEnv();

					this.must_clone_body = this.must_clone_env = false;

					waitToStart();
					this.last_setup = System.currentTimeMillis();

					compute();
				} else {
					for (int i = 0; i < this.reductors.length; i++)
						this.reductors[i].absorbAggregate(this.other.reductors[i].getAggregate());
				}
				this.last_finish = System.currentTimeMillis();
			}
		}

		/*
		public boolean
		next(Env env)
		{
			if (this.initialised_count == 0)
				return false;

			final Join _binder = this.binder;
			final Join _evaluator = this.evaluator;
			final ArrayList<Reductor> _reductors = this.reductors;

			_binder.reset(env);

			throw new RuntimeException("Make me parallel!");
		}
		*/
	}

	public static final class ParallelSegmented extends Parallel
	{
		public ParallelSegmented(Reductor[] reductors, Join body)
		{
			super(reductors, body);
		}

		@Override
		protected boolean
		hasRunnerArray()
		{
			return this.runners != null;
		}

		@Override
		protected void
		allocateRunnerArray(int size)
		{
			this.runners = new SegmentedReductionRunner[size];
		}

		@Override
		protected int
		getRunnersNr()
		{
			return this.runners.length;
		}

		@Override
		protected ReductionRunner
		allocateRunner(int offset)
		{
			return this.runners[offset] = new SegmentedReductionRunner();
		}

		@Override
		protected ReductionRunner
		getRunner(int offset)
		{
			return this.runners[offset];
		}

		private SegmentedReductionRunner[] runners = null;

		@Override
		public Join
		copyRecursively()
		{
			ParallelSegmented retval = (ParallelSegmented) super.copyRecursively();
			retval.runners = null;
			return retval;
		}

		private final class SegmentedReductionRunner extends ReductionRunner
		{
			int start, end;

			public String
			toString()
			{
				return this.start + "--" + this.end;
			}

			public void
			setSpan(int start, int end)
			{
				this.start = start;
				this.end = end;
				if (DEBUG) {
					System.err.println("Running from: " + start + " to: " + end + ", in " + this.reductors[0]);
				}
			}


			// @Override
			// public String
			// lastRuntime()
			// {
			// 	return super.lastRuntime() +
			// 		"; reset:\t"
			// 		+ (this.last_finish - this.initial_reset_time);
			// }

			@Override
			public void
			compute()
			{
				final int _start = this.start;
				final int _end = this.end;
				final Join _body = this.body;
				final Env _env = this.env;
				final Reductor[] _reductors = this.reductors;

				//System.err.println("Checking range: " + _start + " -- " + _end);

				_body.resetForRandomAccess(_env);

				//outer:
				for (int i = _start; i <= _end; i++) {
					_body.moveToIndex(_env, i);
					int run_loop = _body.getAtIndex(_env, i);
					if (run_loop != Join.GETAT_NONE)
						do {
							for (Reductor r : _reductors)
								if (r.isEnabled()
								    && r.succeed(env)) {
									r.disable();
									//retval |= r.eval(env);
									//if (0 == --this.initialised_count) break outer;
								}
							if (run_loop != Join.GETAT_ONE)
								run_loop = _body.getAtIndex(_env, i);
						} while (run_loop == Join.GETAT_ANY);
				}
			}
		}

		public boolean
		next(Env env)
		{
			if (this.initialised_count == 0) // all reductors are done
				return false;

			final Join _body = this.body;
			final Reductor[] _reductors = this.reductors;

			_body.resetForRandomAccess(env);

			final int size = _body.getFanout(env);

			final int block_size = size / this.runners.length;
			int longer_blocks = size - (block_size * this.runners.length);

			int offset = 0;
			int stride_size = block_size + 1;
			for (int i = 0; i < this.runners.length; i++) {
				if (longer_blocks-- == 0)
					stride_size--;

				this.runners[i].setSpan(offset, offset + stride_size - 1);
				offset += stride_size;
			}

			doRun(env);

			boolean retval = true;
			for (Reductor r : _reductors)
				if (/*r.isEnabled() &&*/ !r.eval(env)) { // r.eval() has inverse semantics
					// --this.initialised_count;
					retval = false;
				}
			this.initialised_count = 0; // we only support reductors that yield a single binding
			return retval;
		}
	}


	public static final class ParallelStriped extends Parallel
	{
		public ParallelStriped(Reductor[] reductors, Join body)
		{
			super(reductors, body);
		}

		@Override
		protected boolean
		hasRunnerArray()
		{
			return this.runners != null;
		}

		@Override
		protected void
		allocateRunnerArray(int size)
		{
			this.runners = new StripedReductionRunner[size];
		}

		@Override
		protected int
		getRunnersNr()
		{
			return this.runners.length;
		}

		@Override
		protected ReductionRunner
		allocateRunner(int offset)
		{
			return this.runners[offset] = new StripedReductionRunner();
		}

		@Override
		protected ReductionRunner
		getRunner(int offset)
		{
			return this.runners[offset];
		}

		private StripedReductionRunner[] runners = null;

		@Override
		public Join
		copyRecursively()
		{
			ParallelStriped retval = (ParallelStriped) super.copyRecursively();
			retval.runners = null;
			return retval;
		}

		private final class StripedReductionRunner extends ReductionRunner
		{
			int start, end, stride;

			public String
			toString()
			{
				return this.start + "--" + this.end + " stride " + this.stride;
			}

			public void
			setSpan(int start, int stride, int end)
			{
				this.start = start;
				this.stride = stride;
				this.end = end;
				if (DEBUG) {
					System.err.println("Running from: " + start + " to: " + end + " at stride: "
							   + stride + ", in " + this.reductors[0]);
				}
			}

			// @Override
			// public String
			// lastRuntime()
			// {
			// 	return super.lastRuntime() +
			// 		"; reset:\t"
			// 		+ (this.last_finish - this.initial_reset_time);
			// }

			@Override
			public void
			compute()
			{
				final int _start = this.start;
				final int _end = this.end;
				final int _stride = this.stride;
				final Join _body = this.body;
				final Env _env = this.env;
				final Reductor[] _reductors = this.reductors;

				//System.err.println("Checking range: " + _start + " -- " + _end);

				_body.resetForRandomAccess(_env);

				//outer:
				for (int i = _start; i <= _end; i += _stride) {
					_body.moveToIndex(_env, i);
					int run_loop = _body.getAtIndex(_env, i);
					if (run_loop != Join.GETAT_NONE)
						do {
							for (Reductor r : _reductors)
								if (r.isEnabled()
								    && r.succeed(env)) {
									r.disable();
									//retval |= r.eval(env);
									//if (0 == --this.initialised_count) break outer;
								}
							if (run_loop != Join.GETAT_ONE)
								run_loop = _body.getAtIndex(_env, i);
						} while (run_loop == Join.GETAT_ANY);
				}
			}
		}

		public boolean
		next(Env env)
		{
			if (this.initialised_count == 0) // all reductors are done
				return false;

			final Join _body = this.body;
			//final Join _evaluator = this.evaluator;
			final Reductor[] _reductors = this.reductors;

			_body.resetForRandomAccess(env);

			final int size = _body.getFanout(env);

			final int stride_size = this.runners.length;

			for (int i = 0; i < this.runners.length; i++)
				this.runners[i].setSpan(i, stride_size, size - 1);

			doRun(env);

			boolean retval = true;
			for (Reductor r : _reductors)
				if (/*r.isEnabled() &&*/ !r.eval(env)) { // r.eval() has inverse semantics
					// --this.initialised_count;
					retval = false;
				}
			this.initialised_count = 0; // we only support reductors that yield a single binding
			return retval;
		}
	}

}
