/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.pql.*;

/**
 * Logical short-circuit negation
 *
 * @author creichen
 */
public class Not extends ControlStructure
{
	private Join body;
	private boolean done = false;

	public int getComponentsNr() { return 1; }

	public Join
	copyRecursively()
	{
		Not clone = (Not) this.copy();
		clone.body = body.copyRecursively();
		return clone;
	}

	public Join
	getComponentInternal(int __)
	{
		return body;
	}

	public void
	setComponentInternal(int __, Join new_body)
	{
		body = new_body;
	}

	public Not(Join body)
	{
		this.body = body;
	}

	public void
	reset(Env env)
	{
		body.reset(env);
		this.done = false;
	}

	public boolean
	next(Env env)
	{
		if (this.done)
			return false;
		this.done = true;
		return !(body.next(env));
	}

	@Override
	public boolean
	hasRandomAccess()
	{
		return body.hasRandomAccess();
	}

	@Override
	public int
	getFanout(Env env)
	{
		return body.getFanout(env);
	}

	@Override
	public void
	moveToIndex(Env env, int index)
	{
		body.moveToIndex(env, index);
	}

	@Override
	public int
	getAtIndex(Env env, int index)
	{
		if (this.body.getAtIndex(env, index) == GETAT_NONE)
			return GETAT_ONE;
		else
			return GETAT_NONE;
	}

	@Override
	public double
	getSize(VarInfo var_info, Env env)
	{
		return 1.0;
	}

	@Override
	public double
	getSelectivity(VarInfo var_info, Env env)
	{
		return 1.0 - this.getComponentInternal(0).getSelectivity(var_info, env);
	}

	@Override
	public double
	getAccessCost(VarInfo var_info, Env env)
	{
		return this.getComponentInternal(0).getAccessCost(var_info, env);
	}

	@Override
	public void
	accept(JoinVisitor visitor)
	{
		visitor.visitNot(this);
	}

        @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
}
