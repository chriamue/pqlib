/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

public interface VarConstants
{
	/** Polymorphic wildcard variable */
	public static final int __ = 0;

	public static final int TYPE_WILDCARD	= 0;	// fake type used by wildcards

	public static final int TYPE_INT	= 1;
	public static final int TYPE_LONG	= 2;
	public static final int TYPE_DOUBLE	= 3;
	public static final int TYPE_OBJECT	= 4;

	public static final int MIN_TYPE	= TYPE_INT;
	public static final int MAX_TYPE	= TYPE_OBJECT;

	public static final int VAR_READ_SHIFT = 0;
	public static final int VAR_READ_FLAG = 1 << 0;
	public static final int VAR_READ_SIZE = 1;

	// The `final' flag indicates (within a given context) that a varible is never read from after that context.
	public static final int VAR_FINAL_FLAG = 1 << VAR_READ_SIZE;
	public static final int VAR_FINAL_SIZE = 1;
	public static final int VAR_FINAL_SHIFT = VAR_READ_SIZE + VAR_FINAL_SIZE;

        // The `const' flag indicates that a varible is never write
	public static final int VAR_CONST_FLAG = 1 << VAR_READ_SIZE << VAR_FINAL_SIZE;
	public static final int VAR_CONST_SIZE = 1;
	public static final int VAR_CONST_SHIFT = VAR_READ_SIZE + VAR_FINAL_SIZE + VAR_CONST_SIZE;

	public static final int VAR_TABLE_MASK = 0x0F;
	public static final int VAR_TABLE_SHIFT = VAR_READ_SHIFT + VAR_CONST_SHIFT;
	public static final int VAR_TABLE_SIZE = 4;

	public static final int VAR_INDEX_SHIFT = VAR_TABLE_SHIFT + VAR_TABLE_SIZE;
	public static final int VAR_INDEX_MASK = 0xffffffff;
}

//compatible with old compiler-classes

/*public interface VarConstants
{
	public static final int _ = 0;

	public static final int TYPE_WILDCARD	= 0;	// fake type used by wildcards

	public static final int TYPE_INT	= 1;
	public static final int TYPE_LONG	= 2;
	public static final int TYPE_DOUBLE	= 3;
	public static final int TYPE_OBJECT	= 4;

	public static final int MIN_TYPE	= TYPE_INT;
	public static final int MAX_TYPE	= TYPE_OBJECT;

	public static final int VAR_READ_SHIFT = 0;
	public static final int VAR_READ_FLAG = 1 << 0;
	public static final int VAR_READ_SIZE = 1;

	// The `final' flag indicates (within a given context) that a varible is never read from after that context.
	public static final int VAR_FINAL_FLAG = 1 << VAR_READ_SIZE;
	public static final int VAR_FINAL_SIZE = 1;
	public static final int VAR_FINAL_SHIFT = VAR_READ_SIZE + VAR_FINAL_SIZE;

	public static final int VAR_TABLE_MASK = 0x07;
	public static final int VAR_TABLE_SHIFT = VAR_READ_SHIFT + VAR_FINAL_SIZE;
	public static final int VAR_TABLE_SIZE = 3;

	public static final int VAR_INDEX_SHIFT = VAR_TABLE_SHIFT + VAR_TABLE_SIZE;
	public static final int VAR_INDEX_MASK = 0xffffffff;

        public static final int VAR_CONST_FLAG = -1;
	public static final int VAR_CONST_SIZE = -1;
	public static final int VAR_CONST_SHIFT = -1;
}*/
