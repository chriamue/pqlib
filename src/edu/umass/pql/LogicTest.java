/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;
import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

public class LogicTest extends TestBase
{
    
	@Test
	public void
	testNotTrue()
	{
		setInts(1, 1, 2);
		testCount(0, PQLFactory.Not(PQLFactory.ADD_Int(i0r, i1r, i2r)));
	}

	@Test
	public void
	testNotFalse()
	{
		setInts(0, 1, 2);
		testCount(1, PQLFactory.Not(PQLFactory.ADD_Int(i0r, i1r, i2r)));
	}

	@Test
	public void
	testNotNotTrue()
	{
		setInts(1, 1, 2);
		testCount(1, PQLFactory.Not(PQLFactory.Not(PQLFactory.ADD_Int(i0r, i1r, i2r))));
	}

	@Test
	public void
	testNotNotFalse()
	{
		setInts(0, 1, 2);
		testCount(0, PQLFactory.Not(PQLFactory.Not(PQLFactory.ADD_Int(i0r, i1r, i2r))));
	}

	@Test
	public void
	testTrue()
	{
		testCount(1, PQLFactory.TRUE);
	}

	@Test
	public void
	testFalse()
	{
		testCount(0, PQLFactory.FALSE);
	}

	@Test
	public void
	testNoFalseTrue()
	{
		testCount(1, PQLFactory.NoFail(PQLFactory.TRUE));
	}

	@Test
	public void
	testNoFalseFalse()
	{
		testCount(1, PQLFactory.NoFail(PQLFactory.FALSE));
	}

	@Test
	public void
	testIfThenElseTrueTrue()
	{
		setInts(0, 1, 2);
		setLongs(0l, 0l, 0l);
		testCount(1, PQLFactory.IfThenElse(PQLFactory.ADD_Int(i1r, i1r, i2r), // true
					    PQLFactory.SUB_Int(i2r, i1r, l0w), // true
					    PQLFactory.SUB_Int(i2r, i1r, i0r))); // false
		assertEquals(1l, env.getLong(l0r));
	}

	@Test
	public void
	testIfThenElseFalseTrue()
	{
		setInts(0, 1, 2);
		setLongs(0l, 0l, 0l);
		testCount(1, PQLFactory.IfThenElse(PQLFactory.ADD_Int(i1r, i1r, i0r), // false
					    PQLFactory.SUB_Int(i2r, i1r, i0r), // false
					    PQLFactory.SUB_Int(i2r, i1r, l0w))); // true
		assertEquals(1l, env.getLong(l0r));
	}

	@Test
	public void
	testBoolUnification()
	{
		setInts(0, 1, 2);
		testCount(0, PQLFactory.ConjunctiveBlock(
							 PQLFactory.Bool(PQLFactory.LT_Int(i1r, i2r), i0w),
							 PQLFactory.Bool(PQLFactory.LT_Int(i2r, i1r), i0r)
							 ));
		testCount(1, PQLFactory.ConjunctiveBlock(
							 PQLFactory.Bool(PQLFactory.LT_Int(i1r, i2r), i0w),
							 PQLFactory.Bool(PQLFactory.LT_Int(i1r, i2r), i0r)
							 ));
	}

	@Test
	public void
	testIfThenElseTrueFalse()
	{
		setInts(0, 1, 2);
		testCount(0, PQLFactory.IfThenElse(PQLFactory.ADD_Int(i1r, i1r, i2r), // true
					    PQLFactory.SUB_Int(i2r, i1r, i0r), // false
					    PQLFactory.SUB_Int(i2r, i1r, l0w))); // true
	}


	@Test
	public void
	testIfThenElseFalseFalse()
	{
		setInts(0, 1, 2);
		testCount(0, PQLFactory.IfThenElse(PQLFactory.ADD_Int(i1r, i1r, i0r), // false
					    PQLFactory.SUB_Int(i2r, i1r, i1r), // true
					    PQLFactory.SUB_Int(i2r, i1r, i0r))); // false
	}

	@Test
	public void
	testIfThenElseCountTrue()
	{
		Set<Integer> set1 = new HashSet<Integer>();
		Set<Integer> set2 = new HashSet<Integer>();
		Set<Integer> set3 = new HashSet<Integer>();
		setInts(0, 1, 2);
		setObjects(set1, set2, set3);
		set1.add(1);
		set1.add(2);
		set1.add(3);
		set2.addAll(set1);
		set2.add(4);
		set3.addAll(set2);
		set3.add(5);

		testCount(4, PQLFactory.IfThenElse(PQLFactory.CONTAINS(o0r, i1r),
						   PQLFactory.CONTAINS(o1r, i2w),
						   PQLFactory.CONTAINS(o2r, i2w)));

		set1.clear();
		testCount(5, PQLFactory.IfThenElse(PQLFactory.CONTAINS(o0r, i1r),
						   PQLFactory.CONTAINS(o1r, i2w),
						   PQLFactory.CONTAINS(o2r, i2w)));
	}

	@Test
	public void
	testOrTT()
	{
		testCount(1, PQLFactory.Or(PQLFactory.TRUE, PQLFactory.TRUE));
	}

	@Test
	public void
	testOrTF()
	{
		testCount(1, PQLFactory.Or(PQLFactory.TRUE, PQLFactory.FALSE));
	}

	@Test
	public void
	testOrFT()
	{
		testCount(1, PQLFactory.Or(PQLFactory.FALSE, PQLFactory.TRUE));
	}

	@Test
	public void
	testOrFF()
	{
		testCount(0, PQLFactory.Or(PQLFactory.FALSE, PQLFactory.FALSE));
	}


	@Test
	public void
	testBoolTrue()
	{
		setInts(1, 1, 2);
		testCount(1, PQLFactory.Bool(PQLFactory.ADD_Int(i0r, i1r, i2r), i3w));
		assertEquals(1, env.getInt(i3w));
	}

	@Test
	public void
	testBoolFalse()
	{
		setInts(0, 1, 2);
		testCount(1, PQLFactory.Bool(PQLFactory.ADD_Int(i0r, i1r, i2r), i3w));
		assertEquals(0, env.getInt(i3w));
	}


	@Test public void testBoolForallManifestSuccess() { paratest("para_testBoolForallManifestSuccess"); }

	public void
	para_testBoolForallManifestSuccess()
	{
		int[] values = new int[] { 1, 1, 72, 2, 1 };
		setInts(0, 1, 2);
		setObjects(values, null, null);
		testCount(1, PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i4r, i1r), // i1r:  comparison check
					       PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i2w),
					       PQLFactory.Bool(PQLFactory.LT_Int(i0r, i2r), i4w)));
	}

	@Test public void testBoolForallManifestFailure() { paratest("para_testBoolForallManifestFailure"); }

	public void
	para_testBoolForallManifestFailure()
	{
		int[] values = new int[] { 1, 1, 72, 2, 1, -17 };
		setInts(0, 0, 2);
		setObjects(values, null, null);
		testCount(1, PQLFactory.Reduction(PQLFactory.Reductors.FORALL(i4r, i1r), // i1r:  comparison check
					       PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i2w),
					       PQLFactory.Bool(PQLFactory.LT_Int(i0r, i2r), i4w)));
	}

	@Test public void testBoolExistsManifestSuccess() { paratest("para_testBoolExistsManifestSuccess"); }

	public void
	para_testBoolExistsManifestSuccess()
	{
		int[] values = new int[] { -1, -1, -72, -2, -1, 17 };
		setInts(0, 1, 2);
		setObjects(values, null, null);
		testCount(1, PQLFactory.Reduction(PQLFactory.Reductors.EXISTS(i4r, i1r), // i1r:  comparison check
					       PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i2w),
					       PQLFactory.Bool(PQLFactory.LT_Int(i0r, i2r), i4w)));
	}

	@Test public void testBoolExistsManifestFailure() { paratest("para_testBoolExistsManifestFailure"); }

	public void
	para_testBoolExistsManifestFailure()
	{
		int[] values = new int[] { -1, -1, -72, -2, -1, -17 };
		setInts(0, 0, 2);
		setObjects(values, null, null);
		testCount(1, PQLFactory.Reduction(PQLFactory.Reductors.EXISTS(i4r, i1r), // i1r:  comparison check
					       PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i2w),
					       PQLFactory.Bool(PQLFactory.LT_Int(i0r, i2r), i4w)));
	}
}