/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import org.junit.*;
import static org.junit.Assert.*;

public class TypeTest extends TestBase
{
	enum A {
		Alpha, Beta, Gamma, Delta
	};

	@Test
	public void
	testEnumContains()
	{
		setObjects("foo", null, A.Beta);
		testCount(0, PQLFactory.JAVA_TYPE(A.class, o0r));
		testCount(0, PQLFactory.JAVA_TYPE(A.class, o1r));
		testCount(1, PQLFactory.JAVA_TYPE(A.class, o2r));
	}

	@Test
	public void
	testEnumIterate()
	{
		testCount(4, PQLFactory.JAVA_TYPE(A.class, o0w));
	}

	@Test
	public void
	testIntContains()
	{
		setObjects("foo", null, new Integer(10));
		setLongs(1, -1000000000000l, 0);
		testCount(0, PQLFactory.INT(o0r));
		testCount(0, PQLFactory.INT(o1r));
		testCount(1, PQLFactory.INT(o2r));
		testCount(1, PQLFactory.INT(i2r));
		testCount(1, PQLFactory.INT(l0r));
		testCount(0, PQLFactory.INT(l1r));
	}

	@Test
	public void
	testIntIterate()
	{
		testCountHuge(PQLFactory.INT(i0w));
	}

	@Test
	public void
	testLongContains()
	{
		setObjects("foo", null, new Long(10l));
		setLongs(1, -1000000000000l, 0);
		setDoubles(0.0, -1.5, 0.0);
		testCount(0, PQLFactory.LONG(o0r));
		testCount(0, PQLFactory.LONG(o1r));
		testCount(1, PQLFactory.LONG(o2r));
		testCount(1, PQLFactory.LONG(i2r));
		testCount(1, PQLFactory.LONG(l0r));
		testCount(1, PQLFactory.LONG(l1r));
		testCount(1, PQLFactory.LONG(d0r));
		testCount(0, PQLFactory.LONG(d1r));
		setObjects(new Character((char) 0), new Character('x'), null);
		testCount(1, PQLFactory.LONG(o0r));
		testCount(1, PQLFactory.LONG(o1r));
	}

	@Test
	public void
	testLongIterate()
	{
		testCountHuge(PQLFactory.LONG(l0w));
	}

	@Test
	public void
	testBooleanContains()
	{
		setObjects("foo", null, new Integer(1));
		setLongs(1, -1000000000000l, 0);
		setDoubles(1.0, 0.25, 0.0);
		testCount(0, PQLFactory.BOOLEAN(o0r));
		testCount(0, PQLFactory.BOOLEAN(o1r));
		testCount(1, PQLFactory.BOOLEAN(o2r));
		testCount(1, PQLFactory.BOOLEAN(l0r));
		testCount(0, PQLFactory.BOOLEAN(l1r));
		testCount(1, PQLFactory.BOOLEAN(l2r));
		testCount(1, PQLFactory.BOOLEAN(d0r));
		testCount(0, PQLFactory.BOOLEAN(d1r));
		testCount(1, PQLFactory.BOOLEAN(d2r));
		setObjects(new Character((char) 0), new Character('x'), null);
		testCount(1, PQLFactory.BOOLEAN(o0r));
		testCount(0, PQLFactory.BOOLEAN(o1r));
	}


	@Test
	public void
	testBooleanIterate()
	{
		testCount(2, PQLFactory.BOOLEAN(i0w));
	}


	@Test
	public void
	testByteContains()
	{
		setObjects("foo", null, new Integer(17));
		setLongs(128, -1000000000000l, 0);
		setDoubles(-128.0, 0.25, 127.0);
		testCount(0, PQLFactory.BYTE(o0r));
		testCount(0, PQLFactory.BYTE(o1r));
		testCount(1, PQLFactory.BYTE(o2r));
		testCount(0, PQLFactory.BYTE(l0r));
		testCount(0, PQLFactory.BYTE(l1r));
		testCount(1, PQLFactory.BYTE(l2r));
		testCount(1, PQLFactory.BYTE(d0r));
		testCount(0, PQLFactory.BYTE(d1r));
		testCount(1, PQLFactory.BYTE(d2r));
		setObjects(Boolean.TRUE, Boolean.FALSE, new Character((char) 17));
		testCount(1, PQLFactory.BYTE(o0r));
		testCount(1, PQLFactory.BYTE(o1r));
		testCount(1, PQLFactory.BYTE(o2r));
	}


	@Test
	public void
	testByteIterate()
	{
		testCount(256, PQLFactory.BYTE(i0w));
	}


	@Test
	public void
	testShortContains()
	{
		setObjects("foo", null, new Integer(17));
		setLongs(32768, -1000000000000l, 0);
		setDoubles(-32768.0, 0.25, 32676.0);
		testCount(0, PQLFactory.SHORT(o0r));
		testCount(0, PQLFactory.SHORT(o1r));
		testCount(1, PQLFactory.SHORT(o2r));
		testCount(0, PQLFactory.SHORT(l0r));
		testCount(0, PQLFactory.SHORT(l1r));
		testCount(1, PQLFactory.SHORT(l2r));
		testCount(1, PQLFactory.SHORT(d0r));
		testCount(0, PQLFactory.SHORT(d1r));
		testCount(1, PQLFactory.SHORT(d2r));
	}


	@Test
	public void
	testShortIterate()
	{
		testCount(65536, PQLFactory.SHORT(i0w));
	}


	@Test
	public void
	testCharContains()
	{
		setObjects(new Character('x'), "zeta", new Integer(17));
		setLongs(65536, -1000000000000l, 0);
		setDoubles(-1.0, 0.25, 65535.0);
		testCount(1, PQLFactory.CHAR(o0r));
		testCount(0, PQLFactory.CHAR(o1r));
		testCount(1, PQLFactory.CHAR(o2r));
		testCount(0, PQLFactory.CHAR(l0r));
		testCount(0, PQLFactory.CHAR(l1r));
		testCount(1, PQLFactory.CHAR(l2r));
		testCount(0, PQLFactory.CHAR(d0r));
		testCount(0, PQLFactory.CHAR(d1r));
		testCount(1, PQLFactory.CHAR(d2r));
	}


	@Test
	public void
	testCharIterate()
	{
		testCount(65536, PQLFactory.CHAR(i0w));
	}


	public class B
	{
	}
	public class BC extends B
	{
	}

	final Object b0 = new B();
	final Object b1 = new BC();

	@Test
	public void
	testTypeCheck()
	{
		setObjects("foo", null, b0);
		testCount(0, PQLFactory.JAVA_TYPE(B.class, o0r));
		testCount(0, PQLFactory.JAVA_TYPE(B.class, o1r));
		testCount(1, PQLFactory.JAVA_TYPE(B.class, o2r));
		setObjects("foo", null, b1);
		testCount(1, PQLFactory.JAVA_TYPE(B.class, o2r));
	}

	@Test
	public void
	testTypeIter()
	{
		if (Query.supportsHeapSearch())
			testCount(2, PQLFactory.JAVA_TYPE(B.class, o0w));
		else {
			try {
				testCount(2, PQLFactory.JAVA_TYPE(B.class, o0w));
				fail();
			} catch (MissingFeatureException __) {}
		}
	}


	@Test
	public void
	testBooleanCoerce()
	{
		setInts(0, 3, -17);
		testCount(1, PQLFactory.COERCE_Boolean(i0r, i4w));
		assertEquals(0, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Boolean(i1r, i4w));
		assertEquals(1, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Boolean(i2r, i4w));
		assertEquals(1, env.getInt(i4r));
	}


	@Test
	public void
	testByteCoerce()
	{
		setInts(0, 256, -3);
		testCount(1, PQLFactory.COERCE_Byte(i0r, i4w));
		assertEquals(0, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Byte(i1r, i4w));
		assertEquals(0, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Byte(i2r, i4w));
		assertEquals(-3, env.getInt(i4r));
	}

	@Test
	public void
	testShortCoerce()
	{
		setInts(0, 65535, 257);
		testCount(1, PQLFactory.COERCE_Short(i0r, i4w));
		assertEquals(0, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Short(i1r, i4w));
		assertEquals(-1, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Short(i2r, i4w));
		assertEquals(257, env.getInt(i4r));
	}

	@Test
	public void
	testFloatCoerce()
	{
		setDoubles(0.25, 1.0000000025, 0.0);
		testCount(1, PQLFactory.COERCE_Float(d0r, d2w));
		assertEquals(0.25, env.getDouble(d2r), 0.0);
		testCount(1, PQLFactory.COERCE_Float(d1r, d2w));
		assertEquals(1.0000000025, env.getDouble(d1r), 0.0);
		assertEquals(1.0, env.getDouble(d2r), 0.0);
	}

	@Test
	public void
	testCharCoerce()
	{
		setInts(0, 65534, -1);
		testCount(1, PQLFactory.COERCE_Char(i0r, i4w));
		assertEquals(0, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Char(i1r, i4w));
		assertEquals(65534, env.getInt(i4r));
		testCount(1, PQLFactory.COERCE_Char(i2r, i4w));
		assertEquals(65535, env.getInt(i4r));
	}

}
