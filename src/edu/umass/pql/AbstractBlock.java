/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.*;

import java.util.ArrayList;

/**
 * Conjunctive and disjunctive blocks.  Concrete implementatoins are inner classes
 *
 * @author creichen
 */
public abstract class AbstractBlock extends ControlStructure
{
	protected Join[] elements;

	public int
	getArgsNr() { return 0; };
	public int
	getReadArgsNr() { return 0; };
	public void
	setArg(int i, int v) { throw new RuntimeException(); };
	public int
	getArg(int i) { throw new RuntimeException(); };

	@Override
	public Join
	copyRecursively()
	{
		AbstractBlock clone = (AbstractBlock) this.copy();
		clone.elements = new Join[this.elements.length];
		for (int i = 0; i < this.elements.length; i++)
			clone.elements[i] = this.elements[i].copyRecursively();
		return clone;
	}

	// public
	// Block(ArrayList<Join> it)
	// {
	// 	this.elements = it;
	// }

	public
	AbstractBlock(java.util.Collection<Join> it)
	{
		this.elements = new Join[it.size()];
		int offset = 0;
		for (Join j : it)
			this.elements[offset++] = j;
	}

	public
	AbstractBlock(Join ... it)
	{
		this.elements = it;
	}

	public Join[]
	getComponents()
	{
		return this.elements;
	}

	@Override
	public int
	getComponentsNr()
	{
		return this.elements.length;
	}

	@Override
	public Join
	getComponentInternal(int i)
	{
		return this.elements[i];
	}

	@Override
	public void
	setComponentInternal(int i, Join newblock)
	{
		this.elements[i] = newblock;
	}

	public void
	removeComponentInternal(int i)
	{
                Join [] old_elements = this.elements;
                this.elements = new Join[this.elements.length-1];
                int insert_index = 0;
                for (int j=0; j<this.elements.length+1; j++)
                    if (i != j)
                        this.elements[insert_index++] = old_elements[j];
	}


	@Override
	public String getName() { throw new UnsupportedOperationException(); }

	abstract boolean
	isCompatibleBlock(Join j);

	abstract AbstractBlock
	make(java.util.Collection<Join> elements);

	public static Join
	flattenBlock(Join block)
	{
		if (block instanceof AbstractBlock)
			return ((AbstractBlock) block).flatten();
		else
			return block;
	}

	AbstractBlock
	flatten()
	{
		ArrayList<Join> elements = new ArrayList<Join>();
		for (int i = 0; i < this.getComponentsNr(); i++) {
			if (this.isCompatibleBlock(this.getComponent(i))) {
				AbstractBlock bl = ((AbstractBlock) this.getComponent(i)).flatten();
				for (int j = 0; j < bl.getComponentsNr(); j++)
					elements.add(bl.getComponent(j));
			} else
				elements.add(this.getComponent(i));
		}

		return this.make(elements);
	}

	public static abstract class PreDisjunctive extends AbstractBlock
	{
		public
		PreDisjunctive(java.util.Collection<Join> it)
		{
			super(it);
		}

		public
		PreDisjunctive(Join ... it)
		{
			super (it);
		}

		@Override
		public void
		addWriteDependencies(VarSet set)
		{
			if (this.getComponentsNr() < 1)
				return;

			VarSet vs = new VarSet();
			this.getComponent(0).addWriteDependencies(vs);

			VarSet vs2 = new VarSet();

			for (int j = 1; j < this.getComponentsNr(); j++) {
				final Join join = this.getComponentInternal(j);

				vs2.clear();
				join.addWriteDependencies(vs2);
				vs.retainAll(vs2);
			}
			set.addAll(vs);
		}

		@Override
		public boolean
		hasRandomAccess()
		{
			return this.elements.length > 1
				|| (this.elements.length == 1 && this.elements[0].hasRandomAccess());
		}

		@Override
		public int
		getFanout(Env env)
		{
			int size = 0;
			for (int i = 0; i < this.getComponentsNr(); i++) {
				final Join j = this.getComponentInternal(i);
				if (j.hasRandomAccess())
					size += j.getFanout(env);
				else
					size += 1;
			}

			return size;
		}

		@Override
		public void
		resetForRandomAccess(Env env)
		{
			this.offset_table = new int[this.elements.length];
			int total_offset = 0;
			for (int i = 0; i < this.elements.length; i++) {
				this.offset_table[i] = total_offset;
				final Join j = this.elements[i];
				if (j.hasRandomAccess()) {
					j.resetForRandomAccess(env);
					total_offset += (int) j.getFanout(env);
				} else {
					// System.err.println("slow access to inner parts. :-(");
					j.reset(env);
					total_offset += 1;
				}
			}
		}

		int[] offset_table = null;
		Join current_join = null;
		int current_base_index;

		@Override
		public void
		moveToIndex(Env env, int index)
		{
			// FIXME: optimise to O(1) in the common case!

			int start = 0;
			int stop = offset_table.length - 1;

			while (start < stop) {
				int middle = start + ((stop + 1 - start) >> 1);
				if (index < this.offset_table[middle])
					stop = middle - 1;
				else
					start = middle;
			}

			// System.err.println("Relocating to index " + index);
			this.current_join = this.elements[start];
			final int relative_index = this.current_base_index = this.offset_table[start];

			if (this.current_join.hasRandomAccess())
				this.current_join.moveToIndex(env, relative_index);
		}

		@Override
		public int
		getAtIndex(Env env, int index)
		{
			if (this.current_join.hasRandomAccess()) {
				return this.current_join.getAtIndex(env, index - this.current_base_index);
			}
			else {
				// System.err.println("Checking slowly: ");
				if (this.current_join.next(env)) {
					// System.err.println("Sub-Some success.");
					return GETAT_ANY;
				} else {
					// System.err.println("Sub-fail.");
					return GETAT_NONE;
				}
			}
		}

		public void
		reset(Env env)
		{
			int total_offset = 0;
			for (int i = 0; i < this.elements.length; i++)
				this.elements[i].reset(env);
			this.next_join_index = 0;

			this.current_join = null;
		}

		int next_join_index;

		public boolean
		next(Env env)
		{
			do {
				if (this.current_join == null) {
					if (this.next_join_index < this.elements.length)
						this.current_join = this.elements[this.next_join_index++];
					else
						return false;
				}

			if (this.current_join.next(env))
					return true;
			else
					this.current_join = null;
			} while (true);
		}

		@Override
		public double
		getSize(VarInfo var_info, Env env)
		{
			double size = 0.0;
			for (int i = 0; i < this.getComponentsNr(); i++)
				size += this.getComponentInternal(i).getSize(var_info, env);

			return size;
		}

		final static double SIZE_THRESHOLD = 0.0001;

		@Override
		public double
		getSelectivity(VarInfo var_info, Env env)
		{
			final double size = this.getSize(var_info, env);
			if (size <= SIZE_THRESHOLD)
				return 0.0;

			double weighted_selectivity = 0.0;
			for (int i = 0; i < this.getComponentsNr(); i++) {
				final Join j = this.getComponentInternal(i);
				final double local_size = j.getSize(var_info, env);
				weighted_selectivity += j.getSelectivity(var_info, env) * local_size;
			}

			return weighted_selectivity / size;
		}

		@Override
		public double
		getAccessCost(VarInfo var_info, Env env)
		{
			final double size = this.getSize(var_info, env);
			if (size < 0.00001)
				return 1.0;

			double weighted_access_cost = 0.0;
			for (int i = 0; i < this.getComponentsNr(); i++) {
				final Join j = this.getComponentInternal(i);
				weighted_access_cost += j.getAccessCost(var_info, env) * j.getSize(var_info, env);
			}

			return weighted_access_cost / size;
		}


		@Override
		public String
		toString()
		{
			StringBuffer buf = new StringBuffer("[");
			for (int i = 0; i < this.getComponentsNr(); i++) {
				buf.append(" ");
				buf.append(this.getComponent(i));
				if (i + 1 < this.getComponentsNr())
					buf.append(" |");
			}
			buf.append(" ]");
			return buf.toString();
		}


		@Override
		boolean
		isCompatibleBlock(Join j)
		{
			return j instanceof Disjunctive;
		}

		@Override
		AbstractBlock
		make(java.util.Collection<Join> elements)
		{
			return new Disjunctive(elements);
		}

                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }

	}

	public static abstract class PreConjunctive extends AbstractBlock
	{
		private int offset;

		public
		PreConjunctive(java.util.Collection<Join> it)
		{
			super(it);
		}

		public
		PreConjunctive(Join ... it)
		{
			super (it);
		}

		@Override
		public void
		addWriteDependencies(VarSet set)
		{
			for (int j = 0; j < this.getComponentsNr(); j++) {
				final Join join = this.getComponentInternal(j);

				for (int i = join.getReadArgsNr(); i < join.getArgsNr(); i++) {
					final int v = join.getArg(i);
					set.insert(join.getArg(i));
				}
			}
		}


		@Override
		public boolean
		hasRandomAccess()
		{
			return this.elements.length > 0
				&& this.elements[0].hasRandomAccess();
		}

		@Override
		public int
		getFanout(Env env)
		{
			return this.elements[0].getFanout(env);
		}

		@Override
		public void
		resetForRandomAccess(Env env)
		{
			if (this.elements.length > 0)
				this.elements[0].resetForRandomAccess(env);
		}

		@Override
		public void
		moveToIndex(Env env, int index)
		{
			final Join j0 = this.elements[0];
			// System.err.println("C{} move to index " + index);
			j0.moveToIndex(env, index);
			offset = RESET;
		}

		@Override
		public int
		getAtIndex(Env env, int index)
		{
			boolean do_reset = false;
			if (offset == RESET)
				offset = 0;

			while (offset >= 0 && offset < this.elements.length) {
				// System.err.println("{["+offset+":"+index+"]}");
				if (offset == 0) {
					switch (this.elements[0].getAtIndex(env, index)) {
					case GETAT_NONE:
						// System.err.println("None at " + index);
						return GETAT_NONE;
					case GETAT_ONE:
						first_index_frequency = GETAT_NONE;
						last_index_frequency = GETAT_ONE;
						break;
					case GETAT_ANY:
						first_index_frequency = GETAT_ANY;
						last_index_frequency = GETAT_ANY;
						break;
					}
					++offset;
					do_reset = true;
				} else {
					final Join it = this.elements[offset];
					if (do_reset) {
						it.reset(env);
						do_reset = false;
					}
					if (it.next(env)) {
						// System.err.println("> " + it + " succeeded");
						do_reset = true;
						++offset;
					}
					else {
						// System.err.println("> " + it + " failed");
						--offset;
					}
					if (offset == 0 && first_index_frequency == GETAT_NONE) // index[0] reported GETAT_ONE and we're done with that already
						return GETAT_NONE;
				}
			}
			if (offset == this.elements.length)
				--offset;
			// System.err.println("--- return ---");
			return offset >= 1 ? GETAT_ANY : last_index_frequency;
		}
		private int first_index_frequency;
		private int last_index_frequency;
		private static final int RESET = -1;

                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }

		public void
		reset(Env env)
		{
			offset = RESET;
		}

		public boolean
		next(Env env)
		{
                        if (RuntimeCreator.useRuntimeCreator) {
                            RuntimeCreator.init( this, env );
                            return RuntimeCreator.test(env);
                        }

			boolean do_reset = false;
			if (offset == RESET) {
				do_reset = true;
				offset = 0;
				if (this.elements.length == 0)
					return true;
			}
			while (offset >= 0 && offset < this.elements.length) {
				final Join it = this.elements[offset];
				if (do_reset) {
					it.reset(env);
					do_reset = false;
				}
				if (it.next(env)) {
					do_reset = true;
					++offset;
				}
				else {
					--offset;
				}
			}
			if (offset == this.elements.length)
				--offset;
			return offset >= 0;
		}

		@Override
		public double
		getSize(VarInfo var_info, Env env)
		{
			double size = 1.0;
			for (int i = 0; i < this.getComponentsNr(); i++)
				size *= this.getComponentInternal(i).getSize(var_info, env);

			return size;
		}

		@Override
		public double
		getSelectivity(VarInfo var_info, Env env)
		{
			double selectivity = 1.0;
			for (int i = 0; i < this.getComponentsNr(); i++)
				selectivity *= this.getComponentInternal(i).getSelectivity(var_info, env);

			return selectivity;
		}

		@Override
		public double
		getAccessCost(VarInfo var_info, Env env)
		{
			double access_cost = 0.0;
			for (int i = 0; i < this.getComponentsNr(); i++)
				access_cost += this.getComponentInternal(i).getAccessCost(var_info, env);

			return access_cost;
		}


		@Override
		public String
		toString()
		{
			StringBuffer buf = new StringBuffer("{");
			for (int i = 0; i < this.getComponentsNr(); i++) {
				buf.append(" ");
				buf.append(this.getComponent(i));
				buf.append("; ");
			}
			buf.append("}");
			return buf.toString();
		}

		@Override
		AbstractBlock
		make(java.util.Collection<Join> elements)
		{
			return new Conjunctive(elements);
		}

		@Override
		boolean
		isCompatibleBlock(Join j)
		{
			return j instanceof Conjunctive;
		}

	}

	public static final class Conjunctive extends PreConjunctive
	{
		public
		Conjunctive(java.util.Collection<Join> it)
		{
			super(it);
		}

		public
		Conjunctive(Join ... it)
		{
			super (it);
		}

		@Override
		public void
		accept(JoinVisitor visitor)
		{
			visitor.visitConjunctiveBlock(this);
		}
	}

	public static final class Disjunctive extends PreDisjunctive
	{
		public
		Disjunctive(java.util.Collection<Join> it)
		{
			super(it);
		}

		public
		Disjunctive(Join ... it)
		{
			super (it);
		}

		@Override
		public void
		accept(JoinVisitor visitor)
		{
			visitor.visitDisjunctiveBlock(this);
		}

	}


}
