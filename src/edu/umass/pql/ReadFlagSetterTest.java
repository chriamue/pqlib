/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import edu.umass.pql.il.*;
import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

@SuppressWarnings("unused")
public class ReadFlagSetterTest extends TestBase
{
	static java.lang.reflect.Method sumInt;
	{
		try {
			sumInt = Query.class.getDeclaredMethod("sumInt", int.class, int.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	@Test
	public void
	testSimple()
	{
		test_join(new int[] { i0w, i1w },
				      null,
				      PQLFactory.LT_Int(i0w, i1w));
	}

	@Test
	public void
	testSimple2()
	{
		test_join(new int[] { i0w, i1w, i2w },
				      null,
				      PQLFactory.ADD_Int(i0w, i1w, i2w));
	}

	@Test
	public void
	testContains()
	{
		test_join(new int[] { o0w, i1w },
				      null,
				      PQLFactory.CONTAINS(o0w, i1w));
	}

	@Test
	public void
	testLookup()
	{
		test_join(new int[] { o0w, i1w, i2w },
				      null,
				      PQLFactory.LOOKUP(o0w, i1w, i2w));
	}

	@Test
	public void
	testLookupFail()
	{
		Join l = PQLFactory.LOOKUP(o0w, i1r, i2r);
		test_join(new int[] { i1w, i2w },
				      new Join[] {l},
				      l);
	}

	@Test
	public void
	testBlock()
	{
		test_join(new int[] { o0w, i1w },
				      null,
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, o1w),
						      PQLFactory.LOOKUP(o1w, i1w, i2w)
					      }));
	}

	@Test
	public void
	testNestedBlocks()
	{
		Join j = PQLFactory.Bool(PQLFactory.Not(PQLFactory.ConjunctiveBlock(
										    PQLFactory.ARRAY_LOOKUP_Int(o0r, i1r, i2r),
										    PQLFactory.Not(PQLFactory.ConjunctiveBlock(PQLFactory.ARRAY_LOOKUP_Int(o0r, i1r, i2r),
															       PQLFactory.LT_Int(i2r, i0r)
															       ))
										    )),
					 i4r);

		test_join(new int[] { o0w, i0w },
			  null,
			  j);
	}

	@Test
	public void
	testBlockFail()
	{
		Join errit = PQLFactory.NEQ_Int(i2r, i0r);
		test_join(new int[] { o0w, i1w },
				      new Join[] {errit},
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, o1w),
						      PQLFactory.LOOKUP(o1w, i1w, i2w),
						      errit
					      }));
	}

	@Test public void testExists() { paratest("para_testExists"); }

	public void
	para_testExists()
	{
		setInts(0, 0, 0, 1);
		Reduction red = PQLFactory.Exists(PQLFactory.CONTAINS(o0r, i1w),
					       PQLFactory.LT_Int(i0r, i1r),
					       i4r, i3r);

		test_join(new int[] { o0w, i0w },
				      new Join[] {},
				      red);
	}

	@Test public void testExistsFail() { paratest("para_testExistsFail"); }

	public void
	para_testExistsFail()
	{
		Join errit = PQLFactory.LT_Int(i0r, i1r);
		Reduction red = PQLFactory.Exists(PQLFactory.CONTAINS(o0r, i1w),
					       errit,
					       i4r, i3r);

		test_join(new int[] { o0w },
				      new Join[] { errit },
				      red);
	}

	@Test public void testReductionResult() { paratest("para_testReductionResult"); }

	public void
	para_testReductionResult()
	{
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i1w, i2r),
						  PQLFactory.CONTAINS(o0r, i1w),
						  PQLFactory.LT_Int(i0r, i1r)
						  );
		Join client = PQLFactory.LT_Long(i0r, i2w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				client
			});

		test_join(new int[] { o0w, i0w },
				      new Join[] {},
				      block);
	}


	@Test public void testReductionFailInner() { paratest("para_testReductionFailInner"); }

	public void
	para_testReductionFailInner()
	{
		Reduction red = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i1w, i2r),
						  PQLFactory.CONTAINS(o0r, l1w),
						  PQLFactory.LT_Int(i0r, l1r)
						  );
		Join client = PQLFactory.LT_Long(i0r, i2w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				client
			});

		test_join(new int[] { o0w, i0w },
				      new Join[] { red },
				      block);
	}


	@Test public void testFusedReductionResult() { paratest("para_testFusedReductionResult"); }

	public void
	para_testFusedReductionResult()
	{
		Join generator = PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w);
		ControlStructure computer = PQLFactory.ConjunctiveBlock(new Join[] {
				PQLFactory.MUL_Int(l2r, i0r, i2w),
				PQLFactory.ADD_Int(i2r, i0r, i3w),
				PQLFactory.ADD_Int(i3r, i1r, i4w),
			});

		Reductor r1 = PQLFactory.Reductors.MAP(l2r, i4r, o0w);
		Reductor r2 = PQLFactory.Reductors.SET(i3r, o1w);

		Reduction red = PQLFactory.Reduction(r1,
						  generator,
						  computer);
		red.addReductor(r2);

		Join client1 = PQLFactory.EQ_Object(o0r, o2w);
		Join client2 = PQLFactory.EQ_Object(o1r, o3w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				PQLFactory.ConjunctiveBlock(new Join[] { client1,
								  client2 })
			});

		test_join(new int[] { i0w, i1w, l0r, l1r, l2r },
				      new Join[] {},
				      block);
	}


	@Test public void testFusedReductionFailRhs() { paratest("para_testFusedReductionFailRhs"); }

	public void
	para_testFusedReductionFailRhs()
	{
		Join generator = PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w);
		ControlStructure computer = PQLFactory.ConjunctiveBlock(new Join[] {
				PQLFactory.MUL_Int(l2r, i0r, i2w),
				PQLFactory.ADD_Int(i2r, i0r, i3w),
				PQLFactory.ADD_Int(i3r, i1r, i4w),
			});

		Reductor r1 = PQLFactory.Reductors.MAP(l2r, i4r, o0w);
		Reductor r2 = PQLFactory.Reductors.SET(d0r, o1w);

		Reduction red = PQLFactory.Reduction(r1,
						  generator,
						  computer);
		red.addReductor(r2);

		Join client1 = PQLFactory.EQ_Object(o0r, o2w);
		Join client2 = PQLFactory.EQ_Object(o1r, o3w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				PQLFactory.ConjunctiveBlock(new Join[] { client1,
								  client2 })
			});

		test_join(new int[] { i0w, i1w, l0r, l1r, l2r },
				      new Join[] { red },
				      block);
	}


	@Test public void testFusedReductionFailLhs() { paratest("para_testFusedReductionFailLhs"); }

	public void
	para_testFusedReductionFailLhs()
	{
		Join generator = PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w);
		ControlStructure computer = PQLFactory.ConjunctiveBlock(new Join[] {
				PQLFactory.MUL_Int(l2r, i0r, i2w),
				PQLFactory.ADD_Int(i2r, i0r, i3w),
				PQLFactory.ADD_Int(i3r, i1r, i4w),
			});

		Reductor r1 = PQLFactory.Reductors.MAP(l2r, d0r, o0w);
		Reductor r2 = PQLFactory.Reductors.SET(i3r, o1w);

		Reduction red = PQLFactory.Reduction(r1,
						  generator,
						  computer);
		red.addReductor(r2);

		Join client1 = PQLFactory.EQ_Object(o0r, o2w);
		Join client2 = PQLFactory.EQ_Object(o1r, o3w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				PQLFactory.ConjunctiveBlock(new Join[] { client1,
								  client2 })
			});

		test_join(new int[] { i0w, i1w, l0r, l1r, l2r },
				      new Join[] { red },
				      block);
	}



	@Test public void testDefaultMapBadDefaultWrite() { paratest("para_testDefaultMapBadDefaultWrite"); }

	public void
	para_testDefaultMapBadDefaultWrite()
	{
		Join generator = PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w);
		ControlStructure computer = PQLFactory.ConjunctiveBlock(new Join[] {
				PQLFactory.MUL_Int(l2r, i0r, i2w),
				PQLFactory.ADD_Int(i2r, i0r, i3w),
				PQLFactory.ADD_Int(i3r, i1r, i4w),
			});

		Reductor r = PQLFactory.Reductors.DEFAULT_MAP(o1w, i0r, d0r, o1w); // o1 should be READ from!

		Reduction red = PQLFactory.Reduction(r,
						  generator,
						  computer);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red
			});

		test_join(new int[] { i0w, i1w, l0r, l1r, l2r },
				      new Join[] { red },
				      block);
	}


	@Test public void testDefaultMapBadDefaultInnerRead() { paratest("para_testDefaultMapBadDefaultInnerRead"); }

	public void
	para_testDefaultMapBadDefaultInnerRead()
	{
		Join generator = PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w);
		ControlStructure computer = PQLFactory.ConjunctiveBlock(new Join[] {
				PQLFactory.MUL_Int(l2r, i0r, i2w),
				PQLFactory.ADD_Int(i2r, i0r, i3w),
				PQLFactory.ADD_Int(i3r, i1r, i4w),
			});

		Reductor r = PQLFactory.Reductors.DEFAULT_MAP(i2r, i0r, i4r, o1w); // i2 would have to be initialised before the reduction!

		Reduction red = PQLFactory.Reduction(r,
						  generator,
						  computer);

		Join client2 = PQLFactory.EQ_Object(o1r, o3w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				client2
			});

		test_join(new int[] { i0w, i1w, l0r, l1r, l2r },
				      new Join[] { red },
				      block);

	}


	@Test public void testDefaultMapGood() { paratest("para_testDefaultMapGood"); }

	public void
	para_testDefaultMapGood()
	{
		Join generator = PQLFactory.INT_RANGE_CONTAINS(l0r, l1r, l2w);
		ControlStructure computer = PQLFactory.ConjunctiveBlock(new Join[] {
				PQLFactory.MUL_Int(l2r, i0r, i2w),
				PQLFactory.ADD_Int(i2r, i0r, i3w),
				PQLFactory.ADD_Int(i3r, i1r, i4w),
			});

		Reductor r = PQLFactory.Reductors.DEFAULT_MAP(i2r, i0r, i4r, o1w);

		Reduction red = PQLFactory.Reduction(r,
						  generator,
						  computer);

		Join client2 = PQLFactory.EQ_Object(o1r, o3w);

		ControlStructure block = PQLFactory.ConjunctiveBlock(new Join[] {
				red,
				client2
			});

		test_join(new int[] { i0w, i1w, l0r, l1r, l2r, i2r },
			  null,
			  block);
	}




	public static class A { private int x; }

	@Test
	public void
	testFieldAccess()
	{
		test_join(new int[] { o0w },
				      null,
				      PQLFactory.FIELD(A.class, "x", o0w, o1w));
	}

	@Test
	public void
	testBlockNotSuccess()
	{
		test_join(new int[] { o0w, o1w, i1w },
				      null,
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, i2w),
						      PQLFactory.Not(PQLFactory.LOOKUP(o1w, i1w, l2w)),
						      PQLFactory.NEQ_Int(i2r, i1r)
					      }));
	}

	@Test
	public void
	testBlockNotFailure()
	{
		Join errit = PQLFactory.NEQ_Int(l2r, i1r);
		test_join(new int[] { o0w, i1w, o1w },
				      new Join[] {errit},
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, i2w),
						      PQLFactory.Not(PQLFactory.LOOKUP(o1w, i1w, i2w)),
						      errit
					      }));
	}

	@Test
	public void
	testBlockNotNot()
	{
		test_join(new int[] { o0w, i1w, i1w, o1w },
				      null,
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, i2w),
						      PQLFactory.Not(PQLFactory.Not(PQLFactory.LOOKUP(o1w, i1w, l2w))),
						      PQLFactory.NEQ_Int(i2r, i1r)
					      }));
	}

	@Test
	public void
	testBlockNotNotFailure()
	{
		Join errit = PQLFactory.LOOKUP(o2w, i1w, l2w);
		test_join(new int[] { o0w, i1w, o1w },
				      new Join[] {errit},
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, i2w),
						      PQLFactory.Not(PQLFactory.Not(errit)),
						      PQLFactory.NEQ_Int(i2r, i1r)
					      }));
	}


	@Test
	public void
	testBlockBoolSuccess()
	{
		test_join(new int[] { o0w, o1w, i1w },
				      null,
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, i2w),
						      PQLFactory.Bool(PQLFactory.LOOKUP(o1w, i1w, l2w), i3w),
						      PQLFactory.NEQ_Int(i3r, i1r)
					      }));
	}

	@Test
	public void
	testBlockBoolFailure()
	{
		Join errit = PQLFactory.NEQ_Int(l2r, i1r);
		test_join(new int[] { o0w, i1w, o1w },
				      new Join[] {errit},
				      PQLFactory.ConjunctiveBlock(new Join[] {
						      PQLFactory.LOOKUP(o0w, i1w, i2w),
						      PQLFactory.Bool(PQLFactory.LOOKUP(o1w, i1w, i2w), i3w),
						      errit
					      }));
	}

	@Test
	public void
	testDBlockSuccess()
	{
		test_join(new int[] { o0w, o1w, i1w },
			  null,
			  PQLFactory.ConjunctiveBlock(
						      PQLFactory.DisjunctiveBlock(
										  PQLFactory.EQ_Int(i1w, i2w)
										  ,
										  PQLFactory.LOOKUP(o0w, i1w, i2w)
										  ),
						      PQLFactory.EQ_Int(i2r, i2r)
						      ));
	}

	@Test
	public void
	testDBlockFailR()
	{
		final Join b = PQLFactory.EQ_Int(i2w, i2w);
		test_join(new int[] { o0w, o1w, i1w },
			  new Join[] { b },
			  PQLFactory.ConjunctiveBlock(
						      PQLFactory.DisjunctiveBlock(
										  PQLFactory.LOOKUP(o0w, i1w, i2w),
										  PQLFactory.EQ_Int(i1w, i3w)
										  ),
						      b
						      ));
	}

	@Test
	public void
	testDBlockFailL()
	{
		final Join b = PQLFactory.EQ_Int(i2w, i2w);
		test_join(new int[] { o0w, o1w, i1w },
			  new Join[] { b },
			  PQLFactory.ConjunctiveBlock(
						      PQLFactory.DisjunctiveBlock(
										  PQLFactory.LOOKUP(o0w, i1w, i3w),
										  PQLFactory.EQ_Int(i1w, i2w)
										  ),
						      b
						      ));
	}


	@Test public void testInnerReductionSuccess() { paratest("para_testInnerReductionSuccess"); }

	public void
	para_testInnerReductionSuccess()
	{
		Reductor sum = PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i1w, i0r);
		Reductor map = PQLFactory.Reductors.MAP(i2w, Reductor.INPUT_FROM_INNER_REDUCTOR, o1r);
		map.setInnerReductor(sum);
		Join reduction = PQLFactory.Reduction(map,
						   PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i0w),
						   PQLFactory.DIV_Int(i0r, i4r, i2w)
						   );

		test_join(new int[] { o0r, i4r, i1r },
			  null, // no expected errors
			  reduction);

		assertEquals(sum.toString(), i1r, sum.getArg(0));

		assertEquals(map.toString(), o1w, map.getArg(2));
		assertEquals(map.toString(), Reductor.INPUT_FROM_INNER_REDUCTOR, map.getArg(1));
		assertEquals(map.toString(), i2r, map.getArg(0));
	}

	@Test public void testInnerReductionFailure0() { paratest("para_testInnerReductionFailure0"); }

	public void
	para_testInnerReductionFailure0()
	{
		Reductor sum = PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i1r, i0w);
		Reductor map = PQLFactory.Reductors.MAP(i2r, Reductor.INPUT_FROM_INNER_REDUCTOR, o1w);
		map.setInnerReductor(sum);
		Join reduction = PQLFactory.Reduction(map,
						   PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i0w),
						   PQLFactory.DIV_Int(i0r, i4r, i2w)
						   );

		test_join(new int[] { o0r, i4r },
			  new Join[] { reduction },
			  reduction);
	}

	@Test public void testInnerReductionFailure1() { paratest("para_testInnerReductionFailure1"); }

	public void
	para_testInnerReductionFailure1()
	{
		Reductor sum = PQLFactory.Reductors.METHOD_ADAPTER(sumInt, i1r, i0w);
		Reductor map = PQLFactory.Reductors.MAP(i2r, Reductor.INPUT_FROM_INNER_REDUCTOR, o1w);
		Join reduction = PQLFactory.Reduction(map,
						   PQLFactory.ARRAY_LOOKUP_Int(o0r, __, i0w),
						   PQLFactory.DIV_Int(i0r, i4r, i2w)
						   );

		test_join(new int[] { o0r, i4r, i1r },
			  new Join[] { reduction },
			  reduction);
	}



	/**
	 * Tests whether the join produces exactly the expected errors
	 */
	public static void
	test_join(int[] initially_bound_vars,
			      Join[] expected_errors,
			      Join b)
	{
		VarSet s = new VarSet(initially_bound_vars);
		final ReadFlagSetterVisitor v = new ReadFlagSetterVisitor(s.copy());
		b.accept(v);

		final Set<Join> errors = v.getErrors();

		boolean fail;

		if (expected_errors == null)
			fail = test_bindings(s, new VarSet(), new VarSet(), b);
		else {
			fail = false;

			for (Join e : expected_errors) {
				if (errors != null && errors.contains(e))
					errors.remove(e);
				else {
					System.err.println("- Missed error at: " + e);
					fail = true;
				}
			}
		}

		if (errors != null) {
			for (Join e : errors) {
				System.err.println("- Unexpected error at: " + e);
				fail = true;
			}
		}
		if (fail)
			System.err.println();
		assertFalse(fail);
	}

	interface ReadAdapter
	{
		int getArg(int i);
	}

	public static ReadAdapter fromJoin(final Join j)
	{
		return new ReadAdapter() {
			public int getArg(int i) { return j.getArg(i); }
			public String toString() { return j.toString(); }
		};
	}

	public static ReadAdapter fromReductor(final Reductor j)
	{
		return new ReadAdapter() {
			public int getArg(int i) { return j.getArg(i); }
			public String toString() { return j.toString(); }
		};
	}

	public static boolean
	test_bindings_read_vars(VarSet bound_vars, // variables with guaranteed binding
				VarSet dirty_vars, // variables that definitely do not have a guaranteed binding and must not be read from
				VarSet touched_vars, // variables that have been written to (a subset of bound_vars)
				Join bi,
				int start,
				int stop)
	{
		return test_bindings_read_vars(bound_vars, dirty_vars, touched_vars,
					       fromJoin(bi),
					       start, stop);
	}

	public static boolean
	test_bindings_read_vars(VarSet bound_vars, // variables with guaranteed binding
				VarSet dirty_vars, // variables that definitely do not have a guaranteed binding and must not be read from
				VarSet touched_vars, // variables that have been written to (a subset of bound_vars)
				Reductor bi,
				int start,
				int stop)
	{
		return test_bindings_read_vars(bound_vars, dirty_vars, touched_vars,
					       fromReductor(bi),
					       start, stop);
	}


	public static boolean
	test_bindings_read_vars(VarSet bound_vars, // variables with guaranteed binding
				VarSet dirty_vars, // variables that definitely do not have a guaranteed binding and must not be read from
				VarSet touched_vars, // variables that have been written to (a subset of bound_vars)
				ReadAdapter bi,
				int start,
				int stop)
	{
		boolean fail = false;

		for (int i = start; i < stop; i++) {
			final int var = bi.getArg(i);
			if (var == Reductor.INPUT_FROM_INNER_REDUCTOR)
				continue;
			if (Env.isReadVar(var)) {
				if (!bound_vars.contains(var)) {
					fail = true;
					System.err.println("- Read access to undefined variable `" + Env.showVar(var) + "' at offset #" + i + " in " + bi);
				}
				if (dirty_vars.contains(var)) {
					fail = true;
					System.err.println("- Read access to dirty variable `" + Env.showVar(var) + "' at offset #" + i + " in " + bi);
				}
			} else { // write-var
				fail = true;
				System.err.println("- Expected read-var but found `" + Env.showVar(var) + "' at offset #" + i + " in " + bi);
			}
		}
		return fail;
	}


	public static boolean
	test_bindings_write_vars(VarSet bound_vars, // variables with guaranteed binding
				VarSet dirty_vars, // variables that definitely do not have a guaranteed binding and must not be read from
				VarSet touched_vars, // variables that have been written to (a subset of bound_vars)
				Join bi,
				int start,
				int stop)
	{
		boolean fail = false;

		for (int i = start; i < stop; i++) {
			final int var = bi.getArg(i);		
			if (Env.isWriteVar(var)) {
				if (bound_vars.contains(var)) {
					// Multiple writes to the same variable are permissible in principle, but in our
					// tests we don't allow this on the same path.  Instead, we try to find errors
					// wherein the ReadFlagSetterVisitor inadvertently assigned multiple writes.
					fail = true;
					System.err.println("- Not the first write to variable `" + Env.showVar(var) + "' at offset #" + i + " in " + bi);
				}
				bound_vars.insert(var);
				touched_vars.insert(var);
				dirty_vars.remove(var);
			} else
				if (dirty_vars.contains(var)) {
					fail = true;
					System.err.println("- Read access to dirty variable `" + Env.showVar(var) + "' at offset #" + i + " in " + bi);
				}
		}
		return fail;
	}

	public static boolean
	test_bindings(VarSet bound_vars, // variables with guaranteed binding
		      VarSet dirty_vars, // variables that definitely do not have a guaranteed binding and must not be read from
		      VarSet touched_vars, // variables that have been written to (a subset of bound_vars)
		      Join bi)
	{
		boolean fail = false;

		// System.err.println(bi + ":");
		// System.err.println("  bound: " + bound_vars);
		// System.err.println("  dirty: " + dirty_vars);
		// System.err.println("  touch: " + touched_vars);

		if (bi instanceof Reduction) {
			Reduction red = (Reduction) bi;
			fail |= test_bindings_read_vars(bound_vars, dirty_vars, touched_vars, red,
							0, red.getOuterReadArgsNr());
			for (Reductor r : red.getReductors()) {
				while (null != (r = r.getInnerReductor()))
					fail |= test_bindings_read_vars(bound_vars, dirty_vars, touched_vars, r,
									0, r.getOuterReadArgsNr());
			}
		} else {
			fail |= test_bindings_read_vars(bound_vars, dirty_vars, touched_vars, bi,
							0,
							bi.getReadArgsNr());

			fail |= test_bindings_write_vars(bound_vars, dirty_vars, touched_vars, bi,
							 bi.getReadArgsNr(), bi.getArgsNr());
		}

		if (bi instanceof Predicate) {
			Predicate p = (Predicate) bi;
			// no extra work needed
		} else if (bi instanceof Reduction) {
			Reduction r = (Reduction) bi;
			VarSet sub_bound_vars = bound_vars.copy();
			fail |= test_bindings(sub_bound_vars, dirty_vars, touched_vars, r.getComponent(0));

			fail |= test_bindings_read_vars(sub_bound_vars, dirty_vars, touched_vars, bi,
							r.getOuterReadArgsNr(), r.getReadArgsNr());
			for (Reductor rr : r.getReductors()) {
				while (null != (rr = rr.getInnerReductor()))
					fail |= test_bindings_read_vars(bound_vars, dirty_vars, touched_vars, rr,
									rr.getOuterReadArgsNr(), rr.getReadArgsNr());
			}


			dirty_vars.addAll(touched_vars);
			touched_vars.clear();
			fail |= test_bindings_write_vars(bound_vars, dirty_vars, touched_vars, bi,
							 bi.getReadArgsNr(), bi.getArgsNr());

			// FIXME: check the reductor's binding needs and mark its output
		} else if (bi instanceof AbstractBlock.Conjunctive) {
			AbstractBlock b = (AbstractBlock) bi;
			for (int i = 0; i < b.getComponentsNr(); i++) {
				fail |= test_bindings(bound_vars, dirty_vars, touched_vars, b.getComponent(i));
			}
		} else if (bi instanceof AbstractBlock.Disjunctive) {
			AbstractBlock b = (AbstractBlock) bi;
			
			final VarSet current_bound_vars = bound_vars;
			final VarSet current_touched_vars = new VarSet();

			VarSet finally_bound_vars = null;

			for (int i = 0; i < b.getComponentsNr(); i++) {
				final VarSet newly_bound_vars = current_bound_vars.copy();
				final VarSet newly_touched_vars = new VarSet();

				fail |= test_bindings(newly_bound_vars, dirty_vars, newly_touched_vars, b.getComponent(i));

				if (finally_bound_vars == null)
					finally_bound_vars = newly_bound_vars;
				else
					finally_bound_vars.retainAll(newly_bound_vars);
				current_touched_vars.addAll(newly_touched_vars);
			}

			bound_vars.addAll(finally_bound_vars);
			touched_vars.addAll(current_touched_vars);

		} else if (bi instanceof Container.CONTAINS) {
			// no extra work needed
		} else if (bi instanceof Container.GeneralLookup) {
			// no extra work needed
		} else if (bi instanceof Not) {
			final Not not = (Not) bi;
			final VarSet sub_bound_vars = bound_vars.copy();
			final VarSet sub_touched_vars = new VarSet();
			fail |= test_bindings(sub_bound_vars, dirty_vars, sub_touched_vars, not.getComponent(0));
			dirty_vars.addAll(sub_touched_vars);
			touched_vars.addAll(sub_touched_vars);
		} else if (bi instanceof NoFail) {
			final NoFail nofail = (NoFail) bi;
			final VarSet sub_bound_vars = bound_vars.copy();
			final VarSet sub_touched_vars = new VarSet();
			fail |= test_bindings(sub_bound_vars, dirty_vars, sub_touched_vars, nofail.getComponent(0));
			dirty_vars.addAll(sub_touched_vars);
			touched_vars.addAll(sub_touched_vars);
		} else if (bi instanceof Container.INT_RANGE_CONTAINS) {
			// nothing else needed
		} else
			throw new RuntimeException("Unknown operator type " + bi.getClass());

		// System.err.println(" +bound: " + bound_vars);
		// System.err.println(" +dirty: " + dirty_vars);
		// System.err.println(" +touch: " + touched_vars);

		return fail;
	}
}