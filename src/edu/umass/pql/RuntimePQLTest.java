/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.pql;

import edu.umass.pql.container.AMap;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.il.meta.SelectPath;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import org.junit.Test;

/**
 *
 * @author Hilmar
 */
public class RuntimePQLTest extends TestBase {
    
    public static void main (String [] args) {
        try {    
            RuntimePQLTest testInstance = new RuntimePQLTest();
            testInstance.setUp();
            testInstance.setNestedTest();

            for (int i=0; i<ArithmeticTest.class.getMethods().length; i++) {
                Annotation[] ann = ArithmeticTest.class.getMethods()[i].getAnnotations();
                if (ann.length > 0 && ann[0].toString().startsWith("@org.junit.Test") && !ArithmeticTest.class.getMethods()[i].getName().contains("Wildcard")) {
                    if (i==423432) {
                        System.out.println("Test method: " + ArithmeticTest.class.getMethods()[i].getName());                        
                        ArithmeticTest.class.getMethods()[i].invoke(testInstance, new Object [] {});
                    }
                }
            }
        } catch (InvocationTargetException x) {
                Throwable cause = x.getCause();
                System.err.format("invocation of %s failed: %s%n",
                           "method", cause.getMessage());
                x.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
        public void setNestedTest()
        {        
            Random random = new Random();
            random.setSeed(12345);

            for (int i=0; i<2; i++) {
                 PSet <Integer> newSet = new PSet <Integer> ();

                //zuerst ein grosses, dann ein kleines Set
                for (int j=0; j< (i==0 ? 10000000 : 1000); j++ )
                    newSet.add(random.nextInt(100000));

                env.v_object[1-i] = newSet;
            }

            env.v_int[0] = 10;

            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o2w)},
                PQLFactory.ConjunctiveBlock( PQLFactory.CONTAINS( (o0r | VAR_CONST_FLAG), i1w),
                PQLFactory.LT_Int(i1r, (i0r | VAR_CONST_FLAG) ),
                PQLFactory.CONTAINS( (o1r | VAR_CONST_FLAG), i1r))
                );
        
            VarSet constantVars = new VarSet();
            constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 0));
            constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 1));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 0));
            SelectPath selectPath = SelectPath.create(new Join [] {p});        
            selectPath.setVarInfo(new VarInfo());
            selectPath.schedule(new VarInfo(constantVars), env, false);
            
            for (int i=0; i<11; i++) {
                final long init_time = System.nanoTime();
                checkTrue(p);
                final long stop_time = System.nanoTime();
                System.out.println("Dauer: " + (stop_time-init_time));
            }
            
            System.out.println("==============");
                        
            for (int i=0; i<11; i++) {
                final long init_time = System.nanoTime();
                checkTrue(selectPath);
                final long stop_time = System.nanoTime();
                System.out.println("Dauer: " + (stop_time-init_time));
            }
        }
        
        @Test
        public void
        testIntRange()
        {
            setInts(0, 3, 0, 1, 6);
            setLongs(0, 3, 0, 1, 6);
            Join p = PQLFactory.ConjunctiveBlock(
                    PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                    PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                    PQLFactory.ADD_Int(l3r, l2r, l4r)
                    );
            checkTrue(p);
            System.out.println("RESULT: " + env.v_int[0] + ", " + env.v_int[1] + ", " + env.v_int[2] + ", " + env.v_int[3]);
            
        }
        
        @Test
        public void
        testExists()
        {
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 3, 0, 100, 6);
            setLongs(0, 3, 0, 1, 5);
            Join p = PQLFactory.Exists(PQLFactory.ConjunctiveBlock(
                    PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                    PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                    PQLFactory.ADD_Int(l3r, l2r, l4r)                  
                    ), PQLFactory.ADD_Int(l3r, l2r, i3w), i3r, i4w);
            checkTrue(p);
            System.out.println("RESULT: " + env.v_int[3] + ", " + env.v_int[4]);
            
        }
        
        public static Object methodTestObject (Object l, Object r) {
            System.out.println("METHODTEST.." + l + " + " + r);
            //return (Object)(((Integer)l) + ((Integer)r));
            return new Integer(5);
        }
        
        @DefaultValueLong(0l)
        public static long methodTestLong (long l, long r) {
            System.out.println("METHODTESTLONG..");
            return l+r;
        }
        
        @DefaultValueChar('\00')
        public static char methodTestChar (char l, char r) {
            System.out.println("MethodTest char.. " + l + ", " + r);
            return 'c';
        }
        
        @DefaultValueInt(0)
        public static int methodTestInt (int l, int r) {
            System.out.println("MethodTest int.. " + l + ", " + r);
            return 12;
        }
        
        @Test
        public void
        testMethodAdapter()
        {
            
            Method method = null;
            for (int i=0; i<ArithmeticTest.class.getMethods().length; i++) {
                if (ArithmeticTest.class.getMethods()[i].getName().contains("methodTestChar"))
                    method = ArithmeticTest.class.getMethods()[i];
            }
            
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 3, 65, 66, 6);
            setLongs(0, 3, 0, 1, 5);
            setObjects( new Character('a'), new Integer(4), new Character('b')  );
            Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, i2w, i3w) },
                    PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                    PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                    PQLFactory.ADD_Int(l3r, l2r, l4r)
                    );
            checkTrue(p);
            System.out.println("RESULT: " + env.v_int[2] + ", " + env.v_int[3]);
            
            /*Method method = null;
            for (int i=0; i<ArithmeticTest.class.getMethods().length; i++) {
                if (ArithmeticTest.class.getMethods()[i].getName().contains("methodTestObject"))
                    method = ArithmeticTest.class.getMethods()[i];
            }
            
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 3, 65, 66, 6);
            setLongs(0, 3, 0, 1, 5);
            setObjects( new Character('a'), new Integer(4), new Character('b')  );
            Join p = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, l2w, o2w) },
                    PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                    PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                    PQLFactory.ADD_Int(l3r, l2r, l4r)
                    );
            checkTrue(p);
            System.out.println("RESULT: " + env.v_long[2] + ", " + env.v_object[2]);*/
            
        }
        
        public void testReduction() { paratest("para_testReduction"); }

        @Test
	public void
	para_testReduction()
	{
            
            Method method = null;
            for (int i=0; i<ArithmeticTest.class.getMethods().length; i++) {
                if (ArithmeticTest.class.getMethods()[i].getName().contains("methodTestInt"))
                    method = ArithmeticTest.class.getMethods()[i];
            }
            
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 3, 0, 1, 6);
            env.setInt(i5r, 100);
            setLongs(0, 0, 0, 1, 5);
            /*Join j = PQLFactory.Reduction(PQLFactory.Reductors.SET(i0r, o1w),
					      PQLFactory.CONTAINS(o0r, o2w),
					      PQLFactory.POLY_SIZE(o2r, i0w));*/
            /*Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(l3r, o0w), PQLFactory.Reductors.SET(l3r, o1w), PQLFactory.Reductors.N_MAP(l2r, PQLFactory.Reductors.MAP(i3r, l3r, o4w), o2w), PQLFactory.Reductors.DEFAULT_MAP(l4r, l3r, l3r, o3w)},
                                            PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                                            PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                                            PQLFactory.ADD_Int(l3r, l4r, i3w) );*/
            Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(l3r, o4w), PQLFactory.Reductors.SET(l3r, o1w), PQLFactory.Reductors.N_MAP(l2r, PQLFactory.Reductors.METHOD_ADAPTER(method, i3r, l2r), o2w), PQLFactory.Reductors.DEFAULT_MAP(l4r, l3r, l3r, o3w)},
                                            PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                                            PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                                            PQLFactory.ADD_Long(l4r, l3r, i3w) );
            /*Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(l3r, o0w), PQLFactory.Reductors.SET(l3r, o1w), PQLFactory.Reductors.N_MAP(l2r, PQLFactory.Reductors.EXISTS(i4w, i5w), o2w), PQLFactory.Reductors.DEFAULT_MAP(l4r, l3r, l3r, o3w)},
                                            PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                                            PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                                            PQLFactory.ADD_Int(l3r, l4r, i3w) );*/
            
            checkTrue(j);
            System.out.println("A: " + (env.getObject(o4r) instanceof PSet) );
            System.out.println("B: " + ((PSet)env.getObject(o4r)).toString() );
            System.out.println("A: " + (env.getObject(o1r) instanceof PSet) );
            System.out.println("C: " + ((PSet)env.getObject(o1r)).toString() );
            System.out.println("A: " + (env.getObject(o2r) instanceof PMap) );
            System.out.println("C: " + ((PMap)env.getObject(o2r)).toString() );
            System.out.println("A: " + (env.getObject(o3r) instanceof PDefaultMap) );
            System.out.println("C: " + ((PDefaultMap)env.getObject(o3r)).toString() );
            System.out.println("D: " + env.v_int[4] + ", " + env.v_int[5] );
            /*System.out.println("A: " + (env.getObject(o4r) instanceof PMap) );
            System.out.println("C: " + ((PMap)env.getObject(o4r)).toString() );*/
        }
        
        @Test
	public void
	testArrays()
	{
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 3, 8, 3, 6);
            env.setInt(i5r, 100);
            setLongs(0, 8, 0, 1, 5);
            Join j = PQLFactory.ConjunctiveBlock( PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.OBJECT_ARRAY(l2r, l3r, o0w)},
                                            PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                                            PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                                            PQLFactory.ADD_Long(l4r, l3r, i4w) ) , 
                                            PQLFactory.ARRAY_LOOKUP_Object(o0r, i2r, i3r));
            
            checkFalse(j);
            System.out.println("A: " + (env.getObject(o0r) instanceof Object[]) );
            for (int i=0; i<((Object[])env.getObject(o0r)).length; i++)
                System.out.println((i%2 == 0 ?"Key: " : "Value: ") + ((Object[])env.getObject(o0r))[i] );
        }
       
        @Test
	public void
	testContains()
	{
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 3, 2, 2, 3);
            setLongs(0, 3, 0, 1, 5);
            HashSet <Integer> set = new HashSet <Integer> ();
            HashMap <Integer, Integer> map = new HashMap <Integer, Integer> ();
            set.add(new Integer(2));
            set.add(new Integer(3));
            set.add(new Integer(4));
            map.put(new Integer(2), new Integer(3));
            env.setObject(o4r, set);
            env.setObject(o5r, map);            
            /*Join j = PQLFactory.Reduction(PQLFactory.Reductors.SET(i0r, o1w),
					      PQLFactory.CONTAINS(o0r, o2w),
					      PQLFactory.POLY_SIZE(o2r, i0w));*/
            Join j = PQLFactory.ConjunctiveBlock(PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(l2r, o0w), PQLFactory.Reductors.SET(l3r, o1w), PQLFactory.Reductors.MAP(l2r, l3r, o2w), PQLFactory.Reductors.DEFAULT_MAP(l4r, l2r, l3r, o3w)},
                                            PQLFactory.INT_RANGE_CONTAINS(i0r, i1r, l3w),
                                            PQLFactory.LONG_RANGE_CONTAINS(l0r, l1r, l2w),
                                            PQLFactory.ADD_Int(l3r, l2r, l4r) ),
                                            PQLFactory.CONTAINS(o4r, i2r),
                                            PQLFactory.POLY_LOOKUP(o2r, i3r, i4r),
                                            PQLFactory.POLY_LOOKUP(o5r, i3r, i4r));
            checkTrue(j);
            System.out.println("A: " + (env.getObject(o0r) instanceof PSet) );
            System.out.println("B: " + ((PSet)env.getObject(o0r)).toString() );            
            System.out.println("A: " + (env.getObject(o1r) instanceof PSet) );
            System.out.println("C: " + ((PSet)env.getObject(o1r)).toString() );
            System.out.println("A: " + (env.getObject(o2r) instanceof PMap) );
            System.out.println("C: " + ((PMap)env.getObject(o2r)).toString() );            
            System.out.println("A: " + (env.getObject(o3r) instanceof PDefaultMap) );
            System.out.println("C: " + ((PDefaultMap)env.getObject(o3r)).toString() );
            System.out.println("X: " + (((PDefaultMap)env.getObject(o3r)).getRepresentation().length ) );
        }
        
        @Test
	public void
	testContains2()
	{
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(3, 1, 2, 3, 49);
            setLongs(0, 3, 0, 1, 5);
            PMap <Object, Object> pmap = new PMap <Object, Object> ();
            pmap.put(new Integer(4), new Integer(34));
            pmap.put(new Integer(5), new Integer(68));
            PSet <Object> pset = new PSet <Object> ();
            pset.add(new Integer(24) );
            pset.add(new Integer(342));
            pset.add(new Integer(532));
            env.setObject(o0r, pset);
            env.setObject(o2r, pmap);
            int [] arr = new int[4];
            arr[0] = 34;
            arr[1] = 235;
            arr[2] = 239;
            arr[3] = 49;
            env.setObject(o3r, arr);
            /*Join j = PQLFactory.Reduction(PQLFactory.Reductors.SET(i0r, o1w),
					      PQLFactory.CONTAINS(o0r, o2w),
					      PQLFactory.POLY_SIZE(o2r, i0w));*/
            Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i0r, o1w)},
                                            PQLFactory.ConjunctiveBlock( PQLFactory.LOOKUP(o2r, __, i0w)) );
                                            //PQLFactory.ARRAY_LOOKUP_Int(o3r, i0r, i4r),
                                            //PQLFactory.ADD_Int(i1r, i2r, i3r));
            checkTrue(j);
            System.out.println("A: " + (env.getObject(o1r) instanceof PSet) );
            System.out.println("B: " + ((PSet)env.getObject(o1r)).toString() );            
        }
        
        @Test
	public void
	testLookupReadWrite()
	{
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(0, 1, 2, 25, 4);
            HashSet <Integer> set = new HashSet <Integer> ();
            AMap <Integer, Integer> map = new PMap <Integer, Integer> ();
            Integer sf = new Integer(1);
            Integer fds = new Integer(0);
            map.put(new Integer(2), new Integer(7));
            map.put(new Integer(3), new Integer(5));
            map.put(new Integer(0), new Integer(1));
            //map.put(sf, fds);
            env.setObject(o5r, map);
            env.setObject(o1r, sf);
            env.setObject(o2r, fds);
            /*Join j = PQLFactory.Reduction(PQLFactory.Reductors.SET(i0r, o1w),
					      PQLFactory.CONTAINS(o0r, o2w),
					      PQLFactory.POLY_SIZE(o2r, i0w));*/
            Join j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i3r, o3w)}, PQLFactory.LOOKUP(o5r, __, i3w), PQLFactory.ADD_Int(i2r, i2r, i4r));
            checkTrue(j);
            System.out.println("B: " + ((PMap)env.getObject(o5r)).toString() );            
            System.out.println("B: " + ((PSet)env.getObject(o3r)).toString() );            
            System.out.println("C: " + env.v_int[3] );            
        }
        
        @Test
	public void
	testCoercion()
	{
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            setInts(574398, 574398, 574398, 574398);
            setDoubles(1.23456789, 0, 0);
            System.out.println(env.v_int[0] + " / " + env.v_int[1] + " / " + env.v_int[2] + " / " + env.v_int[3] + " / " + env.v_double[0]);
            Join j = PQLFactory.ConjunctiveBlock(PQLFactory.COERCE_Boolean(i0r, i0w),
                                            PQLFactory.COERCE_Byte(i1r, i1w),
                                            PQLFactory.COERCE_Short(i2r, i2w),
                                            PQLFactory.COERCE_Char(i3r, i3w),
                                            PQLFactory.COERCE_Float(d0r, d0w) );
            checkTrue(j);
            System.out.println(env.v_int[0] + " / " + env.v_int[1] + " / " + env.v_int[2] + " / " + env.v_int[3] + " / " + env.v_double[0]);
        }
    
}
