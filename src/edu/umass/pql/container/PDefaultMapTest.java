/***************************************************************************
 Copyright (C) 2009 Google, Inc.
               2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 3 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.container;

import java.util.*;
import junit.framework.*;
import com.google.common.collect.testing.MapTestSuiteBuilder;
import com.google.common.collect.testing.TestStringMapGenerator;
import com.google.common.collect.testing.features.MapFeature;
import com.google.common.collect.testing.features.CollectionSize;
import java.util.Map.Entry;

public final class PDefaultMapTest
{
	public static TestSuite
	suite()
	{
		return MapTestSuiteBuilder
			.using(new TestStringMapGenerator() {
					@Override protected Map<String, String>
					create(Entry<String, String>[] entries)
					{
						return toMap(entries);
					}
				})
			.named("PDefaultMap")
			.withFeatures(
				      MapFeature.GENERAL_PURPOSE,
				      MapFeature.ALLOWS_NULL_VALUES,
				      CollectionSize.ANY)
			.createTestSuite();
	}

	public static <T> Map<T, String>
	toMap(Entry<T, String>[] entries)
	{
		return populate(new PDefaultMap<T, String>(), entries);
	}

	private static <T> Map<T, String>
	populate(Map<T, String> map, Entry<T, String>[] entries) {
		for (Entry<T, String> entry : entries) {
			map.put(entry.getKey(), entry.getValue());
		}
		return map;
	}
}