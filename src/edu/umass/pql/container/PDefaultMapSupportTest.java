/***************************************************************************
 Copyright (C) 2009 Google, Inc.
               2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 3 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.container;

import static org.junit.Assert.*;
import java.util.HashMap;
import org.junit.*;

public final class PDefaultMapSupportTest
{
	@Test
	public void
	testDefaultContainment()
	{
		PDefaultMap<Integer, Integer> pmap = new PDefaultMap<Integer, Integer>();

		assertEquals(null, pmap.getDefault());
		pmap.setDefault(42);
		assertEquals(42, (int) pmap.getDefault());
	}

	@Test
	public void
	testDefaultDefault()
	{
		PDefaultMap<Integer, Integer> pmap = new PDefaultMap<Integer, Integer>();
		assertNull(pmap.get("foo"));
		assertNull(pmap.get(1));
		assertNull(pmap.get('0'));
	}

	@Test
	public void
	testDefaultValue()
	{
		PDefaultMap<Integer, Integer> pmap = new PDefaultMap<Integer, Integer>(1);
		assertEquals(1, (int) pmap.get("foo"));
		assertEquals(1, (int) pmap.get(1));
		assertEquals(1, (int) pmap.get('0'));

		pmap.setDefault(7);

		assertEquals(7, (int) pmap.get("foo"));
		assertEquals(7, (int) pmap.get(1));
		assertEquals(7, (int) pmap.get('0'));
	}

	@Test
	public void
	testDefaultingLookup()
	{
		PDefaultMap<Integer, Integer> pmap = new PDefaultMap<Integer, Integer>(7);

		pmap.put(1, 19);

		assertEquals(7, (int) pmap.get("foo"));
		assertEquals(19, (int) pmap.get(1));
		assertEquals(7, (int) pmap.get('0'));
	}

	@Test
	public void
	testDefaultingLookupClone()
	{
		PDefaultMap<Integer, Integer> pmap1 = new PDefaultMap<Integer, Integer>(7);

		pmap1.put(1, 19);

		PDefaultMap<Integer, Integer> pmap2 = pmap1.clone();

		pmap2.put(2, 23);

		assertEquals(7, (int) pmap1.get("foo"));
		assertEquals(7, (int) pmap2.get("foo"));
		assertEquals(19, (int) pmap1.get(1));
		assertEquals(19, (int) pmap2.get(1));
		assertEquals(7, (int) pmap1.get(2));
		assertEquals(23, (int) pmap2.get(2));
	}

	@Test
	public void
	testEqualsNoDefault()
	{
		PDefaultMap<Integer, Integer> pmap = new PDefaultMap<Integer, Integer>();
		HashMap<Integer, Integer> hmap = new HashMap<Integer, Integer>();

		pmap.put(1, 19);

		assertFalse(pmap.equals(hmap));
		assertFalse(hmap.equals(pmap));

		hmap.put(1, 19);

		assertTrue(pmap.equals(hmap));
		assertTrue(hmap.equals(pmap));
	}

	@Test
	public void
	testEqualsDefault()
	{
		PDefaultMap<Integer, Integer> pmap = new PDefaultMap<Integer, Integer>(17);
		HashMap<Integer, Integer> hmap = new HashMap<Integer, Integer>();

		pmap.put(1, 19);

		assertFalse(pmap.equals(hmap));
		assertFalse(hmap.equals(pmap));

		hmap.put(1, 19);

		// not symmetric, unfortunately... ideally, both should be false.
		assertFalse(pmap.equals(hmap));
		assertTrue(hmap.equals(pmap));
	}

	@Test
	public void
	testEqualsDefaultOtherPMap()
	{
		PDefaultMap<Integer, Integer> pmap1 = new PDefaultMap<Integer, Integer>(17);
		PDefaultMap<Integer, Integer> pmap2 = new PDefaultMap<Integer, Integer>();

		assertFalse(pmap1.equals(pmap2));
		assertFalse(pmap2.equals(pmap1));

		assertTrue(pmap1.equals(pmap1));
		assertTrue(pmap2.equals(pmap2));

		pmap2.setDefault(17);

		assertTrue(pmap1.equals(pmap2));
		assertTrue(pmap2.equals(pmap1));

		assertTrue(pmap1.equals(pmap1));
		assertTrue(pmap2.equals(pmap2));

	}
}