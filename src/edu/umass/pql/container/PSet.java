/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.container;

import java.util.*;
import java.lang.reflect.Array;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Parallel set
 *
 * This set is designed to be easy to access through parallel fan-out.  In particular,
 * its backing store is a single flat array.
 *
 * Equality in this set is identity equality.
 *
 * @version 0.1.0
 */
public final class PSet<E> implements Set<E>
{
	private static final int MAX_OCCUPANCY_SHIFT = 2; // grow iff:  occupancy > (table.size >> MAX_OCCUPANCY_SHIFT)
	private static final int MIN_OCCUPANCY_SHIFT = 4; // shrink iff:  occupancy < (table.size >> MIN_OCCUPANCY_SHIFT)

	private Object[] table;
	private AtomicInteger occupancy = new AtomicInteger(0); // number of elts stored

	private void
	resize(final int new_size)
	{
		if (new_size < 2)
			return;

		Object[] new_table = new Object[new_size];
		for (int i = 0; i < this.table.length; i++) {
			final Object o = this.table[i];
			if (o != null)
				add_to(new_table, o);
		}
		this.table = new_table;
	}

	public final Object[]
	getRepresentation()
	{
		return this.table;
	}

	private static final int
	do_hash(Object o)
	{
		return 7 * o.hashCode(); // Jikes RVM: please replace this by hashing by object address
	}

	public PSet()
	{
		this.table = new Object[2];
	}

	public PSet(Object[] collection, int initial_occupancy)
	{
		this.table = collection;
		this.occupancy.set(initial_occupancy);
	}

	public PSet(Collection<E> elements)
	{
		this.table = new Object[2];
		addAll(elements);
	}

	public static <X> PSet<X>
	fromCollection(Collection<X> collection)
	{
		return new PSet<X>(collection);
	}

	private final void
	add_to(Object[] dest, Object o)
	{
		final int size = dest.length;
		int pos = do_hash(o) & (size - 1);

		while (dest[pos] != null)
			pos = (pos+1) & (size - 1);
		dest[pos] = o;
	}

	/**
	 * Adds the specified element to this set if it is not already present (optional operation).
	 *
	 * @return true iff element was new
	 */
	public boolean
	add(E e)
	{
		final int size = this.table.length;
		Object elt;
		int pos = do_hash(e) & (size - 1);
		final Object[] dest = this.table;

		while ((elt = dest[pos]) != null) {
			if (elt == e || elt.equals(e))
				return false;
			pos = (pos+1) & (size - 1);
		}
		dest[pos] = e;
		if (this.occupancy.incrementAndGet() > (size >> MAX_OCCUPANCY_SHIFT))
			resize(size << 1);
		return true;
	}

	/**
	 * Adds all of the elements in the specified collection to this set if they're not already present (optional operation).
	 *
	 * @return true iff set changed
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean
	addAll(Collection<? extends E> c)
	{
		boolean retval = false;
		if (c instanceof PSet) {
			final PSet<? extends E> other = (PSet<? extends E>) c;
			for (Object e : other.table)
				if (e != null)
					retval |= this.add((E) e);
		} else {
			for (E e : c)
				retval |= this.add(e);
		}
		return retval;
	}

	/**
	 * Removes all of the elements from this set (optional operation).
	 */
	public void
	clear()
	{
		this.occupancy.set(0);
		this.table = new Object[2];
	}

	/**
	 * Returns true if this set contains the specified element.
	 *
	 */
	public boolean
	contains(Object o)
	{
		if (o == null)
			return false;
		final int size = this.table.length;
		Object elt;
		int pos = do_hash(o) & (size - 1);
		final Object[] dest = this.table;

		while ((elt = dest[pos]) != null) {
			if (elt == o || elt.equals(o))
				return true;
			pos = (pos+1) & (size - 1);
		}

		return false;
	}

	/**
	 * Returns true if this set contains all of the elements of the specified collection.
	 */
	public boolean
	containsAll(Collection<?> c)
	{
		for (Object o : c)
			if (!this.contains(o))
				return false;
		return true;
	}

	/**
	 * Compares the specified object with this set for equality.
	 */
	@SuppressWarnings("unchecked")
	public boolean
	equals(Object other)
	{
		if (other == this)
			return true;
		if (!(other instanceof Set))
			return false;
		Set other_c = (Set) other;
		if (this.size() != other_c.size())
			return false;
		for (Object o : other_c)
			if (!this.contains(o))
				return false;
		for (E e : this)
			if (!other_c.contains(e))
				return false;
		return true;
	}

	/**
	 * Returns the hash code value for this set (sum of all element hashcodes)
	 */
	public int
	hashCode()
	{
		int code = 0;
		for (E e : this)
			code += e.hashCode();
		return code;
	}

	/**
	 * Returns true if this set contains no elements.
	 */
	public boolean
	isEmpty()
	{
		return this.occupancy.get() == 0;
	}

	/**
	 * Returns an iterator over the elements in this set.
	 */
	public Iterator<E>
	iterator()
	{
		return new PSetIterator();
	}

	/**
	 * Removes the specified element from this set if it is present (optional operation).
	 *
	 * @return true iff the set was changed.
	 */
	public boolean
	remove(Object e)
	{
		final int table_length = this.table.length;
		Object elt;
		int pos = do_hash(e) & (table_length - 1);
		final Object[] dest = this.table;

		while ((elt = table[pos]) != null) {
			if (elt == e) {
				int end_pos = pos + 1;
				dest[pos] = null;

				// We can't just remove the element, for otherwise we would break the re-hash chain.
				// We have to continue until we hit a `natural' end for the chain (null) and fill in
				// any gaps we left, unless that would require moving elements to before their natural hash position.

				// Phase 1:  pos < end_pos
				while (end_pos < table_length
				       && dest[end_pos] != null) {
					if ((do_hash(dest[end_pos]) & (table_length - 1)) <= pos) { // should we plug the current hole?
						dest[pos] = dest[end_pos];
						pos = end_pos;
						dest[end_pos] = null;
					}

					++end_pos;
				}

				if (end_pos >= table_length) {
					// wrap around
					end_pos -= table_length;
					// Phase 2: end_pos wrapped around.  end_pos < pos
					while (dest[end_pos] != null
					       && pos > end_pos) {
						if ((do_hash(dest[end_pos]) & (table_length - 1)) > end_pos) { // plug the current hole?
							dest[pos] = dest[end_pos];
							pos = end_pos;
							dest[end_pos] = null;
						}

						++end_pos;
					}

					// Phase 2: both pos and end_pos wrapped around.  pos < end_xpos
					while (dest[end_pos] != null) {
						if ((do_hash(dest[end_pos]) & (table_length - 1)) <= pos) { // should we plug the current hole?
							dest[pos] = dest[end_pos];
							pos = end_pos;
							dest[end_pos] = null;
						}
						++end_pos;
					}
					// No further wrap-around possible due to table occupancy limit.
				}

				if (this.occupancy.decrementAndGet() < (table_length >> MIN_OCCUPANCY_SHIFT))
					this.resize(table_length >> 1);
				return true;
			}
			pos = (pos+1) & (table_length - 1);
		}
		return false;
	}

	/**
	 * Removes from this set all of its elements that are contained in the specified collection (optional operation).
	 */
	public boolean
	removeAll(Collection<?> c)
	{
		boolean retval = false;
		for (Object e : c)
			retval |= this.remove(e);
		return retval;
	}

	/**
	 * Retains only the elements in this set that are contained in the specified collection (optional operation).
	 */
	public boolean
	retainAll(Collection<?> c)
	{
		boolean retval = false;

		Iterator<E> it = this.iterator();
		while (it.hasNext()) {
			E e = it.next();
			if (!c.contains(e)) {
				retval = true;
				it.remove();
			}
		}
		return retval;
	}

	/**
	 * Returns the number of elements in this set (its cardinality).
	 */
	public int
	size()
	{
		return this.occupancy.get();
		// this.occupancy is only an estimate of the current size, so we must update it
		// int real_size = 0;
		// for (int i = 0; i < this.table.length; i++)
		// 	if (this.table[i] != null)
		// 		++real_size;

		// this.occupancy = real_size;

		// return real_size;
	}

	/**
	 * Returns an array containing all of the elements in this set.
	 */
	public Object[]
	toArray()
	{
		Object[] retval = new Object[this.occupancy.get()];
		return toArray(retval);
	}

	/**
	 * Returns an array containing all of the elements in this set; the runtime type of the returned array is that of the specified array.
	 */
	@SuppressWarnings("unchecked")
	public <T> T[]
	toArray(T[] a)
	{
		if (a.length < this.occupancy.get())
			a = (T[]) Array.newInstance(a.getClass().getComponentType(), this.occupancy.get());
		int pos = 0;
		for (E e : this)
			a[pos++] = (T)e;
		while (pos < a.length)
			a[pos++] = null;
		return a;
	}

	public String
	toString()
	{
		StringBuffer sb = new StringBuffer("[");
		boolean has_one = false;
		for (Object o : this) {
			if (has_one)
				sb.append(", ");
			else
				has_one = true;
			sb.append(o.toString());
		}
		sb.append("]");
		return sb.toString();
	}

	private final class PSetIterator implements Iterator<E>
	{
		private int offset = -1;
		private int last_offset = -1;
		private Object[] orig_table; /* copy of the table _reference_.  If the table is resized, we lose any updates.  This choice
					      * ensures that the iterator doesn't have to deal with the offset suddenly becoming invalid.
					      */
		private boolean copied = false; // We do copy the entire table on the first `remove', though.

		public PSetIterator()
		{
			this.orig_table = table;
			search_next();
		}

		private final void
		search_next()
		{
			int off = this.offset;
			final int tlen = orig_table.length;
			do {
				++off;
			} while (off < tlen && orig_table[off] == null);
			this.offset = off;
		}

		public boolean
		hasNext()
		{
			if (this.orig_table != table && !this.copied)
				throw new ConcurrentModificationException();
			return this.offset < table.length;
		}

		@SuppressWarnings("unchecked")
		public E
		next()
		{
			if (!hasNext())
				throw new NoSuchElementException();

			this.last_offset = this.offset;
			search_next();
			return (E) this.orig_table[this.last_offset];
		}

		public void
		remove()
		{
			if (this.last_offset == -1
			    || this.last_offset >= table.length
			    || this.orig_table[this.last_offset] == null)
				throw new IllegalStateException();
			if (this.orig_table == table) {
				Object[] table_copy = new Object[orig_table.length];
				this.copied = true;
				System.arraycopy(orig_table, 0, table_copy, 0, orig_table.length);
				this.orig_table = table_copy;
			}
			if (!PSet.this.remove(this.orig_table[this.last_offset]))
				throw new ConcurrentModificationException();
			this.orig_table[this.last_offset] = null;
		}

		public String
		toString()
		{
			String s  = "{off=" + this.offset + "; lastoff=" + this.last_offset;
			for (int i = 0; i < orig_table.length; i++)
				if (orig_table[i] != null)
					s = s + "; #" + i + "=" + orig_table[i];
			return s + "}";
		}
	}
}
