/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.container;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Parallel map
 *
 * This map is designed to be easy to access through parallel fan-out.  In particular,
 * its backing store is a single flat array.
 *
 * There's a bit too much copy &amp; paste in this implementation (specifically wrt PSet), since I had
 * little time.
 *
 * Equality in this map is identity equality.
 *
 * @version 0.1.0
 */
public final class PMap<K, V> extends AMap<K, V>
{
	public PMap()
	{
		super();
	}

	public PMap(Object[] representation, int initial_occupancy)
	{
		super(representation, initial_occupancy);
	}

	public PMap<K, V>
	clone()
	{
		return (PMap<K, V>)super.clone();
	}
}