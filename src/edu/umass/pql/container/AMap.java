/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.container;

import java.util.*;
import java.lang.reflect.Array;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Parallal Array map
 *
 * This map is designed to be easy to access through parallel fan-out.  In particular,
 * its backing store is a single flat array.
 *
 * There's a bit too much copy &amp; paste in this implementation (specifically wrt PSet), since I had
 * little time.
 *
 * Equality in this map is identity equality.
 *
 * @version 0.1.0
 */
public abstract class AMap<K, V> implements Map<K, V>, Cloneable
{
	private static final int INITIAL_SIZE = 2;
	private static final int MAX_OCCUPANCY_SHIFT = 3; // grow iff:  occupancy > (table.size >> MAX_OCCUPANCY_SHIFT)
	private static final int MIN_OCCUPANCY_SHIFT = 2; // shrink iff:  occupancy < (table.size >> MIN_OCCUPANCY_SHIFT)

	private Object[] table;
	private AtomicInteger occupancy = new AtomicInteger(0); // number of elts stored

	private void
	resize(final int new_size)
	{
		if (new_size < 2)
			return;

		Object[] new_table = new Object[new_size << 1];
		for (int i = 0; i < this.table.length; i += 2) {
			final Object k = this.table[i];
			if (k != null) {
				final Object v = this.table[i + 1];
				add_to(new_table, k, v);
			}
		}
		this.table = new_table;
	}

	private final void
	add_to(final Object[] dest, final Object k, final Object v)
	{
		final int size = dest.length;
		int pos = (do_hash(k) << 1) & (size - 1);

		while (dest[pos] != null)
			pos = (pos + 2) & (size - 1);
		dest[pos] = k;
		dest[pos + 1] = v;
	}

	public AMap()
	{
		this.table = new Object[INITIAL_SIZE << 1];
	}

	public AMap(Object[] representation, int initial_occupancy)
	{
		this.table = representation;
		this.occupancy.set(initial_occupancy);
	}

	private static final int
	do_hash(Object o)
	{
		return 7 * o.hashCode(); // Jikes RVM: please replace this by hashing by object address
	}

	private final int
	get_key_offset(Object o)
	{
		return (do_hash(o) << 1) & (table.length - 1);
	}

	/**
	 * The map representation alternates between keys (even addresses) and associated values (odd addresses).
	 * Empty fields are marked by null keys.
	 */
	public final Object[]
	getRepresentation()
	{
		return this.table;
	}

	/**
	 * Removes all of the mappings from this map (optional operation).
	 */
	public void
	clear()
	{
		this.table = new Object[INITIAL_SIZE << 1];
		this.occupancy.set(0);
	}

	/**
	 * Returns true if this map contains a mapping for the specified key.
	 */
	public boolean
	containsKey(Object key) 
	{
		int pos = get_key_offset(key);
		do {
			if (table[pos] == key)
				return true;
			pos = (pos + 2) & (table.length - 1);
		} while (table[pos] != null);

		return false;
	}

	/**
	 * Returns true if this map maps one or more keys to the specified value.
	 */
	public boolean
	containsValue(Object value)
	{
		for (int i = 1; i < table.length; i += 2)
			if (table[i] == value && table[i - 1] != null)
				return true;
		return false;
	}

	/**
	 * Returns a Set view of the mappings contained in this map.
	 *
	 * @return a Set view of the mappings contained in this
	 * map. The set is backed by the map, so changes to the map
	 * are reflected in the set, and vice-versa. If the map is
	 * modified while an iteration over the set is in progress
	 * (except through the iterator's own remove operation, or
	 * through the setValue operation on a map entry returned by
	 * the iterator) the results of the iteration are
	 * undefined. The set supports element removal, which removes
	 * the corresponding mapping from the map, via the
	 * Iterator.remove, Set.remove, removeAll, retainAll and clear
	 * operations. It does not support the add or addAll
	 * operations.
	 */
	public Set<Map.Entry<K,V>>
	entrySet() 
	{
		return new EntrySet();
	}

	/**
	 * Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
	 */
	@SuppressWarnings("unchecked")
	public V
	get(Object key) 
	{
		final int size = this.table.length;
		Object elt;
		int pos = get_key_offset(key);
		final Object[] dest = this.table;

		while ((elt = dest[pos]) != null) {
			if (elt == key || key.equals(elt))
				return (V) dest[pos + 1];
			pos = (pos + 2) & (size - 1);
		}
		return null;
	}

	/**
	 * Returns the hash code value for this map.
	 *
	 * Returns the hash code value for this map. The hash code of a map is defined to be the sum of the
	 * hash codes of each entry in the map's entrySet() view. This ensures that m1.equals(m2) implies that
	 * m1.hashCode()==m2.hashCode() for any two maps m1 and m2, as required by the general contract of Object.hashCode().
	 */
	public int
	hashCode() 
	{
		int result = 0;
		for (Map.Entry<K, V> entry: entrySet())
			result += entry.hashCode();
		return result;
	}

	/**
	 * Returns true if this map contains no key-value mappings.
	 */
	public boolean
	isEmpty()
	{
		return this.occupancy.get() == 0;
	}

	/**
	 * Returns a Set view of the keys contained in this map.
	 *
	 * Returns a Set view of the keys contained in this map. The set is backed by the map, so changes to the map are
	 * reflected in the set, and vice-versa. If the map is modified while an iteration over the set is in progress
	 * (except through the iterator's own remove operation), the results of the iteration are undefined. The set supports
	 * element removal, which removes the corresponding mapping from the map, via the Iterator.remove, Set.remove, removeAll,
	 * retainAll, and clear operations. It does not support the add or addAll operations.
	 */
	public Set<K>
	keySet()
	{
		return new KeySet();
	}

	/**
	 * Associates the specified value with the specified key in this map (optional operation).
	 *
	 * @return previous mapping or null if not applicable
	 */
	@SuppressWarnings("unchecked")
	public V
	put(K key, V value)
	{
		final int size = this.table.length;
		Object elt;
		int pos = get_key_offset(key);
		final Object[] dest = this.table;

		while ((elt = dest[pos]) != null) {
			if (elt == key) {
				Object old_value = dest[pos + 1];
				dest[pos + 1] = value;
				return (V) old_value;
			}
			pos = (pos + 2) & (size - 1);
		}
		dest[pos] = key;
		dest[pos+1] = value;
		if (this.occupancy.incrementAndGet() > (size >> MAX_OCCUPANCY_SHIFT))
			resize(size << 1);
		return null;
	}

	/**
	 * Compares the specified object with this map for equality.
	 */
	@SuppressWarnings("unchecked")
	public boolean
	equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof Map))
			return false;
		Map other = (Map) o;

		if (this.size() != other.size())
			return false;

		for (Object k : other.keySet()) {
			if (k == null) {
				return false;
			}
			final Object v1 = this.get(k);
			final Object v2 = other.get(k);

			if (v1 == v2) {
				continue;
			}
			if (v1 == null || !v1.equals(v2)) {
				return false;
			}
		}

		for (Object k : this.keySet()) {

			final Object v1 = this.get(k);
			final Object v2 = other.get(k);

			if (v1 == v2)
				continue;
			if (v1 == null || !v1.equals(v2)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Copies all of the mappings from the specified map to this map (optional operation).
	 */
	public void
	putAll(Map<? extends K,? extends V> m)
	{
		for (Map.Entry<? extends K, ? extends V> entry : m.entrySet())
			this.put(entry.getKey(), entry.getValue());
	}

	/**
	 * Removes the mapping for a key from this map if it is present (optional operation).
	 */
	@SuppressWarnings("unchecked")
	public V
	remove(Object key)
	{
		final int table_length = this.table.length;
		Object elt;
		int pos = this.get_key_offset(key);
		final Object[] dest = this.table;

		while ((elt = table[pos]) != null) {
			if (elt == key) {
				final Object old_value = dest[pos + 1];
				int end_pos = pos + 2;
				dest[pos] = null;

				// We can't just remove the element, for otherwise we would break the re-hash chain.
				// We have to continue until we hit a `natural' end for the chain (null) and fill in
				// any gaps we left, unless that would require moving elements to before their natural hash position.

				// Phase 1:  pos < end_pos
				while (end_pos < table_length
				       && dest[end_pos] != null) {
					if ((do_hash(dest[end_pos]) & (table_length - 1)) <= pos) { // should we plug the current hole?
						dest[pos] = dest[end_pos];
						pos = end_pos;
						dest[end_pos] = null;
					}

					end_pos += 2;
				}

				if (end_pos >= table_length) {
					// wrap around
					end_pos -= table_length;
					// Phase 2: end_pos wrapped around.  end_pos < pos
					while (dest[end_pos] != null
					       && pos > end_pos) {
						if ((do_hash(dest[end_pos]) & (table_length - 1)) > end_pos) { // plug the current hole?
							dest[pos] = dest[end_pos];
							pos = end_pos;
							dest[end_pos] = null;
						}
						
						end_pos += 2;
					}

					// Phase 2: both pos and end_pos wrapped around.  pos < end_xpos
					while (dest[end_pos] != null) {
						if ((do_hash(dest[end_pos]) & (table_length - 1)) <= pos) { // should we plug the current hole?
							dest[pos] = dest[end_pos];
							pos = end_pos;
							dest[end_pos] = null;
						}
						end_pos += 2;
					}
					// No further wrap-around possible due to table occupancy limit.
				}

				if (this.occupancy.decrementAndGet() < (table_length >> MIN_OCCUPANCY_SHIFT))
					this.resize(table_length >> 1);
				return (V) old_value;
			}
			pos = (pos + 2) & (table_length - 1);
		}
		return null;
	}

	/**
	 * Returns the number of key-value mappings in this map.
	 */
	public int
	size()
	{
		return this.occupancy.get();
		// // this.occupancy is only an estimate of the current size, so we must update it
		// int real_size = 0;
		// for (int i = 0; i < this.table.length; i += 2)
		// 	if (this.table[i] != null)
		// 		++real_size;

		// this.occupancy = real_size;

		// return real_size;
	}

	/**
	 * Returns a Collection view of the values contained in this map.
	 *
	 * Returns a Collection view of the values contained in this map. The collection is backed by the
	 * map, so changes to the map are reflected in the collection, and vice-versa. If the map is modified while an
	 * iteration over the collection is in progress (except through the iterator's own remove operation),
	 * the results of the iteration are undefined. The collection supports element removal, which removes the
	 * corresponding mapping from the map, via the Iterator.remove, Collection.remove, removeAll, retainAll and
	 * clear operations. It does not support the add or addAll operations.
	 */
	public Collection<V>
	values()
	{
		return new ValueCollection();
	}

	private class AMapSliceIterator<T> implements Iterator<T>
	{
		protected int offset = -2;
		protected int last_offset = -1;
		protected Object[] orig_table; /* copy of the table _reference_.  If the table is resized, we lose any updates.  This choice
					      * ensures that the iterator doesn't have to deal with the offset suddenly becoming invalid.
					      */
		private boolean copied = false; // We do copy the entire table on the first `remove', though.
		private int delta;
		static final int FOR_KEY = 0;
		static final int FOR_VALUE = 1;

		public AMapSliceIterator(int delta)
		{
			this.delta = delta;
			this.orig_table = table;
			search_next();
		}

		protected final void
		search_next()
		{
			int off = this.offset;
			final int tlen = orig_table.length;
			do {
				off += 2;
			} while (off < tlen
				 && orig_table[off] == null);
			this.offset = off;
		}

		public boolean
		hasNext()
		{
			if (this.orig_table != table && !this.copied)
				throw new ConcurrentModificationException();
			return this.offset < table.length;
		}

		@SuppressWarnings("unchecked")
		public T
		next()
		{
			if (!hasNext())
				throw new NoSuchElementException();

			this.last_offset = this.offset;
			search_next();
			return (T) this.orig_table[this.last_offset + this.delta];
		}

		public void
		remove()
		{
			if (this.last_offset < 0
			    || this.last_offset >= table.length
			    || this.orig_table[this.last_offset] == null)
				throw new IllegalStateException();
			if (this.orig_table == table) {
				Object[] table_copy = new Object[orig_table.length];
				this.copied = true;
				System.arraycopy(orig_table, 0, table_copy, 0, orig_table.length);
				this.orig_table = table_copy;
			}
			AMap.this.remove(this.orig_table[this.last_offset]);
			this.orig_table[this.last_offset] = null;
		}

		public String
		toString()
		{
			String s  = "{off=" + this.offset + "; lastoff=" + this.last_offset + "; d=" + this.delta;
			for (int i = 0; i < orig_table.length; i+=2)
				if (orig_table[i] != null)
					s = s + "; #" + i + "=" + orig_table[i] + ":" + orig_table[i + 1];
			return s + "}";
		}

	}

	@SuppressWarnings("unchecked")
	public AMap<K, V>
	clone()
	{
		try {
			final AMap<K, V> retval = (AMap<K, V>)super.clone();
			final Object[] src_table = retval.table;
			retval.table = new Object[src_table.length];
			System.arraycopy(src_table, 0, retval.table, 0, src_table.length);
			return retval;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	/* ================================================================================ */

	public final class ValueCollection extends AbstractCollection<V>
	{
		@Override
		public Iterator<V>
		iterator()
		{
			return new AMapSliceIterator<V>(AMapSliceIterator.FOR_VALUE);
		}

		@Override
		public int
		size()
		{
			return occupancy.get();
		}

		@Override
		public boolean add(V e) { throw new UnsupportedOperationException(); }

		@Override
		public boolean addAll(Collection<? extends V> c) { throw new UnsupportedOperationException(); }

		@Override
		public void
		clear()
		{
			AMap.this.clear();
		}

		@Override
		public boolean
		contains(Object o)
		{
			return AMap.this.containsValue(o);
		}

		@Override
		public boolean
		isEmpty()
		{
			return occupancy.get() == 0;
		}

		/**
		 * Removes a single instance of the specified element from this collection, if it is present (optional operation).
		 */
		@Override
		public boolean
		remove(Object value)
		{
			for (int i = 1; i < table.length; i += 2)
				if (table[i] == value && table[i - 1] != null) {
					AMap.this.remove(table[i - 1]);
					return true;
				}
			return false;
		}

		/**
		 * Returns an array containing all of the elements in this set.
		 */
		@Override
		public Object[]
		toArray()
		{
			Object[] retval = new Object[occupancy.get()];
			return toArray(retval);
		}
          
		/**
		 * Returns an array containing all of the elements in this set; the runtime type of the returned array is that of the specified array.
		 */
		@SuppressWarnings("unchecked")
		@Override
		public <T> T[]
		toArray(T[] a)
		{
			if (a.length < occupancy.get())
				a = (T[]) Array.newInstance(a.getClass().getComponentType(), occupancy.get());

			this.iterator();
			int pos = 0;
			for (V e : this)
				a[pos++] = (T)e;
			while (pos < a.length)
				a[pos++] = null;
			return a;
		}
	}
	/* -------------------------------------------------------------------------------- */
	/* -------------------------------------------------------------------------------- */

	public final class KeySet extends AbstractSet<K>
	{
		/**
		 * Adds the specified element to this set if it is not already present (optional operation).
		 *
		 * @return true iff element was new
		 */
		public boolean
		add(K e)
		{
			throw new UnsupportedOperationException();
		}

		/**
		 * Adds all of the elements in the specified collection to this set if they're not already present (optional operation).
		 *
		 * @return true iff set changed
		 */
		public boolean
		addAll(Collection<? extends K> c)
		{
			throw new UnsupportedOperationException();
		}

		/**
		 * Removes all of the elements from this set (optional operation).
		 */
		public void
		clear()
		{
			AMap.this.clear();
		}

		/**
		 * Returns true if this set contains the specified element.
		 *
		 */
		public boolean
		contains(Object o)
		{
			return AMap.this.containsKey(o);
		}

		/**
		 * Returns true if this set contains all of the elements of the specified collection.
		 */
		public boolean
		containsAll(Collection<?> c)
		{
			for (Object o : c)
				if (!AMap.this.containsKey(o))
					return false;
			return true;
		}
          
		/**
		 * Returns true if this set contains no elements.
		 */
		public boolean
		isEmpty()
		{
			return AMap.this.occupancy.get() == 0;
		}
          
		/**
		 * Returns an iterator over the elements in this set.
		 */
		@Override
		public Iterator<K>
		iterator()
		{
			return new AMapSliceIterator<K>(AMapSliceIterator.FOR_KEY);
		}
          
		/**
		 * Removes the specified element from this set if it is present (optional operation).
		 *
		 * @return true iff the set was changed.
		 */
		public boolean
		remove(Object e)
		{
			return (AMap.this.remove(e)) != null;
		}

		/**
		 * Removes from this set all of its elements that are contained in the specified collection (optional operation).
		 */
		public boolean
		removeAll(Collection<?> c)
		{
			boolean retval = false;
			for (Object e : c)
				retval |= this.remove(e);
			return retval;
		}
          
		/**
		 * Retains only the elements in this set that are contained in the specified collection (optional operation).
		 */
		public boolean
		retainAll(Collection<?> c)
		{
			boolean retval = false;

			Iterator<K> it = this.iterator();
			while (it.hasNext()) {
				K e = it.next();
				if (!c.contains(e)) {
					retval = true;
					it.remove();
				}
			}
			return retval;
		}
          
		/**
		 * Returns the number of elements in this set (its cardinality).
		 */
		public int
		size()
		{
			return occupancy.get();
		}
          
		/**
		 * Returns an array containing all of the elements in this set.
		 */
		public Object[]
		toArray()
		{
			Object[] retval = new Object[occupancy.get()];
			return toArray(retval);
		}
          
		/**
		 * Returns an array containing all of the elements in this set; the runtime type of the returned array is that of the specified array.
		 */
		@SuppressWarnings("unchecked")
		public <T> T[]
		toArray(T[] a)
		{
			if (a.length < occupancy.get())
				a = (T[]) Array.newInstance(a.getClass().getComponentType(), occupancy.get());
			int pos = 0;
			for (K e : this)
				a[pos++] = (T)e;
			while (pos < a.length)
				a[pos++] = null;
			return a;
		}
	}

	/* -------------------------------------------------------------------------------- */

	public final class EntrySet extends AbstractSet<Map.Entry<K, V>>
	{
		/**
		 * Adds the specified element to this set if it is not already present (optional operation).
		 *
		 * @return true iff element was new
		 */
		@SuppressWarnings("unused")
		public boolean
		add(Entry e)
		{
			throw new UnsupportedOperationException();
		}

		/**
		 * Adds all of the elements in the specified collection to this set if they're not already present (optional operation).
		 *
		 * @return true iff set changed
		 */
		public boolean
		addAll(Collection<? extends Map.Entry<K, V>> c)
		{
			throw new UnsupportedOperationException();
		}

		/**
		 * Removes all of the elements from this set (optional operation).
		 */
		public void
		clear()
		{
			AMap.this.clear();
		}

		/**
		 * Returns true if this set contains the specified element.
		 *
		 */
		@SuppressWarnings("unchecked")
		public boolean
		contains(Object o)
		{
			if (o == null
			    || !(o instanceof Map.Entry))
				return false;
			Map.Entry<?, ?> other = (Map.Entry<?, ?>) o;
			return AMap.this.containsKey(other.getKey())
				&& AMap.this.get(other.getKey()) == other.getValue();
		}

		/**
		 * Returns true if this set contains all of the elements of the specified collection.
		 */
		public boolean
		containsAll(Collection<?> c)
		{
			for (Object o : c)
				if (!this.contains(o))
					return false;
			return true;
		}
          
		/**
		 * Returns true if this set contains no elements.
		 */
		public boolean
		isEmpty()
		{
			return occupancy.get() == 0;
		}
          
		/**
		 * Returns an iterator over the elements in this set.
		 */
		public Iterator<Map.Entry<K, V>>
		iterator()
		{
			return new EntryIterator();
		}
          
		/**
		 * Removes the specified element from this set if it is present (optional operation).
		 *
		 * @return true iff the set was changed.
		 */
		public boolean
		remove(Object e)
		{
			if (contains(e)) {
				AMap.this.remove(((Map.Entry<?, ?>) e).getKey());
				return true;
			}
			return false;
		}

		/**
		 * Removes from this set all of its elements that are contained in the specified collection (optional operation).
		 */
		public boolean
		removeAll(Collection<?> c)
		{
			boolean retval = false;
			for (Object e : c)
				retval |= this.remove(e);
			return retval;
		}
          
		/**
		 * Retains only the elements in this set that are contained in the specified collection (optional operation).
		 */
		public boolean
		retainAll(Collection<?> c)
		{
			boolean retval = false;

			Iterator<Map.Entry<K, V>> it = this.iterator();
			while (it.hasNext()) {
				Map.Entry<K, V> e = it.next();
				if (!c.contains(e)) {
					retval = true;
					it.remove();
				}
			}
			return retval;
		}
          
		/**
		 * Returns the number of elements in this set (its cardinality).
		 */
		public int
		size()
		{
			return AMap.this.occupancy.get();
		}
          
		/**
		 * Returns an array containing all of the elements in this set.
		 */
		public Object[]
		toArray()
		{
			Object[] retval = new Object[occupancy.get()];
			return toArray(retval);
		}
          
		/**
		 * Returns an array containing all of the elements in this set; the runtime type of the returned array is that of the specified array.
		 */
		@SuppressWarnings("unchecked")
		public <T> T[]
		toArray(T[] a)
		{
			if (a.length < occupancy.get())
				a = (T[]) Array.newInstance(a.getClass().getComponentType(), occupancy.get());
			int pos = 0;
			for (Map.Entry<K, V> e : this)
				a[pos++] = (T)e;
			while (pos < a.length)
				a[pos++] = null;
			return a;
		}
	}

	// ----------------------------------------------------------------------

	public final class Entry implements Map.Entry<K, V>
	{
		private int offset;

		public Entry(int offset)
		{
			this.offset = offset;
		}

		/**
		 *  Compares the specified object with this entry for equality.
		 */
		@Override
		@SuppressWarnings("unchecked")
		public boolean
		equals(Object o)
		{
			if (o == null)
				return false;
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry<?, ?> other = (Map.Entry<?, ?>) o;
			if (other.getKey() != this.getKey()
			    && (other.getKey() == null
				|| !other.getKey().equals(this.getKey())))
				return false;

			if (other.getValue() != this.getValue()
			    && (other.getValue() == null
				|| !other.getValue().equals(this.getValue())))
				return false;
			return true;
		}

		/**
		 * Returns the key corresponding to this entry.
		 */
		//@Override // commented out for JDK 1.5 bug
		@SuppressWarnings("unchecked")
		public K
		getKey() 
		{
			return (K) AMap.this.table[offset];
		}

		/**
		 * Returns the value corresponding to this entry.
		 */
		//@Override // commented out for JDK 1.5 bug
		@SuppressWarnings("unchecked")
		public V
		getValue() 
		{
			return (V) AMap.this.table[offset + 1];
		}

		/**
		 * Returns the hash code value for this map entry.
		 */
		@Override
		public int
		hashCode()
		{
			return (getKey()==null ? 0 : getKey().hashCode()) ^
				(getValue()==null ? 0 : getValue().hashCode());
		}

		/**
		 * Replaces the value corresponding to this entry with the specified value (optional operation).
		 * @return old value
		 */
		//@Override // commented out for JDK 1.5 bug
		@SuppressWarnings("unchecked")
		public V
		setValue(V value) 
		{
			final V old_val = (V) AMap.this.table[offset + 1];
			AMap.this.table[offset + 1] = value;
			return old_val;
		}

		@Override
		public String
		toString()
		{
			return getKey() + "=" + getValue();
		}
	}

	// ----------------------------------------------------------------------

	private final class EntryIterator extends AMapSliceIterator<Map.Entry<K, V>>
	{
		public EntryIterator()
		{
			super(0);
		}

		public Map.Entry<K, V>
		next()
		{
			if (!hasNext())
				throw new NoSuchElementException();

			this.last_offset = this.offset;
			search_next();
			return new Entry(this.last_offset);
		}
	}

	@Override
	public String
	toString()
	{
		StringBuffer retval = new StringBuffer("{");
		boolean nonfirst = false;

		for (Map.Entry<K, V> entry : this.entrySet()) {
			if (nonfirst)
				retval.append(", ");
			else
				nonfirst = true;
			retval.append(entry.getKey() + "=" + entry.getValue());
		}
		retval.append("}");
		return retval.toString();
	}
}
