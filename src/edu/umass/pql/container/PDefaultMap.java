/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.container;

import java.util.Map;

/**
 * Parallel map
 *
 * This map is designed to be easy to access through parallel fan-out.  In particular,
 * its backing store is a single flat array.
 *
 * There's a bit too much copy &amp; paste in this implementation (specifically wrt PSet), since I had
 * little time.
 *
 * Equality in this map is identity equality.
 *
 * @version 0.1.0
 */
public final class PDefaultMap<K, V> extends AMap<K, V>
{
	private V default_value;

	public PDefaultMap()
	{
		super();
	}

	public PDefaultMap(Object[] representation, int initial_occupancy)
	{
		super(representation, initial_occupancy);
	}

	public PDefaultMap(V deflt)
	{
		super();
		setDefault(deflt);
	}

	public PDefaultMap(Object[] representation, int initial_occupancy, V deflt)
	{
		super(representation, initial_occupancy);
		setDefault(deflt);
	}

	// @Override
	// public V
	// put(K key, V value)
	// {
	// 	if (value == this.default_value) {
	// 		if (this.containsKey(key)) {
	// 			final V old_value = this.get(key);
	// 			this.remove(key);
	// 			return old_value;
	// 		} else
	// 			return null;
	// 	} else
	// 		return super.put(key, value);
	// }

	void // not public; it doesn't make sense to alter default value later
	setDefault(V deflt)
	{
		this.default_value = deflt;
	}

	public V
	getDefault()
	{
		return this.default_value;
	}

	public PDefaultMap<K, V>
	clone()
	{
		return (PDefaultMap<K, V>) super.clone();
	}

	@Override
	public V
	get(Object key)
	{
		final V retval = super.get(key);
		if (retval == null)
			return this.default_value;
		return retval;
	}

	@SuppressWarnings("unchecked")
	public boolean
	equals(Object other)
	{
		// not symmetric, but this is only used for testing.
		if (super.equals(other)) {
			if (other instanceof PDefaultMap) {
				final PDefaultMap _other = (PDefaultMap) other;

				if (this.default_value == _other.default_value)
					return true;

				if (this.default_value == null)
					return false;

				return this.default_value.equals(_other.default_value); // infinite recursion if this.default_value = this.  But I won't implement a graph isomorphism algorithm for that case.
			}
			else
				return this.default_value == null;
		}
		return false;
	}

	@Override
	public String
	toString()
	{
		String retval = super.toString();

		if (this.default_value != null)
			retval += "; default = " + this.default_value;

		return "{" + retval + "}";
	}
}
