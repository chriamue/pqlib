/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;
import java.util.BitSet;
import java.util.Iterator;

/**
 * Normalises variables to eliminate wildcards (which cannot be added and are never part of the set) and equate read/write versions
 */
public final class VarSet implements Iterable<Integer>
{
	private final int SHIFT = VarConstants.VAR_TABLE_SHIFT;
	private BitSet set = new BitSet();

	public
	VarSet()
	{
	}

	public
	VarSet(int... initial_vars)
	{
		if (initial_vars != null) {
			for (int i : initial_vars)
				this.insert(i);
		}
	}

	public VarSet
	copy()
	{
		final VarSet retval = new VarSet();
		retval.set.or(this.set);
		return retval;
	}

	public void
	insert(int key)
	{
		if (!Env.isWildcard(key))
			set.set(key >> SHIFT);
	}

	public void
	remove(int key)
	{
		set.set(key >> SHIFT, false);
	}

	public boolean
	contains(int key)
	{
		return this.set.get(key >> SHIFT);
	}

	public boolean
	containsAll(VarSet other)
	{
		int size = other.set.size();
		this.set.flip(0, size);
		boolean result = !this.set.intersects(other.set);
		this.set.flip(0, size);
		return result;
	}

	public void
	clear()
	{
		this.set.clear();
	}

	public void
	addAll(VarSet set)
	{
		this.set.or(set.set);
	}

	public void
	removeAll(VarSet set)
	{
		this.set.andNot(set.set);
	}

	public void
	retainAll(VarSet set)
	{
		this.set.and(set.set);
	}

	public boolean
	equals(Object other)
	{
		if (other instanceof VarSet) {
			if (other == this)
				return true;
			return ((VarSet) other).set.equals(this.set);
		}
		return false;
	}

	public Iterator<Integer>
	iterator()
	{
		return new Iterator<Integer>() {
			int index = set.nextSetBit(0);

			public boolean
			hasNext()
			{
				return index >= 0;
			}

			public Integer
			next()
			{
				int var = index << SHIFT;
				index = set.nextSetBit(index + 1);
				return var;
			}

			public void remove()
			{
				throw new UnsupportedOperationException();
			}
		};
	}

	public int
	size()
	{
		int size = 0;
		for (int __: this)
			++size;
		return size;
	}

	public boolean
	isEmpty()
	{
		return this.set.isEmpty();
	}

	public String
	toString()
	{
		StringBuffer buf = new StringBuffer("{ ");
		boolean first = true;

		for(int var : this) {
			if (!first) {
				buf.append(", ");
			} else
				first = false;

			buf.append(Env.showVar(var));
		}
		if (first)
			buf.append("}");
		else
			buf.append(" }");
		return buf.toString();
	}
}
