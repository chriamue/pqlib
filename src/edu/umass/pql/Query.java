/***************************************************************************
 Copyright (C) 2010,2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


/**
 * Base access class for PQL queries.  This class contains static operations for constructing
 * PQL queries, as well as several static helper functions.
 *
 * @author creichen@gmail.com
 * @version 0.1.0
 */
public final class Query
	implements VarConstants, TableConstants
{
	public static Env
	query(Join join,
	      int flags,
	      Object[] tables)
	{
		Env env = new Env(flags, tables);
		join.reset(env);
		join.next(env);
		return env;
	}

	public static Env
	parallelQuery(Join join,
		      int flags,
		      Object[] tables)
	{
		Env env = new Env(flags, tables);
		return ParallelQuery.query(env, join);
	}

	// public static Object parallelQueryObject(Join join, int flags, Object[] tables) { return ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static byte parallelQueryByte(Join join, int flags, Object[] tables) { return (Byte) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static int parallelQueryInt(Join join, int flags, Object[] tables) { return (Integer) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static long parallelQueryLong(Join join, int flags, Object[] tables) { return (Long) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static char parallelQueryChar(Join join, int flags, Object[] tables) { return (Character) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static short parallelQueryShort(Join join, int flags, Object[] tables) { return (Short) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static boolean parallelQueryBoolean(Join join, int flags, Object[] tables) { return (Boolean) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static double parallelQueryDouble(Join join, int flags, Object[] tables) { return (Double) ParallelQuery.queryObject(new Env(flags, tables), join); }
	// public static float parallelQueryFloat(Join join, int flags, Object[] tables) { return (Float) ParallelQuery.queryObject(new Env(flags, tables), join); }

	/**
	 * Constructs a set with all elements from the specified range
	 *
	 * @param start the least element to add
	 * @param end the greatest element to add
	 * @return A set containing all specified elements
	 */
	public static Set<Integer>
	range(int start, int end)
	{
		Set<Integer> result = new HashSet<Integer>();
		for (; start <= end; start++)
			result.add(start);
		return result;
	}

	/**
	 * Determines whether the query system supports traversing the heap
	 *
	 * @return true iff heap traversals (get-all-objects-of-type) is supported
	 */
	public static boolean
	supportsHeapSearch()
	{
		return false;
	}

	@DefaultValueInt(0)
	public static int
	sumInt(int lhs, int rhs)
	{
		return lhs + rhs;
	}

	@DefaultValueLong(0l)
	public static long
	sumLong(long lhs, long rhs)
	{
		return lhs + rhs;
	}

	@DefaultValueDouble(0.0)
	public static double
	sumDouble(double lhs, double rhs)
	{
		return lhs + rhs;
	}

	/**
	 * Generic extension point
	 *
	 * @param name Name of the extension mechanism to invoke
	 * @param args List of arguments to the extension query
	 */
	public static boolean
	Extension(String name, Object ... args)
	{
		return false;
	}
}
