/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.il.reductor.ReductorVisitor;

/**
 * A reductor is an Abelian monoid.  Each reductor has an arbitrary number of inner
 * and outer read arguments, where `inner' arguments are arguments that may vary each time
 * the reductor is invoked and `outer' read arguments are arguments that must remain constant
 * throughout the use of the reductor.
 *
 * Each reductor has an `aggregate' object that can be exposed and reset.
 */
public abstract class Reductor implements Cloneable, PQLParameterized
{
	public final static int INPUT_FROM_INNER_REDUCTOR = -1;

	//@Override // JDK 1.5 is confused about this
        public final boolean
	isValidArg(int index)
	{
		return this.getArg(index) != INPUT_FROM_INNER_REDUCTOR;
	}

	public abstract void
	accept(ReductorVisitor visitor);

        public int
        accept(ReductorFlagsVisitor visitor) {return 0;};

	private boolean active;
	private int output_var;

	public Reductor(int output_var)
	{
		this.output_var = output_var;
	}

	/**
	 * Disables the reductor
	 */
	public final void
	disable()
	{
		active = true;
	}

	/**
	 * Tests whether the reductor is enabled
	 *
	 * @return false if the reductor has been disabled since the last reset, true otherwise
	 */
	public final boolean
	isEnabled()
	{
		return this.active;
	}

	/**
	 * Resets the reductor before processing a sequence of bindings.
	 *
	 * @param env The environment to read from (as needed)
	 */
	public final void
	setup(Env env)
	{
		this.active = true;
		setupInternal(env);
		reset();
	}

	/**
	 * Internal realisation of the `reset' method.
	 *
	 * This should extract any outer read arguments.
	 *
	 * @param env The environment to read from (as needed).
	 */
	@SuppressWarnings("unused")
	public void
	setupInternal(Env env)
	{
	}

	/**
	 * Resets the reductor.  Invoked only after setupInternal.
	 */
	protected abstract void
	reset();

	/**
	 * Extracts results from the reductor
	 *
	 * @return An object representing the reductor's aggregate.  Must be null if the evaluator failed.
	 */
	public abstract Object
	getAggregate();

	/**
	 * Changes the reductor's aggregate object.
	 *
	 * @param aggregate The reductor's new aggreagate object.  May be null to indicate the default value.
	 */
	public final void
	setAggregate(Object aggregate)
	{
		if (aggregate == null)
			this.reset();
		else
			this.setAggregateInternal(aggregate);
	}

	/**
	 * Changes the reductor's aggregate object.
	 *
	 * @param aggregate The reductor's new aggreagate object.  May be null to indicate the default value.
	 */
	public abstract void
	setAggregateInternal(Object aggregate);

	/**
	 * Pass a viable binding to the reductor
	 *
	 * @param env The environment containing the viable binding
	 * @return true iff the reductor needs no further processing for this binding sequence
	 */
	public abstract boolean
	succeed(Env env);

	/**
	 * Provides the number that the reductor uses overall
	 *
	 * @return number of variables
	 */
	public int
	getArgsNr()
	{
		return 1;
	}

	/**
	 * Provides the number of variables that the reductor reads from
	 *
	 * @return number of variables
	 */
	public int
	getReadArgsNr()
	{
		return 0;
	}

	/**
	 * Provides the number of variables that the reductor reads from outside of its loop, if any.
	 *
	 * These variables are always at the beginning of the variable list.
	 *
	 * @return Number of read-variables that must be initialised for the `Reduce' that this reductor is contained in
	 */
	public int
	getOuterReadArgsNr()
	{
		return 0;
	}

	public final int
	getInnerReadArgsNr()
	{
		return this.getReadArgsNr() - this.getOuterReadArgsNr();
	}

	/**
	 * Provides the ith variable written to for storing results
	 *
	 * @param Index of the variable to retrieve
	 * @return the ith variable
	 */
	public int
	getArg(int i)
	{
		if (i >= 0 && i < this.getArgsNr()) {
			if (i == this.getReadArgsNr())
				return this.output_var;
			else
				return this.getArgInternal(i);
		} else
			throw new RuntimeException("Invalid argument: " + i);
	}

	/**
	 * Provides the ith variable written to for storing results
	 *
	 * This is the internal helper function.  No bounds checking is needed.
	 *
	 * @param Index of the variable to retrieve
	 * @return the ith variable
	 */
	protected abstract int
	getArgInternal(int i);



	/**
	 * Updates the ith variable written to for storing results
	 *
	 * This is the internal helper function.  No bounds checking is needed.
	 *
	 * @param i Index of the variable to update
	 * @param v New variable
	 */
	protected abstract void
	setArgInternal(int i, int v);


	/**
	 * Updates the ith variable written to for storing results
	 *
	 * @param i Index of the variable to update
	 * @param v New variable
	 */
	public final void
	setArg(int i, int v)
	{
		if (i >= 0 && i < this.getArgsNr()) {
			if (i == this.getReadArgsNr())
				this.output_var = v;
			else
				this.setArgInternal(i, v);
		} else
			throw new RuntimeException("Invalid argument: " + i);
	}

	/**
	 * Merge other reductor into current reductor
	 *
	 * @param other The other aggregate to absorb
	 */
	public abstract void
	absorbAggregate(Object other_aggregate);

	/**
	 * Helper functions for when a reductor is used as a support reductor
	 *
	 */
	public final Object
	liftBindingInto(Env env)
	{
		this.setAggregate(null);
		succeed(env);
		return this.getAggregate();
	}

	/**
	 * Helper functions for when a reductor is used as a support reductor
	 */
	public final Object
	mergeBindingInto(Env env, Object past_aggregate)
	{
		this.setAggregate(past_aggregate);
		this.succeed(env);
		return this.getAggregate();
	}

	public final Object
	merge(Object a, Object b)
	{
		this.setAggregate(a);
		this.absorbAggregate(b);
		return this.getAggregate();
	}

	/**
	 * Evaluates the reductor
	 */
	public final boolean
	eval(Env env)
	{
		return env.unifyObject(output_var, this.getAggregate());
	}

	/**
	 * Create a self-clone capable of parallel execution and later mergeFrom operation
	 *
	 * @return a clone
	 */
	public Reductor
	copy()
	{
		try {
			return (Reductor) super.clone();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Sets an inner reductor to handle merges of values contained within the aggregate.
	 *
	 * This is only relevant to certain structures that allow nested merging, specifically maps.
	 * The inner reductor must be used iff the mergable inner argument index is marked with INPUT_FROM_INNER_REDUCTOR.
	 */
	@SuppressWarnings("unused")
	public void
	setInnerReductor(Reductor __)
	{
		throw new UnsupportedOperationException();
	}

	public Reductor
	getInnerReductor()
	{
		return null;
	}

	/**
	 * Retrieve the index at which the inner reductor would be used, or -1 if there is no such index
	 */
	public int
	getInnerReductorIndex()
	{
		return -1;
	}

	public boolean
	isMethodAdapter()
	{
		return false;
	}

	public abstract String
	getName();

	protected String
	toIDString()
	{
		return super.toString();
	}

	@Override
	public String
	toString()
	{
		StringBuffer buf = new StringBuffer(getName());
		buf.append("(");
		for (int i = 0; i < this.getArgsNr(); i++) {
			final int var = this.getArg(i);
			if (var == INPUT_FROM_INNER_REDUCTOR)
				buf.append(this.getInnerReductor());
			else
				buf.append(Env.showVar(var));
			if (i < this.getArgsNr() - 1) {
				// separator needed
				if (i == this.getOuterReadArgsNr() - 1)
					buf.append("; ");
				else if (i == this.getReadArgsNr() - 1)
					buf.append("): ");
				else
					buf.append(", ");
			}
		}
		if (this.getArgsNr() == this.getReadArgsNr())
			buf.append(")");
		return buf.toString();
	}

	public final void
	addAllVariables(VarSet set, boolean inner)
	{
		final int max = inner ? this.getReadArgsNr() : this.getArgsNr();

		for (int i = 0; i < max; i++) {
			final int var = this.getArg(i);
			if (var == Reductor.INPUT_FROM_INNER_REDUCTOR)
				this.getInnerReductor().addAllVariables(set, true);
			else
				set.insert(var);
		}
	}

	public final void
	addActualReads(VarSet set, VarSet exclusions)
	{
		for (int i = 0; i < this.getArgsNr(); i++) {
			final int v = this.getArg(i);
			if (v == INPUT_FROM_INNER_REDUCTOR)
				this.getInnerReductor().addActualReads(set, exclusions);
			else if (Env.isReadVar(v) && !Env.isWildcard(v)
				 && (exclusions == null || !exclusions.contains(v))) {
				set.insert(v);
			}
		}
	}

	public final void
	addReadDependencies(VarSet set, VarSet exclusions)
	{
		for (int i = 0; i < this.getArgsNr(); i++) {
			final int v = this.getArg(i);
			if (v == INPUT_FROM_INNER_REDUCTOR)
				this.getInnerReductor().addActualReads(set, exclusions);
			else if (Env.isReadVar(v) && !Env.isWildcard(v)
				 && (exclusions == null || !exclusions.contains(v))) {
				set.insert(v);
			}
		}
	}
}
