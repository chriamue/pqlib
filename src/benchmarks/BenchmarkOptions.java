/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks;

import rc2.RCInterface.ParallelMode;

/**
 *
 * @author Hilmar
 */
public class BenchmarkOptions {

    //Runtime-pql v2 options
    public static boolean antiOptimizedInstance = false;
    public static boolean antiOptimizedConst = false;
    public static ParallelMode parallelMode = ParallelMode.Serial;
    public static boolean optimizeStatic = false;
    public static boolean optimizeBytecode = false;
    public static boolean dataSwap = false;
    public static boolean optimizeDynamic = false;
    public static boolean initTimeJustMeasureAccessPath = false;
    public static boolean initTimeJustMeasureRealTime = false;
    public static boolean initTimeJustMeasureRemain = false;
    public static int parallelCores = 4;
    public static boolean oneTimeInit = false;

    //general
    public static int factor = 1;

    public static void setOption (String option) {
        if (option.startsWith("factor"))
            factor = Integer.parseInt(option.substring(6));
        else if (option.equals("constantiopt"))
            antiOptimizedConst = true;
        else if (option.equals("instanceantiopt"))
            antiOptimizedInstance = true;
        else if (option.equals("parallel"))
            parallelMode = ParallelMode.Parallel;
        else if (option.startsWith("parallel_cores_")) {
            parallelMode = ParallelMode.Parallel;
            parallelCores = Integer.parseInt(option.substring(15));
        } else if (option.equals("optimize_static"))
            optimizeStatic = true;
        else if (option.equals("optimize_bytecode"))
            optimizeBytecode = true;
        else if (option.equals("dataswap"))
            dataSwap = true;
        else if (option.equals("optimize_dynamic"))
            optimizeDynamic = true;
        else if (option.equals("just_accesspath"))
            initTimeJustMeasureAccessPath = true;
        else if (option.equals("just_real_init"))
            initTimeJustMeasureRealTime = true;
        else if (option.equals("just_remain"))
            initTimeJustMeasureRemain = true;
        else if (option.equals("one_time_init"))
            oneTimeInit = true;
    }
}
