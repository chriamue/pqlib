package benchmarks.bonus;
import benchmarks.Evaluator;

import benchmarks.SQL;
import java.sql.*;
import edu.umass.pql.container.*;
import java.util.*;

public class SQLBench extends Evaluator
{
	public SQLBench(SQL sql)
	{
		this.sql = sql;
	}

	public SQL sql;

	PreparedStatement query;

	@Override
	public void
	init()
	{
		Generator.install_sql(sql);
	}

	@Override
	public void
	cleanup()
	{
		Generator.uninstall_sql(sql);
	}

	@Override
	public void
	compute()
	{
		try {
			final String employees = sql.tableName("employees");
			final String boni = sql.tableName("boni");
			final String departments = sql.tableName("departments");
			//this.query = sql.prepareQuery("select " + employees + ".eid, SUM(" + boni + ".base * factor) FROM " + employees + " JOIN " + departments + " JOIN " + boni + " ON (" + boni + ".eid = " + employees + ".eid) WHERE " + departments + ".did = " + employees + ".dept GROUP BY " + employees + ".eid;");

			this.query = sql.prepareQuery("select "+employees+".eid, SUM("+boni+".base * factor) from "+employees+" JOIN "+departments+" ON ("+departments+".did = "+employees+".dept) JOIN "+boni+" ON ("+boni+".eid = "+employees+".eid) GROUP BY "+employees+".eid;");
			//this.query = sql.prepareQuery("select " + employees + ".eid, SUM(" + boni + ".base * factor) FROM " + employees + " JOIN " + departments + " JOIN " + boni + " ON (" + boni + ".eid = " + employees + ".eid);");

			PMap<Employee, Double> results = new PMap<Employee, Double>(); //Generator.EMPLOYEES_NR);
			final ResultSet res = sql.runQuery(query);

			// SQL query won't return anything for employees without bonus
			final boolean[] eidt = new boolean[Generator.EMPLOYEES_NR];

			while (res.next()) {
				int eid = res.getInt(1);
				eidt[eid] = true;
				results.put(Generator.employees_array[eid], (double)Math.round(res.getFloat(2)));
			}

			for (int i = 0; i < Generator.EMPLOYEES_NR; i++)
				if (!eidt[i])
					results.put(Generator.employees_array[i], 0.0);

			this.result = results;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String
	getName()
	{
		return this.sql.getDriverName();
	}
}
