package benchmarks.bonus;
import benchmarks.Evaluator;
import edu.umass.pql.container.PMap;
import java.util.*;

public class ParaManual extends Evaluator.ParaEvaluator
{
	Map<Employee, Double> results;

	class EvaluatorThread extends Thread
	{
		public EvaluatorThread(int start, int stop)
		{
			this.start = start;
			this.stop = stop;
		}
		int start, stop;

		public void run()
		{
			for (int i = start; i < stop; i++) {
				Employee employee = Generator.employees_array[i];
				final double dept_bonus_factor = employee.dept.bonus_factor;
				double total_bonus = 0.0;
				for (Bonus b : employee.bonusSet)
					total_bonus += b.bonus_base;
				results.put(employee, total_bonus * dept_bonus_factor);
			}
		}
	}

	@Override
	public Thread
	gen_thread(int __, int start, int stop)
	{
		return new EvaluatorThread(start, stop);
	}

	@Override
	public int
	get_size()
	{
		return Generator.employees_array.length;
	}

	@Override
	public void
	compute_prepare()
	{
		results = new PMap<Employee, Double>(new Object[Generator.EMPLOYEES_NR * 8], 0); // make large enough to prevent resize, since that's currently a bit wobbly
	}

	@Override
	public Object
	compute_finish()
	{
		return results;
	}
}
