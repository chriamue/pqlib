/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.bonus;

import bench.RuntimeCreatorBench;
import benchmarks.BenchmarkOptions;
import benchmarks.Evaluator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import rc2.RCInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class RuntimePQL2 extends Evaluator implements TableConstants, VarConstants
{

        protected Env env;
        protected Join j;
        protected static RCInterface rc = null;

	@Override
	public void
	init()
	{
            if (BenchmarkOptions.oneTimeInit && rc != null) {
                RuntimeCreatorBench.measureSteps[0] = 0;
                RuntimeCreatorBench.measureSteps[1] = 0;
                RuntimeCreatorBench.measureSteps[2] = 0;
                RuntimeCreatorBench.measureSteps[3] = 0;
                return;
            }
            RuntimeCreatorBench.measureSteps[0] = System.nanoTime();
            env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[6],
                        new Object[9]
                });

            env.v_double[2] = 0;
            env.v_double[4] = 0;
            env.v_object[0] = benchmarks.bonus.Generator.employees;
            env.v_object[1] = new Department(0);
            env.v_object[2] = new Employee(null);
            env.v_object[3] = new Bonus("", 0);
            env.v_object[4] = new TreeSet();
            env.v_object[5] = new PMap();

            Method method = null;
            for (int i=0; i<RCInterface.class.getMethods().length; i++) {
                if (RCInterface.class.getMethods()[i].getName().contains("sumDouble"))
                    method = RCInterface.class.getMethods()[i];
            }

            try {
                rc = new RCInterface();
            } catch (RuntimeCreatorException e) {
                e.printStackTrace();
            }
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            final int d4r = Env.encodeReadVar(TYPE_DOUBLE, 4);
            final int d4w = Env.encodeWriteVar(TYPE_DOUBLE, 4);
            j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.MAP(TestBase.o2r, TestBase.d0r, TestBase.o5w) },
                PQLFactory.CONTAINS( TestBase.o0r, TestBase.o2w),
                PQLFactory.FIELD(Employee.class, "dept", TestBase.o2r, TestBase.o1w),
                PQLFactory.FIELD(Department.class, "bonus_factor", TestBase.o1r, TestBase.d1w),
                PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, TestBase.d2r, d4w) },
                    PQLFactory.FIELD(Employee.class, "bonusSet", TestBase.o2r, TestBase.o4w),
                    PQLFactory.CONTAINS(TestBase.o4r, TestBase.o3w),
                    PQLFactory.FIELD(Bonus.class, "bonus_base", TestBase.o3r, TestBase.d2w)
                    ),
                PQLFactory.MUL_Double(TestBase.d1r, d4r, TestBase.d0w)
                );

            Map <Integer, Class> inst = new HashMap <Integer, Class> ();
            inst.put(0, PSet.class);
            inst.put(1, Department.class);
            inst.put(2, Employee.class);
            inst.put(3, Bonus.class);
            inst.put(4, TreeSet.class);
            inst.put(5, PMap.class);

            RuntimeCreatorBench.measureSteps[1] = System.nanoTime();

            if (BenchmarkOptions.optimizeStatic) {
                VarSet constantVars = new VarSet();
                constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 0));
                j = Optimizer.selectAccessPathRecursively(Env.EMPTY, constantVars, j, true);
            }
            RuntimeCreatorBench.measureSteps[2] = System.nanoTime();

            try {
                rc.build(j, env, inst, BenchmarkOptions.parallelMode, (BenchmarkOptions.optimizeBytecode ? RCInterface.OPTIMIZE : 0) );
            } catch (Throwable t) {
                t.printStackTrace();
            }
            RuntimeCreatorBench.measureSteps[3] = System.nanoTime();
        }

        public void
	compute()
	{
            try {
                rc.exec(env);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            this.result = env.v_object[5];
	}

	public String
	getName()
	{
		return "runtime_pql_v2";
	}
}