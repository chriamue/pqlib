package benchmarks.bonus;
import edu.umass.bc.RuntimeCreator;
import bench.RuntimeCreatorBench;
import benchmarks.Evaluator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.TableConstants;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RuntimePQL extends Evaluator implements TableConstants
{

        protected Env env;
        protected Join j;
        protected RuntimeCreatorBench rc;

	@Override
	public void
	init()
	{
            env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[6],
                        new Object[9]
                });

            env.v_double[2] = 0;
            env.v_double[4] = 0;
            env.v_double[5] = 0;
            env.v_object[0] = benchmarks.bonus.Generator.employees;
            env.v_object[1] = new Department(0);
            env.v_object[2] = new Employee(null);
            env.v_object[3] = new Bonus("", 0);
            env.v_object[4] = new TreeSet();
            env.v_object[5] = new PMap();

            rc = new RuntimeCreatorBench();
            j = rc.bonus(env);
            RuntimeCreator.clearTypeInformations();
            RuntimeCreator.setTypeInformation(0, env);
            RuntimeCreator.setTypeInformation(1, env);
            //RuntimeCreator.setTypeInformation(2);
            //RuntimeCreator.setTypeInformation(3);
            RuntimeCreator.setTypeInformation(4, env);
            RuntimeCreator.setTypeInformation(5, env);
            RuntimeCreator.init(j, env);
            RuntimeCreator.alreadyInitialized = true;

        }

        public void
	compute()
	{
            rc.compute(j);
            this.result = rc.getEnv().v_object[5];
	}

	public String
	getName()
	{
		return "runtime_pql";
	}
}
