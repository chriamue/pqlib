package benchmarks.bonus;

import java.util.*;
import edu.umass.pql.container.PSet;

import benchmarks.SQL;
import java.sql.*;

public class Generator extends benchmarks.GeneratorBase
{
	// join employee + department tables, compute employee bonuses
	//  emp-table: (emp-id, dept-id, bonus-descr, bonus-value)
	//  dept-table: (dept-id, bonus-factor)
	//  (Sum and multiply boni)

	public static int EMPLOYEES_NR;
	public static final int DEPARTMENTS_NR = 10;
	public static final int AVG_BONI = 3;

	public static Set<Employee> employees;
	public static Employee[] employees_array;
	public static Department[] departments;

	static Random rand = null;
	static int bonusid;

	@Override
	public void init(int size)
	{
		EMPLOYEES_NR = 1;
		long min_expected_employees_nr = ((32768L * 8) * size) / 100;
		// make sure to retain power of two
		while (EMPLOYEES_NR < min_expected_employees_nr) {
			EMPLOYEES_NR <<= 1;
		}

		//EMPLOYEES_NR = getVar("BONUS_SIZE", SMALL_BENCHMARKS? 32 : (32768 * 8));
		bonusid = 0;
		departments = genDepartments();
		employees = genEmployees();
	}

	public static Department[]
	genDepartments()
	{
		Random rand = new Random(42);
		Department[] result = new Department[DEPARTMENTS_NR];
		for (int i = 0; i < result.length; i++) {
			result[i] = new Department(i);
			if (result[i].id != i)
				throw new RuntimeException();
			result[i].bonus_factor = 0.5 + (rand.nextInt(16) * 0.1);
		}
		return result;
	}

	public static Set<Employee>
	genEmployees()
	{
		Random rand = new Random(42);
		Set<Employee> result = new PSet<Employee>();
		employees_array = new Employee[EMPLOYEES_NR];
		for (int i = 0; i < EMPLOYEES_NR; i++) {
			Employee e = new Employee(departments[rand.nextInt(DEPARTMENTS_NR)]);
			employees_array[i] = e;

			if (e.id != i)
				throw new RuntimeException();
			result.add(e);
			int boni = rand.nextInt(AVG_BONI) + rand.nextInt(AVG_BONI + 1);
			while (boni-- > 0) {
				Bonus b = new Bonus("b"+(bonusid++), rand.nextInt(100) * 100);
				e.bonusSet.add(b);
			}
		}
		return result;
	}

	public static void
	install_sql(SQL sql)
	{
		try {
			sql.open("bonus");
			if (sql.isMySQL()) {
				sql.stmt("USE " + SQL.MYSQL_LOGIN + ";"); // use CREATE DATABASE to set this up
			}

			sql.createTable("employees", " eid  INT NOT NULL PRIMARY KEY,  dept  INT NOT NULL");
			PreparedStatement putter;
			putter = sql.insertStatement("employees", "?, ?");
			for (Employee e : employees) {
				putter.setInt(1, e.id);
				putter.setInt(2, e.dept.id);
				putter.addBatch();
			}
			sql.runBatchUpdate(putter);

			sql.createTable("departments", " did  INT NOT NULL PRIMARY KEY,  factor  FLOAT NOT NULL");
			putter = sql.insertStatement("departments", "?, ?");
			for (Department d : departments) {
				putter.setInt(1, d.id);
				putter.setFloat(2, (float)d.bonus_factor);
				putter.addBatch();
			}
			sql.runBatchUpdate(putter);

			sql.createTable("boni", "eid  INT NOT NULL,  descr  VARCHAR(20) NOT NULL,  base FLOAT NOT NULL");
			putter = sql.insertStatement("boni", "?, ?, ?");
			for (Employee e : employees) {
				for (Bonus b : e.bonusSet) {
					putter.setInt(1, e.id);
					putter.setString(2, b.bonus_descr);
					putter.setFloat(3, (float)b.bonus_base);
					putter.addBatch();
				}
			}
			sql.runBatchUpdate(putter);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public static void
	uninstall_sql(SQL sql)
	{
		sql.deleteTable("employees");
		sql.deleteTable("departments");
		sql.deleteTable("boni");
	}
}
