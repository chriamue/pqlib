package benchmarks.bonus;
import benchmarks.Evaluator;
import edu.umass.pql.container.*;
import java.util.*;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		final PMap<Employee, Double> bonus_map = new PMap<Employee, Double>();
		for (Employee employee : Generator.employees) {
			final double dept_bonus_factor = employee.dept.bonus_factor;
			double total_bonus = 0.0;
			for (Bonus b : employee.bonusSet)
				total_bonus += b.bonus_base;
			bonus_map.put(employee, total_bonus * dept_bonus_factor);
		}
		result = bonus_map;
	}

	public String
	getName()
	{
		return "manual";
	}
}
