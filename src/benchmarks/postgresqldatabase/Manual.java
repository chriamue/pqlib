/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.postgresqldatabase;

import benchmarks.Evaluator;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chriamue
 */
public class Manual extends Evaluator {

    @Override
    public void compute() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://" + Generator.host + "/" + Generator.database + "?user=" + Generator.user + "&password=" + Generator.password);
            Statement statement = connection.createStatement();
            StringBuilder sb = new StringBuilder();
            sb.append("select \"");
            sb.append(Generator.column);
            sb.append("\" from ");
            sb.append(Generator.table);
            ResultSet resultSet = statement.executeQuery(sb.toString());

            while (resultSet.next()) {
                String value = resultSet.getString(Generator.column);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
