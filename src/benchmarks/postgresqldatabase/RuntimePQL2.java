/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.postgresqldatabase;

import benchmarks.Evaluator;
import database.DatabaseExtension;
import rc2.RCInterface;
import rc2.representation.PQLExtension;

/**
 *
 * @author chriamue
 */
public class RuntimePQL2 extends Evaluator {

    @Override
    public void compute() {
        try {
            RCInterface rc = new RCInterface(new DatabaseExtension());

            rc.build(new PQLExtension("mysql", Generator.i1, Generator.i2,
                    Generator.i3, Generator.i4, Generator.i5, Generator.i6, Generator.i7), Generator.env, null, RCInterface.ParallelMode.Serial);

            rc.exec(Generator.env);

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
