/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.postgresqldatabase;

import benchmarks.SQL;
import database.DatabaseExtension;
import edu.umass.pql.Env;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.util.logging.Level;
import java.util.logging.Logger;
import rc2.bcr.BytecodeCreatorRoutine;

/**
 *
 * @author chriamue
 */
public class Generator extends benchmarks.GeneratorBase {

    public static final String databasetype = "mysql";
    public static final String host = "localhost";
    public static final String database = "uni";
    public static final String user = "uni";
    public static final String password = "uni";
    public static final String table = "countries";
    public static final String column = "countryName";

    public static final Object o0 = databasetype;
    public static final Object o1 = host;
    public static final Object o2 = database;
    public static final Object o3 = user;
    public static final Object o4 = password;
    public static final Object o5 = table;
    public static final Object o6 = column;

    public static Env env = new Env(OBJECT_TABLE, new Object[]{new Object[10]});

    public static final int i0 = Env.encodeReadVar(TYPE_OBJECT, 0);
    public static final int i1 = Env.encodeReadVar(TYPE_OBJECT, 1);
    public static final int i2 = Env.encodeReadVar(TYPE_OBJECT, 2);
    public static final int i3 = Env.encodeReadVar(TYPE_OBJECT, 3);
    public static final int i4 = Env.encodeReadVar(TYPE_OBJECT, 4);
    public static final int i5 = Env.encodeReadVar(TYPE_OBJECT, 5);
    public static final int i6 = Env.encodeReadVar(TYPE_OBJECT, 6);
    public static final int i7 = Env.encodeWriteVar(TYPE_OBJECT, 7);

    @Override
    public void init(int i) {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            benchmarks.bonus.Generator.install_sql(SQL.POSTGRESQL);
        } catch (Exception e) {

        }

        env.setObject(i0, o0);
        env.setObject(i1, o1);
        env.setObject(i2, o2);
        env.setObject(i3, o3);
        env.setObject(i4, o4);
        env.setObject(i5, o5);
        env.setObject(i6, o6);
    }

}
