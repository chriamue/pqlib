package benchmarks.threegrep;
import edu.umass.bc.RuntimeCreator;
import bench.RuntimeCreatorBench;
import benchmarks.Evaluator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.TableConstants;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.*;
import java.util.*;

public class RuntimePQL extends Evaluator implements TableConstants
{

        protected Join j;
        protected RuntimeCreatorBench rc;

	@Override
	public void
	init()
	{
            Env env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[3],
                        new Object[6]
                });
            rc = new RuntimeCreatorBench();
            j = rc.threegrep(env);
            RuntimeCreator.clearTypeInformations();
            RuntimeCreator.setTypeInformation(1, env);
            RuntimeCreator.setTypeInformation(2, env);
            RuntimeCreator.setTypeInformation(4, env);
            RuntimeCreator.init(j, env);
            RuntimeCreator.alreadyInitialized = true;
        }

        public void
	compute()
	{
            rc.compute(j);
            this.result = rc.getEnv().v_object[4];
	}

	public String
	getName()
	{
		return "runtime_pql";
	}
}
