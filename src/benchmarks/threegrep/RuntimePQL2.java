/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.threegrep;

import bench.RuntimeCreatorBench;
import benchmarks.BenchmarkOptions;
import benchmarks.Evaluator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.util.HashMap;
import java.util.Map;
import rc2.RCInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class RuntimePQL2 extends Evaluator implements TableConstants, VarConstants {

    protected Env env;
    protected Join j;
    protected static RCInterface rc = null;

    @Override
    public void
    init()
    {
        if (BenchmarkOptions.oneTimeInit && rc != null) {
            RuntimeCreatorBench.measureSteps[0] = 0;
            RuntimeCreatorBench.measureSteps[1] = 0;
            RuntimeCreatorBench.measureSteps[2] = 0;
            RuntimeCreatorBench.measureSteps[3] = 0;
            return;
        }

        RuntimeCreatorBench.measureSteps[0] = System.nanoTime();
        env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[3],
                        new Object[6]
                });

        env.v_object[1] = new byte [1];
        env.v_object[2] = Generator.data_array;
        env.v_object[4] = new PSet ();
        env.v_int[0] = 48;
        env.v_int[2] = 49;
        env.v_int[4] = 50;
        env.v_int[1] = 1;
        env.v_int[3] = 2;
        env.v_int[12] = 0;
        env.v_int[13] = Generator.RECORD_SIZE - 3;

        try {
            rc = new RCInterface();
        } catch (RuntimeCreatorException e) {
            e.printStackTrace();
        }

        final int i12r = Env.encodeReadVar(TYPE_INT, 12);
        final int i13r = Env.encodeReadVar(TYPE_INT, 13);
        final int i8r = Env.encodeReadVar(TYPE_INT, 8);
        final int i8w = Env.encodeWriteVar(TYPE_INT, 8);
        final int i10r = Env.encodeReadVar(TYPE_INT, 10);
        final int i10w = Env.encodeWriteVar(TYPE_INT, 10);

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(TestBase.o1r, TestBase.o4w)},
                PQLFactory.ARRAY_LOOKUP_Object( TestBase.o2r, __, TestBase.o1w),
                PQLFactory.ARRAY_LOOKUP_Byte(TestBase.o1r, TestBase.i6w, (TestBase.i0r ) ),
                PQLFactory.INT_RANGE_CONTAINS( (i12r ), (i13r ), TestBase.i6r),
                PQLFactory.ADD_Int(TestBase.i6r, (TestBase.i1r ), i8w),
                PQLFactory.ARRAY_LOOKUP_Byte(TestBase.o1r, i8r, (TestBase.i2r)),
                PQLFactory.ADD_Int(TestBase.i6r, (TestBase.i3r ), i10w),
                PQLFactory.ARRAY_LOOKUP_Byte(TestBase.o1r, i10r, (TestBase.i4r ))
                );

        Map <Integer, Class> inst = new HashMap <Integer, Class> ();
        inst.put(1, byte[].class);
        inst.put(2, Object[].class);
        inst.put(4, PSet.class);

        RuntimeCreatorBench.measureSteps[1] = System.nanoTime();

        if (BenchmarkOptions.optimizeStatic) {
            VarSet constantVars = new VarSet();
            constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 2));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 0));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 1));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 2));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 3));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 4));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 12));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 13));
            j = Optimizer.selectAccessPathRecursively(Env.EMPTY, constantVars, j, true);
        }

        RuntimeCreatorBench.measureSteps[2] = System.nanoTime();

        try {
            rc.build(j, env, inst, BenchmarkOptions.parallelMode);// (BenchmarkOptions.optimizeBytecode ? RCInterface.OPTIMIZE : 0));
        } catch (Throwable t) {
            t.printStackTrace();
        }
        RuntimeCreatorBench.measureSteps[3] = System.nanoTime();
    }

    public void
    compute()
    {
        try {
            rc.exec(env);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        this.result = env.v_object[4];
    }

    public String
    getName()
    {
            return "runtime_pql_v2";
    }

}
