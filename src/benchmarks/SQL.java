package benchmarks;

import java.sql.*;
import java.util.Properties;

public class SQL
{
	public SQL(String db, int port, String user, String password)
	{
		this.db = db;
		this.user = user;
		this.port_nr = port;
		this.password = password;
	}

	public boolean
	isMySQL()
	{
		return this.db.equals("mysql");
	}

	public boolean
	isPostgreSQL()
	{
		return this.db.equals("postgresql");
	}

	private String db, user, password, dbname;
	int port_nr;
	public Connection connection = null;

	public String
	getDriverName()
	{
		return this.db;
	}

	public String
	getDBName()
	{
		return this.dbname;
	}

	public String
	tableName(String s)
	{
		return this.getDBName() + "_" + s;
	}

	public void
	open(String dbname)
	{
		this.dbname = dbname;
		this.connection = getConnection(this.db, this.user, this.password, this.port_nr);
	}

	public void
	close()
	{
		try {
			this.connection.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			this.connection = null;
		}
	}

	public void
	stmt(String update)
	{
		Statement stmt = null;
		try {
			stmt = this.connection.createStatement();
			dp(update);
			stmt.executeUpdate(update);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (stmt != null) { stmt.close(); }
			} catch (SQLException e) {
			}
		}
	}

	public void
	createTable(String tablename, String columns_descr)
	{
		Statement stmt = null;
		try {
			stmt = this.connection.createStatement();
			String update = "create table " + this.dbname + "_" + tablename + " (" + columns_descr + ");";
			dp(update);
			stmt.executeUpdate(update);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (stmt != null) { stmt.close(); }
			} catch (SQLException e) {
			}
		}
	}

	public void
	deleteTable(String tablename)
	{
		Statement stmt = null;
		try {
			stmt = this.connection.createStatement();
			String update = "drop table " + this.dbname + "_" + tablename + ";";
			dp(update);
			stmt.executeUpdate(update);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (stmt != null) { stmt.close(); }
			} catch (SQLException e) {
			}
		}
	}

	public static final boolean DEBUG = false;
	public static final boolean DEBUG_INSERT = false;

	static final
	String
	dp(String s)
	{
		if (DEBUG)
			dpp(s);
		return s;
	}

	static final
	PreparedStatement
	dpi(PreparedStatement s)
	{
		if (DEBUG_INSERT)
			dpp(s.toString());
		return s;
	}

	static final
	PreparedStatement
	dp(PreparedStatement s)
	{
		if (DEBUG)
			dpp(s.toString());
		return s;
	}

	static final
	void
	dpp(String s)
	{
		System.err.println("[> " + s);
	}


	public PreparedStatement
	prepareQuery(String query)
	{
		PreparedStatement stmt = null;
		try {
			stmt = this.connection.prepareStatement(dp(query));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return stmt;
	}

	public PreparedStatement
	prepareQuery(String tablename, String query, String constraints)
	{
		return prepareQuery("select (" + query + ") from " + this.dbname + "_" + tablename + " " + constraints + ";");
	}

	public PreparedStatement
	insertStatement(String tablename, String query)
	{
		PreparedStatement stmt = null;
		try {
			stmt = this.connection.prepareStatement(dp("insert into " + this.dbname + "_" + tablename + " values (" + query + ");"));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return stmt;
	}

	public ResultSet
	runQuery(PreparedStatement stmt)
	{
		try {
			return stmt.executeQuery();
		} catch (SQLException e) {
			throw new RuntimeException (e);
		}
	}

	public void
	runUpdate(PreparedStatement stmt)
	{
		try {
			dpi(stmt);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException (e);
		}
	}

	public void
	runBatchUpdate(PreparedStatement stmt)
	{
		try {
			dpi(stmt);
			stmt.executeBatch();
		} catch (SQLException e) {
			throw new RuntimeException (e);
		}
	}

	protected Connection
	getConnection(String database, String user, String password, int port_nr)
	{
		try {
			Connection conn = null;
			Properties connectionProps = new Properties();
			connectionProps.put("user", user);
			connectionProps.put("password", password);

			conn = DriverManager.
				getConnection("jdbc:" + database + "://" + "localhost" +
					      ":" + port_nr + "/", connectionProps);
			return conn;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to connect to `" + database + ":" + port_nr + "' as `" + user + "'");
			throw new RuntimeException(e);
		}
	}

	public static final int MYSQL_PORT = 3306;
	public static final String MYSQL_LOGIN = "test";
	public static final String MYSQL_PASSWORD = "test";

	public static final int POSTGRES_PORT = 5432;
	public static final String POSTGRES_LOGIN = "test";
	public static final String POSTGRES_PASSWORD = "test";

	public static SQL MYSQL = new SQL("mysql", MYSQL_PORT, MYSQL_LOGIN, MYSQL_PASSWORD);
	public static SQL POSTGRESQL = getPostgresql();

	static SQL
	getPostgresql()
	{
		try {
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return new SQL("postgresql", POSTGRES_PORT, POSTGRES_LOGIN, POSTGRES_PASSWORD);
	}
}