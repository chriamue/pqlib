package benchmarks.webgraph;
import benchmarks.Evaluator;

import edu.umass.pql.container.PSet;
import java.util.*;

public class ParaManual extends Evaluator.ParaEvaluator
{
	Set<Webdoc> results;

	class EvaluatorThread extends Thread
	{
		public EvaluatorThread(int start, int stop)
		{
			this.start = start;
			this.stop = stop;
		}
		int start, stop;

		public void run()
		{
			for (int i = start; i < stop; i++) {
				Webdoc doc = Generator.documents_array[i];
				if (results.contains(doc))
					continue;

				for (Link l : doc.outlinks) {
					for (Link l2 : l.destination.outlinks)
						if (l2.destination == doc) {
							results.add(doc);
							results.add(l.destination);
						}
				}
			}
		}
	}

	@Override
	public Thread
	gen_thread(int __, int start, int stop)
	{
		return new EvaluatorThread(start, stop);
	}

	@Override
	public int
	get_size()
	{
		return Generator.documents_array.length;
	}

	@Override
	public void
	compute_prepare()
	{
		results = new PSet<Webdoc>(new Object[0x20000 * 2], 0); // make large enough to prevent resize, since that's currently a bit wobbly
	}

	@Override
	public Object
	compute_finish()
	{
		return results;
	}
}
