package benchmarks.webgraph;
import benchmarks.Evaluator;
import edu.umass.pql.container.*;
import java.util.*;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		PSet<Webdoc> results = new PSet<Webdoc>();

		for (Webdoc doc : Generator.documents) {
			if (results.contains(doc))
				continue;

			for (Link l : doc.outlinks) {
				for (Link l2 : l.destination.outlinks)
					if (l2.destination == doc) {
						results.add(doc);
						results.add(l.destination);
					}
			}
		}
		result = results;
	}

	public String
	getName()
	{
		return "manual";
	}
}
