/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.webgraph;

import bench.RuntimeCreatorBench;
import benchmarks.BenchmarkOptions;
import benchmarks.Evaluator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import rc2.RCInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class RuntimePQL2 extends Evaluator implements TableConstants, VarConstants {

    protected Env env;
    protected Join j;
    protected static RCInterface rc = null;

    @Override
    public void
    init()
    {
        if (BenchmarkOptions.oneTimeInit && rc != null) {
            RuntimeCreatorBench.measureSteps[0] = 0;
            RuntimeCreatorBench.measureSteps[1] = 0;
            RuntimeCreatorBench.measureSteps[2] = 0;
            RuntimeCreatorBench.measureSteps[3] = 0;
            return;
        }
        RuntimeCreatorBench.measureSteps[0] = System.nanoTime();
        env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[5],
                        new Object[9]
                });

        env.v_object[0] = new Webdoc (0);
        env.v_object[1] = benchmarks.webgraph.Generator.documents;
        env.v_object[2] = new Link (new Webdoc (0), new Webdoc (0));
        env.v_object[3] = new HashSet();
        env.v_object[4] = new Link (new Webdoc (0), new Webdoc (0));
        env.v_object[5] = new Webdoc (0);
        env.v_object[6] = new HashSet ();
        env.v_object[8] = new PSet();

        try {
            rc = new RCInterface();
        } catch (RuntimeCreatorException e) {
            e.printStackTrace();
        }

        final int o6r = Env.encodeReadVar(TYPE_OBJECT, 6);
        final int o6w = Env.encodeWriteVar(TYPE_OBJECT, 6);
        final int o8w = Env.encodeWriteVar(TYPE_OBJECT, 8);

        PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
        j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(TestBase.o0r, o8w)},
                PQLFactory.ConjunctiveBlock( PQLFactory.CONTAINS( (TestBase.o1r ), TestBase.o0w),
                PQLFactory.FIELD(Webdoc.class, "outlinks", TestBase.o0r, TestBase.o3w),
                PQLFactory.CONTAINS(TestBase.o3r, TestBase.o2w),
                PQLFactory.FIELD(Link.class, "destination", TestBase.o2r, TestBase.o5w),
                PQLFactory.FIELD(Webdoc.class, "outlinks", TestBase.o5r, o6w),
                PQLFactory.CONTAINS(o6r, TestBase.o4w),
                PQLFactory.FIELD(Link.class, "destination", TestBase.o4r, TestBase.o0r))
                );

        Map <Integer, Class> inst = new HashMap <Integer, Class> ();
        inst.put(1, PSet.class);
        inst.put(3, HashSet.class);
        inst.put(5, Webdoc.class);
        inst.put(6, HashSet.class);
        inst.put(8, PSet.class);

        inst.put(0, Webdoc.class);
        inst.put(2, Link.class);
        inst.put(4, Link.class);

        RuntimeCreatorBench.measureSteps[1] = System.nanoTime();

        if (BenchmarkOptions.optimizeStatic) {
            VarSet constantVars = new VarSet();
            constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 1));
            j = Optimizer.selectAccessPathRecursively(Env.EMPTY, constantVars, j, true);
        }

        RuntimeCreatorBench.measureSteps[2] = System.nanoTime();

        try {
            rc.build(j, env, inst, BenchmarkOptions.parallelMode);//, (BenchmarkOptions.optimizeBytecode ? RCInterface.OPTIMIZE : 0));
        } catch (Throwable t) {
            t.printStackTrace();
        }

        RuntimeCreatorBench.measureSteps[3] = System.nanoTime();
    }

    public void
    compute()
    {
        try {
            rc.exec(env);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        this.result = env.v_object[8];
    }

    public String
    getName()
    {
            return "runtime_pql_v2";
    }

}
