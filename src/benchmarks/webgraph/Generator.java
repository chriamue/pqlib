package benchmarks.webgraph;

import java.util.*;
import edu.umass.pql.container.PSet;

import benchmarks.SQL;
import java.sql.*;

public class Generator extends benchmarks.GeneratorBase
{
	public static int WEBDOCS_NR;
	public static final int WORDS_NR = 1000; // total number of words in the language.  Lower-id words are more frequent than higher-id ones.
	public static int AVG_WORDS_PER_DOC;
	public static int MAX_LINKS;
	public static final int MEAN_LINKS = 5;

	public static Webdoc[] documents_array;
	public static Set<Webdoc> documents;

	static Random rand;

	@Override
	public void init(int size)
	{
		//WEBDOCS_NR = getVar("WEB_SIZE", SMALL_BENCHMARKS ? 10 : (40000 * 1));
                WEBDOCS_NR = getVar("WEB_SIZE", SMALL_BENCHMARKS ? 10 : (4 * size * size));
		AVG_WORDS_PER_DOC = SMALL_BENCHMARKS ? 20 : (20 * size);
		MAX_LINKS = SMALL_BENCHMARKS ? 50 : (3 * size);
		documents_array = new Webdoc[WEBDOCS_NR];
		rand = new Random(42);
		documents = genDocuments();
		genLinks();
	}

	public static final int
	nextWord()
	{
		double v = rand.nextGaussian() * WORDS_NR / 4.0;
		if (v < 0) v = -v;
		int word_nr = (int) v;
		if (v >= WORDS_NR)
			v = WORDS_NR - 1;
		return (int) v;
	}

	public static Set<Webdoc>
	genDocuments()
	{
		Set<Webdoc> result = new PSet<Webdoc>();
		for (int i = 0; i < WEBDOCS_NR; i++) {
			Webdoc doc = new Webdoc(i);
			final int len = rand.nextInt(AVG_WORDS_PER_DOC) + rand.nextInt(AVG_WORDS_PER_DOC + 1);
			doc.words = new int[len];
			for (int k = 0; k < len; k++)
				doc.words[k] = nextWord();
			result.add(doc);
			documents_array[i] = doc;
		}
		return result;
	}

	public static void
	genLinks()
	{
		for (int i = 0; i < WEBDOCS_NR; i++) {
			Webdoc doc = documents_array[i];
			if (doc == null)
				throw new NullPointerException();
			int links_nr = (int) ((rand.nextGaussian() * MAX_LINKS / 5.0) + MEAN_LINKS);
			if (links_nr < 0)
				links_nr = -links_nr;
			if (links_nr >= MAX_LINKS)
				links_nr = MAX_LINKS - 1;

			for (int l = 0; l < links_nr; l++)
				new Link(doc, documents_array[rand.nextInt(WEBDOCS_NR)]);
		}
	}

	public static void
	install_sql(SQL sql)
	{
		try {
			sql.open("web");
			if (sql.isMySQL()) {
				sql.stmt("USE " + SQL.MYSQL_LOGIN + ";"); // use CREATE DATABASE to set this up
				sql.stmt("set global innodb_thread_concurrency = 1;");
			}

			sql.createTable("docs", " id  INT NOT NULL PRIMARY KEY");
			PreparedStatement doc_putter = sql.insertStatement("docs", "?");

			sql.createTable("words", " docid  INT NOT NULL,  word_offset  INT NOT NULL,  word  INT NOT NULL");
			PreparedStatement words_putter = sql.insertStatement("words", "?, ?, ?");

			sql.createTable("links", " lid  INT NOT NULL PRIMARY KEY,  source  INT NOT NULL,  destination  INT NOT NULL");
			PreparedStatement link_putter = sql.insertStatement("links", "?, ?, ?");
			for (int i = 0; i < WEBDOCS_NR; i++) {
				Webdoc doc = documents_array[i];
				doc_putter.setInt(1, doc.id);
				if (i != doc.id)
					throw new RuntimeException();
				doc_putter.addBatch();

				for (int k = 0; k < doc.words.length; k++) {
					words_putter.setInt(1, doc.id);
					words_putter.setInt(2, k);
					words_putter.setInt(3, doc.words[k]);
					words_putter.addBatch();
				}
				if (i + 1 == WEBDOCS_NR || (i & 0x3f) == 0x3f)
					sql.runBatchUpdate(words_putter);

				for (Link l : doc.outlinks) {
					link_putter.setInt(1, l.linkid);
					link_putter.setInt(2, l.source.id);
					link_putter.setInt(3, l.destination.id);
					link_putter.addBatch();
				}
				if (i + 1 == WEBDOCS_NR || (i & 0x7f) == 0x7f)
					sql.runBatchUpdate(link_putter);
			}
			sql.runBatchUpdate(doc_putter);

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public static void
	uninstall_sql(SQL sql)
	{
		sql.deleteTable("docs");
		sql.deleteTable("links");
		sql.deleteTable("words");
	}
}
