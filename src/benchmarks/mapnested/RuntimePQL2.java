/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarks.mapnested;

import bench.RuntimeCreatorBench;
import static bench.RuntimeCreatorBench.generatedMap;
import benchmarks.BenchmarkOptions;
import benchmarks.Evaluator;
import benchmarks.bonus.Bonus;
import benchmarks.bonus.Department;
import benchmarks.bonus.Employee;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import edu.umass.pql.TestBase;
import static edu.umass.pql.TestBase.i0r;
import static edu.umass.pql.TestBase.i1r;
import static edu.umass.pql.TestBase.i1w;
import static edu.umass.pql.TestBase.i2r;
import static edu.umass.pql.TestBase.i2w;
import static edu.umass.pql.TestBase.i3r;
import static edu.umass.pql.TestBase.i3w;
import static edu.umass.pql.TestBase.i4r;
import static edu.umass.pql.TestBase.i5r;
import static edu.umass.pql.TestBase.i6r;
import static edu.umass.pql.TestBase.o0r;
import static edu.umass.pql.TestBase.o1r;
import static edu.umass.pql.TestBase.o2w;
import edu.umass.pql.VarConstants;
import static edu.umass.pql.VarConstants.TYPE_DOUBLE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import static edu.umass.pql.VarConstants.VAR_CONST_FLAG;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import rc2.RCInterface;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class RuntimePQL2 extends Evaluator implements TableConstants, VarConstants
{

        protected Env env;
        protected Join j;
        protected static RCInterface rc = null;

	@Override
	public void
	init()
	{
            if (BenchmarkOptions.oneTimeInit && rc != null) {
                RuntimeCreatorBench.measureSteps[0] = 0;
                RuntimeCreatorBench.measureSteps[1] = 0;
                RuntimeCreatorBench.measureSteps[2] = 0;
                RuntimeCreatorBench.measureSteps[3] = 0;
                return;
            }
            RuntimeCreatorBench.measureSteps[0] = System.nanoTime();
            env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[5],
                        new Object[9]
                });

            env.v_object[0] = generatedMap[0];
            env.v_object[1] = generatedMap[1];
            env.v_int[0] = 10;
            env.v_int[2] = 0;
            env.v_int[3] = 0;
            env.v_int[4] = 9;
            env.v_int[5] = 999;
            env.v_int[6] = 0;

            try {
            rc = new RCInterface();
        } catch (RuntimeCreatorException e) {
            e.printStackTrace();
        }
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
            j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.SET(i1r, o2w)},
                PQLFactory.ConjunctiveBlock(PQLFactory.INT_RANGE_CONTAINS((i6r), (i5r), i2w),
                PQLFactory.LOOKUP( o0r, i2r, i1w),
                PQLFactory.LT_Int(i1r, (i0r) ),
                PQLFactory.INT_RANGE_CONTAINS((i6r), (i4r), i3w),
                PQLFactory.LOOKUP(o1r, i3r, i1r))
                );

            Map <Integer, Class> inst = new HashMap <Integer, Class> ();
            inst.put(0, PMap.class);
            inst.put(1, PMap.class);

            RuntimeCreatorBench.measureSteps[1] = System.nanoTime();

            if (BenchmarkOptions.optimizeDynamic) {
                VarSet constantVars = new VarSet();
                constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 0));
                constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 1));
                constantVars.insert(Env.encodeReadVar(TYPE_INT, 0));
                constantVars.insert(Env.encodeReadVar(TYPE_INT, 4));
                constantVars.insert(Env.encodeReadVar(TYPE_INT, 5));
                constantVars.insert(Env.encodeReadVar(TYPE_INT, 6));
                j = Optimizer.selectAccessPathRecursively(env,constantVars,j,true);
            }

            RuntimeCreatorBench.measureSteps[2] = System.nanoTime();

            try {
                rc.build(j, env, inst, BenchmarkOptions.parallelMode);
            } catch (Throwable t) {
                t.printStackTrace();
            }

            RuntimeCreatorBench.measureSteps[3] = System.nanoTime();
        }

        public void
	compute()
	{
            try {
                rc.exec(env);
            } catch (Throwable t) {
                t.printStackTrace();
            }
            this.result = env.v_object[2];
	}

	public String
	getName()
	{
		return "runtime_pql_v2";
	}
}