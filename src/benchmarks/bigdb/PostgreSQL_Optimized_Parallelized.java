/**
 * *************************************************************************
 * Copyright (C) 2014 chriamue
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public Licence as published by the Free Software
 * Foundaton; either version 2 of the Licence, or (at your option) any later
 * version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of merchantability or fitness for
 * a particular purpose. See the GNU General Public Licence for more details.
 *
 * You should have received a copy of the GNU General Public Licence along with
 * this program; see the file COPYING. If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 **************************************************************************
 */
package benchmarks.bigdb;

import benchmarks.Evaluator;
import benchmarks.SQL;
import database.DatabaseExtension;
import database.FieldCompRule;
import database.SelectFromRule;
import database.WhereClauseRule;
import database.tests.MysqlTest;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_DOUBLE;
import static edu.umass.pql.VarConstants.TYPE_INT;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import edu.umass.pql.VarInfo;
import edu.umass.pql.VarSet;
import edu.umass.pql.opt.Optimizer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertTrue;
import rc2.RCInterface;
import rc2.representation.PQLExtension;
import rc2.representation.RuntimeCreatorException;
import replacetool.AbstractRule;
import replacetool.ReplaceTool;

/**
 *
 * @author chriamue
 */
public class PostgreSQL_Optimized_Parallelized extends Evaluator {

    @Override
    public void compute() {
        try {
            RCInterface rc = null;
            Env env;

            final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 21);
            final int iHost = Env.encodeReadVar(TYPE_OBJECT, 22);
            final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 23);
            final int iUser = Env.encodeReadVar(TYPE_OBJECT, 24);
            final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 25);
            final int iTable1 = Env.encodeReadVar(TYPE_OBJECT, 26);
            final int iTable2 = Env.encodeReadVar(TYPE_OBJECT, 27);
            final int iTable3 = Env.encodeReadVar(TYPE_OBJECT, 28);
            final int iColumns1 = Env.encodeReadVar(TYPE_OBJECT, 29);
            final int iColumns2 = Env.encodeReadVar(TYPE_OBJECT, 30);
            final int iColumns3 = Env.encodeReadVar(TYPE_OBJECT, 31);
            final int iWhereClause1 = Env.encodeReadVar(TYPE_OBJECT, 32);
            final int iWhereClause2 = Env.encodeReadVar(TYPE_OBJECT, 33);
            final int iWhereClause3 = Env.encodeReadVar(TYPE_OBJECT, 34);
            final int iValue = Env.encodeReadVar(TYPE_OBJECT, 35);
            final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 36);
            final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 37);
            final int iColumn3 = Env.encodeReadVar(TYPE_OBJECT, 38);
            final int iColumn4 = Env.encodeReadVar(TYPE_OBJECT, 39);
            final int iColumn5 = Env.encodeReadVar(TYPE_OBJECT, 40);
            final int iValue2 = Env.encodeReadVar(TYPE_INT, 41);

            VarSet constVars;
            VarInfo varInfo;

            Map<Integer, Class> inst = new HashMap<Integer, Class>();

            /////////////////
            try {
                rc = new RCInterface(new DatabaseExtension());

            } catch (RuntimeCreatorException ex) {
                Logger.getLogger(MysqlTest.class.getName()).log(Level.SEVERE, null, ex);
            }

            env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                    new Object[]{
                        new int[100], new long[100], new double[100], new Object[100]
                    });

            constVars = new VarSet();
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 21));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 22));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 23));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 24));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 25));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 26));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 27));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 28));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 29));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 30));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 31));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 32));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 33));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 34));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 35));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 36));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 37));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 38));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 39));
            constVars.insert(Env.encodeReadVar(TYPE_OBJECT, 40));
            constVars.insert(Env.encodeReadVar(TYPE_INT, 40));
            varInfo = new VarInfo(constVars);

            env.setObject(iDatabasetype, "postgresql");
            env.setObject(iHost, "localhost");
            env.setObject(iDatabase, "test");
            env.setObject(iUser, "test");
            env.setObject(iPassword, "test");
            env.setObject(iTable1, "bonus_employees");
            env.setObject(iTable2, "bonus_departments");
            env.setObject(iTable3, "bonus_boni");

            env.setObject(iColumns1, "");
            env.setObject(iColumns2, "");
            env.setObject(iColumns3, "");
            env.setObject(iWhereClause1, "");
            env.setObject(iWhereClause2, "");
            env.setObject(iWhereClause3, "");
            env.setObject(iColumn1, "eid");
            env.setObject(iColumn2, "dept");
            env.setObject(iColumn3, "did");
            env.setObject(iColumn4, "factor");
            env.setObject(iColumn5, "base");
            env.setObject(iValue, "b7");
            env.setObject(iValue2, 10);

            inst.put(21, String.class);
            inst.put(22, String.class);
            inst.put(23, String.class);
            inst.put(24, String.class);
            inst.put(25, String.class);
            inst.put(26, String.class);
            inst.put(27, String.class);
            inst.put(28, String.class);
            inst.put(29, String.class);
            inst.put(30, String.class);
            inst.put(31, String.class);
            inst.put(32, String.class);
            inst.put(33, String.class);
            inst.put(34, String.class);
            inst.put(35, String.class);
            inst.put(36, String.class);
            inst.put(37, String.class);
            inst.put(38, String.class);
            inst.put(39, String.class);
            inst.put(40, String.class);
            inst.put(41, int.class);

            final int i1 = Env.encodeVar(TYPE_OBJECT, 11);
            final int i2 = Env.encodeVar(TYPE_OBJECT, 12);
            final int i3 = Env.encodeVar(TYPE_OBJECT, 13);
            final int i4 = Env.encodeVar(TYPE_INT, 14);
            final int i5 = Env.encodeVar(TYPE_INT, 15);
            final int i6 = Env.encodeVar(TYPE_DOUBLE, 16);
            final int i7 = Env.encodeVar(TYPE_INT, 17);
            final int i8 = Env.encodeVar(TYPE_INT, 18);
            final int i9 = Env.encodeVar(TYPE_INT, 19);
            final int i10 = Env.encodeVar(TYPE_DOUBLE, 20);

            final int bonus = Env.encodeVar(TYPE_OBJECT, 51);

            Join j1 = PQLFactory.Reduction(new Reductor[]{PQLFactory.Reductors.SET(Env.readVar(i10), bonus)},
                    new PQLExtension("postgresql", iHost, iDatabase, iUser, iPassword, iTable3, iColumns1, iWhereClause1, i1),
                    new PQLExtension("field_int", i1, iColumn1, i4),
                    new PQLExtension("like_int", i4, iValue2),
                    //PQLFactory.EQ_Int(i4, iValue2),
                    new PQLExtension("field_double", i1, iColumn5, i10)
            );

            Set<AbstractRule> rules = new HashSet();
            rules.add(new SelectFromRule());
            rules.add(new FieldCompRule());
            rules.add(new WhereClauseRule());

            ReplaceTool rt = new ReplaceTool();
            j1 = Optimizer.selectAccessPathRecursively(env, constVars, j1, true);
            ReplaceTool.ReplaceResult res = rt.replace(j1, env, rules);

            env = res.getEnv();
            rc.build(res.getJoin(), env, inst, RCInterface.ParallelMode.Parallel, RCInterface.DYNAMIC_OPTIMIZE);

            assertTrue(rc.exec(env));
        } catch (Throwable ex) {
            Logger.getLogger(PostgreSQL_Optimized_Parallelized.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
