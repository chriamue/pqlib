/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.bigdb;

import benchmarks.mysqldatabase.*;
import benchmarks.SQL;
import database.DatabaseExtension;
import edu.umass.pql.Env;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.util.logging.Level;
import java.util.logging.Logger;
import rc2.bcr.BytecodeCreatorRoutine;

/**
 *
 * @author chriamue
 */
public class Generator extends benchmarks.GeneratorBase {

    @Override
    public void init(int i) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        }

        benchmarks.bonus.Generator g = new benchmarks.bonus.Generator();
        g.init(i);

        g.install_sql(SQL.MYSQL);
        //g.install_sql(SQL.POSTGRESQL);
    }

}
