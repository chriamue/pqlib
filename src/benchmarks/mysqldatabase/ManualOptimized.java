/***************************************************************************
 Copyright (C) 2014 chriamue

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/
package benchmarks.mysqldatabase;

import benchmarks.Evaluator;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author chriamue
 */
public class ManualOptimized extends Evaluator {

    @Override
    public void compute() {
        try {
            Set<Integer> employees = new HashSet<Integer>();
            Map<Integer, Integer> employeesDepartment = new HashMap<Integer, Integer>();
            Map<Integer, Float> departmentsFactor = new HashMap<Integer, Float>();
            Map<Integer, Float> boniBase = new HashMap<Integer, Float>();
            Map<Integer, String> boniDescription = new HashMap<Integer, String>();
            Map<Integer, Float> bonus = new HashMap<Integer, Float>();

            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from bonus_employees");
            while (resultSet.next()) {
                Integer eid = resultSet.getInt("eid");
                Integer dept = resultSet.getInt("dept");
                employeesDepartment.put(eid, dept);
                employees.add(eid);
            }

            resultSet = statement.executeQuery("select * from bonus_departments");
            while (resultSet.next()) {
                Integer did = resultSet.getInt("did");
                Float factor = resultSet.getFloat("factor");
                departmentsFactor.put(did, factor);
            }

            resultSet = statement.executeQuery("select eid,GROUP_CONCAT(descr),SUM(base) from bonus_boni group by eid");
            while (resultSet.next()) {
                Integer eid = resultSet.getInt("eid");
                String descr = resultSet.getString(2);
                String base = resultSet.getString(3);
                boniBase.put(eid, Float.parseFloat(base));
                boniDescription.put(eid, descr);
            }

            for (Integer employee : employees) {
                Float dF = departmentsFactor.get(employeesDepartment.get(employee));
                if (dF == null) {
                    dF = 0f;
                }
                Float bB = boniBase.get(employee);
                if (bB == null) {
                    bB = 0f;
                }
                Float b = dF * bB;
                bonus.put(employee, b);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
