/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.mysqldatabase;

import benchmarks.Evaluator;
import database.DatabaseExtension;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import rc2.RCInterface;
import rc2.representation.PQLExtension;

/**
 *
 * @author chriamue
 */
public class RuntimePQL2 extends Evaluator {

    @Override
    public void compute() {
        try {
            RCInterface rc = new RCInterface(new DatabaseExtension());

            final int r1 = Env.encodeVar(TYPE_OBJECT, 21);
            final int r2 = Env.encodeVar(TYPE_OBJECT, 22);

            Join p = PQLFactory.ConjunctiveBlock(
                    new PQLExtension("mysql", Generator.iHost, Generator.iDatabase, Generator.iUser,
                            Generator.iPassword, Generator.iTable, Generator.iColumns, Generator.iWhereClause, r1),
                    new PQLExtension("field", r1, Generator.iColumn1, r2),
                    new PQLExtension("like", r2, Generator.iValue1),
                    new PQLExtension("field", r1, Generator.iColumn2, Generator.iResult));
            Map<Integer, Class> inst = new HashMap<Integer, Class>();

            inst.put(0, String.class);
            inst.put(1, String.class);
            inst.put(2, String.class);
            inst.put(3, String.class);
            inst.put(4, String.class);
            inst.put(5, String.class);
            inst.put(6, String.class);
            inst.put(7, String.class);
            inst.put(8, String.class);
            inst.put(9, String.class);
            inst.put(10, String.class);

            rc.build(p, Generator.env, inst, RCInterface.ParallelMode.Serial);

            rc.exec(Generator.env);

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
