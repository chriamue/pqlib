/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.mysqldatabase;

import benchmarks.SQL;
import database.DatabaseExtension;
import edu.umass.pql.Env;
import static edu.umass.pql.TableConstants.DOUBLE_TABLE;
import static edu.umass.pql.TableConstants.INT_TABLE;
import static edu.umass.pql.TableConstants.LONG_TABLE;
import static edu.umass.pql.TableConstants.OBJECT_TABLE;
import static edu.umass.pql.VarConstants.TYPE_OBJECT;
import java.util.logging.Level;
import java.util.logging.Logger;
import rc2.bcr.BytecodeCreatorRoutine;

/**
 *
 * @author chriamue
 */
public class Generator extends benchmarks.GeneratorBase {

    public static final String databasetype = "mysql";
    public static final String host = "localhost";
    public static final String database = "uni";
    public static final String user = "uni";
    public static final String password = "uni";
    public static final String table = "countries";
    public static final String column1 = "countryName";
    public static final String column2 = "capital";
    public static final String columns = "countryName,capital";
    public static final String value1 = "Denmark";
    public static final String whereClause = "";

    public static final Object oDatabasetype = databasetype;
    public static final Object oHost = host;
    public static final Object oDatabase = database;
    public static final Object oUser = user;
    public static final Object oPassword = password;
    public static final Object oTable = table;
    public static final Object oColumn1 = column1;
    public static final Object oColumn7 = column2;
    public static final Object oColumns = columns;
    public static final Object oValue1 = value1;
    public static final Object oWhereClause = whereClause;

    public static Env env = new Env(INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
            new Object[]{
                new int[100], new long[100], new double[100], new Object[100]
            });

    public static final int iDatabasetype = Env.encodeReadVar(TYPE_OBJECT, 0);
    public static final int iHost = Env.encodeReadVar(TYPE_OBJECT, 1);
    public static final int iDatabase = Env.encodeReadVar(TYPE_OBJECT, 2);
    public static final int iUser = Env.encodeReadVar(TYPE_OBJECT, 3);
    public static final int iPassword = Env.encodeReadVar(TYPE_OBJECT, 4);
    public static final int iTable = Env.encodeReadVar(TYPE_OBJECT, 5);
    public static final int iColumn1 = Env.encodeReadVar(TYPE_OBJECT, 6);
    public static final int iColumn2 = Env.encodeReadVar(TYPE_OBJECT, 7);
    public static final int iColumns = Env.encodeReadVar(TYPE_OBJECT, 8);
    public static final int iValue1 = Env.encodeReadVar(TYPE_OBJECT, 9);
    public static final int iWhereClause = Env.encodeReadVar(TYPE_OBJECT, 10);

    public static final int iResult = Env.encodeWriteVar(TYPE_OBJECT, 11);

    @Override
    public void init(int i) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            benchmarks.bonus.Generator.install_sql(SQL.MYSQL);
        } catch (Exception e) {

        }
        
        env.setObject(iDatabasetype, oDatabasetype);
        env.setObject(iHost, oHost);
        env.setObject(iDatabase, oDatabase);
        env.setObject(iUser, oUser);
        env.setObject(iPassword, oPassword);
        env.setObject(iTable, oTable);
        env.setObject(iColumn1, oColumn1);
        env.setObject(iColumn2, oColumn7);
        env.setObject(iColumns, oColumns);
        env.setObject(iValue1, oValue1);
        env.setObject(iWhereClause, oWhereClause);
    }

}
