/***************************************************************************
 Copyright (C) 2014 chriamue

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/
package benchmarks.mysqldatabase;

import benchmarks.Evaluator;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chriamue
 */
public class Manual extends Evaluator {

    @Override
    public void compute() {
        try {
            Set<Integer> employees = new HashSet<Integer>();
            Map<Integer, Integer> employeesDepartment = new HashMap<Integer, Integer>();
            Map<Integer, Float> departmentsFactor = new HashMap<Integer, Float>();
            Map<Integer, Set<Float>> boniBase = new HashMap<Integer, Set<Float>>();
            Map<Integer, Set<String>> boniDescription = new HashMap<Integer, Set<String>>();
            Map<Integer, Float> bonus = new HashMap<Integer, Float>();

            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from bonus_employees");
            while (resultSet.next()) {
                Integer eid = resultSet.getInt("eid");
                Integer dept = resultSet.getInt("dept");
                employeesDepartment.put(eid, dept);
                employees.add(eid);
            }

            resultSet = statement.executeQuery("select * from bonus_departments");
            while (resultSet.next()) {
                Integer did = resultSet.getInt("did");
                Float factor = resultSet.getFloat("factor");
                departmentsFactor.put(did, factor);
            }

            resultSet = statement.executeQuery("select * from bonus_boni");
            while (resultSet.next()) {
                Integer eid = resultSet.getInt("eid");
                String descr = resultSet.getString("descr");
                Float base = resultSet.getFloat("base");
                Set<Float> bd = boniBase.get(eid);
                if (bd == null) {
                    bd = new HashSet<Float>();
                }
                bd.add(base);
                boniBase.put(eid, bd);

                Set<String> bB = boniDescription.get(eid);
                if (bB == null) {
                    bB = new HashSet<String>();
                }
                bB.add(descr);
                boniDescription.put(eid, bB);
            }

            for (Integer employee : employees) {
                Float dF = departmentsFactor.get(employeesDepartment.get(employee));
                if (dF == null) {
                    dF = 0f;
                }
                Set<Float> bB = boniBase.get(employee);
                Float vbB = 0f;
                if (bB != null) {
                    for (Float f : bB) {
                        vbB += f;
                    }
                }
                Float b = dF * vbB;
                bonus.put(employee, b);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
