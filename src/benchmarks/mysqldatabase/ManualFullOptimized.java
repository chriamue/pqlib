/***************************************************************************
 Copyright (C) 2014 chriamue

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/
package benchmarks.mysqldatabase;

import benchmarks.Evaluator;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author chriamue
 */
public class ManualFullOptimized extends Evaluator {

    @Override
    public void compute() {
        try {
            Set<Integer> employees = new HashSet<Integer>();
            Map<Integer, String> boniDescription = new HashMap<Integer, String>();
            Map<Integer, Float> bonus = new HashMap<Integer, Float>();

            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/test?user=test&password=test");
            Statement statement = connection.createStatement();

            // source: SQLBench
            ResultSet resultSet = statement.executeQuery("select bonus_employees.eid, SUM(bonus_boni.base * bonus_departments.factor),GROUP_CONCAT(bonus_boni.descr) from bonus_employees JOIN bonus_departments ON (bonus_departments.did = bonus_employees.dept) JOIN bonus_boni ON (bonus_boni.eid = bonus_employees.eid) GROUP BY bonus_employees.eid;");
            while (resultSet.next()) {
                Integer eid = resultSet.getInt("eid");
                String descr = resultSet.getString(3);
                String base = resultSet.getString(2);
                employees.add(eid);
                bonus.put(eid, Float.parseFloat(base));
                boniDescription.put(eid, descr);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
