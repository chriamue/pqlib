/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.idf;

import bench.RuntimeCreatorBench;
import benchmarks.BenchmarkOptions;
import benchmarks.Evaluator;
import benchmarks.webgraph.Webdoc;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.PQLFactory;
import edu.umass.pql.Reductor;
import edu.umass.pql.TableConstants;
import edu.umass.pql.TestBase;
import edu.umass.pql.VarConstants;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.container.PSet;
import edu.umass.pql.opt.Optimizer;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import rc2.RCInterface;
import rc2.representation.PQLReductorExtension;
import rc2.representation.PQLReductorExtension.ReductorExtensionName;
import rc2.representation.RuntimeCreatorException;

/**
 *
 * @author Hilmar
 */
public class RuntimePQL2 extends Evaluator implements TableConstants, VarConstants {

    protected Env env;
    protected Join j;
    protected static RCInterface rc = null;

    @Override
    public void
    init()
    {
        if (BenchmarkOptions.oneTimeInit && rc != null) {
            RuntimeCreatorBench.measureSteps[0] = 0;
            RuntimeCreatorBench.measureSteps[1] = 0;
            RuntimeCreatorBench.measureSteps[2] = 0;
            RuntimeCreatorBench.measureSteps[3] = 0;
            return;
        }
        RuntimeCreatorBench.measureSteps[0] = System.nanoTime();

        env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
            new Object[] {
                    new int[14],
                    new long[5],
                    new double[6],
                    new Object[9]
            });

        env.v_int[0] = 0;
        env.v_int[4] = 1;
        env.v_object[0] = new Webdoc(0);
        env.v_object[1] = benchmarks.webgraph.Generator.documents;
        env.v_object[2] = new int [1];
        env.v_object[3] = new PDefaultMap(0);

        Method method = null;
        for (int i=0; i<RuntimeCreatorBench.class.getMethods().length; i++) {
            if (RuntimeCreatorBench.class.getMethods()[i].getName().contains("sum2Int"))
                method = RuntimeCreatorBench.class.getMethods()[i];
        }

        try {
            rc = new RCInterface();
            PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);


            /*j = PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.DEFAULT_MAP( TestBase.i0r, TestBase.i5r, TestBase.i6r, TestBase.o3w)},
                    PQLFactory.Reduction(  new Reductor [] {PQLFactory.Reductors.METHOD_ADAPTER(method, TestBase.i4r, TestBase.i6w) },
                    PQLFactory.CONTAINS(TestBase.o1r, TestBase.o0w),
                    PQLFactory.FIELD(Webdoc.class, "words", TestBase.o0r, TestBase.o2w),
                    PQLFactory.ARRAY_LOOKUP_Int(TestBase.o2r, __ ,TestBase.i5w),
                    PQLFactory.LOOKUP(TestBase.o3r, TestBase.i5r, TestBase.i6w)
                    ));*/


            j = PQLFactory.Reduction(  new Reductor [] { PQLFactory.Reductors.N_DEFAULT_MAP(TestBase.i0r, TestBase.i5r, PQLFactory.Reductors.METHOD_ADAPTER(method, TestBase.i4r, __), TestBase.o3w)},// new PQLReductorExtension( "n_default_map_method_adapter", method, TestBase.i0r, TestBase.i5r, TestBase.o3w, TestBase.i4r) },
                    PQLFactory.CONTAINS(TestBase.o1r, TestBase.o0w),
                    PQLFactory.FIELD(Webdoc.class, "words", TestBase.o0r, TestBase.o2w),
                    PQLFactory.ARRAY_LOOKUP_Int(TestBase.o2r, __ ,TestBase.i5w)
                    );

            Map <Integer, Class> inst = new HashMap <Integer, Class> ();
            inst.put(0, Webdoc.class);
            inst.put(1, PSet.class);
            inst.put(2, int[].class);
            inst.put(3, PDefaultMap.class);

            RuntimeCreatorBench.measureSteps[1] = System.nanoTime();

            if (BenchmarkOptions.optimizeStatic) {
                VarSet constantVars = new VarSet();
                constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 1));
                constantVars.insert(Env.encodeReadVar(TYPE_INT, 0));
                constantVars.insert(Env.encodeReadVar(TYPE_INT, 4));
                j = Optimizer.selectAccessPathRecursively(Env.EMPTY, constantVars, j, true);
            }

            RuntimeCreatorBench.measureSteps[2] = System.nanoTime();
            rc.build(j, env, inst, BenchmarkOptions.parallelMode, (BenchmarkOptions.optimizeBytecode ? RCInterface.OPTIMIZE : 0));
        } catch (Throwable t) {
            t.printStackTrace();
        }

        RuntimeCreatorBench.measureSteps[3] = System.nanoTime();
    }

    public void
    compute()
    {
        try {
            rc.exec(env);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        this.result = env.v_object[3];
    }

    public String
    getName()
    {
            return "runtime_pql_v2";
    }
}
