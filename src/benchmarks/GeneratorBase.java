package benchmarks;
import java.util.Map;

public abstract class GeneratorBase implements BenchmarkInitInterface
{
	public static boolean SMALL_BENCHMARKS = false;

	public static int getVar(String varname, int default_value)
	{
		Map<String, String> env = System.getenv();
		final String limit_str = env.get(varname);
		if (limit_str == null)
			return default_value;
		else try {
				return Integer.decode(limit_str);
			} catch (Exception __) {
				return default_value;
			}
	}

	public abstract void init(int size); // from BenchmarkInitInterface
}
