/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmarks.arraynested;

import bench.RuntimeCreatorBench;
import benchmarks.Evaluator;
import benchmarks.webgraph.Webdoc;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.TableConstants;
import edu.umass.pql.VarConstants;
import edu.umass.pql.VarSet;
import edu.umass.pql.container.PDefaultMap;
import edu.umass.pql.opt.Optimizer;

/**
 *
 * @author Hilmar
 */
public class RuntimePQL extends Evaluator implements TableConstants, VarConstants
{
    
        protected Env env;
        protected Join j;
        protected RuntimeCreatorBench rc;
    
	@Override
	public void
	init()
	{
            env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[5],
                        new Object[9]
                });
        
            rc = new RuntimeCreatorBench();                
            j = rc.arraynested(env);            
            
            VarSet constantVars = new VarSet();
            constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 0));
            constantVars.insert(Env.encodeReadVar(TYPE_OBJECT, 1));
            constantVars.insert(Env.encodeReadVar(TYPE_INT, 0));
            
            boolean useStandardInterpreter = false, useSelectPath = false;
            for ( String arg : originalArgs) {
                if (arg.equals("-use-select-path")) {
                    /*Object tmp = rc.getEnv().v_object[1];
                    rc.getEnv().v_object[1] = rc.getEnv().v_object[0];
                    rc.getEnv().v_object[0] = tmp;*/
                    useSelectPath = true;
                    
                    /*SelectPath selectPath = SelectPath.create(new Join [] {j});        
                    selectPath.setVarInfo(new VarInfo());
                    selectPath.schedule(new VarInfo(constantVars), rc.getEnv(), false);*/
                            
                    j = Optimizer.selectAccessPathRecursively(rc.getEnv(),
                                          constantVars,
                                          j,
                                          true);
                    
                } else if (arg.equals("-use-standard-interpreter")) {
                    useStandardInterpreter = true;
                }
            }          
            
            if (!useSelectPath)
                j = Optimizer.selectAccessPathRecursively(Env.EMPTY,
                                         constantVars,
                                         j,
                                         true);
            
            if (!useStandardInterpreter) {
                RuntimeCreator.init(j, env);
                RuntimeCreator.alreadyInitialized = true;
            } else
                RuntimeCreator.useRuntimeCreator = false;
        }
    
        public void
	compute()
	{
            rc.compute(j);            
            this.result = rc.getEnv().v_object[2];
	}

	public String
	getName()
	{
		return "runtime_pql";
	}
}
