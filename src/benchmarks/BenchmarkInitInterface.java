package benchmarks;
import java.util.Map;

public interface BenchmarkInitInterface
{
	/**
	 * Initialises benchmark by size
	 *
	 * Size 100 should be default size.  Correlation between
	 * larger/smaller sizes need not be linear.
	 */
	public abstract void init(int size);
}
