package benchmarks;

import java.io.File;
import java.io.IOException;
import edu.umass.pql.*;

import java.io.DataInput;
import java.io.DataOutput;

public abstract class Evaluator implements Cloneable
{
	protected Object result;
	protected int para = 1;
        protected String [] originalArgs;

	public Evaluator
	clone()
	{
		try {
			return (Evaluator) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public void
	init()
	{
	}

	public void
	cleanup()
	{
	}

	/**
	 * Trigger computation and force it to conclude
	 *
	 * If no transfer time is needed, just write the result to `result'
	 */
	public abstract void
	compute();

	/**
	 * Transfer data to querying program (if needed)
	 */
	public Object
	obtainResult()
	{
		return this.result;
	}

	public int
	getMaxParallelism()
	{
		return 1;
	}

	public void
	setParallelism(int i)
	{
	}
        
        public void
        setOriginalArgs(String [] args)
        {
            originalArgs = args;
        }

	public String
	getName()
	{
		return this.getClass().getSimpleName() + this.para;
	}

	public static abstract class ParaEvaluator extends Evaluator
	{
		public int
		getMaxParallelism()
		{
			return Env.DEFAULT_THREADS_NR;
		}

		public void
		setParallelism(int i)
		{
			this.para = i;
		}

		public void
		compute_prepare() {}; 

		public abstract Object
		compute_finish();

		public abstract int
		get_size();

		public abstract Thread
		gen_thread(int index, int start, int stop);

		public void
		compute()
		{
			compute_prepare();

			Thread[] threads = new Thread[para];

			final int total = get_size();
			int start = 0;
			int size = total / para;
			int excess = total - (size * para);
			++size;

			for (int i = 0; i < threads.length; i++) {
				if (excess-- == 0)
					--size;
				final int stop = start + size;
				threads[i] = gen_thread(i, start, stop);
				threads[i].start();
				start = stop;
			}

			boolean retry;
			do {
				retry = false;
				try {
					for (int i = 0; i < threads.length; i++)
						threads[i].join();
				} catch (InterruptedException __) {
					retry = true;
				}
			} while (retry);

			this.result = compute_finish();
		}

		public String
		getName()
		{
			return "para-manual-" + para;
		}
	}

	public static abstract class PQLEvaluator extends Evaluator
	{
		public final void
		init()
		{
			if (this.para == 1) {
				PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
				Env.setThreadsNr(1);
			} else {
				PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.SEGMENTED);
				Env.setThreadsNr(this.para);
			}
		}

		public int
		getMaxParallelism()
		{
			return Env.DEFAULT_THREADS_NR;
		}

		public void
		setParallelism(int i)
		{
			this.para = i;
		}

		public String
		getName()
		{
			return "pql-" + para;
		}
	}

	public static abstract class HadoopEvaluator extends Evaluator
	{
		static void delete(File f) throws IOException {
			if (f.isDirectory()) {
				for (File c : f.listFiles())
					delete(c);
			}
			if (!f.delete()) {
				System.err.println("Failed to delete file: " + f);
			}
		}


		public static int THREADS_NR = 0;
		public static int LAST_THREADS_NR = -1;

		public static final String HDDATA = "./hadoop-data";
		public static final String INFILE = HDDATA + "/intable"; // needn't strictly be a table
		public static final String OUTFILE = HDDATA + "/outtable";

		public void
		pre_init()
		{
			try {
				delete(new File(HDDATA));
				(new File(HDDATA)).mkdir();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			// full pre-initialisation takes a lot of time, since we may have to wait for the servers to come up.
			// Hence we only restart them if the number of threads changed.
			if (LAST_THREADS_NR == THREADS_NR)
				return;

			LAST_THREADS_NR = THREADS_NR; // time to update

			try {
				System.err.println("[] Shutting down Hadoop...");
				Runtime.getRuntime().exec("Hadoop-run/stop.sh");
				Thread.sleep(3000); // should suffice to bring them down
				int decaseconds = 6;  // 60 seconds to bring them up; a mite conservative.
				System.err.println("[] Starting Hadoop (takes " + decaseconds + "0s...");
				Runtime.getRuntime().exec("Hadoop-run/start.sh " + THREADS_NR);
				for (; decaseconds > 0; --decaseconds) {
					System.err.print(decaseconds + " . . . ");
					Thread.sleep(10000);
				}
				System.err.println("lift-off!");
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}

		public int
		getMaxParallelism()
		{
			return Env.DEFAULT_THREADS_NR;
		}

		public void
		setParallelism(int i)
		{
			THREADS_NR = i;
			this.para = i;
		}

		public String
		getName()
		{
			return "hadoop-" + para;
		}
	}
}
