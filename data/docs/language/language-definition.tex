\documentclass{article}
\usepackage{proof}
\usepackage{amsmath}

\newcommand{\Prod}{::=}
\newcommand{\VB}{\ |\ }
\newcommand{\ntd}[1]{{\textit{#1}}}
\newcommand{\nt}[1]{\ \ensuremath{\langle{\ntd{#1}}\rangle}\ }
\newcommand{\kw}[1]{\ \textbf{\textsf{#1}}\ }
\newcommand{\fw}[1]{{\ \textsf{`#1'}}\ }
\newcommand{\term}[1]{\ {\textsf{#1}}\ }
\newcommand{\sem}[1]{\ensuremath{\llbracket #1 \rrbracket}}
\newcommand{\iju}{\texttt{:}\hspace{-1.3mm}\texttt{:}}
\usepackage{listings}

\lstset{basicstyle=\small\ttfamily,keywordstyle=\bf\sffamily,commentstyle=\sl\sffamily,columns=fullflexible,language=Java,mathescape=true,escapechar=?}
\lstset{morekeywords={query,reduce,forall,exists,over}}

\title{PQL/Java language definition, v0.2}
\author{Christoph Reichenbach, Yannis Smaragdakis, Neil Immerman}
\begin{document}
\maketitle

This document defines the domain language `PQL/Java'.  The purpose of
PQL/Java is to facilitate parallelisable queries that can be embedded
in Java programs.

\section{Integration with Java}

PQL/Java defines a number of additional keywords.  To integrate with
Java, these keywords assume the meaning defined in this document iff
the source file they are defined in contains the following import
statement:

\begin{lstlisting}
import static edu.umass.pql.Query;
\end{lstlisting}

In the absence of this statement, the syntax and semantics of a
PQL/Java program are identical to those of a regular Java program.

\paragraph{Keywords.}

PQL/Java defines the following keywords:

\begin{tabular}{p{1.5cm}p{1.5cm}p{1.5cm}p{1.5cm}p{1.5cm}}
\kw{query}
&
\kw{reduce}
&
\kw{forall}
&
\kw{exists}
&
\kw{over}
\end{tabular}


\section{Syntax}

Any Java expression (\ntd{JAVA-EXPR}) may contain a query
(\ntd{QUERY}) as a subexpression.  A query in turn follows the following production:
\[
\begin{array}{lcl}
\ntd{QUERY} & \Prod	& \nt{QUANT-EXPR}
			\VB \term{id}
			\VB \nt{JAVA-EXPR}
			\VB \nt{QEXPR}
\end{array}
\]
That is, it may be a quantifier
expression (\ntd{QUANT-EXPR}) that quantifies one or more logical variables (such as \lstinline{forall x, y : a[x] > b[y]}), a
single identifier that references such a logical variable (such as \lstinline{x} or \lstinline{y} in the above example for \ntd{QUANT-EXPR}), or one of
two unquantified expressions:  an arbitrary Java expression (which may
contain side effects but no logical variables) or a Q-Expression (\ntd{QEXPR}),
which may contain logical variables and sub-queries but no side effects.

That the semantics of Q-Expressions and Java-expressions are identical
whenever they produce the same expression.

Note that since Java expressions may contain subqueries, it is
possible to nest multiple queries in the same expression, though these
must not share variables.

In the following, we first describe quantifier expressions, then Q-expressions.

\subsection{Quantifier expressions}

Quantifier expressions take one of the following forms:

\[ %----------------------------------------
\begin{array}{lcl}
\ntd{QUANT-EXPR} & \Prod	& \nt{QUANT} \nt{ID} \fw{:} \nt{QUERY}
\\ &&
				\VB \kw{query} \fw{(} \nt{MATCH} \fw{)} \fw{:} \nt{QUERY}
\\ &&
				\VB \kw{reduce} \fw{(} \term{id} \fw{)} \nt{ID} [\kw{over} \nt{ID-SEQ}] : \nt{QUERY}
\end{array}
\] %----------------------------------------

The first form of quantification is universal or existential
quantification:  here, \ntd{QUANT} may be either \lstinline{forall} or \lstinline{exists}.
The second form of quantification, a \lstinline{container query}, constructs
maps, sets, or arrays.
The third and final form is a general-purpose
reduction operation.  We describe these forms in more detail below.

\subsubsection{Universal and existential quantifications}

Universal or existential quantification extends over an identifier
\ntd{ID}, which can be one of the following:

\[ %----------------------------------------
\begin{array}{lcl}
\ntd{ID}	& \Prod	& \term{id}
			\VB \nt{JAVA-TY} \term{id}
\end{array}
\] %----------------------------------------

For now, consider an example of the second form:
\begin{lstlisting}
  forall int x : x == x
\end{lstlisting}

This tests whether all \lstinline{x} that are of type \lstinline{int}
are equal to themselves.  This particular should always evaluate to
\lstinline{true}.  Similarly,
\begin{lstlisting}
  exists int x : x == 0.5
\end{lstlisting}
will test whether there exists an integer \lstinline{x} that is equal
to 0.5; this test will evaluate to \lstinline{false}.

We refer to the identifier occuring in the \lstinline{ID} construct as
the \emph{query variable}.  If a java type (\ntd{JAVA-TY}) is present,
the query variable is \emph{explicitly typed}.  These two cases behave
differently, as we discuss in Section~\ref{sec:domains}.  Informally,
the difference is that for queries

\begin{lstlisting}
 /* A */ forall int x : a[x] > 0
 /* B */ forall     x : a[x] > 0
\end{lstlisting}

the compiler will infer the static type for Example~B, and also infer
that it should only consider values for \lstinline{x} that occur in
the domain of \lstinline{a}, whereas for Example~A, it will consider
all $2^32$ possible \lstinline{int} values for \lstinline{x},
irrespectively of the size of \lstinline{a}.  Unless \lstinline{a} is
a special-case \lstinline{java.util.Map}, this will always yield an
array index out of bounds exception.

Section~\ref{sec:extended} describes extended syntax through which
multiple identifiers may be quantified by the same existential or
universal clause.

\subsubsection{Container queries}

A container query has the syntax

\[
\kw{query} \fw{(} \nt{MATCH} \fw{)} \fw{:} \nt{QUERY}
\]

where a \ntd{MATCH} is one of the following:

\[ %----------------------------------------
\begin{array}{lcl}
\ntd{MATCH} & \Prod	&  \fw{Set} \fw{.} \fw{contains} \fw{(} \nt{CM} \fw{)}
\\ &&
			\VB \fw{Map} \fw{.} \fw{find} \fw{(} \nt{CM} \fw{)} \fw{==} \nt{CM} [\kw{default} \nt{QUERY}]
\\ &&
			\VB \fw{Array} \fw{[} \nt{ID} \fw{]} \fw{==} \nt{CM}
\end{array}
\] %----------------------------------------

For now, assume that \ntd{CM} is an identifier.  The first of the above productions then constructs a set, as in the following example:
\begin{lstlisting}
  query (Set.contains(x)): x == 0
\end{lstlisting}
This would construct a set of integers containing precisely the number zero.

The second construction builds a map:
\begin{lstlisting}
  query (Map.find(x) == y): range(1, 10).contains(x) && y == x*x
\end{lstlisting}

This would construct a map of all numbers from 1 to 10 to their squares.  \lstinline{range(1, 10)} here is a logical
constant and a Java expression, denoting a set of all integers from 1 through 10.

Note that the above does not provide mappings for other numbers in the set; dereferencing them and treating them as integers has the
same effect as trying to implicitly unbox a \lstinline{null} in Java.

Maps may contain a default clause:
\begin{lstlisting}
  query (Map.find(x) == y default -1): range(1, 10).contains(x) && y == x*x
\end{lstlisting}

This would construct the same map as above, except that all numbers outside of the range 1 through 10 are mapped to \lstinline{-1}.

The third construct builds arrays.  For example,

\begin{lstlisting}
  query (Array[x] == y): range(1, 10).contains(x) && y == x*x
\end{lstlisting}

is the same as our map construction without defaults, with one exception:  the missing array index (0) is filled in with the default
value for the relevant type (i.e., 0 for integers).  Thus, this will construct an 11-element array containing $0, 1, \ldots, 81, 100$.


In the above, we assumed that \ntd{CM} are always identifiers.  This is not necessarily the case, since they may also be logical constants
(Java expressions) or constructor invocations:

\[ %----------------------------------------
\begin{array}{lcl}
\ntd{CM} & \Prod	& \nt{JAVA-EXPR}
			\VB \nt{ID}
\\ &&
			\VB \kw{new} \nt{JAVA-TY} \fw{(} [ \nt{CM-SEQ} ] \fw{)}
\end{array}
\] %----------------------------------------

In the above, \ntd{CM-SEQ} denotes a comma-separated sequence of \ntd{CM}.

For example, if we have a datatype `IntPair' that contains two integer values, we can construct a set of pairs of values and their negations as follows:

\begin{lstlisting}
  query (Set.contains(new IntPair(x, y))) : range(1, 100).contains(x) && y == -x
\end{lstlisting}


\subsubsection{Reductions}

The last query construct are reductions, which follow the syntax below:

\[
 \kw{reduce} \fw{(} \term{id} \fw{)} \nt{ID} [\kw{over} \nt{ID-SEQ}] : \nt{QUERY}
\]
In the above, \ntd{ID-SEQ} denotes a comma-separated sequence of \ntd{ID}.

The first parameter to a reduction is always a reduction operator, such as the built-in \lstinline{sumInt}, \lstinline{sumLong} and \lstinline{sumDouble} operators.  For example,

\begin{lstlisting}
  reduce (sumInt) x :  set.contains(x)
\end{lstlisting}

This will sum up all numbers contained in \lstinline{set} after coercing them to \lstinline{int}.

Sometimes reduction requires additional free variables.  We obtain these using the keyword \lstinline{over}:
\begin{lstlisting}
  reduce (sumDouble) x over y :  set.contains(y) && x == 1.0 / y
\end{lstlisting}

This will sum up the inverses of all numbers contained in the set.

\subsection{Q-expressions}
Specifically, a \ntd{QEXPR} has the following form:

\[
\begin{array}{lcl}
\ntd{QEXPR} & \Prod	& \fw{(} \nt{QUERY} \fw{)}
\\ &&
			\VB \nt{QUERY} \nt{BINOP} \nt{QUERY}
\\ &&
			\VB \nt{QUERY} \kw{instanceof} \nt{JAVA-TY}
\\ &&
			\VB \nt{UNOP} \nt{QUERY}
\\ &&
			\VB \nt{QUERY} \fw{?} \nt{QUERY} \fw{:} \nt{QUERY}
\\ &&
			\VB \nt{QUERY} \fw{.} \fw{find} \fw{(} \nt{QUERY} \fw{)}
\\ &&
			\VB \nt{QUERY} \fw{[} \nt{QUERY} \fw{]}
\\ &&
			\VB \nt{QUERY} \fw{.} \fw{contains} \fw{(} \nt{QUERY} \fw{)}
\\ &&
			\VB \nt{QUERY} \fw{.} \term{id}
\\ &&
			\VB \nt{QUERY} \fw{.} \kw{length}
\\ &&
			\VB \nt{QUERY} \fw{.} \kw{size} \fw{(} \fw{)}
\end{array}
\]
That is, it combines queries through a number of operators.  In the above, \ntd{BINOP} is taken from a set of binary operators listed below (these have the same semantics as in Java):
\begin{itemize}
\item {\lstinline{||}}:  logical disjunction
\item {\lstinline{&&}}:  logical conjunction
\item {\lstinline{|}}:  bitwise disjunction (this is identical to logical disjunction for booleans)
\item {\lstinline{&}}:  bitwise disjunction (this is identical to logical disjunction for booleans)
\item {\lstinline{^}}:  bitwise exclusive-or
\item {\lstinline{\%}}: modulo
\item {\lstinline{*}}: multiplication
\item {\lstinline{+}}: addition
\item {\lstinline{-}}: subtraction
\item {\lstinline{/}}: division
\item {\lstinline{>}}: greater-than comparison
\item {\lstinline{<}}: less-than comparison
\item {\lstinline{<=}}: less-than or equal comparison
\item {\lstinline{>=}}: greater-than or equal comparison
\item {\lstinline{!=}}: inequality (for objects, this is reference inequality)
\item {\lstinline{==}}: equality (for objects, this is reference equality)
\item {\lstinline{<<}}: logical shift-left
\item {\lstinline{>>}}: logical shift-right, carrying the sign
\item {\lstinline{>>>}}: logical shift-right, not carrying the sign
\item {\lstinline{=>}}: logical implication (such that `\lstinline{a => b}' is equivalent to `\lstinline{(!a) || b}', if \lstinline{!} is logical negation).  This is the only new operator.
\end{itemize}

The \lstinline{instanceof} operator also works as in Java, meaning that \lstinline{a instanceof java.lang.Set} evaluates to true iff the object \lstinline{a} has a dynamic type that is a subtype of \lstinline{java.lang.Set}.

\ntd{UNOP} expands to the three unary Java operators, \lstinline{-} (arithmetic negation), \lstinline{!} (logical negation),
and \lstinline{~} (bitwise inversion).

The ternary operator $- ? - : -$ is identical to Java's if-then-else operator for expressions (though PQL/Java provide syntactic sugar for the same construct, cf.\@ Section~\ref{sec:extended}).
For example, \lstinline{list.size() > 0 ? 1 : -1} evaluates to \lstinline{1} iff the size of the list \lstinline{list} is greater than zero, and to \lstinline{-1} otherwise.

Q-expressions of the form \lstinline{q.find(x)} or \lstinline{q[x]}
are equivalent.  Both denote a map or array lookup (depending on the
static type of \lstinline{q}) and evaluate to the element indexed by
\lstinline{x}.  For example, \lstinline{myArray.find(3)} for an
integer array \lstinline{myArray} would obtain the 4th element of the
array.  This operation raises the same exceptions as regular Java
array accesses might raise.

Q-expressions of the form \lstinline{q.contains(x)} test whether a
given Java set \lstinline{q} contains element \lstinline{x}.  They
evaluate to \lstinline{true} or \lstinline{false} and again copy the
exception behaviour of Java.

Q-expressions of the form \lstinline{q.f} are projections that obtain
the contents of field \lstinline{f}.  Field \lstinline{f} must be
accessible from the context the query originates in according to the
rules of reflective field access in Java (i.e., the field may be
private and in a different class, but field access via the Java
reflection API must be permitted).

Q-expressions of the form \lstinline{q.length} or \lstinline{q.size()}
are equivalent.  Both evaluate to the size of an array,
\lstinline{java.util.Collection}, or \lstinline{java.util.Map}, as
determined by the static type of \lstinline{q}.


%----------------------------------------------------------------------
Figure~\ref{fig:syntax} summarises the language syntax.  We define the
semantics in the next section, except for constructs inherited from
Java:

\begin{tabular} {ll}
\ntd{JAVA-EXPR} & A Java expression \\
\ntd{JAVA-TY} & A Java type \\
\term{id} & A Java identifier \\
\end{tabular}

\begin{figure}
\[
\begin{array}{lcl}
\ntd{QUERY} & \Prod	& \nt{QUANT-EXPR}
			\VB \term{id}
			\VB \nt{JAVA-EXPR}
			\VB \nt{QEXPR}
\\ \\ %--------------------------------------------------------------------------------
\ntd{QUANT-EXPR} & \Prod	& \nt{QUANT} \nt{ID} \fw{:} \nt{QUERY}
\\ &&
				\VB \kw{query} \fw{(} \nt{MATCH} \fw{)} \fw{:} \nt{QUERY}
\\ &&
				\VB \kw{reduce} \fw{(} \term{id} \fw{)} \nt{ID} [\kw{over} \nt{ID-SEQ}] : \nt{QUERY}
\\ \\ %--------------------------------------------------------------------------------
\ntd{OVER} & \Prod	& \varepsilon
			\VB \kw{over} \nt{ID-SEQ'}
\\ \\ %--------------------------------------------------------------------------------
\ntd{QUANT} & \Prod	& \kw{forall}
			\VB \kw{exists}
\\ \\ %--------------------------------------------------------------------------------
\ntd{QEXPR} & \Prod	& \fw{(} \nt{QUERY} \fw{)}
\\ &&
			\VB \nt{QUERY} \nt{BINOP} \nt{QUERY}
\\ &&
			\VB \nt{QUERY} \kw{instanceof} \nt{JAVA-TY}
\\ &&
			\VB \nt{UNOP} \nt{QUERY}
\\ &&
			\VB \nt{QUERY} \fw{?} \nt{QUERY} \fw{:} \nt{QUERY}
\\ &&
			\VB \nt{QUERY} \fw{.} \fw{find} \fw{(} \nt{QUERY} \fw{)}
\\ &&
			\VB \nt{QUERY} \fw{[} \nt{QUERY} \fw{]}
\\ &&
			\VB \nt{QUERY} \fw{.} \fw{contains} \fw{(} \nt{QUERY} \fw{)}
\\ &&
			\VB \nt{QUERY} \fw{.} \term{id}
\\ &&
			\VB \nt{QUERY} \fw{.} \kw{length}
\\ &&
			\VB \nt{QUERY} \fw{.} \kw{size} \fw{(} \fw{)}
\\ \\ %--------------------------------------------------------------------------------
\ntd{BINOP} & \Prod	& \fw{\lstinline{||}}
 			\VB \fw{\lstinline{&&}}
 			\VB \fw{\lstinline{|}}
 			\VB \fw{\lstinline{&}}
 			\VB \fw{\lstinline{^}}
 			\VB \fw{\lstinline{\%}}
 			\VB \fw{\lstinline{*}}
 			\VB \fw{\lstinline{+}}
 			\VB \fw{\lstinline{-}}
\\ &&
 			\VB \fw{\lstinline{/}}
 			\VB \fw{\lstinline{>}}
 			\VB \fw{\lstinline{<}}
 			\VB \fw{\lstinline{<=}}
 			\VB \fw{\lstinline{>=}}
 			\VB \fw{\lstinline{!=}}
 			\VB \fw{\lstinline{==}}
 			\VB \fw{\lstinline{<<}}
\\ &&
 			\VB \fw{\lstinline{>>}}
 			\VB \fw{\lstinline{>>>}}
 			\VB \fw{\lstinline{=>}}
\\ \\ %--------------------------------------------------------------------------------
\ntd{UNOP} & \Prod	& \fw{\lstinline{!}}
 			\VB \fw{\lstinline{\~}}
 			\VB \fw{\lstinline{-}}
\\ \\ %--------------------------------------------------------------------------------
\ntd{MATCH} & \Prod	&  \fw{Set} \fw{.} \fw{contains} \fw{(} \nt{CM} \fw{)}
\\ &&
			\VB \fw{Map} \fw{.} \fw{find} \fw{(} \nt{CM} \fw{)} \fw{==} \nt{CM} [\kw{default} \nt{QUERY} ]
\\ &&
			\VB \fw{Array} \fw{[} \nt{ID} \fw{]} \fw{==} \nt{CM}
\\ \\ %--------------------------------------------------------------------------------
\ntd{CM} & \Prod	& \nt{JAVA-EXPR}
			\VB \term{id}
\\ &&
			\VB \kw{new} \nt{JAVA-TY} \fw{(} [ \nt{CM-SEQ} ] \fw{)}
\\ \\ %--------------------------------------------------------------------------------
\ntd{CM-SEQ} & \Prod	& \nt{CM}
				\VB \nt{CM-SEQ} \fw{,} \nt{CM}
\\ \\ %--------------------------------------------------------------------------------
\ntd{ID}	& \Prod	& \nt{ID}
			\VB \nt{JAVA-TY} \term{id}
\\ \\ %--------------------------------------------------------------------------------
\ntd{ID-SEQ} & \Prod	& \nt{ID}
			\VB \nt{ID-SEQ} \fw{,} \nt{ID}
\end{array}
\]
\caption{PQL/J syntax}\label{fig:syntax}
\end{figure}


\section{Static Semantics}

\subsection{Types}
Type checking is as for Java.  All types are implicit, unless
annotated explicitly in \nt{ID}.
Boxing/unboxing conversions are implicit; failed conversion are
described in the dynamic semantics.

reductions, the type of the body is the type of the value being
reduced over (e.g., int for a sum of integers).  This means that
summing a set of values requires an explicit contains check:

  reduce(addInt) x : set.contains(x)

\subsection{Domains}\label{sec:domains}

Logical variables can either be declared `with type' as in
\begin{lstlisting}
forall int i : ...
\end{lstlisting}

or `without type', as in

\begin{lstlisting}
forall i : ...
\end{lstlisting}

These declarations specify different semantics.  The explicitly typed
variant has the obvious semantics, for example,
\lstinline{forall int i : a[i]}
will raise an `index out of bounds' exception if \lstinline{a} is an
array.  Explicitly typed variables
conceptually iterate over all \emph{viable} values of their type $\tau$, where
\emph{viable} is defined as follows:
\begin{itemize}
\item If $\tau$ is an enumeration, the viable values of $\tau$ are all
  members of the enumeration, as per
  \lstinline{java.util.EnumSet.allOf}.
\item If $\tau$ is an ordinal type such as \lstinline{int} or
  \lstinline{boolean}, the viable values of $\tau$ are all possible
  values for $\tau$ permitted by the Java programming language.
\item If $\tau$ is a floating-point type, i.e., \lstinline{float} or
  \lstinline{double}, then the viable values for $\tau$ are all the
  values of that type that exist in live objects on the Java heap.
\item Otherwise, $\tau$ is a reference type, and
  the viable values for $\tau$ are all the
  objects of that type that exist in live objects on the Java heap.
\end{itemize}

By contrast, a local variable without explicit type is subject to
\emph{domain inference}.  This means that the variable's type and
bounds are inferred from the body of the quantification.  The precise
rules will be described in a later version of the specification.  For now, the
following shall give an intuition ($\Delta$ denotes a set):

\[
\begin{array}{c}
\infer{\Delta \vdash \kw{forall} i : B  \iju  \delta }
      {\Delta, i \vdash B \iju \delta}
\\ \\
\infer{\Delta, i \vdash \textsf{a[i]} \iju \textsf{range(0, a.length - 1).contains(i)} }
      {}
\end{array}
\]

where \lstinline{range(a, b)} computes the set of all integers from
\lstinline{a} to \lstinline{b} (inclusive):
\begin{lstlisting}
public static java.util.Set<Integer> range(int min, int max);
\end{lstlisting}

Whenever a quantification has the judgement
\[
Q : B \iju \delta
\]
we rewrite it to
\[
Q : \delta \implies B
\]

\subsection{Semantic constraints}

\section{Dynamic Semantics}

Exceptions in the query body are propagated to the outside.  There is
no guarantee about the order in which exceptions are delivered.
Failed lookups in maps are translated into
\lstinline{java.lang.IndexOutOfBoundsException}s.

   Queries such as 
\begin{lstlisting}
query(Map.find(int a) == int b) : a == 1
                && range(1,2).contains(b)
\end{lstlisting}
 in which we construct a multi-map that is not
a partial map are invalid and raise a query failure through the
\lstinline{edu.umass.pql.AmbiguousMapException}.


\paragraph{Map defaults}  Maps constructed with the
\lstinline{default} keyword are total maps that produce the default
value unless they have been bound differently.  Such maps cannot
produce a

\section{Extended syntax}\label{sec:extended}

The language further defines the following syntactic shortcuts,
expressed as rewritings:

\begin{verbatim}
 RW0:
  'if' <QUERY.0> 'then' <QUERY.1> 'else' <QUERY.2>
  ==>
   <QUERY.0> '?' <QUERY.1> ':' <QUERY.2>

 RW1:
  <QUANT> <ID> <ID-SEQ> ':' <QUERY>
  ==>
   <QUANT> <ID> ':' <QUANT> <ID-SEQ> <QUERY>

 RW2:
  'reduce' '(' X ')' ':' S
  ==>
   'reduce' '(' X ')' <JAVA-TY> id ':' id 'where' S '.' 'contains' '(' id ')'  // id fresh

 RW3:
  'count' '(' X ')' Y ':' S
  ==>
   'reduce' '(' 'sum' ')' Y ':' '1' 'where' S

 RW4
   Map[<CM>]  ==>  Map.find(<CM>):

 RW5
   Array.find(<ID>)  ==>  Array[<ID>]
\end{verbatim}

\section{Reduction operator definition}

Reduction operators must be associative and commutative, and provide a
neutral element.  In Java, they must be static methods annotated
with exactly one of \lstinline{@DefaultValueInt(d)},
\lstinline{@DefaultValueLong(d)}
or
\lstinline{@DefaultValueDouble(d)}, where \lstinline{d} gives their
neutral element, or not annotated at all, in which case their neutral
element is \lstinline{null}.


\end{document}
