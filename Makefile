JAVAC=javac -target 1.5 -source 1.5 -Xlint:deprecation -Xlint:unchecked -cp "src/:lib/asm-4.1.jar:lib/asm-util-4.1.jar:lib/antlr-4.2.2-complete.jar:lib/junit-4.0.jar"
JAVA=java
SOURCES=`find . -name "*.java" | grep -v \\\#`
JAR=pqlib.jar

default: jar

all:
	${JAVAC} `find . -name "*.java" | grep -v \\\# | egrep -v 'benchmarks\/.*\/PQL.java'`

clean:
	rm -f `find . -name "*.class"`

test: all
	(cd src; ${JAVA} org.junit.runner.JUnitCore `find edu -name "*Test.java" | grep -v \\\# | sed 's/^\.\///' | sed 's/\.java//' | tr '/' '.'`)

benchmark: all bench/MiniBench.class
	${JAVA} bench.MiniBench

jar: all
	rm -f ${JAR}
	(cd src; jar cf ../${JAR} `find . -name "*.class"`)
