/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

import benchmarkgraphs.OutputTable.OutputType;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilmar
 */
public class Graph {

    public Platform [] platforms;
    public Platform [] standardPlatforms;
    public boolean relative;
    public int id;
    public static int idCounter = 0;

    public Graph (Platform ... _platforms) {
        platforms = _platforms;
        relative = true;
        id = idCounter++;
    }

    public Graph (Platform[] ... _platforms) {
        int totalLength = 0;
        for (int i =0; i<_platforms.length; i++)
            totalLength += _platforms[i].length;
        platforms = new Platform[totalLength];
        int counter = 0;
        for (int i =0; i<_platforms.length; i++)
            for (int j =0; j<_platforms[i].length; j++)
                platforms[counter++] = _platforms[i][j];
        relative = true;
        id = idCounter++;
    }

    public void calc (int index, OutputTable parent, PrintWriter texOutput) throws Exception {
        Result [] results = new Result [platforms.length];
        for (int i=0; i<platforms.length; i++) {
            RunProcess process = new RunProcess();
            process.run(platforms[i], standardPlatforms, id);
            results[i] = process.result;
        }

        if (parent.outputType == OutputTable.OutputType.Average3PerGraph || parent.outputType == OutputType.AverageLarge || parent.outputType == OutputType.AverageCores || parent.outputType == OutputType.BarGraph) {
            int graphsPerGraph = -1;
            switch (parent.outputType) {
                case Average3PerGraph:
                    graphsPerGraph = 3;
                    break;
                case AverageLarge:
                    graphsPerGraph = BenchmarkGraphs.factors.length;
                    break;
                case AverageCores:
                    graphsPerGraph = BenchmarkGraphs.coresList.length;
                    break;
                case BarGraph:
                    graphsPerGraph = 3;
                    break;
            }
            Result [] newResultsObj = new Result [results.length / graphsPerGraph];
            Platform [] newPlatformsObj = new Platform [platforms.length / graphsPerGraph];
            for (int i=0; i<results.length; i+=graphsPerGraph) {
                long [] newInit = new long [graphsPerGraph];
                long [] newResults = new long [graphsPerGraph];
                for (int j=0; j<newInit.length; j++) {
                    newInit[j] = results[i+j].getAverageTime(true);
                    newResults[j] = results[i+j].getAverageTime(false);
                }
                results[i].initTimes = newInit;
                results[i].resultTimes = newResults;
                newResultsObj[i/graphsPerGraph] = results[i];
                newPlatformsObj[i/graphsPerGraph] = platforms[i];
            }
            platforms = newPlatformsObj;
            results = newResultsObj;
        }

        long normalizeTo = results[0].getAverageTime(platforms[0].initValues);
        if (parent.outputType == OutputTable.OutputType.Average3PerGraph || parent.outputType == OutputTable.OutputType.AverageLarge || parent.outputType == OutputTable.OutputType.AverageCores || parent.outputType == OutputTable.OutputType.BarGraph)
            normalizeTo = (platforms[0].initValues ? results[0].initTimes : results[0].resultTimes )[0];

        List <Double> data = new ArrayList <Double> ();
        PrintWriter rFile = new PrintWriter(new File(BenchmarkGraphs.scriptDirectory + "script" + index + ".r"));
        for (int i=0; i<platforms.length; i++) {
            String arrayStr = "a" + i + " <- c(";
            for (int j=0; j< (platforms[i].initValues ? results[i].initTimes : results[i].resultTimes ).length; j++) {
                double value = (platforms[i].initValues ? results[i].initTimes : results[i].resultTimes )[j];
                if (relative)
                    value /= normalizeTo;
                else
                    value /= 1000000.0D;
                arrayStr += (j == 0 ? "" : ",") + value;
                data.add(value);
            }
            arrayStr += ")\n";
            rFile.write(arrayStr);
        }

        double highestBarGraph = 0;
        for (int i=0; i<data.size()-1; i+=2) {
            if (data.get(i) + data.get(i+1) > highestBarGraph)
                highestBarGraph = data.get(i) + data.get(i+1);
        }
        String title = (relative ? "normalized to " + (normalizeTo) / 1000000.0D + "ms" : "values in ms");
        String colors = "", arrayNames = "", maxArrayNames = "max(c(", otherGraphs = "", names = "", boxPlotColors = "";
        final String [] colorNames = {"blue", "red", "darkolivegreen3", "orange", "pink", "black"};
        String cexStyles = "", lwdStyles = "", pchStyles = "", ltyStyles = "";
        String [] lineStyles = new String[platforms.length];
        final int [] symbols = new int [] {4, 15, 10, 23, 19};
        for (int i=0; i<platforms.length; i++) {
            int [] newStyle = new int[]{2,4,symbols[i%symbols.length],i%6+1};
            cexStyles += (i == 0 ? "" : ", ") + newStyle[0];
            lwdStyles += (i == 0 ? "" : ", ") + newStyle[1];
            pchStyles += (i == 0 ? "" : ", ") + newStyle[2];
            ltyStyles += (i == 0 ? "" : ", ") + newStyle[3];
            lineStyles[i] = "pch=" + newStyle[2] + ", lty=" + newStyle[3] + ", lwd=" + newStyle[1] + ", cex=" + newStyle[0];
            colors += (i == 0 ? "" : ", ") + "\"" + colorNames[i%colorNames.length] + "\"";
            boxPlotColors += (i == 0 ? "" : ", ") + "\"" + (platforms[i].initValues ? "peachpuff2" : "lightsteelblue1") + "\"";
            arrayNames += (i == 0 ? "" : ", ") + "a" + i;
            maxArrayNames += (i == 0 ? "" : ", ") + "max(a" + i + ")";
            names += (i == 0 ? "" : ", ") + "\"" + platforms[i].getGraphName() + "\"";
            if (i > 0 ) {
                otherGraphs += "lines(a" + i + ", type=\"o\", " + lineStyles[i] + ", col=\"" + colorNames[i%colorNames.length] + "\")\n";
            }
        }
        maxArrayNames+="))";
        String factorNames = "";
        for (int i=0; i<BenchmarkGraphs.factors.length; i++)
            factorNames += (i > 0 ? ", " : "") + "\"" + BenchmarkGraphs.factors[i] + "\"";
        String coresNames = "";
        for (int i=0; i<BenchmarkGraphs.coresList.length; i++)
            coresNames += (i > 0 ? ", " : "") + "\"" + BenchmarkGraphs.coresList[i] + "\"";

        if (parent.outputType == OutputTable.OutputType.Average3PerGraph)
            rFile.write("pdf(\"" + BenchmarkGraphs.tablesDirectory + "table_" + index + ".pdf\", width=18, height=6)\npar(cex.main=3)\npar(cex.axis=2)\ng_range <- range(0, " + maxArrayNames + ")\nplot(a0, type=\"o\", " + lineStyles[0] + ", col=\"" + colorNames[0] + "\", ylim=g_range, axes=FALSE, ann=FALSE)\naxis(1, at=1:3, lab=c(\"Standard\", \"Factor 10\", \"Factor 100\"))\naxis(2, las=1)\nbox()\n" + otherGraphs + "title(main=\"" + title + "\", font.main=3)\nlegend(x=\"topleft\", legend=c(" + names + "), cex=3, pt.cex=c(" + cexStyles + "), lwd=c(" + lwdStyles + "), pch=c(" + pchStyles + "), lty=c(" + ltyStyles + "), col=c(" + colors + "))\nabline(h = 1, untf = FALSE, lty = \"dashed\")\ngarbage <- dev.off()");
        else if (parent.outputType == OutputTable.OutputType.AverageLarge)
            rFile.write("pdf(\"" + BenchmarkGraphs.tablesDirectory + "table_" + index + ".pdf\", width=18, height=6)\npar(cex.main=3)\npar(cex.axis=2)\ng_range <- range(0, " + maxArrayNames + ")\nplot(a0, type=\"o\", " + lineStyles[0] + ", col=\"" + colorNames[0] + "\", ylim=g_range, axes=FALSE, ann=FALSE)\naxis(1, at=1:" + BenchmarkGraphs.factors.length + ", lab=c(" + factorNames + "))\naxis(2, las=1)\nbox()\n" + otherGraphs + "title(main=\"" + title + "\", font.main=3)\nlegend(x=\"topleft\", legend=c(" + names + "), cex=3, pt.cex=c(" + cexStyles + "), lwd=c(" + lwdStyles + "), pch=c(" + pchStyles + "), lty=c(" + ltyStyles + "), col=c(" + colors + "))\nabline(h = 1, untf = FALSE, lty = \"dashed\")\ngarbage <- dev.off()");
        else if (parent.outputType == OutputTable.OutputType.AverageCores)
            rFile.write("pdf(\"" + BenchmarkGraphs.tablesDirectory + "table_" + index + ".pdf\", width=18, height=6)\npar(cex.main=3)\npar(cex.axis=2)\ng_range <- range(0, " + maxArrayNames + ")\nplot(a0, type=\"o\", " + lineStyles[0] + ", col=\"" + colorNames[0] + "\", ylim=g_range, axes=FALSE, ann=FALSE)\naxis(1, at=1:" + BenchmarkGraphs.coresList.length + ", lab=c(" + coresNames + "))\naxis(2, las=1)\nbox()\n" + otherGraphs + "title(main=\"" + title + "\", font.main=3)\nlegend(x=\"bottomleft\", legend=c(" + names + "), cex=2, pt.cex=c(" + cexStyles + "), lwd=c(" + lwdStyles + "), pch=c(" + pchStyles + "), lty=c(" + ltyStyles + "), col=c(" + colors + "))\nabline(h = 1, untf = FALSE, lty = \"dashed\")\ngarbage <- dev.off()");
        else if (parent.outputType == OutputTable.OutputType.BoxGraph)
            rFile.write("pdf(\"" + BenchmarkGraphs.tablesDirectory + "table_" + index + ".pdf\", width=12, height=6)\npar(cex.main=3)\npar(cex.axis=3)\npar(mgp = c(0, 2, 0))\nboxplot(" + arrayNames + ", names=c(" + names + "), col=(c(" + boxPlotColors + ")), main=\"" + title + "\")\nabline(h = 1, untf = FALSE, lty = \"dashed\")\ngarbage <- dev.off()");
        else
            rFile.write("myData = matrix(c(" + arrayNames + "), nrow=3, ncol=" + platforms.length + ", byrow=FALSE)\npdf(\"" + BenchmarkGraphs.tablesDirectory + "table_" + index + ".pdf\", width=12, height=6)\nbarplot(myData, density = c(10,20,15), angle = c(45, 90,180), ylim=c(0," + (highestBarGraph*1.5) + "), names=c(" + names + "), col=c(\"red\", \"blue\", \"darkolivegreen3\"), main = \"" + title + "\")\nlegend(x=\"topright\", density = c(10,20,15), angle = c(45, 90, 180), legend=c(\"remaining init\", \"just access-path\", \"just bytecode-init\"), cex=2, fill=c(\"red\", \"blue\", \"darkolivegreen3\"))\ngarbage <- dev.off()");

        rFile.close();

        if (BenchmarkGraphs.rCommand != null)
            RProcess.run(index);
    }

    public Graph setStandardGraphs (Platform ... _platforms) {
        standardPlatforms = _platforms;
        return this;
    }

}
