/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

/**
 *
 * @author Hilmar
 */
public class Result {    
    
    public long [] initTimes;
    public long [] resultTimes;            
    
    public long getAverageTime (boolean initTime) {
        long average = 0;
        for (long value : (initTime ? initTimes : resultTimes))
            average += value;
        return average / (initTime ? initTimes : resultTimes).length;
    }
    
}
