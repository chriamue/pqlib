/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

import benchmarkgraphs.Platform.BasicPlatform;
import benchmarkgraphs.Platform.Benchmark;
import benchmarkgraphs.Platform.ParallelMode;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilmar
 */
public class RunProcess {

    private static String [] fromFile = null;
    private static int globalFileRead = 0;

    public Result result;

    public static void init() throws Exception {
        if (BenchmarkGraphs.resultsFromFile != null) {
            BufferedReader in = new BufferedReader( new InputStreamReader(new FileInputStream(BenchmarkGraphs.resultsFromFile), "UTF-8"));
            String nextLine;
            List <String> lines = new ArrayList <String> ();
            while ( (nextLine = in.readLine()) != null)
                lines.add(nextLine);
            in.close();
            fromFile = lines.toArray(new String[lines.size()]);
        }
    }

    public void run (Platform platform, Platform [] standardPlatforms, int graphId) throws Exception {

        if (!BenchmarkGraphs.alternativeCommand)
            System.out.println("  =>   Starting benchmark: " + platform.toString());

        result = new Result();
        result.initTimes = new long [10];
        result.resultTimes = new long [10];

        List <String> options = new ArrayList <String> () ;
        if (platform.dataSize > 1)
            options.add("factor" + platform.dataSize);
        if (platform.parallel == ParallelMode.Parallel)
            options.add("parallel");
        if (platform.constAntiOptimize)
            options.add("constantiopt");
        if (platform.instanceAntiOptimize)
            options.add("instanceantiopt");
        if (platform.optimizeStatic)
            options.add("optimize_static");
        if (platform.optimizeBytecode)
            options.add("optimize_bytecode");
        if (platform.dataSwap)
            options.add("dataswap");
        if (platform.optimizeDynamic)
            options.add("optimize_dynamic");
        if (platform.justAccessPath)
            options.add("just_accesspath");
        if (platform.justRealInit)
            options.add("just_real_init");
        if (platform.justRemain)
            options.add("just_remain");
        if (platform.parallel == ParallelMode.ParallelIndividual)
            options.add("parallel_cores_" + platform.cores);

        String optionsStr = "";
        for (int i=0; i<options.size(); i++)
            optionsStr += (i > 0 ? "," : "") + options.get(i);
        if (!optionsStr.equals(""))
            optionsStr = " \"-extended(" + optionsStr + ")\"";

        String javaJar = BenchmarkGraphs.jarStd;
        String standardPlatformNames = "";
        if (!(standardPlatforms == null || platform.noResultCheck()))
            for (Platform standard : standardPlatforms)
                standardPlatformNames += "," + standard.getPlatformName();
        String globalSizeStr = BenchmarkGraphs.globalSizes[graphId%BenchmarkGraphs.globalSizes.length];
        if (!globalSizeStr.equals("100"))
            globalSizeStr = "--size " + globalSizeStr + " ";
        else
            globalSizeStr = "";
        String completeCommand = BenchmarkGraphs.javaCommand + " \"" + javaJar + "\" " + globalSizeStr + "run " + platform.benchmark.toString() + " " + platform.getPlatformName()+standardPlatformNames + optionsStr;
        if (!BenchmarkGraphs.alternativeCommand)
            System.out.println("  =>  Command: " + completeCommand);
        else {
            System.out.println(completeCommand);
            return;
        }
        Process process = null;
        BufferedReader reader = null;
        BufferedReader readerErr = null;

        if (fromFile == null) {
            process = Runtime.getRuntime ().exec (completeCommand.split(" "));
            InputStream stderr = process.getErrorStream ();
            InputStream stdout = process.getInputStream ();

            reader = new BufferedReader (new InputStreamReader(stdout));
            readerErr = new BufferedReader (new InputStreamReader(stderr));
        }

        int counter = 0, counter2 = 0;
        boolean ignore = false;

        while (true) {
            boolean finished = true;
            if (fromFile == null) {
                try {
                    process.exitValue();
                } catch (Exception e) {
                    finished = false;
                }
            } else
                finished = (counter2 >= 27 * (1 + (standardPlatformNames.equals("") || (standardPlatforms.length > 0 && standardPlatforms[0].getPlatformName().equals(platform.getPlatformName())) ? 0 : 1) ));

            if (finished)
                break;
            String line = "";
            while (true) {
                if (fromFile == null) {
                    line = reader.readLine ();
                    if (line == null)
                        break;
                } else {
                    if (!line.equals("") && counter2%27 == 0)
                        break;
                    line = fromFile[globalFileRead++];
                }
                counter2++;
                System.out.println("[runjava]   " + line);
                if (line.contains("stddev: "))
                    continue;

                if (!(standardPlatforms == null || platform.noResultCheck())) {
                    if (line.contains("[BENCH]")) {
                        boolean stdContain = false, mainContain = false;
                        for (Platform stdPlatform : standardPlatforms)
                            if (line.contains("\t"+stdPlatform.getPlatformName()+"\t"))
                                stdContain = true;
                        if (line.contains("\t"+platform.getPlatformName()+"\t"))
                            mainContain = true;
                        ignore = (stdContain && !mainContain);
                    }
                }

                if (ignore)
                    continue;

                long value = Long.parseLong(line.substring(line.indexOf("result: ")+8));
                if (counter/2 >= 3) {
                    if (counter%2 == 0)
                        result.resultTimes[counter/2-3] = value;
                    else
                        result.initTimes[counter/2-3] = value;
                }
                counter++;
            }

            if (fromFile == null) {
                while ((line = readerErr.readLine ()) != null) {
                    throw new Exception(line);
                }
            }
        }
        SleepThread.wait10Seconds();
    }

}
