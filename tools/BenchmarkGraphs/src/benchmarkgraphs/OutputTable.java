/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

import benchmarkgraphs.OutputTable.OutputType;
import java.io.File;
import java.io.PrintWriter;

/**
 *
 * @author Hilmar
 */
public class OutputTable {

    public enum OutputType {BoxGraph, Average3PerGraph, AverageLarge, AverageCores, BarGraph};

    public OutputType outputType;
    public Graph [][] table;
    public String [] columns;
    public String [] lines;

    public boolean continuing;
    public Graph continueWith;

    public OutputTable (OutputType _outputType, int width, int height, String [] _columns, String [] _lines) {
        outputType = _outputType;
        table = new Graph[width][height];
        columns = _columns;
        lines = _lines;
        continuing = false;
    }

    public void continueWith (Graph _graph) {
        continueWith = _graph;
        continuing = true;
    }

    public void calc () throws Exception {
        PrintWriter texOutput = new PrintWriter(new File(BenchmarkGraphs.texFile));
        texOutput.write("\\documentclass[a4paper, 12pt]{article}\n\\usepackage{todonotes}\n\\begin{document}\n\\begin{tabular}[ht]{|");
        for (int i=0; i<table.length+1; i++)
            texOutput.write("c|");
        texOutput.write("}\n\\hline\n");
        for (int i=0; i<table.length; i++)
            texOutput.write(" & \\textbf{" + columns[i] + "}");
        texOutput.write("\\\\");
        for (int i=0; i<table[0].length; i++) {
            texOutput.write("\n\\hline\n\\textbf{" + lines[i] + "}");
            for (int j=0; j<table.length; j++) {
                if (continuing) {
                    if (table[j][i] == continueWith)
                        continuing = false;
                }
                if (!continuing)
                    table[j][i].calc(i*table.length + j, this, texOutput);
                texOutput.write(" & \\includegraphics[width=" + (0.9D / table.length ) + "\\textwidth]{" + BenchmarkGraphs.tablesDirectoryInTexPath + "table_" + (i*table.length + j) + ".pdf}");
            }
            texOutput.write("\\\\");
        }
        texOutput.write("\n\\hline\n\\end{tabular}\n\\end{document}");
        texOutput.close();

        if (BenchmarkGraphs.texCommand != null)
            LatexProcess.run();
    }

}
