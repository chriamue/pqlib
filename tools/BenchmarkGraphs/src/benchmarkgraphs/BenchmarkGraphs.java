/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

import benchmarkgraphs.OutputTable.OutputType;
import benchmarkgraphs.Platform.BasicPlatform;
import benchmarkgraphs.Platform.Benchmark;
import benchmarkgraphs.Platform.ParallelMode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hilmar
 */
public class BenchmarkGraphs {


    public static String javaCommand = "java -Xms13200m -jar";
    public static String jarStd = "jars/PQLib.jar";
    public static String rCommand = "RScript";
    public static String texCommand = "pdflatex -output-directory=output";
    public static String scriptDirectory = "output/scripts/";
    public static String tablesDirectory = "output/tables/";
    public static String tablesDirectoryInTexPath = "tables/";
    public static String texFile = "output/preview.tex";
    public static int cores = 4;
    public static String[] globalSizes = new String[] {"100"};
    public static int factors [] = {1, 2, 4, 8, 16, 32, 64, 128, 256};
    public static int coresList [] = {1, 2, 4, 8};
    public static boolean alternativeCommand = false;
    public static String resultsFromFile = "results1.v7.txt";

    //sisyphos
    /*public static String javaCommand = "/opt/jdk1.8.0/bin/java -Xms13200m -jar";
    public static String jarStd = "jars/PQLib.jar";
    public static String rCommand = null;
    public static String texCommand = null;
    public static String scriptDirectory = "output/scripts/";
    public static String tablesDirectory = "output/tables/";
    public static String tablesDirectoryInTexPath = "tables/";
    public static String texFile = "output/preview.tex";
    public static int cores = 4;
    public static String[] globalSizes = new String[] {"100"};
    public static int factors [] = {1, 2, 4, 8, 16, 32, 64, 128, 256};
    public static int coresList [] = {1, 2, 4, 8};
    public static boolean alternativeCommand = true;
    public static String resultsFromFile = null;*/

    public static void main(String[] args) {

        try {

            RunProcess.init();

            if (args.length >= 3) {
                javaCommand = args[1] + " -Xms13200m -jar";
                int coresScale = Integer.parseInt(args[2]);
                List <Integer> coresScaleList = new ArrayList <Integer> ();
                for (int i=1; true; i*=2) {
                    if (i > 128)
                        throw new Error("invalid cores (have to be 2^x and <128)");
                    coresScaleList.add(i);
                    if (i == coresScale)
                        break;
                }
                coresList = new int [coresScaleList.size()];
                Integer [] coresScaleListInteger = coresScaleList.toArray(new Integer [coresScaleList.size()]);
                for (int i=0; i<coresScaleListInteger.length; i++ )
                    coresList[i] = coresScaleListInteger[i];
            }

            if (args.length >= 4) {
                globalSizes = args[3].split("/");
            }

            if (args.length >= 5) {
                if (args[4].toLowerCase().equals("xcomp"))
                    javaCommand = javaCommand.replace("-Xms13200m -jar", "-Xms13200m -Xcomp -jar");
                else
                    throw new Exception("arg 5 have to be empty or 'xcomp'.");
            }

            if (args.length >= 1) {
                if (args[0].equals("init-times-split")) {
                    OutputTable output = new OutputTable(OutputType.BarGraph, 1, 1,new String [] {""}, new String [] {"init-times"});
                    Graph [] graphs = new Graph [1];
                    graphs[0] = new Graph( new Platform("wordcount", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRemain(true), new Platform("wordcount", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustAccessPath(true), new Platform("wordcount", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRealInit(true),
                            new Platform("threegrep", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRemain(true), new Platform("threegrep", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustAccessPath(true), new Platform("threegrep", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRealInit(true),
                            new Platform("bonus", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRemain(true), new Platform("bonus", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustAccessPath(true), new Platform("bonus", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRealInit(true),
                            new Platform("webgraph", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRemain(true), new Platform("webgraph", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustAccessPath(true), new Platform("webgraph", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRealInit(true),
                            new Platform("setnested", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRemain(true).setDataSize(64).setOptimizeDynamic(true), new Platform("setnested", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustAccessPath(true).setDataSize(64).setOptimizeDynamic(true), new Platform("setnested", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRealInit(true).setDataSize(64).setOptimizeDynamic(true),
                            new Platform("arraynested", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRemain(true).setDataSize(64).setOptimizeDynamic(true), new Platform("arraynested", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustAccessPath(true).setDataSize(64).setOptimizeDynamic(true), new Platform("arraynested", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setInitValues(true).setJustRealInit(true).setDataSize(64).setOptimizeDynamic(true));
                    graphs[0].relative = false;
                    output.table[0][0] = graphs[0];
                    output.calc();
                } else if (args[0].equals("cores")) {
                    OutputTable output = new OutputTable(OutputType.AverageCores, 1, 4,new String [] {""}, new String [] {"wordcount", "threegrep", "bonus", "webgraph"});
                    Graph [] graphs = new Graph [4];
                    graphs[0] = new Graph( new Platform("manual", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("v2", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("comp", Benchmark.wordcount, BasicPlatform.Compiler).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.wordcount, BasicPlatform.ManualParallel).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel));
                    graphs[1] = new Graph( new Platform("manual", Benchmark.threegrep, BasicPlatform.Manual).setOptimizeStatic(false).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("v2", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).setOptimizeStatic(false).allPlatformsOfCores(), new Platform("comp", Benchmark.threegrep, BasicPlatform.Compiler).setOptimizeStatic(false).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.threegrep, BasicPlatform.ManualParallel).setOptimizeStatic(false).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel).setOptimizeStatic(false));
                    graphs[2] = new Graph( new Platform("manual", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("v2", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("comp", Benchmark.bonus, BasicPlatform.Compiler).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.bonus, BasicPlatform.ManualParallel).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.bonus, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel));
                    graphs[3] = new Graph( new Platform("manual", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("v2", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("comp", Benchmark.webgraph, BasicPlatform.Compiler).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.webgraph, BasicPlatform.ManualParallel).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel));
                    /*graphs[0] = new Graph( new Platform("v2", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("comp", Benchmark.wordcount, BasicPlatform.Compiler).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.wordcount, BasicPlatform.ManualParallel).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel));
                    graphs[1] = new Graph( new Platform("v2", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).setOptimizeStatic(false).allPlatformsOfCores(), new Platform("comp", Benchmark.threegrep, BasicPlatform.Compiler).setOptimizeStatic(false).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.threegrep, BasicPlatform.ManualParallel).setOptimizeStatic(false).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual", Benchmark.threegrep, BasicPlatform.Manual).setOptimizeStatic(false).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel).setOptimizeStatic(false));
                    graphs[2] = new Graph( new Platform("v2", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("comp", Benchmark.bonus, BasicPlatform.Compiler).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.bonus, BasicPlatform.ManualParallel).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.bonus, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel));
                    graphs[3] = new Graph( new Platform("v2", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("comp", Benchmark.webgraph, BasicPlatform.Compiler).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual_p", Benchmark.webgraph, BasicPlatform.ManualParallel).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores(), new Platform("manual", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(ParallelMode.ParallelIndividual).allPlatformsOfCores()).setStandardGraphs(new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.ManualParallel).setParallelMode(Platform.ParallelMode.Parallel));*/
                    for (int i=0; i<graphs.length; i++)
                        output.table[0][i] = graphs[i];
                    output.calc();
                } else if (args[0].equals("scaled")) {
                    OutputTable output = new OutputTable(OutputType.AverageLarge, 1, 2, new String [] {""}, new String [] {"setnested", "arraynested"});
                    Graph [] graphs = new Graph [2];
                    graphs[0] = new Graph( new Platform("v2", Benchmark.setnested, BasicPlatform.RuntimePQLV2).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("v2_init", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.setnested, BasicPlatform.Compiler).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.setnested, BasicPlatform.Compiler).setDataSwap(true).allPlatformsOfFactors() );
                    graphs[1] = new Graph( new Platform("v2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("v2_init", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.arraynested, BasicPlatform.Compiler).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.arraynested, BasicPlatform.Compiler).setDataSwap(true).allPlatformsOfFactors() );
                    for (int i=0; i<graphs.length; i++)
                        output.table[0][i] = graphs[i];
                    output.calc();
                }
            }

            /*OutputTable output = new OutputTable(OutputType.BoxGraph, 2, 1);
            Graph graph1 = new Graph( new Platform("Threegrep", Benchmark.threegrep, BasicPlatform.RuntimePQLV2), new Platform("Threegrep Init", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setInitValues(true) );
            Graph graph2 = new Graph( new Platform("Bonus", Benchmark.bonus, BasicPlatform.RuntimePQLV2), new Platform("Bonus Init", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setInitValues(true) );
            output.table[0][0] = graph1;
            output.table[1][0] = graph2;
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.Average3PerGraph, 1, 3, new String [] {""}, new String [] {"setnested", "arraynested", "mapnested"});
            Graph graph1 = new Graph( new Platform("runtime_pql", Benchmark.setnested, BasicPlatform.RuntimePQL).setDataSize(1), new Platform("runtime_pql", Benchmark.setnested, BasicPlatform.RuntimePQL).setDataSize(10), new Platform("runtime_pql", Benchmark.setnested, BasicPlatform.RuntimePQL).setDataSize(100),
                                     new Platform("runtime_pql2", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSize(1), new Platform("runtime_pql2", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSize(10), new Platform("runtime_pql2", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSize(100));
            Graph graph2 = new Graph( new Platform("runtime_pql", Benchmark.arraynested, BasicPlatform.RuntimePQL).setDataSize(1), new Platform("runtime_pql", Benchmark.arraynested, BasicPlatform.RuntimePQL).setDataSize(10), new Platform("runtime_pql", Benchmark.arraynested, BasicPlatform.RuntimePQL).setDataSize(100),
                                     new Platform("runtime_pql2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSize(1), new Platform("runtime_pql2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSize(10), new Platform("runtime_pql2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSize(100));
            Graph graph3 = new Graph(
                                     new Platform("runtime_pql2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSize(1), new Platform("runtime_pql2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSize(10), new Platform("runtime_pql2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSize(100));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.BoxGraph, 1, 4,new String [] {""}, new String [] {"wordcount", "threegrep", "bonus", "webgraph"});
            Graph graph1 = new Graph( new Platform("RTC_serial", Benchmark.wordcount, BasicPlatform.RuntimePQLV2), new Platform("RTC_parallel", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_serial", Benchmark.wordcount, BasicPlatform.Compiler), new Platform("Comp_parallel", Benchmark.wordcount, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_serial", Benchmark.wordcount, BasicPlatform.Manual), new Platform("Manual_parallel", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph2 = new Graph( new Platform("RTC_serial", Benchmark.threegrep, BasicPlatform.RuntimePQLV2), new Platform("RTC_parallel", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_serial", Benchmark.threegrep, BasicPlatform.Compiler), new Platform("Comp_parallel", Benchmark.threegrep, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_serial", Benchmark.threegrep, BasicPlatform.Manual), new Platform("Manual_parallel", Benchmark.threegrep, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph3 = new Graph( new Platform("RTC_serial", Benchmark.bonus, BasicPlatform.RuntimePQLV2), new Platform("RTC_parallel", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_serial", Benchmark.bonus, BasicPlatform.Compiler), new Platform("Comp_parallel", Benchmark.bonus, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_serial", Benchmark.bonus, BasicPlatform.Manual), new Platform("Manual_parallel", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph4 = new Graph( new Platform("RTC_serial", Benchmark.webgraph, BasicPlatform.RuntimePQLV2), new Platform("RTC_parallel", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_serial", Benchmark.webgraph, BasicPlatform.Compiler), new Platform("Comp_parallel", Benchmark.webgraph, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_serial", Benchmark.webgraph, BasicPlatform.Manual), new Platform("Manual_parallel", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.table[0][3] = graph4;
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.BoxGraph, 1, 4,new String [] {""}, new String [] {"wordcount", "threegrep", "bonus", "webgraph"});
            Graph graph1 = new Graph( new Platform("standard", Benchmark.wordcount, BasicPlatform.RuntimePQLV2), new Platform("const", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true), new Platform("both", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true).setInstanceAntiOptimize(true)).setStandardGraphs(new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph2 = new Graph( new Platform("standard", Benchmark.threegrep, BasicPlatform.RuntimePQLV2), new Platform("const", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true), new Platform("both", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true).setInstanceAntiOptimize(true)).setStandardGraphs(new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph3 = new Graph( new Platform("standard", Benchmark.bonus, BasicPlatform.RuntimePQLV2), new Platform("const", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true), new Platform("both", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true).setInstanceAntiOptimize(true)).setStandardGraphs(new Platform("Manual_s", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph4 = new Graph( new Platform("standard", Benchmark.webgraph, BasicPlatform.RuntimePQLV2), new Platform("const", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true), new Platform("both", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setConstAntiOptimize(true).setInstanceAntiOptimize(true)).setStandardGraphs(new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.table[0][3] = graph4;
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.BoxGraph, 1, 4,new String [] {""}, new String [] {"wordcount", "threegrep", "bonus", "webgraph"});
            Graph graph1 = new Graph( new Platform("RTC", Benchmark.wordcount, BasicPlatform.RuntimePQL), new Platform("v2_s", Benchmark.wordcount, BasicPlatform.RuntimePQLV2), new Platform("v2_p", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_s", Benchmark.wordcount, BasicPlatform.Compiler), new Platform("Comp_p", Benchmark.wordcount, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.Manual), new Platform("Manual_p", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel)).setStandardGraphs(new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph2 = new Graph( new Platform("RTC", Benchmark.threegrep, BasicPlatform.RuntimePQL), new Platform("v2_s", Benchmark.threegrep, BasicPlatform.RuntimePQLV2), new Platform("v2_p", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_s", Benchmark.threegrep, BasicPlatform.Compiler), new Platform("Comp_p", Benchmark.threegrep, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.Manual), new Platform("Manual_p", Benchmark.threegrep, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel)).setStandardGraphs(new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph3 = new Graph( new Platform("RTC", Benchmark.bonus, BasicPlatform.RuntimePQL), new Platform("v2_s", Benchmark.bonus, BasicPlatform.RuntimePQLV2), new Platform("v2_p", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_s", Benchmark.bonus, BasicPlatform.Compiler), new Platform("Comp_p", Benchmark.bonus, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_s", Benchmark.bonus, BasicPlatform.Manual), new Platform("Manual_p", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel)).setStandardGraphs(new Platform("Manual_s", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph4 = new Graph( new Platform("RTC", Benchmark.webgraph, BasicPlatform.RuntimePQL), new Platform("v2_s", Benchmark.webgraph, BasicPlatform.RuntimePQLV2), new Platform("v2_p", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Comp_s", Benchmark.webgraph, BasicPlatform.Compiler), new Platform("Comp_p", Benchmark.webgraph, BasicPlatform.Compiler).setParallelMode(Platform.ParallelMode.Parallel), new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.Manual), new Platform("Manual_p", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel)).setStandardGraphs(new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.table[0][3] = graph4;
            output.continueWith(graph4);
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.BoxGraph, 1, 4,new String [] {""}, new String [] {"wordcount", "threegrep", "bonus", "webgraph"});
            Graph graph1 = new Graph( new Platform("v2_s", Benchmark.wordcount, BasicPlatform.RuntimePQLV2), new Platform("static", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true), new Platform("bytecode", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setOptimizeBytecode(true), new Platform("both", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true).setOptimizeBytecode(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph2 = new Graph( new Platform("v2_s", Benchmark.threegrep, BasicPlatform.RuntimePQLV2), new Platform("static", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true), new Platform("bytecode", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setOptimizeBytecode(true), new Platform("both", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true).setOptimizeBytecode(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph3 = new Graph( new Platform("v2_s", Benchmark.bonus, BasicPlatform.RuntimePQLV2), new Platform("static", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true), new Platform("bytecode", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setOptimizeBytecode(true), new Platform("both", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true).setOptimizeBytecode(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph4 = new Graph( new Platform("v2_s", Benchmark.webgraph, BasicPlatform.RuntimePQLV2), new Platform("static", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true), new Platform("bytecode", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setOptimizeBytecode(true), new Platform("both", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setOptimizeStatic(true).setOptimizeBytecode(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.table[0][3] = graph4;
            output.calc();*/


            /*OutputTable output = new OutputTable(OutputType.AverageLarge, 1, 3, new String [] {""}, new String [] {"setnested", "arraynested", "mapnested"});
            Graph graph1 = new Graph( new Platform("v2", Benchmark.setnested, BasicPlatform.RuntimePQLV2).allPlatformsOfFactors(), new Platform("v2_swap", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("v2_opt_swap", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.setnested, BasicPlatform.Compiler).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.setnested, BasicPlatform.Compiler).setDataSwap(true).allPlatformsOfFactors());//.setStandardGraphs(new Platform("runtime_pql2", Benchmark.setnested, BasicPlatform.RuntimePQLV2));
            Graph graph2 = new Graph( new Platform("v2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).allPlatformsOfFactors(), new Platform("v2_swap", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSwap(true).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("v2_opt_swap", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.arraynested, BasicPlatform.Compiler).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.arraynested, BasicPlatform.Compiler).setDataSwap(true).allPlatformsOfFactors());//.setStandardGraphs(new Platform("runtime_pql2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2));
            Graph graph3 = new Graph( new Platform("v2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).allPlatformsOfFactors(), new Platform("v2_swap", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("v2_opt_swap", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setOptimizeDynamic(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.mapnested, BasicPlatform.Compiler).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.mapnested, BasicPlatform.Compiler).setDataSwap(true).allPlatformsOfFactors());//.setStandardGraphs(new Platform("runtime_pql2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.AverageLarge, 1, 3, new String [] {""}, new String [] {"setnested", "arraynested", "mapnested"});
            Graph graph1 = new Graph( new Platform("v2", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_swap", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_opt_swap", Benchmark.setnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.setnested, BasicPlatform.Compiler).setInitValues(true).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.setnested, BasicPlatform.Compiler).setDataSwap(true).setInitValues(true).allPlatformsOfFactors());//.setStandardGraphs(new Platform("runtime_pql2", Benchmark.setnested, BasicPlatform.RuntimePQLV2));
            Graph graph2 = new Graph( new Platform("v2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_swap", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_opt_swap", Benchmark.arraynested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.arraynested, BasicPlatform.Compiler).setInitValues(true).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.arraynested, BasicPlatform.Compiler).setDataSwap(true).setInitValues(true).allPlatformsOfFactors());//.setStandardGraphs(new Platform("runtime_pql2", Benchmark.arraynested, BasicPlatform.RuntimePQLV2));
            Graph graph3 = new Graph( new Platform("v2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_swap", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_opt", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("v2_opt_swap", Benchmark.mapnested, BasicPlatform.RuntimePQLV2).setDataSwap(true).setOptimizeDynamic(true).setInitValues(true).allPlatformsOfFactors(), new Platform("comp", Benchmark.mapnested, BasicPlatform.Compiler).setInitValues(true).allPlatformsOfFactors(), new Platform("comp_swap", Benchmark.mapnested, BasicPlatform.Compiler).setDataSwap(true).setInitValues(true).allPlatformsOfFactors());//.setStandardGraphs(new Platform("runtime_pql2", Benchmark.mapnested, BasicPlatform.RuntimePQLV2));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.calc();*/

            /*OutputTable output = new OutputTable(OutputType.BoxGraph, 1, 4,new String [] {""}, new String [] {"wordcount", "threegrep", "bonus", "webgraph"});
            Graph graph1 = new Graph( new Platform("v2_s", Benchmark.wordcount, BasicPlatform.RuntimePQLV2), new Platform("init_s", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setInitValues(true), new Platform("v2_p", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel), new Platform("init_p", Benchmark.wordcount, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel).setInitValues(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.wordcount, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph2 = new Graph( new Platform("v2_s", Benchmark.threegrep, BasicPlatform.RuntimePQLV2), new Platform("init_s", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setInitValues(true), new Platform("v2_p", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel), new Platform("init_p", Benchmark.threegrep, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel).setInitValues(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.threegrep, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph3 = new Graph( new Platform("v2_s", Benchmark.bonus, BasicPlatform.RuntimePQLV2), new Platform("init_s", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setInitValues(true), new Platform("v2_p", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel), new Platform("init_p", Benchmark.bonus, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel).setInitValues(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.bonus, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            Graph graph4 = new Graph( new Platform("v2_s", Benchmark.webgraph, BasicPlatform.RuntimePQLV2), new Platform("init_s", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setInitValues(true), new Platform("v2_p", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel), new Platform("init_p", Benchmark.webgraph, BasicPlatform.RuntimePQLV2).setParallelMode(ParallelMode.Parallel).setInitValues(true) ).setStandardGraphs(new Platform("Manual_s", Benchmark.webgraph, BasicPlatform.Manual).setParallelMode(Platform.ParallelMode.Parallel));
            output.table[0][0] = graph1;
            output.table[0][1] = graph2;
            output.table[0][2] = graph3;
            output.table[0][3] = graph4;
            output.calc();*/

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
