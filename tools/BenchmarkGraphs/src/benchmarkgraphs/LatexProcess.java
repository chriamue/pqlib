/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author Hilmar
 */
public class LatexProcess {
    
    public static void run () throws Exception {
        
        Process process = Runtime.getRuntime ().exec (BenchmarkGraphs.texCommand + " " + BenchmarkGraphs.texFile);
        InputStream stderr = process.getErrorStream ();
        InputStream stdout = process.getInputStream ();

        BufferedReader reader = new BufferedReader (new InputStreamReader(stdout));
        BufferedReader readerErr = new BufferedReader (new InputStreamReader(stderr));       

        while (true) {
            boolean finished = true;
            try {
                process.exitValue();
            } catch (Exception e) {
                finished = false;
            }
            if (finished)
                break;
            String line = "";
            while ((line = reader.readLine ()) != null) {                
                System.out.println("[pdflatex]  " + line);
            }

            while ((line = readerErr.readLine ()) != null) {
                throw new Exception(line);
            }
        }
    }
    
}
