/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

/**
 *
 * @author Hilmar
 */
public class Platform {

    public enum Benchmark {threegrep, webgraph, wordcount, bonus, setnested, mapnested, arraynested};
    public enum BasicPlatform {RuntimePQL, RuntimePQLV2, Compiler, Manual, ManualParallel};
    public enum ParallelMode {Serial, Parallel, ParallelIndividual};

    public String name;
    public Benchmark benchmark;
    public BasicPlatform basicPlatform;
    public ParallelMode parallel;
    public boolean constAntiOptimize;
    public boolean instanceAntiOptimize;
    public int dataSize;
    public boolean initValues;
    public boolean optimizeStatic;
    public boolean optimizeBytecode;
    public boolean dataSwap;
    public boolean optimizeDynamic;
    public boolean justAccessPath;
    public boolean justRealInit;
    public boolean justRemain;
    public int cores;

    public Platform (String _name, Benchmark _benchmark, BasicPlatform _basicPlatform) {
        name = _name;
        benchmark = _benchmark;
        basicPlatform = _basicPlatform;
        parallel = ParallelMode.Serial;
        constAntiOptimize = false;
        instanceAntiOptimize = false;
        dataSize = 1;
        initValues = false;
        optimizeStatic = true;
        optimizeBytecode = false;
        dataSwap = false;
        optimizeDynamic = false;
        justAccessPath = false;
        justRealInit = false;
        cores = 1;
    }

    public Platform copy () {
        Platform newPlatform = new Platform(name, benchmark, basicPlatform);
        newPlatform.parallel = parallel;
        newPlatform.constAntiOptimize = constAntiOptimize;
        newPlatform.instanceAntiOptimize = instanceAntiOptimize;
        newPlatform.dataSize = dataSize;
        newPlatform.initValues = initValues;
        newPlatform.optimizeStatic = optimizeStatic;
        newPlatform.optimizeBytecode = optimizeBytecode;
        newPlatform.dataSwap = dataSwap;
        newPlatform.optimizeDynamic = optimizeDynamic;
        newPlatform.justAccessPath = justAccessPath;
        newPlatform.justRealInit = justRealInit;
        newPlatform.cores = cores;
        return newPlatform;
    }

    public String getPlatformName () {
        switch (basicPlatform) {
            case RuntimePQL:
                return "runtime_pql";
            case RuntimePQLV2:
                return "runtime_pql_v2";
            case Compiler:
                return "pql-" + (parallel == ParallelMode.Serial ? 1 : (parallel == ParallelMode.Parallel ? BenchmarkGraphs.cores : cores) );
            case Manual:
                return "manual";
            case ManualParallel:
                return ("para-manual-" + (parallel == ParallelMode.Parallel ? BenchmarkGraphs.cores : cores));
        }
        return null;
    }

    public boolean noResultCheck () {
        if (basicPlatform == BasicPlatform.Compiler && (parallel == ParallelMode.Serial || (parallel == ParallelMode.ParallelIndividual && cores == 1)) && (benchmark == Benchmark.threegrep || benchmark == Benchmark.wordcount || benchmark == Benchmark.bonus || benchmark == Benchmark.webgraph))
            return true;
        return false;
    }

    public Platform [] allPlatformsOfFactors () {
        Platform [] returnPlatforms = new Platform[BenchmarkGraphs.factors.length];
        for (int i=0; i<BenchmarkGraphs.factors.length; i++)
            returnPlatforms[i] = this.copy().setDataSize(BenchmarkGraphs.factors[i]);
        return returnPlatforms;
    }

    public Platform [] allPlatformsOfCores () {
        Platform [] returnPlatforms = new Platform[BenchmarkGraphs.coresList.length];
        for (int i=0; i<BenchmarkGraphs.coresList.length; i++)
            returnPlatforms[i] = this.copy().setCores(BenchmarkGraphs.coresList[i]);
        return returnPlatforms;
    }

    public String getGraphName () {
        return name;
    }

    public Platform setParallelMode (ParallelMode _parallel) {
        parallel = _parallel;
        return this;
    }

    public Platform setConstAntiOptimize (boolean _constAntiOptimize) {
        constAntiOptimize = _constAntiOptimize;
        return this;
    }

    public Platform setInstanceAntiOptimize (boolean _instanceAntiOptimize) {
        instanceAntiOptimize = _instanceAntiOptimize;
        return this;
    }

    public Platform setDataSize (int _dataSize) {
        dataSize = _dataSize;
        return this;
    }

    public Platform setInitValues (boolean _initValues) {
        initValues = _initValues;
        return this;
    }

    public Platform setOptimizeStatic (boolean _optimizeStatic) {
        optimizeStatic = _optimizeStatic;
        return this;
    }

    public Platform setOptimizeBytecode (boolean _optimizeBytecode) {
        optimizeBytecode = _optimizeBytecode;
        return this;
    }

    public Platform setDataSwap (boolean _dataSwap) {
        dataSwap = _dataSwap;
        return this;
    }

    public Platform setOptimizeDynamic (boolean _optimizeDynamic) {
        optimizeDynamic = _optimizeDynamic;
        return this;
    }

    public Platform setJustAccessPath (boolean _justAccessPath) {
        justAccessPath = _justAccessPath;
        return this;
    }

    public Platform setJustRealInit (boolean _justRealInit) {
        justRealInit = _justRealInit;
        return this;
    }

    public Platform setJustRemain (boolean _justRemain) {
        justRemain = _justRemain;
        return this;
    }

    public Platform setCores (int _cores) {
        cores = _cores;
        return this;
    }

    @Override
    public String toString () {
        return "[Platform]  \"" + name + "\" " + benchmark.toString() + " " + basicPlatform.toString() + " " +  parallel.toString() + " (" + (constAntiOptimize? " const-anti-optimized," : "") + (instanceAntiOptimize? " instance-anti-optimized," : "") + (optimizeStatic? " optimize-static," : "") + (optimizeBytecode? " optimize-static," : "") + (dataSwap ? " data-swap," : "") + (optimizeDynamic ? " optimize-dynamic," : "") + (justAccessPath ? " just-access-path," : "") + (justRealInit ? " just-real-init," : "") + (justRemain ? " just-remain," : "") + (cores > 1 ? " cores: " + cores : "") + " datasize: " + dataSize + ", init: " + initValues + ")";
    }

}
