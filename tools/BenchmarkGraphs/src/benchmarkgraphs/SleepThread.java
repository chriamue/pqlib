/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarkgraphs;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hilmar
 */
public class SleepThread extends Thread {

    public Set emptySet;

    public SleepThread (Set _emptySet) {
        synchronized(_emptySet) {
            emptySet = _emptySet;
        }
    }

    @Override
    public void run () {
        try {
            //Thread.sleep(3*1000);
            Thread.sleep(250);
        } catch (InterruptedException ex) {
            Logger.getLogger(SleepThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        synchronized(emptySet) {
        emptySet.add(null);
        }
    }

    public static void wait10Seconds () {

        if (BenchmarkGraphs.resultsFromFile != null)
            return;
        if (!BenchmarkGraphs.alternativeCommand)
            System.out.println("   =>  Sleep start.");
        Set set = Collections.synchronizedSet(new HashSet<String>());
        new SleepThread(set).start();
        while (true) {
        boolean ready = false;
        synchronized (set) {
            if (set.size() > 0)
                ready = true;
        }
        if (ready)
            break;
        }
        if (!BenchmarkGraphs.alternativeCommand)
            System.out.println("   =>  Sleep ready.");
    }
}
